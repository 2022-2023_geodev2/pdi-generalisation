-- file: galbe_C.ads

--***************************************************************************--
--**  Ce projet a pour objectif de creer une librairie C comprennant les   **--
--**  algorithmes GALBE et de lissage_filtrage pour qu'elles soient        **--
--**  utilisables sous python (bindage)                                    **--
--**                                                                       **--
--**  Fichiers sources:  galbe_C.adb  galbe_C.ads  galbe_C.gpr             **--
--**                                                                       **--
--**  Auteur: Giudicelli Vincent                                           **--
--**  Email:  vincent.giudicelli@ensg.eu                                   **--
--**  Date de création: 2023-04-04                                         **--
--**  Dernière modification: 2023-04-13 par V. Giudicelli                  **--
--***************************************************************************--

With Interfaces.C.Strings;

Package Galbe_C IS

	Value_Error : exception;

	FUNCTION Douglas_Peucker (
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		resolution	: positive;
		seuil_str	: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Douglas_Peucker, "douglas_peucker");

	FUNCTION Filtre_Gaussien (
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		resolution	: positive;
		sigma_str	: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Filtre_Gaussien, "filtre_gaussien");



--	FUNCTION Faille_Min (
--		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
--		Largeur_str	: Interfaces.C.Strings.chars_ptr;
--		Courbure_str: Interfaces.C.Strings.chars_ptr;
--		Resolution	: positive)
--	RETURN Interfaces.C.Strings.chars_ptr;
--	PRAGMA EXPORT (C, Faille_Min, "faille_min");

	FUNCTION Faille_Max (
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		Largeur_str	: Interfaces.C.Strings.chars_ptr;
		Resolution	: positive)
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Faille_Max, "faille_max");

	FUNCTION Accordeon(
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		Largeur_str	: Interfaces.C.Strings.chars_ptr;
		Resolution	: positive;
		Auto_str	: Interfaces.C.Strings.chars_ptr;	-- default 1 (=True)
		Sigma_str	: Interfaces.C.Strings.chars_ptr)	-- default 20.0
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Accordeon, "accordeon");

	FUNCTION Schemat(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: positive;
		Auto_sigma_str	: Interfaces.C.Strings.chars_ptr;
		Sigma_str		: Interfaces.C.Strings.chars_ptr;
		Auto_choix_str	: Interfaces.C.Strings.chars_ptr;
		virag1			: integer;
		virag2			: integer)
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Schemat, "schematisation");


	FUNCTION Decoupage_Conflits(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive;
		Largeur_str		: Interfaces.C.Strings.chars_ptr;
		Separabilite_str: Interfaces.C.Strings.chars_ptr;
		Tolerance_str	: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Decoupage_Conflits, "decoupage_conflits");
	
	FUNCTION Indicateur_Conflits(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive;
		Largeur_str		: Interfaces.C.Strings.chars_ptr;
		Tolerance_str	: Interfaces.C.Strings.chars_ptr) 
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Indicateur_Conflits, "indicateur_conflits");
	
	FUNCTION Intersections(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive) 
	RETURN Interfaces.C.Strings.chars_ptr;
	PRAGMA EXPORT (C, Intersections, "nb_intersections");

	
--	FUNCTION Deplacement_amorti(
--		WKT_line_in			: Interfaces.C.Strings.chars_ptr;
--		Resolution			: Positive;
--		Point_initial_str	: Interfaces.C.Strings.chars_ptr;
--		Point_final_str		: Interfaces.C.Strings.chars_ptr;
--		Amortissement_str	: Interfaces.C.Strings.chars_ptr)
--	RETURN Interfaces.C.Strings.chars_ptr;
--	PRAGMA EXPORT (C, Deplacement_amorti, "deplacement_amorti");
	
END Galbe_C;
