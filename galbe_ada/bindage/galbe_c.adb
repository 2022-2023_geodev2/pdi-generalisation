-- file: galbe_C.adb

--***************************************************************************--
--**  Description du projet: voir le fichier galbe_C.ads                   **--
--***************************************************************************--
With Ada.Exceptions;

With WKT_io; Use WKT_io;
With Gen_io; Use Gen_io;
With Ada.Strings.Fixed;

With Lissage_Filtrage;
With Caricature;
With Schematisation;
With Morpholigne;
With Mesures_Comparaison;
With Propagation;

--with ADA.Text_io; Use ada.text_io;

Package BODY Galbe_C IS

	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Douglas_Peucker (
		WKT_line_in : Interfaces.C.Strings.chars_ptr;
		resolution: positive;
		seuil_str: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr IS
		-- manipulation des lignes
		tab_points_in_double: Point_Access_Double;
		centre			: Point_type_double;
		tab_points_in	: Point_Access_Type;
		tab_points_out	: Point_Access_Type;
		nb_pts_in		: Natural;
		nb_pts_out		: Natural;
		-- préparation des autres paramètres
		seuil			: Float;
	BEGIN 
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		nb_pts_in := tab_points_in'Length;
		tab_points_out := new Point_Liste_type(tab_points_in'Range);
		-- préparation des autres paramètres
		BEGIN
			seuil := Float'value(Interfaces.C.Strings.value(seuil_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'seuil'. Expected: Float";
		END;
		-- appel de la fonction bindée
		Lissage_Filtrage.Douglas_Peucker(
			tab_points_in,
			nb_pts_in,
			tab_points_out,
			nb_pts_out,
			seuil,
			resolution);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					Clip_list(
						tab_points_out,
						nb_pts_out
					),
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Douglas_Peucker;
	
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Filtre_Gaussien (
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		resolution	: positive;
		sigma_str	: Interfaces.C.Strings.chars_ptr)	-- ce parametre est actuellement une enigme
	RETURN Interfaces.C.Strings.chars_ptr
	IS 
		-- manipulation des lignes
		tab_points_in_double : Point_Access_Double;
		centre : Point_type_double;
		tab_points_in : Point_Access_Type;
		tab_points_out : Point_Access_Type;
		nb_pts_in : Natural;
		-- préparation des autres paramètres
		sigma  : Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		nb_pts_in := tab_points_in'Length;
		DECLARE
			list_points_out : Point_Liste_Type := tab_points_in(tab_points_in'Range);	-- INITIALISATION DE LONGUEUR ARBITRAIRE !!!
		BEGIN
			-- préparation des autres paramètres
			BEGIN
				sigma := Float'value(Interfaces.C.Strings.value(sigma_str)) * Float(Resolution);
			EXCEPTION
				WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'sigma'. Expected: Float";
			END;
			-- appel de la fonction bindée
			Lissage_Filtrage.Filtre_Gaussien_N(
				tab_points_in(tab_points_in'Range),
				nb_pts_in,
				sigma,
				list_points_out);
			-- traitement du résultat
			tab_points_out := new Point_Liste_type(list_points_out'Range);
			tab_points_out(list_points_out'Range) := list_points_out(list_points_out'Range);
			RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
				Point_Access_Double_To_Linestring(				-- transformation en String
					Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
						tab_points_out,
						resolution,
						centre)
					)
				);
		END;
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Filtre_Gaussien;

	
	
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ERREUR DANS CERTAINS CAS => accès mémoire entrainant la corruption de l'executeur.                            --
	-- Fonction redéveloppée en python                                                                               --
	-- ############################################################################################################# --
	FUNCTION Faille_Min (
		WKT_line_in : Interfaces.C.Strings.chars_ptr;
		Largeur_str : Interfaces.C.Strings.chars_ptr;
		Courbure_str: Interfaces.C.Strings.chars_ptr;
		Resolution  : positive)
	RETURN Interfaces.C.Strings.chars_ptr
	IS 
		-- manipulation des lignes
		tab_points_in_double : Point_Access_Double;
		centre : Point_type_double;
		tab_points_in : Point_Access_Type;
		tab_points_out : Point_Access_Type;
		-- préparation des autres paramètres
		largeur  : Float;
		courbure : Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			largeur := Float'value(Interfaces.C.Strings.value(largeur_str)) * Float(Resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'largeur'. Expected: Float";
		END;
		BEGIN
			courbure := Float'value(Interfaces.C.Strings.value(courbure_str)) * Float(Resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'courbure'. Expected: Float";
		END;
		-- appel de la fonction bindée
		Caricature.decalage_squelette(
			tab_points_in(tab_points_in'Range),
			largeur,
			courbure,
			tab_points_out);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Faille_Min;
		
	
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Faille_Max (
		WKT_line_in : Interfaces.C.Strings.chars_ptr;
		Largeur_str : Interfaces.C.Strings.chars_ptr;
		Resolution  : positive)
	RETURN Interfaces.C.Strings.chars_ptr
	IS 
		-- manipulation des lignes
		tab_points_in_double : Point_Access_Double;
		centre : Point_type_double;
		tab_points_in : Point_Access_Type;
		tab_points_out : Point_Access_Type;
		-- préparation des autres paramètres
		largeur  : Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			largeur := Float'value(Interfaces.C.Strings.value(largeur_str)) * Float(Resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'largeur'. Expected: Float";
		END;
		-- appel de la fonction bindée
		Caricature.Faille_max(
			tab_points_in(tab_points_in'Range),
			largeur,
			tab_points_out);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Faille_Max;
	
	
	
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Accordeon(
		WKT_line_in	: Interfaces.C.Strings.chars_ptr;
		Largeur_str	: Interfaces.C.Strings.chars_ptr;
		Resolution	: positive;
		Auto_str	: Interfaces.C.Strings.chars_ptr;	-- default TRUE: "1" pour TRUE, "0" ou autre pour FALSE
		Sigma_str	: Interfaces.C.Strings.chars_ptr)	-- default 20.0
	RETURN Interfaces.C.Strings.chars_ptr
	IS
		-- manipulation des lignes
		tab_points_in_double : Point_Access_Double;
		centre : Point_type_double;
		tab_points_in : Point_Access_Type;
		tab_points_out : Point_Access_Type;
		-- préparation des autres paramètres
		largeur	: Float;
		sigma	: Float;
		auto    : Boolean := Interfaces.C.Strings.value(Auto_str) = "1";
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			largeur := Float'value(Interfaces.C.Strings.value(largeur_str)) * Float(Resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'largeur'. Expected: Float";
		END;
		BEGIN
			sigma := Float'value(Interfaces.C.Strings.value(sigma_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'sigma'. Expected: Float";
		END;
		-- appel de la fonction bindée
		Caricature.Accordeon(
			tab_points_in(tab_points_in'Range),
			largeur,
			tab_points_out,
			auto,
			sigma);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Accordeon;
	
	
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Schemat(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: positive;
		Auto_sigma_str	: Interfaces.C.Strings.chars_ptr;
		Sigma_str		: Interfaces.C.Strings.chars_ptr;
		Auto_choix_str	: Interfaces.C.Strings.chars_ptr;
		virag1			: integer;
		virag2			: integer)
	RETURN Interfaces.C.Strings.chars_ptr IS
		-- manipulation des lignes
		tab_points_in_double : Point_Access_Double;
		centre : Point_type_double;
		tab_points_in : Point_Access_Type;
		tab_points_out : Point_Access_Type;
		-- préparation des autres paramètres
		auto_sigma	: Boolean := Interfaces.C.Strings.value(Auto_sigma_str) = "1";
		sigma		: Float;
		auto_choix	: Boolean := Interfaces.C.Strings.value(Auto_choix_str) = "1";
		v1			: Integer := virag1;	-- on doit passer par une variable accessible en écriture
		v2			: Integer := virag2;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			sigma := Float'value(Interfaces.C.Strings.value(sigma_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'sigma'. Expected: Float";
		END;
		-- appel de la fonction bindée
		Schematisation.Schemat (
			tab_points_in(tab_points_in'Range),
			tab_points_out,
			auto_sigma,
			sigma,
			auto_choix,
			v1,
			v2);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Schemat;


	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Decoupage_Conflits(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive;
		Largeur_str		: Interfaces.C.Strings.chars_ptr;
		Separabilite_str: Interfaces.C.Strings.chars_ptr;
		Tolerance_str	: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr
	IS
		-- manipulation des lignes
		tab_points_in_double: Point_Access_Double;
		centre	: Point_type_double;
		tab_points_in	: Point_Access_Type;
		tab_points_out	: Point_Access_Type;
		-- préparation des autres paramètres
		largeur		: Float;
		separabilite: Float;
		tolerance	: Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			largeur := Float'value(Interfaces.C.Strings.value(largeur_str)) * float(resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'largeur'. Expected: Float";
		END;
		BEGIN
			separabilite := Float'value(Interfaces.C.Strings.value(separabilite_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'separabilite'. Expected: Float";
		END;
		BEGIN
			tolerance := Float'value(Interfaces.C.Strings.value(tolerance_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'tolerance'. Expected: Float";
		END;
		-- appel de la fonction bindée
		tab_points_out := Morpholigne.Decoupage_Conflits(
			tab_points_in(tab_points_in'Range),
			largeur,
			separabilite,
			tolerance);
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Decoupage_Conflits;

	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Indicateur_Conflits(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive;
		Largeur_str		: Interfaces.C.Strings.chars_ptr;
		Tolerance_str	: Interfaces.C.Strings.chars_ptr) 
	RETURN Interfaces.C.Strings.chars_ptr
	IS
		-- manipulation des lignes
		tab_points_in_double: Point_Access_Double;
		centre	: Point_type_double;
		tab_points_in	: Point_Access_Type;
		-- préparation des autres paramètres
		largeur		: Float;
		tolerance	: Float;
		force_conflit : Morpholigne.Force_Conflit;
		gauche		: Float;
		droite		: Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			largeur := Float'value(Interfaces.C.Strings.value(largeur_str)) * float(resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'largeur'. Expected: Float";
		END;
		BEGIN
			tolerance := Float'value(Interfaces.C.Strings.value(tolerance_str));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'tolerance'. Expected: Float";
		END;
		-- appel de la fonction bindée
		force_conflit := Morpholigne.Indicateur_Conflits(
			tab_points_in(tab_points_in'Range),
			largeur,
			tolerance
			);
		-- traitement du résultat
		gauche := force_conflit(Morpholigne.Gauche)/float(resolution);
		droite := force_conflit(Morpholigne.Droite)/float(resolution);
		RETURN Interfaces.C.Strings.New_String(gauche'img & " " & droite'img);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Indicateur_Conflits;

	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ############################################################################################################# --
	FUNCTION Intersections(
		WKT_line_in		: Interfaces.C.Strings.chars_ptr;
		Resolution		: Positive) 
	RETURN Interfaces.C.Strings.chars_ptr
	IS
		-- manipulation des lignes
		tab_points_in_double: Point_Access_Double;
		centre	: Point_type_double;
		tab_points_in	: Point_Access_Type;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- appel et retour de la fonction bindée
		RETURN Interfaces.C.Strings.New_String(Natural'Image(Mesures_Comparaison.Intersections(
			tab_points_in(tab_points_in'Range),
			tab_points_in'Last
			)));
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Intersections;


	-- ############################################################################################################# --
	-- ############################################################################################################# --
	-- ERREUR DANS CERTAINS CAS => accès mémoire entrainant la corruption de l'executeur.                            --
	-- Fonction redéveloppée en python                                                                               --
	-- ############################################################################################################# --
	FUNCTION Deplacement_amorti(
		WKT_line_in			: Interfaces.C.Strings.chars_ptr;
		Resolution			: Positive;
		Point_initial_str	: Interfaces.C.Strings.chars_ptr;
		Point_final_str		: Interfaces.C.Strings.chars_ptr;
		Amortissement_str	: Interfaces.C.Strings.chars_ptr)
	RETURN Interfaces.C.Strings.chars_ptr
	IS
		-- manipulation des lignes
		tab_points_in_double: Point_Access_Double;
		centre	: Point_type_double;
		tab_points_in	: Point_Access_Type;
		tab_points_out	: Point_Access_Type;
		-- préparation des autres paramètres
		point_initial	: Point_type;
		point_final		: Point_type;
		amortissement	: Float;
	BEGIN
		-- transformation de la wkt_line en liste de points, puis en liste de points en coordonnées pixels centré
		tab_points_in_double := LineString_to_Point_Access_double(Interfaces.C.Strings.value(WKT_line_in));
		centre := MIN(tab_points_in_double);--(tab_points_in_double'First);
		tab_points_in := Point_Access_Double_to_Point_Access_Type(
			tab_points_in_double,
			resolution,
			centre);
		-- préparation des autres paramètres
		BEGIN
			amortissement := Float'value(Interfaces.C.Strings.value(amortissement_str)) * float(resolution);
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'amortissement'. Expected: Float";
		END;
		DECLARE
			point_str: string := Interfaces.C.Strings.value(Point_initial_str);
			index_sep: integer := Ada.Strings.Fixed.INDEX(point_str, " ");
		BEGIN
			point_initial := (
				coor_x => integer((double'Value(point_str(    1    ..  index_sep   )) - centre.coor_x) * double(resolution)),
				coor_y => integer((double'Value(point_str(index_sep..point_str'Last)) - centre.coor_y) * double(resolution)));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'point_initial'. Expected: '{x} {y}' with {x} in FLOAT, {y} in FLOAT";
		END;
		DECLARE
			point_str: string := Interfaces.C.Strings.value(Point_final_str);
			index_sep: integer := Ada.Strings.Fixed.INDEX(point_str, " ");
		BEGIN
			point_final := (
				coor_x => integer((double'Value(point_str(    1    ..  index_sep   )) - centre.coor_x) * double(resolution)),
				coor_y => integer((double'Value(point_str(index_sep..point_str'Last)) - centre.coor_y) * double(resolution)));
		EXCEPTION
			WHEN CONSTRAINT_ERROR => RAISE Value_Error WITH "Invalid value for parameter 'point_initial'. Expected: '{x} {y}' with {x} in FLOAT, {y} in FLOAT";
		END;
		-- appel de la fonction bindée
		DECLARE
			list_points_out: Point_Liste_type(tab_points_in'Range);
			nb_pts: integer := tab_points_in'Last;
		BEGIN
			Propagation.DeplaceArc_amorti(
				tab_points_in(tab_points_in'Range),
				list_points_out,
				nb_pts,
				point_initial,
				point_final,
				amortissement);
			tab_points_out := new Point_Liste_Type(tab_points_in'Range);
			tab_points_out(tab_points_out'Range) := list_points_out;
		END;
		-- traitement du résultat
		RETURN Interfaces.C.Strings.New_String(				-- transformation en C_String
			Point_Access_Double_To_Linestring(				-- transformation en String
				Point_Access_type_to_Point_Access_Double(	-- retour en coordonnées réélles
					tab_points_out,
					resolution,
					centre)
				)
			);
	EXCEPTION -- gestion des erreurs
		WHEN error : OTHERS =>
			RETURN Interfaces.C.Strings.New_String(Ada.Exceptions.Exception_Information(error));
	END Deplacement_amorti;


END Galbe_C;

