-- file: WKT_io.ads

--***************************************************************************--
--**  Ce package à pour objectif de permettre l'affichage et la conversion **--
--**  de liste de points en une chaine de caractère (String) au format WKT **--
--**  (LineString), dans l'objectif d'un bindage des algoritmes GALBE en   **--
--**  python.                                                              **--
--**                                                                       **--
--**  Fichiers sources:  wkt_io.adb.adb  wkt_io.ads                        **--
--**                                                                       **--
--**  Auteur: Giudicelli Vincent                                           **--
--**  Email:  vincent.giudicelli@ensg.eu                                   **--
--**  Date de création: 2023-03-29                                         **--
--**  Dernière modification: 2023-04-04 par V. Giudicelli                  **--
--***************************************************************************--

With Ada.Strings.Unbounded; Use Ada.Strings.Unbounded;

With gen_io; Use gen_io;

Package WKT_io IS

	--***********************************************************************--
	--**  Exceptions                                                       **--
	--**                                                                   **--
	--**  NOTE : les exceptions ne sont pas nativement supportées par le   **--
	--**  bindage, elles devront etre transmises sous la forme d'une       **--
	--**  chaine de caractère de type Intefaces.C.Strings puis être        **--
	--**  traitées manuellement en python.                                 **--
	--***********************************************************************--
	WKT_LineString_Error : EXCEPTION;

	--***********************************************************************--
	--**  Nouveau type de points                                           **--
	--***********************************************************************--
	TYPE double IS digits 18;
	
	TYPE Point_Type_double IS RECORD
		Coor_x: double;
		Coor_y: double;
	END RECORD;
	
	TYPE Point_Liste_double IS Array(natural Range<>) Of Point_Type_double;
	TYPE Point_Access_double IS Access Point_Liste_double;

	--***********************************************************************--
	--**  Affichage des points et listes de points                         **--
	--***********************************************************************--
	PROCEDURE Put(point: Point_type);
	PROCEDURE Put(point: Point_type_reel);
	PROCEDURE Put(point: Point_type_double);
	PROCEDURE Put(list_points: Point_liste_type);
	PROCEDURE Put(list_points: Point_liste_reel);
	PROCEDURE Put(list_points: Point_liste_double);
	PROCEDURE Put(tab_points : Point_Access_type);
	PROCEDURE Put(tab_points : Point_Access_reel);
	PROCEDURE Put(tab_points : Point_Access_double);

	--***********************************************************************--
	--**  Conversions de string WKT en Point_Access et réciproquement      **--
	--***********************************************************************--

	FUNCTION LineString_to_Point_Access_double(
		LineString: String)
	RETURN Point_Access_double;
	
	FUNCTION Point_Access_double_to_LineString(
		Point_list : Point_Access_double)
	RETURN String;
	
	FUNCTION Point_Access_Double_to_Point_Access_Type(
		tab_points_double: Point_Access_double;
		resolution: positive;
		centre: Point_type_double := (0.0, 0.0)) 
	RETURN Point_Access_Type;

	FUNCTION Point_Access_Type_to_Point_Access_Double(
		tab_points_type: Point_Access_type;
		resolution: positive;
		centre: Point_type_double := (0.0, 0.0)) 
	RETURN Point_Access_Double;

	--***********************************************************************--
	--**  Autres manipulations de listes de points                         **--
	--***********************************************************************--

	FUNCTION Clip_List(
		tab_points_in: Point_Access_reel;
		size: Positive)
	RETURN Point_Access_reel;

	FUNCTION Clip_List(
		tab_points_in: Point_Access_type;
		size: Positive)
	RETURN Point_Access_type;

	FUNCTION Clip_List(
		tab_points_in: Point_Access_double;
		size: Positive)
	RETURN Point_Access_double;
	
	--***********************************************************************--
	--**  récupération du point minimum                                    **--
	--***********************************************************************--

	FUNCTION MIN( tab_points_in: Point_Access_Type	)	RETURN Point_Type;
	FUNCTION MIN( tab_points_in: Point_Access_Reel	)	RETURN Point_Type_Reel;
	FUNCTION MIN( tab_points_in: Point_Access_Double)	RETURN Point_Type_Double;

END WKT_io;

