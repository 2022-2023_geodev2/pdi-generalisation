-- file: WKT_io.adb

--***************************************************************************--
--**  Description du package: voir le fichier WKT_io.ads                   **--
--***************************************************************************--

With Ada.Text_IO; Use Ada.Text_IO;
With Ada.Integer_Text_IO; Use Ada.Integer_Text_IO;
With Ada.Strings.Unbounded; Use Ada.Strings.Unbounded;
With Ada.Float_Text_IO; Use Ada.Float_Text_IO;
With Ada.Strings.Fixed; Use Ada.Strings.Fixed;

With gen_io; Use gen_io;

Package BODY WKT_io IS

	--***********************************************************************--
	--**  Affichage des points et listes de points                         **--
	--***********************************************************************--

	-- points
	PROCEDURE Put(point: Point_type) IS
	BEGIN
		Put(Item => point.Coor_x'Img & " " & point.Coor_y'Img, File => Standard_Output);
	END Put;

	PROCEDURE Put(point: Point_type_reel) IS
	BEGIN
		Put(Item => point.Coor_x'Img & " " & point.Coor_y'Img, File => Standard_Output);
	END Put;

	PROCEDURE Put(point: Point_type_double) IS
	BEGIN
		Put(Item => point.Coor_x'Img & " " & point.Coor_y'Img, File => Standard_Output);
	END Put;

	-- liste de points
	PROCEDURE Put(list_points: Point_liste_type) IS
	BEGIN
		Put(Item => "(", File => Standard_Output);
		FOR i_point IN list_points'Range LOOP
			Put(list_points(i_point));
			IF i_point /= list_points'Last THEN
				Put(Item => ", ", File => Standard_Output);
			END IF;
		END LOOP;
		Put(Item => ")", File => Standard_Output);
	END Put;

	PROCEDURE Put(list_points: Point_liste_reel) IS
	BEGIN
		Put(Item => "(", File => Standard_Output);
		FOR i_point IN list_points'Range LOOP
			Put(list_points(i_point));
			IF i_point /= list_points'Last THEN
				Put(Item => ", ", File => Standard_Output);
			END IF;
		END LOOP;
		Put(Item => ")", File => Standard_Output);
	END Put;
	
	PROCEDURE Put(list_points: Point_liste_double) IS
	BEGIN
		Put(Item => "(", File => Standard_Output);
		FOR i_point IN list_points'Range LOOP
			Put(list_points(i_point));
			IF i_point /= list_points'Last THEN
				Put(Item => ", ", File => Standard_Output);
			END IF;
		END LOOP;
		Put(Item => ")", File => Standard_Output);
	END Put;

	-- access liste de points
	PROCEDURE Put(tab_points : Point_Access_type) IS
	BEGIN
		Put(Item => "Point_Access_type", File => Standard_Output);
		Put(tab_points(tab_points'Range));
		New_line(File => Standard_Output);
	END Put;

	PROCEDURE Put(tab_points : Point_Access_reel) IS
	BEGIN
		Put(Item => "Point_Access_reel", File => Standard_Output);
		Put(tab_points(tab_points'Range));
		New_line(File => Standard_Output);
	END Put;

	PROCEDURE Put(tab_points : Point_Access_double) IS
	BEGIN
		Put(Item => "Point_Access_double", File => Standard_Output);
		Put(tab_points(tab_points'Range));
		New_line(File => Standard_Output);
	END Put;


	--***********************************************************************--
	--**  Conversions de string WKT en Point_Access et réciproquement      **--
	--***********************************************************************--

	-- Convertit une linestring en une liste de points
	FUNCTION LineString_to_Point_Access_double(LineString: String)
	RETURN Point_Access_double IS
		-- Détermine la longueur de la liste
		Length_Line : natural := Ada.Strings.Fixed.Count(Source => LineString, Pattern => ",") + 1;
		-- Crée la liste de points
		Point_list : Point_Access_double := new Point_liste_double(1 .. Length_Line);
		-- Initialise les index pour parcourir la linestring
		Next_index_sep_pt : natural := Index(LineString, "(");
		Next_index_sep_nb : natural := Next_index_sep_pt;
		Actual_index_sep_pt : natural := Next_index_sep_pt;
		index_point : natural := point_list'First;
	BEGIN
		IF LineString = "" THEN
			RAISE WKT_LineString_Error WITH "Invalid LineString : ''";
		END IF;
		-- Parcours la linestring pour extraire chaque point
		LOOP
			-- Recherche le prochain séparateur pour la coordonnée x
			Actual_index_sep_pt := Next_index_sep_pt+1;
			IF LineString(Actual_index_sep_pt) = ' '
				THEN Actual_index_sep_pt := Actual_index_sep_pt + 1;
			END IF;
			-- Recherche le prochain séparateur pour la coordonnée y
			Next_index_sep_nb := Index(LineString(Actual_index_sep_pt .. LineString'Last-1), " ");
			Next_index_sep_pt := Index(LineString(Actual_index_sep_pt .. LineString'Last-1), ",");
			IF Next_index_sep_pt = 0 THEN
				Next_index_sep_pt := LineString'Last;
				IF LineString(Next_index_sep_pt) /= ')' THEN
					RAISE WKT_LineString_Error WITH "Invalid LineString : '" & LineString & "'";
				END IF;
			END IF;
			-- Extrait les coordonnées x et y
			DECLARE
				X_str : String := LineString(Actual_index_sep_pt .. Next_index_sep_nb-1);
				Y_str : String := LineString(Next_index_sep_nb+1 .. Next_index_sep_pt-1);
			BEGIN
				-- Ajoute le point à la liste
				Point_list(index_point) := (Coor_x => Double'Value(X_str), Coor_y => Double'Value(Y_str));
				index_point := index_point + 1;
			EXCEPTION
				WHEN CONSTRAINT_ERROR => RAISE WKT_LineString_Error WITH "Invalid LineString : '" & LineString & "'";
			END;
			EXIT WHEN Next_index_sep_pt = LineString'Last;
		END LOOP;
		RETURN Point_list;
	END LineString_to_Point_Access_double;
	-- Commenté avec ChatGPT
	
	
	-- Convertit une liste de points en linestring
	FUNCTION Point_Access_double_to_LineString(
		Point_list : Point_Access_double)
	RETURN String IS
		WKT_line : Unbounded_String := To_Unbounded_String("");
	BEGIN
		-- Vérifier si la liste est vide
		IF Point_list'Length = 0 THEN
			RETURN "LINESTRING EMPTY";
		END IF;
		
		-- Ajouter le début de la chaîne de caractères WKT
		WKT_line := WKT_line & "LINESTRING (";
		
		-- Ajouter chaque point de la linestring
		FOR i_point IN Point_list'Range LOOP
			WKT_line := WKT_line & Point_list(i_point).Coor_x'Img;
			WKT_line := WKT_line & " " & Point_list(i_point).Coor_y'Img;
			IF i_point /= Point_list'Last THEN
				WKT_line := WKT_line & ",";
			END IF;
		END LOOP;
		
		-- Ajouter la fin de la chaîne de caractères WKT
		WKT_line := WKT_line & ")";
		
		-- Retourner la chaîne de caractères WKT
		RETURN To_String(WKT_line);
	END Point_Access_double_to_LineString;
	-- Commenté avec ChatGPT
	
	
	-- Conversion  Point_Access_Type  <=>  Point_Access_Reel
	-- point_access_type: coordoonées en pixels (entiers)
	-- point_access_reel: coordonnées en mètres (flotants)
	-- resolution: en pixels / metres
	-- centre: point (de type point_reel) utilisé pour centrer la ligne (par exemple : le baricentre de la ligne)
	FUNCTION Point_Access_Double_to_Point_Access_Type(
		tab_points_double: Point_Access_double;
		resolution: positive;
		centre: Point_type_double := (0.0, 0.0)) 
	RETURN Point_Access_Type IS
		tab_points_type : Point_Access_Type := new Point_liste_type(tab_points_double'Range);
	BEGIN
		FOR i_point IN tab_points_double'Range LOOP
			tab_points_type(i_point).Coor_x := Integer((tab_points_double(i_point).Coor_x - centre.Coor_x) * Double(resolution));
			tab_points_type(i_point).Coor_y := Integer((tab_points_double(i_point).Coor_y - centre.Coor_y) * Double(resolution));
		END LOOP;
		RETURN tab_points_type;
	END Point_Access_Double_to_Point_Access_Type;

	FUNCTION Point_Access_Type_to_Point_Access_Double(
		tab_points_type: Point_Access_type;
		resolution: positive;
		centre: Point_type_double := (0.0, 0.0)) 
	RETURN Point_Access_Double IS
		tab_points_double : Point_Access_Double := new Point_liste_double(tab_points_type'Range);
	BEGIN
		FOR i_point IN tab_points_type'Range LOOP
			tab_points_double(i_point).Coor_x := Double(tab_points_type(i_point).Coor_x) / Double(resolution) + centre.Coor_x;
			tab_points_double(i_point).Coor_y := Double(tab_points_type(i_point).Coor_y) / Double(resolution) + centre.Coor_y;
		END LOOP;
		RETURN tab_points_double;
	END Point_Access_Type_to_Point_Access_Double;

	--***********************************************************************--
	--**  Autres manipulations de listes de points                         **--
	--***********************************************************************--

	-- Découpage de la liste
	-- tab_points_in : Point_Access_Reel ou Point_Access_Type de `size` ou plus points
	-- size: nombre de points à conserver
	FUNCTION Clip_List(
		tab_points_in: Point_Access_reel;
		size: Positive)
	RETURN Point_Access_reel IS
		tab_points_out : Point_Access_reel := new Point_Liste_reel(tab_points_in'First .. tab_points_in'First+size);
	BEGIN
		FOR i_pt IN tab_points_out'RANGE LOOP
			tab_points_out(i_pt) := tab_points_in(i_pt);
		END LOOP;
		RETURN tab_points_out;
	END Clip_List;

	FUNCTION Clip_List(
		tab_points_in: Point_Access_double;
		size: Positive)
	RETURN Point_Access_double IS
		tab_points_out : Point_Access_double := new Point_Liste_double(tab_points_in'First .. tab_points_in'First+size);
	BEGIN
		FOR i_pt IN tab_points_out'RANGE LOOP
			tab_points_out(i_pt) := tab_points_in(i_pt);
		END LOOP;
		RETURN tab_points_out;
	END Clip_List;

	FUNCTION Clip_List(
		tab_points_in: Point_Access_type;
		size: Positive)
	RETURN Point_Access_type IS
		tab_points_out : Point_Access_type := new Point_Liste_type(tab_points_in'First .. tab_points_in'First+size-1);
	BEGIN
		FOR i_pt IN tab_points_out'RANGE LOOP
			tab_points_out(i_pt) := tab_points_in(i_pt);
		END LOOP;
		RETURN tab_points_out;
	END Clip_List;
	
	
	--***********************************************************************--
	--**  récupération du point minimum                                    **--
	--***********************************************************************--
	
	FUNCTION MIN(
		tab_points_in: Point_Access_type
		)
	RETURN Point_Type IS
		mini : Point_Type := (integer'last, integer'last);
	BEGIN
		FOR i_point IN tab_points_in'Range LOOP
			IF tab_points_in(i_point).coor_x < mini.coor_x THEN mini.coor_x := tab_points_in(i_point).coor_x; END IF;
			IF tab_points_in(i_point).coor_y < mini.coor_y THEN mini.coor_y := tab_points_in(i_point).coor_y; END IF;
		END LOOP;
		RETURN mini;
	END MIN;

	FUNCTION MIN(
		tab_points_in: Point_Access_Reel
		)
	RETURN Point_Type_Reel IS
		mini : Point_Type_Reel := (Float'last, Float'last);
	BEGIN
		FOR i_point IN tab_points_in'Range LOOP
			IF tab_points_in(i_point).coor_x < mini.coor_x THEN mini.coor_x := tab_points_in(i_point).coor_x; END IF;
			IF tab_points_in(i_point).coor_y < mini.coor_y THEN mini.coor_y := tab_points_in(i_point).coor_y; END IF;
		END LOOP;
		RETURN mini;
	END MIN;

	FUNCTION MIN(
		tab_points_in: Point_Access_Double
		)
	RETURN Point_Type_Double IS
		mini : Point_Type_Double := (Double'last, Double'last);
	BEGIN
		FOR i_point IN tab_points_in'Range LOOP
			IF tab_points_in(i_point).coor_x < mini.coor_x THEN mini.coor_x := tab_points_in(i_point).coor_x; END IF;
			IF tab_points_in(i_point).coor_y < mini.coor_y THEN mini.coor_y := tab_points_in(i_point).coor_y; END IF;
		END LOOP;
		RETURN mini;
	END MIN;


END WKT_io;
