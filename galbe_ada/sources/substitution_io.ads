--================== SPECIFICATION DU PAQUETAGE SUBSTITUTION_IO =======================

with inipan; use inipan;
with tiff_io; use tiff_io;
with unchecked_deallocation;
with unchecked_conversion;


package SUBSTITUTION_IO  is

-- STRUCTURES
-------------  

type tabinterp is array (integer range <>) of string30;
type acces_tabinterp is access tabinterp;

type tabsub is array (integer range <>, integer range <>) of integer;
type acces_tabsub is access tabsub;


-- PROCEDURES ET FONCTIONS 
--------------------------
  
  procedure Free_tabinterp is
      new unchecked_deallocation(tabinterp, acces_tabinterp);
       
  procedure Free_tabsub is
      new unchecked_deallocation(tabsub, acces_tabsub);
      
  function convert_poi is new unchecked_conversion(integer, typipoi);
      

------------------------------------------------------------------------------------------------------------
-- procedure de lecture du fichier contenant la table des interpretations
-- et la table de substitution
------------------------------------------------------------------------------------------------------------
procedure Charge_tabsub(tsubst: out acces_tabsub;
  											tinterp: out acces_tabinterp;
                        nb_interp: out integer
                        );
                                      
-------------------------------------------------------------------------------
-- procÚdure capable de lire les pixels d'une image tiff et de renvoyer 
-- les poids de chacun en fonction d'une table dite de substitution
-------------------------------------------------------------------------------
procedure remplir_tpoi (file_image: in  out byte_io.file_type;
		                		xpixmin		: in integer;
		                		ypixmin		: in integer;
		                		xpixmax		: in integer;
		                		ypixmax		: in integer;
                        codetypo : in string30;
                        lps, ncol   : in integer;
                        stripoffset : in acces_tabstrip;
                        tinterp      : in acces_tabinterp;
                        tsubst      : in acces_tabsub;
                        nb_interp   : in integer;
		                		tabpoids	: in out typtipoi
                        );                       
                                                                                                                           
-------------------------------------------------------------------------------                                  
end SUBSTITUTION_IO;

