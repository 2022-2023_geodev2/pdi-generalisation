--******************************************************************************
-- Modifie le 21/06/1999 pour portage sous WindowsNT
--******************************************************************************
With Geometrie; use Geometrie;
With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
With math_int_basic; use math_int_basic;

Package body Lissage_filtrage is



procedure Repartition_reguliere(TABE : in point_liste_reel; Pas : in float;TABS : in out point_liste_reel) is
CE : integer:=1;
CS : integer:=2;
CECEP1 : float;
L : float;
premier : boolean;

begin
  TABS(1):=TABE(1);
  while CS<TABS'last loop
    l:=0.0;
	premier:=true;
    while L<Pas loop
	  if Premier=true then
	    L:=L+sqrt(distance_de_points(TABS(CS-1),TABE(CE+1)));
	    Premier:=false;
      else
		if CE=TABE'last-1 then
          L:=Pas;
          exit;
        end if;
		CE:=CE+1;
        L:=L+sqrt(distance_de_points(TABE(CE),TABE(CE+1)));
      end if;
    end loop;
	CECEP1:=sqrt(distance_de_points(TABE(CE),TABE(CE+1)));
    if CECEP1/=0.0 then
      TABS(CS).coor_x:=TABE(CE+1).coor_x-(L-Pas)*(TABE(CE+1).coor_x-TABE(CE).coor_x)/CECEP1;
      TABS(CS).coor_Y:=TABE(CE+1).coor_Y-(L-Pas)*(TABE(CE+1).coor_Y-TABE(CE).coor_y)/CECEP1;
    else
      TABS(CS).coor_x:=TABE(CE+1).coor_x;
      TABS(CS).coor_Y:=TABE(CE+1).coor_Y;
    end if;
    CS:=CS+1;
  end loop;
  TABS(TABS'last):=TABE(TABE'last);
end Repartition_reguliere;


procedure Repartition_reguliere(TABE : in point_liste_type; Pas : in float;TABS : in out point_liste_type) is
CE : integer:=1;
CS : integer:=2;
CECEP1 : float;
L : float;
premier : boolean;

begin
  TABS(1):=TABE(1);
  while CS<TABS'last loop
    l:=0.0;
	premier:=true;
    while L<Pas loop
	  if Premier=true then
	    L:=L+sqrt(distance_de_points(TABS(CS-1),TABE(CE+1)));
		premier:=false;
      else
		if CE=TABE'last-1 then
          L:=Pas;
          exit;
        end if;
		CE:=CE+1;
        L:=L+sqrt(distance_de_points(TABE(CE),TABE(CE+1)));
      end if;
    end loop;
	CECEP1:=sqrt(distance_de_points(TABE(CE),TABE(CE+1)));
	if CECEP1/=0.0 then
      TABS(CS).coor_x:=integer(float(TABE(CE+1).coor_x)-(L-Pas)*float(TABE(CE+1).coor_x-TABE(CE).coor_x)/CECEP1);
      TABS(CS).coor_Y:=integer(float(TABE(CE+1).coor_Y)-(L-Pas)*float(TABE(CE+1).coor_Y-TABE(CE).coor_Y)/CECEP1);
    else
      TABS(CS).coor_x:=integer(float(TABE(CE+1).coor_x));
      TABS(CS).coor_Y:=integer(float(TABE(CE+1).coor_Y));
    end if;
    CS:=CS+1;
  end loop;
  TABS(TABS'last):=TABE(TABE'last);
end Repartition_reguliere;



--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme du Filtre Gaussien en entier **--
--*************************************************************************-- 

Procedure FILTRE_GAUSSIEN_N ( ligne 	   : in point_liste_type;
                  -- debut modif 18.04.2005
                  -- Np   	   : in natural;
			      np   	   : in out natural;
                  -- fin modif 18.04.2005
			      sig   	   : in float;
                   	      ligne_lissee : out point_liste_type) is


 Type listemp_reel is array(integer range <>) of gen_io.Point_type_reel;

 x0,y0					: float;
-- Parametres du decoupage de l'arc en points equidistants
-- Modif SM 17/06/99
 pas					: float := 20.0;
 n_aux	: natural := Natural(Taille_ligne(Ligne,np)*3.0/pas);

 lgr_segments				: liens_array_type(1..n_aux);
-- Parametres du lissage gaussien ->
 mult_sigma				: float := 4.0;
 k1 					: integer := integer(mult_sigma*sig)-1;
 n_gs					: integer := max(k1,1);
 gs 					: reel_tableau_type(0..n_gs);
 c1,c2					: float;
 sigma					: float := sig;
 ligne_aux,ligne_lisse			: point_liste_reel(1..n_aux);
 ligne_bis				: listemp_reel(1-n_gs..n_aux+n_gs);
 ligne_lisse2				: point_liste_reel(1..np);
 l 					: natural;


begin

-- debut ajout 18.04.2005
if n_aux=0 then
  Np:=0;
  return;
end if;
-- fin ajout 18.04.2005


If (sig <= 0.0) then
  Ligne_lissee(1..np) := Ligne(1..np);
  Return;
End If;

----------------------------------------------------------------------------
-- Morcelement de la ligne en segments egaux de lgr PAS (soit la resolution)
----------------------------------------------------------------------------

  Decompose_ligne(ligne,np,ligne_aux,n_aux,pas,lgr_segments);

----------------------------------------------------------------
-- Reevaluation de SIGMA s'il k1 depasse le nombre de points !!!
----------------------------------------------------------------

  sigma:=sig;
  k1:= natural(mult_sigma * sigma)-1;
  if k1<1 then 
    k1 := 1;
  end if; 

  if k1 > n_aux-1 then
     	sigma:= float(n_aux) / mult_sigma;
	k1:= natural(mult_sigma * sigma)-1;
  end if;

----------------------------------------------------------------
-- Lissage de la ligne par convolution avec un filtre Gaussien :
-- (Calcul des poids (GAUSS) pour moyenner les pts et p.v)
----------------------------------------------------------------

--  c1:=1.0/(sigma*sqrt(2.0*PI));
  c2:=-1.0/(2.0*(sigma**2));

-- ************
-- Reevaluation de c1 pour eviter les erreurs d'arrondi
-- Pb : difference entre integrale de fn continue et integrale
--      de fn en escalier.
-- But : s'assurer que la somme des poids vaut 1
--       (ie (Somme Gs(k), k entre 0 et k1) = 1)
-- ************
  c1 := 1.0;
  for k in 1..k1 loop
    c1 := c1 + 2.0*exp(c2*float(k**2));
  end loop;
  c1 := 1.0 / c1;
  
-- Les poids, fn de c1 et c2
  for k in 0..k1 loop
     Gs(k):=c1*exp(c2*float(k**2));
  end loop;

  -- intialisation et prolongement du tableau de points aux 2 extremites
  for i in 1..n_aux loop
        ligne_bis(i).coor_x:= float(ligne_aux(i).coor_x);
        ligne_bis(i).coor_y:= float(ligne_aux(i).coor_y);
  end loop;
  for i in 1..k1 loop
    ligne_bis(1-i).Coor_x  := 2.0*ligne_aux(1).Coor_x  - ligne_aux(i+1).Coor_x;
    ligne_bis(1-i).Coor_y  := 2.0*ligne_aux(1).Coor_y  - ligne_aux(i+1).Coor_y;
    ligne_bis(n_aux+i).Coor_x := 
	2.0*ligne_aux(n_aux).Coor_x - ligne_aux(n_aux-i).Coor_x;
    ligne_bis(n_aux+i).Coor_y := 
	2.0*ligne_aux(n_aux).Coor_y - ligne_aux(n_aux-i).Coor_y;
  end loop;

  for i in 1..n_aux loop
     x0:=gs(0) * ligne_bis(i).coor_x; 
     y0:=gs(0) * ligne_bis(i).coor_y; 
     for k in 1..k1 loop
     	x0:= x0 + gs(k) * (ligne_bis(i-k).coor_x+ligne_bis(i+k).coor_x); 
     	y0:= y0 + gs(k) * (ligne_bis(i-k).coor_y+ligne_bis(i+k).coor_y); 
     end loop;
     ligne_lisse(i):=(x0,y0);
  end loop;

-------------------------------------------------------------------------
-- Extraction des points correspondant aux homologues des points initiaux
-------------------------------------------------------------------------

  l:= 1;
  Ligne_lisse2(1):= Ligne_lisse(1);
  for i in 2..np loop
    l:= l + lgr_segments(i-1);
    Ligne_lisse2(i):= Ligne_lisse(l);
  end loop;

-------------------------------------------
-- Mise au format dessin de la ligne lissee
-------------------------------------------

-- ***** Correction pour empecher le deplacement des extremites *****
  Ligne_lissee(1) := Ligne(1);  
  for i in 2..(np-1) loop
    Ligne_lissee(i).coor_x:= integer(Ligne_lisse2(i).coor_x);
    Ligne_lissee(i).coor_y:= integer(Ligne_lisse2(i).coor_y);
  end loop;
  Ligne_lissee(np) := Ligne(np);  

end FILTRE_GAUSSIEN_N;


--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme du Filtre Gaussien en reel   **--
--*************************************************************************-- 

Procedure FILTRE_GAUSSIEN_R ( ligne_aux   : in point_liste_reel;
			      n_aux   	  : in natural;
			      sig   	  : in float;
                   	      ligne_lisse : out point_liste_reel) is


 Type listemp_reel is array(integer range <>) of gen_io.Point_type_reel;

 x0,y0		: float;
-- Parametres du decoupage de l'arc en points equidistants
-- Modif SM 17/06/99
 pas					: float := 1.0;
 n_dec	: natural := natural(Taille_ligne(Ligne_aux,n_aux)*3.0/pas);

 ligne_dec				: Point_access_reel;
 lgr_segments				: liens_array_type(1..n_dec);
-- Parametres du lissage gaussien ->
 mult_sigma	: float := 4.0;
 k1 		: integer:= integer(mult_sigma*sig)-1;
 n_gs		: integer := max(k1,1);
 gs 		: reel_tableau_type(0..n_gs);
 c1,c2		: float;
 sigma		: float := sig;
 ligne_bis	: listemp_reel(1-k1..n_dec+k1);  
 ligne_lisse2	: Point_liste_reel(1..n_dec); 
 l		: integer;


begin

If (sig <= 0.0) then
  Ligne_lisse(1..n_aux) := Ligne_aux(1..n_aux);
  Return;
End If;

----------------------------------------------------------------------------
-- Morcelement de la ligne en segments egaux de lgr PAS (soit la resolution)
----------------------------------------------------------------------------

  Decomposition_ligne(ligne_aux(1..n_aux),pas,
			Ligne_dec,lgr_segments);
  n_dec := Ligne_dec'length;

----------------------------------------------------------------
-- Reevaluation de SIGMA s'il k1 depasse le nombre de points !!!
----------------------------------------------------------------

  sigma:=sig;
  k1:= natural(mult_sigma * sigma)-1;
  if k1<1 then
    k1:=1;
  end if;

  if k1 > n_dec-1 then
     	sigma:= float(n_dec) / mult_sigma;
	k1:= natural(mult_sigma * sigma)-1;
  end if;

----------------------------------------------------------------
-- Lissage de la ligne par convolution avec un filtre Gaussien :
-- (Calcul des poids (GAUSS) pour moyenner les pts et p.v)
----------------------------------------------------------------

--  c1:=1.0/(sigma*sqrt(2.0*PI));
  c2:=-1.0/(2.0*(sigma**2));

-- ************
-- Reevaluation de c1 pour eviter les erreurs d'arrondi
-- Pb : difference entre integrale de fn continue et integrale
--      de fn en escalier.
-- But : s'assurer que la somme des poids vaut 1
--       (ie (Somme Gs(k), k entre 0 et k1) = 1)
-- ************
  c1 := 1.0;
  for k in 1..k1 loop
    c1 := c1 + 2.0*exp(c2*float(k**2));
  end loop;
  c1 := 1.0 / c1;
  
-- Les poids, fn de c1 et c2
  for k in 0..k1 loop
     Gs(k):=c1*exp(c2*float(k**2));
  end loop;

  -- intialisation et prolongement du tableau de points aux 2 extremites
  for i in 1..n_dec loop
        ligne_bis(i).coor_x:= ligne_dec(i).coor_x;
        ligne_bis(i).coor_y:= ligne_dec(i).coor_y;
  end loop;
  for i in 1..k1 loop
    ligne_bis(1-i).Coor_x  := 2.0*ligne_dec(1).Coor_x - ligne_dec(i+1).Coor_x;
    ligne_bis(1-i).Coor_y  := 2.0*ligne_dec(1).Coor_y - ligne_dec(i+1).Coor_y;
    ligne_bis(n_dec+i).Coor_x := 
	2.0*ligne_dec(n_dec).Coor_x - ligne_dec(n_dec-i).Coor_x;
    ligne_bis(n_dec+i).Coor_y := 
	2.0*ligne_dec(n_dec).Coor_y - ligne_dec(n_dec-i).Coor_y;
  end loop;

  for j in 1..n_dec loop
     x0:=gs(0) * ligne_bis(j).coor_x; 
     y0:=gs(0) * ligne_bis(j).coor_y; 
     for k in 1..k1 loop
     	x0:= x0 + gs(k) * (ligne_bis(j-k).coor_x+ligne_bis(j+k).coor_x); 
     	y0:= y0 + gs(k) * (ligne_bis(j-k).coor_y+ligne_bis(j+k).coor_y); 
     end loop;
     ligne_lisse2(j):=(x0,y0);
  end loop;

-------------------------------------------------------------------------
-- Extraction des points correspondant aux homologues des points initiaux
-------------------------------------------------------------------------

  l:= 1;
  Ligne_lisse(1):= Ligne_lisse2(1);
  for i in 2..n_aux loop
    l:= l + lgr_segments(i-1);
    Ligne_lisse(i):= Ligne_lisse2(l);
  end loop;


  GR_Free_point_liste_reel(Ligne_dec);
end FILTRE_GAUSSIEN_R;

--*************************************************************************-- 
--**   Filtre gaussien reel "general", i.e. non dedie au lissage de	 **--
--** lignes sous Plage : on peut choisir de decomposer la ligne		 **--
--** (decomposer = true) ou non, auquel cas la ligne doit deja etre	 **--
--** decomposee a pas constant. Dans les 2 cas on passe a la procedure	 **--
--** le pas de decomposition (valeur par defaut 1.0) : pour decomposition**--
--** si decomposer=true, a titre d'information pour le calcul des poids	 **--
--** sinon								 **-- 
--*************************************************************************-- 

Procedure FILTRE_GAUSSIEN_R ( ligne_aux   : in point_liste_reel;
			      n_aux   	  : in natural;
			      sig   	  : in float;
                   	      ligne_lisse : out point_liste_reel;
			      decomposer  : boolean := true;
			      pas	  : float := 1.0) is


 Type listemp_reel is array(integer range <>) of gen_io.Point_type_reel;

 x0,y0		: float;
-- Parametres du decoupage de l'arc en points equidistants ->
 ligne_dec				: Point_access_reel;
 n_dec					: integer := 20000; -- arbitraire
 lgr_segments				: liens_array_type(1..n_dec);
-- Parametres du lissage gaussien ->
 mult_sigma	: float := 4.0;
 k1 		: integer:= integer((mult_sigma*sig-1.0)/pas);
 n_gs		: integer := max(k1,1);
 gs 		: reel_tableau_type(0..n_gs);
 c1,c2		: float;
 sigma		: float := sig;
 ligne_bis	: listemp_reel(1-k1..n_dec+k1);  
 ligne_lisse2	: Point_liste_reel(1..n_dec); -- 20000 arbitraire
 l		: integer;


begin

If (sig <= 0.0) then
  Ligne_lisse(1..n_aux) := Ligne_aux(1..n_aux);
  Return;
End If;

----------------------------------------------------------------------------
-- S'il y a lieu, morcellement de la ligne en segments egaux de lgr PAS
----------------------------------------------------------------------------

  If (decomposer) then
    Decomposition_ligne(ligne_aux(1..n_aux),pas,
			Ligne_dec,lgr_segments);
  Else
    Ligne_dec := New Point_liste_reel'(Ligne_aux(1..n_aux));
  End If;
  n_dec := Ligne_dec'length;

----------------------------------------------------------------
-- Reevaluation de SIGMA s'il k1 depasse le nombre de points !!!
----------------------------------------------------------------

  sigma:=sig;
  k1:= natural((mult_sigma*sigma-1.0)/pas);
  if k1<1 then
    k1:=1;
  end if;

  if k1 > n_dec-1 then
     	sigma:= (float(n_dec-1)*pas+1.0) / mult_sigma;
	k1:= natural((mult_sigma*sigma-1.0)/pas);
  end if;

----------------------------------------------------------------
-- Lissage de la ligne par convolution avec un filtre Gaussien :
-- (Calcul des poids (GAUSS) pour moyenner les pts et p.v)
----------------------------------------------------------------

  c2:=-1.0/(2.0*(sigma**2));

-- ************
-- Calcul de c1 de facon a eviter les erreurs d'arrondi
-- Pb : difference entre integrale de fn continue et integrale
--      de fn en escalier.
-- But : s'assurer que la somme des poids vaut 1
--       (ie (Somme Gs(k), k entre 0 et k1) = 1)
-- ************
  c1 := 1.0;
  for k in 1..k1 loop
    c1 := c1 + 2.0*exp(c2*float(k**2)*(pas*pas));
  end loop;
  c1 := 1.0 / c1;
  
-- Les poids, fn de c1 et c2
  for k in 0..k1 loop
     Gs(k):=c1*exp(c2*float(k**2)*(pas*pas));
  end loop;

  -- intialisation et prolongement du tableau de points aux 2 extremites
  for i in 1..n_dec loop
        ligne_bis(i).coor_x:= ligne_dec(i).coor_x;
        ligne_bis(i).coor_y:= ligne_dec(i).coor_y;
  end loop;
  for i in 1..k1 loop
    ligne_bis(1-i).Coor_x  := 2.0*ligne_dec(1).Coor_x - ligne_dec(i+1).Coor_x;
    ligne_bis(1-i).Coor_y  := 2.0*ligne_dec(1).Coor_y - ligne_dec(i+1).Coor_y;
    ligne_bis(n_dec+i).Coor_x := 
	2.0*ligne_dec(n_dec).Coor_x - ligne_dec(n_dec-i).Coor_x;
    ligne_bis(n_dec+i).Coor_y := 
	2.0*ligne_dec(n_dec).Coor_y - ligne_dec(n_dec-i).Coor_y;
  end loop;

  for j in 1..n_dec loop
     x0:=gs(0) * ligne_bis(j).coor_x; 
     y0:=gs(0) * ligne_bis(j).coor_y; 
     for k in 1..k1 loop
     	x0:= x0 + gs(k) * (ligne_bis(j-k).coor_x+ligne_bis(j+k).coor_x); 
     	y0:= y0 + gs(k) * (ligne_bis(j-k).coor_y+ligne_bis(j+k).coor_y); 
     end loop;
     ligne_lisse2(j):=(x0,y0);
  end loop;

-------------------------------------------------------------------------
-- Si la ligne a ete decomposee en debut de procedure :
-- Extraction des points correspondant aux homologues des points initiaux
-------------------------------------------------------------------------

  If (decomposer) then
    l:= 1;
    Ligne_lisse(1):= Ligne_lisse2(1);
    for i in 2..n_aux loop
      l:= l + lgr_segments(i-1);
      Ligne_lisse(i):= Ligne_lisse2(l);
    end loop;
  Else
    Ligne_lisse(1..n_aux) := Ligne_lisse2(1..n_aux);
  End If;

    GR_Free_point_liste_reel(Ligne_dec);

end FILTRE_GAUSSIEN_R;


--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme du Filtre Gaussien "matheux"   --
--*************************************************************************-- 
-- But = lisser une fonction mathematique, i.e. les ordonnees des points
--       d'une ligne.
-- En entree : -la ligne (coordonnees reelles), deja segmentee a pas constant
-- 		en abscisse
-- 	       -N = le nb de points a prendre en compte pour le lissage de 
--		    part et d'autre de chaque point
--	            (on en deduit sigma tq le dernier pt pris en compte ait
--		     un poids corresp. a 4 sigma, i.e. considere comme
--		     negligeable)

Procedure FILTRE_GAUSSIEN_MATH ( ligne_aux   : in point_liste_reel;
			      n_aux   	  : in natural;
			      N   	  : in natural;
                   	      ligne_lisse : out point_liste_reel) is


 Type listemp_reel is array(integer range <>) of gen_io.Point_type_reel;

 x0,y0		: float;
-- Parametres du lissage gaussien ->
 gs 		: reel_tableau_type(0..N);
 c1,c2		: float;
 sigma		: float;
 ligne_bis	: listemp_reel(1-N..n_aux+N);  
 l		: integer;


begin

If (N = 0) then
  Ligne_lisse(1..n_aux) := Ligne_aux(1..n_aux);
  Return;
End If;

----------------------------------------------------------------
-- Determination de sigma en fonction du nombre de points N
----------------------------------------------------------------
-- En fait sigma = N*pas/4 ou pas=pas de decomposition (en x)
-- de la ligne
-- Mais ds le calcul des poids ca se simplifie...
-- Formule de Gauss Gs(x=k*pas) = c1*exp(c2*(k))
-- avec c2(k) = -(k*pas)^2/(2*(N*pas/4)^2) = -8*k^2/N^2
-- 
----------------------------------------------------------------
-- Lissage de la ligne par convolution avec un filtre Gaussien :
-- (Calcul des poids (GAUSS) pour moyenner les pts et p.v)
----------------------------------------------------------------

  c2:=-8.0/(float(N)**2);

-- ************
-- Reevaluation de c1 pour eviter les erreurs d'arrondi
-- Pb : difference entre integrale de fn continue et integrale
--      de fn en escalier.
-- But : s'assurer que la somme des poids vaut 1
--       (ie (Somme Gs(k), k entre -N et N) = 1)
-- ************
  c1 := 1.0;
  for k in 1..N loop
    c1 := c1 + 2.0*exp(c2*float(k**2));
  end loop;
  c1 := 1.0 / c1;
  
-- Les poids, fn de c1 et c2
  for k in 0..N loop
     Gs(k):=c1*exp(c2*float(k**2));
  end loop;

  -- intialisation et prolongement du tableau de points aux 2 extremites
  for i in 1..n_aux loop
        ligne_bis(i).coor_x:= ligne_aux(i).coor_x;
        ligne_bis(i).coor_y:= ligne_aux(i).coor_y;
  end loop;
  for i in 1..N loop
    ligne_bis(1-i).Coor_x  := 2.0*ligne_aux(1).Coor_x - ligne_aux(i+1).Coor_x;
    ligne_bis(1-i).Coor_y  := 2.0*ligne_aux(1).Coor_y - ligne_aux(i+1).Coor_y;
    ligne_bis(n_aux+i).Coor_x := 
	2.0*ligne_aux(n_aux).Coor_x - ligne_aux(n_aux-i).Coor_x;
    ligne_bis(n_aux+i).Coor_y := 
	2.0*ligne_aux(n_aux).Coor_y - ligne_aux(n_aux-i).Coor_y;
  end loop;

  for j in 1..n_aux loop
     y0:=gs(0) * ligne_bis(j).coor_y; 
     for k in 1..N loop
     	y0:= y0 + gs(k) * (ligne_bis(j-k).coor_y+ligne_bis(j+k).coor_y); 
     end loop;
     ligne_lisse(j):=(ligne_aux(j).coor_x,y0);
  end loop;

end FILTRE_GAUSSIEN_MATH;


--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de Douglas & Peucker         **--
--*************************************************************************-- 

  -- fonction permettant de savoir si deux points sont egaux 
  --   retourne vrai si les deux points sont identiques, faux sinon
  function egalite_2_points(pt1,pt2 : point_type) return boolean is
    begin
      if (( pt1.Coor_x = pt2.Coor_x) and (pt1.Coor_y = pt2.Coor_y)) then
        return true;
      else
        return false;
      end if;
  end egalite_2_points;


--*************************************************************************-- 
--** Fonction recherche_dist_entre_pt_segment :                          **--
--**   Elle recherche parmi les points du tableau tab_points_init compo- **--
--** sant un arc dont les extremites sont Point_Ancre et Point_Flottant  **--
--** le point le plus distant de sa projection orthogonale sur le segment**--
--** [Point_Ancre,Point_Flottant]. Une fois ce point trouve, elle le re- **--
--** tourne avec la distance associee. L'arc doit contenir au moins un   **--
--** point sans compter les extremites.                                  **--
--*************************************************************************--
                                                                            
  function recherche_dist_entre_pt_segment
                            ( tab_points_init : point_access_type;
                              Point_Ancre,Point_Flottant : positive)
                             return couple_dist_pt is
    dist_i : float;
    couple : couple_dist_pt;
    
    begin
      couple.distance:=0.0;
      if ((Point_Flottant-Point_Ancre)>1) then
        for i in (Point_Ancre+1)..(Point_Flottant-1) loop
           dist_i:=Distance_a_ligne(tab_points_init.all(i),
                                    tab_points_init.all(Point_Ancre),
                                    tab_points_init.all(Point_Flottant),
                                    C_ligne_type);
           if (dist_i>=couple.distance) then
             couple.distance := dist_i;
             couple.point:=i;
           end if;
        end loop;
      end if;
      return couple;
  end recherche_dist_entre_pt_segment;


--*************************************************************************--
--** Fonction recherche_dist_entre_2_points :                            **--
--**   Elle recherche parmi les points du tableau tab_points composant un**--
--** arc delimite par les points point_ref et point_final le point le    **--
--** plus distant du point point_ref. Une fois, ce point trouve, elle le **--
--** retourne avec la distance associee.                                 **--
--*************************************************************************--

  function recherche_dist_entre_2_points
                ( point_ref, point_final : positive;
                  tab_points : point_access_type) return couple_dist_pt is
  
    dist_i : float;
    couple : couple_dist_pt;

    begin
      couple.distance:=0.0;
      for i in (point_ref+1)..point_final loop
        dist_i:=Norme_v2(tab_points.all(point_ref),tab_points.all(i));
        if (dist_i>couple.distance) then
          couple.distance:=dist_i;
          couple.point:=i;
        end if;
      end loop;
      return couple;
  end recherche_dist_entre_2_points;


--*************************************************************************--
--** Procedure AlgorithmeDP :                                            **--
--**   Procedure recursive mettant en application sur un arc ou une por- **--
--** tion d'arc les principes decrits par Douglas et Peucker.            **--
--**   Critere d'arret : Point_Ancre = Point_Flottant : l'arc a ete to-  **--
--** talement parcouru.                                                  **--
--**   Les Points Flottants sont au fur et a mesure "empiler" dans le ta-**--
--** bleau Pile dont le "sommet" est repere par Sommet_pile              **--
--*************************************************************************--  
           
  procedure AlgorithmeDP
                ( Point_Ancre, Point_Flottant : positive;
                  tab_points_init : point_access_type;
                  tab_points_final : point_access_type;
                  nb_points_final :in out natural;
                  seuil : float;
                  resolution : positive;
                  Pile : in out pile_type;
                  Sommet_pile : in out natural ) is
  
    PA,PF  : positive;
    couple : couple_dist_pt;

    begin
      PA:=Point_Ancre;
      PF:=Point_Flottant;
      if (PA/=PF) then
        couple:=recherche_dist_entre_pt_segment(tab_points_init,PA,PF);
        if ((couple.distance>=(seuil*float(resolution))) and ((PF-PA)>1)) then
        -- le point doit etre retenu :                      --
        --  il represente un trait caracteristique de l'arc --
        -- la condition (PF-PA)>1 represente les cas ou il existe au moins un
        -- point entre le Point Ancre et le Point Flottant et elle sert si le
        -- seuil est nul :
          Sommet_pile:=Sommet_pile+1;
          Pile(Sommet_pile):=PF;
          PF:=couple.point;
        else
        -- aucun point interessant n'a ete detecte entre le point ancre et le --
        -- point flottant : le point flottant devient le nouveau point ancre  --
        -- et le dernier point flottant empile,s'il en a encore un, devient le--
        -- nouveau point flottant,sinon ce dernier ne change pas              --
          PA:=PF;
          nb_points_final:=nb_points_final+1;
          tab_points_final.all(nb_points_final):=tab_points_init.all(PA);
          if (Sommet_pile>0) then
            PF:=Pile(Sommet_pile);
            Sommet_pile:=Sommet_pile-1;
          end if;
        end if;
        AlgorithmeDP(PA,PF,tab_points_init,tab_points_final,nb_points_final,
                     seuil,resolution,Pile,Sommet_pile);
      end if;
  end AlgorithmeDP;


--*************************************************************************--  
--** Procedure Douglas_Peucker :                                         **--
--**   Elle permet d'appeler la procedure AlgorithmeDP en fonction des   **--
--** donnees initiales, sur un arc                                       **--
--**  tab_points_init : liste des points constituant l'arc a generaliser **--
--**  nb_pts_init : nombre de points constituant cet arc                 **--
--**  tab_points_final : liste des points constituant l'arc generalise   **--
--**  nb_pts_final : nombre de points constituant cet arc generalise     **--
--**  seuil : critere de selection des points (en mm)                    **--
--**  resolution : resolution utilisee                                   **--
--*************************************************************************--  


  procedure Douglas_Peucker( tab_points_init : point_access_type;
                             nb_pts_init : natural;
                             tab_points_final : point_access_type;
                             nb_pts_final : out natural;
                             seuil : float;
                             resolution : positive) is

  Pile : pile_type(1..nb_pts_init);
  Sommet_pile : natural := 0;
  couple : couple_dist_pt;
  nb_pts_final_prov : natural;

  begin
    -- le premier point de l'arc est toujours conserve --
    nb_pts_final_prov:=1;
    tab_points_final.all(nb_pts_final_prov):=tab_points_init.all(nb_pts_final_prov);
    if nb_pts_init > 1 then
      if egalite_2_points(tab_points_init.all(1),
                          tab_points_init.all(nb_pts_init)) then
      -- l'arc a deux extremites egales --
         -- s'il ne contient que ses deux points, le premier constitue --
         -- l'arc generalise sinon l'algorithme de Douglas et Peucker lui  --
         -- est applique --
        if nb_pts_init>2 then
          couple:=recherche_dist_entre_2_points(1,nb_pts_init-1,
                                                tab_points_init);
          if (couple.distance>=(seuil*float(resolution))) then
            -- un point a ete selectionne ; l'arc est scinde en deux pour --
            -- revenir au cas ou un arc a deux extremites distinctes  --
            AlgorithmeDP(1,couple.point,tab_points_init,tab_points_final,
                         nb_pts_final_prov,seuil,resolution,Pile,Sommet_pile);
            AlgorithmeDP(couple.point,nb_pts_init,tab_points_init,
                         tab_points_final,nb_pts_final_prov,seuil,resolution,Pile,
                         Sommet_pile);
          end if;
        end if;
      else
      -- l'arc a deux extremites distinctes -- 
        if nb_pts_init > 2 then
          AlgorithmeDP(1,nb_pts_init,tab_points_init,tab_points_final,
                       nb_pts_final_prov,seuil,resolution,Pile,Sommet_pile);
        else 
        -- l'arc est constitue uniquement de ses deux extremites --
        -- la generalisation ne change rien --
          nb_pts_final_prov:=2;
          tab_points_final.all(nb_pts_final_prov):=tab_points_init.all(nb_pts_final_prov);
        end if;
      end if;
    end if;    
    nb_pts_final:=nb_pts_final_prov;
  end Douglas_Peucker;


--************************************************************************--
--**   Nouvelles versions de Douglas proposees par le COGIT (Xavier)    **--
--************************************************************************--

--------------------------------------------------------------------------
-- Identique a DP si ce n'est qu'on conserve en plus les points precedents 
-- et suivants des points retenus par DP
--------------------------------------------------------------------------
Procedure AlgorithmeDPPlus( Point_Ancre, Point_Flottant	: positive;
  			    tab_points_init		: point_access_type;
			    tab_points_final		: point_access_type;
			    nb_points_final		: in out natural;
			    seuil			: float;
			    resolution			: positive;
			    Pile			: in out pile_type;
			    Sommet_pile			: in out natural ) is
PA,PF : positive;
couple : couple_dist_pt;

Begin
  PA:=Point_Ancre;
  PF:=Point_Flottant;
  if (PA/=PF) then
    couple:=recherche_dist_entre_pt_segment(tab_points_init,PA,PF);
    if ((couple.distance>=(seuil*float(resolution))) and ((PF-PA)>1)) then
	-- le point doit etre retenu :
	-- il represente un trait caracteristique de l'arc
	-- la condition (PF-PA)>1 represente les cas ou il existe au moins un
	-- point entre le Point Ancre et le Point Flottant et elle sert si le
	-- seuil est nul :
      Sommet_pile:=Sommet_pile+1;
      Pile(Sommet_pile):=PF;
      PF:=couple.point;
    else
	-- aucun point interessant n'a ete detecte entre le point ancre et le
 	-- point flottant : le point flottant devient le nouveau point ancre
	-- et le dernier point flottant empile,s'il en a encore un, devient le
	-- nouveau point flottant,sinon ce dernier ne change pas
      PA:=PF;
      if tab_points_init.all(PA-1)/=tab_points_final.all(nb_points_final) then
	nb_points_final:=nb_points_final+1;
	tab_points_final.all(nb_points_final):=tab_points_init.all(PA-1);
      end if;
      nb_points_final:=nb_points_final+1;
      tab_points_final.all(nb_points_final):=tab_points_init.all(PA);
      if sommet_pile>0 then 
	if Pile(Sommet_pile)-PA>1 then
	  nb_points_final:=nb_points_final+1;
	  tab_points_final.all(nb_points_final):=tab_points_init.all(PA+1);
	end if;
      end if;
      if (Sommet_pile>0) then
	PF:=Pile(Sommet_pile);
	Sommet_pile:=Sommet_pile-1;
      end if;
    end if;
    AlgorithmeDPPlus(PA,PF,tab_points_init,tab_points_final,nb_points_final,
		    seuil,resolution,Pile,Sommet_pile);
  end if;
End AlgorithmeDPPlus;

---------------------------------------------------------------------
-- Procedure Douglas_Peucker_Plus :
---------------------------------------------------------------------
Procedure Douglas_Peucker_Plus( tab_points_init		: point_access_type;
			        nb_pts_init		: natural;
			        tab_points_final 	: point_access_type;
			        nb_pts_final		: out natural;
			        seuil			: float;
   			        resolution		: positive) is
Pile : pile_type(1..nb_pts_init);
Sommet_pile : natural := 0;
couple : couple_dist_pt;
nb_pts_final_prov : natural;

Begin
	-- le premier point de l'arc est toujours conserve
  if seuil=0.0 then
       Douglas_Peucker(tab_points_init,nb_pts_init,tab_points_final,
             nb_pts_final_prov,seuil,resolution);          
       nb_pts_final:=nb_pts_final_prov;       
       return;
  end if;
  nb_pts_final_prov:=1;
  tab_points_final.all(nb_pts_final_prov):=
	tab_points_init.all(nb_pts_final_prov);
  if nb_pts_init > 1 then
    if egalite_2_points(tab_points_init.all(1),
                        tab_points_init.all(nb_pts_init)) then
	-- l'arc a deux extremites egales s'il ne contient que ses deux
	-- points, le premier constitue l'arc generalise sinon l'algorithme
	-- de Douglas et Peucker lui est applique
      if nb_pts_init>2 then
	couple:=recherche_dist_entre_2_points(1,nb_pts_init-1,tab_points_init);
	if (couple.distance>=(seuil*float(resolution))) then
	-- un point a ete selectionne ; l'arc est scinde en deux pour
	-- revenir au cas ou un arc a deux extremites distinctes
	  AlgorithmeDPPlus( 1,couple.point,tab_points_init,tab_points_final,
			nb_pts_final_prov,seuil,resolution,Pile,Sommet_pile);
	  AlgorithmeDPPlus( couple.point,nb_pts_init,tab_points_init,
			tab_points_final,nb_pts_final_prov,seuil,resolution,
			Pile,Sommet_pile);
	end if;
      end if;
    else
	-- l'arc a deux extremites distinctes
      if nb_pts_init > 2 then
        AlgorithmeDPPlus(1,nb_pts_init,tab_points_init,tab_points_final,
		     nb_pts_final_prov,seuil,resolution,Pile,Sommet_pile);
      else
	-- l'arc est constitue uniquement de ses deux extremites
	-- la generalisation ne change rien
	nb_pts_final_prov:=2;
	tab_points_final.all(nb_pts_final_prov):=
		tab_points_init.all(nb_pts_final_prov);
      end if;
    end if;
  end if;    
  nb_pts_final:=nb_pts_final_prov;
End Douglas_Peucker_Plus;

-----------------------------------------------------------------------
-- 2eme version amelioree : les points precedents et suivants de ceux 
-- retenus par DP sont choisis de facon plus subtile que dans la version 
-- precedente :
--
-----------------------------------------------------------------------

--------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. reelles) (de M1 a M2)
-- resultat dans ]-3.14,+3,14]
--------------------------------------------------------------------------
Function Angpente (X1,Y1,X2,Y2  : in float) return float is
 pente   : float;
Begin
  if X2=X1 then
    if Y2>Y1 then pente:=PI/2.0; else pente:=-PI/2.0; end if;
  else
    pente:=arctan((Y2-Y1)/(X2-X1));
    if (Y2<Y1) and (pente>0.0) then pente:=pente-PI;
    elsif (Y2>Y1) and (pente<0.0) then pente:=pente+PI;
    elsif (Y2=Y1) and (X2<X1) then pente:=PI;
    end if;
  end if;
  return pente;
End Angpente;

------------------------------------------------------------------------------
-- Verification et correction par insertion ou suppression de points (entiers)
------------------------------------------------------------------------------
Procedure Verif(ligne           : in out point_liste_type;
                rapall          : in float;
                ectm            : in float;
                n_points        : in out natural) is
 ligne_cor,ligne_flo            : point_liste_reel(1..10*(n_points+2));
 a,alf,d1,d3,av,ap,p,p1,p2      : float;
 i,k,top,tip,cpt                : integer;
Begin
 for i in 1..n_points loop
  ligne_flo(i).coor_X:=float(ligne(i).coor_X);
  ligne_flo(i).coor_Y:=float(ligne(i).coor_Y);
 end loop;
 tip:=1; 
 cpt:=0;
 while tip=1 loop
  cpt:=cpt+1; 
  top:=0; 
  k:=1; ligne_cor(k).coor_X:=ligne_flo(1).coor_X;
  ligne_cor(k).coor_Y:=ligne_flo(1).coor_Y; 
  tip:=0;
  for i in 2..n_points-1 loop
    av:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,
                    ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
    ap:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,
                    ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y);
    a:=abs(av-ap);
    d1:=sqrt((ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)*
             (ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)+
             (ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y)*
             (ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y));
    d3:=sqrt((ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)*
             (ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)+
             (ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y)*
             (ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y));
    if abs(a-PI)>1.0E-02 then
      if ((a<=PI/1.99) or (a>=1.49*PI)) and
         (top=0) and (d1>2.0) and (d3>2.0) then
        tip:=1; top:=1;
        p:=Angpente(ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y,
                    ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
        p1:=p;
        if abs(p1-av)>PI then
          if av<0.0 then 
             p1:=p1-2.0*PI; 
          else 
             p1:=p1+2.0*PI; 
          end if;
        end if;
        alf:=av+(p1-av)*ectm; 
        k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d1*cos(alf)/rapall;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d1*sin(alf)/rapall; 
        k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
        if p>0.0 then 
           p2:=p-PI; 
        else  
           p2:=p+PI;
        end if;
        if abs(p2-ap)>PI then
          if ap<0.0 then 
             p2:=p2-2.0*PI; 
          else 
             p2:=p2+2.0*PI; 
          end if;
        end if;
        alf:=ap+(p2-ap)*ectm; 
        k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d3*cos(alf)/rapall;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d3*sin(alf)/rapall;
      else
        top:=0; k:=k+1; ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
      end if;
    else
--      new_line(1); put("   -> Elimination du point "); put(i,4);
--      put(" (aligne avec ses 2 voisins).");
        null;
    end if;
  end loop;
  k:=k+1; 
  ligne_cor(k).coor_X:=ligne_flo(n_points).coor_X;
  ligne_cor(k).coor_Y:=ligne_flo(n_points).coor_Y; 
  n_points:=k;
  ligne_flo:=ligne_cor;
 end loop;
 for i in 1..n_points loop
  ligne(i).coor_X:=integer(ligne_cor(i).coor_X);
  ligne(i).coor_Y:=integer(ligne_cor(i).coor_Y);
 end loop;
End Verif;

-----------------------------------------------------------------------------

Procedure Douglas_Peucker_2Plus( tab_points_init	: point_access_type;
			         nb_pts_init		: natural;
			         tab_points_final 	: point_access_type;
			         nb_pts_final		: out natural;
			         seuil			: float;
                                 rapall 		: float;
                                 ectm			: float;
   			         resolution		: positive) is

 Ligne_finale	: point_access_type;
 ligne_gen	: point_liste_type(1..nb_pts_init);
 nb_pts_gen	: natural;

Begin
  ligne_finale :=new point_liste_type(1..nb_pts_init);
  Douglas_Peucker(tab_points_init,nb_pts_init,ligne_finale,nb_pts_gen,
                  seuil,resolution);
  ligne_gen:=ligne_finale.all; 
--  put("Donnez rapall (dist du pt cree au pt anguleux) [2.0-10.0] -> ");
--  put("Donnez ectm (angle entre pt cree et pt anguleux) [0.1-1.0] -> ");
  if seuil/=0.0 then
     Verif(ligne_gen,rapall,ectm,nb_pts_gen);
  end if;
  nb_pts_final:=nb_pts_gen;  
  tab_points_final(1..nb_pts_gen):=ligne_gen(1..nb_pts_gen);

End Douglas_Peucker_2Plus;



--*************************************************************************-- 
--**     Corps du paquetage pour l'algorithme de VAN_HORN                **--
--*************************************************************************-- 


  -- fonction permettant de savoir si le point Point appartient au rectangle 
  -- determine par les deux points (xmin,ymin) et (xmax,ymax), les bords   
  -- du rectangle y compris. Vrai si cela est le cas, faux sinon         
  function Point_Dans_Rectangle(Point:Point_type;
                                xmin,xmax,ymin,ymax:integer) 
                                return boolean is
    begin
      if ((Point.coor_x in xmin..(xmax)) and 
          (Point.coor_y in ymin..(ymax))) then
        return true;
      else 
        return false;
      end if;
  end Point_Dans_Rectangle;


  -- fonction permettant de savoir si le point pt se trouve sur les coordonnees
  -- (x,y). faux si cela est le cas, vrai sinon  
  function Point_Prec_Diff(pt:point_type;x,y:integer) return boolean is
    begin
      if ((pt.Coor_x = x) and (pt.Coor_y = y)) then
        return false;
      else
        return true;
      end if;
  end Point_Prec_Diff;


--*************************************************************************-- 
--** Procedure Van_horn :                                                **-
--** Elle permet d'appliquer a un arc la methode de filtrage de Van Horn **-
--**  tab_points_init : liste des points constituant l'arc a generaliser **--
--**  nb_pts_init : nombre de points constituant cet arc                 **--
--**  (Xrep,Yrep) : origine du repere servant de base pour le treillis   **--
--**  pas : longueur, donnee en pixel, du cote des carres                **--
--**        constituant le treillis (strictement positive)               **--
--**  tab_points_final : liste des points constituant l'arc generalise   **--
--**  nb_pts_final : nombre de points constituant cet arc generalise     **--
--*************************************************************************--  

  procedure Van_Horn( tab_points_init : point_access_type;
                      nb_pts_init : natural;
                      Xrep,Yrep : integer;
                      pas : integer;
                      tab_points_final : point_access_type;
                      nb_pts_final : out natural) is
 
    -- le point (xm,ym) represente le coin gauche et bas de la maille courante
    -- par consequent, la maille courante est le rectangle delimite par les
    -- deux points extremes (xm,ym) et (xm+pas,ym+pas)
    xm : integer:=Xrep; 
    ym : integer:=Yrep; 
    nb_pts_final_prov : natural;

    --******************************************************************--
    --** Procedure Test_Ordonnee :                                    **--
    --**   Elle permet de savoir si le point courant doit etre place  **--
    --** dans le coin haut ou le coin bas de la maille tout en con-   **--
    --** naissant sa position horiziontale (coin gauche ou coin droit **--
    --** de la maille). Le point n'est conserve que si son predeces-  **--
    --** seur n'a pas ete deplace dans le meme coin.                  **--
    --******************************************************************--
 
    procedure Test_Ordonnee(x,pt:integer) is


      --***************************************************************--
      --** Procedure Affectation :                                   **--
      --**   Elle permet d'ajouter un nouveau point a l'arc en cours **--
      --** de generalisation si ce dernier est different du point    **-
      --** precedemment enregistre ou si aucun point n'a encore ete  **--
      --** sauvegarde                                                **--
      --***************************************************************--

      procedure Affectation (y:integer) is
        begin
          if ((nb_pts_final_prov=0) or else  
               Point_Prec_Diff(tab_points_final(nb_pts_final_prov),x,y)) then
            nb_pts_final_prov:=nb_pts_final_prov+1;
            tab_points_final.all(nb_pts_final_prov).Coor_x:=x;
            tab_points_final.all(nb_pts_final_prov).Coor_y:=y;
          end if;
      end Affectation;

                  -- Corps de la procedure Test_Ordonnee --
      begin
        -- Les tests suivants concernent le deplacement du point courant pt
        -- dans le plan vertical et sont similaires a ceux decrits dans le 
        -- corps de la procedure Van_Horn. 
        if ((pt/=nb_pts_init) and then
            (float(tab_points_init.all(pt).Coor_y)<(float(ym)+float(pas)/2.0)))
           or else
           ((pt=nb_pts_init) and then
            (float(tab_points_init.all(pt).Coor_y)<=(float(ym)+float(pas)/2.0)))
         then
           Affectation(ym); -- coin bas
        else
          if (float(tab_points_init.all(pt).Coor_y)>(float(ym)+float(pas)/2.0))
           then
            Affectation(ym+pas); -- coin haut
          else
            if (tab_points_init.all(pt).Coor_y
                   < tab_points_init.all(pt+1).Coor_y) then 
              Affectation(ym+pas);
            else
              Affectation(ym);
            end if;
          end if;
        end if;
    end Test_Ordonnee;    

               -- Corps de la procedure Van_horn  --
    begin
      nb_pts_final_prov:=0;
      for pt in 1..nb_pts_init loop
        -- determination de la maille englobant le point courant si ce dernier
        -- n'appartient pas a la maille courante
        if not Point_Dans_Rectangle(tab_points_init.all(pt),
                                 xm,xm+pas,ym,ym+pas) then
          xm:=((tab_points_init.all(pt).Coor_x-Xrep)/pas)*pas+Xrep;
          ym:=((tab_points_init.all(pt).Coor_y-Yrep)/pas)*pas+Yrep;
        end if;
        -- Les tests suivants permettent de determiner l'abscisse du coin de
        -- la maille courante vers lequel le point pt doit etre deplace
        -- Comme la methode consiste a regarder la position du point suivant
        -- lorsque le point courant se trouve sur la moitie de la maille, il
        -- est necessaire de tenir compte du cas particulier que constitue le
        -- dernier point de l'arc initial
        if ((pt/=nb_pts_init) and then 
            (float(tab_points_init.all(pt).Coor_x)<(float(xm)+float(pas)/2.0)))
             -- pt n'est pas le dernier point de l'arc initial et il se trouve 
             -- dans la partie gauche de la maille, milieu non compris
           or else 
           ((pt=nb_pts_init) and then 
            (float(tab_points_init.all(pt).Coor_x)<=(float(xm)+float(pas)/2.0)))
             -- pt est le dernier point de l'arc initial et il se trouve
             -- dans la partie gauche de la maille, milieu compris
         then  
          Test_Ordonnee(xm,pt); -- coin gauche
        else
          -- pt est ou n'est pas le dernier point de l'arc intial et il se
          -- trouve dans la partie droite de la maille, milieu non compris      
          if (float(tab_points_init.all(pt).Coor_x)>(float(xm)+float(pas)/2.0))
           then   
            Test_Ordonnee(xm+pas,pt); -- coin droit
          else
            -- pt n'est pas le dernier point de l'arc initial et il se trouve
            -- au milieu de la maille, son deplacement va dependre de la posi-
            -- tion du point suivant de l'arc initial 
            if (tab_points_init.all(pt).Coor_x
                     < tab_points_init.all(pt+1).Coor_x) then 
            -- point suivant a droite du point courant 
              Test_Ordonnee(xm+pas,pt);
            else
            -- point suivant a gauche du point courant ou sur la meme abscisse 
              Test_Ordonnee(xm,pt);
            end if;
          end if;  
        end if;
      end loop;
      nb_pts_final:=nb_pts_final_prov;
  end Van_Horn;




--*************************************************************************-- 
--**     Corps du paquetage pour l'algorithme de Wall & Danielsson       **--
--**                  contenant les trois versions                       **--
--*************************************************************************-- 


--**********************************************************************-- 
--* Fonction Delta_Aire :                                              *--
--*  Elle permet de calculer la variation de l'aire de deviation due   *--
--* au nouveau point pt pris en consideration                          *--
--**********************************************************************-- 
  function Delta_Aire(pt:integer;Origine:point_type;
                      tab_points_init:point_access_type) return integer is

    Delta_y:integer:=tab_points_init(pt+1).Coor_y
                     -tab_points_init(pt).Coor_y;
    Delta_x:integer:=tab_points_init(pt+1).Coor_x
                     -tab_points_init(pt).Coor_x;

    begin
        
      return((tab_points_init(pt+1).Coor_x-Origine.Coor_x)*Delta_y
             -(tab_points_init(pt+1).Coor_y-Origine.Coor_y)*Delta_x);
  end Delta_Aire;


--*************************************************************************-- 
--* Procedure Wall_Danielsson :                                           *--
--*  Elle permet d'appliquer a un arc l'algorithme de WALL & DANIELSSON   *--
--*    tab_points_init : liste des points constituamt l'arc initial       *--
--*    nb_pts_init : dimension de la liste precedente                     *--
--*    tab_points_final : liste des points constituant l'arc final        *--
--*    nb_pts_final : dimension de la liste precedente                    *--
--*    seuil : critere de selection/suppression des points                *--
--*************************************************************************-- 

  procedure Wall_Danielsson( tab_points_init : point_access_type;
                             nb_pts_init : natural;
                             tab_points_final : point_access_type;
                             nb_pts_final : out natural;
                             seuil: float; resolution : positive) is

    -- Cette variable represente l'origine du repere et le premier point de la 
    -- portion d'arc sur laquelle l'algorithme travaille. Au depart, elle con-
    -- tient le premier point de l'arc  
    Origine:point_type:=(Coor_x => tab_points_init(1).Coor_x,
                         Coor_y => tab_points_init(1).Coor_y);

    -- surface de deviation de la portion d'arc examinee
    aire : float:=0.0;

    longueur : float;
    nb_pts_final_prov : natural;
            
    begin               -- Corps de la procedure Wall_danielsson --
      -- le premier point de l'arc est conserve --
      nb_pts_final_prov:=1;
      tab_points_final(nb_pts_final_prov):=tab_points_init(1);
      if nb_pts_init > 1 then
        if nb_pts_init > 2 then
          -- les deux extremites ne sont pas concernes par l'algorithme --
          for i in 2..(nb_pts_init-1) loop
            -- calcul de l'aire de deviation generee par tous les points 
            -- examines jusqu'a lors
            aire:=aire+float(Delta_Aire(i,Origine,tab_points_init));
            -- calcul de la distance entre le point de l'arc pris commme origine
            -- et le point suivant le point courant
            longueur :=sqrt(float((origine.Coor_x
                                         -tab_points_init(i+1).Coor_x)**2)
                           +float((origine.Coor_y
                                         -tab_points_init(i+1).Coor_y)**2));
            if (abs(aire)>
                (seuil*float(resolution)*longueur))  then
               -- le point courant i a satisfait le critere de surface : l'aire
               -- de deviation lui correspondant est plus grande que la surface
               -- seuil. Il est donc retenu et devient la nouvelle origine 
               nb_pts_final_prov:=nb_pts_final_prov+1;
               tab_points_final(nb_pts_final_prov):=tab_points_init(i);
               Origine.Coor_x:=tab_points_init(i).Coor_x;
               Origine.Coor_y:=tab_points_init(i).Coor_y;
               -- la variable aire est reinitialisee a zero afin de ne mesurer
               -- la deviation de la surface qu'a partir de l'origine nouvelle 
               aire:=0.0;
            end if;
          end loop;
        end if;
        -- le dernier point de l'arc est obligatoirement retenu
        nb_pts_final_prov:=nb_pts_final_prov+1;
        tab_points_final(nb_pts_final_prov):=tab_points_init(nb_pts_init);
      end if;
      nb_pts_final:=nb_pts_final_prov;
  end Wall_Danielsson;


--*************************************************************************-- 
--* Procedure Wall_Danielsson :                                           *--
--*  Elle permet d'appliquer a un arc la version amelioree de l'algorith- *--
--*  me de WALL & DANIELSSON : cette amelioration consiste a detecter les *--
--*  points caracteristiques de l'arc parmi ceux qui ne satisfont pas le  *--
--*  critere de surface. Un point est considere comme caracteristique     *--
--*  lorsqu'il est le plus eloigne de l'origine.                          *--
--****                    I M P O R T A N T                            ****--
--*    Dans cette version amelioree, les points caracteristiques ne sont  *--
--*  pas pris comme origine de la portion d'arc restante                  *--
--*                                                                       *--
--*    tab_points_init : liste des points constituamt l'arc initial       *--
--*    nb_pts_init : dimension de la liste precedente                     *--
--*    tab_points_final : liste des points constituant l'arc final        *--
--*    nb_pts_final : dimension de la liste precedente                    *--
--*    seuil : critere de selection/suppression des points                *--
--*************************************************************************-- 

  procedure Wall_Danielsson_amelioree( tab_points_init : point_access_type;
                                       nb_pts_init : natural;
                                       tab_points_final : point_access_type;
                                       nb_pts_final : out natural;
                                       seuil: float; resolution : positive) is
  
    -- Cette variable represente l'origine du repere et le premier point de la 
    -- portion d'arc sur laquelle l'algorithme travaille. Au depart, elle con-
    -- tient le premier point de l'arc  
    Origine:point_type:=(Coor_x => tab_points_init(1).Coor_x,
                         Coor_y => tab_points_init(1).Coor_y);

    -- aire : surface de deviation de la portion d'arc examinee
    -- longueur : distance entre l'origine et le point suivant le point courant
    -- longueur_i : distance entre l'origine et le point courant
    -- longueur_max : valeur maximale de la variable longueur_i : distance 
    --                associee au point caracteristique
    -- longueur_precedente : valeur precedente de la variable longueur_i
    aire,longueur,longueur_max,longueur_precedente,longueur_i:float:=0.0;

    -- identifie le point caracteristique au fur et a mesure de la recherche
    point_carac : integer;

    -- sauve_pos : vrai => la sauvegarde du point caracteristique point_carac
    --                     est autorisee
    -- phase_ascendante : vrai => la distance longueur_i croit
    --                    faux => la distance longueur_i decroit, il faut alors
    --  sauvegarder le point caracteristique point_carac
    sauve_pos,phase_ascendante:boolean:=true;
    nb_pts_final_prov : natural;



    --**********************************************************************-- 
    --* Procedure Sauvegarde_Point_Caracteristique :                       *--
    --*  Elle permet de conserver le point repere par la variable point_   *--
    --* carac si la distance le separant du point courant est superieure ou*--
    --* egale au seuil et s'il n'a pas deja ete sauvegarde.                *--
    --**********************************************************************-- 
       procedure Sauvegarde_Point_Caracteristique(point:integer;
                                               new_valeur_sauve:boolean) is
      begin
        if (sqrt(float((tab_points_init(point_carac).Coor_x
                        -tab_points_init(point).Coor_x)**2+
                       (tab_points_init(point_carac).Coor_y
                        -tab_points_init(point).Coor_y)**2))
                 >=(seuil*float(resolution))) and sauve_pos then
          nb_pts_final_prov:=nb_pts_final_prov+1;
          tab_points_final(nb_pts_final_prov):=
                                            tab_points_init(point_carac);
          sauve_pos:=new_valeur_sauve;
        end if;
      end Sauvegarde_Point_Caracteristique; 


    begin               -- Corps de la procedure Wall_danielsson --
      -- le premier point de l'arc est conserve --
      nb_pts_final_prov:=1;
      tab_points_final(nb_pts_final_prov):=tab_points_init(1);
      if nb_pts_init > 1 then
        if nb_pts_init > 2 then
          -- les deux extremites ne sont pas concernes par l'algorithme --
          for i in 2..(nb_pts_init-1) loop
            -- calcul de l'aire de deviation generee par tous les points 
            -- examines jusqu'a lors
            aire:=aire+float(Delta_Aire(i,Origine,tab_points_init));
            -- calcul de la distance entre le point de l'arc pris commme origine
            -- et le point suivant le point courant
            longueur :=sqrt(float((origine.Coor_x
                                   -tab_points_init(i+1).Coor_x)**2
                                  +(origine.Coor_y
                                   -tab_points_init(i+1).Coor_y)**2));
            if (abs(aire)>
                (seuil*float(resolution)*longueur))  then
               -- le point courant i a satisfait le critere de surface : l'aire
               -- de deviation lui correspondant est plus grande que la surface
               -- seuil. Il est donc retenu et devient la nouvelle origine 
               -- Si la distance entre l'origine et le point courant est entree
               -- ou entre dans une phase de decroissance, il faut conserver
               -- le point caracteristique en autorisant une prochaine sauvegar
               -- de (true) puisque l'origine change.
               -- les deux tests semblent redondants. Pourtant, il n'en est rien
               -- ils couvrent chacun un cas particulier que l'autre ne prend 
               -- pas en compte :
               --  - le booleen couvre le cas ou la distance du point a l'ori-
               --    gine est superieure a celle du point precedent alors que 
               --    cette distance etait dans une phase de decroissance
               --  - le test sur la longueur couvre le cas ou le point indique
               --    peut etre le debut d'une phase de decroissance
              if ((not phase_ascendante) 
                    or else
                  (sqrt(float((origine.Coor_x-tab_points_init(i).Coor_x)**2
                        +(origine.Coor_y -tab_points_init(i).Coor_y)**2))
                   <=longueur_precedente)) then
                Sauvegarde_Point_Caracteristique(i,true);
              end if;
              nb_pts_final_prov:=nb_pts_final_prov+1;
              tab_points_final(nb_pts_final_prov):=tab_points_init(i);
              Origine.Coor_x:=tab_points_init(i).Coor_x;
              Origine.Coor_y:=tab_points_init(i).Coor_y;
               -- la variable aire est reinitialisee a zero afin de ne mesurer
               -- la deviation de la surface qu'a partir de l'origine nouvelle 
              aire:=0.0;
              longueur_precedente:=0.0; 
              longueur_max:=0.0;
              -- necessaire pour le cas ou le point caracteristique n'est pas
              -- assez distant du point courant. la sauvegarde doit toujours 
              -- etre autorisee apres un changement d'origine 
              sauve_pos:=true;
              phase_ascendante:=true;
            else
              -- le point courant ne satisfaisant pas le critere de surface, 
              -- nous verifions s'il peut etre considere comme un point carac
              -- teristique
              longueur_i :=sqrt(float((origine.Coor_x
                                   -tab_points_init(i).Coor_x)**2
                                  +(origine.Coor_y
                                   -tab_points_init(i).Coor_y)**2));
              if (longueur_i>longueur_max) then
                -- la distance entre l'origine et le point courant croit,
                -- ce dernier est un point caracteristique de l'arc
                point_carac:=i;
                longueur_max:=longueur_i;
                phase_ascendante:=true;
              else
                if (longueur_i<=longueur_precedente) then
                  -- la distance entre l'origine et le point courant decroit,
                  -- le point caracteristique doit etre sauve. si cela est pos-
                  -- sible, tant que l'origine ne change pas ou que la distance
                  -- longueur_i ne croit pas a nouveau, la sauvegarde doit etre
                  -- interdite d'ou le passage de la valeur false
                  Sauvegarde_Point_Caracteristique(i,false);
                  phase_ascendante:=false;
                else
                  -- longueur_precedente < longueur_i <= longueur_max --
                  -- la distance longueur_i croit a nouveau sans pour autant de-
                  -- passer la valeur maximale precedemment enregistree. Une 
                  -- nouvelle phase de croissance est alors presumee. Le point
                  -- caracteristique doit etre conserve et le point courant de-
                  -- vient le nouveau point caracteristique dont la sauvegarde
                  -- eventuelle doit etre possible (true)
                  Sauvegarde_Point_Caracteristique(i,true);
                  longueur_max:=longueur_i;
                  point_carac:=i;
                end if;
              end if;                   
              longueur_precedente:=longueur_i;
            end if;
          end loop;
        end if;
        -- le dernier point de l'arc est obligatoirement retenu
        nb_pts_final_prov:=nb_pts_final_prov+1;
        tab_points_final(nb_pts_final_prov):=tab_points_init(nb_pts_init);
      end if;
      nb_pts_final:=nb_pts_final_prov;
  end Wall_Danielsson_amelioree;


--*************************************************************************-- 
--* Procedure Wall_Danielsson :                                           *--
--*  Elle permet d'appliquer a un arc la version amelioree de l'algorith- *--
--*  me de WALL & DANIELSSON : cette amelioration consiste a detecter les *--
--*  points caracteristiques de l'arc parmi ceux qui ne satisfont pas le  *--
--*  critere de surface. Un point est considere comme caracteristique     *--
--*  lorsqu'il est le plus eloigne de l'origine.                          *--
--****                    I M P O R T A N T                            ****--
--*    Dans cette version amelioree, tout point caracteristique devient   *--
--*  l'origine de la portion d'arc restante                               *--
--*                                                                       *--
--*    tab_points_init : liste des points constituamt l'arc initial       *--
--*    nb_pts_init : dimension de la liste precedente                     *--
--*    tab_points_final : liste des points constituant l'arc final        *--
--*    nb_pts_final : dimension de la liste precedente                    *--
--*    seuil : critere de selection/suppression des points                *--
--*************************************************************************-- 

  procedure Wall_Danielsson_amelioree_CHO(tab_points_init : point_access_type;
                                          nb_pts_init : natural;
                                          tab_points_final : point_access_type;
                                          nb_pts_final : out natural;
                                          seuil: float;
                                          resolution : positive) is
  
    -- Cette variable represente l'origine du repere et le premier point de la 
    -- portion d'arc sur laquelle l'algorithme travaille. Au depart, elle con-
    -- tient le premier point de l'arc  
    Origine:point_type:=(Coor_x => tab_points_init(1).Coor_x,
                         Coor_y => tab_points_init(1).Coor_y);

    -- aire : surface de deviation de la portion d'arc examinee
    -- longueur : distance entre l'origine et le point suivant le point courant
    -- longueur_i : distance entre l'origine et le point courant
    -- longueur_max : valeur maximale de la variable longueur_i : distance 
    --                associee au point caracteristique
    -- longueur_precedente : valeur precedente de la variable longueur_i
    aire,longueur,longueur_max,longueur_precedente,longueur_i:float:=0.0;

    -- point_carac : identifie le point caracteristique au fur et a mesure 
    --               de la recherche
    -- i : point courant
    point_carac,i : integer;

    -- phase_ascendante : vrai => la distance longueur_i croit
    --                    faux => la distance longueur_i decroit, il faut alors
    --  sauvegarder le point caracteristique point_carac
    phase_ascendante:boolean:=true;

   -- origine_changee : vrai si le point caracteristique a ete sauvegarde et 
   --                   donc pris comme nouvelle origine
   --                   faux sinon 
    origine_changee:boolean;
    nb_pts_final_prov : natural;


    --**********************************************************************-- 
    --* Procedure Changement_Origine                                       *--
    --*  Elle permet de sauvegarder les points satisfaisant le critere de  *--
    --* surface ainsi que les points caracteristiques, de changer l'origine*--
    --* et d'effectuer les reinitialisations dues a ce changement d'origine*--
    --**********************************************************************-- 
    procedure Changement_Origine(nouvelle_origine:integer) is
      begin   
        nb_pts_final_prov:=nb_pts_final_prov+1;
        tab_points_final(nb_pts_final_prov):=
                                         tab_points_init(nouvelle_origine);
        Origine.Coor_x:=tab_points_init(nouvelle_origine).Coor_x;
        Origine.Coor_y:=tab_points_init(nouvelle_origine).Coor_y;
        -- la variable aire est reinitialisee a zero afin de ne mesurer
        -- la deviation de la surface qu'a partir de l'origine nouvelle 
        aire:=0.0;
        longueur_precedente:=0.0; 
        longueur_max:=0.0;
        phase_ascendante:=true;
      end Changement_Origine;

    --**********************************************************************-- 
    --* Procedure Sauvegarde_Point_Caracteristique :                       *--
    --*  Elle permet de conserver le point repere par la variable point_   *--
    --* carac si la distance le separant du point courant est superieure   *--
    --* ou egale au seuil                                                  *--
    --**********************************************************************-- 
    procedure Sauvegarde_Point_Caracteristique is
      begin
        if (sqrt(float((tab_points_init(point_carac).Coor_x
                        -tab_points_init(i).Coor_x)**2+
                       (tab_points_init(point_carac).Coor_y
                        -tab_points_init(i).Coor_y)**2))
                 >=(seuil*float(resolution))) then
          Changement_Origine(point_carac);
          i:=point_carac;
          origine_changee:=true;
        end if;
      end Sauvegarde_Point_Caracteristique; 


    begin               -- Corps de la procedure Wall_danielsson --
      -- le premier point de l'arc est conserve --
      nb_pts_final_prov:=1;
      tab_points_final(nb_pts_final_prov):=tab_points_init(1);
      if nb_pts_init > 1 then
          i:=2;
          -- les deux extremites ne sont pas concernes par l'algorithme --
          while (i<=(nb_pts_init-1)) loop
            -- calcul de l'aire de deviation generee par tous les points 
            -- examines jusqu'a lors
            aire:=aire+float(Delta_Aire(i,Origine,tab_points_init));
            -- calcul de la distance entre le point de l'arc pris commme origine
            -- et le point suivant le point courant
            longueur :=sqrt(float((origine.Coor_x
                                   -tab_points_init(i+1).Coor_x)**2
                                  +(origine.Coor_y
                                   -tab_points_init(i+1).Coor_y)**2));
            if (abs(aire)>
                (seuil*float(resolution)*longueur))  then
               -- le point courant i a satisfait le critere de surface : l'aire
               -- de deviation lui correspondant est plus grande que la surface
               -- seuil. Il est donc retenu et il devient la nouvelle origine  
               -- si la tentative de sauvegarde du point caracteristique echoue
             
              -- par defaut, le point caracteristique est suppose ne pas satis
              -- faire le critere d'eloignement
              origine_changee:=false;

               -- Si la distance entre l'origine et le point courant est entree
               -- ou entre dans une phase de decroissance, il faut conserver
               -- le point caracteristique 
              if ((not phase_ascendante) 
                    or else
                  (sqrt(float((origine.Coor_x-tab_points_init(i).Coor_x)**2
                        +(origine.Coor_y -tab_points_init(i).Coor_y)**2))
                   <=longueur_precedente)) then
               -- les deux tests semblent redondants. Pourtant, il n'en est rien
               -- ils couvrent chacun un cas particulier que l'autre ne prend 
               -- pas en compte :
               --  - le booleen couvre le cas ou la distance du point a l'ori-
               --    gine est superieure a celle du point precedent alors que 
               --    cette distance etait dans une phase de decroissance
               --  - le test sur la longueur couvre le cas ou le point indique
               --    peut etre le debut d'une phase de decroissance
                Sauvegarde_Point_Caracteristique;
              end if;
              if not origine_changee then
                -- le point caracteristique,s'il existait, n'a pas ete conserve
                -- le point courant est donc pris comme nouvelle origine
                Changement_Origine(i);
              end if;
            else
              -- le point courant ne satisfaisant pas le critere de surface, 
              -- nous verifions s'il peut etre considere comme un point carac
              -- teristique
              longueur_i :=sqrt(float((origine.Coor_x
                                   -tab_points_init(i).Coor_x)**2
                                  +(origine.Coor_y
                                   -tab_points_init(i).Coor_y)**2));
              if (longueur_i>longueur_max) then
                -- la distance entre l'origine et le point courant croit,
                -- ce dernier est un point caracteristique de l'arc
                point_carac:=i;
                longueur_max:=longueur_i;
                phase_ascendante:=true;
              else
                origine_changee:=false;
                Sauvegarde_Point_Caracteristique;
                if not origine_changee then
                  if (longueur_i<=longueur_precedente) then
                  -- la distance entre l'origine et le point courant decroit,
                  -- le point caracteristique doit etre sauve. Si cela n'a pas
                  -- pu etre fait, nous restons dans une phase de decroissance
                    phase_ascendante:=false;
                  else
                  -- longueur_precedente < longueur_i <= longueur_max --
                  -- la distance longueur_i croit a nouveau sans pour autant de-
                  -- passer la valeur maximale precedemment enregistree. Une 
                  -- nouvelle phase de croissance est alors presumee. Le point
                  -- caracteristique doit etre conserve et le point courant de-
                  -- vient le nouveau point caracteristique si le precedent a
                  -- pu etre sauvegarde
                    longueur_max:=longueur_i;
                    point_carac:=i;
                  end if;
                  -- la distance a l'origine pour le point courant ne doit etre
                  -- conserve que si l'origine n'a pas ete changee
                  longueur_precedente:=longueur_i;
                end if;
              end if;                   
            end if;
            -- point suivant
            i:=i+1;
          end loop;
        -- le dernier point de l'arc est obligatoirement retenu
        nb_pts_final_prov:=nb_pts_final_prov+1;
        tab_points_final(nb_pts_final_prov):=tab_points_init(nb_pts_init);
      end if;
      nb_pts_final:=nb_pts_final_prov;
  end Wall_Danielsson_amelioree_CHO;



--*************************************************************************-- 
--**     Corps du paquetage pour l'algorithme de BROPHY                  **--
--**                  contenant les trois versions                       **--
--*************************************************************************-- 

--- Sous-programme de lissage de ligne
--- Methode : BROPHY
--- Fichier : BROPHYX.ADP
--- Le 08/07/1993

------------------------------------------------------------------------
-- Entree :	tableau : Liste des points de l'arc                   --
--		navant : Nombre de points de l'arc                    --
--		seuil : Facteur de lissage compris entre 0 et 1       --
--                                                                    --
-- Sortie :                                                           --
--		tabres : Liste des points apres lissage               --
--		napres : Nombre de points apres lissage (idem navant) --
------------------------------------------------------------------------ 
procedure Brophyx(	tableau : in Point_Access_type;
 			navant : natural; 
 			tabres : Point_Access_type;
                        napres : out natural; 
 			seuil : float ) is

 -- Cette fonction retourne le centre du cercle inscrit dans le triangle
 -- forme par les trois points p1, p2 et p3 

 function CentreCercleInscrit(p1 : Point_type;p2 : Point_type;p3 : Point_type)
          return Point_type is 

 np1p2,np1p3,np2p3 : float;
 ux,uy,vx,vy,wx,wy : float;
 uux,uuy,vvx,vvy : float;
 alpha,beta :  float;
 centreX,centreY : float;  
 res : Point_type; -- centre du cercle
 cosAngle : float;
 Det : float;
 
 begin -- CentreCercleInscrit

  -- On commence par calculer les normes des vecteurs situes sur les cotes
  -- du triangle

  np1p2 := sqrt(float((p1.Coor_x-p2.Coor_x)*(p1.Coor_x-p2.Coor_x) 
                    + (p1.Coor_y-p2.Coor_y)*(p1.Coor_y-p2.Coor_y) ));
  np1p3 := sqrt(float((p1.Coor_x-p3.Coor_x)*(p1.Coor_x-p3.Coor_x) 
                    + (p1.Coor_y-p3.Coor_y)*(p1.Coor_y-p3.Coor_y) ));
  np2p3 := sqrt(float((p2.Coor_x-p3.Coor_x)*(p2.Coor_x-p3.Coor_x) 
                    + (p2.Coor_y-p3.Coor_y)*(p2.Coor_y-p3.Coor_y) ));
  
  if ((np1p2 /= 0.0) AND (np1p3 /= 0.0) AND (np2p3 /= 0.0)) then

  -- Si aucune de ces normes est nulle, on peut continuer le lissage

  ux := float(p2.Coor_x-p1.Coor_x) / np1p2; 
  uy := float(p2.Coor_y-p1.Coor_y) / np1p2;
  vx := float(p3.Coor_x-p1.Coor_x) / np1p3;
  vy := float(p3.Coor_y-p1.Coor_y) / np1p3;
  wx := float(p3.Coor_x-p2.Coor_x) / np2p3;
  wy := float(p3.Coor_y-p2.Coor_y) / np2p3;

  uux := ux + vx;
  uuy := uy + vy;
  vvx := wx - ux;
  vvy := wy - uy;
 
  alpha := uuy*float(p1.Coor_x) - uux*float(p1.Coor_y);
  beta := vvy*float(p2.Coor_x) - vvx*float(p2.Coor_y);
 
  det := (uux*vvy - uuy*vvx);  
 
  if (det = 0.0) then
   -- Les 3 points sont alignes
   res := p2;
  else
   centreX := (beta*uux - alpha*vvx) / det;
   centreY := (beta*uuy - alpha*vvy) / det;

   res.Coor_x := integer(centreX);
   res.Coor_y := integer(centreY);
  end if;
  else

   -- Si une seule des normes est nulle, on decide de renvoyer le point p1

   res := p1;
  end if;

  return res;
  
 end CentreCercleInscrit;

 -- Cette fonction realise une interpolation lineaire entre les deux points
 -- passes en parametre en fonction de la valeur seuil

 function Interpolation(p : Point_type; centre : Point_type) return Point_type is
 
 res : Point_type;
 norme : float;
 ux,uy : float;

 begin -- Interpolation
  
   res.Coor_x := p.Coor_x + integer(seuil*float(centre.Coor_x-p.Coor_x));
   res.Coor_y := p.Coor_y + integer(seuil*float(centre.Coor_y-p.Coor_y));

  return res;
         
 end Interpolation;

 -- Cette procedure realise le lissage sur l'ensemble des points de l'arc

 procedure methode_brophy is 
 
 sup : natural;
 centre : Point_type;
 point : Point_type; -- Nouveau point 
 
 begin -- brophy

  if (navant <= 2) then 
   
   tabres(1..navant) := tableau(1..navant);
  
  else

   -- La methode ne deplace pas les points extremites

   tabres(1) := tableau(1);
   tabres(navant) := tableau(navant);
  
   sup := navant - 1;

   for I in 2..sup loop
 
    -- Calcul du centre du cercle inscrit dans le triangle compose des points
    -- voisins du sommet I

    centre := CentreCercleInscrit(tableau(I-1),tableau(I),tableau(I+1));
    
    -- A partir de ce point, on calcule la nouvelle position du sommet I par
    -- interpolation lineaire reliant le point a deplacer au centre du cercle

    tabres(I) := Interpolation(tableau(I),centre);  
             
   end loop;
  end if;
 
 end methode_brophy;


begin -- Brophyx

  methode_Brophy; -- Lissage par la methode Brophy  
  napres := navant;

end Brophyx;






--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de Moyenne ponderee          **--
--*************************************************************************-- 


-- Sous-programme de simplification lineaire ( lissage )
-- Methode : Moyennage pondere ( k-voisinage )
-- Fichier : PAVERAGEX.ADP
-- Le 08/07/1993


----------------------------------------------------------------
-- Entree :	tableau : Liste des points de l'arc initial   --
--		navant : Nombre de points de l'arc            --
--		k : Type de voisinage (3, 5, 7...)            --
--                                                            --
-- Sortie :	tabres : Liste des points de l'arc resultat   --
--		napres : Nombre de points de l'arc            --
----------------------------------------------------------------
procedure paveragex(
			tableau : Point_Access_type; 
			navant : natural;
			tabres : Point_Access_type;							
			napres : out natural; 
			k : natural ) is 


MAXK : natural := 4; -- Maximum (2*4+1 =) 9-points voisinage

-- tableau des poids associes aux points          

tabpoids : array(1..MAXK,1..(2*MAXK+1)) of float
           := 	(1 => (0.25,0.5,0.25,0.0,0.0,0.0,0.0,0.0,0.0),
	       	 2 => (0.1,0.2,0.4,0.2,0.1,0.0,0.0,0.0,0.0),
		 3 => (0.05,0.1,0.15,0.4,0.15,0.1,0.05,0.0,0.0),
		 4 => (0.02,0.08,0.1,0.12,0.36,0.12,0.1,0.08,0.02));
 
-- procedure effectuant le lissage d'un arc

procedure lissage is
maxpoints : natural;
mink : natural; -- type de voisinage en cours, qui peut etre different du
		-- voisinage choisi (points extremites)
borne_inf,borne_sup : natural;
ind : natural;

 -- fonction retournant le minimum de 3 valeurs

 function min(a : natural;b : natural;c : natural) return natural is
 
 begin -- min

  if (a < b) then
 
   if (a < c) then return a;
   else return c;
   end if;

  elsif (b < c) then return b;

  else return c; 
  
  end if;
 end min;

begin -- lissage

 -- Les points extremites ne bougent pas
 
 tabres(1) := tableau(1);
 tabres(navant) := tableau(navant);

 maxpoints := navant -1;
 for I in 2..maxpoints loop
 
  -- Recherche du voisinage adequate

  mink := min(k,2*(I-1)+1,2*(navant-I)+1);
  ind := mink / 2;
  borne_inf := I - ind;
  borne_sup := I + ind; 
  

  -- Calcul du nouveau point  

  tabres(I).Coor_x := 0;tabres(I).Coor_y := 0;

  for J in borne_inf..borne_sup loop
  
   tabres(I).Coor_x := tabres(I).Coor_x + integer(float(tableau(J).Coor_x)*tabpoids(ind,J-borne_inf+1));
   
   tabres(I).Coor_y := tabres(I).Coor_y + integer(float(tableau(J).Coor_y)*tabpoids(ind,J-borne_inf+1));   
 
  end loop;
 
 end loop;
 
end lissage; 
                                     
begin -- paveragex
   
  lissage; -- Appel de la procedure de lissage
  napres := navant;
 
end paveragex;





--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de Moyenne ponderee          **--
--**                             avec glissement vers le point moyen     **--
--*************************************************************************-- 

-- Sous-programme de simplification lineaire ( lissage )
-- Methode : Glissement moyen pondere ( k-voisinage )
-- Fichier : SAVERAGEX.ADP
-- Le 08/07/1993

----------------------------------------------------------------------
-- Entree :	tableau : Liste des points de l'arc                 --
--		navant : Nombre de points de l'arc                  --
--		k : Valeur du voisinage (3, 5, 7...)                --
--		slide : Facteur de glissement compris entre 0 et 1  --
--                                                                  --
-- Sortie :	tabres : Liste des points de l'arc resultat         --
--		napres : nombre de points de l'arc (idem navant)    --
----------------------------------------------------------------------
procedure saveragex(
			tableau : Point_Access_type;
			navant : natural; 
			tabres : Point_Access_type;	
			napres : out natural;
			k : natural; 
			slide : float ) is

-- tableau des poids associes aux points  
        
MAXK : natural := 4; -- au maximum voisinage de 2*4+1 points
tabpoids : array(1..MAXK,1..(2*MAXK+1)) of float
           := 	(1 => (0.25,0.5,0.25,0.0,0.0,0.0,0.0,0.0,0.0),
	       	 2 => (0.1,0.2,0.4,0.2,0.1,0.0,0.0,0.0,0.0),
		 3 => (0.05,0.1,0.15,0.4,0.15,0.1,0.05,0.0,0.0),
		 4 => (0.02,0.08,0.1,0.12,0.36,0.12,0.1,0.08,0.02));
rep : character;
 
-- procedure effectuant le lissage d'un arc

procedure lissage is
maxpoints : natural;
mink : natural; -- type de voisinage en cours, qui peut etre different du
		-- voisinage choisi (points extremites)
borne_inf,borne_sup : natural;
ind : natural;

 -- fonction retournant le minimum de 3 valeurs

 function min(a : natural;b : natural;c : natural) return natural is
 
 begin -- min

  if (a < b) then
 
   if (a < c) then return a;
   else return c;
   end if;

  elsif (b < c) then return b;

  else return c; 
  
  end if;
 end min;

begin -- lissage

 -- Les points extremites ne bougent pas
 
 tabres(1) := tableau(1);
 tabres(navant) := tableau(navant);

 maxpoints := navant -1;
 for I in 2..maxpoints loop
 
  -- Recherche du voisinage adequate

  mink := min(k,2*(I-1)+1,2*(navant-I)+1);
  ind := mink / 2;
  borne_inf := I - ind;
  borne_sup := I + ind; 
  

  -- Calcul du nouveau point  

  tabres(I).Coor_x := 0;tabres(I).Coor_y := 0;

  for J in borne_inf..borne_sup loop
  
   tabres(I).Coor_x := tabres(I).Coor_x 
+ integer(float(tableau(J).Coor_x)*tabpoids(ind,J-borne_inf+1));
   
   tabres(I).Coor_y := tabres(I).Coor_y 
+ integer(float(tableau(J).Coor_y)*tabpoids(ind,J-borne_inf+1));   
 
  end loop;

 -- Glissement du point 
 
 tabres(I).Coor_x := tableau(I).Coor_x + integer(float(tabres(I).Coor_x-tableau(I).Coor_x)*slide);

 tabres(I).Coor_y := tableau(I).Coor_y + integer(float(tabres(I).Coor_y-tableau(I).Coor_y)*slide); 
 
 end loop;
 
end lissage; 
                                     
begin -- saveragex
   
  lissage; -- Appel de la procedure de lissage
  napres := navant;
 
end saveragex;





--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de Moyenne ponderee          **--
--**                                                par distance         **--
--*************************************************************************-- 

-- Sous-programme de simplification lineaire ( lissage )
-- Methode : moyennage pondere des distances ( k-voisinage )
-- Fichier : DAVERAGEX.ADP
-- Le 08/07/1993

------------------------------------------------------------------------------- 
-- Entree :	tableau : Liste des points de l'arc
--		navant : Nombre de points de l'arc
--		poids_points : Poids associe au point a deplacer
--		poids_dist : Poids associe a la somme pondere par les distances
--		k : Valeur du voisinage (3, 5, 7...)
--
-- Sortie :	tabres : Liste des points apres lissage
--		napres : Nombre de points de l'arc resultat (idem navant)
-------------------------------------------------------------------------------
procedure daveragex(
			tableau : Point_Access_type; 
			navant : natural; 
			tabres : Point_Access_type;	
			napres : out natural;
	                poids_point : float;
			poids_dist : float;
			k : natural ) is

 
-- procedure effectuant le lissage d'un arc

procedure lissage is



DISTMIN : float := 0.0625; -- Indique la distance minimale entre deux points 
maxpoints : natural;
mink : natural; -- type de voisinage en cours, qui peut etre different du
		-- voisinage choisi (points extremites)
borne_inf,borne_sup : natural;
ind : natural;
vresX,vresY : float;
distances : array(1..((2*k)+1)) of float;
coeff : float;
s : float;

 -- fonction retournant le minimum de 3 valeurs

 function min(a : natural;b : natural;c : natural) return natural is
 
 begin -- min

  if (a < b) then
 
   if (a < c) then return a;
   else return c;
   end if;

  elsif (b < c) then return b;

  else return c; 
  
  end if;
 end min;

begin -- lissage

 -- Les points extremites ne bougent pas
 
 tabres(1) := tableau(1);
 tabres(navant) := tableau(navant);

 maxpoints := navant -1;
 for I in 2..maxpoints loop
 
  -- Recherche du voisinage adequate

  mink := min(k,2*(I-1)+1,2*(navant-I)+1);
  ind := mink / 2;
  borne_inf := I - ind;                                  
  borne_sup := I + ind; 
  

  -- Calcul du nouveau point  

  -- Calcul des distances au point I
  
 -- calcul de la somme des 1/di...

  s := 0.0;

  for J in borne_inf..borne_sup loop
  
   if (J /= I) then 
    distances(J-borne_inf+1) := sqrt(float(
                     (tableau(I).Coor_x-tableau(J).coor_x)*(tableau(I).Coor_x-tableau(J).Coor_x) 
        		   +(tableau(I).Coor_y-tableau(J).Coor_y)*(tableau(I).Coor_y-tableau(J).Coor_y)));
                
    -- Cas particulier : la distance entre le point a deplacer et un point du voisinage est nulle
    if (distances(J-borne_inf+1) = 0.0) then 
	distances(J-borne_inf+1) := DISTMIN; 
    end if;
    s := s + 1.0/distances(J-borne_inf+1);
   end if;

  end loop;

  coeff := poids_dist/s;

  vresX := 0.0;vresY := 0.0;

  for J in borne_inf..borne_sup loop
  
   if (J /= I) then              
    vresX := vresX + (coeff*float(tableau(J).Coor_x) / distances(J-borne_inf+1)); 
    vresY := vresY + (coeff*float(tableau(J).Coor_y) / distances(J-borne_inf+1)); 
   end if;

  end loop;
  
  tabres(I).Coor_x := integer( poids_point*float(tableau(I).Coor_x) + vresX);
  tabres(I).Coor_y := integer( poids_point*float(tableau(I).Coor_y) + vresY);
 
 end loop;
 
end lissage; 
                                     
begin -- daveragex

 lissage; -- Appel de la procedure de lissage
 napres := navant; 
  
end daveragex;



--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de THAPA                     **--
--*************************************************************************-- 

-- Sous-programme de detection de points critiques
-- Methode : THAPA
-- Fichier : THAPA1X.ADP
-- Le 08/07/1993

-- Variante : Dx est calcule sur l'ensemble des points depuis le dernier point
--	      critique trouve. En fait la taille de l'ensemble est au plus 4.

------------------------------------------------------------------------------- 
-- Entree :	tableau : Liste des points de l'arc initial                  --
--		navant : Nombre de points de l'arc                           --
--		seuil : Valeur permettant de controler le degre de detection --
--			des points critiques ( < 1 )                         --
--                                                                           --
-- Sortie : 	tabres : Liste des points critiques                          --
--		napres : Nombre de points critiques                          --
-------------------------------------------------------------------------------
procedure thapa1x(
 			tableau : Point_Access_type;
 			navant : natural; 
			tabres : Point_Access_type;
 			napres : out natural;
			seuil : float ) is

  napres_prov : natural;

  procedure Calcul is 

  borne_inf,borne_sup : natural;
  maxpoints : natural;
  Xm,Ym : float; -- Composante du vecteur vp
  Deno : float;
  a11,a12,a21,a22 : float; -- Element de la matrice de dispersion
  Dx : float; 


  begin -- Calcul

  tabres(1) := tableau(1); -- Le point initial est un point critique
 
  napres_prov := 1;
  borne_inf := 1;
  maxpoints := navant - 1;

  for I in 2..maxpoints loop
 
   borne_sup := I + 1;
   Xm := 0.0;Ym := 0.0;

   -- Calcul du vecteur vp
  
   for J in borne_inf..borne_sup loop

    Xm := Xm + float(tableau(J).Coor_x);
    Ym := Ym + float(tableau(J).Coor_y);

   end loop;

   Xm := Xm / float(borne_sup-borne_inf+1);
   Ym := Ym / float(borne_sup-borne_inf+1);

   -- Calcul de la valeur Deno (?)

   Deno := 0.0;

   for J in borne_inf..borne_sup loop
    Deno := Deno + float( (float(tableau(J).Coor_x) - Xm)*(float(tableau(J).Coor_x) - Xm)
                         +(float(tableau(J).Coor_y) - Ym)*(float(tableau(J).Coor_y) - Ym));
   end loop;

   -- Calcul de la matrice de dispersion 

   a11 := 0.0; a12 := 0.0; a22 := 0.0;

   for J in borne_inf..borne_sup loop

    a11 := a11 + (float(tableau(J).Coor_x) - Xm)*(float(tableau(J).Coor_x) - Xm);
    a12 := a12 + (float(tableau(J).Coor_x) - Xm)*(float(tableau(J).Coor_y) - Ym);
    a22 := a22 + (float(tableau(J).Coor_y) - Ym)*(float(tableau(J).Coor_y) - Ym);

   end loop;

   a11 := a11 / Deno;
   a22 := a22 / Deno;
   a12 := a12 / Deno;
  
   a21 := a12;

   -- Calcul de la valeur Dx
   
   Dx := sqrt(1.0 - 4.0*(a11*a22 - a12*a21));

   if (Dx > seuil) then -- droite
    if ( (I-borne_inf) > 4) then
     borne_inf := borne_inf + 1; 
    end if;
   else -- Le point considere est un point critique
 
    napres_prov := napres_prov + 1;
    tabres(napres_prov) := tableau(I);
    borne_inf := I; -- Ce point devient le nouveau point de depart
   
   end if;
  
   end loop;

   napres_prov := napres_prov + 1;

   tabres(napres_prov) := tableau(navant); -- le point final est un point critique
 
  end Calcul;

begin -- thapa1x

  napres_prov := 1;
   
  Calcul; -- Lancement du processus de detection de points critiques
  napres:=napres_prov;

end thapa1x;








--*************************************************************************-- 
--**   Corps du paquetage pour l'algorithme de WHIRLPOOL                    **--
--*************************************************************************-- 

--- Sous-programme de simplification lineaire (compression+lissage)
--- Methode : Adaptation bande epsilon (Perkal)
--- Fichier : WHIRLPOOLX.ADP
--- Le 18/06/1993

---------------------------------------------------------------------------
-- Entree :	tableau : Liste des points de l'arc                      --
--		navant : Nombre de points de l'arc                       --
--		seuil : Indique la proximite maximale entre deux points  --
--              precision : Nombre de pixel/mm                           --
--                                                                       --
-- Sortie :	tabres : Liste des points apres simplification           --
--		napres : Nombre de points du nouvel arc                  --
---------------------------------------------------------------------------

 procedure whirlpoolx(
			tableau : Point_Access_type;
 			navant : natural; 
 			tabres : Point_Access_type;
 			napres : out natural;
 			seuil : float;
			precision : positive ) is 

 leSeuil : float; -- Seuil effectivement applique
 napres_prov : natural;

 procedure calcul is 
 
 nbamas : natural := 0; -- Nombre d'amas de points pour l'arc courant
 amas : array(1..navant) of integer := (others => 0) ;  -- tableau indiquant a quel amas appartient un point   
 proche_amas : array(1..navant) of integer := (others => 0); -- tableau contenant amas proches du point en cours
 count_amas : natural; -- Nombre d'amas proche du point considere 
 temp : integer;
 numero : integer;
 centroide : Point_type; -- Centre de gravite d'un amas
 amas_courant : integer; 
 cpt : integer;
 sup : natural;


 -- Fonction calculant le barycentre d'un amas de points 

 function centroide_amas(num : integer) return Point_type is
 
 baryX : float; -- Le barycentre
 baryY : float;
 res : Point_type;
 n : natural;
  
 begin -- centroide

  baryX := 0.0;
  baryY := 0.0; 

  n := 0;
 
  for I in 1..navant loop
 
   if (amas(I) = num) then
    baryX := baryX + float(tableau(I).Coor_x);
    baryY := baryY + float(tableau(I).Coor_y);
    n := n + 1;
   end if;
 
  end loop;
  
  baryX := baryX / float(n);
  baryY := baryY / float(n);

  res.Coor_x := integer(baryX);
  res.Coor_y := integer(baryY); 
 
  return res;  

 end centroide_amas;


 -- Fonction testant la proximite de 2 points, relativement au seuil

 function est_proche(p1 : Point_type; p2 : Point_type) return boolean is
 
 distance : float;

 begin -- proche
  
  distance := sqrt(float(  (p1.Coor_x-p2.Coor_x)*(p1.Coor_x-p2.Coor_x) 
                         + (p1.Coor_y-p2.Coor_y)*(p1.Coor_y-p2.Coor_y)));

  return (distance < leSeuil);

 end est_proche;

 begin -- calcul
   
   -- Le point initial ne bouge pas
   tabres(1) := tableau(1);
  
  -- On commence par clusteriser l'ensemble des points de l'arc hormis les points extremites

  sup := navant - 1;  

  for I in 2..sup loop
   if (amas(I) = 0) then 
    -- Ce point n'a pas encore ete considere 
    
    -- On commence par detecter les amas situes a proximite du point etudie
  
    count_amas := 0;
    
    for J in 2..sup loop
     if ( (amas(J) /= 0) ) then 
      if (est_proche(tableau(I),tableau(J))) then 
   
       cpt := 1;
       while (cpt <= count_amas) loop
        exit when (amas(J) = proche_amas(cpt));
        cpt := cpt + 1; 
       end loop;
  
       if (cpt > count_amas) then 
        count_amas := count_amas + 1; -- On ajoute l'amas
        proche_amas(count_amas) := amas(J);
       end if;
      end if;
     end if;
    end loop;    
  
    nbamas := nbamas - count_amas + 1; -- Mise a jour du nombre d'amas construit 
 
    -- Puis, on fusionne l'emsemble des amas proches 
  
    for J in 1..count_amas loop
     for K in 2..sup loop 
      if (amas(K) = proche_amas(J)) then 
       amas(K) := nbamas;
      end if;
     end loop;
    end loop;

    amas(I) := nbamas;

   end if;
  end loop;

--  put_line("Nombre d'amas = "&INTEGER'IMAGE(nbamas));

  -- Une fois les points regroupes dans des amas, un traitement prealable assigne a chaque amas un
  -- numero, dont la valeur est d'autant plus grande que son point extremite est loin ( lorsque l'on
  -- parcourt la ligne ) ( un dessin serait beaucoup mieux !!!)
  -- Cela est utile lorsque l'on devra decider de connaitre les amas a conserver ( ou tout du moins
  -- leur centroide ) 

  numero := -1; -- Compteur de numero d'amas

  for I in reverse 2..sup loop

  if (amas(I) > 0) then 
 
    temp := amas(I);
  
    for J in reverse 2..I loop
  
     if (amas(J) = temp) then
      amas(J) := numero;
     end if;
   
    end loop;
   
    numero := numero - 1;
   
   end if;
  end loop;

  -- Enfin, on parcourt les points pour ne retenir que les centroides des amas

  amas_courant := numero;
 
  for I in 2..sup loop
  
   if (amas(I) > amas_courant) then
    napres_prov := napres_prov + 1;
    tabres(napres_prov) := centroide_amas(amas(I));
    amas_courant := amas(I);
   end if;  
 
  end loop;

  napres_prov := napres_prov + 1; -- On ajoute le point final a la liste des points

  tabres(napres_prov) := tableau(navant);
  

 end calcul;


begin -- Whirlpoolx

   leSeuil := seuil*float(precision);

   napres_prov := 1; 
      
   calcul; -- Appel a la procedure de simplification
     
   napres:=napres_prov;   

end whirlpoolx;



--*************************************************************************--
--* Procedure Lang:                                                       *--
--*  Elle permet d'appliquer a un arc l'algorithme de LANG                *--
--*    tabi : liste des points constituant l'arc initial                  *--
--*    nbi : dimension de la liste precedente                             *--
--*    tabf : liste des points constituant l'arc final                    *--
--*    nbf : dimension de la liste precedente                             *--
--*    n : nombre de points d'analyse                                     *--
--*    t : tolerance                                                      *--
--*************************************************************************--


Procedure Lang  (       tabi    	:       Point_Access_type;
                        nbi     	:       natural;
                        tabf    	:       Point_Access_type;
                        nbf     	:       out natural;
                        n       	:       natural;
                        t       	:       float;
			resolution      :	positive		)   is

prec : natural;
i : natural;
fin : natural;
flag : boolean;
nbf_prov : natural;
 -- Cette variable represente l'origine du repere et le premier point de la
 -- portion d'arc sur laquelle l'algorithme travaille. Au depart, elle con-
 -- tient le premier point de l'arc.
origine : point_type := ( coor_x => tabi(1).coor_x,
                          coor_y => tabi(1).coor_y);


begin

  -- Le premier point de l'arc est conserve. --
nbf_prov := 1;
tabf(nbf_prov) := tabi(1);
prec := 0;
i := 1;
  -- L'analyse s'arrete au dernier point de l'arc obligatoirement. --
fin := min ( nbi , ( i + n - prec ) );


while i /= nbi loop
      flag := true;
      for j in ( i + 1 )..( fin - 1 ) loop
          if distance_a_ligne(tabi.all(j),tabi.all(i),tabi.all(fin),
                               C_segment_type )<(t*float(resolution)) then 
             null;
          else
             flag := false;
             exit;
          end if;
      end loop;

      if flag = false then 
	 -- Si une distance est hors tolerance, on recule d'un point. --
         prec := prec + 1;
         fin := min ( nbi , ( i + n - prec ) );
      else
	 -- Si toutes les distances rentrent dans la tolerance, on enregistre --
	 --le point final et on le prend comme origine pour l'analyse suivante--
         nbf_prov := nbf_prov + 1;
         i := fin;
         tabf(nbf_prov) := tabi(i);
         prec := 0;
         fin := min ( nbi , ( i + n - prec ) );
      end if;
end loop;
nbf:=nbf_prov;

end Lang;


end Lissage_filtrage;
