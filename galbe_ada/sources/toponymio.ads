--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with inipan; use inipan;
with detpos; use detpos;
with gen_io; use gen_io;
with sequential_io;
with text_io; use text_io;
with win32.winnt;
with Interfaces.C;
with Win32.Winbase;

package toponymio is

 Package nom_io is new sequential_io(nom_type); use nom_io;
 Package position_io is new sequential_io(position_type); use position_io;
 Package integ_io is new sequential_io(integer); use integ_io;
 package int_io is new text_io.integer_io(integer); use int_io; 
 package flo_io is new text_io.float_io(float); use flo_io; 

--===================================================
-- Creation d'un fichier de sauvegarde des donnees --
-- En fin du module de calcul --
--===================================================
procedure Sauvegarde (tbnomi:in typtbnomi;
                      tnp   :in typtnp;
                      tp    :in typtp;
                      tnci  :in typtnci;
                      tici  :in typtici;
                      InvEchelle      : positive;
                      Resolution      : positive;
                      Minimum_terrain : Point_type_reel;
					  sp : in out boolean);
                      
--================================================
-- Lecture du fichier de sauvegarde des donnees --
--================================================
procedure Lecture_positions (tbnomi:in out typtbnomi;
                             tnp   :in out typtnp;
                             tp    :in out typtp;
                             tnci  :in out typtnci;
                             tici  :in out typtici );
                             
--===============================================================
--Insertion des nouvelles positions --
--===============================================================
procedure Insere_Nouvelles_Positions (ficNouvPos: in string;
                                      tbnomi	: in out typtbnomi;
                                      tnp   	: in out typtnp;
                                      tp    	: in out typtp;
                                      InvEchelle      : positive;
                                      Resolution      : positive;
                                      Minimum_terrain : Point_type_reel;
                                      hWnd: in Win32.winnt.Handle);
                                       
--=========================================================
-- Fichier de sortie au format texte des ecritures placees
-- a support rectiligne + sortie viewer avec texte des blasons texte
--=========================================================
procedure sauve_ecritures_Placees_recti (tbnomi : in typtbnomi;
                                      tnp : in typtnp;  --nb de positions
                                      tp : in typtp;   -- tableau des positions
                                      InvEchelle : positive;
                                      Resolution : positive;
                                      Minimum_terrain : Point_type_reel;
                                      f2 : in out text_io.file_type;
                                      miecp : in out boolean;
									  f3 : in out text_io.file_type);

--=========================================================
-- Fichier de sortie au format texte des ecritures placees
-- a disposition sur courbe + sortie viewer
--=========================================================
procedure sauve_ecritures_placees_dispo (tbnomi : in typtbnomi;
                                      tnp : in typtnp;  --nb de positions
                                      tp : in typtp;   -- tableau des positions
                                      InvEchelle : positive;
                                      Resolution : positive;
                                      Minimum_terrain : Point_type_reel;
                                      f2 : in out text_io.file_type;
                                      miecp : in out boolean;
									  f3 : in out text_io.file_type);

 
--==========================================================================
-- Fichier de sortie au format texte des ecritures bloquees + sortie viewer --
--==========================================================================
procedure sauve_ecritures_bloquees (tbnomi : in typtbnomi;
                                       tnp : in typtnp;  --nb de positions
                                       tp : in typtp;   -- tableau des positions
                                       InvEchelle : positive;
                                       Resolution : positive;
                                       Minimum_terrain : Point_type_reel;                                     
                                       f2 : in out text_io.file_type;
                                       compesp : in out integer;
                                       compeb : in out integer;
                                       compmtsp : in out integer;
                                       compmtb : in out integer;
 									   f3 : in out text_io.file_type);

                                            
--====================================================================
-- Fichier de sortie au format texte des boites des ecritures placees
-- et (separement) de leurs blasons + sortie viewer
--====================================================================
procedure sauve_boites_placees (tbnomi : in typtbnomi; 
                                   tnp : in typtnp;  --nb de positions
                                   tp : in typtp;   -- tableau des positions
                                   InvEchelle : positive;
                                   Resolution : positive;
                                   Minimum_terrain : Point_type_reel;
                                   f2 : in out text_io.file_type;
                                   fblatex : in out text_io.file_type;
                                   fblaima : in out text_io.file_type);
                                   
                           
--===================================================================================
-- Fichier de sortie au format texte des boites des ecritures bloquees + sortie viewer
--===================================================================================
procedure sauve_boites_bloquees (tbnomi : in typtbnomi;
                                    tnp : in typtnp;  --nb de positions
		   	   						InvEchelle : positive;
               						Resolution : positive;
               						Minimum_terrain : Point_type_reel;
                                    f2 : in out text_io.file_type;
                                    fblatex : in out text_io.file_type;
                                    fblaima : in out text_io.file_type);

--================================================
end toponymio;
