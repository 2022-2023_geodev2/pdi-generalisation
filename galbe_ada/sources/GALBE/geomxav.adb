with geometrie; use geometrie;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

With text_io; use text_io;
--with Gen_io; use Gen_io;
--With math_cogit; use math_cogit;
--With  ada_cogit; use  ada_cogit;
--With  xlib_cogit; use xlib_cogit;
--With lissage_filtrage;
--With  x_lib; use x_lib;
--with sequential_io;
--with export; use export;
                     
Package body Geomxav is 

  PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)

Package real_io is new Float_io(float); Use real_io;
 Package int_io is new Integer_io(integer); Use int_io;
-----------------------------------
-- Distance (A B) -> coord entieres
----------------------------------
Function Distance(A,B	: in gen_io.point_type) return float is 
dist	: float;
Begin  
	dist:=sqrt(float(A.coor_x-B.coor_x)*float(A.coor_x-B.coor_x)+ float(A.coor_y-B.coor_y)*float(A.coor_y-B.coor_y));
  return dist;
End Distance;
----------------------------------
-- Distance (A B) -> coord reelles
----------------------------------
Function Distance(A,B	: in gen_io.point_type_reel) return float is
 dist	: float;
Begin
  dist:=sqrt((A.coor_x-B.coor_x)*(A.coor_x-B.coor_x)+(A.coor_y-B.coor_y)*(A.coor_y-B.coor_y));
  return dist;
End Distance;
----------------------------------------------
-- Distance (A B) -> coord reelles et entieres
---------------------------------------------
Function Distance(A 	: in gen_io.point_type;
			B	: in gen_io.point_type_reel) return float is
dist	: float;
Begin  
	dist:=sqrt((float(A.coor_x)-B.coor_x)*(float(A.coor_x)-B.coor_x)+ (float(A.coor_y)-B.coor_y)*(float(A.coor_y)-B.coor_y));
  return dist;
End Distance;
-----------------------------------------------------------
-- Produit scalaire vect(A B) * vect(C D) -> coord entieres
-----------------------------------------------------------
Function Prodscal(A,B,C,D 	: in gen_io.point_type) return float is
ps	: float;
Begin
  ps:=float((B.coor_x-A.coor_x)*(D.coor_x-C.coor_x)+ (B.coor_y-A.coor_y)*(D.coor_y-C.coor_y));
  return ps;
End Prodscal;
----------------------------------------------------------
-- Produit scalaire vect(A B) * vect(C D) -> coord reelles
----------------------------------------------------------
Function Prodscal(A,B,C,D 	: in gen_io.point_type_reel) return float is
 ps	: float;
Begin
  ps:=(B.coor_x-A.coor_x)*(D.coor_x-C.coor_x)+  (B.coor_y-A.coor_y)*(D.coor_y-C.coor_y);
  return ps;
End Prodscal;
---------------------------------------------------------------------------
-- teste si l'angle a1 est strictement superieur a a2 (1 en sortie sinon 0)
-- les angles sont donnes en entree dans [-PI,PI] modulo 2PI et
-- leur difference n'excede jamais PI
-- Ex : -PI+e=PI+e donc -PI+e>PI-e, si e<PI/2.
---------------------------------------------------------------------------
Function Superieur_angle(a1,a2: in float) return integer is
i : integer;
Begin
  if ((a1>a2) and ((a1*a2>=0.0) or ((a2>-PI/2.0) and (a1<PI/2.0)))) or     ((a1<-PI/2.0) and (a2>PI/2.0)) then
	i:=1;
  else
	i:=0;
  end if;
  return i;
End Superieur_angle;
------------------------------------------------------------------------------
-- Calcule la difference d'angle entre a1 et a2 (= a1-a2) donnee dans ]-PI,PI]
-- les angles sont donnes en entree dans [-PI,PI] modulo 2PI et
-- leur difference absolue n'excede jamais PI-- Ex : -PI+e=PI+e donc (-PI+e)-(PI-e)=2*e si e<PI/2.
------------------------------------------------------------------------------
Function Difference_angle(a1,a2: in float) return float is
r : float;
Begin
  r:=a1-a2;
  if r>PI then
    r:=r-2.0*PI;
  elsif r<=-PI then 
     r:=r+2.0*PI;
  end if;
  return r;
End Difference_angle;
--------------------------------------------------------------------------
-- Calcule la valeur du secteur angulaire de [a1,a2] (~ a2-a1) donnee dans
-- ]0,2*PI], les angles sont donnes en entree dans [-PI,PI] modulo PI
-- Ex :[a2,a1]: [-PI+e,PI-e]=2*PI-2*e mais [PI-e,-PI+e]=2*e
--------------------------------------------------------------------------
Function Valeur_angle(a1,a2: in float) return float is
r : float;
Begin
  if a1<a2 then
     r:=a2-a1;
  else
     r:=a2-a1+2.0*PI;
  end if;
  return r;
End Valeur_angle;
-----------------------------------------------------------------------------
-- Determine si un angle appartient ou non a un secteur delimite par 2 angles
-- Les valeurs de ces 3 angles doivent etre dans un intervalle [k,k+2PI[
-----------------------------------------------------------------------------
Function Appartient(x,a1,a2: in float) return integer is
i : integer;
Begin
  i:=0;
  if a1<a2 then
    if (a1<x) and (x<a2) then
		i:=1;
	end if;
	elsif (a1<x) or (x<a2) then 
		i:=1;
  end if;
  return i;
End Appartient;
------------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord reelles
------------------------------------------------------------------------------
Function CoordCenCer(A,B,C : in gen_io.point_type_reel)	return gen_io.point_type_reel is 
x,y,xx,tan,tan1,tan2	: float;
CCC,I1,I2		: gen_io.point_type_reel;
Begin
  I1.coor_x:=(B.coor_x+A.coor_x)/2.0;
  I1.coor_y:=(B.coor_y+A.coor_y)/2.0;
  I2.coor_x:=(C.coor_x+B.coor_x)/2.0;
  I2.coor_y:=(C.coor_y+B.coor_y)/2.0;
  if (A.coor_x/=B.coor_x) and (C.coor_x/=B.coor_x) then
	tan1:=(B.coor_y-A.coor_y)/(B.coor_x-A.coor_x);
    tan2:=(C.coor_y-B.coor_y)/(C.coor_x-B.coor_x);
    if tan1/=tan2 then
		y:=(I2.coor_x-I1.coor_x+I2.coor_y*tan2-I1.coor_y*tan1)/(tan2-tan1);
		x:=I1.coor_x-(y-I1.coor_y)*tan1;
    else
		y:=(I2.coor_x-I1.coor_x+(I2.coor_y-I1.coor_y)*tan1)*1000.0;
		x:=y*tan1;
    end if;
  elsif A.coor_x/=B.coor_x then
	tan:=(B.coor_y-A.coor_y)/(B.coor_x-A.coor_x);
	y:=I2.coor_y;
	x:=I1.coor_x-tan*(y-I1.coor_y);
  elsif C.coor_x/=B.coor_x then
		tan:=(C.coor_y-B.coor_y)/(C.coor_x-B.coor_x);
		y:=I1.coor_y;
		x:=I2.coor_x-tan*(y-I2.coor_y);
	else
		x:=0.0;
		y:=0.0;
  end if;
  CCC.coor_x:=x;
   CCC.coor_y:=y;
  return CCC;
End CoordCenCer;
-------------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord entieres
-------------------------------------------------------------------------------
Function CoordCenCer(A,B,C : in gen_io.point_type)				 return gen_io.point_type_reel is
 x,y,xx,tan,tan1,tan2	: float;
 CCC,I1,I2		: gen_io.point_type_reel;
Begin
  I1.coor_x:=float(B.coor_x+A.coor_x)/2.0;
  I1.coor_y:=float(B.coor_y+A.coor_y)/2.0;
  I2.coor_x:=float(C.coor_x+B.coor_x)/2.0;
  I2.coor_y:=float(C.coor_y+B.coor_y)/2.0;
  if (A.coor_x/=B.coor_x) and (C.coor_x/=B.coor_x) then
	tan1:=float(B.coor_y-A.coor_y)/float(B.coor_x-A.coor_x);
    tan2:=float(C.coor_y-B.coor_y)/float(C.coor_x-B.coor_x);
    if tan1/=tan2 then
		y:=(I2.coor_x-I1.coor_x+I2.coor_y*tan2-I1.coor_y*tan1)/(tan2-tan1);
		x:=I1.coor_x-(y-I1.coor_y)*tan1;
    else      y:=(I2.coor_x-I1.coor_x+(I2.coor_y-I1.coor_y)*tan1)*1000.0;
		x:=y*tan1;
    end if;
  elsif A.coor_x/=B.coor_x then
	tan:=float(B.coor_y-A.coor_y)/float(B.coor_x-A.coor_x);
    y:=I2.coor_y;
    x:=I1.coor_x-tan*(y-I1.coor_y);
  elsif C.coor_x/=B.coor_x then
	tan:=float(C.coor_y-B.coor_y)/float(C.coor_x-B.coor_x);
    y:=I1.coor_y;
    x:=I2.coor_x-tan*(y-I2.coor_y);
  else
  x:=0.0;
  y:=0.0;
  end if;
  CCC.coor_x:=x;
  CCC.coor_y:=y;
  return CCC;
End CoordCenCer;
-----------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord mixtes
-----------------------------------------------------------------------------
Function CoordCenCer(A,B : in gen_io.point_type;
		     C   : in gen_io.point_type_reel)				 
			 return gen_io.point_type_reel is
 x,y,xx,tan,tan1,tan2	: float;
 CCC,I1,I2		: gen_io.point_type_reel;
Begin
  I1.coor_x:=float(B.coor_x+A.coor_x)/2.0;
  I1.coor_y:=float(B.coor_y+A.coor_y)/2.0;
  I2.coor_x:=(C.coor_x+float(B.coor_x))/2.0;
  I2.coor_y:=(C.coor_y+float(B.coor_y))/2.0;
  if (A.coor_x/=B.coor_x) and (C.coor_x/=float(B.coor_x)) then 
    tan1:=float(B.coor_y-A.coor_y)/float(B.coor_x-A.coor_x);
    tan2:=(C.coor_y-float(B.coor_y))/(C.coor_x-float(B.coor_x));
    if tan1/=tan2 then
    	y:=(I2.coor_x-I1.coor_x+I2.coor_y*tan2-I1.coor_y*tan1)/(tan2-tan1);
		x:=I1.coor_x-(y-I1.coor_y)*tan1;
    else
    	y:=(I2.coor_x-I1.coor_x+(I2.coor_y-I1.coor_y)*tan1)*1000.0;
        x:=y*tan1;
    end if;
  elsif A.coor_x/=B.coor_x then
    tan:=float(B.coor_y-A.coor_y)/float(B.coor_x-A.coor_x);
    y:=I2.coor_y;
    x:=I1.coor_x-tan*(y-I1.coor_y);
  elsif C.coor_x/=float(B.coor_x) then
    tan:=(C.coor_y-float(B.coor_y))/(C.coor_x-float(B.coor_x));
    y:=I1.coor_y;
    x:=I2.coor_x-tan*(y-I2.coor_y);
  else
   x:=0.0;
   y:=0.0;
  end if;
  CCC.coor_x:=x;
 CCC.coor_y:=y;
  return CCC;
End CoordCenCer;
-----------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord mixtes
-----------------------------------------------------------------------------
Function CoordCenCer(A   : in gen_io.point_type;
		     B,C : in gen_io.point_type_reel)				 
			 return gen_io.point_type_reel is 
x,y,xx,tan,tan1,tan2	: float;
 CCC,I1,I2		: gen_io.point_type_reel;
Begin
  I1.coor_x:=(B.coor_x+float(A.coor_x))/2.0;
  I1.coor_y:=(B.coor_y+float(A.coor_y))/2.0;
  I2.coor_x:=(C.coor_x+B.coor_x)/2.0;
  I2.coor_y:=(C.coor_y+B.coor_y)/2.0;
  if (float(A.coor_x)/=B.coor_x) and (C.coor_x/=B.coor_x) then
    tan1:=(B.coor_y-float(A.coor_y))/(B.coor_x-float(A.coor_x));
    tan2:=(C.coor_y-B.coor_y)/(C.coor_x-B.coor_x);
    if tan1/=tan2 then
    	y:=(I2.coor_x-I1.coor_x+I2.coor_y*tan2-I1.coor_y*tan1)/(tan2-tan1);
      x:=I1.coor_x-(y-I1.coor_y)*tan1;
    else 
      y:=(I2.coor_x-I1.coor_x+(I2.coor_y-I1.coor_y)*tan1)*1000.0;
      x:=y*tan1;
    end if;
  elsif float(A.coor_x)/=B.coor_x then
    tan:=(B.coor_y-float(A.coor_y))/(B.coor_x-float(A.coor_x));
    y:=I2.coor_y;
    x:=I1.coor_x-tan*(y-I1.coor_y);
  elsif C.coor_x/=B.coor_x then
    tan:=(C.coor_y-B.coor_y)/(C.coor_x-B.coor_x);
    y:=I1.coor_y;
    x:=I2.coor_x-tan*(y-I2.coor_y);
  else
    x:=0.0;
    y:=0.0;
  end if;
  CCC.coor_x:=x;
 CCC.coor_y:=y;
  return CCC;
End CoordCenCer;
-----------------------------------------
-- Vecteur (M(i)-M(i-1)) : ligne en reels
-----------------------------------------
Function Vecteur(	ligne_w	 : in point_liste_reel;
			AL	 : in reel_tableau_type;
			i	 : in integer;
			n_points : in natural) return point_type_reel is
j,fin	: natural;
 vect	: point_type_reel;
Begin
  fin:=0;
  for j in 1..n_points loop
	if AL(i-j)=0.0 then
		vect.coor_x:=ligne_w(i).coor_x-ligne_w(i-j).coor_x;
		vect.coor_y:=ligne_w(i).coor_y-ligne_w(i-j).coor_y;
		fin:=1;
    end if;
    exit when fin=1;
  end loop;
  return vect;
End Vecteur;
-------------------------------------------
-- Vecteur (M(i)-M(i-1)) : ligne en entiers
-------------------------------------------
Function Vecteur(	ligne_w	 : in point_liste_type;
			AL	 : in reel_tableau_type;
			i	 : in integer;
			n_points : in natural) return point_type_reel is
j,fin	: natural;
 vect	: point_type_reel;
Begin
  fin:=0;
  for j in 1..n_points loop
		if AL(i-j)=0.0 then
			vect.coor_x:=float(ligne_w(i).coor_x-ligne_w(i-j).coor_x);
			vect.coor_y:=float(ligne_w(i).coor_y-ligne_w(i-j).coor_y);
		fin:=1;
		end if;
		exit when fin=1;
  end loop;
  return vect;
End Vecteur;
------------------------
-- Vecteur (M(i)+M(i-1))
------------------------
Function Somme(	ligne_w	 : in point_liste_reel;
		AL	 : in reel_tableau_type;
		i	 : in integer;
		n_points : in natural) return point_type_reel is
j,fin	: natural;
 som	: point_type_reel;
Begin
  fin:=0;
  for j in 1..n_points loop
		if AL(i-j)=0.0 then
			som.coor_x:=ligne_w(i).coor_x+ligne_w(i-j).coor_x;
			som.coor_y:=ligne_w(i).coor_y+ligne_w(i-j).coor_y;
			fin:=1;
		end if;
    exit when fin=1;
  end loop;
  return som;
End Somme;
------------------------------------------------------------
-- Remplissage du tableau des points alignes : coord reelles
------------------------------------------------------------
Function Aligne(ligne_w	 : in point_liste_reel;
		n_points : in natural) return reel_tableau_type is 
AL	: reel_tableau_type(1..2000);
 i	: integer;
 det	: float;
Begin
  for i in 1..n_points loop
	AL(i):=0.0;
  end loop;
  for i in 2..n_points-1 loop
	det:=Vecteur(ligne_w,AL,i,n_points).coor_y*	 Vecteur(ligne_w,AL,i+1,n_points).coor_x-	 Vecteur(ligne_w,AL,i,n_points).coor_x*	 Vecteur(ligne_w,AL,i+1,n_points).coor_y;
    if abs(det)<1.0E-05 then 
	   AL(i):=1.0;
	end if;
  end loop;
  return AL;
End Aligne;
-------------------------------------------------------------
-- Remplissage du tableau des points alignes : coord entieres
-------------------------------------------------------------
Function Aligne(ligne_w	 : in point_liste_type;
		n_points : in natural) return reel_tableau_type is 
AL	: reel_tableau_type(1..2000);
 i	: integer;
 det	: float;
Begin
  for i in 1..n_points loop
		AL(i):=0.0;
  end loop;
  for i in 2..n_points-1 loop
		det:=Vecteur(ligne_w,AL,i,n_points).coor_y*	 Vecteur(ligne_w,AL,i+1,n_points).coor_x-	 Vecteur(ligne_w,AL,i,n_points).coor_x*	 Vecteur(ligne_w,AL,i+1,n_points).coor_y;
		if abs(det)<1.0E-05 then
			AL(i):=1.0;
		end if;
  end loop;
  return AL;
End Aligne;
-------------------------------------------------
-- Calcul des coordonnees des centres de courbure
-------------------------------------------------
Function CoorCenCourb(ligne_w    : in point_liste_reel;
		      n_points : in natural) return point_liste_reel is
i	: integer;
 CC	: point_liste_reel(1..2000);
 AL	: reel_tableau_type(1..2000);
Begin
  AL:=Aligne(ligne_w,n_points);
  for i in 2..n_points-1 loop
	if AL(i)=0.0 then
		if Vecteur(ligne_w,AL,i,n_points).coor_x=0.0 then
			CC(i).coor_y:=Somme(ligne_w,AL,i,n_points).coor_y/2.0;
			CC(i).coor_x:=(Vecteur(ligne_w,AL,i+1,n_points).coor_y/
			Vecteur(ligne_w,AL,i+1,n_points).coor_x*
			(Somme(ligne_w,AL,i+1,n_points).coor_y-
			Somme(ligne_w,AL,i,n_points).coor_y)+
			Somme(ligne_w,AL,i+1,n_points).coor_x)/2.0;
      elsif Vecteur(ligne_w,AL,i+1,n_points).coor_x=0.0 then
			CC(i).coor_y:=Somme(ligne_w,AL,i+1,n_points).coor_y/2.0;
			CC(i).coor_x:=(Vecteur(ligne_w,AL,i,n_points).coor_y/
			Vecteur(ligne_w,AL,i,n_points).coor_x*
			(Somme(ligne_w,AL,i,n_points).coor_y-
			Somme(ligne_w,AL,i+1,n_points).coor_y)+
			Somme(ligne_w,AL,i,n_points).coor_x)/2.0;
      else
			CC(i).coor_y:=((Vecteur(ligne_w,AL,i,n_points).coor_y/
			Vecteur(ligne_w,AL,i,n_points).coor_x*
			Somme(ligne_w,AL,i,n_points).coor_y)+
			Somme(ligne_w,AL,i,n_points).coor_x-
			(Vecteur(ligne_w,AL,i+1,n_points).coor_y/
			Vecteur(ligne_w,AL,i+1,n_points).coor_x*
			Somme(ligne_w,AL,i+1,n_points).coor_y)-
			Somme(ligne_w,AL,i+1,n_points).coor_x)/
			((Vecteur(ligne_w,AL,i,n_points).coor_y/
			Vecteur(ligne_w,AL,i,n_points).coor_x)-
			(Vecteur(ligne_w,AL,i+1,n_points).coor_y/
			Vecteur(ligne_w,AL,i+1,n_points).coor_x))/2.0;
			CC(i).coor_x:=((Vecteur(ligne_w,AL,i,n_points).coor_y/
			Vecteur(ligne_w,AL,i,n_points).coor_x)*
			(Somme(ligne_w,AL,i,n_points).coor_y-(2.0*CC(i).coor_y))+
			Somme(ligne_w,AL,i,n_points).coor_x)/2.0;
      end if;
    end if;
  end loop;
  CC(1):=CC(2);
 CC(n_points):=CC(n_points-1);
  return CC;
End CoorCenCourb;
----------------------------
-- Orientation de la normale
----------------------------
Procedure Orient_norm(ligne_w    : in point_liste_reel;
	 	      n_points : in natural;
		      theta    : in out reel_tableau_type;
		      ray      : out point_liste_reel) is
 i	: integer;
 CC	: point_liste_reel(1..2000);
 dx,dy	: float;
Begin
  CC:=CoorCenCourb(ligne_w,n_points);
  for i in 1..n_points loop
	ray(i).coor_x:=CC(i).coor_x-ligne_w(i).coor_x;
    ray(i).coor_y:=CC(i).coor_y-ligne_w(i).coor_y;
  end loop;
  for i in 1..n_points loop
	dy:=CC(i).coor_y-ligne_w(i).coor_y;
    dx:=CC(i).coor_x-ligne_w(i).coor_x;
    if abs(dx)<1.0E-05 then
		if dy<0.0 then
			theta(i):=PI/2.0;
		else
			theta(i):=1.5*PI;
		end if;
    else
		theta(i):=Arctan(dy/dx);
      if dx>0.0 then
		if dy<0.0 then
			if theta(i)<0.0 then
				theta(i):=theta(i)+2.0*PI;
			end if;
		end if;
      else
		if dy<0.0 then
			if theta(i)>0.0 then
				theta(i):=theta(i)+PI;
			end if;
		else
			if theta(i)<0.0 then
				theta(i):=theta(i)+PI;
			end if;
		end if;
      end if;
    end if;
  end loop;
End Orient_norm;
---------------------------------------------------------
-- Calcul aire algebrique comprise entre les deux courbes
---------------------------------------------------------
Function Aire_alg( ligne	: in point_liste_type;
		   n		: in natural;
		   ligne_lowe	: in point_liste_type;
		   n_lowe	: in natural) return float is
 a,aire			: float;
 i			: natural;
Begin
  a:=0.0;
  for i in 1..n-1 loop
	a:=a+0.5*float(ligne(i+1).coor_X-ligne(i).coor_X)* float(ligne(i+1).coor_Y+ligne(i).coor_Y);
  end loop;
  for i in 1..n_lowe-1 loop
		a:=a+0.5*float(ligne_lowe(n_lowe-i).coor_X-	ligne_lowe(n_lowe+1-i).coor_X)*	float(ligne_lowe(n_lowe-i).coor_Y+ 
		ligne_lowe(n_lowe+1-i).coor_Y);
  end loop;
  aire:=a;
  return aire;
End Aire_alg;
-------------------------------------
-- Calcul du maximum entre deux reels
-------------------------------------
Function Max (a,b : in float) return float is
 m	: float;
Begin
  if a<=b then
	m:=b;
 else
	m:=a;
 end if;
  return m;
End Max;
-------------------------------------
-- Calcul du minimum entre deux reels
-------------------------------------
Function Min (a,b : in float) return float is
 m	: float;
Begin
  if a<=b then
	m:=a;
  else
	m:=b;
 end if;
  return m;
End Min;
---------------------------------------------------------------
-- Calcul du lieu d'un point A par rapport a une droite (BC)
-- lieu>0 <=> le point est en dessous ou au dessus de la droite
-- lieu<0 <=> contraire de lieu>0
-- lieu=0 <=> le point est situe sur la droite
---------------------------------------------------------------
Function Lieu (A,B,C : in gen_io.point_type_reel) return float is
 l	: float;
Begin
  l:=(B.coor_X-A.coor_X)*(C.coor_Y-A.coor_Y)- (B.coor_Y-A.coor_Y)*(C.coor_X-A.coor_X);
  return l;
End Lieu;
---------------------------------------------------------------
-- Calcul du lieu d'un point A par rapport a une droite (BC)
-- lieu>0 <=> le point est en dessous ou au dessus de la droite
-- lieu<0 <=> contraire de lieu>0
-- lieu=0 <=> le point est situe sur la droite
---------------------------------------------------------------
Function Lieu (A,B,C : in gen_io.point_type) return float is
 l	: float;
Begin
  l:=float((B.coor_X-A.coor_X)*(C.coor_Y-A.coor_Y)- (B.coor_Y-A.coor_Y)*(C.coor_X-A.coor_X));
  return l;
End Lieu;
---------------------------------------------------------------
-- Calcul du lieu d'un point A par rapport a une droite (BC)
-- lieu>0 <=> le point est en dessous ou au dessus de la droite
-- lieu<0 <=> contraire de lieu>0
-- lieu=0 <=> le point est situe sur la droite
---------------------------------------------------------------
Function Lieu ( A   : in gen_io.point_type_reel;
		B,C : in gen_io.point_type) return float is 
l	: float;
Begin
  l:=((float(B.coor_X)-A.coor_X)*(float(C.coor_Y)-A.coor_Y))- ((float(B.coor_Y)-A.coor_Y)*(float(C.coor_X)-A.coor_X));
  return l;
End Lieu;
----------------------------------------------------------------------
-- Renvoie 1 ssi les points A et B sont du meme cote de la droite (CD)
-- 	   0 ssi un des points est situe sur la droite
--	  -1 ssi les points ne sont pas du meme cote de la droite
----------------------------------------------------------------------
Function Meme_cote (A,B,C,D   : in gen_io.point_type) return integer is
 l	: float;
 i	: integer;
Begin
  l:=Geomxav.Lieu(A,C,D)*Geomxav.Lieu(B,C,D);
  if l>0.0 then
	i:=1;
  elsif l=0.0 then
	i:=0;
  else
	i:=-1;
  end if;
  return i;
End Meme_cote;
----------------------------------------------------------------------
-- Renvoie 1 ssi les points A et B sont du meme cote de la droite (CD)
-- 	   0 ssi un des points est situe sur la droite
--	  -1 ssi les points ne sont pas du meme cote de la droite
----------------------------------------------------------------------
Function Meme_cote (A,B   : in gen_io.point_type_reel;
		    C,D   : in gen_io.point_type) return integer is
 l	: float;
 i	: integer;
Begin
  l:=Geomxav.Lieu(A,C,D)*Geomxav.Lieu(B,C,D);
  if l>0.0 then
	i:=1;
	elsif l=0.0 then
		i:=0;
	else
		i:=-1;
 end if;
  return i;
End Meme_cote;
--------------------------------------------------------------------------------
-- Calcul de la pente (tangente de l'angle) d'une droite passant par deux points
--------------------------------------------------------------------------------
Function Pente (A,B : in gen_io.point_type) return float is
 p	: float;
Begin
  if B.coor_X/=A.coor_X then
    p:=float(B.coor_Y-A.coor_Y)/float(B.coor_X-A.coor_X);
  else
	p:=0.0;
  end if;
  return p;
End Pente;
----------------- Idem en reel---------------
Function Pente (A,B : in gen_io.point_type_reel) return float is
 p	: float;
Begin
  if B.coor_X/=A.coor_X then
	p:=(B.coor_Y-A.coor_Y)/(B.coor_X-A.coor_X);
  else
	p:=0.0;
  end if;
  return p;
End Pente;
---------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees entieres)
-- dans [0,PI]
---------------------------------------------------------------------
	Function Angle_3pts	(A,B,C : in gen_io.point_type) return float is 
ang,av,ap,angle	: float;
Begin
  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=abs(av-ap);
 if ang>PI then
	ang:=2.0*PI-ang;
 end if;
  if abs(ang-PI)>1.0E-02 then
  angle:=ang;
 else
	angle:=PI;
 end if;
 return angle;
End Angle_3pts;
--------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees reelles)
-- dans [0,PI] (=> ordre indifferent)
--------------------------------------------------------------------
Function Angle_3pts	(A,B,C : in gen_io.point_type_reel) return float is
 ang,av,ap,angle	: float;
Begin
  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=abs(av-ap);
 if ang>PI then
	ang:=2.0*PI-ang;
 end if;
  if abs(ang-PI)>1.0E-02 then
	angle:=ang;
  else
		angle:=PI;
  end if;
  return angle;
End Angle_3pts;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [0,PI] (=> ordre indifferent)
-------------------------------------------------------------------
Function Angle_3pts    (A   : in gen_io.point_type_reel;
			B,C : in gen_io.point_type) return float is
ang,av,ap,angle	: float;
Begin
  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=abs(av-ap);
 if ang>PI then 
	ang:=2.0*PI-ang;
 end if;
 if abs(ang-PI)>1.0E-02 then 
	angle:=ang;
 else
	angle:=PI;
 end if;
  return angle;
End Angle_3pts;
---------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees entieres)
-- dans [-PI,PI]
---------------------------------------------------------------------
Function Angle_3pts_alg	(A,B,C : in gen_io.point_type) return float is
 ang,av,ap,angle	: float;
Begin  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=ap-av;
  if ang>PI then ang:=ang-2.0*PI;
  elsif ang<-PI then ang:=2.0*PI+ang;
  end if;
  if abs(abs(ang)-PI)>1.0E-02 then angle:=ang;
 else angle:=PI;
 end if;
  return angle;
End Angle_3pts_alg;
--------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees reelles)
-- dans [-PI,PI]
--------------------------------------------------------------------
Function Angle_3pts_alg	(A,B,C : in gen_io.point_type_reel) return float is
 ang,av,ap,angle	: float;
Begin  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=ap-av;
  if ang>PI then ang:=ang-2.0*PI;
  elsif ang<-PI then ang:=2.0*PI+ang;
  end if;
  if abs(abs(ang)-PI)>1.0E-02 then angle:=ang;
 else angle:=PI;
 end if;
  return angle;
End Angle_3pts_alg;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A : in gen_io.point_type_reel;
			B : in gen_io.point_type;
			C : in gen_io.point_type_reel) return float is
ang,av,ap,angle	: float;
Begin
  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=ap-av;
  if ang>PI then ang:=ang-2.0*PI;
  elsif ang<-PI then ang:=2.0*PI+ang;
  end if;
  if abs(abs(ang)-PI)>1.0E-02 then angle:=ang;
 else angle:=PI;
 end if;
  return angle;
End Angle_3pts_alg;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A   : in gen_io.point_type_reel;
			B,C : in gen_io.point_type) return float is 
ang,av,ap,angle	: float;
Begin  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=ap-av;
  if ang>PI then ang:=ang-2.0*PI;
  elsif ang<-PI then ang:=2.0*PI+ang;
  end if;
  if abs(abs(ang)-PI)>1.0E-02 then angle:=ang;
 else angle:=PI;
 end if;
  return angle;
End Angle_3pts_alg;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A,B : in gen_io.point_type;
			C   : in gen_io.point_type_reel) return float is
ang,av,ap,angle	: float;
Begin  av:=Angpente(B.coor_X,B.coor_Y,A.coor_X,A.coor_Y);
  ap:=Angpente(B.coor_X,B.coor_Y,C.coor_X,C.coor_Y);
  ang:=ap-av;
  if ang>PI then ang:=ang-2.0*PI;
  elsif ang<-PI then ang:=2.0*PI+ang;
  end if;
  if abs(abs(ang)-PI)>1.0E-02 then angle:=ang;
 else angle:=PI;
 end if;
  return angle;
End Angle_3pts_alg;
---------------------------------------------------------------------------
-- Calcul de l'intersection entre deux segments [AB] et [CD]
-- Retourne doublet de points (segment intersection) :
-- 1) 2 points egaux => intersection non vide et differentes des extremites
-- 2) [PT1,PT1] => intersection = 2eme point de chaque segment
-- 3) [PT0,PT0] => intersection vide
-- 4) [M,PT1] => le point M appartient a l'autre segment
---------------------------------------------------------------------------
Function Inters( A,B,C,D : in gen_io.point_type) return point_liste_type is
 p1,p2,r1,r2,rl1,rl2		: float;
 PT1,PT0			: gen_io.point_type;
 Int				: point_liste_type(1..2);
Begin  PT1.coor_X:=-1;
 PT1.coor_Y:=-1;
 PT0.coor_X:=0;
 PT0.coor_Y:=0;
  r1:=lieu(C,D,A);
	-- Place (+ ou -) de A par rapport a la droite (CD)
	r2:=lieu(C,D,B);
	-- Place (+ ou -) de B par rapport a la droite (CD)
	rl1:=lieu(A,B,C);
	-- Place (+ ou -) de C par rapport a la droite (AB)
	rl2:=lieu(A,B,D);
	-- Place (+ ou -) de D par rapport a la droite (AB)
	if (r1*r2<0.0) and (rl1*rl2<0.0) then	
	-- inters. non vide et <> des extremites
		if B.coor_X=A.coor_X then
		-- 1) xA=xB => xI=xA
		p2:=pente(C,D);
		Int(1).coor_X:=A.coor_X;
      Int(1).coor_Y:=C.coor_Y+integer(p2*float(A.coor_X-C.coor_X));
      Int(2):=Int(1);
    elsif C.coor_X=D.coor_X then
	-- 2) xC=xD => xI=xC
		p1:=pente(A,B);
		Int(1).coor_X:=C.coor_X;
      Int(1).coor_Y:=A.coor_Y+integer(p1*float(C.coor_X-A.coor_X));
      Int(2):=Int(1);
    else					
	-- 3) autres cas
	p1:=pente(A,B);
	p2:=pente(C,D);
      Int(1).coor_X:=integer((float(C.coor_Y)-p2*float(C.coor_X)-  float(A.coor_Y)+p1*float(A.coor_X))/(p1-p2));
      Int(1).coor_Y:=integer((p1*(float(C.coor_Y)-p2*float(C.coor_X))-   p2*(float(A.coor_Y)-p1*float(A.coor_X)))/(p1-p2));
      Int(2):=Int(1);
    end if;
  elsif (r2=0.0) and (rl2=0.0) and (r1=0.0) then 
  -- les 4 points sont alignes
  if B=D then					 -- 1) extremites confondues
        Int(1):=PT1;
 Int(2):=PT1;
    elsif (((C.coor_X-B.coor_X)*(D.coor_X-B.coor_X)<0) or 
	-- 2) B dans [CD]	
	((C.coor_Y-B.coor_Y)*(D.coor_Y-B.coor_Y)<0)) and (((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or 
	--  et D dans [AB]
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0)) then 
		Int(1):=D;
		Int(2):=B;
		-- resultat = [D,B] 
		elsif ((C.coor_X-B.coor_X)*(D.coor_X-B.coor_X)<0) or 
		-- 3) B dans [CD]
		((C.coor_Y-B.coor_Y)*(D.coor_Y-B.coor_Y)<0) then
		Int(2):=B;
 Int(1):=PT1;
    elsif ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or 
	-- 4) D dans [AB]
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0) then
	Int(1):=D;
 Int(2):=PT1;
    else						
	-- 5) autre cas
	Int(1):=PT0;
	Int(2):=PT0;
	-- points alignes et intersection vide
    end if;
  elsif (r2=0.0) and (rl2=0.0) then	
  --  B et D confondus et A pas sur (CD)
  Int(1):=PT1;
 Int(2):=PT1;
  elsif (r2=0.0) and (rl1*rl2<0.0) then
  -- B entre C et D 
  Int(2):=B;
 Int(1):=PT1;
  elsif (r1*r2<0.0) and (rl2=0.0) then	
  -- D entre A et B
  Int(1):=D;
 Int(2):=PT1;
  else
  -- autres cas --> intersection vide
  Int(1):=PT0;
 Int(2):=PT0;
  end if;
  return Int;
End Inters;
-----------------------------------------------------------------
-- Calcul de l'intersection entre deux segments [AB] et [CD]
-- Retourne le point intersection = PINT
-- Retourne ali=4 si les 4 points sont alignes
-- Retourne ali=3 si 3 points sont alignes
-- Retourne ali=2 si pas de points alignes
-- Retourne typ=0 si intersection = vide
-- Retourne typ=-1 si intersection vide mais droite coupe un segment
-- Retourne typ=-2 si intersection vide et droites paralleles (trapeze)
-- Retourne typ=-3 si intersection vide et droites paralleles (parallelogramme)
-- Retourne typ=1 si intersection = 1 point different des 4 points
-- Retourne typ=2 si intersection = 1 point des 4 points (tous differents)
-- Retourne typ=3 si intersection = 1 point (2 points sur les 4 sont confondus)
-- Retourne typ=4 si intersection = segment sans inclusion
-- Retourne typ=5 si intersection = segment avec inclusion (points differents)
-- Retourne typ=6 si intersection = segment avec inclusion (2 points egaux)
-- Retourne typ=7 si intersection = segment avec confusion
-----------------------------------------------------------------
Procedure Inters_2seg (A,B,C,D : in gen_io.point_type;
		       ali,typ : out integer;
		       PINT    : out gen_io.point_type) is 
 p1,p2,r1,r2,rl1,rl2	: float;
 Int			: gen_io.point_type;
Begin  r1:=lieu(C,D,A);
	-- Place (+ ou -) de A par rapport a la droite (CD)
	r2:=lieu(C,D,B);
	-- Place (+ ou -) de B par rapport a la droite (CD)
	rl1:=lieu(A,B,C);
	-- Place (+ ou -) de C par rapport a la droite (AB)
	rl2:=lieu(A,B,D);
	-- Place (+ ou -) de D par rapport a la droite (AB)
	if (r1*r2<0.0) and (rl1*rl2<0.0) then	
	-- A) int. non vide et <> des extremites
    ali:=2;
	typ:=1;
    if B.coor_X=A.coor_X then			
	-- 1) xA=xB => xI=xA  
    p2:=pente(C,D);
 Int.coor_X:=A.coor_X;
      Int.coor_Y:=C.coor_Y+integer(p2*float(A.coor_X-C.coor_X));
    elsif C.coor_X=D.coor_X then		
	-- 2) xC=xD => xI=xC
	p1:=pente(A,B);
 Int.coor_X:=C.coor_X;
      Int.coor_Y:=A.coor_Y+integer(p1*float(C.coor_X-A.coor_X));
    else
	-- 3) autres cas 
	     p1:=pente(A,B);
 p2:=pente(C,D);
      Int.coor_X:=integer((float(C.coor_Y)-p2*float(C.coor_X)-			  float(A.coor_Y)+p1*float(A.coor_X))/(p1-p2));
      Int.coor_Y:=integer((p1*(float(C.coor_Y)-p2*float(C.coor_X))-			   p2*(float(A.coor_Y)-p1*float(A.coor_X)))/(p1-p2));
    end if;
    PINT:=int;
  elsif (r2=0.0) and (rl2=0.0) and (r1=0.0) then 
  -- B) les 4 points sont alignes
  ali:=4;
    if B=D then					 
	-- 1) extremites confondues 
	if ((C.coor_X-B.coor_X)*(A.coor_X-B.coor_X)<0) or 
	-- 11) B dans [AC]
	((C.coor_Y-B.coor_Y)*(A.coor_Y-B.coor_Y)<0) then PINT:=B;
 typ:=3;
      elsif A=C then PINT:=A;
 typ:=7;
	-- 12) 2 segments confondus
	else PINT:=B;
 typ:=6;
	-- 13) segment inclus
	end if;
    elsif A=C then		
	-- 2) points initiaux confondus
	if ((D.coor_X-A.coor_X)*(B.coor_X-A.coor_X)<0) or 
	-- 21) A dans [BD]
	((D.coor_Y-A.coor_Y)*(B.coor_Y-A.coor_Y)<0) then PINT:=A;
 typ:=3;
      else PINT:=A;
 typ:=6;
	-- 22) segment inclus
	end if;
    elsif ((C.coor_X-B.coor_X)*(D.coor_X-B.coor_X)<0) or 
	-- 3) B dans [CD]
	((C.coor_Y-B.coor_Y)*(D.coor_Y-B.coor_Y)<0) then
	if ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or  
	--  31) D dans [AB]	
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0) or    	 ((C.coor_X-A.coor_X)*(C.coor_X-B.coor_X)<0) or   
	--  ou C dans [AB]	 
	((C.coor_Y-A.coor_Y)*(C.coor_Y-B.coor_Y)<0) then
	PINT:=B;
 typ:=4;
	-- intersection = segment (pas d'inclusion)
	else PINT:=A;
 typ:=5;
	-- 32) inclusion de [A,B] dans [C,D]
	end if;
    elsif ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or 
	-- 4) D dans [AB]	
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0) then
	if ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or
	--  41) A dans [CD]
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0) then PINT:=A;
 typ:=4;
	-- intersection = segment (pas d'inclusion)
	else PINT:=C;
 typ:=5;
	-- 42) inclusion de [C,D] dans [A,B]
	end if;
    elsif ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0) or 
	--  5) A dans [CD]
	((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0) then	PINT:=A;
 typ:=4;
	-- intersection = segment (pas d'inclusion)
    else typ:=0;
					
	-- 6) autre cas
    end if;
  elsif (r2=0.0) and (rl2=0.0) then
  -- C) B et D confondus et A pas sur (CD)
  PINT:=B;
 ali:=3;
 typ:=3;
  elsif (r1=0.0) and (rl1=0.0) then	
  -- D) A et C confondus et B pas sur (CD)
  PINT:=A;
 ali:=3;
 typ:=3;
  elsif (r1=0.0) and (rl1*rl2<0.0) then	
  -- E) A entre C et D
  PINT:=A;
 ali:=3;
 typ:=2;
  elsif (r2=0.0) and (rl1*rl2<0.0) then	
  -- F) B entre C et D    
  PINT:=B;
 ali:=3;
 typ:=2;
  elsif (r1*r2<0.0) and (rl2=0.0) then	
  -- G) D entre A et B
  PINT:=D;
 ali:=3;
 typ:=2;
  elsif (r1*r2<0.0) and (rl1=0.0) then
  -- H) C entre A et B
  PINT:=C;
 ali:=3;
 typ:=2;
  elsif (r1=0.0) or (r2=0.0) or (rl1=0.0) or (rl2=0.0) then
  ali:=3;
 typ:=0;
 -- I) 3 points alignes mais intersection vide
 elsif (r1*r2<0.0) or (rl1*rl2<0.0) then
 ali:=2;
 typ:=-1;
 -- J) int. vide mais une droite coupe un segment
 elsif (B.coor_X-A.coor_X)*(D.coor_Y-C.coor_Y)=	(B.coor_Y-A.coor_Y)*(D.coor_X-C.coor_X) then 
 -- K) droites paralleles
 ali:=2;
    if Distance(A,B)=Distance(C,D) then typ:=-3;
 -- 1) parallelogramme
 else typ:=-2;
 -- 2) trapeze
 end if;
  else typ:=0;
 ali:=2;
	-- L) autres cas --> intersection vide
	end if;
End Inters_2seg;
-----------------------------------------------------------------
-- Calcul de l'intersection entre deux segments [AB] et [CD]
-- Retourne le point intersection = PINT
-- Retourne ali=4 si les 4 points sont alignes
-- Retourne ali=3 si 3 points sont alignes
-- Retourne ali=2 si pas de points alignes
-- Retourne typ=0 si intersection = vide
-- Retourne typ=-1 si intersection vide mais droite coupe un segment
-- Retourne typ=-2 si intersection vide et droites paralleles (trapeze)
-- Retourne typ=-3 si intersection vide et droites paralleles (parallelogramme)
-- Retourne typ=1 si intersection = 1 point different des 4 points
-- Retourne typ=2 si intersection = 1 point des 4 points (tous differents)
-- Retourne typ=3 si intersection = 1 point (2 points sur les 4 sont confondus)
-- Retourne typ=4 si intersection = segment sans inclusion
-- Retourne typ=5 si intersection = segment avec inclusion (points differents)
-- Retourne typ=6 si intersection = segment avec inclusion (2 points egaux)
-- Retourne typ=7 si intersection = segment avec confusion
-----------------------------------------------------------------

Procedure Inters_2seg (A,B,C,D : in gen_io.point_type_reel;
		       ali,typ : out integer;
		       PINT    : out gen_io.point_type_reel) is p1,p2,r1,r2,rl1,rl2	: float;
				Int		: gen_io.point_type_reel;
Begin  
	r1:=lieu(C,D,A);
	-- Place (+ ou -) de A par rapport a la droite (CD)  
	r2:=lieu(C,D,B);
	-- Place (+ ou -) de B par rapport a la droite (CD)  
	rl1:=lieu(A,B,C);
	-- Place (+ ou -) de C par rapport a la droite (AB)  
	rl2:=lieu(A,B,D);
	-- Place (+ ou -) de D par rapport a la droite (AB)
	if (r1*r2<0.0) and (rl1*rl2<0.0) then
		-- A) int. non vide et <> des extremites
		ali:=2;
		typ:=1;
		if B.coor_X=A.coor_X then
			-- 1) xA=xB => xI=xA
			p2:=pente(C,D);
			Int.coor_X:=A.coor_X;
			Int.coor_Y:=C.coor_Y+p2*(A.coor_X-C.coor_X);
		elsif C.coor_X=D.coor_X then
			-- 2) xC=xD => xI=xC
			p1:=pente(A,B);
			Int.coor_X:=C.coor_X;
			Int.coor_Y:=A.coor_Y+p1*(C.coor_X-A.coor_X);
		else					
			-- 3) autres cas
			p1:=pente(A,B);
			p2:=pente(C,D);
			Int.coor_X:=(C.coor_Y-p2*C.coor_X-A.coor_Y+p1*A.coor_X)/(p1-p2);
			Int.coor_Y:=(p1*(C.coor_Y-p2*C.coor_X)-p2*(A.coor_Y-p1*A.coor_X))/(p1-p2);
		end if;
		PINT:=int;
	elsif (r2=0.0) and (rl2=0.0) and (r1=0.0) then 
	-- B) les 4 points sont alignes
		ali:=4;
		if B=D then	
			-- 1) extremites confondues
			if ((C.coor_X-B.coor_X)*(A.coor_X-B.coor_X)<0.0) or
			-- 11) B dans [AC]
				((C.coor_Y-B.coor_Y)*(A.coor_Y-B.coor_Y)<0.0) then
				PINT:=B;
				typ:=3;
			elsif A=C then
				PINT:=A;
				typ:=7;
				-- 12) 2 segments confondus
			else
				PINT:=B;
				typ:=6;
				-- 13) segment inclus
			end if;
		elsif A=C then		
			-- 2) points initiaux confondus
			if ((D.coor_X-A.coor_X)*(B.coor_X-A.coor_X)<0.0) or
			-- 21) A dans [BD]
				((D.coor_Y-A.coor_Y)*(B.coor_Y-A.coor_Y)<0.0) then 
				PINT:=A;
				typ:=3;
			else PINT:=A;
				typ:=6;
				-- 22) segment inclus
		end if;
	elsif
		((C.coor_X-B.coor_X)*(D.coor_X-B.coor_X)<0.0)
		or -- 3) B dans [CD]
		((C.coor_Y-B.coor_Y)*(D.coor_Y-B.coor_Y)<0.0) then
		if ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0.0) or 
			--  31) D dans [AB]
			((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0.0) or
			((C.coor_X-A.coor_X)*(C.coor_X-B.coor_X)<0.0) or 
			--  ou C dans [AB]	
			((C.coor_Y-A.coor_Y)*(C.coor_Y-B.coor_Y)<0.0) then
			PINT:=B;
			typ:=4;
			-- intersection = segment (pas d'inclusion)
		else 
			PINT:=A;
			typ:=5;
			-- 32) inclusion de [A,B] dans [C,D]
		end if;
    elsif
		((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0.0) or 
		-- 4) D dans [AB]
		((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0.0) then
		if ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0.0) or  
			--  41) A dans [CD]
			((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0.0) then
			PINT:=A;
			typ:=4;
			-- intersection = segment (pas d'inclusion)
		else PINT:=C;
			typ:=5;
			-- 42) inclusion de [C,D] dans [A,B]
		end if;
		elsif ((D.coor_X-A.coor_X)*(D.coor_X-B.coor_X)<0.0) or  
				--  5) A dans [CD]
				((D.coor_Y-A.coor_Y)*(D.coor_Y-B.coor_Y)<0.0) then
				PINT:=A;
				typ:=4;
				-- intersection = segment (pas d'inclusion)
				else typ:=0;
					-- 6) autre cas
				end if;
				elsif (r2=0.0) and (rl2=0.0) then
					-- C) B et D confondus et A pas sur (CD)
					PINT:=B;
					ali:=3;
					typ:=3;
				elsif (r1=0.0) and (rl1=0.0) then
				-- D) A et C confondus et B pas sur (CD)
				PINT:=A;
				ali:=3;
				typ:=3;
				elsif (r1=0.0) and (rl1*rl2<0.0) then
				-- E) A entre C et D
				PINT:=A;
				ali:=3;
				typ:=2;
				elsif (r2=0.0) and (rl1*rl2<0.0) then
				-- F) B entre C et D
				PINT:=B;
				ali:=3;
				typ:=2;
				elsif (r1*r2<0.0) and (rl2=0.0) then
				-- G) D entre A et B
				PINT:=D;
				ali:=3;
				typ:=2;
				elsif (r1*r2<0.0) and (rl1=0.0) then
				-- H) C entre A et B
				PINT:=C;
				ali:=3;
				typ:=2;
				elsif (r1=0.0) or (r2=0.0) or (rl1=0.0) or (rl2=0.0) then
				ali:=3;
				typ:=0;
				-- I) 3 points alignes mais intersection vide
				elsif (r1*r2<0.0) or (rl1*rl2<0.0) then
				ali:=2;
				typ:=-1;
				-- J) int. vide mais une droite coupe un segment
				elsif (B.coor_X-A.coor_X)*(D.coor_Y-C.coor_Y)=	(B.coor_Y-A.coor_Y)*(D.coor_X-C.coor_X) then
				-- K) droites paralleles
				ali:=2;
				if Distance(A,B)=Distance(C,D) then
					typ:=-3;
					-- 1) parallelogramme
				else
					typ:=-2;
					-- 2) trapeze
				end if;
				else
					typ:=0;
					ali:=2;
					-- L) autres cas --> intersection vide
				end if;
End Inters_2seg;


-------------------------------------
-- Calcul de l'aire d'un quadrilatere
-------------------------------------
Function Quadrigone(A,B,C,D	: in gen_io.point_type) return float is
 aire		: integer;
 aire_quad	: float;
Begin  aire:=(B.coor_X-A.coor_X)*(A.coor_Y+B.coor_Y)+(C.coor_X-B.coor_X)*(B.coor_Y+C.coor_Y)+
(D.coor_X-C.coor_X)*(C.coor_Y+D.coor_Y)+	(A.coor_X-D.coor_X)*(D.coor_Y+A.coor_Y);
  aire_quad:=float(aire)/2.0;
  return aire_quad;
End Quadrigone;
--------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes :
-- Cas de courbes assez proches et assez regulieres
 -- ayant le meme nombre de points.
 -- Calcul d'intersection entre segments de meme rang.
 --------------------------------------------------------
 Function Aire_abs2( ligne	: point_liste_type;
		    ligne_lowe	: point_liste_type;
		    n_points	: natural) return float is
t,a,aire	: float;
 i		: natural;
 TInt		: point_liste_type(1..2);
 P0,P1		: gen_io.point_type;
Begin
 P1.coor_X:=-1;
 P1.coor_Y:=-1;
 P0.coor_X:=0;
 P0.coor_Y:=0;
 a:=0.0;
  for i in 1..n_points-1 loop
  TInt:=inters(ligne(i),ligne(i+1),ligne_lowe(i),ligne_lowe(i+1));
    if (TInt(1)=P0) or (TInt(1)=P1) or (TInt(2)=P1) or (Tint(1)/=Tint(2)) then
		t:=abs(Quadrigone(ligne(i),ligne(i+1),ligne_lowe(i+1),ligne_lowe(i)));
    else
		t:=abs(Quadrigone(ligne(i),TInt(1),TInt(1),ligne_lowe(i)))+ abs(Quadrigone(TInt(1),ligne(i+1),ligne_lowe(i+1),TInt(1)));
    end if;
    a:=a+t;
  end loop;
  aire:=a;
  return aire;
End Aire_abs2;
	
-----------------------------------------------------------------------
-- Confusion des extremites des deux lignes si ce n'est pas le cas
-- et suppression des points doubles (nbre egal de points entre lignes)
-----------------------------------------------------------------------
Procedure Homogena( ligne	 : in point_liste_type;
		    ligne_lowe	 : in point_liste_type;
		    n_points	 : in natural;
		    ligne_c	 : in out point_liste_type;
		    ligne_lowe_c : in out point_liste_type;
		    kk,ll	 : out natural) is 
i,k,l	: natural;
Begin
  k:=1;
  l:=1;
  if ligne(1)/=ligne_lowe(1) then
	ligne_c(k).coor_X:=integer(float(ligne(1).coor_X+ligne_lowe(1).coor_X)/2.0);
    ligne_c(k).coor_Y:=integer(float(ligne(1).coor_Y+ligne_lowe(1).coor_Y)/2.0);
    ligne_lowe_c(l):=ligne_c(k);
  end if;
  k:=k+1;
  l:=l+1;
  ligne_c(k):=ligne(1);
  ligne_lowe_c(l):=ligne_lowe(1);
  for i in 1..n_points-1 loop
	if ligne(i)/=ligne(i+1) then
		k:=k+1;
		ligne_c(k):=ligne(i);
	end if;
    if ligne_lowe(i)/=ligne_lowe(i+1) then
		l:=l+1;
		ligne_lowe_c(l):=ligne_lowe(i);
    end if;
  end loop;
  if ligne(n_points)/=ligne_lowe(n_points) then
	k:=k+1;
	l:=l+1;
    ligne_c(k).coor_X:=integer(float(ligne(n_points).coor_X+ligne_lowe(n_points).coor_X)/2.0);
    ligne_c(k).coor_Y:=integer(float(ligne(n_points).coor_Y+ ligne_lowe(n_points).coor_Y)/2.0);
    ligne_lowe_c(l):=ligne_c(k);
  end if;
  kk:=k;
  ll:=l;
End Homogena;
--------------------------------------------------------------------------
-- Confusion des extremites des deux lignes si ce n'est pas le cas
-- et suppression des points doubles (nbre de points des lignes different)
--------------------------------------------------------------------------
Procedure Homogena1 (ligne	   : in point_liste_type;
		     n_points	   : in natural;
		     ligne_lowe	   : in point_liste_type;
		     n_points_lowe : in natural;
		     ligne_c	   : in out point_liste_type;
		     ligne_lowe_c  : in out point_liste_type;
		     kk,ll	   : out natural) is 
i,j,k,l	: natural;
Begin
  k:=1;
  l:=1;
  if ligne(1)/=ligne_lowe(1) then
	ligne_c(1).coor_X:=integer(float(ligne(1).coor_X+ligne_lowe(1).coor_X)/2.0);
    ligne_c(1).coor_Y:=integer(float(ligne(1).coor_Y+ligne_lowe(1).coor_Y)/2.0);
    ligne_lowe_c(1):=ligne_c(1);
  else
	ligne_c(1):=ligne(1);
	ligne_lowe_c(1):=ligne_lowe(1);
  end if;
  k:=k+1;
  l:=l+1;
  ligne_c(k):=ligne(1);
  ligne_lowe_c(l):=ligne_lowe(1);
  for i in 1..n_points-1 loop
	if ligne(i)/=ligne(i+1) then
		k:=k+1;
		ligne_c(k):=ligne(i);
	end if;
  end loop;
  for i in 1..n_points_lowe-1 loop
	if ligne_lowe(i)/=ligne_lowe(i+1) then
		l:=l+1;
		ligne_lowe_c(l):=ligne_lowe(i);
    end if;
  end loop;
  if ligne(n_points)/=ligne_lowe(n_points_lowe) then
	k:=k+1;
	l:=l+1;
    ligne_c(k).coor_X:=integer(float(ligne(n_points).coor_X+ligne_lowe(n_points_lowe).coor_X)/2.0);
    ligne_c(k).coor_Y:=integer(float(ligne(n_points).coor_Y+ligne_lowe(n_points_lowe).coor_Y)/2.0);
    ligne_lowe_c(l):=ligne_c(k);
  end if;
  kk:=k;
 ll:=l;
End Homogena1;

--------------------------------------------------------------------------
-- Tri des elements d'un tableau en fonction de la distance a un point (A)
--------------------------------------------------------------------------
Procedure Tri	(Tab	: in out point_liste_type;
		 IInt	: in out point_liste_type;
		 IO,IL	: in out int_tableau_type;
		 lg,kk	: in natural;
		 A,B	: in gen_io.point_type) is
min,i,j,l,itemp,jj	: integer;
 temp			: gen_io.point_type;
 rep			: character;
Begin
  if A.coor_X/=B.coor_X then
	for i in 1..kk-1 loop
		min:=abs(A.coor_X-B.coor_X);
		for j in i..kk loop
			if abs(A.coor_X-Tab(j).coor_X)<min then
				min:=abs(A.coor_X-Tab(j).coor_X);
				l:=j;
			end if;
		end loop;
		if l/=i then
			temp.coor_X:=Tab(i).coor_X;
			temp.coor_Y:=Tab(i).coor_Y;
			Tab(i).coor_X:=Tab(l).coor_X;
			Tab(i).coor_Y:=Tab(l).coor_Y;
			Tab(l).coor_X:=temp.coor_X;
			Tab(l).coor_Y:=temp.coor_Y;
			temp:=IInt(lg-kk+i);
			IInt(lg-kk+i):=IInt(lg-kk+l);
			IInt(lg-kk+l):=temp;
			itemp:=IO(lg-kk+i);
			IO(lg-kk+i):=IO(lg-kk+l);
			IO(lg-kk+l):=itemp;
			itemp:=IL(lg-kk+i);
			IL(lg-kk+i):=IL(lg-kk+l);
			IL(lg-kk+l):=itemp;
		end if;
    end loop;
  else
	for i in 1..kk-1 loop
		min:=abs(A.coor_Y-B.coor_Y);
		for j in i..kk loop
			if abs(A.coor_Y-Tab(j).coor_Y)<min then
				min:=abs(A.coor_Y-Tab(j).coor_Y);
				l:=j;
			end if;
		end loop;
		if l/=i then
			temp:=Tab(i);
			Tab(i):=Tab(l);
			Tab(l):=temp;
			temp:=IInt(lg-kk+i);
			IInt(lg-kk+i):=IInt(lg-kk+l);
			IInt(lg-kk+l):=temp;
			itemp:=IO(lg-kk+i);
			IO(lg-kk+i):=IO(lg-kk+l);
			IO(lg-kk+l):=itemp;
			itemp:=IL(lg-kk+i);
			IL(lg-kk+i):=IL(lg-kk+l);
			IL(lg-kk+l):=itemp;
		end if;
    end loop;
  end if;
End Tri;

------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes
-- meme nombre de points pour les deux courbes
------------------------------------------------------
Function Aire_abs1( lign	: point_liste_type;
		    lign_lowe	: point_liste_type;
		    n_points	: natural) return float is
a,aire					: float;
 i,j,k,norig,nlowe,m,lg1,lg2,kk		: natural;
 Tab					: point_liste_type(1..20);
 Int,P0,P1,P2				: gen_io.point_type;
 IO,IL,Noeud1,Noeud2			: int_tableau_type(0..100);
 IInt					: point_liste_type(0..100);
 TInt					: point_liste_type(1..2);
 ligne,ligne_lowe,LIG1,LIG2,LIG1a,LIG2a	: point_liste_type(1..2000);
Begin
  Homogena(lign,lign_lowe,n_points,ligne,ligne_lowe,norig,nlowe);
  P2.coor_X:=-2;
 P2.coor_Y:=-2;
 P1.coor_X:=-1;
 P1.coor_Y:=-1;
  P0.coor_X:=0;
 P0.coor_Y:=0;
 k:=0;
 IO(0):=0;
 IL(0):=0;
 a:=0.0;
  for i in 1..norig-1 loop
	-- recherche des intersections entre les lignes
	if ligne(i)/=ligne(i+1) then
		-- points consecutifs differents      
		TInt(1):=P0;
		j:=IL(k)+1;
		kk:=0;
		for j in 1..nlowe-1 loop
			if ligne_lowe(j)/=ligne_lowe(j+1) then
				-- points consecutifs differents
				TInt:=Inters(ligne(i),ligne(i+1),ligne_lowe(j),ligne_lowe(j+1));
			end if;
			if (TInt(1)/=P0) and (TInt(2)/=P0) then
				-- intersection non vide	
				kk:=kk+1;
				k:=k+1;
				if (TInt(1)=P1) and (TInt(2)=P1) then	
					-- Extremites confondues
					IO(k):=i+1;
					IL(k):=j+1;
					IInt(k):=P0;
					Tab(kk):=ligne(i+1);
				elsif TInt(1)=P1 then
					-- Extremite de ligne appartient a ligne_lowe
					IO(k):=i+1;
					IL(k):=j;
					IInt(k):=P1;
					Tab(kk):=TInt(2);
					elsif TInt(2)=P1 then	
					-- Extremite de ligne_lowe appartient a ligne
					IO(k):=i;
					IL(k):=j+1;
					IInt(k):=P2;
					Tab(kk):=TInt(1);
					else			
					-- Intersection  differente des extremites
					IO(k):=i;
					IL(k):=j;
					IInt(k):=TInt(1);
					Tab(kk):=TInt(1);
				end if;
			end if;
		end loop;
		if kk>1 then
			Tri(Tab,IInt,IO,IL,k,kk,ligne(i),ligne(i+1));
		end if;
    end if;
  end loop;
  lg1:=0;
  lg2:=0;
  m:=1;
  Noeud1(m):=1;
  Noeud2(m):=1;
  for i in 0..k-1 loop
	if IO(i)/=IO(i+1) then
		for j in IO(i)+1..IO(i+1) loop
			LIG1(j+lg1-IO(i)):=ligne(j);
		end loop;
		lg1:=lg1+IO(i+1)-IO(i);
    end if;
    if IL(i)/=IL(i+1) then
		if IL(i)<IL(i+1) then
			for j in IL(i)+1..IL(i+1) loop
				LIG2(j+lg2-IL(i)):=ligne_lowe(j);
			end loop;
			lg2:=lg2+IL(i+1)-IL(i);
		else
			for j in IL(i+1)+1..IL(i) loop
				LIG2(j+lg2-IL(i+1)):=ligne_lowe(IL(i)+IL(i+1)+1-j);
			end loop;
			lg2:=lg2+IL(i)-IL(i+1);
		end if;
    end if;
    if IInt(i+1)=P0 then
		m:=m+1;
		Noeud1(m):=lg1;
		Noeud2(m):=lg2;
    elsif IInt(i+1)=P1 then
		m:=m+1;
		Noeud1(m):=lg1;
		lg2:=lg2+1;
		LIG2(lg2):=ligne(IO(i+1));
		Noeud2(m):=lg2;
		elsif IInt(i+1)=P2 then
			m:=m+1;
			Noeud2(m):=lg2;
			lg1:=lg1+1;
			LIG1(lg1):=ligne_lowe(IL(i+1));
			Noeud1(m):=lg1;
		else      
			m:=m+1;
			lg1:=lg1+1;
			LIG1(lg1):=IInt(i+1);
			lg2:=lg2+1;
			LIG2(lg2):=IInt(i+1);
			Noeud1(m):=lg1;
			Noeud2(m):=lg2;
	end if;
  end loop;
  a:=0.0;
  for i in 1..m-1 loop
	for j in noeud1(i)..noeud1(i+1) loop
		LIG1a(j-noeud1(i)+1):=LIG1(j);
    end loop;
    for j in noeud2(i)..noeud2(i+1) loop
		LIG2a(j-noeud2(i)+1):=LIG2(j);
    end loop;
    a:=a+abs(Aire_alg(LIG1a,noeud1(i+1)-noeud1(i)+1,
    		      LIG2a,noeud2(i+1)-noeud2(i)+1));
  end loop;
  aire:=a;
  return aire;
End Aire_abs1;

------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes
-- nombre de points different pour les deux courbes
------------------------------------------------------
Function Aire_abs3( lign	: point_liste_type;
		    n_p		: natural;
		    lign_lowe	: point_liste_type;
		    n_p_lowe	: natural) return float	is
a,aire					: float;
 i,j,k,norig,nlowe,m,lg1,lg2,kk		: natural;
 Tab					: point_liste_type(1..20);
 Int,P0,P1,P2				: gen_io.point_type;
 IO,IL,Noeud1,Noeud2			: int_tableau_type(0..100);
 IInt					: point_liste_type(0..100);
 TInt					: point_liste_type(1..2);
 ligne,ligne_lowe,LIG1,LIG2,LIG1a,LIG2a	: point_liste_type(1..2000);
Begin
  put("  ** aire_abs3 **  ");
  Homogena1(lign,n_p,lign_lowe,n_p_lowe,ligne,ligne_lowe,norig,nlowe);
  P2.coor_X:=-2;
  P2.coor_Y:=-2;
  P1.coor_X:=-1;
  P1.coor_Y:=-1;
  P0.coor_X:=0;
  P0.coor_Y:=0;
  k:=0;
  IO(0):=0;
  IL(0):=0;
  a:=0.0;
  for i in 1..norig-1 loop
	-- recherche des intersections entre les lignes
    if ligne(i)/=ligne(i+1) then
		-- points consecutifs differents
		TInt(1):=P0;
		j:=IL(k)+1;
		kk:=0;
		for j in 1..nlowe-1 loop
			if ligne_lowe(j)/=ligne_lowe(j+1) then
				-- points consecutifs differents
				TInt:=Inters(ligne(i),ligne(i+1),ligne_lowe(j),ligne_lowe(j+1));
			end if;
			if (TInt(1)/=P0) and (TInt(2)/=P0) then
				-- intersection non vide
				kk:=kk+1;
				k:=k+1;
				if (TInt(1)=P1) and (TInt(2)=P1) then
					-- Extremites confondues	
					IO(k):=i+1;
					IL(k):=j+1;
					IInt(k):=P0;
					Tab(kk):=ligne(i+1);
				elsif TInt(1)=P1 then
					-- Extremite de ligne appartient a ligne_lowe
					IO(k):=i+1;
					IL(k):=j;
					IInt(k):=P1;
					Tab(kk):=TInt(2);
				elsif TInt(2)=P1 then
					-- Extremite de ligne_lowe appartient a ligne
					IO(k):=i;
					IL(k):=j+1;
					IInt(k):=P2;
					Tab(kk):=TInt(1);
				else
					-- Intersection  differente des extremites	    
					IO(k):=i;
					IL(k):=j;
					IInt(k):=TInt(1);
					Tab(kk):=TInt(1);
				end if;
			end if;
		end loop;
		if kk>1 then
			Tri(Tab,IInt,IO,IL,k,kk,ligne(i),ligne(i+1));
		end if;
    end if;
  end loop;
  lg1:=0;
  lg2:=0;
  m:=1;
  Noeud1(m):=1;
  Noeud2(m):=1;
  for i in 0..k-1 loop
	if IO(i)/=IO(i+1) then
		for j in IO(i)+1..IO(i+1) loop
			LIG1(j+lg1-IO(i)):=ligne(j);
		end loop;
		lg1:=lg1+IO(i+1)-IO(i);
    end if;
    if IL(i)/=IL(i+1) then
		if IL(i)<IL(i+1) then
			for j in IL(i)+1..IL(i+1) loop
				LIG2(j+lg2-IL(i)):=ligne_lowe(j);
			end loop;
			lg2:=lg2+IL(i+1)-IL(i);
		else
			for j in IL(i+1)+1..IL(i) loop
				LIG2(j+lg2-IL(i+1)):=ligne_lowe(IL(i)+IL(i+1)+1-j);
			end loop;
			lg2:=lg2+IL(i)-IL(i+1);
		end if;
    end if;
    if IInt(i+1)=P0 then
		m:=m+1;
	Noeud1(m):=lg1;
	Noeud2(m):=lg2;
    elsif IInt(i+1)=P1 then
		m:=m+1;
		Noeud1(m):=lg1;
		lg2:=lg2+1;
		LIG2(lg2):=ligne(IO(i+1));
		Noeud2(m):=lg2;
    elsif IInt(i+1)=P2 then
		m:=m+1;
		Noeud2(m):=lg2;
		lg1:=lg1+1;
		LIG1(lg1):=ligne_lowe(IL(i+1));
		Noeud1(m):=lg1;
    else
		m:=m+1;
		lg1:=lg1+1;
		LIG1(lg1):=IInt(i+1);
		lg2:=lg2+1;
		LIG2(lg2):=IInt(i+1);
		Noeud1(m):=lg1;
		Noeud2(m):=lg2;
	end if;
  end loop;
  a:=0.0;
  for i in 1..m-1 loop
	for j in noeud1(i)..noeud1(i+1) loop
		LIG1a(j-noeud1(i)+1):=LIG1(j);
    end loop;
    for j in noeud2(i)..noeud2(i+1) loop
		LIG2a(j-noeud2(i)+1):=LIG2(j);
    end loop;
    a:=a+abs(Aire_alg(LIG1a,noeud1(i+1)-noeud1(i)+1, LIG2a,noeud2(i+1)-noeud2(i)+1));
  end loop;
  aire:=a;
  return aire;
End Aire_abs3;

-----------------------------------
-- Choix de la ligne : 1ere version
-----------------------------------
Procedure Choix_ligne  (Ligne    : out point_liste_type;
                        n_points : out natural) is 
numero : natural := 0;
 -- Numero de l'arc (compris entre 1 et Info.Nb_arcs) 
 graphe : Graphe_type;
 -- Le graphe lu a partir du fichier plateforme
 info : Info_type;
 -- Informations relatives au graphe
 ligne_ini                      : point_liste_type(1..2000);
 choix,numpre,numder,temp       : integer;
 rep                            : character;
 i,n                            : natural;
 nomfic				: string(1..20);
Begin
  put("Choix de la ligne (1=r2, 2=perso, 3=externe) : ");
  get(choix);
 new_line(1);
  if choix=1 then
	graphe := GR_LOAD_FROM_DISK("[barillot.graphes]R2",10,In_graphe);
    info:= GR_INFOS(graphe);
    put("Entrez le numero de l'arc ["&INTEGER'IMAGE(1)&":"&INTEGER'IMAGE(Info.Nb_arcs)&"] : ");
    INT_IO.get(numero);
	NEW_LINE(1);
    GR_POINTS_D_ARC(graphe,numero,ligne_ini,n);
    new_line(1);
	put(" Voulez l'arc entier ? [O] -> ");
	get(rep);
    if (rep='N') or (rep='n') then
		new_line(1);
		put("Le nombre de points de l'arc est de ");
		put(n);
		new_line(1);
		put("Donnez le numero du premier noeud intermediaire -> ");
		get(numpre);
		new_line(1);
		put("Donnez le numero du dernier noeud intermediaire -> ");
		get(numder);
		if numpre<1 then 
			numpre:=1;
		end if;
		if numder<1 then
			numder:=20;
		end if;
		if numpre>n then
			numpre:=n-20;
		end if;
		if numder>n then
			numder:=n;
		end if;
		if numpre>numder then
			temp:=numpre;
			numpre:=numder;
			numder:=temp;
		end if;
		if numpre=numder then 
			numpre:=numder-10;
			numder:=numder+10;
		end if;
		n:=numder-numpre;
		for i in 1..n loop
			ligne(i):=ligne_ini(numpre+i-1);
		end loop;
    elsif rep='m' then
		numpre:=130;
		numder:=190;
		n:=numder-numpre;
		for i in 1..n loop
			ligne(i):=ligne_ini(numpre+i-1);
		end loop;
    else 
		ligne:=ligne_ini;
    end if;
  elsif choix=2 then    put(" nbre total de points -> ");
	get(n);
    for i in 1..n loop
		put(" i=");
		put(i);
		put(" donnez x -> ");
		get(ligne(i).coor_X);
		put(" donnez y -> ");
		get(ligne(i).coor_Y);
		new_line(1);
    end loop;
	else 
		new_line(1);
		put("donnez le nom du fichier contenant la ligne -> ");
-- 
		get(nomfic);
		put("LIGNE SUPPRIMEE ARBITRAIREMENT: Export.Import(nomfic,ligne_ini,n);");
		--Export.Import(nomfic,ligne_ini,n);
		ligne:=ligne_ini;
	end if;
  n_points:=n;
End Choix_ligne;


-----------------------------------
-- Choix de la ligne : 2eme version
-----------------------------------
Procedure Choix_ligne1  (Ligne    : in out point_liste_type;
                         n_points : out natural;
			 rp,rp1 : out character) is 
numero : natural := 0; -- Numero de l'arc (compris entre 1 et Info.Nb_arcs) 
 graphe : Graphe_type; -- Le graphe lu a partir du fichier plateforme 
 info : Info_type;     -- Informations relatives au graphe 
 ligne_ini			: point_liste_type(1..2000);
 choix,numpre,numder,temp,x,y	: integer;
 i,np				: natural;
 rep,rep1			: character;
 nomfic				: string(1..20);
 gra				: string(1..3);
 ngra				: string(1..21);
Begin
  graphe := GR_LOAD_FROM_DISK("[barillot.graphes]R2",10,In_graphe);-- Inout ?!!
 info:= GR_INFOS(graphe);
--  put(" Voulez l'arc 4374 de 120 a 190 ? [O] -> "); get(rep);
  put(" Voulez l'arc 3102 ? [R] -> "); get(rep);
 if (rep='N') or (rep='n') then
 new_line(1);
 put(" (p)erso, (g)raphe, (r)2 ou (e)xterne ? [O] -> ");  new_line(1);
 get(rep1);
  IF (rep1='p') or (rep1='P') then
	put(" Combien de points pour la ligne originelle ?");  new_line(1);
	get(np);
    for i in 1..np loop
		put(" Point ");
		put(i);
		put(" ->");
		put(" donne X : ");
		get(x);
		put(" Point ");
		put(i);
		put(" ->");
		put(" donne Y : ");
		get(y);
		ligne(i).coor_X:=x;
		ligne(i).coor_Y:=y;
    end loop;
	ELSIF (rep1='r') or (rep1='R') or (rep1='g') or (rep1='G') then
		if (rep1='g') or (rep1='G') then
			new_line(1);
			put("Entrez le nom du graphe (3 lettres) : ");
			get(gra);
			ngra(1..18):="[barillot.graphes]";
			ngra(19..21):=gra(1..3);
			graphe := GR_LOAD_FROM_DISK(ngra(1..21),10,In_graphe);
			info:= GR_INFOS(graphe);
		end if;
    put("Entrez le numero de l'arc ["&        INTEGER'IMAGE(1)&":"&INTEGER'IMAGE(Info.Nb_arcs)&"] : ");
    INT_IO.get(numero); NEW_LINE(1);
    GR_POINTS_D_ARC(graphe,numero,ligne_ini,np);
	new_line(1);
	put(" Voulez l'arc entier ? [O] -> ");
	get(rep);
    if (rep='N') or (rep='n') then
		new_line(1);
		put("Le nombre de points de l'arc est de ");
		put(np);
		new_line(1);
		put("Donnez le numero du premier noeud intermediaire -> ");
		get(numpre);
		new_line(1);
		put("Donnez le numero du dernier noeud intermediaire -> ");
		get(numder);
		if numpre<1 then 
			numpre:=1;
		end if;
		if numder<1 then 
			numder:=20;
		end if;
		if numpre>np then
			numpre:=np-20;
		end if;
		if numder>np then 
			numder:=np;
		end if;
		if numpre>numder then
			temp:=numpre;
			numpre:=numder;
			numder:=temp;
		end if;
		if numpre=numder then numpre:=numder-10;
			numder:=numder+10;
		end if;
		np:=numder-numpre;
		for i in 1..np loop
			ligne(i):=ligne_ini(numpre+i-1);
		end loop;
		elsif rep='m' then
			numpre:=130;
			numder:=190;
			np:=numder-numpre;
      for i in 1..np loop
	  ligne(i):=ligne_ini(numpre+i-1);
 end loop;
    else ligne:=ligne_ini;
    end if;
  ELSE    new_line(1);
 put("donnez le nom du fichier contenant la ligne -> ");
--    get(nomfic);
 put("LIGNE SUPPIMEE ARBITRAIREMENT : Export.Import(nomfic,ligne_ini,np);");
 --Export.Import(nomfic,ligne_ini,np);
 ligne:=ligne_ini;
  END IF;
 elsif (rep='R') or (rep='r') then  numero:=3102;
 GR_POINTS_D_ARC(graphe,numero,ligne_ini,np);
  new_line(1);
 put("Le nombre de points de l'arc est de ");
 put(np);
  new_line(1);
 put("Donnez le numero du premier noeud intermediaire -> ");
  get(numpre);
  new_line(1);
 put("Donnez le numero du dernier noeud intermediaire -> ");
  get(numder);
  np:=numder-numpre;
  for i in 1..np loop ligne(i):=ligne_ini(numpre+i-1);
 end loop;
   else  numero:=4374;
 GR_POINTS_D_ARC(graphe,numero,ligne_ini,np);
  numpre:=120;
 numder:=190;
 np:=numder-numpre;
  for i in 1..np loop ligne(i):=ligne_ini(numpre+i-1);
 end loop;
 end if;
 n_points:=np;
 rp:=rep;
 rp1:=rep1;
End Choix_ligne1;


---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. entieres) (de M1 a M2)
-- resultat dans ]-3.14,+3,14]
---------------------------------------------------------------------------
 Function Angpente (X1,Y1,X2,Y2  : in integer) return float is pente   : float;
Begin  if X2=X1 then    if Y2>Y1 then pente:=PI/2.0;
 else pente:=-PI/2.0;
 end if;
  else    pente:=Arctan(float(Y2-Y1)/float(X2-X1));
    if (Y2<Y1) and (pente>0.0) then pente:=pente-PI;
    elsif (Y2>Y1) and (pente<0.0) then pente:=pente+PI;
    elsif (Y2=Y1) and (X2<X1) then pente:=PI;
    end if;
  end if;
  return pente;
End Angpente;
---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. mixtes) (de M1 a M2) 
-- resultat dans ]-3.14,+3,14]
---------------------------------------------------------------------------
Function Angpente (X1,Y1  : in integer;
		   X2,Y2  : in float) return float is 
pente   : float;
Begin
  if X2=float(X1) then
	if Y2>float(Y1) then 
		pente:=PI/2.0;
	else 
		pente:=-PI/2.0;
	end if;
  else
	pente:=arctan((Y2-float(Y1))/(X2-float(X1)));
    if (Y2<float(Y1)) and (pente>0.0) then 
		pente:=pente-PI;
    elsif (Y2>float(Y1)) and (pente<0.0) then 
		pente:=pente+PI;
    elsif (Y2=float(Y1)) and (X2<float(X1)) then
		pente:=PI;
    end if;
  end if;
  return pente;
End Angpente;
---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. mixtes) (de M1 a M2)
-- resultat dans ]-3.14,+3,14]
---------------------------------------------------------------------------
 Function Angpente (X1,Y1  : in float;
		   X2,Y2  : in integer) return float is 
pente   : float;
Begin
  if X1=float(X2) then
	if Y1<float(Y2) then
		pente:=PI/2.0;
	else
		pente:=-PI/2.0;
	end if;
  else 
	pente:=arctan((float(Y2)-Y1)/(float(X2)-X1));
    if (float(Y2)<Y1) and (pente>0.0) then 
		pente:=pente-PI;
    elsif (float(Y2)>Y1) and (pente<0.0) then 
		pente:=pente+PI;
    elsif (float(Y2)=Y1) and (float(X2)<X1) then 
		pente:=PI;
    end if;
  end if;
  return pente;
End Angpente;
--------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. reelles) (de M1 a M2)
-- resultat dans ]-3.14,+3,14]
--------------------------------------------------------------------------
Function Angpente (X1,Y1,X2,Y2  : in float) return float is
 pente   : float;
Begin
  if X2=X1 then
	if Y2>Y1 then 
		pente:=PI/2.0;
	else 
		pente:=-PI/2.0;
	end if;
  else    
	pente:=arctan((Y2-Y1)/(X2-X1));
    if (Y2<Y1) and (pente>0.0) then
		pente:=pente-PI;
    elsif (Y2>Y1) and (pente<0.0) then
		pente:=pente+PI;
    elsif (Y2=Y1) and (X2<X1) then
		pente:=PI;
    end if;
  end if;
  return pente;
End Angpente;
------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------
Procedure Verif(ligne		: in out point_liste_type;
                rapall		: in float;
                ectm		: in float;
                n_points	: in out natural) is 
ligne_cor,ligne_flo		: point_liste_reel(1..2000);
 a,alf,d1,d3,av,ap,p,p1,p2	: float;
 i,k,top,tip,cpt		: integer;
Begin
 for i in 1..n_points loop
	ligne_flo(i).coor_X:=float(ligne(i).coor_X);
	ligne_flo(i).coor_Y:=float(ligne(i).coor_Y);
 end loop;
 tip:=1;
 cpt:=0;
 while tip=1 loop
	cpt:=cpt+1;
	top:=0;
	k:=1;
	ligne_cor(k).coor_X:=ligne_flo(1).coor_X;
	ligne_cor(k).coor_Y:=ligne_flo(1).coor_Y;
	tip:=0;
	for i in 2..n_points-1 loop
    av:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
    ap:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y);
    a:=abs(av-ap);
    d1:=sqrt((ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)*(ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)+  
			(ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y)* (ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y));
    d3:=sqrt((ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)*(ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)+ 
			(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y)*(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y));
    if abs(a-PI)>1.0E-02 then
		if ((a<=PI/1.99) or (a>=1.49*PI)) and (top=0) and (d1>2.0) and (d3>2.0) then
			new_line(1);
			put("   -> Correction de la ligne en ");
			put(i,4);
			tip:=1;
			top:=1;
			p:=Angpente(ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y,ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
			p1:=p;
			if abs(p1-av)>PI then
				if av<0.0 then
					p1:=p1-2.0*PI;
				else 
					p1:=p1+2.0*PI;
			end if;
        end if;
        alf:=av+(p1-av)*ectm;
		k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d1*cos(alf)/rapall;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d1*sin(alf)/rapall;
		k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
        if p>0.0 then
			p2:=p-PI;
		else 
			p2:=p+PI;
		end if;
        if abs(p2-ap)>PI then
			if ap<0.0 then
				p2:=p2-2.0*PI;
			else 
				p2:=p2+2.0*PI;
			end if;
        end if;
        alf:=ap+(p2-ap)*ectm;
		k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d3*cos(alf)/rapall;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d3*sin(alf)/rapall;
      else        top:=0;
		k:=k+1;
		ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
      end if;
    else      new_line(1);
		put("   -> Elimination du point ");
		put(i,4);
		put(" (aligne avec ses 2 voisins).");
    end if;
  end loop;
  k:=k+1;
  ligne_cor(k).coor_X:=ligne_flo(n_points).coor_X;
  ligne_cor(k).coor_Y:=ligne_flo(n_points).coor_Y;
  n_points:=k;
  ligne_flo:=ligne_cor;
 end loop;
 for i in 1..n_points loop
	ligne(i).coor_X:=integer(ligne_cor(i).coor_X);
  ligne(i).coor_Y:=integer(ligne_cor(i).coor_Y);
 end loop;
End Verif;
-------------------------------------------
-- Detection des points anguleux et alignes
-------------------------------------------
Procedure Verif1(ligne		 : in point_liste_type;
		 angle		 : in float;
		 n_points	 : in natural;
		 n_ang,n_ali	 : out integer;
		 tab_ang,tab_ali : out int_tableau_type) is 
ligne_cor,ligne_flo		: point_liste_reel(1..2000);
 a,alf,d1,d3,av,ap,p,p1,p2	: float;
 i,k,cpt,n_al,n_an		: integer;
Begin
  for i in 1..n_points loop
	ligne_flo(i).coor_X:=float(ligne(i).coor_X);
    ligne_flo(i).coor_Y:=float(ligne(i).coor_Y);
  end loop;
  cpt:=0;
  cpt:=cpt+1;
   k:=1;
   n_al:=0;
   n_an:=0;
  ligne_cor(k).coor_X:=ligne_flo(1).coor_X;
  ligne_cor(k).coor_Y:=ligne_flo(1).coor_Y;
  for i in 2..n_points-1 loop
	av:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
    ap:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y, ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y);
    a:=abs(av-ap);
    d1:=sqrt((ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)* (ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)+ 
		(ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y)* (ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y));
    d3:=sqrt((ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)* (ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)+
		(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y)*(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y));
    if abs(a-PI)>1.0E-02 then
		if ((a<=angle) or (a>=2.0*PI-angle)) and (d1>2.0) and (d3>2.0) then
			new_line(1);
			put("   -> Detection d'un pt anguleux au pt ");
			put(i,4);
			n_an:=n_an+1;
			tab_ang(n_an):=i;
      end if;
    else      
		new_line(1);
		put("   -> Detection d'un pt aligne au pt ");
		put(i,4);
		n_al:=n_al+1;
		tab_ali(n_al):=i;
    end if;
  end loop;
  n_ali:=n_al;
 n_ang:=n_an;
End Verif1;
----------------------------------------------------------------------------
-- Verification et correction par insertion ou suppression de points (reels)
----------------------------------------------------------------------------
Procedure Verif(ligne_flo	: in out point_liste_reel;
                rapall		: in float;
                ectm		: in float;
                n_points	: in out natural) is 
ligne_cor			: point_liste_reel(1..2000);
 a,alf,d1,d3,av,ap,p,p1,p2	: float;
 i,k,top,tip,cpt		: integer;
Begin
 tip:=1;
 cpt:=0;
 while tip=1 loop 
	cpt:=cpt+1;
	 top:=0;
	k:=1;
	ligne_cor(k).coor_X:=ligne_flo(1).coor_X;
	ligne_cor(k).coor_Y:=ligne_flo(1).coor_Y;
	tip:=0;
	for i in 2..n_points-1 loop
		av:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
		ap:=Angpente(ligne_flo(i).coor_X,ligne_flo(i).coor_Y,ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y);
		a:=abs(av-ap);
		d1:=sqrt((ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)*(ligne_flo(i).coor_X-ligne_flo(i-1).coor_X)+ 
			(ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y)* (ligne_flo(i).coor_Y-ligne_flo(i-1).coor_Y));
		d3:=sqrt((ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)* (ligne_flo(i).coor_X-ligne_flo(i+1).coor_X)+ 
			(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y)*(ligne_flo(i).coor_Y-ligne_flo(i+1).coor_Y));
		if abs(a-PI)>1.0E-02 then
			if ((a<=PI/1.99) or (a>=1.49*PI)) and (top=0) and (d1>2.0) and (d3>2.0) then
				new_line(1);
				put(" -> Correction de la ligne en ");
				put(i,4);
				tip:=1;
				top:=1;
				p:=Angpente(ligne_flo(i+1).coor_X,ligne_flo(i+1).coor_Y, ligne_flo(i-1).coor_X,ligne_flo(i-1).coor_Y);
				p1:=p;
				if abs(p1-av)>PI then 
					if av<0.0 then
						p1:=p1-2.0*PI;
					else 
						p1:=p1+2.0*PI;
				end if;
			end if;
			alf:=av+(p1-av)*ectm;
			k:=k+1;
			ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d1*cos(alf)/rapall;
			ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d1*sin(alf)/rapall;
			k:=k+1;
			ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
			ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
			if p>0.0 then 
				p2:=p-PI;
			else 
				p2:=p+PI;
			end if;
        if abs(p2-ap)>PI then
			if ap<0.0 then
				p2:=p2-2.0*PI;
			else
				p2:=p2+2.0*PI;
			end if;
        end if;
        alf:=ap+(p2-ap)*ectm;
		k:=k+1;
        ligne_cor(k).coor_X:=ligne_flo(i).coor_X+d3*cos(alf)/rapall;
        ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y+d3*sin(alf)/rapall;
		else 
			top:=0;
			k:=k+1;
			ligne_cor(k).coor_X:=ligne_flo(i).coor_X;
			ligne_cor(k).coor_Y:=ligne_flo(i).coor_Y;
		end if;
	else
		new_line(1);
		put(" -> Elimination du point ");
		put(i,4);
		put(" (aligne avec ses 2 voisins).");
    end if;
  end loop;
  k:=k+1;
  ligne_cor(k).coor_X:=ligne_flo(n_points).coor_X;
  ligne_cor(k).coor_Y:=ligne_flo(n_points).coor_Y;
  n_points:=k;
  ligne_flo:=ligne_cor;
 end loop;
End Verif;
------------------------------------------------------------------------------
-- Calcul des pentes des pts (coord entieres) d'une ligne (dans ]-3.14,+3.14])   
--> pente en un point = droite joignant les 2 points adjacents
------------------------------------------------------------------------------
Function Calpente (ligne        : in point_liste_type;
		   n_points     : in natural) return reel_tableau_type is
pente   : reel_tableau_type(1..2000);
 i       : integer;
Begin
  for i in 2..n_points-1 loop
	pente(i):=Angpente(ligne(i-1).coor_X,ligne(i-1).coor_Y,ligne(i+1).coor_X,ligne(i+1).coor_Y);
  end loop;
  pente(1):=1.5*Angpente(ligne(1).coor_X,ligne(1).coor_Y, ligne(2).coor_X,ligne(2).coor_Y)-0.5*pente(2);
  pente(n_points):= 1.5*Angpente(ligne(n_points-1).coor_X,ligne(n_points-1).coor_Y,	ligne(n_points).coor_X,ligne(n_points).coor_Y)-0.5*pente(n_points-1);
  return pente;
End Calpente;
-----------------------------------------------------------------------------
-- Calcul des pentes des pts (coord reelles) d'une ligne (dans ]-3.14,+3.14])
-----------------------------------------------------------------------------
Function Calpente (ligne        : in point_liste_reel;
		   n_points     : in natural) return reel_tableau_type is 
pente   : reel_tableau_type(1..2000);
 i       : integer;
Begin
  for i in 2..n_points-1 loop
	pente(i):=Angpente(ligne(i-1).coor_X,ligne(i-1).coor_Y,	ligne(i+1).coor_X,ligne(i+1).coor_Y);
  end loop;
  pente(1):=1.5*Angpente(ligne(1).coor_X,ligne(1).coor_Y, ligne(2).coor_X,ligne(2).coor_Y)-0.5*pente(2);
  pente(n_points):=    1.5*Angpente(ligne(n_points-1).coor_X,ligne(n_points-1).coor_Y,	ligne(n_points).coor_X,ligne(n_points).coor_Y)-0.5*pente(n_points-1);
  return pente;
End Calpente;
------------------------------------------------------------------------------
-- Calcul des pentes des pts (coord entieres) d'une ligne (dans ]-3.14,+3.14])   
--> pente au point Mi = ponderation en 1/k des droites (Mi-k Mi+k)
 ------------------------------------------------------------------------------
 Function Calpente_multi (ligne	    : in point_liste_type;
		   	 n_points,n : in natural) return reel_tableau_type is
 pente						: reel_tableau_type(1..2000);
 i,j,k						: integer;
 somme_poids,x1,x2,xk,xI,y1,y2,yk,yI,D2,a0,a	: float;
 E						: point_liste_reel(1..2000);
Begin
  k:=n;
  if k>n_points-2 then
	k:=n_points-2;
	-- nombre maximal = n_points-2
	elsif k<1 then 
		k:=1;
		-- nombre minimal = 1
	end if;
	x1:=float(ligne(1).coor_x);
	y1:=float(ligne(1).coor_y);
	x2:=float(ligne(2).coor_x);
	y2:=float(ligne(2).coor_y);
	xI:=(x1+x2)/2.0;
	yI:=(y1+y2)/2.0;
	D2:=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
	for i in 1..k loop
		xk:=float(ligne(k+2).coor_x);
		yk:=float(ligne(k+2).coor_y);
		E(i).coor_x:=2.0*xI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(x2-x1)-0.5*xk;
		E(i).coor_y:=2.0*yI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(y2-y1)-0.5*yk;
	end loop;
	for i in 1..n_points loop
		E(k+i).coor_x:=float(ligne(i).coor_x);
		E(k+i).coor_y:=float(ligne(i).coor_y);
	end loop;
    x1:=float(ligne(n_points).coor_x);
	y1:=float(ligne(n_points).coor_y);
	x2:=float(ligne(n_points-1).coor_x);
	y2:=float(ligne(n_points-1).coor_y);
	xI:=(x1+x2)/2.0;
	yI:=(y1+y2)/2.0;
	D2:=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
	for i in 1..k loop
		j:=i+k+n_points;
		xk:=float(ligne(n_points-k-1).coor_x);
		yk:=float(ligne(n_points-k-1).coor_y);
		E(j).coor_x:=2.0*xI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(x2-x1)-0.5*xk;
		E(j).coor_y:=2.0*yI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(y2-y1)-0.5*yk;
	end loop;
	somme_poids:=0.0;
	for j in 1..k loop
		somme_poids:=somme_poids+1.0/float(k);
	end loop;
	for i in 1..n_points loop
		a0:=Angpente(E(k+i-j).coor_X,E(k+i-j).coor_Y, E(k+i+j).coor_X,E(k+i+j).coor_Y)/float(k);
		pente(i):=a0;
		for j in 1..k loop
			a:=Angpente(E(k+i-j).coor_X,E(k+i-j).coor_Y,E(k+i+j).coor_X,E(k+i+j).coor_Y)/float(k);
		if abs(a-a0)>=PI/2.0 then 
		-- Si on est en limite de PI/-PI ! 
			if a>a0 then
				a:=a-2.0*PI;
			else
				a:=a+2.0*PI;
			end if;
		end if;
		if abs(a-a0)<PI/2.0 then pente(i):=pente(i)+a;
			a0:=a;
		else 
			new_line(1);
			put("PROBLEME");
		end if;
    end loop;
    pente(i):=pente(i)/somme_poids;
    if pente(i)>PI then 
		pente(i):=pente(i)-2.0*PI;
    elsif pente(i)<-PI then
		pente(i):=pente(i)+2.0*PI;
    end if;
  end loop;
  return pente;
End Calpente_multi;


-----------------------------------------------------------------------------
-- Calcul des pentes des pts (coord reelles) d'une ligne (dans ]-3.14,+3.14])   
--> pente au point Mi = ponderation en 1/k des droites (Mi-k Mi+k) 
-----------------------------------------------------------------------------
Function Calpente_multi (ligne	    : in point_liste_reel;
		   	 n_points,n : in natural) return reel_tableau_type is
 
 pente : reel_tableau_type(1..2000);
 i,j,k	: integer;
 somme_poids,x1,x2,xk,xI,y1,y2,yk,yI,D2,a0,a	: float;
 E : point_liste_reel(1..2000);
Begin
  k:=n;
  if k>n_points-2 then
		k:=n_points-2;
	-- nombre maximal = n_points-2
  elsif k<1 then 
		k:=1;
	-- nombre minimal			= 1
  end if;
  x1:=ligne(1).coor_x;
  y1:=ligne(1).coor_y;
  x2:=ligne(2).coor_x;
  y2:=ligne(2).coor_y;
  xI:=(x1+x2)/2.0;
  yI:=(y1+y2)/2.0;
  D2:=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
  for i in 1..k loop
	xk:=ligne(k+2).coor_x;
	yk:=ligne(k+2).coor_y;
    E(i).coor_x:=2.0*xI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(x2-x1)-0.5*xk;
    E(i).coor_y:=2.0*yI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(y2-y1)-0.5*yk;
  end loop;
  for i in 1..n_points loop
	E(k+i):=ligne(i);
  end loop;
  x1:=ligne(n_points).coor_x;
  y1:=ligne(n_points).coor_y;
  x2:=ligne(n_points-1).coor_x;
  y2:=ligne(n_points-1).coor_y;
  xI:=(x1+x2)/2.0;
  yI:=(y1+y2)/2.0;
  D2:=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
  for i in 1..k loop
	j:=i+k+n_points;
    xk:=ligne(n_points-k-1).coor_x;
    yk:=ligne(n_points-k-1).coor_y;
    E(j).coor_x:=2.0*xI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(x2-x1)-0.5*xk;
    E(j).coor_y:=2.0*yI-3.0*((xk-xI)*(x2-xI)+(yk-xI)*(y2-yI))/D2*(y2-y1)-0.5*yk;
  end loop;
  somme_poids:=0.0;
  for j in 1..k loop
	somme_poids:=somme_poids+1.0/float(k);
  end loop;
  for i in 1..n_points loop
	a0:=Angpente(E(k+i-j).coor_X,E(k+i-j).coor_Y, E(k+i+j).coor_X,E(k+i+j).coor_Y)/float(k);
    pente(i):=a0;
    for j in 1..k loop
		a:=Angpente(E(k+i-j).coor_X,E(k+i-j).coor_Y, E(k+i+j).coor_X,E(k+i+j).coor_Y)/float(k);
		if abs(a-a0)>=PI/2.0 then
		-- Si on est en limite de PI/-PI ! 
			if a>a0 then
				a:=a-2.0*PI;
			else 
				a:=a+2.0*PI;
			end if;
		end if;
        if abs(a-a0)<PI/2.0 then
			pente(i):=pente(i)+a;
			a0:=a;
		else 
			new_line(1);
			put("PROBLEME");
		end if;
    end loop;
    pente(i):=pente(i)/somme_poids;
    if pente(i)>PI then
		pente(i):=pente(i)-2.0*PI;
    elsif pente(i)<-PI then
		pente(i):=pente(i)+2.0*PI;
    end if;
  end loop;
  return pente;
End Calpente_multi;
----------------------------------------------------------------------------
-- Decomposition d'une ligne en segments irregulers (ligne reelle en entree)
----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant	: in Point_liste_reel;
			  navant	: in natural;
			  l_apres	: out Point_liste_reel;
			  napres	: out natural;
			  pas		: in float;
			  nb_segments	: in out Liens_array_type) is
 NNP		: natural;
 X1,Y1,X2,Y2	: float;
 DX,DY,L	: float;
 n_seg		: integer;
Begin
 NNP:=1;
 X1:=l_avant(1).Coor_x;
 Y1:=l_avant(1).Coor_y;
 l_apres(1).Coor_x:=X1;
 l_apres(1).Coor_y:=Y1;
 for I in 2..navant loop
	X2:=l_avant(I).Coor_x;
	Y2:=l_avant(I).Coor_y;
	DX:=X2-X1;
	DY:=Y2-Y1;
		L:=sqrt(DX*DX + DY*DY)/Pas;
	n_seg:=integer(L);
	nb_segments(I-1):=n_seg;
    if n_seg/=0 then
		DX:=DX/float(n_seg);
		DY:=DY/float(n_seg);
		for J in 1..n_seg loop
			NNP:=NNP+1;
			l_apres(NNP).Coor_x:=X1+float(J)*DX;
			l_apres(NNP).Coor_y:=Y1+float(J)*DY;
		end loop;
	end if;
    X1:=X2;
	Y1:=Y2;
  end loop;
  napres:=NNP;
End Decompose_ligne;
----------------------------------------------------------------------------
-- Decomposition d'une ligne en segments irregulers (ligne reelle en entree)
----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant	: in Point_liste_reel;
			  navant	: in natural;
			  l_apres	: out Point_liste_type;
			  napres	: out natural;
			  pas		: in float;
			  nb_segments	: in out Liens_array_type) is NNP		: natural;
 X1,Y1,X2,Y2	: float;
 DX,DY,L	: float;
 n_seg		: integer;
 Begin  NNP:=1;
 X1:=l_avant(1).Coor_x;
 Y1:=l_avant(1).Coor_y;
  l_apres(1).Coor_x:=integer(l_avant(1).Coor_x);
 l_apres(1).Coor_y:=integer(l_avant(1).Coor_y);
  for I in 2..navant loop    X2:=l_avant(I).Coor_x;
 Y2:=l_avant(I).Coor_y;
 DX:=X2-X1;
 DY:=Y2-Y1;
    L:=sqrt(DX*DX+DY*DY)/Pas;
 n_seg:=integer(L);
 nb_segments(I-1):=n_seg;
     if n_seg/=0 then      DX:=DX/float(n_seg);
 DY:=DY/float(n_seg);
      for J in 1..n_seg loop	NNP:=NNP+1;
 l_apres(NNP).Coor_x:=integer(X1+float(J)*DX);
	l_apres(NNP).Coor_y:=integer(Y1+float(J)*DY);
      end loop;
    end if;
    X1:=X2;
 Y1:=Y2;
  end loop;
  napres:=NNP;
End Decompose_ligne;
----------------------------------------------------------------------------
-- Decomposition d'une ligne en segments irregulers (ligne reelle en entree)
----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant	: in Point_liste_reel;
			  navant	: in natural;
			  l_apres	: in out Point_Access_reel;
			  napres	: out natural;
			  pas		: in float;
			  nb_segments	: in out Liens_array_type) is NNP		: natural;
 X1,Y1,X2,Y2	: float;
 DX,DY,L	: float;
 n_seg		: integer;
 taille		: float;
Begin-- Calcul de la longueur totale de la ligne  
taille:=taille_ligne(l_avant,navant);
  n_seg:=integer(taille/Pas);
    -- Arrondi a l'entier le plus proche
    -- Allocation memoire de la ligne resultat  
    l_apres:=new Point_liste_reel(1..(n_seg+100));
 NNP:=1;
  X1:=l_avant(1).Coor_x;
 Y1:=l_avant(1).Coor_y;
  l_apres(1).Coor_x:=X1;
 l_apres(1).Coor_y:=Y1;
  for I in 2..navant loop    X2:=l_avant(I).Coor_x;
 Y2:=l_avant(I).Coor_y;
 DX:=X2-X1;
 DY:=Y2-Y1;
    L:=sqrt(DX*DX+DY*DY)/Pas;
 n_seg:=integer(L);
 nb_segments(I-1):=n_seg;
    if n_seg/=0 then      DX:=DX/float(n_seg);
 DY:=DY/float(n_seg);
      for J in 1..n_seg loop	NNP:=NNP+1;
 l_apres(NNP).Coor_x:=X1+float(J)*DX;
	l_apres(NNP).Coor_y:=Y1+float(J)*DY;
      end loop;
    end if;
    X1:=X2;
 Y1:=Y2;
  end loop;
  napres:=NNP;
End Decompose_ligne;
---------------------------------------------
-- Teste si un entier est pair (1) ou non (0)
---------------------------------------------
Function Paire (i : integer) return integer is r       : float;
 res     : integer;
Begin  res:=0;
 r:=float(i)/2.0;
 if r-float(integer(r))=0.0 then res:=1;
 end if;
  return res;
End Paire;
-------------------------------------------------------------------
-- Calcul de l'ordonnee d'une cubique (a,b,c,d) pour une abscisse x
-------------------------------------------------------------------
Function Y_cubique ( a,b,c,d,x :float) return float is y  : float;
Begin  y:=a*x*x*x+b*x*x+c*x+d;
 return Y;
End Y_cubique;
---------------------------------------------------------------
-- Calcul de la difference de deux angles (a2-a1) a modulo 2*PI
---------------------------------------------------------------
Function Deltatrigo (a1,a2 : float) return float is y    : float;
Begin  y:=a2-a1;
  if abs(y)>3.0 then y:=abs(abs(y)-2.0*PI);
 if a1<0.0 then y:=-y;
 end if;
  end if;
  return y;
End Deltatrigo;
--------------------------------------------------------------------------
-- Calcul du rang de la valeur minimale d'un extrait d'un tableau de reels
-- On considere une suite extraite (par P entre 2 indices ri et rf) de T
--------------------------------------------------------------------------
Function Mintab ( T	: reel_tableau_type;
		  P	: Int_tableau_type;
		  ri,rf : integer) return integer is min     : float;
 imin,i  : integer;
Begin  min:=T(P(ri));
 imin:=P(ri);
  for i in ri+1..rf loop    if T(P(i))<min then min:=T(P(i));
 imin:=P(i);
 end if;
  end loop;
  return imin;
End Mintab;
--------------------------------------------------------------------------
-- Calcul du rang de la valeur maximale d'un extrait d'un tableau de reels
-- On considere une suite extraite (par P entre 2 indices ri et rf) de T
--------------------------------------------------------------------------
Function Maxtab ( T	: reel_tableau_type;
		  P	: Int_tableau_type;
		  ri,rf : integer) return integer is max     : float;
 imax,i  : integer;
Begin  max:=T(P(ri));
 imax:=P(ri);
  for i in ri+1..rf loop    if T(P(i))>max then max:=T(P(i));
 imax:=P(i);
 end if;
  end loop;
  return imax;
End Maxtab;
--------------------------------------------------------------------
-- Detection des maxima d'une courbe entre deux valeurs nini et nfin
-- pos=1 <=> on commence a partir de nini-1
--------------------------------------------------------------------
Procedure RechMax (fct		: in reel_tableau_type;
		   nini,nfin	: in natural;
		   pos		: in integer;
		   P_MAX	: out Int_tableau_type;
		   n_max	: out integer) is PMAX			: Int_tableau_type(0..100);
 nmax			: natural;
 i,ii,lin,n_ini,n_fin	: integer;
 a1,a2			: float;
 rep			: character;
Begin  if pos=1 then n_ini:=nini-1;
 else n_ini:=nini;
 end if;
  n_fin:=nfin;
 ii:=n_ini;
 a1:=fct(ii)-fct(ii+1);
  while (a1=0.0) and (ii<n_fin) loop a1:=fct(ii)-fct(ii+1);
 ii:=ii+1;
 end loop;
  lin:=0;
 nmax:=0;
  if ii/=n_fin then    for i in ii..n_fin-1 loop      a2:=fct(i)-fct(i+1);
      if a1*a2<0.0 then	nmax:=nmax+1;
 PMAX(nmax):=i-integer(float(lin)/2.0);
 a1:=a2;
 lin:=0;
      elsif a2=0.0 then -- pente constante => element lineaire	
      lin:=lin+1;
      else -- signes de a1 et a2 identiques	
      lin:=0;
 a1:=a1+a2;
      end if;
    end loop;
  end if;
  n_max:=nmax;
 P_MAX:=PMAX;
End RechMax;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord entieres)
--------------------------------------------------------------------------------
Function Rayon_3pts (A,B,C : in gen_io.point_type) return float is d1,d2,ang,co,si,rayon	: float;
Begin  d1:=Geomxav.Distance(B,C);
 d2:=Geomxav.Distance(A,C);
  ang:=Geomxav.Angle_3pts(A,B,C);
 co:=cos(ang);
 si:=sin(ang);
  if si=0.0 then rayon:=0.0;
  elsif si>0.0 then rayon:=sqrt(d1*d1+d2*d2-2.0*d1*d2*co)/2.0/si;
  else rayon:=-sqrt(d1*d1+d2*d2-2.0*d1*d2*co)/2.0/si;
  end if;
  return rayon;
End Rayon_3pts;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord. reelles)
--------------------------------------------------------------------------------
Function Rayon_3pts (A,B,C : in gen_io.point_type_reel) return float is d1,d2,ang,co,si,rayon	: float;
Begin  d1:=Geomxav.Distance(B,A);
 d2:=Geomxav.Distance(B,C);
  ang:=Geomxav.Angle_3pts(A,B,C);
 co:=cos(ang);
 si:=sin(ang);
  if si=0.0 then rayon:=0.0;
  elsif si>0.0 then rayon:=sqrt(d1*d1+d2*d2-2.0*d1*d2*co)/2.0/si;
  else rayon:=-sqrt(d1*d1+d2*d2-2.0*d1*d2*co)/2.0/si;
  end if;
  return rayon;
End Rayon_3pts;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord entieres)
--------------------------------------------------------------------------------
Function Ray_3pts (A,B,C : in gen_io.point_type) return float is xv1,xv2,yv1,yv2,xm1,xm2,ym1,ym2,rayon,r1,r2,r3	: float;
 CC,AR,BR,CR					: gen_io.point_type_reel;
Begin  AR.coor_x:=float(A.coor_x);
 AR.coor_y:=float(A.coor_y);
  BR.coor_x:=float(B.coor_x);
 BR.coor_y:=float(B.coor_y);
  CR.coor_x:=float(C.coor_x);
 CR.coor_y:=float(C.coor_y);
  xv1:=BR.coor_x-CR.coor_x;
 yv1:=BR.coor_y-CR.coor_y;
  xv2:=AR.coor_x-CR.coor_x;
 yv2:=AR.coor_y-CR.coor_y;
  xm1:=(BR.coor_x+CR.coor_x)/2.0;
 ym1:=(BR.coor_y+CR.coor_y)/2.0;
  xm2:=(AR.coor_x+CR.coor_x)/2.0;
 ym2:=(AR.coor_y+CR.coor_y)/2.0;
  if xv1*yv2=xv2*yv1 then rayon:=1.0E+38;
  else    CC.coor_x:=(xv1*yv2*xm1-xv2*yv1*xm2+yv1*yv2*(ym1-ym2))/(xv1*yv2-xv2*yv1);
    if yv1/=0.0 then CC.coor_y:=xv1/yv1*(xm1-CC.coor_x)+ym1;
    else CC.coor_y:=xv2/yv2*(xm2-CC.coor_x)+ym2;
    end if;
    rayon:=Geomxav.Distance(CC,AR);
   end if;
  return rayon;
End Ray_3pts;
-------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord reelles)
-------------------------------------------------------------------------------
Function Ray_3pts (A,B,C : in gen_io.point_type_reel) return float is xv1,xv2,yv1,yv2,xm1,xm2,ym1,ym2,rayon,r1,r2,r3	: float;
 CC 						: gen_io.point_type_reel;
Begin  xv1:=B.coor_x-C.coor_x;
 yv1:=B.coor_y-C.coor_y;
  xv2:=A.coor_x-C.coor_x;
 yv2:=A.coor_y-C.coor_y;
  xm1:=(B.coor_x+C.coor_x)/2.0;
 ym1:=(B.coor_y+C.coor_y)/2.0;
  xm2:=(A.coor_x+C.coor_x)/2.0;
 ym2:=(A.coor_y+C.coor_y)/2.0;
  if xv1*yv2=xv2*yv1 then rayon:=1.0E+38;
  else    CC.coor_x:=(xv1/yv1*xm1+ym1-xv2/yv2*xm2-ym2)/(xv1/yv1-xv2/yv2);
    if yv1/=0.0 then CC.coor_y:=xv1/yv1*(xm1-CC.coor_x)+ym1;
    else CC.coor_y:=xv2/yv2*(xm2-CC.coor_x)+ym2;
    end if;
    rayon:=Geomxav.Distance(CC,A);
   end if;
  return rayon;
End Ray_3pts;
----------------------------------------------------------------------
-- Calcul du determinant entre les vecteurs MN et MP (coord. entieres)
----------------------------------------------------------------------
Function Det (M,N,P : Gen_io.point_type) return float is
Begin  return float((N.coor_x-M.coor_x)*(P.coor_y-M.coor_y)-               (N.coor_y-M.coor_y)*(P.coor_x-M.coor_x));
End Det;
---------------------------------------------------------------------
-- Calcul du determinant entre les vecteurs MN et MP (coord. reelles)
---------------------------------------------------------------------
Function Det (M,N,P : Gen_io.point_type_reel) return float is
Begin  return (N.coor_x-M.coor_x)*(P.coor_y-M.coor_y)-         (N.coor_y-M.coor_y)*(P.coor_x-M.coor_x);
End Det;
-----------------------------------------------------------
-- Teste l'appartenance de X au segment MN (coord. reelles)
-----------------------------------------------------------
Function Appseg (M,N,X : Gen_io.point_type_reel) return boolean is
Begin  if (Det(M,N,X)=0.0) and ((M.coor_x-X.coor_x)*(N.coor_x-X.coor_x)<=0.0) and			  ((M.coor_y-X.coor_y)*(N.coor_y-X.coor_y)<=0.0) then    return true;
  else return false;
  end if;
End Appseg;
------------------------------------------------------------
-- Teste l'appartenance de X au segment MN (coord. entieres)
------------------------------------------------------------
Function Appseg (M,N,X : Gen_io.point_type) return boolean is
Begin  if (Det(M,N,X)=0.0) and ((M.coor_x-X.coor_x)*(N.coor_x-X.coor_x)<=0) and			  ((M.coor_y-X.coor_y)*(N.coor_y-X.coor_y)<=0) then    return true;
  else return false;
  end if;
End Appseg;
------------------------------------------------------------
-- Teste si les segments MN et PX se coupent (coord.reelles)
------------------------------------------------------------
Function Coupseg (M,N,P,X : Gen_io.point_type_reel) return boolean is
Begin  if (Det(M,N,P)*Det(M,N,X)<=0.0) and (Det(M,P,N)*Det(M,P,X)>=0.0) and                                      (Det(N,P,M)*Det(N,P,X)>=0.0) then    return true;
  else return false;
  end if;
End Coupseg;
--------------------------------------------------------------
-- Teste si les segments MN et PX se coupent (coord. entieres)
--------------------------------------------------------------
Function Coupseg (M,N,P,X : Gen_io.point_type) return boolean is
Begin  if (Det(M,N,P)*Det(M,N,X)<=0.0) and (Det(M,P,N)*Det(M,P,X)>=0.0) and                                      (Det(N,P,M)*Det(N,P,X)>=0.0) then    return true;
  else return false;
  end if;
End Coupseg;
-----------------------------------------------
-- Calcul de l'intersection entre deux segments
-----------------------------------------------
Function Inters( A,B,C,D : in gen_io.point_type) return gen_io.point_type is p1,p2,r1,r2,rl1,rl2	: float;
 Int			: gen_io.point_type;
Begin  r1:=lieu(C,D,A);
 r2:=lieu(C,D,B);
 rl1:=lieu(A,B,C);
 rl2:=lieu(A,B,D);
  if (r1*r2<0.0) and (rl1*rl2<0.0) then    if B.coor_X=A.coor_X then      p2:=pente(C,D);
 Int.coor_X:=A.coor_X;
      Int.coor_Y:=C.coor_Y+integer(p2*float(A.coor_X-C.coor_X));
    elsif C.coor_X/=D.coor_X then      p1:=pente(A,B);
 Int.coor_X:=A.coor_X;
      Int.coor_Y:=A.coor_Y+integer(p1*float(C.coor_X-A.coor_X));
    else      p1:=pente(A,B);
 p2:=pente(C,D);
      Int.coor_X:=integer((float(C.coor_Y)-p2*float(C.coor_X)-			   float(A.coor_Y)+p1*float(A.coor_X))/(p1-p2));
      Int.coor_Y:=integer((p1*(float(C.coor_Y)-p2*float(C.coor_X))-			   p2*(float(A.coor_Y)-p1*float(A.coor_X)))/(p1-p2));
    end if;
  else Int.coor_X:=0;
 Int.coor_Y:=0;
  end if;
  return Int;
End Inters;
------------------------------------------------------
-- Translation du polygone TAB pour que TAB(im):=(0,0)
-- Permutation de TAB pour que TAB(1):=TAB(im)
------------------------------------------------------
Function Transl(TAB     : point_liste_reel;
                nbpt,im : integer) return point_liste_reel is i,k    : integer;
 TAB1   : point_liste_reel(1..500);
Begin  k:=0;
  for i in im..nbpt loop    k:=k+1;
    TAB1(k).coor_x:=TAB(i).coor_x-TAB(im).coor_x;
    TAB1(k).coor_y:=TAB(i).coor_y-TAB(im).coor_y;
  end loop;
  if im/=1 then    for i in 1..im-1 loop      k:=k+1;
      TAB1(k).coor_x:=TAB(i).coor_x-TAB(im).coor_x;
      TAB1(k).coor_y:=TAB(i).coor_y-TAB(im).coor_y;
    end loop;
  end if;
  return TAB1;
End Transl;
-----------------------------------------------------------------
-- Translation d'un polygone en TAB(i0)=0
-- Rotation autour de TAB(i0) pour que TAB(i1).x<0 et TAB(i1).y=0
-----------------------------------------------------------------
Function Rot2(  TAB1            : point_liste_reel;
                nbpt,i0,i1      : integer) return point_liste_reel is i              : integer;
 TAB            : point_liste_reel(1..500);
 d,c,s,x,y      : float;
Begin  for i in 1..nbpt loop    TAB(i).coor_x:=TAB1(i).coor_x-TAB1(i0).coor_x;
    TAB(i).coor_y:=TAB1(i).coor_y-TAB1(i0).coor_y;
  end loop;
  d:=Geomxav.Distance(TAB1(i0),TAB1(i1));
  if d/=0.0 then    c:=-TAB(i1).coor_x/d;
 s:=-TAB(i1).coor_y/d;
    for i in 1..nbpt loop      x:=TAB(i).coor_x;
 y:=TAB(i).coor_y;
      TAB(i).coor_x:=c*x+s*y;
 TAB(i).coor_y:=c*y-s*x;
    end loop;
  end if;
  return TAB;
End Rot2;
-----------------------------------
-- Polygone mis dans le sens direct
-----------------------------------
Procedure SensDirect(   TAB0    : in point_liste_reel;
                        nbpt    : in integer;
                        sd      : out integer) is i               : integer;
 angtotal,a      : float;
 TAB1            : point_liste_reel(0..500);
Begin  angtotal:=0.0;
  for i in 1..nbpt loop TAB1(i):=TAB0(i);
 end loop;
  TAB1(0):=TAB0(nbpt-1);
  for i in 1..nbpt-1 loop    a:=Geomxav.Angpente(TAB1(i).coor_x,TAB1(i).coor_y,                        TAB1(i+1).coor_x,TAB1(i+1).coor_y)-       Geomxav.Angpente(TAB1(i-1).coor_x,TAB1(i-1).coor_y,                        TAB1(i).coor_x,TAB1(i).coor_y);
    if a>PI then a:=a-2.0*PI;
    elsif a<=-PI then a:=a+2.0*PI;
    end if;
    angtotal:=angtotal+a;
  end loop;
  if angtotal>0.0 then sd:=0;
 else sd:=1;
 end if;
End SensDirect;
-----------------------------------------------------------------------------
-- Supprime les intersections d'un polygone (polyligne fermee par la base)
-- en creant un nouveau polygone en partant du point initial jusqu'au point
-- final en restant toujours au dessus de la base ou confondu avec cette
-- derniere et en revenant au point initial en restant au dessous ou confondu
-- REM = les tableaux de points sont fixes a 500 ...
-----------------------------------------------------------------------------
Procedure Supcoup(TABI          : in point_liste_type;
                  nbpt_ini      : in integer;
                  TABF          : out point_liste_type;
                  nbpt_fin      : out integer) is TAB,TABS,TABS1,TABS2   		: point_liste_type(1..500);
 PIN                                    : point_liste_type(1..2);
 i0,nbpt,nconv,k1,k2,k,i,nint,ii,itemp  : integer;
 temp                                   : float;
 INTORD,INT,SUIV,SUIVORD                : Int_tableau_type(1..500);
 DIS,LIEUX                              : Reel_tableau_type(1..500);
begin  nbpt:=nbpt_ini;
-- Fermeture de la ligne  
for i in 1..nbpt loop TAB(i):=TABI(i);
 end loop;
  nbpt:=nbpt+1;
 TAB(nbpt):=TAB(1);
-- Augmentation de la ligne des points d'intersection et calcul des lieux :  
k:=0;
  for i in 1..nbpt-2 loop    k:=k+1;
 TABS(k):=TAB(i);
    LIEUX(k):=Geomxav.lieu(TAB(i),TAB(nbpt),TAB(nbpt-1));
    if (Geomxav.Coupseg(TAB(i),TAB(i+1),TAB(nbpt),TAB(nbpt-1))) and       (not (Geomxav.Appseg(TAB(nbpt),TAB(nbpt-1),TAB(i+1)))) and       (not (Geomxav.Appseg(TAB(nbpt),TAB(nbpt-1),TAB(i)))) then      PIN:=Geomxav.Inters(TAB(i),TAB(i+1),TAB(nbpt),TAB(nbpt-1));
      k:=k+1;
 TABS(k):=PIN(1);
 LIEUX(k):=0.0;
    end if;
  end loop;
  k:=k+1;
 TABS(k):=TAB(nbpt-1);
 LIEUX(k):=0.0;
-- Tri des points d'intersection (ordre des pts de la ligne/ordre topol) :  
nint:=0;
  for i in 1..k loop    if LIEUX(i)=0.0 then      nint:=nint+1;
 INT(nint):=i;
 DIS(nint):=Geomxav.Distance(TABS(i),TAB(1));
    end if;
  end loop;
  for i in 1..nint loop INTORD(i):=INT(i);
 end loop;
  for i in 1..nint-1 loop    for ii in i+1..nint loop      if DIS(ii)<DIS(i) then        temp:=DIS(i);
 DIS(i):=DIS(ii);
 DIS(ii):=temp;
        itemp:=INTORD(i);
 INTORD(i):=INTORD(ii);
 INTORD(ii):=itemp;
      end if;
    end loop;
  end loop;
-- Tableau des suivants :  
for i in 1..nint-1 loop    SUIV(INT(i)):=INT(i+1);
 SUIVORD(INT(i)):=INTORD(i+1);
  end loop;
-- Construction du polygone a partir de la ligne : 
-- points de lieu>0.0 :  
i:=1;
 k1:=0;
  while (i<k) loop    if LIEUX(i)=0.0 then k1:=k1+1;
 TABS1(k1):=TABS(i);
 i0:=i;
      if i=k-1 then i:=k;
      else        if LIEUX(i0+1)=0.0 then k1:=k1+1;
 TABS1(k1):=TABS(i0);
 i:=i0+1;
        elsif LIEUX(i0+1)>0.0 then          for j in i0+1..SUIV(i0)-1 loop k1:=k1+1;
 TABS1(k1):=TABS(j);
 end loop;
          i:=SUIV(i0);
        else i:=SUIVORD(i0);
        end if;
      end if;
    end if;
  end loop;
  k1:=k1+1;
 TABS1(k1):=TABS(i);
 -- points de lieu<0.0 :  
 i:=1;
 k2:=0;
  while (i<k) loop    if LIEUX(i)=0.0 then k2:=k2+1;
 TABS2(k2):=TABS(i);
 i0:=i;
      if i=k-1 then i:=k;
      else        if LIEUX(i0+1)=0.0 then k2:=k2+1;
 TABS2(k2):=TABS(i0);
 i:=i0+1;
        elsif LIEUX(i0+1)<0.0 then          for j in i0+1..SUIV(i0)-1 loop k2:=k2+1;
 TABS2(k2):=TABS(j);
 end loop;
          i:=SUIV(i0);
        else i:=SUIVORD(i0);
        end if;
      end if;
    end if;
  end loop;
  k2:=k2+1;
 TABS2(k2):=TABS(k);
  for i in 1..k1 loop TABS(i):=TABS1(i);
 end loop;
  for i in 1..k2-2 loop TABS(k1+i):=TABS2(k2-i);
 end loop;
  nbpt:=k1+k2-1;
 nbpt_fin:=nbpt;
 TABS(nbpt):=TABS(nbpt-1);
 TABF:=TABS;
End Supcoup;
------------------------------------------------------------------
-- Calcul de l'enveloppe convexe d'un polygone qui ne se coupe pas
-- REM = les tableaux de points sont fixes a 500 ...
------------------------------------------------------------------
Procedure Enveloppe ( TABL              : in point_liste_type;
                      np                : in integer;
                      TABC              : out point_liste_reel;
                      nc                : out integer) is boucle,nconv,im,i,k,nbpt,nf,ider,sd     : integer;
 angle,angle0,yy,ym,dist,dist0,y,x,d     : float;
 TAB0,TAB,TABC0,TAB1                     : point_liste_reel(1..500);
 L,L1,L2,U                               : Int_tableau_type(1..500);
begin--> Initialisations -- TAB0 = TABL en coordonnees reelles  
nbpt:=np;
  for i in 1..nbpt loop    TAB0(i).coor_x:=float(TABL(i).coor_x);
    TAB0(i).coor_y:=float(TABL(i).coor_y);
  end loop;
 -- TAB = polygone de travail  
 TAB:=TAB0;
 -- Calcul du point d'ordonnee minimale ym (abscisse minimale si plusieurs)  
 ym:=TAB(1).coor_y;
 im:=1;
  for i in 2..nbpt loop    if (ym>TAB(i).coor_y) or       ((ym=TAB(i).coor_y) and (TAB(im).coor_x>TAB(i).coor_x)) then      ym:=TAB(i).coor_y;
 im:=i;
    end if;
  end loop;
 -- Translation + permutation au point d'ordonnee minimale 
  TAB:=Geomxav.Transl(TAB,nbpt,im);
  for i in im..nbpt loop L1(i-im+1):=i;
 end loop;
  if im>1 then    for i in 1..im-1 loop L1(nbpt-im+i+1):=i;
 end loop;
  end if;
  nbpt:=nbpt+1;
 TAB(nbpt):=TAB(1);
  Geomxav.sensdirect(TAB,nbpt,sd);
  if sd=1 then    for i in 1..nbpt loop L2(i):=L1(nbpt+1-i);
 TAB1(i):=TAB(nbpt+1-i);
 end loop;
    L1:=L2;
 TAB:=TAB1;
  end if;
  TAB1:=TAB;
 -- Calcul de l'enveloppe convexe  
 L(1):=im;
 k:=1;
 boucle:=0;
 i:=1;
 U(1):=1;
 ider:=1;
  for i in 2..nbpt loop U(i):=0;
 end loop;
  while (i<nbpt) and (boucle=0) loop   -- Calcul du point suivant de l'enveloppe convexe (sens direct)    
  angle0:=7.0;
 dist0:=0.0;
    if i<nbpt then      if i=1 then nf:=nbpt-1;
 else nf:=nbpt;
 end if;
      for j in i+1..nf loop        if U(j)=0 then          angle:=Geomxav.Angpente(TAB(i).coor_x,TAB(i).coor_y,                                  TAB(j).coor_x,TAB(j).coor_y);
          dist:=Geomxav.Distance(TAB(i),TAB(j));
          if angle<0.0 then angle:=2.0*PI+angle;
 end if;
          if abs(angle-angle0)<1.0E-3 then            if dist>dist0 then im:=j;
 dist0:=dist;
 end if;
          elsif angle<angle0 then im:=j;
 angle0:=angle;
 dist0:=dist;
          end if;
        end if;
      end loop;
    end if;
   -- Ordre des points de l'enveloppe convexe ou fin    
   if im=nbpt then boucle:=1;
    else k:=k+1;
 L(k):=L1(im);
 i:=im;
 U(im):=1;
     -- Rotation et translation de TAB pour TAB(im)=0 et yTAB0(ider)=0      
     TAB:=Geomxav.rot2(TAB1,nbpt,im,ider);
 ider:=im;
    end if;
  end loop;
  nconv:=k;
 -- TABC0 = polygone de l'enveloppe convexe  
 for i in 1..nconv loop TABC0(i):=TAB0(L(i));
 end loop;
  TABC:=TABC0;
 nc:=nconv;
End Enveloppe;
--------------------------------------------------------------------------
-- Calcul de l'enveloppe convexe d'un polygone qui eventuellement se coupe
-- REM = les tableaux de points sont fixes a 500 ...
--------------------------------------------------------------------------
Procedure Envconvligne (TABL	: in point_liste_type;
                        np	: in integer;
                        TABC	: out point_liste_reel;
                        nc	: out integer) is TABS	: point_liste_type(1..500);
 nbpt	: integer;
Begin  Supcoup(TABL,np,TABS,nbpt);
  Geomxav.Enveloppe(TABS,nbpt,TABC,nc);
End Envconvligne;
-----------------------------------------------
-- Recherche du point ayant l'abscisse maximale
-----------------------------------------------
Procedure Rechminxa (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Mxas  : out Gen_io.point_type_reel;
                        rMxas : out integer) is x,y    : float;
 rMxa   : integer;
Begin  x:=-1.0E+38;
  for i in 1..nbpt loop    if TAB(i).coor_x>x then x:=TAB(i).coor_x;
 y:=TAB(i).coor_y;
 rMxa:=i;
    elsif TAB(i).coor_x=x then      if TAB(i).coor_y>y then y:=TAB(i).coor_y;
 rMxa:=i;
 end if;
    end if;
  end loop;
  Mxas:=TAB(rMxa);
 rMxas:=rMxa;
End Rechminxa;
-----------------------------------------------
-- Recherche du point ayant l'abscisse minimale
-----------------------------------------------
Procedure Rechminxi (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Mxis  : out Gen_io.point_type_reel;
                        rMxis : out integer) is x,y    : float;
 rMxi   : integer;
Begin  x:=1.0E+38;
  for i in 1..nbpt loop    if TAB(i).coor_x<x then x:=TAB(i).coor_x;
 y:=TAB(i).coor_y;
 rMxi:=i;
    elsif TAB(i).coor_x=x then      if TAB(i).coor_y<y then y:=TAB(i).coor_y;
 rMxi:=i;
 end if;
    end if;
  end loop;
  Mxis:=TAB(rMxi);
 rMxis:=rMxi;
End Rechminxi;
-----------------------------------------------
-- Recherche du point ayant l'ordonnee minimale
-----------------------------------------------
Procedure Rechminyi (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Myis  : out Gen_io.point_type_reel;
                        rMyis : out integer) is x,y    : float;
 rMyi   : integer;
Begin  y:=1.0E+38;
  for i in 1..nbpt loop    if TAB(i).coor_y<y then x:=TAB(i).coor_x;
 y:=TAB(i).coor_y;
 rMyi:=i;
    elsif TAB(i).coor_y=y then      if TAB(i).coor_x>x then x:=TAB(i).coor_x;
 rMyi:=i;
 end if;
    end if;
  end loop;
  Myis:=TAB(rMyi);
 rMyis:=rMyi;
End Rechminyi;
-----------------------------------------------
-- Recherche du point ayant l'ordonnee maximale
-----------------------------------------------
Procedure Rechminya (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Myas  : out Gen_io.point_type_reel;
                        rMyas : out integer) is x,y    : float;
 rMya   : integer;
Begin  y:=-1.0E+38;
  for i in 1..nbpt loop    if TAB(i).coor_y>y then x:=TAB(i).coor_x;
 y:=TAB(i).coor_y;
 rMya:=i;
    elsif TAB(i).coor_y=y then      if TAB(i).coor_x<x then x:=TAB(i).coor_x;
 rMya:=i;
 end if;
    end if;
  end loop;
  Myas:=TAB(rMya);
 rMyas:=rMya;
End Rechminya;
-----------------------------------------------------------------
-- Rotation du polygone TAB autour de TAB(1) pour que TAB(im).x=0
-- Translation du polygone TAB pour que TAB(im):=(0,0)
-- Permutation de TAB pour que TAB(1):=TAB(im)
-----------------------------------------------------------------
Function Rot(   TAB0    : point_liste_reel;
                nbpt,im : integer) return point_liste_reel is i,k            : integer;
 TAB,TAB1       : point_liste_reel(1..500);
 d,c,s,x,y      : float;
 M              : Gen_io.point_type_reel;
Begin -- Rotation  
d:=Geomxav.Distance(TAB0(1),TAB0(im));
  c:=TAB0(im).coor_x/d;
 s:=TAB0(im).coor_y/d;
  for i in 1..nbpt loop    x:=TAB0(i).coor_x;
 y:=TAB0(i).coor_y;
    TAB(i).coor_x:=c*x+s*y;
 TAB(i).coor_y:=c*y-s*x;
  end loop;
 -- Translation et permutation  
 k:=0;
  for i in im..nbpt loop    k:=k+1;
    TAB1(k).coor_x:=TAB(i).coor_x-TAB(im).coor_x;
    TAB1(k).coor_y:=TAB(i).coor_y-TAB(im).coor_y;
  end loop;
  for i in 1..im-1 loop    k:=k+1;
    TAB1(k).coor_x:=TAB(i).coor_x-TAB(im).coor_x;
    TAB1(k).coor_y:=TAB(i).coor_y-TAB(im).coor_y;
  end loop;
  return TAB1;
End Rot;
-----------------------------------------------------------------
-- Rotation du polygone TAB autour de TAB(1) pour que TAB(im).x=0
-----------------------------------------------------------------
Function Rot1(  TAB0    : point_liste_reel;
                nbpt,im : integer) return point_liste_reel is
i              : integer;
 TAB            : point_liste_reel(1..500);
 d,c,s,x,y      : float;
Begin
  d:=Geomxav.Distance(TAB0(1),TAB0(im));
  if d/=0.0 then
	c:=TAB0(im).coor_x/d;
	s:=TAB0(im).coor_y/d;
    for i in 1..nbpt loop
		x:=TAB0(i).coor_x;
		y:=TAB0(i).coor_y;
		TAB(i).coor_x:=c*x+s*y;
		TAB(i).coor_y:=c*y-s*x;
    end loop;
  else 
	TAB:=TAB0;
  end if;
  return TAB;
End Rot1;
-------------------------------------
-- Calcul des parametres du rectangle
-------------------------------------
Procedure Parect(TABC           : in point_liste_reel;
                 nconv,rmin     : in integer;
                 RECT           : in out point_liste_reel;
                 PTS            : out Int_tableau_type;
                 angle          : out float) is 
i,minx,miny,maxx,maxy  : integer;
 TABC1,TABC2            : point_liste_reel(1..500);
 Mnx,Mny,Mxx,Mxy,M      : Gen_io.point_type_reel;
 c,s,d,x,y              : float;
Begin
 -- Translation pour que TABC(rmin)=0
  for i in 1..nconv loop
	TABC1(i).coor_x:=TABC(i).coor_x-TABC(rmin).coor_x;
    TABC1(i).coor_y:=TABC(i).coor_y-TABC(rmin).coor_y;
  end loop;
 -- Rotation pour que TABC(rmin+1).coor_y=0  
  d:=Geomxav.Distance(TABC1(rmin),TABC1(rmin+1));
  if d/=0.0 then
	c:=TABC1(rmin+1).coor_x/d;
	s:=TABC1(rmin+1).coor_y/d;
    for i in 1..nconv loop
		x:=TABC1(i).coor_x;
		y:=TABC1(i).coor_y;
		TABC2(i).coor_x:=c*x+s*y;
		TABC2(i).coor_y:=c*y-s*x;
    end loop;
  else
	TABC2:=TABC1;
  end if;
 -- Parametres du rectangle
  Geomxav.Rechminxa(TABC2,nconv,Mxx,maxx);
  Geomxav.Rechminxi(TABC2,nconv,Mnx,minx);
  Geomxav.Rechminya(TABC2,nconv,Mxy,maxy);
  Geomxav.Rechminyi(TABC2,nconv,Mny,miny);
  M.coor_x:=Mnx.coor_x;
  M.coor_y:=Mny.coor_y;
  RECT(1).coor_x:=c*M.coor_x-s*M.coor_y+TABC(rmin).coor_x;
  RECT(1).coor_y:=s*M.coor_x+c*M.coor_y+TABC(rmin).coor_y;
  M.coor_x:=Mxx.coor_x;
  M.coor_y:=Mny.coor_y;
  RECT(2).coor_x:=c*M.coor_x-s*M.coor_y+TABC(rmin).coor_x;
  RECT(2).coor_y:=s*M.coor_x+c*M.coor_y+TABC(rmin).coor_y;
  M.coor_x:=Mxx.coor_x;
  M.coor_y:=Mxy.coor_y;
  RECT(3).coor_x:=c*M.coor_x-s*M.coor_y+TABC(rmin).coor_x;
  RECT(3).coor_y:=s*M.coor_x+c*M.coor_y+TABC(rmin).coor_y;
  M.coor_x:=Mnx.coor_x;
  M.coor_y:=Mxy.coor_y;
  RECT(4).coor_x:=c*M.coor_x-s*M.coor_y+TABC(rmin).coor_x;
  RECT(4).coor_y:=s*M.coor_x+c*M.coor_y+TABC(rmin).coor_y;
  PTS(1):=minx;
  PTS(2):=miny;
  PTS(3):=maxx;
  PTS(4):=maxy;
  if (Mxx.coor_x-Mnx.coor_x)<(Mxy.coor_y-Mny.coor_y) then
	angle:=Geomxav.Angpente(TABC(rmin).coor_x,TABC(rmin).coor_y,TABC(rmin+1).coor_x,TABC(rmin+1).coor_y)+PI/2.0;
  else 
	angle:=Geomxav.Angpente(TABC(rmin).coor_x,TABC(rmin).coor_y,  TABC(rmin+1).coor_x,TABC(rmin+1).coor_y);
  end if;
End Parect;
----------------------------------------
-- Calcul du rectangle englobant minimal
----------------------------------------
Procedure Rectanglob (TABC              : in point_liste_reel;
                      nconv             : in integer;
                      RECTANGLE         : out bloc_type;
                      airemin           : out float;
                      orientprinc       : out float) is
minx,miny,maxx,maxy,rmin,i             : integer;
 aire,angle,aire1,y,x,d                 : float;
 TABC0,TABC1,TABC2                      : point_liste_reel(1..500);
 RECT                                   : point_liste_reel(1..5);
 PTS                                    : Int_tableau_type(1..5);
 M                                      : Gen_io.point_type_reel;
begin
--> Calcul du rectangle minimal
  aire:=1.0E+38;
 TABC0:=TABC;
  for i in 1..nconv loop
  -- Changement de repere, origine en TABC(i) et permutation TABC(1) <- TABC(i)
	TABC1:=Transl(TABC0,nconv,i);
   -- Changement de repere (rotation), TABC(2).coor_X <- 0.0
	TABC2:=Geomxav.Rot1(TABC1,nconv,2);
   -- Recherche des minima et maxima
	Geomxav.Rechminxa(TABC2,nconv,M,maxx);
    Geomxav.Rechminxi(TABC2,nconv,M,minx);
    Geomxav.Rechminya(TABC2,nconv,M,maxy);
    Geomxav.Rechminyi(TABC2,nconv,M,miny);
   -- Calcul du rectangle
	Aire1:=(TABC2(maxx).coor_x-TABC2(minx).coor_x)* (TABC2(maxy).coor_y-TABC2(miny).coor_y);
    if aire1<aire then 
		rmin:=i;
		aire:=aire1;
	end if;
  end loop;
--> Calcul des parametres du rectangle minimal
  Parect(TABC0,nconv,rmin,RECT,PTS,angle);
  y:=1.0E+38;
  for i in 1..4 loop
	if RECT(i).coor_y<y then
		x:=RECT(i).coor_x;
		y:=RECT(i).coor_y;
		miny:=i;
    elsif abs(RECT(i).coor_y-y)<1.0E-3 then
		if RECT(i).coor_x<x then
			x:=RECT(i).coor_x;
			miny:=i;
		end if;
    end if;
  end loop;
  RECTANGLE.P1.coor_x:=integer(RECT(miny).coor_x);
  RECTANGLE.P1.coor_y:=integer(RECT(miny).coor_y);
  x:=-1.0E+38;
  for i in 1..4 loop
	if RECT(i).coor_x>x then
		x:=RECT(i).coor_x;
		y:=RECT(i).coor_y;
		maxx:=i;
    elsif abs(RECT(i).coor_x-x)<1.0E-3 then
		if RECT(i).coor_y<y then
			y:=RECT(i).coor_y;
			maxx:=i;
		end if;
    end if;
  end loop;
  RECTANGLE.P2.coor_x:=integer(RECT(maxx).coor_x);
  RECTANGLE.P2.coor_y:=integer(RECT(maxx).coor_y);
  y:=-1.0E+38;
  for i in 1..4 loop
	if RECT(i).coor_y>y then
		x:=RECT(i).coor_x;
		y:=RECT(i).coor_y;
		maxy:=i;
    elsif abs(RECT(i).coor_y-y)<1.0E-3 then
		if RECT(i).coor_x>x then
			x:=RECT(i).coor_x;
			maxy:=i;
		end if;
    end if;
  end loop;
  RECTANGLE.P3.coor_x:=integer(RECT(maxy).coor_x);
  RECTANGLE.P3.coor_y:=integer(RECT(maxy).coor_y);
  x:=1.0E+38;
  for i in 1..4 loop
	if RECT(i).coor_x<x then
		x:=RECT(i).coor_x;
		y:=RECT(i).coor_y;
		minx:=i;
    elsif abs(RECT(i).coor_x-x)<1.0E-3 then
		if RECT(i).coor_y>y then
			y:=RECT(i).coor_y;
			minx:=i;
		end if;
    end if;
  end loop;
  RECTANGLE.P4.coor_x:=integer(RECT(minx).coor_x);
  RECTANGLE.P4.coor_y:=integer(RECT(minx).coor_y);
  airemin:=aire;
 orientprinc:=angle;
End Rectanglob;
End Geomxav;
