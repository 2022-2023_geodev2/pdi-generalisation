with Gen_io; use Gen_io;
With Pi_et_sommet; use PI_et_sommet;
                     
Package Schematisation is
       procedure SCHEMAT (	ligne		  : IN point_liste_type;
	   ligne_schematise  : OUT point_access_type;
	   Auto_sigma	  : IN boolean :=true;
	   Sigma		  : IN float := 20.0;
	   Auto_choix        : IN boolean :=true;
	   virag1		  : IN OUT integer;
		virag2		  : IN OUT integer;
      Det_pi		  : IN Type_detection_PI                                            := Milieu_des_sommets;
	  Det_som		  : IN Type_detection_sommets					    := Max_courbure_MB);
	  
	  
	  procedure SCHEMAT_SELECT (      ligne              : IN point_liste_type;
	  coord_pi           : IN point_access_type;
	  points_pred_pi      : IN liens_access_type;
	  coord_sommets      : IN point_access_type;
	  points_pred_sommets : IN liens_access_type;
	  vi1	           : OUT integer;
	  vi2                : OUT integer);                                                
	  procedure SCHEMAT_MESURES(ligne         	: IN point_liste_type;
	  coord_pi      	: IN point_liste_type;
	  points_pred_pi 	: IN liens_array_type;
	  coord_sommets  	: IN point_liste_type;
	  points_pred_sommets 	: IN liens_array_type;
	  L_int    		: OUT liens_array_type;
	  card1    		: OUT integer;
	  L_aut    		: OUT liens_array_type;
	  card2    		: OUT integer	);
	  -- Ancienne procedure de schematisation a ne plus utiliser 
	  procedure SCHEMAT_EXTREMITE (  ligne             : IN point_liste_type;
	  n_points          : IN integer;
	  Nb_virage_elimine : IN integer;
	  SIGMA             : IN float;
	  ligne_schematise  : IN OUT point_liste_type;
	  np                : IN OUT integer);
	  
end schematisation;                    