----------------------------------------------------
-- GALBE
----------------------------------------------------
-- Premier essai de choix automatique d'algorithme
-- en fonction uniquement du type de conflit
----------------------------------------------------
with gen_io; use gen_io;

Package GALBE is

Type Type_parametres is array(integer range <>) of float;

-- Parametres(1) : Separabilite du premier decoupage
-- Parametres(2) : Sigma du lissage des zones sans conflit
-- Parametres(3) : Exageration de faille max
-- Parametres(4) : Exageration de l'accordeon
-- Parametres(5) : Separabilite du deuxieme decoupage
-- Parametres(6) : Exageration de faille min
-- Parametres(7) : amortissement des deplacements
-- Parametres(8) : critere d'ecart maxi de l'accordeon (=P(8)*Largeur_symb) 
-- Parametres(9) : nombre maximal de schematisations
-- Parametres(10): Seuil de Douglas et Peucker pour le filtrage final
-- Parametres(11): Sigma pour Gauss dans le lissage final
-- Parametres(12): Facteur de largeur initiale pour Faille Min 
--                 Largeur_symb_initiale:=(P(12)*Largeur_symb_finale

Parametres_defaut : Type_parametres(1..12) := 
              (2.5, 11.0, 1.2, 1.2, 0.0, 1.2, 0.05, 2.0, 5.0, 0.05, 2.5, 0.0);

-- Correspondance = True <=> creation d'un fichier de correspondance entre les
-- numeros des arcs avant et apres
procedure Galbage(Graphe     :  in out graphe_type;
                  Legende    :  in Legende_type;
                  Rappech    :  in float;
                  Arc        :  in natural;
                  Parametres :  in Type_parametres := parametres_defaut);


----------------------------------------------------------------------
-- Galbe avec trou realise par le stagiaire Bruneau en mars 2001
--
procedure Galbage_trou (Graphe     :  in out graphe_type;
                  Legende    :  in Legende_type;
                  Rappech    :  in float;
                  Arc        :  in natural;
                  Parametres :  in Type_parametres := parametres_defaut);
 



end GALBE;

