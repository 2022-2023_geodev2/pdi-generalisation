with Gen_io; 		  use Gen_io;
With math_cogit;	  use math_cogit;
With text_io; 		  use text_io;
-- *****************************************************************
-- ** Package de decoupage et de calcul de mesures sur des lignes **
-- *****************************************************************
Package mesure_ligne is subtype string7 is string(1..7);
 Type nom_presence_mesure_type is record	nom_mesure: string7;
 	presence  : boolean := False;
 end record;
--
 type   Matrice_mesures_Type is array(natural range <>,natural range <>) of float;
 type      Vect_mesures_Type is array(natural range <>) of nom_presence_mesure_type;
 type     Vect_num_arcs_Type is array(natural range <>) of integer;
 type    Vect_noms_arcs_Type is array(natural range <>) of string7;
--
 type   Matrice_mesures_Access_Type is access Matrice_mesures_type;
 type   vect_mesures_Access_Type is access vect_mesures_type;
 type   vect_num_arcs_Access_Type is access vect_num_arcs_type;
 type   vect_noms_arcs_Access_Type is access vect_noms_arcs_type;
 -- ========================================================== -- 
 -- PARAMETRES de determination des PI et des Points critiques -- 
 -- ========== 
 -- Seuil_dist_entre_pi_homogenes: integer := 2;
 -- 2 dist. entre PI moyennes !!!
 -- =======================================================
 -- Procedure de mesure de resistance au lissage
 --   Methode suggeree par Rosin
 --   Mais S(Sigma) (= significance en anglais) est ici
 --   approximee a S(Sigma) = sigma * nb de pts d'inflexion
 -- =======================================================
Function Resistence (  	ligne 		: in point_liste_type;
		       	npts		: in natural;
 			sigma_ini	: in float   ) return float;
Procedure Resistence (	ligne 		: in point_liste_type;
		        npts		: in natural;
			sigma_max	: in out float;
			ncourb		: In Out natural;
			Tcourb		: In Out reel_tableau_type ) ;
 -- ==========================================================================
-- Choix automatique de la valeur de SIGMA : 
-- Proportionnel a la longueur de la ligne : lgr/100 * LN(lgr/100)  
-- ========================================================================== 
Procedure CHOIX_SIGMA( ligne		: IN point_liste_type;
			nb_points	: IN integer;
			Sigma		: IN OUT float );
 -- ==========================================================================
-- Test d'homogeneite avant decoupage : 
-- ========================================================================== 
Procedure TEST_HOMOGENEITE (  nb_virages	: IN  integer;
				Tpinflex  	: IN  point_liste_type;
				HOMOGENEITE	: OUT boolean );
 -- ==========================================================================
-- Decoupages en segments DROITS / SINUEUX : 
-- ========================================================================== 
Procedure DECOUPAGE (  nb_virages	 : in     integer;
			Tpinflex  	 : in     point_liste_type;
		ligne 		   	 : in     point_liste_type;
		Tpoints_pred_pi	   	 : in     liens_access_type;
			nb_pts_critiques : in out integer;
			Tpcritiques  	 : in out liens_array_type );
 -- ============================================================================
-- Mesures simples basees sur les PI et les sommets 
-- ============================================================================ 
Procedure MESURES( ligne 		: in point_liste_type;
		    npts		: in natural;
  		    nb_pts_inflex	: in natural;
  		    Tpoints_pred_pi	: in liens_access_type;
  		    Tpinflex	 	: in point_liste_type;
    		    largeur_symbole	: in float;
		    f			: in file_type);
 -- La meme, mais sort une matrice de mesures au lieu d'ecrire dans un fichier : 
 Procedure MESURES( ligne 		: in point_liste_type;
		    npts		: in natural;
  		    nb_pts_inflex	: in natural;
  		    Tpoints_pred_pi	: in liens_access_type;
  		    Tpinflex	 	: in point_liste_type;
  		    Tab_sommets		: in point_liste_type;
  		    Nb_sommets 		: in integer;
    		    largeur_symbole	: in float;
		    arc			: in integer;
		    vect_mesures	: in vect_mesures_type;
		    nb_mesures_total	: in integer;
		    matrice_mesures	: in out matrice_type);
-- ==========================================================================
-- Procedure de classification d'un ensemble de troncons d'arcs d'un graphe
-- (donc deja segmentes) 
--                       -> La procedure MESURES calcule les mesures choisies 
--                          dans le fichier PRESENCE_MESURES.DAT
-- 			 -> Les troncons sont classes par KMEANS
-- ==========================================================================
procedure Classification ( graphe 	: in graphe_type;
			   legende   	: in legende_type;
			   tab_arcs	: in liens_array_type;
			   narc 	: in integer;
			   nb_clusters 	: in integer;
	                           fic_mesure   : in out file_type;
 			   cluster      : in out liens_array_type );
end ;
                           
