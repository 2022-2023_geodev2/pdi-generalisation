---------------------------
-- GALBE
---------------------------
-- Choix automatique d'algorithme
-- en fonction uniquement du type de conflit
---------------------------
-- SM - FL
-- derniere modif: 03/03/99
---------------------------

With Propagation;
with Morpholigne;use morpholigne;
with lissage_filtrage; use lissage_filtrage;
with Schematisation; 
with Geometrie; use Geometrie;
with caricature; 
With math_int_basic;
with text_IO; use text_IO;
with mesures_comparaison; use mesures_comparaison;

Package body GALBE is

-- Parametres(1) : Separabilite du premier decoupage
-- Parametres(2) : Sigma du lissage des zones sans conflit
-- Parametres(3) : Exageration de faille max
-- Parametres(4) : Exageration de l'accordeon
-- Parametres(5) : Separabilite du deuxieme decoupage
-- Parametres(6) : Exageration de faille min
-- Parametres(7) : amortissement des deplacements
-- Parametres(8) : critere d'ecart maxi de l'accordeon (=P(8)*Lareur_symb) 
-- Parametres(9) : nombre maximal de schematisations
-- Parametres(10): Seuil de Douglas et Peucker pour le filtrage apres fusion
-- Parametres(11): Sigma pour Gauss dans le lissage final apres fusion 
-- Parametres(12): Facteur de largeur initiale pour Faille Min
--                 Largeur_symb_initiale:=(P(12)*Largeur_symb_finale

Package int_io is new integer_io(integer); use int_io;
Package flo_io is new float_io(float); use flo_io;

Procedure Recollage(Graphe  : in out Graphe_type;
                    Num_arc : in Natural;
                    Amortissement : in float) is

Num_noeud : natural;
nb_arc_deplace : integer;
tab_arc_deplace,tab_arc_deplacant : liens_array_type(1..2*Gr_infos(graphe).nb_arcs)
                                   :=(others=>0);
Arcd : Arc_type;

begin
  nb_arc_deplace :=1;
  Tab_arc_deplace(nb_arc_deplace):=Num_arc;
  Tab_arc_deplacant(nb_arc_deplace):=Num_arc;
  Propagation.Propagation_globale(Graphe,tab_arc_deplace,tab_arc_deplacant,
          nb_arc_deplace,Amortissement,Propagation.Propagation_iterative);

end;
  
-----------------------------------
-- GALBAGE
-----------------------------------
-- PROCEDURE EXPORTEE
-----------------------------------
procedure Galbage(Graphe     :  in out graphe_type;
                  Legende    :  in Legende_type;
                  Rappech    :  in float;
                  Arc        :  in natural;
                  Parametres :  in Type_parametres := parametres_defaut) is


Grinf        : Info_type;
Tab_arcs     : Liens_access_type;
Tab_filtre,
Tabini,
Tabfin,
Tabfin2,
Tabschem,
Pts_dec      : Point_access_type;
Arcini       : Arc_type;

Conf         : Force_conflit;
Fin          : boolean := false;
Tab_noeuds, 
Tab_accor,
Tab_fmin,
Tab_fmax,
Tab_liss     : Liens_array_type(1..1000);
-- NB BIDOUILLE: on suppose que 1 arc ne sera pas decoupe en plus de 1000 parties

Nb_noeuds,
Nb_accor,
Nb_fmin,
Nb_fmax,
Nfiltre,
Nbini, 
Nfin, 
Nfin2,
Nb_schem,
nv,
Num,
Arc_a_decouper,
arc_debut,
arc_fin,
Nb_liss      : natural := 0;
Lini, 
Lfin : float :=0.0;
Deuxieme_passe : boolean;
virage1,virage2 : integer :=0;

-- FL : Optimisation des traitements :
Gr_interm : graphe_type;
grinf_interm : Info_type;
Noeud_ini, Noeud_fin,Numarc_interm : integer;

-- A T T E N T I O N  : Signification du parametre(12)
-- Coefficcient correcteur lie a la symbolisation
-- Param(12) := Inverse de l'echelle de la base lisible cartographiquement /
--              Inverse de l'Echelle stockee dans le graphe
--                      
-- Exemple, BD CArto : Correction_echelle:= 50 000 / 100 000 = 0.5


begin

  -- 2eme begin pour gerer les plantages par exception

    --------------------------------------------------
    -- Lecture des infos de description de l'arc en entree
    Grinf := Gr_infos(Graphe);
    Arcini := GR_ARC(Graphe,Arc);


    -- Largeur de symbole aux echelles finales et initales (en pt PlaGe)
    if GR_LEGENDE_L(Legende,Arcini.att_graph).largeur =
       GR_LEGENDE_L(Legende,Arcini.att_graph).larg_int then
       -- cas ou il n'y a pas de bordure noir
       -- (largeur totale + seuil de lisibilite)
       Lfin := (GR_LEGENDE_L(Legende,Arcini.att_graph).largeur+0.1)
               * Rappech * float(Grinf.resolution);
       Lini := Lfin / Rappech * Parametres(12);
    else 
       -- cas ou il y a une bordure noir
       -- (largeur interne + largeur du bord noir = Int + (ext-int)/2
       Lfin :=(GR_LEGENDE_L(Legende,Arcini.att_graph).largeur / 2.0 +
               GR_LEGENDE_L(Legende,Arcini.att_graph).larg_int / 2.0 )
               * Rappech * float(Grinf.resolution); 
       Lini := Lfin / Rappech * Parametres(12);
    end if;                 

    -- liste des points de l'arc en entree
    Tabini := new Point_liste_type(1..Grinf.max_pts_arc);
    Gr_points_d_arc(Graphe,Arc,Tabini.all,Nbini);
    if Nbini < 3 then  
       GR_free_point_liste(Tabini); 
       return; 
    end if; 


    -- Il faut gerer des problemes d'optimisation de temps de calcul :
    -- 80% des arcs sont juste a lisser : traitement direct dans le graphe
    -- 20% des arcs sont a decouper : gestion dans un nouveau graphe provisoire
    Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);
    if Conf = Conflit_nul then
       -- Arc juste a lissser
       if parametres(2)/=0.0 or parametres(10)/=0.0 then    
          Tabfin := new Point_liste_type(1..Nbini);
          Lissage_filtrage.Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,
                                             Parametres(2),Tabfin.all);
          tabini(1..Nbini):=tabfin(1..Nbini);
          lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,
                                          Parametres(10),grinf.resolution);
          gr_mod_points_d_arc(Graphe,Arc,Tabfin(1..nfin),nfin);
       end if;
       GR_free_point_liste(Tabini); 
       GR_free_point_liste(Tabfin); 
       return;
    end if;


    -- Creation d'un graphe intermediaire pour optimiser les temps de calcul 
    -- dans lequel on met l'arc initial
    gr_interm:=GR_CREATE_NEW(Grinf.delta_x,grinf.delta_y,grinf.resolution,
                           grinf.echelle,300,300,10,"Gr_interm");
    Gr_creer_noeud_sur_rien(gr_interm,tabini(1),true,0.0,0,noeud_ini);
    if tabini(1)=tabini(Nbini) then
       Noeud_fin:=noeud_ini;
    else
       Gr_creer_noeud_sur_rien(gr_interm,tabini(Nbini),true,0.0,0,noeud_fin);
    end if;
    Gr_creer_arc(gr_interm,noeud_ini,noeud_fin,tabini(1..nbini),nbini,Ligne,
               Arcini.att_graph,numarc_interm);


    --------------------------------------------------
    -- decoupage de l'arc en zone homgenes de conflit

    -- determination des points de decoupage de l'arc en premiere passe
    -- NB: meme si l'exageration des caricature est superieure a 1.0, le
    -- decoupage se fait avec la largeur du symbole non exagere.
    -- NB: Pour le decoupage on passe la DEMI largeur de symbole
    Pts_dec := Morpholigne.Decoupage_conflits(Tabini(1..Nbini),
                                  Lfin/2.0*parametres(3),Parametres(1),1.7);

    -- Premier decoupage de l'arc, creation du tableau des nouveaux 
    -- numeros d'arc Tab_arcs
    Tab_arcs := new Liens_array_type(1..Pts_dec'length-1);
    Arc_a_decouper := NumArc_interm;   
    for i in Pts_dec'first+1..Pts_dec'last-1 loop 	
       -- creation des sous-arcs
      Gr_creer_noeud_sur_arc(Gr_interm,Arc_a_decouper,Pts_dec(i),False,0.0,0,
                             Num, arc_debut, arc_fin);
      Tab_arcs(i-1) := Arc_debut;
      Arc_a_decouper := Arc_fin;
      Nb_noeuds := Nb_noeuds + 1;
      Tab_noeuds(Nb_noeuds) := Num;
    end loop;
    Tab_arcs(Tab_arcs'last) := Arc_a_decouper;
    GR_free_point_liste(Pts_dec);  

  --------------------------------------------------
  -- Boucle sur les arcs decoupes en premiere passe
  -- Evaluation des conflits de chaque arc
  for i in Tab_arcs'range loop  
    Gr_points_d_arc(Gr_interm,Tab_arcs(i),Tabini.all,Nbini);    
    -- filtrage des points doubles
    if Nbini = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Geometrie.Filtrage(Tabini(1..Nbini));
      Nfiltre := Tab_filtre'length;
      if Nbini /= Nfiltre then
         GR_mod_points_d_arc(Gr_interm,Tab_arcs(i),Tab_filtre.all,
                            Tab_filtre'length);
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre.all;
      end if;
      GR_free_point_liste(Tab_filtre);
      -- evaluation du conflit
      -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
      Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);

      -- classement des arcs en fonction des traitements a faire
      if Conf = Conflit_nul then
        Nb_liss := Nb_liss + 1;
        Tab_liss(Nb_liss) := Tab_arcs(i);
      elsif Conf(Gauche) = 0.0 or Conf(Droite) = 0.0 then
        Nb_fmax := Nb_fmax + 1;
        Tab_fmax(Nb_fmax) := Tab_arcs(i);
      else    
        Nb_accor := Nb_accor + 1;
        Tab_accor(Nb_accor) := Tab_arcs(i);
      end if;
    end if;
  end loop; -- fin de la boucle de determination du type de conflit
  GR_free_liens_array(Tab_arcs);

  --------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par accordeon
  -- ou schematisation
  for i in 1..Nb_accor loop
    Gr_points_d_arc(Gr_interm,Tab_accor(i),Tabini.all,Nbini);    
    if tabini(1)=tabini(Nbini) then
       -- L'accordeon deplacant les 2 extremites de l'arc de facon distincte
       -- l'algorithme ne peut fonctionner pour ce type d'arc
       exit;
    end if;
    -- calcul de la nouvelle geometrie
    Tabfin2 := new Point_liste_type(1..Grinf.Max_pts_arc);
    Tabfin2(1..Nbini) := Tabini(1..Nbini);
    Nfin2 := Nbini;
    Fin := false;
    Nb_schem := 1;

    while not Fin and Nb_schem <=integer(Parametres(9))+1 loop
      if Nb_schem = integer(Parametres(9))+1 then
         Nfin := Nbini;
         Tabfin := new Point_liste_type(1..Nfin);
         Tabfin(1..Nfin) := Tabini(1..Nbini);
         Fin := true;
      else
         -- NB: Pour l'accordeon on passe la largeur TOTALE de symbole
         Caricature.Accordeon(Tabfin2(1..Nfin2),Lfin*Parametres(4), tabfin);
         Nfin:=tabfin'length;

         -- filtrage des points doubles apres accordeon
         Tab_filtre := Filtrage(Tabfin(1..Nfin));
         GR_free_point_liste(Tabfin);
         Nfin:=Tab_filtre'length;
         Tabfin:=New Point_liste_type'(Tab_filtre(1..Nfin));

         if Intersections(Tabfin(1..Nfin),Nfin) /= 0 or
            Math_int_basic.Max(Norme(Tabini(1),Tabfin(1)),
                Norme(Tabini(nbini),Tabfin(Nfin))) > Parametres(8)*Lfin then
-- Solution avec Hausdorff au lieu des extremites
--            Hausdorff(Tabini(1..Nbini), Tabfin(1..Nfin), Nbini, Nfin) >
--                                                      Parametres(8)*Lfin then

            -- NB: la largeur de symbole n'intervient pas pour la schematisation
            GR_free_point_liste(Tabfin);
            Schematisation.Schemat ( Tabfin2(1..Nfin2),Tabschem,true,10.0,true,
                                     virage1,virage2);
            GR_free_point_liste(Tabfin2);
            Nfin2:=Tabschem'length;
            Tabfin2:=New Point_liste_type'(Tabschem(1..Nfin2));
            Nb_schem := Nb_schem +1;
         else
            Fin := true;
         end if;
      end if;
    end loop;
    GR_free_point_liste(Tabfin2);

    -- filtrage des points doubles apres accordeon et schematisation
    if Nfin = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabfin(1..Nfin));
    end if;
    Nfiltre := Tab_filtre'length;
    GR_free_point_liste(Tabfin);
    Gr_mod_points_d_arc(Gr_interm,Tab_accor(i),Tab_filtre(1..Nfiltre),Nfiltre);

    -- propagation des deplacements
    Recollage(Gr_interm,  Tab_accor(i), Parametres(7));

    --------------------------------------------------
    -- determination des points de decoupage en deuxieme passe apres l'accordeon
    -- NB: Pour le decoupage on passe la DEMI largeur de symbole
    -- NB: le multiplicateur 0.8 est pour forcer a isoler les virages dans une serie
    Pts_dec := Morpholigne.Decoupage_conflits(Tab_filtre(1..Nfiltre),
                                 Lfin/2.0*parametres(3)*0.8,Parametres(5),1.7);
    Deuxieme_passe := true;  
   
    -- decoupage en deuxieme passe
    if deuxieme_passe then
      Tab_arcs := new Liens_array_type(1..Pts_dec'length-1);
      Arc_a_decouper := Tab_accor(i); 
      for j in Pts_dec'first+1..Pts_dec'last-1 loop 	
          Gr_creer_noeud_sur_arc(Gr_interm,Arc_a_decouper,Pts_dec(j),False,
                                 0.0,0,Num,arc_debut,arc_fin);
          Tab_arcs(j-1) := Arc_debut;
          Arc_a_decouper := Arc_fin;
          Nb_noeuds := Nb_noeuds +1 ;
          Tab_noeuds(Nb_noeuds) := Num;
      end loop;
      Tab_arcs(Tab_arcs'last) := Arc_a_decouper;
      GR_free_point_liste(Pts_dec);
      GR_free_point_liste(Tab_filtre);
    end if;


    -- boucle sur les arcs decoupes en deuxieme passe
    if deuxieme_passe then
      for j in Tab_arcs'range loop
        Gr_points_d_arc(Gr_interm,Tab_arcs(j),Tabini.all,Nbini);
        -- filtrage des points doubles
        if Tabini'length = 0 then 
          Tab_filtre := null;
        else
          Tab_filtre := Filtrage(Tabini(1..Nbini));
        end if;
        Nfiltre := Tab_filtre'length;
        if Nbini /= Nfiltre then
          GR_mod_points_d_arc(Gr_interm,Tab_arcs(j),Tab_filtre(1..Nfiltre),
                              Nfiltre);
          Nbini := Nfiltre;
          Tabini(1..Nbini) := Tab_filtre.all;
        end if;
        GR_free_point_liste(Tab_filtre);
        
        -- evaluation du conflit dexieme passe
        -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
        Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3)*0.8,
                                  1.7);
  
        -- determination des arcs ou agir en deuxieme passe
        if Conf = Conflit_nul then
           -- si pas de conflit en deuxieme passe
          null;
        elsif Conf(Gauche) = 0.0 or Conf(Droite)=0.0 then
          -- si conflit d'un cote en deuxieme passe
          Nb_fmin := Nb_fmin + 1;
          Tab_fmin(Nb_fmin) := Tab_arcs(j);
        else
          -- si conflit des deux cotes, alors RIEN
          null;
        end if;
      end loop;
      GR_free_liens_array(tab_arcs);
    end if;   -- fin test sur Deuxieme_passe
  end loop;  -- fin boucle sur tab_accor

  --------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par faille max
  for i in 1..Nb_fmax loop
    Gr_points_d_arc(Gr_interm,Tab_fmax(i),Tabini.all,Nbini);    
    if Tabini'length = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabini(1..Nbini));
    end if;
    Nfiltre := Tab_filtre'length;
    if Nfiltre /= Nbini then
      Nbini := Nfiltre;
      Tabini(1..Nbini) := Tab_filtre(1..Nbini);
    end if;
    GR_free_point_liste(Tab_filtre);
 
    Caricature.Faille_max(Tabini(1..nbini),(Lfin-Lini)/2.0*parametres(3),
                                 tabfin);

    -- filtrage des points doubles
    if Tabfin'length = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabfin.all);
    end if;
    Nfiltre := Tab_filtre'length;

    GR_mod_points_d_arc(Gr_interm,Tab_fmax(i),Tab_filtre(1..Nfiltre),Nfiltre);
    GR_free_point_liste(Tabfin);

    Recollage(Gr_interm, Tab_fmax(i), Parametres(7));
    GR_free_point_liste(Tab_filtre);
  end loop;

  --------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par faille min
  for i in 1..Nb_fmin loop
      Gr_points_d_arc(Gr_interm,Tab_fmin(i),Tabini.all,Nbini);    
      if Tabini'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabini(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;
      if Nfiltre /= Nbini then
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre(1..Nbini);
      end if;
      GR_free_point_liste(Tab_filtre);
      -- squelette
      -- NB: Pour la faille min on passe la DEMI largeur de symbole
      Caricature.Decalage_squelette(Tabini(1..Nbini),
                           Lfin/2.0*parametres(6),0.0,Tabfin);
      if Tabfin = null then
         tabfin := new Point_liste_type(1..Nbini);
         tabfin.all := Tabini(1..Nbini);
      end if;

      -- filtrage des points doubles
      if Tabfin'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabfin.all);
      end if;
      Nfiltre := Tab_filtre'length;
      GR_free_point_liste(Tabfin);

      Gr_mod_points_d_arc(Gr_interm,Tab_fmin(i),Tab_filtre(1..Nfiltre),Nfiltre);

      -- recollage
      Recollage(Gr_interm,  Tab_fmin(i), Parametres(7));
      GR_free_point_liste(Tab_filtre);
  end loop;

  --------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par le lissage
  for i in 1..Nb_liss loop
      Gr_points_d_arc(Gr_interm,Tab_liss(i),Tabini.all,Nbini);    
      if Tabini'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabini(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;
      if Nfiltre /= Nbini then
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre(1..Nbini);
      end if;
      GR_free_point_liste(Tab_filtre);

      Tabfin := new point_liste_type(1..Nbini);
      if Parametres(2) /= 0.0 then
        Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,Parametres(2),Tabfin.all);
      else
        Tabfin.all := Tabini(1..Nbini);
      end if;

      -- filtrage des points doubles
      if Tabfin'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabfin(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;

      GR_free_point_liste(Tabfin);

      GR_mod_points_d_arc(Gr_interm,Tab_liss(i),Tab_filtre(1..Nfiltre),Nfiltre);
      GR_free_point_liste(Tab_filtre);
  end loop;

  GR_free_point_liste(Tabini); 

  -----------------------------------------------------
  -- TROISIEME PASSE: RECHAINAGE DES ARCS 

  for i in 1..Nb_noeuds loop
    Gr_fusion_au_noeud(Gr_interm,Tab_noeuds(i),Num);
  end loop;
  if Nb_noeuds = 0 then 
    Num:= NumArc_interm;
  end if;

  ---------------------------------------------------
  -- QUATRIEME PASSE : PETIT LISSAGE (Gauss) ET FILTRAGE (Douglas) 
  -- POUR ELIMINER LES POINTS INUTILES

  if parametres(10)/=0.0 or parametres(11)/=0.0 then    
     Grinf_interm:=Gr_infos(gr_interm);
     Tabini := new Point_liste_type(1..Grinf_interm.max_pts_arc);
     Tabfin := new Point_liste_type(1..Grinf_interm.max_pts_arc);
     gr_points_d_arc(Gr_interm,Num,Tabini.all,nbini);
     Lissage_filtrage.Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,Parametres(11),
                                        Tabfin.all);
     tabini(1..Nbini):=tabfin(1..Nbini);
     lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,Parametres(10),
                                     grinf_interm.resolution);
     GR_free_point_liste(Tabini); 
  end if;


  -- On reintroduit les nouvelles coordonnees de la ligne sur le graphe initial
  gr_mod_points_d_arc(Graphe,Arc,Tabfin(1..nfin),nfin);
  Gr_delete(Gr_interm);  

  GR_free_point_liste(Tabfin); 

  exception  -- ## GESTION DES BUGS ## 
    -- Si Bug, on reintroduit la geoemetrie initiale de la ligne
    -- et on desalloue tous les pointeurs non nuls;
    -- Si bug des maintenant alors fuyons: fin du traitement
    when others =>
        Put("Plantage sur l'arc :");
        Put(Arc);
        New_line;
        GR_free_point_liste(Pts_dec);
        GR_free_point_liste(Tabini);
        GR_free_point_liste(Tabfin);
        GR_free_point_liste(Tabfin2);
        GR_free_point_liste(Tab_filtre);
        GR_free_liens_array(Tab_arcs);

        Gr_delete(Gr_interm);  

end;


---------------------------------------------------------------------------
-- Modification de Bruneau (mars 2001) pour gerer les trous dans le symbole

---------------------------------------------
-- Specifiquement dans le cas unique des trous
-- Pas de propagation, amortissement tres raide a 0.7 

Procedure Recollage_trou(Graphe  : in out Graphe_type;
                         Num_arc : in Natural) is

Num_noeud : natural;
nb_arc_deplace : integer;
tab_arc_deplace,tab_arc_deplacant : liens_array_type(1..2*Gr_infos(graphe).nb_arcs)
                                   :=(others=>0);
Arcd : Arc_type;

begin
  nb_arc_deplace :=1;
  Tab_arc_deplace(nb_arc_deplace):=Num_arc;
  Tab_arc_deplacant(nb_arc_deplace):=Num_arc;
  Propagation.Propagation_globale(Graphe,tab_arc_deplace,tab_arc_deplacant,
          nb_arc_deplace,0.70,Propagation.Propagation_nulle);

end;

-----------------------------------
-- PROCEDURE EXPORTEE
-----------------------------------
procedure Galbage_trou (Graphe     :  in out graphe_type;
                        Legende    :  in Legende_type;
                        Rappech    :  in float;
                        Arc        :  in natural;
                       Parametres :  in Type_parametres := parametres_defaut) is


TP1,TP2 : Point_access_type;
Grinf        : Info_type;
Tab_arcs     : Liens_access_type;
Tab_filtre,
Tabini,
Tabfin,
Tabfin1,
Tabfin2,
Tabschem,
Pts_dec      : Point_access_type;
Arcini       : Arc_type;
Conf_Tab_Arcs :Access_flag;
Conf3 : Access_flag;
Conf         : Force_conflit;
Fin          : boolean := false;
test1,test2  : boolean;
Rap : float;
Tab_noeuds, 
Tab_accor,
Tab_fmin,
Tab_trou,
Tab_fmax,
Tab_liss     : Liens_array_type(1..1000);
-- NB BIDOUILLE: on suppose que 1 arc ne sera pas decoupe en plus de 1000 parties

Nb_noeuds,
Nb_accor,
Nb_fmin,
Nb_fmax,
Nfiltre,
Nbini,
NbTP1,
NbTP2, 
Nb_Trou,
Nfin, 
Nfin2,
Nb_schem,
nv,
Num,
Arc_a_decouper,
arc_debut,
arc_fin,
Nb_liss      : natural := 0;
K : integer :=0;
D1, D2 : float;
Lini, 
Lfin : float :=0.0;
Deuxieme_passe : boolean;
N4,N5,virage1,virage2 : integer :=0;

-- FL : Optimisation des traitements :
Gr_interm : graphe_type;
grinf_interm : Info_type;
Noeud_ini, Noeud_fin,Numarc_interm : integer;

-- A T T E N T I O N  : Signification du parametre(12)
-- Coefficcient correcteur lie a la symbolisation
-- Param(12) := Inverse de l'echelle de la base lisible cartographiquement /
--              Inverse de l'Echelle stockee dans le graphe
--                      
-- Exemple, BD CArto : Correction_echelle:= 50 000 / 100 000 = 0.5


begin

  -- 2eme begin pour gerer les plantages par exception

    --------------------------------------------------
    -- Lecture des infos de description de l'arc en entree
    Grinf := Gr_infos(Graphe);
    Arcini := GR_ARC(Graphe,Arc);


    -- Largeur de symbole aux echelles finales et initales (en pt PlaGe)
    if GR_LEGENDE_L(Legende,Arcini.att_graph).largeur =
       GR_LEGENDE_L(Legende,Arcini.att_graph).larg_int then
       -- cas ou il n'y a pas de bordure noir
       -- (largeur totale + seuil de lisibilite)
       Lfin := (GR_LEGENDE_L(Legende,Arcini.att_graph).largeur+0.1)
               * Rappech * float(Grinf.resolution);
       Lini := Lfin / Rappech * Parametres(12);
    else 
       -- cas ou il y a une bordure noir
       -- (largeur interne + largeur du bord noir = Int + (ext-int)/2
       Lfin :=(GR_LEGENDE_L(Legende,Arcini.att_graph).largeur / 2.0 +
               GR_LEGENDE_L(Legende,Arcini.att_graph).larg_int / 2.0 )
               * Rappech * float(Grinf.resolution); 
       Lini := Lfin / Rappech * Parametres(12);
    end if;                 

    -- liste des points de l'arc en entree
    Tabini := new Point_liste_type(1..Grinf.max_pts_arc);
    Gr_points_d_arc(Graphe,Arc,Tabini.all,Nbini);
    if Nbini < 3 then  
       GR_free_point_liste(Tabini); 
       return; 
    end if; 


    -- Il faut gerer des problemes d'optimisation de temps de calcul :
    -- 80% des arcs sont juste a lisser : traitement direct dans le graphe
    -- 20% des arcs sont a decouper : gestion dans un nouveau graphe provisoire
    Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);
    if Conf = Conflit_nul then
       -- Arc juste a lissser
       if parametres(2)/=0.0 or parametres(10)/=0.0 then    
          Tabfin := new Point_liste_type(1..Nbini);
          Lissage_filtrage.Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,
                                             Parametres(2),Tabfin.all);
          tabini(1..Nbini):=tabfin(1..Nbini);
          lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,
                                          Parametres(10),grinf.resolution);
          gr_mod_points_d_arc(Graphe,Arc,Tabfin(1..nfin),nfin);
       end if;
       GR_free_point_liste(Tabini); 
       GR_free_point_liste(Tabfin); 
       return;
    end if;


    -- Creation d'un graphe intermediaire pour optimiser les temps de calcul 
    -- dans lequel on met l'arc initial
    gr_interm:=GR_CREATE_NEW(Grinf.delta_x,grinf.delta_y,grinf.resolution,
                           grinf.echelle,300,300,10,"Gr_interm");
    Gr_creer_noeud_sur_rien(gr_interm,tabini(1),true,0.0,0,noeud_ini);
    if tabini(1)=tabini(Nbini) then
       Noeud_fin:=noeud_ini;
    else
       Gr_creer_noeud_sur_rien(gr_interm,tabini(Nbini),true,0.0,0,noeud_fin);
    end if;
    Gr_creer_arc(gr_interm,noeud_ini,noeud_fin,tabini(1..nbini),nbini,Ligne,
               Arcini.att_graph,numarc_interm);


    --------------------------------------------------
    -- decoupage de l'arc en zone homogenes de conflit

    -- determination des points de decoupage de l'arc en premiere passe
    -- NB: meme si l'exageration des caricature est superieure a 1.0, le
    -- decoupage se fait avec la largeur du symbole non exagere.
    -- NB: Pour le decoupage on passe la DEMI largeur de symbole
    Pts_dec:=Morpholigne.Decoupage_conflits(Tabini(1..Nbini),
                  Lfin/2.0*parametres(3),Parametres(1),1.7);

    -- Premier decoupage de l'arc, creation du tableau des nouveaux 
    -- numeros d'arc Tab_arcs
    N4:=Pts_dec'length;
    Tab_arcs := new Liens_array_type(1..N4-1);
    Arc_a_decouper := NumArc_interm;   
    for i in 2..N4-1 loop 	
       -- creation des sous-arcs
      Gr_creer_noeud_sur_arc(Gr_interm,Arc_a_decouper,Pts_dec(i),False,0.0,0,
                             Num, arc_debut, arc_fin);
      Tab_arcs(i-1) := Arc_debut;
      Arc_a_decouper := Arc_fin;
      Nb_noeuds := Nb_noeuds + 1;
      Tab_noeuds(Nb_noeuds) := Num;
    end loop;
    Tab_arcs(N4-1) := Arc_a_decouper;
    GR_free_point_liste(Pts_dec);  

  --------------------------------------------------
  -- Boucle sur les arcs decoupes en premiere passe
  -- Evaluation des conflits de chaque arc
  for i in Tab_arcs'range loop  
    Gr_points_d_arc(Gr_interm,Tab_arcs(i),Tabini.all,Nbini);    
    -- filtrage des points doubles
    if Nbini = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Geometrie.Filtrage(Tabini(1..Nbini));
      Nfiltre := Tab_filtre'length;
      if Nbini /= Nfiltre then
         GR_mod_points_d_arc(Gr_interm,Tab_arcs(i),Tab_filtre.all,
                            Tab_filtre'length);
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre.all;
      end if;
      GR_free_point_liste(Tab_filtre);
      -- evaluation du conflit
      -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
      Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);

      -- classement des arcs en fonction des traitements a faire
      if Conf = Conflit_nul then
         Nb_liss := Nb_liss + 1;
         Tab_liss(Nb_liss) := Tab_arcs(i);
      elsif Conf(Gauche) = 0.0 or Conf(Droite) = 0.0 then
        Nb_fmax := Nb_fmax + 1;
        Tab_fmax(Nb_fmax) := Tab_arcs(i);
      else    
        Nb_accor := Nb_accor + 1;
        Tab_accor(Nb_accor) := Tab_arcs(i);
      end if;
    end if;
  end loop; -- fin de la boucle de determination du type de conflit
  GR_free_liens_array(Tab_arcs);

  --------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par accordeon
  -- ou schematisation
  for i in 1..Nb_accor loop
    Gr_points_d_arc(Gr_interm,Tab_accor(i),Tabini.all,Nbini);    
    if tabini(1)=tabini(Nbini) then
       -- L'accordeon deplacant les 2 extremites de l'arc de facon distincte
       -- l'algorithme ne peut fonctionner pour ce type d'arc
       exit;
    end if;
    -- calcul de la nouvelle geometrie
    Tabfin2 := new Point_liste_type(1..Grinf.Max_pts_arc);
    Tabfin2(1..Nbini) := Tabini(1..Nbini);
    Nfin2 := Nbini;
    Fin := false;
    Nb_schem := 1;

    while not Fin and Nb_schem <=integer(Parametres(9))+1 loop
      if Nb_schem = integer(Parametres(9))+1 then
         Nfin := Nbini;
         Tabfin := new Point_liste_type(1..Nfin);
         Tabfin(1..Nfin) := Tabini(1..Nbini);
         Fin := true;
      else
         -- NB: Pour l'accordeon on passe la largeur TOTALE de symbole
         Caricature.Accordeon(Tabfin2(1..Nfin2),Lfin*Parametres(4), tabfin);
         Nfin:=tabfin'length;

         -- filtrage des points doubles apres accordeon
         Tab_filtre := Filtrage(Tabfin(1..Nfin));
         GR_free_point_liste(Tabfin);
         Nfin:=Tab_filtre'length;
         Tabfin:=New Point_liste_type'(Tab_filtre(1..Nfin));

         if Intersections(Tabfin(1..Nfin),Nfin) /= 0 or
            Math_int_basic.Max(Norme(Tabini(1),Tabfin(1)),
                Norme(Tabini(nbini),Tabfin(Nfin))) > Parametres(8)*Lfin then
-- Solution avec Hausdorff au lieu des extremites
--            Hausdorff(Tabini(1..Nbini), Tabfin(1..Nfin), Nbini, Nfin) >
--                                                      Parametres(8)*Lfin then

            -- NB: la largeur de symbole n'intervient pas pour la schematisation
            GR_free_point_liste(Tabfin);
            Schematisation.Schemat ( Tabfin2(1..Nfin2),Tabschem,true,10.0,true,
                                     virage1,virage2);
            GR_free_point_liste(Tabfin2);
            Nfin2:=Tabschem'length;
            Tabfin2:=New Point_liste_type'(Tabschem(1..Nfin2));
            Nb_schem := Nb_schem +1;
         else
            Fin := true;
         end if;
      end if;
    end loop;
    GR_free_point_liste(Tabfin2);

    -- filtrage des points doubles apres accordeon et schematisation
    if Nfin = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabfin(1..Nfin));
    end if;
    Nfiltre := Tab_filtre'length;
    GR_free_point_liste(Tabfin);
    Gr_mod_points_d_arc(Gr_interm,Tab_accor(i),Tab_filtre(1..Nfiltre),Nfiltre);

    -- propagation des deplacements
    Recollage(Gr_interm,  Tab_accor(i), Parametres(7));

    --------------------------------------------------
    -- determination des points de decoupage en deuxieme passe apres l'accordeon
    -- NB: Pour le decoupage on passe la DEMI largeur de symbole
    -- NB: le multiplicateur 0.8 est pour forcer a isoler les virages dans une serie
    Pts_dec := Morpholigne.Decoupage_conflits(Tab_filtre(1..Nfiltre),
                                 Lfin/2.0*parametres(3)*0.8,Parametres(5),1.7);
    Deuxieme_passe := true;  
   
    -- decoupage en deuxieme passe
    if deuxieme_passe then
      Tab_arcs := new Liens_array_type(1..Pts_dec'length-1);
      Arc_a_decouper := Tab_accor(i); 
      for j in Pts_dec'first+1..Pts_dec'last-1 loop 	
          Gr_creer_noeud_sur_arc(Gr_interm,Arc_a_decouper,Pts_dec(j),False,
                                 0.0,0,Num,arc_debut,arc_fin);
          Tab_arcs(j-1) := Arc_debut;
          Arc_a_decouper := Arc_fin;
          Nb_noeuds := Nb_noeuds +1 ;
          Tab_noeuds(Nb_noeuds) := Num;
      end loop;
      Tab_arcs(Tab_arcs'last) := Arc_a_decouper;
      GR_free_point_liste(Pts_dec);
      GR_free_point_liste(Tab_filtre);
    end if;


    -- boucle sur les arcs decoupes en deuxieme passe
    if deuxieme_passe then
      for j in Tab_arcs'range loop
        Gr_points_d_arc(Gr_interm,Tab_arcs(j),Tabini.all,Nbini);
        -- filtrage des points doubles
        if Tabini'length = 0 then 
          Tab_filtre := null;
        else
          Tab_filtre := Filtrage(Tabini(1..Nbini));
        end if;
        Nfiltre := Tab_filtre'length;
        if Nbini /= Nfiltre then
          GR_mod_points_d_arc(Gr_interm,Tab_arcs(j),Tab_filtre(1..Nfiltre),
                              Nfiltre);
          Nbini := Nfiltre;
          Tabini(1..Nbini) := Tab_filtre.all;
        end if;
        GR_free_point_liste(Tab_filtre);
        
        -- evaluation du conflit dexieme passe
        -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
        Conf:=Indicateur_conflits(Tabini(1..Nbini),Lfin/2.0*parametres(3)*0.8,
                                  1.7);
  
        -- determination des arcs ou agir en deuxieme passe
        if Conf = Conflit_nul then
           -- si pas de conflit en deuxieme passe
          null;
        elsif Conf(Gauche) = 0.0 or Conf(Droite)=0.0 then
          -- si conflit d'un cote en deuxieme passe
          Nb_fmin := Nb_fmin + 1;
          Tab_fmin(Nb_fmin) := Tab_arcs(j);
        else
          -- si conflit des deux cotes, alors RIEN
          null;
        end if;
      end loop;
      GR_free_liens_array(tab_arcs);
    end if;   -- fin test sur Deuxieme_passe
  end loop;  -- fin boucle sur tab_accor

  ----------------------------------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par faille max
------------------------------------------------------------------------------
  for i in 1..Nb_fmax loop
    Gr_points_d_arc(Gr_interm,Tab_fmax(i),Tabini.all,Nbini);    
    if Tabini'length = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabini(1..Nbini));
    end if;
    Nfiltre := Tab_filtre'length;
    if Nfiltre /= Nbini then
      Nbini := Nfiltre;
      Tabini(1..Nbini) := Tab_filtre(1..Nbini);
    end if;
    GR_free_point_liste(Tab_filtre);
 
    Caricature.Faille_max(Tabini(1..nbini),(Lfin-Lini)/2.0*parametres(3),
                                 tabfin);

    -- filtrage des points doubles
    if Tabfin'length = 0 then 
      Tab_filtre := null;
    else
      Tab_filtre := Filtrage(Tabfin.all);
    end if;
    Nfiltre := Tab_filtre'length;

    GR_mod_points_d_arc(Gr_interm,Tab_fmax(i),Tab_filtre(1..Nfiltre),Nfiltre);
    GR_free_point_liste(Tabfin);

    Recollage(Gr_interm, Tab_fmax(i), Parametres(7));
    GR_free_point_liste(Tab_filtre);
  end loop;

-----------------------------------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par faille min
----------------------------------------------------------------------------- 
 for i in 1..Nb_fmin loop
      Gr_points_d_arc(Gr_interm,Tab_fmin(i),Tabini.all,Nbini);    
      if Tabini'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabini(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;
      if Nfiltre /= Nbini then
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre(1..Nbini);
      end if;
      GR_free_point_liste(Tab_filtre);
      -- squelette
      -- NB: Pour la faille min on passe la DEMI largeur de symbole
      Caricature.Decalage_squelette(Tabini(1..Nbini),
                           Lfin/2.0*parametres(6),0.0,Tabfin);
      if Tabfin = null then
         tabfin := new Point_liste_type(1..Nbini);
         tabfin.all := Tabini(1..Nbini);
      end if;

      -- filtrage des points doubles
      if Tabfin'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabfin.all);
      end if;
      Nfiltre := Tab_filtre'length;
      GR_free_point_liste(Tabfin);

      Gr_mod_points_d_arc(Gr_interm,Tab_fmin(i),Tab_filtre(1..Nfiltre),Nfiltre);

      -- recollage
      Recollage(Gr_interm,  Tab_fmin(i), Parametres(7));
      GR_free_point_liste(Tab_filtre);
  end loop;


  -------------------------------------------------------------------
  -- Boucle sur les arcs qui doivent etre traites par le lissage
  -------------------------------------------------------------------
  for i in 1..Nb_liss loop
      Gr_points_d_arc(Gr_interm,Tab_liss(i),Tabini.all,Nbini);    
      if Tabini'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabini(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;
      if Nfiltre /= Nbini then
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre(1..Nbini);
      end if;
      GR_free_point_liste(Tab_filtre);

      Tabfin := new point_liste_type(1..Nbini);
      if Parametres(2) /= 0.0 then
        Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,Parametres(2),Tabfin.all);
      else
        Tabfin.all := Tabini(1..Nbini);
      end if;

      -- filtrage des points doubles
      if Tabfin'length = 0 then 
         Tab_filtre := null;
      else
         Tab_filtre := Filtrage(Tabfin(1..Nbini));
      end if;
      Nfiltre := Tab_filtre'length;

      GR_free_point_liste(Tabfin);

      GR_mod_points_d_arc(Gr_interm,Tab_liss(i),Tab_filtre(1..Nfiltre),Nfiltre);
      GR_free_point_liste(Tab_filtre);
  end loop;

  GR_free_point_liste(Tabini); 

  -----------------------------------------------------
  -- TROISIEME PASSE: RECHAINAGE DES ARCS 

  for i in 1..Nb_noeuds loop
    Gr_fusion_au_noeud(Gr_interm,Tab_noeuds(i),Num);
  end loop;
  if Nb_noeuds=0 then
     Num:=NumArc_interm;
  end if;

  ---------------------------------------------------
  -- QUATRIEME PASSE : PETIT LISSAGE FILTRAGE (Douglas) 
  -- POUR ELIMINER LES POINTS INUTILES
  Grinf_interm:=Gr_infos(gr_interm);
  Tabini := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  Tabfin := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  gr_points_d_arc(Gr_interm,Num,Tabini.all,nbini);
  if parametres(10)/=0.0 then    
     lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,Parametres(10),
                                     grinf_interm.resolution);
     GR_MOD_POINTS_D_ARC(Gr_interm,Num,Tabfin(1..nfin),nfin);
  else
     Nfin:=Nbini;
     tabfin(1..Nfin):=tabini(1..Nbini);
  end if;
  Nb_noeuds:=0;

--****************************************************************************
--on traite maintenant uniquement les pb de trous_dans_le_symbole (cote gauche)
--****************************************************************************
--nouveau decoupage de l'arc en zones homogenes de conflit
  MORPHOLIGNE.DECOUPAGE_CONFLITS_TROU_GD(Gauche,Tabfin(1..nfin),
                      Lfin/2.0*parametres(3),parametres(1),1.7,Pts_dec,Conf3);
  GR_free_point_liste(Tabfin);
  N5:=Pts_dec'length;
  Tab_arcs:=new Liens_array_type(1..N5-1);
  Conf_tab_arcs:=new Liste_flag(1..N5-1);
  Arc_a_decouper:=Num;
  for i in 2..N5-1 loop
      --creation des sous-arcs
      GR_CREER_NOEUD_SUR_ARC(Gr_interm,Arc_a_decouper,Pts_dec(i),False,0.0,0,
                         Num,arc_debut,arc_fin);
      Tab_arcs(i-1):=Arc_debut;
      Conf_tab_arcs(i-1):=Conf3(i-1);
      Arc_a_decouper:=Arc_fin;
      Nb_noeuds := Nb_noeuds + 1;
      Tab_noeuds(Nb_noeuds) := Num;
  end loop;
  Tab_arcs(N5-1) := Arc_a_decouper;
  Conf_tab_arcs(N5-1):=Conf3(N5-1);
  GR_free_point_liste(Pts_dec);
  
  -- Boucle sur les arcs decoupes en premiere passe
  -- Evaluation des conflits de chaque arc
  for i in Tab_arcs'range loop
    GR_POINTS_D_ARC(Gr_interm,Tab_arcs(i),Tabini.all,Nbini);
    -- filtrage des points doubles
   if Nbini = 0 then
      Tab_filtre := null;
    else
      Tab_filtre := Geometrie.Filtrage(Tabini(1..Nbini));
      Nfiltre := Tab_filtre'length;
      if Nbini /= Nfiltre then
         GR_MOD_POINTS_D_ARC(Gr_interm,Tab_arcs(i),Tab_filtre.all,
                            Tab_filtre'length);
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre.all;
      end if;
      GR_FREE_POINT_LISTE(Tab_filtre);
      -- evaluation du conflit
      -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
      Conf:=INDICATEUR_CONFLITS(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);
                               

 --ne sont plus traites que les pb de trous dans le symbole -(gauche)
      if Conf = Conflit_nul then
         if Conf_Tab_arcs(i)=true then
           Nb_Trou:=Nb_Trou+1;
           Tab_Trou(Nb_Trou):=Tab_Arcs(i);
         end if;
      end if;
    end if;
  end loop; 
  GR_free_liens_array(Tab_arcs);
  Morpholigne.Free_flag(Conf_tab_arcs);

 ---------------------------------------------------------------------------
 --boucle sur les arcs qui doivent etre traites par trou_dans_le_symbole
 --------------------------------------------------------------------------   
  for i in 1..Nb_trou-1 loop
      GR_POINTS_D_ARC(Gr_Interm,Tab_Trou(i),Tabini.all,Nbini);
      if Tabini'length = 0 then
         TP1:=null;
         NbTP1:=0;
      else
         TP1:=new Point_liste_type(1..Nbini);
         TP1:=Filtrage(Tabini(1..Nbini));
         NbTP1:=TP1'length;
      end if;
      for j in i+1..Nb_trou loop
--          if i=K then
--             GR_FREE_POINT_LISTE(TP1);
--             exit;
--          end if;
          GR_POINTS_D_ARC(Gr_Interm,Tab_Trou(j),Tabini.all,Nbini);
          if Tabini'length = 0 then
             TP2:=null;
             NbTP2:=0;
          else
             TP2:=new Point_liste_type(1..Nbini);
             TP2:=Filtrage(Tabini(1..Nbini));
             NbTP2:=TP2'length;
          end if;
          -- test sur les deux listes de points TP1 et TP2 pour trouver 
          -- les veritables homolgues
  -- forment elles deux arcs dont les extremites sont inferieures a la largeur
  -- du signe
          if nbtp2/=0 and nbtp1/=0 then
             D1:=Distance_a_polylignes(TP1((1+NbTP1)/2),TP2.all,NbTP2,C_Segment_type);
             D2:=Distance_a_polylignes(TP2((1+nbTP2)/2),TP1.all,NbTP1,C_Segment_type);
             if D1< float(lfin)*1.2 and D2< float(Lfin)*1.2 then
                test2:=true;
             else
                test2:=false;
             end if;
          else
             test2:=false;
          end if;
  --forment elles deux arcs de longueur equivalente a 5% pres ?
          test1:= MORPHOLIGNE.ARC_PROCHE_TROU(TP1(1..NbTP1),TP2(1..NbTP2),0.05);
          if test1 = true and test2=true then
--             K:=j;
             CARICATURE.DECALAGE_SQUELETTE_TROU(TP1(1..NbTP1),TP2(1..NbTP2),
                                  Lfin/2.0*parametres(6),0.0,Tabfin1,Tabfin2);
  --filtrage des points doubles pour Tabfin1
             if Tabfin1'length=0 then
                Tab_filtre:=null;
             else
                Tab_filtre:=Filtrage(Tabfin1.all);
             end if;
             Nfiltre:=Tab_filtre'length;
             GR_FREE_POINT_LISTE(Tabfin1);
             GR_MOD_POINTS_D_ARC(Gr_interm,Tab_trou(i),Tab_filtre(1..Nfiltre),
                                                                  Nfiltre);
--recollage pour l'arc Tab_trou(i)
             RECOLLAGE_trou(Gr_interm,Tab_trou(i));
             GR_FREE_POINT_LISTE(Tab_filtre);
          
 --filtrage des points doubles pour Tabfin2
             if Tabfin2'length=0 then
                Tab_filtre:=null;
             else
                Tab_filtre:=Filtrage(Tabfin2.all);
             end if;
             Nfiltre:=Tab_filtre'length;
             GR_FREE_POINT_LISTE(TP2);
             GR_FREE_POINT_LISTE(Tabfin2);
             GR_MOD_POINTS_D_ARC(Gr_interm,Tab_trou(j),Tab_filtre(1..Nfiltre),
                                                                  Nfiltre);
             GR_FREE_POINT_LISTE(Tab_filtre);
             --recollage pour l'arc Tab_trou(j)
             RECOLLAGE_trou(Gr_interm,Tab_trou(j));
             exit;
         end if;
         GR_FREE_POINT_LISTE(TP2); 
      end loop;
      GR_FREE_POINT_LISTE(TP1); 
   end loop;
--fin du traitement pour les trous dans le symbole gauche---------------

--------------------------------------------------------------------------
--RECHAINAGE DES ARCS
   
  for i in 1..Nb_noeuds loop
    Gr_fusion_au_noeud(Gr_interm,Tab_noeuds(i),Num);
  end loop;
  GR_FREE_POINT_LISTE(Tabini);
   ---------------------------------------------------
  -- QUATRIEME PASSE : PETIT  FILTRAGE (Douglas)
  -- POUR ELIMINER LES POINTS INUTILES

  Grinf_interm:=Gr_infos(gr_interm);
  Tabini := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  Tabfin := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  GR_POINTS_D_ARC(Gr_interm,Num,Tabini.all,nbini);
  if parametres(10)/=0.0 then    
     lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,Parametres(10),
                                     grinf_interm.resolution);
     GR_MOD_POINTS_D_ARC(Gr_interm,Num,Tabfin(1..nfin),nfin);
  else
     Nfin:=Nbini;
     tabfin(1..Nfin):=tabini(1..Nbini);
  end if;
  Nb_Noeuds:=0;
  Nb_trou:=0;
  Nb_fmin:=0;


--****************************************************************************
--on traite maintenant uniquement les pb de trous_dans_le_symbole (cote droit)
--****************************************************************************
--nouveau decoupage de l'arc en zones homogenes de conflit
  MORPHOLIGNE.DECOUPAGE_CONFLITS_TROU_GD(Droite,Tabfin(1..nfin),
                      Lfin/2.0*parametres(3),parametres(1),1.7,Pts_dec,Conf3);
  GR_free_point_liste(Tabfin);
  N5:=Pts_dec'length;
  Tab_arcs:=new Liens_array_type(1..N5-1);
  Conf_tab_arcs:=new Liste_flag(1..N5-1);
  Arc_a_decouper:=Num;
  for i in 2..N5-1 loop
      --creation des sous-arcs
      GR_CREER_NOEUD_SUR_ARC(Gr_interm,Arc_a_decouper,Pts_dec(i),False,0.0,0,
                         Num,arc_debut,arc_fin);
      Tab_arcs(i-1):=Arc_debut;
      Conf_tab_arcs(i-1):=Conf3(i-1);
      Arc_a_decouper:=Arc_fin;
      Nb_noeuds := Nb_noeuds + 1;
      Tab_noeuds(Nb_noeuds) := Num;
  end loop;
  Tab_arcs(N5-1) := Arc_a_decouper;
  Conf_tab_arcs(N5-1):=Conf3(N5-1);
  GR_free_point_liste(Pts_dec);

  -- Boucle sur les arcs decoupes en premiere passe
  -- Evaluation des conflits de chaque arc
  for i in Tab_arcs'range loop
    GR_POINTS_D_ARC(Gr_interm,Tab_arcs(i),Tabini.all,Nbini);
    -- filtrage des points doubles
   if Nbini = 0 then
      Tab_filtre := null;
    else
      Tab_filtre := Geometrie.Filtrage(Tabini(1..Nbini));
      Nfiltre := Tab_filtre'length;
      if Nbini /= Nfiltre then
         GR_MOD_POINTS_D_ARC(Gr_interm,Tab_arcs(i),Tab_filtre.all,
                            Tab_filtre'length);
         Nbini := Nfiltre;
         Tabini(1..Nbini) := Tab_filtre.all;
      end if;
      GR_FREE_POINT_LISTE(Tab_filtre);
      -- evaluation du conflit
      -- NB: Pour l'evaluation du conflit on passe la DEMI largeur de symbole
      Conf:=INDICATEUR_CONFLITS(Tabini(1..Nbini),Lfin/2.0*parametres(3),1.7);

 --ne sont plus traites que les pb de trous dans le symbole -(cote droit)
      if Conf = Conflit_nul then
         if Conf_Tab_arcs(i)=true then
            Nb_Trou:=Nb_Trou+1;
            Tab_Trou(Nb_Trou):=Tab_Arcs(i);
         end if;
      else
         if Conf(Gauche)/=0.0 and Conf(Droite)/=0.0 then
          Nb_fmin:=Nb_fmin+1;
          Tab_fmin(Nb_fmin):=Tab_arcs(i);
         end if;
      end if;
    end if;
  end loop;
  GR_free_liens_array(Tab_arcs);
  Morpholigne.Free_flag(Conf_tab_arcs);
 ---------------------------------------------------------------------------
 --boucle sur les arcs qui doivent etre traites par trou_dans_le_symbole
 --------------------------------------------------------------------------
  for i in 1..Nb_trou-1 loop
      GR_POINTS_D_ARC(Gr_Interm,Tab_Trou(i),Tabini.all,Nbini);
      if Tabini'length = 0 then
         TP1:=null;
         NbTP1:=0;
      else
         TP1:=new Point_liste_type(1..Nbini);
         TP1:=Filtrage(Tabini(1..Nbini));
         NbTP1:=TP1'length;
      end if;
      for j in i+1..Nb_trou loop
          GR_POINTS_D_ARC(Gr_Interm,Tab_Trou(j),Tabini.all,Nbini);
          if Tabini'length = 0 then
             TP2:=null;
             NbTP2:=0;
          else
             TP2:=new Point_liste_type(1..Nbini);
             TP2:=Filtrage(Tabini(1..Nbini));
             NbTP2:=TP2'length;
          end if;
          -- test sur les deux listes de points TP1 et TP2 pour trouver 
          -- les veritables homolgues
  -- forment elles deux arcs dont les extremites sont inferieures a la largeur
  -- du signe
          if nbtp2/=0 and nbtp1/=0 then
             D1:=Distance_a_polylignes(TP1((1+NbTP1)/2),TP2.all,NbTP2,C_Segment_type);
             D2:=Distance_a_polylignes(TP2((1+nbTP2)/2),TP1.all,NbTP1,C_Segment_type);
             if D1< float(lfin)*1.2 and D2< float(Lfin)*1.2 then
                test2:=true;
             else
                test2:=false;
             end if;
          else
             test2:=false;
          end if;

  --forment elles deux arcs de longueur equivalente a 5% pres ?
          test1:= MORPHOLIGNE.ARC_PROCHE_TROU(TP1(1..NbTP1),TP2(1..NbTP2),0.05);
          if test1 = true and test2=true then
             CARICATURE.DECALAGE_SQUELETTE_TROU(TP1(1..NbTP1),TP2(1..NbTP2),
                                  Lfin/2.0*parametres(6),0.0,Tabfin1,Tabfin2);
  --filtrage des points doubles pour Tabfin1
             if Tabfin1'length=0 then
                Tab_filtre:=null;
             else
                Tab_filtre:=Filtrage(Tabfin1.all);
             end if;
             Nfiltre:=Tab_filtre'length;
             GR_FREE_POINT_LISTE(Tabfin1);
             GR_MOD_POINTS_D_ARC(Gr_interm,Tab_trou(i),Tab_filtre(1..Nfiltre),
                                                                  Nfiltre);
             GR_FREE_POINT_LISTE(Tab_filtre);
--recollage pour l'arc Tab_trou(i)
             RECOLLAGE_TROU(Gr_interm,Tab_trou(i));

 --filtrage des points doubles pour Tabfin2
             if Tabfin2'length=0 then
                Tab_filtre:=null;
             else
                Tab_filtre:=Filtrage(Tabfin2.all);
             end if;
             Nfiltre:=Tab_filtre'length;
             GR_FREE_POINT_LISTE(TP2);
             GR_FREE_POINT_LISTE(Tabfin2);
             GR_MOD_POINTS_D_ARC(Gr_interm,Tab_trou(j),Tab_filtre(1..Nfiltre),
                                                                  Nfiltre);
             GR_FREE_POINT_LISTE(Tab_filtre);

--recollage pour l'arc Tab_trou(j)
             RECOLLAGE_TROU(Gr_interm,Tab_trou(j));
             exit;
         end if;
         GR_FREE_POINT_LISTE(TP2);
      end loop;
      GR_FREE_POINT_LISTE(TP1);
   end loop;
--fin du traitement pour les trous dans le symbole cote droit---------------


---------------------------------------------------------------------------
--RECHAINAGE DES ARCS

  for i in 1..Nb_noeuds loop
    Gr_fusion_au_noeud(Gr_interm,Tab_noeuds(i),Num);
  end loop;
  GR_FREE_POINT_LISTE(Tabini);

   ---------------------------------------------------
  -- QUATRIEME PASSE : PETIT LISSAGE (Gauss) ET FILTRAGE (Douglas)
  -- POUR ELIMINER LES POINTS INUTILES

  Grinf_interm:=Gr_infos(gr_interm);
  Tabini := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  Tabfin := new Point_liste_type(1..Grinf_interm.max_pts_arc);
  gr_points_d_arc(Gr_interm,Num,Tabini.all,nbini);
  if parametres(10)/=0.0 or parametres(11)/=0.0 then
     Lissage_filtrage.Filtre_Gaussien_N(Tabini(1..Nbini),Nbini,Parametres(11),
                                        Tabfin.all);
     tabini(1..Nbini):=tabfin(1..Nbini);
     lissage_filtrage.Douglas_Peucker(Tabini,nbini,Tabfin,nfin,Parametres(10),
                                     grinf_interm.resolution);
     GR_MOD_POINTS_D_ARC(Gr_interm,Num,Tabfin(1..nfin),nfin);
  else
     Nfin:=Nbini;
     tabfin(1..Nfin):=tabini(1..Nbini);
  end if;
  
                                                                  
--***************************************************************************
--reintroduction des nouvelles coordonnees de la ligne sur le graphe initial
--***************************************************************************
  gr_mod_points_d_arc(Graphe,Arc,Tabfin(1..nfin),nfin);
  Gr_delete(Gr_interm);  

  GR_free_point_liste(Tabfin); 

  exception  -- ## GESTION DES BUGS ## 
    -- Si Bug, on reintroduit la geoemetrie initiale de la ligne
    -- et on desalloue tous les pointeurs non nuls;
    -- Si bug des maintenant alors fuyons: fin du traitement
    when others =>
        Put("Plantage sur l'arc :");
        Put(Arc);
        New_line;
        GR_free_point_liste(Pts_dec);
        GR_free_point_liste(Tabini);
        GR_free_point_liste(Tabfin);
        GR_free_point_liste(Tabfin2);
        GR_free_point_liste(Tab_filtre);
        GR_free_liens_array(Tab_arcs);

        Gr_delete(Gr_interm);  

  end;




end GALBE;
