with Gen_io; 		use Gen_io;
-- ****************************************************************
-- ** Package de detection des points d'inflexion et des sommets **
-- ****************************************************************
Package pts_caracteristique is Type liens_neg_type is array(integer range <>) of integer;
 Type reel_neg_type is array(integer range <>) of float;
 Type reel_access_neg_type is access reel_neg_type;
 type SOMMET_TYPE is record
	  NS 	    : integer:= 1 ;
      TS 	    : point_liste_type(1..30);
      indice_sommet : liens_array_type(1..30);
 end record;
 type  tsommet_type is array(integer range <>) of sommet_type;
 Seuil_micro_inflex : integer := 2;
 -- 2 PV de meme signe au moins ! 
 -- ===========================================================================
 -- Detection des changements de courbure  
 --	et des pts pred les pts d'inflexion Tpoints_pred_pi : 
 -- =========================================================================== 
 Procedure DET_POSITION_PI ( Ligne_lisse    : IN     point_liste_reel;
			     Np		    : IN     natural;
	   			     Tprove   	    : IN     reel_tableau_type;
 			     Nb_pts_inflex  : IN OUT natural;
			     Tpoints_pred_pi: IN OUT liens_access_type);
 Procedure DET_POSITION_PI ( Ligne_lisse    : IN     point_liste_reel;
			     Np		    : IN     natural;
				     Nb_pts_inflex  : IN OUT natural;
			     Tpoints_pred_pi: IN OUT liens_access_type);
 -- ==========================================================================
 -- Determination des coordonnees des PI : 
 -- ========================================================================== 
 Procedure DET_COORD_PI  ( ligne                   : in     point_liste_type;
			   ligne_lisse 		   : in     point_liste_reel;
			   n_points		   : in     integer;
			   nb_pts_inflex 	   : in     natural;
 			   Tpoints_pred_pi	   : in     liens_access_type;
			   tpinflex, tpinflex_lisse: in out point_liste_type);
 -- ==========================================================================
 -- Appels des fonctions DET_POSITIONS_PI et DET_COORD_PI :
 -- ========================================================================= 
 Procedure INFLEXIONS(  ligne 		: in point_liste_type;
		        npts		: in natural;
			sigma 		: in float;
			Ligne_lisse 	: in out point_liste_reel;
  			nb_pts_inflex	: in out natural;
  			Tpoints_pred_pi	: in out liens_access_type;
  			Tpinflex,   			Tpinflex_lisse  : in out point_liste_type);
-- ================================================================
-- Procedure de dettermination de 2 niveaux de points d'inflexion :
-- 	les inflexions de niveau 1 delimitant un virage
-- 	les inflexions de niveau 2 dans les virages-- !!! ATTENTION au pbm du choix des 2 SIGMA 
-- ================================================================
Procedure HIERARCHIE_DES_PI (  	ligne 		: in point_liste_type;
		        	npts		: in natural;
				sigma_faible	: in Out float;
				sigma_fort	: in Out float;
				nb_pts_inflex	: IN Out natural;
 				Tpoints_pred_pi	: In out liens_access_type;
 				Tpinflex	: In Out point_liste_type;
				Tniveau_pi	: In Out liens_array_type ) ;
-- ============================================================
-- Procedure de detection des maximas de courbure, i.e. Sommets
-- Methode de Mathieu pour le calcul de courbure
-- Dans cette version, il y a un seul sommet par virage
-- ============================================================
procedure SOMMETS (  	tabIni 		: IN Point_liste_type;
  			nb_points 	: IN natural ;
			SIGMA		: IN float;
  			nb_pts_inflex   : IN natural ;
  			Tpoints_pred_pi : IN liens_access_type;
  			Tab_sommets	: OUT point_liste_type;
  			Nb_sommets 	: OUT integer );
-- ==================================================================
-- Procedure de detection des maximas de courbure, i.e. Sommets
-- Approximation au point le plus eloigne (en projection orthogonale)
-- de la droite joignant les points d'inflexion
-- 
procedure SOMMETS ( 	ligne		: IN point_liste_type;
				ligne_lisse	: IN point_liste_type;
	  			nb_points 	: IN natural ;
			SIGMA		: IN float;
			Tpinflex	: IN point_liste_type;
  			nb_pts_inflex   : IN natural ;
  			Tpoints_pred_pi : IN liens_access_type;
  			Tsommet		: OUT tsommet_type );
end;
