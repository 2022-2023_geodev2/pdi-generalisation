with text_io;use text_io;
with pts_caracteristique;
with pi_et_sommet; 
With Geometrie; use Geometrie;
with math_cogit; use math_cogit;
with math_int_basic; use math_int_basic;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

with caricature;use caricature;



package body Schematisation is

  package int_io is new text_io.integer_io(integer);    use int_io;
  package flo_io is new text_io.float_io(float);        use flo_io;



---------------------------------------------------------------------------
-- Procedure developpee par Serge Skarbinck pour eliminer 2  virages
-- soit designes interactivement, soit evalues apres des mesures sur 
-- l'ensemble des virages de l'arc designe par l'utilisateur (avril 1999).
----------------------------------------------------------------------------

procedure SCHEMAT (  ligne	       : IN point_liste_type;
                     ligne_schematise  : OUT point_access_type;
                     Auto_sigma        : IN boolean :=true;
		     Sigma	       : IN float :=20.0;
                     Auto_choix        : IN boolean :=true;
                     virag1            : IN OUT integer;
                     virag2            : IN OUT integer;
                     Det_pi            : IN pi_et_sommet.Type_detection_PI
					:= Milieu_des_sommets;
                     Det_som           : IN pi_et_sommet.Type_detection_sommets
					:= Max_courbure_MB
			) is

n_points : integer := ligne'length;
Ligne_schematise2 :Point_access_type;
flag_auto : boolean:=false;
coord_pi,coord_sommets : point_access_type;
points_pred_pi,points_pred_sommets : liens_access_type;
sigma_calcule : float;
n1, n2, N : integer:=0;             
lgr, lgr1, lgr2, S : float;
deplacement1, deplacement2 : point_type_reel;
pt_raccord : point_type;
ligne1,ligne2,ligne_interm,ligne_schem_prov : point_liste_type(1..n_points);
virage1,virage2,vir1,vir2 : integer;

u1,u2,v1,v2,deplacement1u,deplacement1v,deplacement2u,deplacement2v:point_type_reel;
theta1,theta2,confiance1,confiance2 :float;
s1,lg1,s2,lg2 :float;
virage_pred_1,virage_suiv_2 : point_liste_type(1..n_points);
nv1,nv2,indice_debut,indice_fin, npts_v1,npts_v2 :integer;
nbpts_virage_prec_1, nbpts_virage_suiv_2 :integer;

begin              

  lgr:=taille_ligne(ligne(1..n_points),n_points);

  Flag_auto:=Auto_sigma;
  Pi_et_sommet.inflexions_sommets(ligne(1..n_points),flag_auto,sigma,points_pred_pi,coord_pi,
				points_pred_sommets, coord_sommets,
				sigma_calcule,det_pi,det_som);
  if auto_choix=true then
      SCHEMAT_SELECT(ligne, coord_pi, points_pred_pi, coord_sommets,
                 points_pred_sommets, virage1,virage2);
      virag1:=virage1;
      virag2:=virage2;
  end if; 


  -- Controle de coherence et sortie sans rien faire
  if virag1 > coord_sommets'last or virag1 < 1 or
     virag2 > coord_sommets'last or virag2 < 1 or
     virag1=virag2 or coord_sommets'last<=2 then
     ligne_schematise:= New Point_liste_type'(ligne);
     Gr_free_point_liste(coord_pi);
     Gr_free_point_liste(coord_sommets);
     Gr_free_liens_array(points_pred_pi);
     Gr_free_liens_array(points_pred_sommets);
     return;
  end if;

  if virag1 > virag2 then
     vir1:=virag2;
     vir2:=virag1;
  else
     vir1:=virag1;
     vir2:=virag2;
  end if;

  --determination des deux lignes resultant de la double section
  if vir1/=1 then
     n1 := points_pred_pi(vir1)+1;
  else
     n1:=1;
  end if;
  ligne1(1..n1-1) := ligne(1..n1-1);
  ligne1(n1) := coord_pi(vir1);


  if vir2/=points_pred_sommets'length then
     n2 := n_points - points_pred_pi(vir2 + 1) +1;
  else
     n2:= 1;
  end if;
  ligne2(1) := coord_pi(vir2 + 1);
  ligne2(2..n2) := ligne((points_pred_pi(vir2 + 1))+1..n_points);

  --determination du point de raccordement
  lgr1:=taille_ligne(ligne1(1..n1),n1);
  lgr2:=taille_ligne(ligne2(1..n2),n2);

  pt_raccord:= Entier( (Reel(ligne1(n1))*lgr2 + 
                        Reel(ligne2(1))*lgr1    ) / (lgr1 + lgr2) );

  --determination des vecteurs maximums de deplacement de chaque portion
  deplacement1 := reel(Pt_raccord - ligne1(n1));
  deplacement2 := reel(Pt_raccord - ligne2(1));

  --Definition du repere local de chacun des deux virages
  if vir1=1 then
     nbpts_virage_prec_1:=0;
     theta1:=0.0;
  else
     nbpts_virage_prec_1 := points_pred_pi(vir1)-points_pred_pi(vir1-1)+1;
     virage_pred_1(1..nbpts_virage_prec_1) := ligne1(points_pred_pi(vir1 -1)..
                                                   points_pred_pi(vir1));
     caricature.direction_principale(virage_pred_1(1..nbpts_virage_prec_1),
                                   theta1,confiance1);
  end if;
  if vir2 = coord_sommets'length then
     nbpts_virage_suiv_2 :=0;
     theta2:=0.0;
  else
     nbpts_virage_suiv_2 :=
     points_pred_pi(vir2+2) - points_pred_pi(vir2+1) +1;
     virage_suiv_2(1..nbpts_virage_suiv_2) := 
            ligne(points_pred_pi(vir2+1).. points_pred_pi(vir2+2));
     caricature.direction_principale(virage_suiv_2(1..nbpts_virage_suiv_2),
            theta2,confiance2);
  end if;


u1 := (cos(theta1), sin(theta1));
v1 := (-sin(theta1), cos(theta1));
deplacement1u := scalaire(deplacement1,u1)*u1;
deplacement1v := scalaire(deplacement1,v1)*v1;

u2 := (cos(theta2), sin(theta2));
v2 := (-sin(theta2), cos(theta2));
deplacement2u := scalaire(deplacement2,u2)*u2;
deplacement2v := scalaire(deplacement2,v2)*v2;

--REPARTITION DU DEPLACEMENT SUR TOUTE LA LIGNE 1 POUR LA DIRECTION
--PERPENDICULAIRE A LA DIRECTION PRINCIPALE

ligne_schem_prov(1) := ligne(1);
N := 1;
S := 0.0;
s1:= 0.0;
nv1 := 0;  
if vir1/=1 then
   indice_debut := points_pred_sommets(vir1-1);
   indice_fin   := n1;
   npts_v1 := indice_fin - indice_debut + 1;
   ligne_interm(1..npts_v1):=ligne1(indice_debut..indice_fin);
   lg1 := taille_ligne(ligne_interm(1..npts_v1),npts_v1);
end if;
for i in 1..n1-1 loop
       N := N + 1;
       S := S + norme(ligne1(i), ligne1(i+1));
       if i >= indice_debut and i <= indice_fin then
       		nv1 := nv1 + 1;
       		s1 := s1 + norme(ligne1(i),ligne1(i+1));
       		ligne_schem_prov(N):= ligne1(i+1) +Entier(deplacement1v*S/lgr1 
                                       + deplacement1u*s1/lg1);
	else 
        	ligne_schem_prov(N) := ligne1(i+1) + Entier(deplacement1v*S/lgr1);
	end if;       
        if Ligne_schem_prov(N)=ligne_schem_prov(N-1) then
            --Meme coordonnee, on n'ajoute pas le point
           N:=N-1;
        end if;
end loop;
--AVEC UNE REPARTITION DU DEPLACEMENT SUR SEULEMENT LE  DEMI-VIRAGE PRECEDANT 
--LA DOUBLE SECTION, EN CE QUI CONCERNE LA DIRECTION PRINCIPALE

--  Ajout du point de raccordement
  N:=N+1;
  Ligne_schem_prov(N):=Pt_raccord;
  if Ligne_schem_prov(N)=ligne_schem_prov(N-1) then
     --Meme coordonnee, on n'ajoute pas le point
     N:=N-1;
  end if;
  


  S:=0.0;  
  s2:=0.0;
  nv2:=0;
  if vir2 /= coord_sommets'length then
     indice_debut:= 1; 
     indice_fin := points_pred_sommets(vir2+1)-(points_pred_pi(vir2+1)+1)+2;
     npts_v2 := indice_fin - indice_debut + 1;
     ligne_interm(1..npts_v2):=ligne2(indice_debut..indice_fin);
     lg2 := taille_ligne(ligne_interm(1..npts_v2),npts_v2);
  end if;

--REPARTITION DU DEPLACEMENT SUR TOUTE LA LIGNE 2 POUR LA DIRECTION
--PERPENDICULAIRE A LA DIRECTION PRINCIPALE, ET SUR SEULEMENT LE DEMI
--VIRAGE SUIVANT LA DOUBLE SECTION EN CE QUI CONCERNE LA DIRECTION PRINCIPALE

  for i in 1..n2-1 loop
      N:=N+1;
      S := S + norme(ligne2(i), ligne2(i+1));
      if i >= indice_debut and i <= indice_fin then
          nv2:=nv2 +1;
          s2:= s2 + norme(ligne2(i), ligne2(i+1));
	  Ligne_schem_prov(N):= ligne2(i+1) +
                     Entier(deplacement2v*(lgr2-S)/lgr2 
                            + deplacement2u*(lg2-s2)/lg2);
      else
	  ligne_schem_prov(N):= ligne2(i+1) + Entier(deplacement2v*(lgr2-S)/lgr2);
      end if;  
     
      if Ligne_schem_prov(N)=ligne_schem_prov(N-1) then
           --Meme coordonnee, on n'ajoute pas le point
           N:=N-1;
      end if;
  end loop;


  if Ligne_schem_prov(N)/=ligne(n_points) then
     --Coordonnee different, on ajoute le dernier point
     N:=N+1;
     ligne_schem_prov(N) := ligne(n_points);
  end if;


  Ligne_schematise2:=new Point_liste_type(1..N);
  Ligne_schematise2(1..N):=Ligne_schem_prov(1..N);
  Ligne_schematise:=Ligne_schematise2;


  Gr_free_point_liste(coord_pi);
  Gr_free_point_liste(coord_sommets);
  Gr_free_liens_array(points_pred_pi);
  Gr_free_liens_array(points_pred_sommets);

end SCHEMAT;


         
----------------------------------------------------------------------
-- Procedure developpee pour selectionner 2 virages
-- a eliminer dans une liste ordonnee sous la contrainte d'une liste de 
-- virages a conserver
----------------------------------------------------------------------

procedure SCHEMAT_SELECT       ( ligne  	     : IN point_liste_type;
	                         coord_pi 	     : IN point_access_type;
				 points_pred_pi      : IN liens_access_type;
				 coord_sommets       : IN point_access_type;
				 points_pred_sommets : IN liens_access_type;
				 vi1                 : OUT integer;
                                 vi2                 : OUT integer
				)is

card1, card2, nbv, v1  : integer:=0;
L,L_int,L_aut : liens_array_type(1..1000);             
rejet, consec : boolean;


begin              

                                    
SCHEMAT_MESURES(ligne,coord_pi.all, points_pred_pi.all, coord_sommets.all,
 points_pred_sommets.all,L_int,card1,L_aut,card2);
nbv := 0;
consec := false;
if card2 = 0 or card2 = 1 or card2 = 2 then 
   new_line;
   Put_line("La schematisation n'est pas recommandee pour cet arc");
   vi1:=0;
   vi2:=0;
   return;
end if;
for i in 1..card2 loop
	rejet := false;
	for j in 1..card1 loop
		if L_aut(i) = L_int(j) then
		    rejet := true;
		end if;
        end loop;
	if rejet = false then
       		nbv := nbv + 1;
       		L(nbv) := L_aut(i);
	end if;
end loop;	
if nbv = 0 or nbv = 1 then 
        new_line;
	put_line("AUCUN COUPLE DE VIRAGES TROUVE SUR CET ARC... ");
        vi1:=0;
	vi2:=0;
	return;
end if;
if nbv = 2 then
       		vi1 := L(1)  ;
		vi2 := L(2)  ;
		return;
	else
       		for i in 1..nbv loop
       			v1 := L(i)   ;
			for j in i..nbv loop
		        	if L(j) = v1+1 or L(j) = v1-1 then
       		                        consec := true;
					vi1 := v1;
			       		vi2 := L(j);
                                      return;
		      		end if;
			end loop;			
	        end loop;
end if;	
if consec = false then
   new_line;	
   put_line("LES DEUX VIRAGES SONT NON CONSECUTIFS... ");
   return;	
end if;

end SCHEMAT_SELECT;




procedure SCHEMAT_MESURES(ligne              : IN point_liste_type;
			  coord_pi           : IN point_liste_type;
		          points_pred_pi     : IN liens_array_type;
		          coord_sommets      : IN point_liste_type;
			  points_pred_sommets: IN liens_array_type;
	  		  L_int              : OUT liens_array_type;		
	                  card1              : OUT integer;			  
			  L_aut              : OUT liens_array_type;
			  card2              : OUT integer   	) is


nbvirages,numero_virage : integer:=0; 
long,haut,base,sym,f,t,s,l,somme_forme, somme_taille, somme_longueur:float:=0.0;
T_indicateur,T_symetrie,T_longueur,T_forme,T_taille:
reel_tableau_type(1..coord_sommets'length); 
T_virages_trie_longueur,T_virages_trie_forme, T_virages_trie_taille,
T_virages_trie_symetrie:
liens_array_type(1..coord_sommets'length);
T_virages, T_virages_trie :liens_array_type(1..coord_sommets'length) ;


-----------------------------------------------
-- MESURES DE  QUALIFICATION D'UN VIRAGE

-- Pour chaque fonction:
-- Ligne est la liste des points de la ligne a etudier
-- Coord_PI est la liste des points d'inflexion
-- Pts_pred_PI est la liste des numeros de points de Ligne precedant les PI
-- Coord_som est la liste des sommets des virages
-- Pts_pred_som est la liste des numeros de points de Ligne precedant les 
--   sommets
-- Numero virage est le numero du virage a qualifer

-- LONGUEUR_D_UN_VIRAGE
-- renvoie la longueur (en unite points PlaGe) du virage

function Longueur_d_un_virage(Ligne         : Point_liste_type;
                              Coord_PI      : Point_liste_type;
                              Pts_pred_PI   : Liens_array_type;
                              Coord_som     : Point_liste_type;
                              Pts_pred_som  : Liens_array_type;
                              Numero_virage : in natural)
                              return float is
Longueur : float;

begin
  Longueur := Norme(Coord_PI(Numero_virage),
                    Ligne(Pts_pred_PI(Numero_virage)+1));
  for i in Pts_pred_PI(Numero_virage)+1..Pts_pred_PI(Numero_virage+1)-1 loop
    Longueur := Longueur + Norme(Ligne(i),Ligne(i+1));
  end loop;
  Longueur := Longueur + Norme(Ligne(Pts_pred_PI(Numero_virage+1)),
                               Coord_PI(Numero_virage+1));

return Longueur;
end Longueur_d_un_virage;



-- HAUTEUR_D_UN_VIRAGE
-- renvoie la hauteur (en unite points PlaGe) du virage, c'est a dire
-- la distance du sommet a la ligne de base

function Hauteur_d_un_virage(Ligne          : Point_liste_type;
                              Coord_PI      : Point_liste_type;
                              Pts_pred_PI   : Liens_array_type;
                              Coord_som     : Point_liste_type;
                              Pts_pred_som  : Liens_array_type;
                              Numero_virage : in natural)
                              return float is

begin
  return Distance_a_ligne(Coord_som(Numero_virage), Coord_PI(Numero_virage),
                          Coord_PI(Numero_virage+1), C_segment_type);
end Hauteur_d_un_virage;

-- BASE_D_UN_VIRAGE
-- renvoie la longueur de la base (en unite points PlaGe) du virage,
-- c'est a dire la distance entre les deux points de la base

function Base_d_un_virage(Ligne         : Point_liste_type;
                          Coord_PI      : Point_liste_type;
                          Pts_pred_PI   : Liens_array_type;
                          Coord_som     : Point_liste_type;
                          Pts_pred_som  : Liens_array_type;
                          Numero_virage : in natural)
                          return float is
begin
  return Norme(Coord_PI(Numero_virage),Coord_PI(Numero_virage+1));
end Base_d_un_virage;

-- SYMETRIE_D_UN_VIRAGE
-- renvoie une mesure de la symetrie d'un virage, c'est a dire, si D1 est
-- la distance curviligne du premier point du virage au sommet et
-- et D2 la distance curviligne du sommet au dernier point du virage
-- la mesure est egale a Min(D1,D2)/Max(D1,D2)
-- La mesure prend ses valeurs entre 0 et 1 et vaut 0 si le virage est tres
-- dyssymetrique, 1 si le virage est symetrique

function Symetrie_d_un_virage(Ligne         : Point_liste_type;
                              Coord_PI      : Point_liste_type;
                              Pts_pred_PI   : Liens_array_type;
                              Coord_som     : Point_liste_type;
                              Pts_pred_som  : Liens_array_type;
                              Numero_virage : in natural)
                              return float is

D1, D2, Dmax, Dmin : float;

begin
  D1 := Norme(Coord_PI(Numero_virage), Ligne(Pts_pred_PI(Numero_virage)+1));

  for i in Pts_pred_PI(Numero_virage)+1..Pts_pred_som(Numero_virage)-1 loop
    D1 := D1 + Norme(Ligne(i),Ligne(i+1));
  end loop;
  D1 := D1 + Norme(Ligne(Pts_pred_som(Numero_virage)),
                   Coord_som(Numero_virage));
  
  D2 := Norme(Coord_som(Numero_virage), 
              Ligne(Pts_pred_som(Numero_virage)+1));

  for i in Pts_pred_som(Numero_virage)+1..Pts_pred_PI(Numero_virage+1)-1 loop
    D1 := D1 + Norme(Ligne(i),Ligne(i+1));
  end loop;
  D1 := D1 + Norme(Ligne(Pts_pred_PI(Numero_virage+1)),
                   Coord_PI(Numero_virage+1));
  if D1 > D2 then
    Dmin := D2;
    Dmax := D1;
  else
    Dmin := D1;
    Dmax := D2;
  end if;
  
  return Dmin/Dmax;
  
end Symetrie_d_un_virage;

--**********************************************************************
-- Procedure Trier_tableau_croissant modifiee pour conserver les numeros de virages
--***********************************************************************
-- Cette procedure trie le tableau Tab par ordre croissant
-- modif du package geometrie pour des tableaux de float

procedure Trier_virages(Tab_mesures      : IN reel_tableau_type;
                        Tab_virages      : In Liens_array_type;    
			Tab_virages_trie : OUT Liens_array_type) is

Tab_mes2 : reel_tableau_type(Tab_mesures'range);
Tab_vir2 : Liens_array_type(Tab_virages'range);
min    : float;  
j_min	: integer;
Virage_min : natural;

Begin
  Tab_mes2 := Tab_mesures;
  Tab_vir2 := Tab_virages;

  For i in Tab_mes2'range Loop
    min := Tab_mes2(i);
    j_min := i;
    For j in (i+1)..Tab_mes2'last loop
      If (Tab_mes2(j) < min) Then
        j_min := j;
        min := Tab_mes2(j);
      End if;
    End loop; -- j
    Tab_mes2(j_min) := Tab_mes2(i);
    Tab_mes2(i) := min;
    Virage_min := Tab_vir2(j_min);
    Tab_vir2(j_min) := Tab_vir2(i);
    Tab_vir2(i) := Virage_min;

  End Loop; -- i


  Tab_virages_trie := Tab_vir2;

End Trier_virages;

-----------------------------------------------
-- FIN DES MESURES DE  QUALIFICATION D'UN VIRAGE




begin


card1 := 0;
card2 := 0;
nbvirages := coord_sommets'length;
card1 := 3;
card2 := nbvirages;
for i in 1..nbvirages loop
	T_virages(i):=0;
	T_virages_trie(i) := 0;
	T_indicateur(i) := 0.0;
	T_taille(i) := 0.0;
	T_virages_trie_taille(i):= 0;
	T_symetrie(i):= 0.0;
	T_virages_trie_symetrie(i):= 0;
	T_virages_trie_forme(i):= 0;
	T_forme(i):= 0.0;
	T_longueur(i) := 0.0;
	T_virages_trie_longueur(i) := 0;
end loop;
if nbvirages < 3  then
       	new_line;
         put_line("NOMBRE DE VIRAGES STRICTEMENT INFERIEUR A TROIS...");
	 put_line("UNE SCHEMATISATION SUPPRIMERAIT L'ARC...");
	 new_line; 
         return;
end if;
if nbvirages = 3 then
	L_int(1):=0;
        L_int(2):=0;
	L_int(3):=0;
end if;
if nbvirages >= 4 then
	L_int(1):=1;
	L_int(2):=nbvirages;
end if;
if nbvirages >= 3 and  nbvirages <= 5 then
       for i in 1..nbvirages loop
		numero_virage := i;	
		long:=longueur_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		haut:=hauteur_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage) ;
		base:=base_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage  ) ;
		sym:=symetrie_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		f:= (4.0*(haut*base) + (long*base))/5.0;
		t:= (4.0*haut + base)/5.0;
		l:= long;
		s:= sym;
		T_virages(i):=i;
		T_taille (i) := t;
		T_forme(i) := f;
		T_longueur(i):=l;
		T_symetrie(i):=s;
	end loop;
--normalisation des variables forme,taille,longueur
	Somme_forme := 0.0;
	Somme_taille := 0.0;
	Somme_longueur := 0.0;
	for i in 1..nbvirages loop
		Somme_forme := Somme_forme + T_forme(i); 
		Somme_taille := Somme_taille + T_taille(i);
	 	Somme_longueur := Somme_longueur + T_longueur(i); 
	end loop;
        for i in 1..nbvirages loop
                 T_forme(i):=T_forme(i) / Somme_forme;
 		 T_taille(i):=T_taille(i) / Somme_taille;
		 T_longueur(i):=T_longueur(i) / Somme_longueur;
 	end loop;
	for i in 1..nbvirages loop
		T_indicateur(i):= (2.0*T_forme(i) + T_taille(i) + T_longueur(i)) / 4.0;
	end loop; 
        trier_virages(T_indicateur,T_virages,T_virages_trie);
	trier_virages(T_forme,T_virages,T_virages_trie_forme);
	trier_virages(T_longueur,T_virages,T_virages_trie_longueur);
	trier_virages(T_symetrie,T_virages,T_virages_trie_symetrie);
	trier_virages(T_taille,T_virages, T_virages_trie_taille);
	for i in 1..nbvirages loop
		L_aut(i) := T_virages_trie(i);
	end loop;
	if nbvirages = 5  then
		L_int(3):= T_virages_trie_taille(nbvirages);
                -- Cas particulier ou on aura les virages 1,3, 5 interdits :
                -- On ne pourra pas trouver 2 virages consecutif a eliminer
                -- sans decontraindre le systeme : on autorise donc 
                -- l'elimination de tous les virages sauf le plus grand.
                L_int(1):=0;
                L_int(2):=0;
	end if;
 	return;
end if;

if nbvirages >= 6 and nbvirages <=10 then
        for i in 1..nbvirages loop
		numero_virage := i;
		long:=longueur_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		haut:=hauteur_d_un_virage(ligne, coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage) ;
		base:=base_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage  ) ;
		sym:=symetrie_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		f:= (4.0*(haut*base) + (long*base)) / 5.0;
		t:= (4.0*haut + base) / 5.0;
		l:= long;
		s:= sym;
	        T_virages(i):=i;
               	T_taille (i) := t;
		T_forme(i) := f;
		T_longueur(i):=l;
		T_symetrie(i):=s;
        end loop;
--normalisation des variables forme,taille,longueur
	Somme_forme := 0.0;
	Somme_taille := 0.0;
	Somme_longueur := 0.0;
	for i in 1..nbvirages loop
		Somme_forme := Somme_forme + T_forme(i); 
		Somme_taille := Somme_taille + T_taille(i);
	 	Somme_longueur := Somme_longueur + T_longueur(i); 
	end loop;
        for i in 1..nbvirages loop
                 T_forme(i):=T_forme(i) / Somme_forme;
 		 T_taille(i):=T_taille(i) / Somme_taille;
		 T_longueur(i):=T_longueur(i) / Somme_longueur;
 	end loop;
	for i in 1..nbvirages loop
		T_indicateur(i):= (T_forme(i) + T_taille(i) + T_longueur(i) + T_symetrie(i)) / 4.0;
	end loop; 
        trier_virages(T_indicateur,T_virages, T_virages_trie);	
	trier_virages(T_forme,T_virages,T_virages_trie_forme);
	trier_virages(T_longueur,T_virages,T_virages_trie_longueur);
	trier_virages(T_symetrie,T_virages,T_virages_trie_symetrie);
	trier_virages(T_taille,T_virages,T_virages_trie_taille);
	for i in 1..nbvirages loop	
		L_aut(i):=T_virages_trie(i);
	end loop;
	L_int(3) := T_virages_trie_taille(nbvirages); 
	return;
else
	for i in 1..nbvirages loop
	        numero_virage := i;
		long:=longueur_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		haut:=hauteur_d_un_virage(ligne, coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage) ;
		base:=base_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage  ) ;
		sym:=symetrie_d_un_virage(ligne,coord_pi,points_pred_pi,coord_sommets,
		points_pred_sommets,numero_virage);
		f:= (4.0*(haut*base) + (long*base))/5.0;
		t:= (4.0*haut + base)/5.0;
		l:= long;
		s:= sym;
	   	T_virages(i):=i;
		T_taille (i) := t;
		T_forme(i) := f;
		T_longueur(i):=l;
		T_symetrie(i):=s;
        end loop;
--normalisation des variables forme,taille,longueur
	Somme_forme := 0.0;
	Somme_taille := 0.0;
	Somme_longueur := 0.0;
	for i in 1..nbvirages loop
		Somme_forme := Somme_forme + T_forme(i); 
		Somme_taille := Somme_taille + T_taille(i);
	 	Somme_longueur := Somme_longueur + T_longueur(i); 
	end loop;
        for i in 1..nbvirages loop
                 T_forme(i):=T_forme(i) / Somme_forme;
 		 T_taille(i):=T_taille(i) / Somme_taille;
		 T_longueur(i):=T_longueur(i) / Somme_longueur;
 	 end loop;
	for i in 1..nbvirages loop
		T_indicateur(i):= (T_forme(i) + 2.0*T_taille(i) + 0.5*T_longueur(i) + 0.5*T_symetrie(i)) / 4.0;
	end loop; 
        trier_virages(T_indicateur,T_virages,T_virages_trie);
	trier_virages(T_forme,T_virages,T_virages_trie_forme);
	trier_virages(T_longueur,T_virages,T_virages_trie_longueur);
	trier_virages(T_symetrie,T_virages,T_virages_trie_symetrie);
	trier_virages(T_taille,T_virages,T_virages_trie_taille);
	for i in 1..nbvirages loop	
		L_aut(i):=T_virages_trie(i);
	end loop;
	L_int(3) := T_virages_trie_taille(nbvirages);
	return;
end if;

end SCHEMAT_MESURES;   
                                                  
-- Ancienne Procedure developpee par FL pour eliminer les 2n derniers virages
-- 12/03/96
----------------------------------------------------------------------

procedure SCHEMAT_EXTREMITE (	ligne		  : IN point_liste_type;
				n_points	  : IN integer;
				Nb_virage_elimine : IN integer;
				SIGMA		  : IN float;
				ligne_schematise  : IN OUT point_liste_type;
				np		  : IN OUT integer
				) is

 Ligne_lisse		    	: point_liste_reel(1..1000);        
 Tpoints_pred_pi		: liens_access_type; 
 Tpinflex,tpinflex_lisse 	: point_liste_type(1..300);         
 n_points_lisses 		: natural;
 nb_pts_inflex			: natural:= 0;
 np1				: natural:= 0;
 a,b 				: float:=0.0;
 point_centre			: gen_io.point_type;
 point_debut, point_fin		: gen_io.point_type;
 indice_debut, indice_fin	: integer;
 lg,d, sens :float;
 dx, dy, dxip, dyip: float;
 Epsilon : float;       
             
begin              
 -- ----------------------------------------------------------------------- --
 -- On choisit SIGMA de maniere a retenir les points d'inflexion principaux --
 -- et les plus alignes possible autour de l'axe principal de la serie      --
 -- ----------------------------------------------------------------------- --
 pts_caracteristique.INFLEXIONS( ligne, n_points, sigma, ligne_lisse,
        	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);

 ------------------------------------------------------------------
 -- Elimination des nb_virage_elimine en fin de serie de virages
 ------------------------------------------------------------------
 if nb_pts_inflex-1 > nb_virage_elimine then
    EPSILON:=Norme(Tpinflex(nb_pts_inflex),Tpinflex(nb_pts_inflex - 
                    nb_virage_elimine))
                    /float(nb_pts_inflex - nb_virage_elimine - 1);
    nb_pts_inflex:=nb_pts_inflex-nb_virage_elimine;
 else
    return;
 end if;

 -- --------------------------------------------------------------------
 -- Droite des moindres carres de la ligne des PI passant par le 1er PI:
 -- --------------------------------------------------------------------
 point_centre := tpinflex(1);
 Math_cogit.DROITE_REGRESSION_GENERALE (tpinflex(1..nb_pts_inflex), 
					point_centre,a,b);

 -- verification du sens de parcours des points par rapport a la droite
 -- des PI determinee precedemmment
 if b=0.0 then
    -- droite verticale, en fonction des y des PI, on va determiner le sens
    if tpinflex(nb_pts_inflex).coor_y-tpinflex(1).coor_y > 0 then
       sens := 1.0;
    else 
       sens := -1.0;
    end if;
 elsif a/b>1.0 or a/b<-1.0 then
       -- droite plutot verticale (plus de 45 degre), les y donneront le sens
       if tpinflex(nb_pts_inflex).coor_y-tpinflex(1).coor_y > 0 then
          sens := 1.0;
       else 
          sens := -1.0;
       end if;
    else
       -- droite plutot horizontale (moins de 45 degre), les x donneront le sens
       if tpinflex(nb_pts_inflex).coor_x-tpinflex(1).coor_x > 0 then
          sens := 1.0;
       else 
          sens := -1.0;
       end if;
 end if;

 text_io.put("Droite des moindres carres => a = ");flo_io.put(a,1,3,0);text_io.new_line(1);
 text_io.put("                              b = ");flo_io.put(b,1,3,0);text_io.new_line(1);

 -- --------------------------------------------------------------------
 -- Traitement de la serie de virages avant le point d'inflexion centre: 
 -- (avant dans le sens de parcours de la droite des PI 
 --   (de gauche vers droite)
                                     
 np1:= 0;
 indice_debut:= 1;
 point_debut:= point_centre;

 -- Traitement de chaque virage de la serie:
 for v in 1..nb_pts_inflex-1 loop
     text_io.put("Virage ");int_io.put(v);text_io.new_line(1);

     indice_fin:= tpoints_pred_pi(v+1);
     point_fin := tpinflex(v+1);

     -- Calcul de lg : longueur curviligne du virage:
     lg:= Norme(point_debut,ligne(indice_debut));             
     for i in indice_debut..indice_fin-1 loop
         lg:= lg + Norme(ligne(i),ligne(i+1));
     end loop;
     lg:= lg + Norme(ligne(indice_fin),point_fin);      

     -- Deplacement de chaque point du virage:
     d := Norme(point_debut,ligne(indice_debut));
     for i in indice_debut..indice_fin loop
         np1:= np1+1;
    	 dx:= EPSILON * sens * (-b) * (d/lg + float(v-1));
    	 dy:= EPSILON * sens * (a)  * (d/lg + float(v-1));
         ligne_schematise(np1).coor_x:= ligne(i).coor_x + integer(dx);
  	 ligne_schematise(np1).coor_y:= ligne(i).coor_y + integer(dy);
         if i/=indice_fin then 
     	    d:= d + Norme(ligne(i),ligne(i+1)); 
         end if;
     end loop;
     indice_debut:= indice_fin+1;
     point_debut:= point_fin;
 end loop;
 np1:=np1+1;
 ligne_schematise(np1).coor_x:= ligne(n_points).coor_x;
 ligne_schematise(np1).coor_y:= ligne(n_points).coor_y;

 np:=np1;
end;


end Schematisation; 
