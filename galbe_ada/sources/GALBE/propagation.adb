With Geometrie;
 use Geometrie;
With math_int_basic;
 use math_int_basic;

--With float_math_lib;						-- pour VMS avant ADA 95
with Ada.Numerics;
 use Ada.Numerics;							-- depuis ADA 95
with Ada.Numerics.Elementary_Functions;
 use Ada.Numerics.Elementary_Functions;		-- depuis ADA 95

With text_io;
 use text_io;

Package body Propagation is

Procedure Propagation_noeud_amorti (
	Mygr			: in out graphe_type;
	num_noeud		: in integer;
	num_arc_deplace	: in integer;
	P_final			: in point_type;
	Amortissement	: In float;
	Propage			: propagation_type;
	tab_arc_deplace	: in out liens_array_type;
	nb_arc_deplace	: in out integer)
is
	Package int_io is new integer_io(integer); use Int_io;
	Resolution : integer :=Gr_infos(mygr).resolution;
	my_noeud : noeud_type;
	tarc_issu : liens_array_type(1..20);
	tab_pts,tabout_pts : point_liste_type(1..gr_infos(mygr).max_pts_arc);
	nb_pts,sens : integer;
	P_final_suivant : point_type;
	Num_noeud_suivant :integer;
	Flag_poursuite : boolean;
begin
	my_noeud:=Gr_noeud(mygr,Num_noeud);
	tarc_issu(1..my_noeud.nombre_arc):=Gr_arcs_du_noeud(mygr,Num_noeud);
	-- Pour gerer le non bouclage infini de deplacement
	for i in 1..my_noeud.nombre_arc loop
		for j in 1..nb_arc_deplace loop
			if tarc_issu(i)=tab_arc_deplace(j) then
				return;
            end if;
        end loop;
    end loop;
	-- Modification des coordonnees du noeud
	Gr_mod_noeud(mygr,Num_noeud,P_Final);
	-- Modification des coordonnes du sommet des arcs + propagation
	for i in 1..my_noeud.nombre_arc loop
		-- Pour des problemes de boucle, il faut traiter en premier l'arc
		-- provoquant le deplacement
		if abs(tarc_issu(i)) = num_arc_deplace then
			-- arc provoquant le deplacement
			gr_points_d_arc(mygr,abs(tarc_issu(i)),tab_pts,nb_pts);
			if tab_pts(1) = my_noeud.coords then
				tab_pts(1):= P_final;
			end if;
			if tab_pts(nb_pts) = my_noeud.coords then
				tab_pts(nb_pts):= P_final;
			end if;
			Gr_mod_points_d_arc(mygr,abs(tarc_issu(i)),tab_pts,nb_pts);
			exit;
		end if;
	end loop;
	for i in 1..my_noeud.nombre_arc loop
		-- Traitement des arcs n'ayant pas provoque le deplacement
		if abs(tarc_issu(i)) = num_arc_deplace then -- arc provoquant le deplacement
			null;
		else -- les autres arcs issus du noeud qui subissent le deplacement
			if tarc_issu(i)>0 then 
				gr_points_d_arc(mygr,tarc_issu(i),tab_pts,nb_pts);
				sens:=+1;
				-- arc sortant donc les coordonnes du                         
				-- noeud sont au debut de la liste de points
			else
				tarc_issu(i):=-tarc_issu(i);
				gr_points_d_arc(mygr,tarc_issu(i),tab_pts,nb_pts);
				Sens:=nb_pts;
			end if;
			Deplacearc_amorti(
				Tab_pts,Tabout_pts,nb_pts,
				tab_pts(Sens),P_Final,Amortissement);
			-- pour la coherence, actuellement les noeuds (autres            
			-- que celui modifie) ne sont pas deplaces
			if sens=1 then
				P_final_suivant:=tabout_pts(nb_pts);
				num_noeud_suivant:=Gr_arc(mygr,tarc_issu(i)).noeud_fin;
				if tabout_pts(nb_pts)/=tab_pts(nb_pts) then
					Flag_poursuite:=true;
				else
					Flag_poursuite:=false;
				end if;
				tabout_pts(nb_pts):=tab_pts(nb_pts);
			else
				P_final_suivant:=tabout_pts(1);
				num_noeud_suivant:=Gr_arc(mygr,tarc_issu(i)).noeud_ini;
				if tabout_pts(1)/=tab_pts(1) then
					Flag_poursuite:=true;
				else
					Flag_poursuite:=false;
				end if;
				tabout_pts(1):=tab_pts(1);
			end if;
			Gr_mod_points_d_arc(mygr,tarc_issu(i),tabout_pts,nb_pts);
			-- Mise a jour de la liste des arcs deja deplaces en tenant compte
			-- du signe
			nb_arc_deplace:=nb_arc_deplace+1;
			if sens=+1 then
				tab_arc_deplace(nb_arc_deplace):=tarc_issu(i);
			else
				tab_arc_deplace(nb_arc_deplace):=-tarc_issu(i);
			end if;
			-- Propagation iterative
			if Propage = propagation_iterative and flag_poursuite=true then
				Propagation_noeud_amorti (
					Mygr,
					num_noeud_suivant,
					tarc_issu(i),
					P_final_suivant,
					Amortissement,
					Propage,
					tab_arc_deplace,
					nb_arc_deplace);
			end if;
		end if;
	end loop;
	exception  -- ## GESTION DES BUGS ## 
	when others =>
		Put("Plantage de propagation :");
		New_line;
		Put("           Numero du noeud :");
		Put(Num_noeud);
		New_line;
		Put("           Numero de l'arc :");
		Put(Num_arc_deplace);
		New_line;
end Propagation_noeud_amorti;

-------------------------------------------------------------------------------

Procedure Deplacearc_amorti (
	tab_in			: in out Point_liste_type;
	tab_out			: in out Point_liste_type;
	nb_pts			: in out integer;
	P_initial		: in point_type;
	P_final			: in point_type;
	Amortissement	: In float) 
IS
	--isDist,norme_delta : float;
	Delta_x,Delta_y : integer;
	P1arc : point_type;
	rang_precedent,Psca,Indicesortie : integer;
	delta_j,unit_delta_j,vecteur_j,Ajout : point_type_reel;
	Arrondi : Point_type;
	norme_delta_j,norme_vecteur_j, Norme_amorti : float;
	-- scalaire_j: float;
	proj_para,pousse : float;
	Dmin,rang_p1 : integer;
begin
	P1arc:=Projection_polylignes(tab_in,nb_pts,P_initial);
	Rang_p1:=RANG_POINT_PLUS_PROCHE(P_initial,tab_in,nb_pts);
	-- calcul du deplacement max
	delta_x:=P_final.coor_x - P_initial.coor_x;
	delta_y:=P_final.coor_y - P_initial.coor_y;
	Dmin:=Distance_de_points(P1arc,tab_in(Rang_p1));
	if Dmin=0 then
		-- 1er cas : le pointe est deja sur un point intermediaire
		null;
	else
		-- 2eme cas : le pointe n'est pas sur un point intermediaire
		if rang_P1=nb_pts then
			Rang_precedent:=nb_pts-1;
		else
			Psca:=((P1arc.coor_x - tab_in(rang_p1).coor_x)*
				(P1arc.coor_x - tab_in(rang_p1+1).coor_x)) +
				((P1arc.coor_y - tab_in(rang_p1).coor_y)* 
				(P1arc.coor_y - tab_in(rang_p1+1).coor_y));
			if Psca>0 then
				rang_precedent:=rang_p1-1;
			else
				rang_precedent:=rang_p1;
			end if;
		end if;
		-- on va ajouter un point correspondant au pointe
		-- sur l'arc, different du point intermediaire
		for i in reverse rang_precedent+1..nb_pts loop
			tab_in(i+1):=tab_in(i);
		end loop;
		rang_p1:=rang_precedent+1;
		tab_in(rang_p1):=P1arc;
		nb_pts:=nb_pts+1;
	end if;
	-- Deplacement integral du point
	tab_out(rang_P1):=P_final;
	-- amortissement sur les points suivants
	Indicesortie:=nb_pts+1;
	for j in rang_P1+1..nb_pts loop
		-- determination du deplacement restant
		delta_j:=reel(tab_out(j-1) - tab_in(j-1));
		norme_delta_j:=Norme(tab_out(j-1),tab_in(j-1));
		if norme_delta_j <= sqrt(2.0) then
			indicesortie:=j;
			exit;
		end if;
		-- determination du vecteur unitaire du deplacement
		unit_delta_j:=delta_j/norme_delta_j;
		-- determination du vecteur entre les 2 points initiaux (a deplacer)
		vecteur_j:=Reel(tab_in(j) - tab_in(j-1));
		if vecteur_j=(0.0,0.0) then
			tab_out(j):=tab_out(j-1);
		else
			-- determination de la norme du vecteur precedent
			Norme_vecteur_j:=Norme(tab_in(j),tab_in(j-1));
			-- Determination du sens ou on effectue le deplacement :
			if Scalaire(unit_delta_j,vecteur_j) > 0.0 then
				-- on pousse le point j-1 vers le point j
				Pousse:=-1.0;
			else
				if Scalaire(unit_delta_j,vecteur_j) < 0.0 then
					-- on tire le point j-1 de facon opposee au point j
					Pousse:=+1.0;
				else
					-- Deplacement perpendiculaire au vecteur j-1,j
					Pousse:=0.0;
				end if;
			end if;
			-- Determination de la norme du vecteur de deplacement amorti
			Norme_amorti:=Amortissement*norme_delta_j;
			-- Pour eviter les situations abherentes physiquement, on limite
			-- la taille de ce vecteur par un Min qui est different
			-- si on pousse ou si on tire ou si on est perpendiculaire
			Proj_para:= -Pousse * Scalaire(unit_delta_j,vecteur_j);
			if Pousse=0.0 then
				Norme_amorti:=0.0;
				tab_out(j):=tab_out(j-1) + entier(vecteur_j);
			else
				if Pousse=-1.0 then
					-- on contraint que le point ne revienne pas en arriere de j-1
					Norme_amorti:=Min(Norme_amorti,Norme_vecteur_j**2/Proj_para);
				else
					-- on contraint a 2 fois la taille si Amoritssment/=1
					if Amortissement/=1.0 then
						Norme_amorti:=Min(Norme_amorti,2.0*Norme_vecteur_j);
					end if;
				end if;
				-- Gestion de probleme d'arrondi pour qu'on revienne sur la
				-- courbe intiale
				if Norme_amorti*unit_delta_j.coor_x=0.0 then
					Ajout.coor_x:=0.0;
				else
					if Norme_amorti*unit_delta_j.coor_x>0.0 then
						Ajout.coor_x:=Max(1.0,Norme_amorti*unit_delta_j.coor_x);
					else
						Ajout.coor_x:=Min(-1.0,Norme_amorti*unit_delta_j.coor_x);
					end if;
				end if;
				if Norme_amorti*unit_delta_j.coor_y=0.0 then
					Ajout.coor_y:=0.0;
				else
					if Norme_amorti*unit_delta_j.coor_y>0.0 then
						Ajout.coor_y:=Max(1.0,Norme_amorti*unit_delta_j.coor_y);
					else
						Ajout.coor_y:=Min(-1.0,Norme_amorti*unit_delta_j.coor_y);
					end if;
				end if;
				-- Calcul du point final
				tab_out(j):=tab_out(j-1) + entier(vecteur_j - Ajout);
				-- correction FL 25/02/99 : refus de la creation de points double
				if tab_out(j)=tab_out(j-1) then
					Arrondi:=(0,0);
					if tab_in(j).coor_x - tab_in(j-1).coor_x < 0 then
						Arrondi.coor_x:=-1;
					elsif tab_in(j).coor_x - tab_in(j-1).coor_x > 0 then
						Arrondi.coor_x:=1;
					end if;
					if tab_in(j).coor_y - tab_in(j-1).coor_y < 0 then
						Arrondi.coor_y:=-1;
					elsif tab_in(j).coor_y - tab_in(j-1).coor_y > 0 then
						Arrondi.coor_y:=1;
					end if;
					tab_out(j):=tab_out(j-1) + arrondi;
				end if;
			end if;
		end if;
	end loop;
	for i in Indicesortie..nb_pts loop
		tab_out(i):=tab_in(i);
	end loop;
	-- amortissement sur les points precedents
	Indicesortie:=0;
	for j in reverse 1..rang_P1-1 loop
		-- determination du deplacement restant a amortir
		delta_j:=reel(tab_out(j+1) - tab_in(j+1));
		norme_delta_j:=Norme(tab_out(j+1),tab_in(j+1));
		if norme_delta_j <= sqrt(2.0) then
			indicesortie:=j;
			exit;
		end if;
		-- determination du vecteur unitaire du deplacement
		unit_delta_j:=delta_j/norme_delta_j;
		-- determination du vecteur entre les 2 points initiaux (a deplacer)
		vecteur_j:=Reel(tab_in(j) - tab_in(j+1));
		if vecteur_j=(0.0,0.0) then
			tab_out(j):=tab_out(j+1);
		else
			-- determination de la norme du vecteur precedent
			Norme_vecteur_j:=Norme(tab_in(j),tab_in(j+1));
			-- Determination du sens ou on effectue le deplacement :
			if Scalaire(unit_delta_j,vecteur_j) > 0.0 then
				-- on pousse le point j+1 vers le point j
				Pousse:=-1.0;
			else
				if Scalaire(unit_delta_j,vecteur_j) < 0.0 then
					-- on tire le point j+1 de facon opposee au point j
					Pousse:=+1.0;
				else
					-- Deplacement perpendiculaire au vecteur j+1,j
					Pousse:=0.0;
				end if;
			end if;
			-- Determination de la norme du vecteur de deplacement amorti
			Norme_amorti:=Amortissement*norme_delta_j;
			-- Pour eviter les situations abherentes physiquement, on limite
			-- la taille de ce vecteur par un Min qui est different
			-- si on pousse ou si on tire ou si on est perpendiculaire
			Proj_para:= -Pousse * Scalaire(unit_delta_j,vecteur_j);
			if Pousse=0.0 then
				Norme_amorti:=0.0;
				tab_out(j):=tab_out(j+1) + entier(vecteur_j);
			else
				if Pousse=-1.0 then
					-- on contraint que le point ne revienne pas en arriere de j+1
					Norme_amorti:=Min(Norme_amorti,Norme_vecteur_j**2/Proj_para);
				else
					-- on contraint a 2 fois la taille si Amoritssment/=1
					if Amortissement/=1.0 then
						Norme_amorti:=Min(Norme_amorti,2.0*Norme_vecteur_j);
					end if;
				end if;
				-- Gestion de probleme d'arrondi pour qu'on revienne sur la
				-- courbe intiale
				if Norme_amorti*unit_delta_j.coor_x=0.0 then
					Ajout.coor_x:=0.0;
				else
					if Norme_amorti*unit_delta_j.coor_x>0.0 then
						Ajout.coor_x:=Max(1.0,Norme_amorti*unit_delta_j.coor_x);
					else
						Ajout.coor_x:=Min(-1.0,Norme_amorti*unit_delta_j.coor_x);
					end if;
				end if;
				if Norme_amorti*unit_delta_j.coor_y=0.0 then
					Ajout.coor_y:=0.0;
				else
					if Norme_amorti*unit_delta_j.coor_y>0.0 then
						Ajout.coor_y:=Max(1.0,Norme_amorti*unit_delta_j.coor_y);
					else
						Ajout.coor_y:=Min(-1.0,Norme_amorti*unit_delta_j.coor_y);
					end if;
				end if;
				-- Calcul du point final
				tab_out(j):=tab_out(j+1) + entier(vecteur_j - Ajout);
				-- correction FL 25/02/99 : refus de la creation de points double
				if tab_out(j)=tab_out(j+1) then
					Arrondi:=(0,0);
					if tab_in(j).coor_x - tab_in(j+1).coor_x < 0 then
						Arrondi.coor_x:=-1;
					elsif tab_in(j).coor_x - tab_in(j+1).coor_x > 0 then
						Arrondi.coor_x:=1;
					end if;
					if tab_in(j).coor_y - tab_in(j+1).coor_y < 0 then
						Arrondi.coor_y:=-1;
					elsif tab_in(j).coor_y - tab_in(j+1).coor_y > 0 then
						Arrondi.coor_y:=1;
					end if;
					tab_out(j):=tab_out(j+1) + arrondi;
				end if;
			end if;
		end if;
	end loop;
	for i in 1..Indicesortie loop
		tab_out(i):=tab_in(i);
	end loop;
end Deplacearc_amorti;


-------------------------------------------------------------------------------
Procedure Propagation_globale(
	Mygr : in out graphe_type;
	tab_arc_deplace: in liens_array_type;
	tab_arc_deplacant : in liens_array_type;
	nb_deplace : in out integer;
	Amortissement : in float;
	Propage : in Propagation_type)
is
	nb_occurence_noeud : liens_array_type(1..1000);
	tab_noeud_modifie : liens_array_type(1..1000);
	tab_coord_noeud : point_liste_type(1..gr_infos(mygr).max_pts_arc);
	tab_points,tabout_pts : point_liste_type(1..gr_infos(mygr).max_pts_arc);
	Num_noeud,nb_points,nb_noeud_modifie : integer;
	rencontre,flag_poursuite : boolean;
	P_final_suivant : point_type;
	sens : integer;
	nb_arc_deja_deplace : integer;
	tab_arc_deja_deplace : liens_array_type(1..4000);
	my_noeud : noeud_type;
	tarc_issu : liens_array_type(1..gr_infos(mygr).max_arc_neu);
	num_noeud_suivant : integer;
begin
	nb_noeud_modifie:=0;
	for i in 1..nb_deplace loop
		Gr_points_d_arc(Mygr,tab_arc_deplace(i),tab_points,nb_points);
		-- 1er noeud : noeud initial
		Num_noeud:=GR_arc(mygr,tab_arc_deplace(i)).noeud_ini;
		rencontre:=false;
		for j in 1..nb_noeud_modifie loop
			if num_noeud=tab_noeud_modifie(j) then
				nb_occurence_noeud(j):=nb_occurence_noeud(j)+1;
				tab_coord_noeud(j).coor_x:=tab_coord_noeud(j).coor_x + tab_points(1).coor_x;
				tab_coord_noeud(j).coor_y:=tab_coord_noeud(j).coor_y + tab_points(1).coor_y;
				rencontre:=true;
				exit;
			end if;
		end loop;
		if rencontre=false then
			nb_noeud_modifie:=nb_noeud_modifie+1;
			tab_noeud_modifie(nb_noeud_modifie):=Num_noeud;
			nb_occurence_noeud(nb_noeud_modifie):=1;
			tab_coord_noeud(nb_noeud_modifie):=tab_points(1);
		end if;
		-- 2eme noeud : noeud final
		Num_noeud:=GR_arc(mygr,tab_arc_deplace(i)).noeud_fin;
		rencontre:=false;
		for j in 1..nb_noeud_modifie loop
			if num_noeud=tab_noeud_modifie(j) then
				nb_occurence_noeud(j):=nb_occurence_noeud(j)+1;
				tab_coord_noeud(j).coor_x:=tab_coord_noeud(j).coor_x + tab_points(nb_points).coor_x;
				tab_coord_noeud(j).coor_y:=tab_coord_noeud(j).coor_y + tab_points(nb_points).coor_y;
				rencontre:=true;
				exit;
			end if;
		end loop;
		if rencontre=false then
			nb_noeud_modifie:=nb_noeud_modifie+1;
			tab_noeud_modifie(nb_noeud_modifie):=Num_noeud;
			nb_occurence_noeud(nb_noeud_modifie):=1;
			tab_coord_noeud(nb_noeud_modifie):=tab_points(nb_points);
		end if;
	end loop;
	-- calcul des coordonnees definitives des noeuds des arcs deplaces (moyenne)
	for i in 1..nb_noeud_modifie loop
		tab_coord_noeud(i).coor_x:=tab_coord_noeud(i).coor_x/nb_occurence_noeud(i);
		tab_coord_noeud(i).coor_y:=tab_coord_noeud(i).coor_y/nb_occurence_noeud(i);
	end loop;
	-- Mise a jour des points extremites des arcs deplaces 
	for i in 1..nb_deplace loop
		Gr_points_d_arc(Mygr,tab_arc_deplace(i),tab_points,nb_points);
		--	noeud initial
		Num_noeud:=GR_arc(mygr,tab_arc_deplace(i)).noeud_ini;
		for j in 1..nb_noeud_modifie loop
			if num_noeud=tab_noeud_modifie(j) then
				tab_points(1):=tab_coord_noeud(j);
				exit;
			end if;
		end loop;
		--	noeud final
		Num_noeud:=GR_arc(mygr,tab_arc_deplace(i)).noeud_fin;
		for j in 1..nb_noeud_modifie loop
			if num_noeud=tab_noeud_modifie(j) then
				tab_points(nb_points):=tab_coord_noeud(j);
				exit;
			end if;
		end loop;
		Gr_mod_points_d_arc(mygr,tab_arc_deplace(i),tab_points,nb_points);
	end loop;
	-- Modification  du noeud
	for i in 1..nb_noeud_modifie loop
		Gr_mod_noeud(mygr,tab_noeud_modifie(i),tab_coord_noeud(i));
	end loop;
	-- Il faut maintenant propager  sur tous les arcs issus de la liste des 
	-- noeuds modifies precedents, sauf sur les arcs deja deplaces  
	-- (ou ayant provoque le deplacement ???) 
	-- pour des problemes de signe dans la propagation, on est oblige de stocker  
	-- les arcs non bougeables avec leur 2 signes. 
	nb_arc_deja_deplace:=4*nb_deplace;
	tab_arc_deja_deplace(1..nb_deplace):=tab_arc_deplace(1..nb_deplace);
	for i in 1..nb_deplace loop
		tab_arc_deja_deplace(nb_deplace+i):= - tab_arc_deplace(i);
	end loop;
	tab_arc_deja_deplace(2*nb_deplace+1..3*nb_deplace):=tab_arc_deplacant(1..nb_deplace);
	for i in 1..nb_deplace loop
		tab_arc_deja_deplace(3*nb_deplace+i):= - tab_arc_deplacant(i);
	end loop;
	for i in 1..nb_noeud_modifie loop
		my_noeud:=Gr_noeud(mygr,tab_noeud_modifie(i));
		tarc_issu(1..my_noeud.nombre_arc):=Gr_arcs_du_noeud(mygr, tab_noeud_modifie(i));
		--
		nb_arc_deja_deplace:=4*nb_deplace;	-- CETTE LIGNE EST PEUX-ETRE A EFFACER. CODE MORT ? ###########################################
		for j in 1..my_noeud.nombre_arc loop
			rencontre:=false;
			for k in 1..nb_deplace loop
				if abs(tarc_issu(j))=tab_arc_deplace(k) then
					rencontre:=true;
					exit;
				end if;
			end loop;
			if rencontre=false then
				if tarc_issu(j)>0 then
					gr_points_d_arc(mygr,tarc_issu(j),tab_points,nb_points);
					sens:=+1;
					-- arc sortant donc les coordonnes du                        
					-- noeud sont au debut de la liste de points 
				else
					tarc_issu(j):=-tarc_issu(j);
					gr_points_d_arc(mygr,tarc_issu(j),tab_points,nb_points);
					Sens:=nb_points;
				end if;
				Deplacearc_amorti(Tab_points,Tabout_pts,nb_points, tab_points(Sens),tab_coord_noeud(i), Amortissement);
				-- pour la coherence, actuellement les noeuds (autres            
				-- que celui modifie) ne sont pas deplaces
				if sens=1 then
					P_final_suivant:=tabout_pts(nb_points);
					num_noeud_suivant:=Gr_arc(mygr,tarc_issu(j)).noeud_fin;
					if tabout_pts(nb_points)/=tab_points(nb_points) then
						Flag_poursuite:=true;
					else
						Flag_poursuite:=false;
					end if;
					tabout_pts(nb_points):=tab_points(nb_points);
				else
					P_final_suivant:=tabout_pts(1);
					num_noeud_suivant:=Gr_arc(mygr,tarc_issu(j)).noeud_ini;
					if tabout_pts(1)/=tab_points(1) then
						Flag_poursuite:=true;
					else
						Flag_poursuite:=false;
					end if;
					tabout_pts(1):=tab_points(1);
				end if;
				Gr_mod_points_d_arc(mygr,tarc_issu(j),tabout_pts,nb_points);
				-- Mise a jour de la liste des arcs deja deplaces en tenant compte
				-- du signe
				nb_arc_deja_deplace:=nb_arc_deja_deplace+1;
				if sens=+1 then
					tab_arc_deja_deplace(nb_arc_deja_deplace):=tarc_issu(j);
				else
					tab_arc_deja_deplace(nb_arc_deja_deplace):=-tarc_issu(j);
				end if;
				-- Propagation par les noeuds
				if flag_poursuite=true then
				--          and Propage =propagation_iterative then
					Propagation_noeud_amorti(
						Mygr,
						num_noeud_suivant,
						tarc_issu(j),
						P_final_suivant,
						Amortissement,
						Propage,
						tab_arc_deja_deplace,
						nb_arc_deja_deplace);
				end if;
			end if;
		end loop;
	end loop;
	nb_deplace:=0;
end Propagation_globale;


end;
