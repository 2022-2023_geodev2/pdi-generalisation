With Gen_io; use Gen_io;

Package Propagation is
	
	Type Propagation_type is (
		Propagation_nulle,
		Propagation_niveau1,
		Propagation_iterative);

	Procedure Propagation_noeud_amorti (
		Mygr			: in out graphe_type;
		num_noeud		: in integer;
		num_arc_deplace	: in integer;
		P_final			: in point_type;
		Amortissement	: In float;
		Propage			: propagation_type;
		Tab_arc_deplace	: in out liens_array_type;
		nb_arc_deplace	: in out integer);

	Procedure Deplacearc_amorti (
		tab_in			: in out Point_liste_type;
		tab_out			: in out Point_liste_type;
		nb_pts			: in out integer;
		P_initial		: in point_type;
		P_final			: in point_type;
		Amortissement	: In float);

	Procedure Propagation_globale(
		Mygr				: in out graphe_type;
		tab_arc_deplace		: in liens_array_type;
		tab_arc_deplacant	: in liens_array_type;
		nb_deplace			: in out integer;
		Amortissement		: in float;
		Propage				: in Propagation_type);

end Propagation;
