with gen_io; use gen_io;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

with math_int_basic; use math_int_basic;
with geometrie; use geometrie;

with unchecked_deallocation;
    

package vect is 

epsilon: float :=0.000000000001;

-------------------------------------------------
-- Declaration des types -------------------------
---------------------------------------------------

---------------------------
type vector is record 
                x,y:float;
              end record;
---------------------------
type elemline;
type line is access elemline;
type elemline is record
                   vect: vector;
                   prec,suiv: line;
                  end record;
---------------------------

vectnul: vector :=(0.0,0.0);
---------------------------

               
---------------------------------------------------
-- operations arithmethiques sur les vecteurs -------
------------------------------------------------------
function "+"(a,b : vector) return vector ;
function "-"(a,b : vector) return vector ;
function "*" (a: float; b : vector) return vector ;
function "/" ( b : vector; a:float ) return vector ;
   -- Les vecteurs du plan constituent un R-espace vectoriel


function "/"(a,b : vector) return vector ;
function "*"(a,b : vector) return vector ;
   -- Multiplication des nombres complexes. C est une R-algebre

function prod_scal(a,b : vector ) return float ;
function prod_vect(a,b: vector) return float ;
function unitaire(alpha: float) return vector;
function norme(a: vector) return float ;
function argument(a:vector) return float; 
   -- produit scalaire, produit vectoriel, et norme.

---------------------------------------------------
-- Operateurs de distances sur lignes et segments ---
------------------------------------------------------

function deviation(a,b:vector) return float;

function length(line_in: line) return float ; 
   --longueur de la ligne

function dist_point_segm(point: vector; segment: line;
			 epsilon: float:=0.000000001)
                                           return float ;
function dist_point_segm(point,extrem0,extrem1: vector;
			 epsilon: float:=0.000000001)
                                           return float ;
   -- distance d'un point a un segment.
   -- epsilon est le seuil d'approximation d'une longueur nulle

---------------------------------------------------
---- Creation et destruction de polylignes ---------
------------------------------------------------------
               
procedure recupere is new unchecked_deallocation(elemline,line);
procedure dissoudre(line_in: in out line);               
function duplicata(line_in: line) return line; 
  
---------------------------------------------------
-- compression d'information sur une polyligne -----
------------------------------------------------------

procedure comprime(line_in: line; seuil: in out float ;
                   nbpoint_out,mode:in out integer) ;

  -- seuil: valeur du deplacement maximal.
  -- nbpoint_out: nb de point en sortie.
  -- mode en entree:
  --  1:  La compression s'arrete des que le seuil est atteint
  --  2:  La compression s'arrete des que le nb de point est atteint
  --  3:  La compression s'arrete des que 1 ou 2 est realise.
  -- mode en sortie:
  --  0:  Il ne reste plus que deux points
  --  -1: Le critere 1 est realise
  --  -2: Le critere 2 est realise

---------------------------------------------------
-- Conversion entre polylignes "line" et point_liste_type"
------------------------------------------------------

procedure convert(line_in: line; tableau: out point_liste_type; 
					nbpoint_out :out integer);
procedure convert(tableau: point_liste_type; nbpoint:integer; 
                                             line_out:out line);  
procedure convert(line_in: line; tableau: point_access_type; 
					nbpoint_out :out integer);
procedure convert(tableau: point_access_type; nbpoint:integer; 
                                             line_out:out line);  
end vect;

---------------------------------------------------
---------------------------------------------------

