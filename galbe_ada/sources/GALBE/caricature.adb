With Lissage_filtrage;
With Morpholigne; use morpholigne; 
With Geometrie; use Geometrie;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

With math_cogit; use math_cogit;
with Text_io; 
-- packages de B. Hergott de triangulation de Delaunay + Arbre 
-- + squelettisation (adapte par Seb) 
with Squelettisation_seb; 
with Parbre; 
with Delaunay;
with vect ; use vect ;       
with pi_et_sommet; use pi_et_sommet;
with pts_caracteristique;  --- necessaire pour des anciennes procedures

package body Caricature is

  package int_io is new text_io.integer_io(integer);    use int_io;
  package flo_io is new text_io.float_io(float);        use flo_io;
  
  PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)
  
-- ------------------------------------------------------------------------ --
-- procedure fournissant la direction principale d'un virage
-- ------------------------------------------------------------------------ --
procedure direction_principale (ligne_in : point_liste_type ; 
                                angle     : out float ; 
                                confiance : out float )is 

 indicateur_direction: vector :=vectnul;
 lg: float:=0.0;

 vect: vector;
 indicateur_confiance, l_elem: float;

 begin 

 calcangle : for i in ligne_in'first..ligne_in'last-1 loop
     if ligne_in(i+1).coor_x = 0 and ligne_in(i+1).coor_y = 0
     then
       exit calcangle;
     else 
       vect.x:=float(ligne_in(i+1).coor_x - ligne_in(i).coor_x );
       vect.y:=float(ligne_in(i+1).coor_y - ligne_in(i).coor_y );
       l_elem := norme(vect) ;
       lg:=lg+l_elem ; 
         
       if l_elem > epsilon  then
          indicateur_direction:= indicateur_direction
	                        + vect * unitaire(argument(vect));
          -- Cette instruction somme les directions, 
          -- en operant dans C, l'espace des complexes, 
          -- quotiente par la relation d'equivalence  
          -- R: "est egal ou oppose a"
          -- ( u R v <=> u=v ou u=-v )
        end if;
      end if;
 end loop calcangle;  
       
 indicateur_confiance := norme(indicateur_direction) / lg ;
 if indicateur_confiance > epsilon 
 then   
   angle := 0.5*argument(indicateur_direction) ;
   confiance := indicateur_confiance ;         
 end if;
 end ; 

----------------------------------------------------------------------
-- POINT_DANS_RECTANGLE
-- Un point est il dans un rectangle aux bords "horizontaux et verticaux"

Function Point_dans_rectangle(P : Point_type_reel;
                              P1, P2 : Point_type) return boolean is

begin
  if integer(P.coor_X) <= Max((P1.coor_X,P2.coor_X)) and then
     integer(P.coor_X) >= Min((P1.coor_X,P2.coor_X)) and then
     integer(P.coor_Y) <= Max((P1.coor_Y,P2.coor_Y)) and then
     integer(P.coor_Y) >= Min((P1.coor_Y,P2.coor_Y)) then
     return true;
   else 
     return false;
   end if;
end Point_dans_rectangle;

----------------------------------------------------------------------
-- ACCORDEON
-- Developpee par Corinne 01/12/95
-- Modif par hugues 20/04/98
-- Modif par Seb M. 09/98
-- Modif par Cecile 01/99
-- Modif par Seb 02/99
-- Modif par FL 01/02
-- Modif par FL 02/02 (ajout des points d'inflexion dans la ligne traitee)
----------------------------------------------------------------------
-- Par defaut: determination automatique de sigma, et si sigma introuvable
-- on prend 20.0
-- Pour changer la valeur "a defaut d'automatique" de sigma, changer le
-- parametre sigma
-- Pour entrer une valeur de sigma et ne pas passer par la detection
-- automatique, mettre le param. Auto a False et changer le param. Sigma

procedure ACCORDEON (   Ligne 		: IN point_liste_type;
                        Largeur         : IN float;
			Ligne_acc       : OUT point_access_type;
			Auto		: IN boolean := TRUE;
			Sigma		: IN float := 20.0 ) is

  Ligne_dec     : Point_access_type; -- Modif FL 25/02/02
  Ligne_acc2,
  Ligne_virage,
  Coord_PI,
  Coord_sommets : Point_access_type;
  points_pred_sommets,
  Tab_limites,Tab_limites2   : Liens_access_type;
  Lg_seg        : Reel_access_type;
  Vect_recal,
  Pt_perp,
  Pt_milieu     : Point_type;
  Vect_ecart,
  Vect_maxi,
  Pt_inter      : Point_type_reel;

  Nb_pts_virage,
  Num_inters,
  Num_milieu    : natural;
  Angle,
  Confiance,
  Lg_vir,
  Lg_cur,
  Ecart,
  Dec_maxi,
  Sigma_calcule : float;
  Flag_auto,
  Inters,
  Pt_trouve     : boolean;
  Pt_debut 	: boolean; -- Modif FL 11/01/02
  Num_inters_global : natural; -- Modif FL 11/01/02
  k : integer;-- Modif FL 25/02/02

begin 
  -- determination des virages de la ligne
  Flag_auto := Auto;
  PI_et_sommet.Inflexions_sommets(Ligne,Flag_auto,Sigma,Tab_limites,
                                  Coord_PI,Points_pred_sommets,
				  Coord_sommets,Sigma_calcule,
                                  Milieu_des_sommets,Pas_de_sommet);
  -- NB: les pointeurs Coord_sommets et Points_pred_sommets seront
  -- renvoyes a null. Pas besoin de les desallouer donc.
  -- NB2: on n'utilise pas les coordonnees des PI
--  GR_free_point_liste(Coord_PI); -- Modif FL 25/02/02

  if Tab_limites = null then 
    -- si il y a eu un probleme dans la detection des PI: courage fuyons
    Ligne_acc := new point_liste_type'(Ligne);
    GR_free_point_liste(Coord_PI);
    return;
  end if;

  -- Modif FL 25/02/02  
  -- Fabrication de la ligne decalee incluant les points d'inflexion
  --  ligne_dec:=Ligne;  Modif FL 25/02/02
  tab_limites2:= new Liens_array_type(tab_limites'range);
  ligne_dec := new point_liste_type(1..ligne'last+coord_pi'last-2);
  k:=1;
  ligne_dec(1):=ligne(1);
  for i in tab_limites'first..Tab_limites'last-1 loop
      ligne_dec(tab_limites(i)+k..tab_limites(i+1)+k-1):=
                  ligne(tab_limites(i)+1..tab_limites(i+1));
      tab_limites2(i):=tab_limites(i)+k-1;
      if i/=Tab_limites'last-1 then
         ligne_dec(tab_limites(i+1)+k):=coord_pi(k+1);
      end if;
      k:=k+1;
  end loop;
  tab_limites2(tab_limites'last):=tab_limites(tab_limites'last)+k-2;         
  GR_free_point_liste(Coord_PI);
  GR_free_liens_array(Tab_limites);

-- Determination du point d'inflexion central pour le recalage final :
  -- son numero sur l'arc original, et ses coordonnees originales.
  Num_milieu := Tab_limites2(Integer(float(Tab_limites2'first
                                           +Tab_limites2'last)/2.0));
  Pt_milieu := ligne_dec(Num_milieu);



  -- Traitement de chaque virage de la serie:
  for i in Tab_limites2'first..Tab_limites2'last-1 loop
    -- fabrication d'un tableau de points correspondant au virage courant
    Nb_pts_virage := Tab_limites2(i+1)-Tab_limites2(i)+1;
    Ligne_virage := new Point_liste_type(1..Nb_pts_virage);
    Ligne_virage.all := ligne_dec(Tab_limites2(i)..Tab_limites2(i+1));

    -- determination de l'axe du virage, Angle dans  [0,Pi[
    Direction_principale(Ligne_virage.all, Angle, Confiance);

    -- determination du vecteur d'ecart maximal du virage

    -- recherche d'un point P du virage tel que la direction
    -- (P , Premier point du virage) soit perpendiculaire 
    -- a la direction du virage
    Pt_trouve := false;
    Pt_debut  := false;-- Modif FL 11/01/02

    -- determination d'un segment quelconque passant par le premier point 
    -- du virage et perpendiculaire a la direction principale
    Pt_perp := (Ligne_virage(1).coor_X+integer(1000.0*sin(Angle)), 
                Ligne_virage(1).coor_Y-integer(1000.0*cos(Angle)));

    -- recherche des intersections du virage avec la ligne definie par
    -- ce segment
    for j in reverse 3..Nb_pts_virage loop
      Intersection_de_lignes(Ligne_virage(1),Pt_perp,
                             Ligne_virage(j),Ligne_virage(j-1),
                             C_ligne_type, Inters, Pt_inter);
      if Inters and then 
        Point_dans_rectangle(Pt_inter, Ligne_virage(j), Ligne_virage(j-1)) then
        -- la droite definie avant coupe le segment du virage
        Pt_trouve := true;
        Pt_debut:= false; -- Modif FL 11/01/02 : le point se trouve en fin de ligne
        Num_inters := j-1; 
        Vect_ecart := Pt_inter - Reel(Ligne_virage(1));
        exit;
      end if;
    end loop;

    -- Si aucun point n'a ete trouve alors
    -- recherche d'un point P du virage tel que la direction
    -- (P , dernier point du virage) soit perpendiculaire 
    -- a la direction du virage
    if not Pt_trouve then
      -- determination d'un segment quelconque passant par le dernier point 
      -- du virage et perpendiculaire a la direction principale
      Pt_perp := (Ligne_virage(Nb_pts_virage).coor_X+integer(1000.0*sin(Angle)), 
                  Ligne_virage(Nb_pts_virage).coor_Y-integer(1000.0*cos(Angle)));
      for j in 1..Nb_pts_virage-2 loop
        Intersection_de_lignes(Ligne_virage(Nb_pts_virage),Pt_perp,
                               Ligne_virage(j),Ligne_virage(j+1),
                               C_ligne_type, Inters, Pt_inter);
        if Inters and then
          Point_dans_rectangle(Pt_inter, Ligne_virage(j), Ligne_virage(j+1)) then
          -- la droite definie avant coupe le segment du virage
          Pt_trouve := true;
          Pt_debut:= true; -- Modif FL 11/01/02 : le point se trouve en debut de ligne
          Num_inters := j;
          Vect_ecart := Reel(Ligne_virage(Nb_pts_virage)) - Pt_inter;
          exit;
        end if;
      end loop;
    end if;
    GR_free_point_liste(Ligne_virage); 

    if Pt_trouve then       -- ce doit etre quasiment toujours le cas
      -- Calcul du vecteur maximum de decalage, decalage qui sera a tous les pts
      -- entre le dernier point du virage et le pt d'intersection
      Ecart := Norme(Vect_ecart,(0.0,0.0));
      Dec_maxi := Max((0.0,Largeur-Ecart));
      Vect_maxi := Dec_maxi*Vect_ecart/Ecart;

      if Dec_maxi /= 0.0 then
        -- decalage des points du virage

        -- Modif FL 11/01/01 : il faut conserver Num_inters comme valeur utile
        -- car une partie seulement du virage est pris en compte pour le 
        -- decalage et non l'extremite
-- Ancien code :
--        Num_inters := Nb_pts_virage;
--        Lg_seg := new Reel_tableau_type(Tab_limites2(i)..Tab_limites2(i)+
--                                          Num_inters-2);
--        Lg_vir := 0.0;
--        for j in Tab_limites2(i)..Tab_limites2(i)+Num_inters-2 loop
--             Lg_seg(j) := Norme(Ligne_dec(j),Ligne_dec(j+1));
--             Lg_vir := Lg_vir + Lg_seg(j);
--        end loop;
-- Nouveau code : on utilise num_inters  
        Lg_seg := new Reel_tableau_type(Tab_limites2(i)..Tab_limites2(i)+
                                          Nb_pts_virage-1);
        Lg_vir := 0.0;
        Num_inters_global:=Tab_limites2(i)+Num_inters-1;

        if Pt_debut=true then
           for j in tab_limites2(i)..Num_inters_global loop
               lg_seg(j):=0.0;
           end loop;
           for j in Num_inters_global+1..Tab_limites2(i)+Nb_pts_virage-1 loop
               Lg_seg(j) := Norme(ligne_dec(j),ligne_dec(j-1));
               Lg_vir := Lg_vir + Lg_seg(j);
           end loop;
        else
           lg_seg(tab_limites2(i)):=0.0;
           for j in tab_limites2(i)+1..Num_inters_global+1 loop
               lg_seg(j):=Norme(ligne_dec(j),ligne_dec(j-1));
               Lg_vir := Lg_vir + Lg_seg(j);
           end loop;
           for j in Num_inters_global+2..Tab_limites2(i)+Nb_pts_virage-1 loop
               Lg_seg(j) := 0.0;
           end loop;
        end if;
-------- Fin du nouveau code FL

        Lg_cur := 0.0;
        for j in Tab_limites2(i)+1..Tab_limites2(i)+Nb_pts_virage-1 loop
            Lg_cur := Lg_cur + Lg_seg(j);
            ligne_dec(j) := ligne_dec(j) + Entier(Lg_cur*Vect_maxi/Lg_vir);
        end loop;
        GR_free_reel_tableau(Lg_seg);

        -- decalage des virages suivants
        for j in Tab_limites2(i)+Nb_pts_virage..ligne_dec'last loop
          ligne_dec(j) := ligne_dec(j) + Entier(Vect_maxi);
        end loop;
      end if;
    end if;
  end loop; -- fin boucle sur les virages
  GR_free_liens_array(Tab_limites2);

  -- Recalage general afin de recaler le point central de la ligne
  Vect_recal := Pt_milieu - ligne_dec(Num_milieu);
  Ligne_acc2 := new Point_liste_type(ligne_dec'range);
  for i in ligne_dec'range loop
    Ligne_acc2(i) := ligne_dec(i) + Vect_recal;
  end loop;
  Ligne_acc := Ligne_acc2;

  -- Modif FL 25/02/02
  GR_free_point_liste(ligne_dec);

end Accordeon;

----------------------------------------------------------------------
-- Procedure developpee par Corinne pour decaler une serie de virages
-- 01/12/95
----------------------------------------------------------------------

procedure ACCORDEON_ANCIEN (	ligne		  : IN point_liste_type;
				n_points	  : IN integer;
				EPSILON		  : IN OUT float;
				SIGMA		  : IN float;
                                e                 : IN float;
				ligne_caricature  : IN OUT point_liste_type;
				np		  : IN OUT integer		 
				) is

 Ligne_lisse		    	: point_liste_reel(1..1000);        
 Tpoints_pred_pi		: liens_access_type; 
 Tpinflex,tpinflex_lisse 	: point_liste_type(1..2*n_points);         
 n_points_lisses 		: natural;
 nb_pts_inflex			: natural:= 0;
 tab_sommets	        	: point_liste_type(1..2*n_points);
 nb_sommets		        : natural; 
 np1, np2			: natural:= 0;
 a,b 				: float:=0.0;
 p1,p2, point_centre		: gen_io.point_type;
 point_debut, point_fin		: gen_io.point_type;
 tab1, tab2			: point_liste_type(1..1000);         
 indice_centre, indice_pi_centre, indice_debut, indice_fin: integer;
 lg,d, sens :float;
 dx, dy, dxip, dyip: float;
 ajuster_parite : integer;
 Sigma_calcule : float;       
 Flag_termine : boolean;
 Alpha : angle_type;
             
begin              
 -- ----------------------------------------------------------------------- --
 -- On choisit SIGMA de maniere a retenir les points d'inflexion principaux --
 -- et les plus alignes possible autour de l'axe principal de la serie      --
 -- ----------------------------------------------------------------------- --

 -- Modification FL 26/02/98 : Determination automatique du sigma
 -- pour l'accordeon si on introduit la valeur nulle au depart.
 if sigma/=0.0 then
    pts_caracteristique.INFLEXIONS( ligne, n_points, sigma, ligne_lisse,
	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);
 else
   -- on va determiner automatiquement sigma en bouclant avec un pas de 1 :
   -- des qu'on n'a plus de configuration ou Pi Si Pi+1 
   -- fait un angle superieur a 100 degre, on a atteint le bon sigma
    for i in 1..80 loop
        sigma_calcule:=float(i)*1.0;

        pts_caracteristique.INFLEXIONS( ligne, n_points, sigma_calcule, 
          ligne_lisse,nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);
        Pts_caracteristique.SOMMETS( ligne, n_points, sigma_calcule, 
                  nb_pts_inflex, Tpoints_pred_pi,tab_sommets, nb_sommets); 

        Flag_termine:=true;
        for j in 1..nb_pts_inflex-1 loop
            Alpha:=Angle_3points(tpinflex(j),tab_sommets(j),tpinflex(j+1));
            if abs(Alpha)>100.0 then 
               Flag_termine:=false;
               exit;
            end if;
        end loop;

        if Flag_termine=true then
           exit;
        else
           GR_Free_liens_array(Tpoints_pred_pi);        
        end if;
    end loop;

    if Flag_termine=true  then
       Text_io.put("Calcul automatique de sigma : ");
       flo_io.put(sigma_calcule); text_io.new_line;
    else
       text_io.put("Impossibilite de trouver Sigma. La valeur est fixee a 20.0"); text_io.new_line;
       sigma_calcule:=20.0;
       pts_caracteristique.INFLEXIONS( ligne, n_points, sigma_calcule, 
         ligne_lisse,nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);
    end if;
 end if;
 --- Fin de modification;



 -- ------------------------------------------------------------------------
 -- CAS PARTICULIER IMPORTANT : Nb de virages = 1 (soit 2 points d'inflexion
 -- qui sont les 2 noeuds)
 -- On va decaler par rapport au milieu des 2 points d'inflexion
 -- et suivant la droite qui relie les 2 points d'inflexion
 ----------------------------------------------------------------------
 if nb_pts_inflex = 2 then

    -- determination des coefs a et b de la droite qui passe par le 1er point 
-- Modification FL 13/02/98 car bug mauvaise utilisation de 
-- droite_regression_generale. Il vaut mieux prendre DROTIE_REGRESSION
-- qu'on recale sur le point_centre (que droite_regression_generale qui passe
-- par point_centre)
    point_centre:=tpinflex(1);
    DROITE_REGRESSION (tpinflex(1..nb_pts_inflex),a,b);

    -- Determination du sens de parcours des points par rapport a la droite
    -- des PI determinee precedemmment
    if a*float((tpinflex(nb_pts_inflex).coor_y-tpinflex(1).coor_y)) - 
       b*float((tpinflex(nb_pts_inflex).coor_x-tpinflex(1).coor_x)) <0.0 then
       sens:=-1.0;
    else 
       sens:=1.0;
    end if;

    -- calcul de la longueur du virage
    lg:=0.0;
    for i in 1..n_points-1 loop
        lg:= lg + Norme(ligne(i),ligne(i+1));
    end loop;

    -- deplacement des points du virage proportionnellement a l'abcisse 
    -- curviligne du point par rapport au point d'inflexion = noeud initia
    d := 0.0;
    for i in 1..n_points-1 loop
        dx:= EPSILON * sens * (-b) * (d/lg - 0.5); 
        dy:= EPSILON * sens *  a  * (d/lg - 0.5);
        ligne_caricature(i).coor_x:= ligne(i).coor_x+integer(dx);
        ligne_caricature(i).coor_y:= ligne(i).coor_y+integer(dy);
        d:= d + Norme(ligne(i),ligne(i+1));
     end loop;
    dx:= EPSILON * sens * (-b) * 0.5; 
    dy:= EPSILON * sens *  a  * 0.5;
    ligne_caricature(n_points).coor_x:= ligne(n_points).coor_x+integer(dx);
    ligne_caricature(n_points).coor_y:= ligne(n_points).coor_y+integer(dy);

    np:=n_points;

 else
 -- ------------------------------------------------------------------- --
 -- CAS GENERAL : Nb de virages >1
 -- Nb de virages pair ou impair, on decalera par rapport a un point 
 -- d'inflexion 'central',dans la direction de la droite des moindre carres
 -- reliant les points d'inflexion
 -- ------------------------------------------------------------------- --
    if (nb_pts_inflex mod 2) /= 0 then
  	indice_pi_centre := nb_pts_inflex/2 +1;
        ajuster_parite:=1; 
    else
  	indice_pi_centre := nb_pts_inflex/2;
        ajuster_parite:=0; 
    end if;

    -- ----------------------------------------------
    -- Droite des moindres carres de la ligne des PI:
    -- ----------------------------------------------
-- Modification FL 13/02/98 car bug mauvaise utilisation de 
-- droite_regression_generale. Il vaut mieux prendre DROTIE_REGRESSION
-- qu'on recale sur le point_centre (que droite_regression_generale qui passe
-- par point_centre)
-- point_centre:= tpinflex(indice_pi_centre);
--    DROITE_REGRESSION_GENERALE (tpinflex(1..nb_pts_inflex), point_centre,a,b);
   DROITE_REGRESSION (tpinflex(1..nb_pts_inflex), a,b);
   point_centre:= tpinflex(indice_pi_centre);

    -- Determination du sens de parcours des points par rapport a la droite
    -- des PI determinee precedemmment
    if a*float((tpinflex(nb_pts_inflex).coor_y-tpinflex(1).coor_y)) - 
       b*float((tpinflex(nb_pts_inflex).coor_x-tpinflex(1).coor_x)) <0.0 then
       sens:=-1.0;
    else 
       sens:=1.0;
    end if;

 text_io.put("Droite des moindres carres => a = ");flo_io.put(a,1,3,0);text_io.new_line(1);
 text_io.put("                              b = ");flo_io.put(b,1,3,0);text_io.new_line(1);

    -- --------------------------------------------------------------------
    -- Traitement de la serie de virages avant le point d'inflexion centre: 
    -- (avant dans le sens de parcours de la droite des PI 
    --   (de gauche vers droite)
                                     
    np1:= 0;
    indice_centre:= tpoints_pred_pi(indice_pi_centre)+1;
    indice_debut:= indice_centre;
    point_debut:= point_centre;

    -- Traitement de chaque virage de la serie:
    for v in 1..indice_pi_centre-ajuster_parite loop
        text_io.put("Virage ");int_io.put(v);text_io.new_line(1);

        indice_fin:= tpoints_pred_pi(indice_pi_centre + v);
        point_fin := tpinflex(indice_pi_centre + v);

        -- Calcul de lg : longueur curviligne du virage:
        lg:= Norme(point_debut,ligne(indice_debut));             
        for i in indice_debut..indice_fin-1 loop
            lg:= lg + Norme(ligne(i),ligne(i+1));
        end loop;
        lg:= lg + Norme(ligne(indice_fin),point_fin);      

        -- Deplacement de chaque point du virage:
--        d := Norme(point_centre,ligne(indice_debut));
        d := Norme(point_debut,ligne(indice_debut));
        for i in indice_debut..indice_fin loop
            np1:= np1+1;
       	    dx:= EPSILON * sens * (-b) * (d/lg + float(v-1));
    	    dy:= EPSILON * sens * (a)  * (d/lg + float(v-1));
    	    tab1(np1).coor_x:= ligne(i).coor_x + integer(dx);
    	    tab1(np1).coor_y:= ligne(i).coor_y + integer(dy);
            if i/=indice_fin then 
     	       d:= d + Norme(ligne(i),ligne(i+1)); 
            end if;
        end loop;
        indice_debut:= indice_fin+1;
        point_debut:= point_fin;
    end loop;

    -- --------------------------------------------------------------------
    -- Traitement de la serie de virages apres le point d'inflexion centre:  
    -- (avant dans le sens de parcours de la droite des PI 
    --   (de gauche vers droite)

    np2:= 0;
    indice_centre:= tpoints_pred_pi(indice_pi_centre);
    indice_debut:= indice_centre;
    point_debut:= point_centre;

    for v in 1..indice_pi_centre-1 loop
        text_io.put("Virage ");int_io.put(v);text_io.new_line(1);

        indice_fin:= tpoints_pred_pi(indice_pi_centre - v) + 1;
        if indice_fin = 2 then 
           -- le point d'inflexion est alors le noeud de la courbe et 
           -- il faut le prendre pour aller jusqu'au bout de la courbe
           indice_fin:=1;
        end if;
        point_fin := tpinflex(indice_pi_centre - v);

        lg:=Norme(point_debut,ligne(indice_debut));                             
        for i in indice_fin..indice_debut-1 loop
            lg:= lg + Norme(ligne(i),ligne(i+1));
        end loop;
        lg:= lg + Norme(ligne(indice_fin),point_fin);

--        d := Norme(ligne(indice_debut),point_centre);
        d := Norme(ligne(indice_debut),point_debut);
        for i in 1..indice_debut-indice_fin+1 loop
            np2:= np2+1;
            dx:= EPSILON * sens * (b) * (d/lg+ float(v-1)); 
            dy:= EPSILON * sens * (-a) * (d/lg+ float(v-1));
            tab2(np2).coor_x:= ligne(indice_debut-i+1).coor_x+integer(dx);
            tab2(np2).coor_y:= ligne(indice_debut-i+1).coor_y+integer(dy);
            if i/=indice_debut then 
 	       d:= d + Norme(ligne(indice_debut-i),ligne(indice_debut-i+1)); 
            end if;                          
        end loop;
        indice_debut:= indice_fin - 1;
        point_debut:= point_fin;
    end loop;

    np:= 0;
    for i in 1..np2 loop
        np:= np+1;
        ligne_caricature(np).coor_x:= tab2(np2-i+1).coor_x;
        ligne_caricature(np).coor_y:= tab2(np2-i+1).coor_y;
    end loop;
    for i in 1..np1 loop
        np:= np+1;
        ligne_caricature(np).coor_x:= tab1(i).coor_x;
        ligne_caricature(np).coor_y:= tab1(i).coor_y;
    end loop;

 end if;
  GR_Free_liens_array(Tpoints_pred_pi);

end Accordeon_ancien;

----------------------------------------------------------------------------
-- Procedure developpe par Xavier pour faire de la caricature en utilisant 
-- le principe de Lowe
-----------------------------------------------------------------------------

Procedure Der_lowe ( Ligne			: in point_liste_type;
		     n_points			: in natural;
		     sigma			: in float;
		     sigma_lowe			: in float;
		     coe			: in float;
		     Ligne_lisse2_dessin_lowe 	: out point_liste_type) is


-- Lignes ->
 Ligne_lisse2_lowe		: point_liste_reel(1..3000);
 Ligne_aux,Ligne_aux_lisse 	: point_liste_reel(1..3000);
 Ligne_aux_lowe_lisse 		: point_liste_reel(1..3000);
 Ligne_aux_lowe			: point_liste_reel(1..3000);
-- Parametres du lissage gaussien ->
 sigfin				: float := 1.0;
-- Parametres du decoupage de l'arc en pts equidistants ->
 lgr_segments			: liens_array_type(1..3000);
 pas	      			: float := 1.0;
 n_aux 				: natural := 1;
-- Autres variables ->
 l,i				: natural := 0;
 d1x,d1y,dnx,dny		: float;

------------------------
-- Vecteur (M(i)-M(i-1))
------------------------

Function Vecteur(	ligne_w	 : in point_liste_reel;
			AL	 : in reel_tableau_type;
			i	 : in integer;
			n_points : in natural) return point_type_reel is
j,fin	: natural;
vect	: point_type_reel;

Begin
  fin:=0;
  for j in 1..n_points loop
    if AL(i-j)=0.0 then
      vect.coor_x:=ligne_w(i).coor_x-ligne_w(i-j).coor_x;
      vect.coor_y:=ligne_w(i).coor_y-ligne_w(i-j).coor_y;
      fin:=1;
    end if;
    exit when fin=1;
  end loop;
  return vect;
End Vecteur;

------------------------
-- Vecteur (M(i)+M(i-1))
------------------------

Function Somme(	ligne_w	 : in point_liste_reel;
		AL	 : in reel_tableau_type;
		i	 : in integer;
		n_points : in natural) return point_type_reel is
j,fin	: natural;
som	: point_type_reel;

Begin
  fin:=0;
  for j in 1..n_points loop
    if AL(i-j)=0.0 then
      som.coor_x:=ligne_w(i).coor_x+ligne_w(i-j).coor_x;
      som.coor_y:=ligne_w(i).coor_y+ligne_w(i-j).coor_y;
      fin:=1;
    end if;
    exit when fin=1;
  end loop;
  return som;
End Somme;

--------------------------------------------
-- Remplissage du tableau des points alignes
--------------------------------------------

Function Aligne(ligne_w	 : in point_liste_reel;
		n_points : in natural) return reel_tableau_type is

AL	: reel_tableau_type(1..3000);
i	: integer;
det	: float;

Begin
  for i in 1..n_points loop
    AL(i):=0.0;
  end loop;
  for i in 2..n_points-1 loop
    det:=Vecteur(ligne_w,AL,i,n_points).coor_y*
	 Vecteur(ligne_w,AL,i+1,n_points).coor_x-
	 Vecteur(ligne_w,AL,i,n_points).coor_x*
	 Vecteur(ligne_w,AL,i+1,n_points).coor_y;
    if abs(det)<1.0E-05 then
      AL(i):=1.0;
    end if;
  end loop;
  return AL;
End Aligne;

-------------------------------------------------
-- Calcul des coordonnees des centres de courbure
-------------------------------------------------

Function CoorCenCourb(ligne_w    : in point_liste_reel;
		      n_points : in natural) return point_liste_reel is

i	: integer;
CC	: point_liste_reel(1..3000);
AL	: reel_tableau_type(1..3000);

Begin
  AL:=Aligne(ligne_w,n_points);
  for i in 2..n_points-1 loop
    if AL(i)=0.0 then
      if Vecteur(ligne_w,AL,i,n_points).coor_x=0.0 then
	CC(i).coor_y:=Somme(ligne_w,AL,i,n_points).coor_y/2.0;
	CC(i).coor_x:=(Vecteur(ligne_w,AL,i+1,n_points).coor_y/
		       Vecteur(ligne_w,AL,i+1,n_points).coor_x*
		       (Somme(ligne_w,AL,i+1,n_points).coor_y-
			Somme(ligne_w,AL,i,n_points).coor_y)+
		       Somme(ligne_w,AL,i+1,n_points).coor_x)/2.0;
      elsif Vecteur(ligne_w,AL,i+1,n_points).coor_x=0.0 then
	CC(i).coor_y:=Somme(ligne_w,AL,i+1,n_points).coor_y/2.0;
	CC(i).coor_x:=(Vecteur(ligne_w,AL,i,n_points).coor_y/
		       Vecteur(ligne_w,AL,i,n_points).coor_x*
		       (Somme(ligne_w,AL,i,n_points).coor_y-
			Somme(ligne_w,AL,i+1,n_points).coor_y)+
		       Somme(ligne_w,AL,i,n_points).coor_x)/2.0;
      else
	CC(i).coor_y:=((Vecteur(ligne_w,AL,i,n_points).coor_y/
		        Vecteur(ligne_w,AL,i,n_points).coor_x*
		        Somme(ligne_w,AL,i,n_points).coor_y)+
		       Somme(ligne_w,AL,i,n_points).coor_x-
		       (Vecteur(ligne_w,AL,i+1,n_points).coor_y/
		        Vecteur(ligne_w,AL,i+1,n_points).coor_x*
		        Somme(ligne_w,AL,i+1,n_points).coor_y)-
		       Somme(ligne_w,AL,i+1,n_points).coor_x)/
		      ((Vecteur(ligne_w,AL,i,n_points).coor_y/
		        Vecteur(ligne_w,AL,i,n_points).coor_x)-
		       (Vecteur(ligne_w,AL,i+1,n_points).coor_y/
		        Vecteur(ligne_w,AL,i+1,n_points).coor_x))/2.0;
	CC(i).coor_x:=((Vecteur(ligne_w,AL,i,n_points).coor_y/
		        Vecteur(ligne_w,AL,i,n_points).coor_x)*
		       (Somme(ligne_w,AL,i,n_points).coor_y-(2.0*CC(i).coor_y))+
		       Somme(ligne_w,AL,i,n_points).coor_x)/2.0;
      end if;
    end if;
  end loop;
  CC(1):=CC(2);
  CC(n_points):=CC(n_points-1);
  return CC;
End CoorCenCourb;

----------------------------
-- Orientation de la normale
----------------------------

Procedure Orient_norm(ligne_w    : in point_liste_reel;
	 	      n_points : in natural;
		      theta    : in out reel_tableau_type;
		      ray      : out point_liste_reel) is

i	: integer;
CC	: point_liste_reel(1..3000);
dx,dy	: float;

Begin
  CC:=CoorCenCourb(ligne_w,n_points);
  for i in 1..n_points loop
    ray(i).coor_x:=CC(i).coor_x-ligne_w(i).coor_x;
    ray(i).coor_y:=CC(i).coor_y-ligne_w(i).coor_y;
  end loop;
  for i in 1..n_points loop
    dy:=CC(i).coor_y-ligne_w(i).coor_y;
    dx:=CC(i).coor_x-ligne_w(i).coor_x;
    if abs(dx)<1.0E-05 then
      if dy<0.0 then 
		theta(i):=pi/2.0;
      else
		theta(i):=1.5*pi;
      end if;
    else
      theta(i):=Arctan(dy/dx);	-- maintenant la fonction Arctan(float, float) existe
      if dx>0.0 then
		if dy<0.0 then
		  if theta(i)<0.0 then
			theta(i):=theta(i)+2.0*pi;
		  end if;
		end if;
    	else
		if dy<0.0 then
		  if theta(i)>0.0 then
	  	theta(i):=theta(i)+pi;
		  end if;
		else
		  if theta(i)<0.0 then
			theta(i):=theta(i)+pi;
		  end if;
		end if;
      end if;
    end if;
  end loop;
End Orient_norm;

---------------------------------------------------
-- Correction de compression -> Algorithme de Lowe.
---------------------------------------------------

Function Ligne_lowe( ligne_lisse_gauss	: point_liste_reel;
		     sigma		: float;
		     sig,coe1		: float;
		     n_points	     	: natural) return point_liste_reel is

RAYON			: point_liste_reel(1..3000);
cor_X,cor_Y,som		: float;
pds_X,pds_Y,cor1,cor2,ray_courb,poids : float;
maxRx,maxRy,minRx,minRy : float;
i,j,k,kx,ky		: natural;
ligne_lisse_lowe	: point_liste_reel(1..3000);
theta			: reel_tableau_type(1..3000);
suite			: reel_tableau_type(0..3000);
rep			: character;

Begin
  Orient_norm(ligne_lisse_gauss,n_points,theta,RAYON);
  for i in 1..n_points loop
    if RAYON(i).coor_X>maxRx then
      maxRx:=RAYON(i).coor_X;
    elsif RAYON(i).coor_X<minRx then
      minRx:=RAYON(i).coor_X;
    end if;
    if RAYON(i).coor_Y>maxRy then
      maxRy:=RAYON(i).coor_Y;
    elsif RAYON(i).coor_Y<minRy then
      minRy:=RAYON(i).coor_Y;
    end if;
  end loop;

--##-- Ponderation des valeurs des rayons d'apres le contexte ---

  k:=0;  -- Nbre de points pour ponderer la valeur du rayon d'apres le contexte

 if k/=0 then
  if 2*k>n_points then
     k:=integer(float(n_points)/2.0);
  end if;
  rep:='A';  -- Decroissance Arithmetique (ou Geometrique)
  som:=0.0;
  if rep='A' then
    for i in 0..k loop
      suite(i):=float(k+1-i);
      som:=som+suite(i);
    end loop;
  else
    for i in 0..k loop
      suite(i):=1.0/float(i+1);
      som:=som+suite(i);
    end loop;
  end if;
  som:=2.0*som-suite(0);
  for i in 0..k loop
    suite(i):=suite(i)/som;
  end loop;
  for i in 1..n_points loop
    RAYON(i).coor_X:=suite(0)*RAYON(i).coor_X;
    RAYON(i).coor_Y:=suite(0)*RAYON(i).coor_Y;
    kx:=integer((maxRx-RAYON(i).coor_X)*float(k)/(maxRx-minRx));
    for j in 1..kx loop
      if i-j<1 then
	RAYON(i).coor_X:=RAYON(i).coor_X+2.0*suite(j)*RAYON(i+j).coor_X;
      elsif i+j>n_points then
	RAYON(i).coor_X:=RAYON(i).coor_X+2.0*suite(j)*RAYON(i-j).coor_X;
      else
	RAYON(i).coor_X:=RAYON(i).coor_X+
			 suite(j)*(RAYON(i+j).coor_X+RAYON(i-j).coor_X);
      end if;
    end loop;
    ky:=integer((maxRy-RAYON(i).coor_Y)*float(k)/(maxRy-minRy));
    for j in 1..ky loop
      if i-j<1 then
	RAYON(i).coor_Y:=RAYON(i).coor_Y+2.0*suite(j)*RAYON(i+j).coor_Y;
      elsif i+j>n_points then
	RAYON(i).coor_Y:=RAYON(i).coor_Y+2.0*suite(j)*RAYON(i-j).coor_Y;
      else
	RAYON(i).coor_Y:=RAYON(i).coor_Y+
			 suite(j)*(RAYON(i+j).coor_Y+RAYON(i-j).coor_Y);
      end if;
    end loop;
  end loop;
 end if;

--##-- Calcul de la correction adaptee de "lowe" ---

  for i in 1..n_points loop
    if RAYON(i).coor_X/=0.0 then
      cor_X:=RAYON(i).coor_X*(1.0-exp(-(sigma/RAYON(i).coor_X)**2/2.0));
    else
      cor_X:=0.0;
    end if;
    if RAYON(i).coor_Y/=0.0 then
      cor_Y:=RAYON(i).coor_Y*(1.0-exp(-(sigma/RAYON(i).coor_Y)**2/2.0));
    else
      cor_Y:=0.0;
    end if;
    Ray_courb:=sqrt(RAYON(i).coor_X*RAYON(i).coor_X+
		    RAYON(i).coor_Y*RAYON(i).coor_Y);
    pds_X:=RAYON(i).coor_X/ray_courb;
    pds_Y:=RAYON(i).coor_Y/ray_courb;
    cor1:=(0.8+0.8/sqrt(ray_courb))*(1.0-exp(-(sigma/ray_courb)**2/2.0));
    cor2:=sig/20.0*(0.5+0.2/sqrt(ray_courb))*exp(-(sigma/ray_courb)**2/2.0);
    poids:=arctan(sig-15.0)/PI+0.5;
    cor_X:=coe1*(poids*pds_X*(cor1+cor2)+(1.0-poids)*cor_X);
    cor_Y:=coe1*(poids*pds_Y*(cor1+cor2)+(1.0-poids)*cor_Y);
    ligne_lisse_lowe(i).coor_x:=ligne_lisse_gauss(i).coor_x-cor_X;
    ligne_lisse_lowe(i).coor_y:=ligne_lisse_gauss(i).coor_y-cor_Y;
  end loop;
  return ligne_lisse_lowe;
End Ligne_lowe;

--------------------------------------------------------------------------------
---------------------------  CORPUS PROGRAMI  ----------------------------------
--------------------------------------------------------------------------------

Begin

----------------------------------------------------------------------------
-- Morcelement de la ligne en segments egaux de lgr PAS (soit la resolution)
----------------------------------------------------------------------------
-- Correction FL 02-02-99 : Modif de Filtre_gaussien_r qui contient 
-- deja la decomposition
-- Avant :  Decompose_ligne(ligne,n_points,ligne_aux,n_aux,pas,lgr_segments);
--  Transformation de la ligne en coord. reelles
    For i in 1..n_aux loop
      Ligne_aux(i).Coor_x := Float(Ligne(i).Coor_x);
      Ligne_aux(i).Coor_y := Float(Ligne(i).Coor_y);
    End loop;

--------------------------------------------------------------
-- Lissage de la ligne par convolution avec un filtre Gaussien
--------------------------------------------------------------

  Lissage_filtrage.Filtre_Gaussien_r(ligne_aux(1..n_aux),n_aux,sigma,
		      	    ligne_aux_lisse(1..n_aux));

----------------------------------------------------------------
-- Correction de compression en fonction de sigma et de x" et y"
----------------------------------------------------------------

  Ligne_aux_lowe:=Ligne_lowe(ligne_aux_lisse(1..n_aux),sigma_lowe,
						sigma,coe,n_aux);

------------------------------------------------------------------------
-- Lissage de la ligne lowee par convolution avec un filtre Gaussien fin
------------------------------------------------------------------------

  sigfin:=sigma/10.0; -- sigma evalue au 10eme de sigma de Gauss ???
  if sigfin<1.0 then
    sigfin:=1.0;
  end if;
  Lissage_filtrage.Filtre_Gaussien_r(ligne_aux_lowe(1..n_aux),n_aux,sigfin,
		ligne_aux_lowe_lisse(1..n_aux));

-------------------------------------------------------------------
-- Deplacement de la ligne aux extremites pour assurer la connexite
-------------------------------------------------------------------

  d1x:=Ligne_aux_lowe_lisse(1).coor_x-float(Ligne(1).coor_x);
  dnx:=Ligne_aux_lowe_lisse(n_aux).coor_x-float(Ligne(n_points).coor_x);
  d1y:=Ligne_aux_lowe_lisse(1).coor_y-float(Ligne(1).coor_y);
  dny:=Ligne_aux_lowe_lisse(n_aux).coor_y-float(Ligne(n_points).coor_y);
  for i in 1..n_aux-1 loop
    Ligne_aux_lowe_lisse(i).coor_x:=ligne_aux_lowe_lisse(i).coor_x-d1x/float(i);
    Ligne_aux_lowe_lisse(i).coor_y:=ligne_aux_lowe_lisse(i).coor_y-d1y/float(i);
  end loop;
  for i in 2..n_aux loop
    Ligne_aux_lowe_lisse(i).coor_x:=
	ligne_aux_lowe_lisse(i).coor_x-dnx/float(n_aux+1-i);
    Ligne_aux_lowe_lisse(i).coor_y:=
	ligne_aux_lowe_lisse(i).coor_y-dny/float(n_aux+1-i);
  end loop;

-------------------------------------------------------------------------
-- Extraction des points correspondant aux homologues des points initiaux
-------------------------------------------------------------------------

  l:= 1;
  Ligne_lisse2_lowe(1):= Ligne_aux_lowe_lisse(1);
  for i in 2..n_points loop
    l:= l + lgr_segments(i-1);
    Ligne_lisse2_lowe(i):= Ligne_aux_lowe_lisse(l);
  end loop;

-------------------------------------------
-- Mise au format dessin de la ligne lissee
-------------------------------------------

  for i in 1..n_points loop
    Ligne_lisse2_dessin_lowe(i).coor_x:= integer(Ligne_lisse2_lowe(i).coor_x);
    Ligne_lisse2_dessin_lowe(i).coor_y:= integer(Ligne_lisse2_lowe(i).coor_y);
  end loop;

end Der_lowe;



----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
-- Procedure developpee par Emmanuel pour elargir un virage
-- 04/12/95
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

procedure Elargit(arc_tab_in: point_liste_type; 
                  nb_point: natural;
                  arc_tab_out: out point_liste_type; 
                  rayon_min: float)  is



-----------------------------------------------------------
--   Cet algorithme elargit tous les virages trop serres --
-- Pour cela, il rallonge les segments de chaque virage  --
-- de la quantite necessaire pour que les rayons de      --
-- courbure en chaque point soit superieurs a rayon_min  --
--   Une fois effectue ces allongements locaux, la poly- --
-- ligne obtenu subit une similitude afin de ramener les --
-- points extremites sur ceux de la polyligne originale. --
-----------------------------------------------------------

-- Pour des raisons d'harmonisation, la variable nb_point
--est passee en in-out, alors qu'elle ne change pas dans
-- l'algorithme presente ici.

--line_in et line_out sont deux intermediaires 
--     entre la procedure et la convention PLAGE.
-- variable_inutile ne sert qu'a remplir un champ 
--     "out" dans convert (le nombre de point).
line_in,line_out_principale: line;
variable_inutile: integer;



function elargissement(line_in: line; rayon_min: float := 1.0) return line is

 --line_in: ligne origine 
 --line_out: ligne generalisee
 --line1: pointeur de description du point courant sur la ligne origine
 --line2: pointeur de construction du point courant sur la ligne generalisee
 --vect0,vect1:segments precedent et suivant le point courant sur la ligne origine
 --vect_nouv: nouvelle position du point courant, sur la ligne generalisee
 --l0,l1: longueur de vect0 et de vect1
 --l_min_nouv: valeur minimum qu'il faut imposer a l0 et l1 pour assurer
 --     le respect de la contrainte de courbure sur le point designe par line1
 --l_min_anc: idem pour le point precedent le point courant. Cette
 --     contrainte s'applique a vect0, mais pas a vect1.
 line1,line2,line_out: line; 
 vect0,vect1,vect_nouv,base_in,base_out,orig: vector;
 l0,l1,l_min_anc,l_min_nouv: float;
no: integer :=1;
begin
  --verouillage du cas ou il n'y a pas de ligne
  if line_in=null then return(null); end if;
  --initialisation
  line_out:=new elemline'(vectnul,null,null);
  line1:=line_in.suiv;                              
  line2:=line_out;
  --verouillage du cas ou la ligne est reduite a un seul point
  if line1=null then return(line_out); end if;
  --suite et fin de l'initialisation
  vect1:=line1.vect-line_in.vect;
  l1:=norme(vect1);
  l_min_nouv:=l1;
  --debut des iterations sur le point courant
  while line1.suiv/=null loop
    --mise a jour des variables courantes
    vect0:=vect1;
    vect1:=line1.suiv.vect-line1.vect;
    l0:=l1;
    l1:=norme(vect1);
  no:=no+1;
    --mise a jour de l'ancienne contrainte 
    l_min_anc:=l_min_nouv;
    --calcul de la nouvelle contrainte
    l_min_nouv:=rayon_min*abs(prod_vect(vect0,vect1)) / 
               (l0*l1+prod_scal(vect0,vect1));
    --test de deternination de la plus forte contrainte
    if l_min_anc<l_min_nouv then l_min_anc:=l_min_nouv; end if;
    --test d'application de la contrainte
    if l0<l_min_anc then
      --la contrainte joue
--int_io.put(no);
      vect_nouv:=line2.vect+l_min_anc/l0*vect0;
    else
      --la contrainte ne joue pas
      vect_nouv:=line2.vect+vect0;
    end if;
    --construction du point courant sur la ligne generalisee
    line2.suiv:=new elemline'(vect_nouv,line2,null);
    --changement de point courant
    line1:=line1.suiv;
    line2:=line2.suiv;
  end loop;
  --terminaison de la boucle: test d'aplication de la derniere
  --contrainte sur le dernier segment
  if l1<l_min_nouv then
    --la contrainte joue
--int_io.put(no);
    vect_nouv:=line2.vect+l_min_nouv/l1*vect1;
  else
    --la contrainte ne joue pas
    vect_nouv:=line2.vect+vect1;
  end if;
  --construction du dernier point de la ligne generalisee
  line2.suiv:=new elemline'(vect_nouv,line2,null);
  --recalage de la ligne generalisee afin que ses extremites 
  --coincident avec celle de la ligne originale
  --calcul des constantes de similitude
  orig:=line_in.vect;
  base_in:=line1.vect-orig;
  base_out:=vect_nouv;
  --initialisation du point courant sur la ligne generalisee
  line2:=line_out;
  --application de la similitude a chaque point de la ligne generalisee
  while line2/=null loop
    line2.vect:=orig+line2.vect*base_in/base_out;
    line2:=line2.suiv;
  end loop;
  --la ligne generalisee est envoye a qui de droit.
  return(line_out);
end;


-------------------------------------------------------
-- Procedure principale
--    Cette procedure est une encapsulation visant a --
-- assurer l'adaptation aux specifications de PLAGE, --
-- la plate-forme de generalisation.                 --
-------------------------------------------------------

begin
    convert(arc_tab_in,nb_point,line_in);
    line_out_principale:=elargissement(line_in,rayon_min);
    convert(line_out_principale,arc_tab_out,variable_inutile);
    dissoudre(line_out_principale);
    dissoudre(line_in);
end;
                    



----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
-- Procedure developpee par Francois pour elargir des virages
-- 10/01/96
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
Procedure Baudruche_1_virage( PI1 : in point_type;
                              PI2 : in point_type;
                              Indice_sommet : natural;
                              Ligne : in out point_liste_type;
                              n_points : in natural;
                              Gonflement : in float) is

-- Principe du ballon baudruche qui se gonfle avec les 2 extremites 
-- (les 2 points d'inflexion) qui restent fixes et le maximum de 
-- gonflement se produisant a l'extremite opposee (=sommet)

-- ATTENTION : Gonflement est un floattant SIGNE :
--     	* si Gonflement > 0 : le decalage s'effectuera a gauche par rapport
--                            au sens de parcours de la ligne;
--     	* si Gonflement < 0 : le decalage s'effectuera a droite par rapport
--                            au sens de parcours de la ligne;


Ligne_aux : point_liste_type(0..n_points+1);
tab : point_liste_type(1..n_points);
S_ligne : float; 			-- abscisse curviligne de la polyligne;
d : float;   				-- abscisse curviligne courante
no : float;				-- norme 
u,v : integer;				-- vecteur normal
dx,dy : float;				-- deplacement du point


begin

   if indice_sommet < 1 or indice_sommet > n_points then
      return;
   else
      Ligne_aux(0):=PI1;
      Ligne_aux(1..n_points):=Ligne(1..n_points);
      Ligne_aux(n_points+1):=PI2;

      -- 1ere partie : les points AVANT le sommet
      -- Calcul de S_ligne : longueur curviligne du 1/2 virage:
      s_ligne:= Norme(PI1,ligne(1));
      for i in 1..indice_sommet-1 loop
          s_ligne:= s_ligne + Norme(ligne(i),ligne(i+1));
      end loop;


      -- Deplacement de chaque point du virage en fonction de l'abscisse
      -- curviligne
      d := Norme(PI1,ligne(1));
      for i in 1..indice_sommet loop
          -- en tout point de la courbe, on va calculer la bissectrice 
          -- aux 2 segments partant du point considere
          no:=norme(Ligne_aux(i-1),Ligne_aux(i+1));
          u:=(Ligne_aux(i+1).coor_x - Ligne_aux(i-1).coor_x);
          v:=(Ligne_aux(i+1).coor_y - Ligne_aux(i-1).coor_y);
          -- calcul alors du vecteur de deplacement
    	  dx:= Gonflement * float(-v)/no * d/S_ligne;
    	  dy:= Gonflement *  float(u)/no  * d/S_ligne;
    	  tab(i).coor_x:= ligne(i).coor_x + integer(dx);
    	  tab(i).coor_y:= ligne(i).coor_y + integer(dy);
          d:= d + Norme(ligne(i),ligne(i+1)); 
      end loop;

      -- 2eme partie : les points APRES le sommet
      -- Calcul de S_ligne : longueur curviligne du 1/2 virage:
      s_ligne:= 0.0;

      for i in indice_sommet..n_points-1 loop
          s_ligne:= s_ligne + Norme(ligne(i),ligne(i+1));
      end loop;

      S_ligne:=S_ligne + Norme(ligne(n_points),PI2);      


      -- Deplacement de chaque point du virage en fonction de l'abscisse
      -- curviligne
      d := S_ligne;
      for i in indice_sommet+1..n_points loop
          -- en tout point de la courbe, on va calculer la bissectrice 
          -- aux 2 segments partant du point considere
          no:=norme(Ligne_aux(i-1),Ligne_aux(i+1));
          u:=(Ligne_aux(i+1).coor_x - Ligne_aux(i-1).coor_x);
          v:=(Ligne_aux(i+1).coor_y - Ligne_aux(i-1).coor_y);
          -- calcul alors du vecteur de deplacement
    	  dx:= Gonflement * float(-v)/no * d/S_ligne;
    	  dy:= Gonflement * float(u)/no  * d/S_ligne;
    	  tab(i).coor_x:= ligne(i).coor_x + integer(dx);
    	  tab(i).coor_y:= ligne(i).coor_y + integer(dy);
          d:= d - Norme(ligne(i-1),ligne(i)); 
      end loop;
                                         
      Ligne(1..n_points):=tab(1..n_points);

   end if;

end;


Procedure BAUDRUCHE (	ligne		  : in point_liste_type;
			n_points	  : in natural;
			GONFLEMENT	  : in float;
			SIGMA		  : in float;
			ligne_caricature  : in out point_liste_type;
			np		  : in out natural		 
			) is

Ligne_lisse		: point_liste_reel(1..1000);        
Tpoints_pred_pi		: liens_access_type; 
Tpinflex,tpinflex_lisse : point_liste_type(1..300);         
n_points_lisses 	: natural;
nb_pts_inflex 		: natural:=0;
tab_sommets		: point_liste_type(1..300);
nb_sommets		: natural:=0;
sommet_courant,indice_sommet	: natural;
indice_debut,indice_fin	: natural;
nb_pts_virage 		: natural;
pv 			: integer;
signe			: float;
virage 			: point_liste_type(1..500);


begin
      -- Il faut d'abord determiner le sens de parcours de la  courbe
      -- pour savoir si on decalera a gauche ou a droite de la courbe
      -- suivant la courbure
      --!!!!!!!!!!!!!!!! Methode provisoire non fiable !!!
      pv:=(ligne(2).coor_x-ligne(1).coor_x)*(ligne(3).coor_y - ligne(2).coor_y)
         -(ligne(2).coor_y-ligne(1).coor_y)*(ligne(3).coor_x - ligne(2).coor_x);
      if pv<0 then
         signe:=1.0;
      else 
         signe:=-1.0;
      end if;

   -- Determination des points d'inflexion et des sommets
   pts_caracteristique.INFLEXIONS( ligne, n_points, sigma, ligne_lisse,
    	       nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);
   pts_caracteristique.SOMMETS( ligne, n_points, sigma, nb_pts_inflex, 
                              Tpoints_pred_pi,tab_sommets, nb_sommets);

   sommet_courant:=1;
   for v in 1..nb_pts_inflex-1 loop -- pour tous les virages
       indice_debut:=tpoints_pred_pi(v)+1;
       indice_fin:=tpoints_pred_pi(v+1);
       nb_pts_virage:=indice_fin - indice_debut + 1;
       virage(1..nb_pts_virage):=ligne(indice_debut..indice_fin);
       indice_sommet:=0;
       for i in 1..nb_pts_virage loop
           if virage(i) = tab_sommets(sommet_courant) then
              indice_sommet:=i;
              sommet_courant:=sommet_courant+1;
              exit;
           end if;
       end loop;
       if indice_sommet /= 0 then
          Baudruche_1_virage(Tpinflex(v), Tpinflex(v+1), Indice_sommet, 
                             virage, nb_pts_virage, signe*Gonflement);
          signe:=-signe;
       end if;
       ligne_caricature(indice_debut..indice_fin):=virage(1..nb_pts_virage);
       ligne_caricature(1):=ligne(1);
       ligne_caricature(n_points):=ligne(n_points);
       np:=n_points;

   end loop;

  GR_Free_liens_array(Tpoints_pred_pi);

end;



------------------------------------------------------------------------------
-- Procedure developpee par FL pour detecter les rangs des points d'une courbe
-- ou il existe de l'empatement, c'est a dire les points qui sont eloignes 
-- d'un des 2 bords d'une valeur superieure a la demi largeur plus un surplus
-- Normalement les rangs vont part 2 : 1er rang donne le debut de l'empatement
--                                     2eme rang donne la fin de l'empatement
-- 25/12/95
------------------------------------------------------------------------------

Procedure Rang_Pts_Empate ( Ligne       : in point_liste_type;
                            n_points    : in natural;
                            Demi_largeur: in float;
                            Max_surplus : in float;
                            Rang_pts_dr : out liens_array_type;
                            Nb_rangs_dr : out natural;
                            Rang_pts_ga : out liens_array_type;
                            Nb_rangs_ga : out natural)  is


arc_reel,arcdroit_reel,arcgauche_reel : Point_access_reel;
Parentg,Parentd : Liens_access_type;
N_droit,N_gauche : natural :=0;
Compteur : natural;
Flag_active : boolean;
Eloignement : float;

begin

  -- transformation de la liste de points en flottant
  arc_reel :=new point_liste_reel(1..n_points);
  Parentg:= new Liens_array_type(1..n_points);
  Parentd:= new Liens_array_type(1..n_points);

  for j in 1..n_points loop
      arc_reel(j).coor_x:=float(Ligne(j).coor_x);
      arc_reel(j).coor_y:=float(Ligne(j).coor_y);
  end loop;

  -- Calcul des bords gauche et droit de la ligne en tenant compte de la 
  -- demi-largeur du signe
--  symbolisation.calcul_bords(arc_reel,arcdroit_reel,arcgauche_reel,n_points,
--                                   N_droit,N_gauche,Demi_largeur);

  Morpholigne.Dilatation_arc(Arc_reel(1..n_points),Demi_largeur,
                                  Arcgauche_reel,Parentg,Arcdroit_reel,Parentd);                                                                 


  -- Droite : determination des rangs des points de la ligne "centrale"
  -- qui sont eloignes du bord droit de plus de Demi_largeur + Max_surplus

  flag_active:=false;
  compteur:=0;
  if  Arcdroit_reel/=null then
      N_droit:=Arcdroit_reel'length;
      for i in 2..n_points loop
         Eloignement:=Distance_a_polylignes(arc_reel(i),arcdroit_reel.all,N_droit,
                                         C_segment_type);
         if flag_active = false then
            if Eloignement > Demi_largeur + Max_surplus then
               compteur:=compteur+1;
               rang_pts_dr(compteur):=i-1;
               flag_active:=true;
            end if;
         else
            if Eloignement <= Demi_largeur + Max_surplus then
               compteur:=compteur+1;
               rang_pts_dr(compteur):=i;
               flag_active:=false;
            end if;
         end if;
      end loop;                
  end if;
  if flag_active=true then
     compteur:=compteur+1;
     rang_pts_dr(compteur):=n_points;
     flag_active:=false;     
  end if;
  Nb_rangs_dr:=compteur;


  -- Gauche : determination des rangs des points de la ligne "centrale"
  -- qui sont eloignes du bord gauche de plus de Demi_largeur + Max_surplus
  flag_active:=false;
  compteur:=0;
  if Arcgauche_reel/=null then 
     N_gauche:=Arcgauche_reel'length;
     for i in 2..n_points loop
         Eloignement:=Distance_a_polylignes(arc_reel(i),arcgauche_reel.all,N_gauche,
                                         C_segment_type);
         if flag_active = false then
            if Eloignement > Demi_largeur + Max_surplus then
               compteur:=compteur+1;
               rang_pts_ga(compteur):=i-1;
               flag_active:=true;
            end if;
         else
            if Eloignement <= Demi_largeur + Max_surplus then
               compteur:=compteur+1;
               rang_pts_ga(compteur):=i;
               flag_active:=false;
            end if;
         end if;
     end loop;                
  end if;
  if flag_active=true then
     compteur:=compteur+1;
     rang_pts_ga(compteur):=n_points;
     flag_active:=false;     
  end if;
  Nb_rangs_ga:=compteur;

  -- Liberation de la memoire
  Gr_free_point_liste_reel(arc_reel);
  Gr_Free_liens_array(Parentg);
  Gr_Free_liens_array(Parentd);
  Gr_free_point_liste_reel(arcgauche_reel);
  Gr_free_point_liste_reel(arcdroit_reel);

end Rang_Pts_empate;



----------------------------------------
-- SQUELETTE
----------------------------------------
-- Algorithme de caricature d'un virage
----------------------------------------
-- Sebastien Mustiere
-- Janv 98
----------------------------------------
-- DECALAGE_SQUELETTE
----------------------------------------
-- PROCEDURE EXPORTEE
----------------------------------------
-- Arcin : l'arc en entree
-- Largeur : la largeur du decalage du squelette
-- Courbure : le rayon de courbure minimal assure lors du lissage du squelette
-- Arcout : l'arc en sortie
----------------------------------------

procedure decalage_squelette(Arcin    : in Point_liste_type;
                             Largeur  : in float;
                             Courbure : in float;
                             Arcout   : out Point_access_type) is

Surface,
Squel,
Squel2,
Squel_lisse,
Squel_final, 
Arc_final,
Pts_chemin : Point_access_type;
Squel_dilate,
Squel_AR   : Point_access_reel;
Extremites,
Chemin,
Parent     : Liens_access_type;
Barycentre : point_type;
Dmax,
D1,
D2,
D          : float := 0.0;
Ordre      : boolean;
N,
Nlisse      : Natural;
Tree       : Parbre.Arbre;
Triangles  : Delaunay.a_liste_triangle;

begin
  if Arcin'length <=2 then
    Arcout := new Point_liste_type'(Arcin);
    return;
  end if;
  
  if Arcin'length <=4 then
    -- dans le cas d'un arc a 3 ou 4 points la squeletisation ne
    -- trouve pas de squelette. On considere alors que le squelette
    -- est le segment rejoignant le milieu de la base au barycentre
    -- des 3 ou 4 points
    Squel_lisse := new Point_liste_type(1..2);
    Squel_lisse(1) := Entier( Reel(Arcin(Arcin'first) 
                              + Arcin(Arcin'last))/2.0);
    Barycentre := (0,0);
    for i in Arcin'range loop
      Barycentre := Barycentre + Arcin(i);
    end loop;
    Squel_lisse(2) :=  Entier(Reel(Barycentre) / float(Arcin'length));
    Nlisse := 2;
    if Squel_lisse(2)=Squel_lisse(1) then 
       -- triangle microscopique, pas de squelette
       Arcout := new Point_liste_type'(Arcin);
       Gr_free_point_liste(Squel_lisse);
       return;
    end if; 
  else  
    -- cas general d'un arc a plus de 4 points

    -- fabrication de la surface a squelletiser
    Surface := new Point_liste_type(Arcin'range);
    Surface.all := Arcin;

    -- Determination du squelette
    Tree := Squelettisation_seb.Squelettise(Surface, 0.0,0.0);
    Gr_free_point_liste(Surface);

    -- recherche du chemin le plus long entre deux extremites 
    -- dans le squelette
    Extremites := Parbre.Extrait_extremites(Tree);
    if Extremites = null then
      arcout := null;
      return;
    end if;
    for i in 1..Extremites'length loop
      for j in i+1..extremites'length loop
        -- chemin le plus court entre extremites(i) et extremites(j):
        Parbre.Distance(Tree, extremites(i), extremites(j), N, chemin);
            -- N ne nous sert pas par la suite
        Pts_chemin := Parbre.Points_chemin(Tree, chemin);
        D := Geometrie.Taille_ligne(Pts_chemin.all,Pts_chemin'length);
        if D > Dmax then
          Squel := Pts_chemin;
          Dmax := D;
        else
          GR_free_point_liste(Pts_chemin);
        end if;
        GR_free_liens_array(chemin);
      end loop;
    end loop;
    GR_free_liens_array(Extremites);
    Parbre.Vide_arbre(Tree);
    if Squel = null then
      arcout := null;
      return;
    end if;
    -- mise "dans le bon ordre" du squelette et
    -- traitment des extemites pour assurer un bon recollage
    Squel2 := new Point_liste_type(1..squel'length+1);

    -- le premier point est le milieu de deux extremites du virage
    Squel2(1):= ((Arcin(Arcin'first).coor_X + Arcin(Arcin'last).coor_X)/2, 
              (Arcin(Arcin'first).coor_Y + Arcin(Arcin'last).coor_Y)/2); 
    D1 :=Geometrie.Distance_de_points(Squel(Squel'first),Arcin(Arcin'first))
       +Geometrie.Distance_de_points(Squel(Squel'first),Arcin(Arcin'last));
    D2 := Geometrie.Distance_de_points(Squel(Squel'last),Arcin(Arcin'first))
       +Geometrie.Distance_de_points(Squel(Squel'last),Arcin(Arcin'last));
    if D1 < D2 then
      -- arc dans le bon sens
      for i in 1..Squel'length loop
        Squel2(i+1) := Squel(i+Squel'first-1);
      end loop;
    else
      -- arc dans le mauvais sens
      for i in 1..Squel'length loop
        Squel2(i+1) := Squel(Squel'last-i+1);
      end loop;
    end if;
    GR_free_point_liste(Squel);

    -- lissage du squelette par Gauss
    if Courbure /= 0.0 then
      Squel_lisse := new Point_liste_type(1..Squel2'length);
      -- modif pour etre compatible avec la modif de 2005 dans lissage_filtrage (11.04.2023)
      --Lissage_filtrage.Filtre_gaussien_N(Squel2.all,Squel2'length,
      --                                  Courbure,Squel_lisse.all);  
      declare
      	nb: integer := Squel2'length;
      begin
        Lissage_filtrage.Filtre_gaussien_N(Squel2.all,nb,
                                          Courbure,Squel_lisse.all);
      end;
      -- fin de modif
      Nlisse := Squel2'length;
    else
      Squel_lisse := new Point_liste_type'(Squel2.all);
      Nlisse := Squel2'length;
    end if;
    GR_free_point_liste(Squel2);

  end if; -- fin condition sur nombre de points de Arcin

  -- decalage du squelette
  -- par morpholigne
  -- creation de l'arc aller et retour, puis dilatation
  Squel_AR := new Point_liste_reel(1..Nlisse*2-1);
  for i in 1..Nlisse loop
    Squel_AR(i) := (float(Squel_lisse(i).coor_X),float(Squel_lisse(i).coor_Y));
  end loop;
  for i in 1..Nlisse-1 loop
    Squel_AR(Nlisse+i) := (float(Squel_lisse(Nlisse-i).coor_X),
                           float(Squel_lisse(Nlisse-i).coor_Y));
  end loop;
  GR_free_point_liste(Squel_lisse);

  Morpholigne.Dilatation_gauche(Squel_AR.all, Largeur, Squel_dilate, Parent, false,0.2);
  GR_free_point_liste_reel(Squel_AR);
  GR_free_liens_array(Parent);

  -- mise dans le bon sens et passage en integer
  Arc_final := new Point_liste_type(Squel_dilate'range);
  D1:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'first),
           (float(Arcin(Arcin'first).coor_X),float(Arcin(Arcin'first).coor_Y)));
  D2:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'last),
           (float(Arcin(Arcin'first).coor_X),float(Arcin(Arcin'first).coor_Y)));
  if D1 < D2 then
    -- arc dans le bon sens
    for i in Squel_dilate'range loop
      Arc_final(i) 
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  else
    -- arc en sens inverse
    for i in Squel_dilate'range loop
      Arc_final(Squel_dilate'last-i+1) 
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  end if;
  GR_free_point_liste_reel(Squel_dilate);

  -- filtrage de l'arc decale
  -- Par Douglas
  Arcout := Arc_final;
  
end Decalage_squelette;
----------------------------------------------------------------------------
-- DECALAGE_SQUELETTE_TROU
-- presque identique a decalage squelette
-- difference = 2 arcs en entree et 2 arcs en sortie
-- Arcin1 et 2 : arcs en entree
-- Largeur : la largeur du decalage du squelette
-- Courbure : le rayon de courbure minimal assure lors du lissage du squelette
-- Arcout1 et 2 : arcs en sortie
-----------------------------------------------------------------------------

procedure DECALAGE_SQUELETTE_TROU(Arcin1   : in Point_liste_type;
                                  Arcin2   : in Point_liste_type;
                                  Largeur  : in float;
                                  Courbure : in float;
                                  Arcout1  : out Point_access_type;
                                  Arcout2  : out Point_access_type) is

Surface,
Squel,
Squel2,
Squel_lisse,
Squel_final, 
Arc_final1,
Arc_final2,
Pts_chemin : Point_access_type;
Squel_dilate,
Squel_lisse_reel   : Point_access_reel;
Extremites,
Chemin,
Parent     : Liens_access_type;
E12,E21,Barycentre : point_type;
Dmax,
D1,
D2,
D3,
D4,
D          : float := 0.0;
Ordre      : boolean;
N,
Nlisse      : Natural;
Tree       : Parbre.Arbre;
Triangles  : Delaunay.a_liste_triangle;

begin
  
  if Arcin1'length=2 and Arcin2'length=2 then
--les 2 arcs-causes du trou dans le symbole ne sont chacun composes que de 2 pts
--le squelette est alors une droite
  
    Squel_lisse := new Point_liste_type(1..2);
    Squel_lisse(1) := Entier( Reel(Arcin1(Arcin1'first) 
                              + Arcin2(Arcin2'last))/2.0);
    Squel_lisse(2) := Entier( Reel(Arcin2(Arcin2'first)
                              + Arcin1(Arcin1'last))/2.0);
    Nlisse := 2;
  else  
    -- cas general d'un arc a plus de 4 points

    -- fabrication de la surface a squelletiser
    Surface := new Point_liste_type(1..Arcin1'length+Arcin2'length);
    for i in 1..Arcin1'length loop
        Surface(i):=Arcin1(i);
    end loop;
    for j in 1..Arcin2'length loop
        Surface(j+Arcin1'length):=Arcin2(j);
    end loop;

    -- Determination du squelette
    Tree := Squelettisation_seb.Squelettise(Surface, 0.0,0.0);
    Gr_free_point_liste(Surface);

    -- recherche du chemin le plus long entre deux extremites 
    -- dans le squelette
    Extremites := Parbre.Extrait_extremites(Tree);
    if Extremites = null then
      arcout1 := null;
      arcout2 := null;
      return;
    end if;
    for i in 1..Extremites'length loop
      for j in i+1..extremites'length loop
        -- chemin le plus court entre extremites(i) et extremites(j):
        Parbre.Distance(Tree, extremites(i), extremites(j), N, chemin);
            -- N ne nous sert pas par la suite
        Pts_chemin := Parbre.Points_chemin(Tree, chemin);
        D := Geometrie.Taille_ligne(Pts_chemin.all,Pts_chemin'length);
        if D > Dmax then
          Squel := Pts_chemin;
          Dmax := D;
        else
          GR_free_point_liste(Pts_chemin);
        end if;
        GR_free_liens_array(chemin);
      end loop;
    end loop;
    GR_free_liens_array(Extremites);
    Parbre.Vide_arbre(Tree);
    if Squel = null then
      arcout1 := null;
      arcout2 := null;
      return;
    end if;
    -- mise "dans le bon ordre" du squelette et
    -- traitement des extremites pour assurer un bon recollage
    Squel2 := new Point_liste_type(1..squel'length);
--    --Squel2 regroupera Squelette et les deux extremites E12 et E21
--    --E12=milieu de (Arcin1'first,Arcin2'last)
--    --E21=milieu de (Arcin2'first,Arcin1'last)
    E12:=((Arcin1(Arcin1'first).coor_X+Arcin2(Arcin2'last).coor_X)/2,
          (Arcin1(Arcin1'first).coor_Y+Arcin2(Arcin2'last).coor_Y)/2);
    E21:=((Arcin2(Arcin2'first).coor_X+Arcin1(Arcin1'last).coor_X)/2,
          (Arcin2(Arcin2'first).coor_Y+Arcin1(Arcin1'last).coor_Y)/2);
    --D1=distance entre Squel(1) et E12
    --D2=distance entre Squel(1) et E21
    D1:=Geometrie.Norme(Squel(1),E12);
    D2:=Geometrie.Norme(Squel(1),E21);
    if D1 < D2 then
    -- arc dans le bon sens
--      Squel2(1):=E12;
      for i in 1..Squel'length loop
        Squel2(i) := Squel(i);
      end loop;
--      Squel2(Squel'length+2):=E21;
    else
    -- arc dans le mauvais sens
--      Squel2(1):=E12;
      for i in 1..Squel'length loop
        Squel2(i) := Squel(Squel'last-i+1);
      end loop;
--      Squel2(Squel'length+2):=E21;
    end if;
    GR_FREE_POINT_LISTE(Squel);

    -- lissage du squelette par Gauss
    if Courbure /= 0.0 then
      Squel_lisse := new Point_liste_type(1..Squel2'length);
      -- modif pour etre compatible avec la modif de 2005 dans lissage_filtrage (11.04.2023)
      --Lissage_filtrage.Filtre_gaussien_N(Squel2.all,Squel2'length,
      --                                  Courbure,Squel_lisse.all);
      declare
      	nb: integer := Squel2'length;
      begin
        Lissage_filtrage.Filtre_gaussien_N(Squel2.all,nb,
                                          Courbure,Squel_lisse.all);
      end;
      -- fin de modif
      Nlisse := Squel2'length;
    else
      Squel_lisse := new Point_liste_type'(Squel2.all);
      Nlisse := Squel2'length;
    end if;
    GR_FREE_POINT_LISTE(Squel2);
  end if;

--affichage des coordonnes du squelette dans le programme
--  for i in 1..Nlisse loop
--      int_io.put(integer(i));
--      put(squel_lisse(i).coor_x);
--      put(squel_lisse(i).coor_y);text_io.new_line;
--  end loop;
--  text_io.new_line;


--passage du squelette en coordonnes reelles
  Squel_lisse_reel:=new Point_liste_reel(1..Nlisse);
  for i in 1..Nlisse loop
      Squel_lisse_reel(i):=(float(Squel_lisse(i).coor_X),
                                          float(Squel_lisse(i).coor_Y));
  end loop;

--dilatation du squelette lisse  a gauche pour sortir Arc_final1
  Morpholigne.Dilatation_gauche(Squel_lisse_reel.all, Largeur, Squel_dilate,
                                                            Parent, false,0.2);
  GR_FREE_LIENS_ARRAY(Parent);

  -- mise dans le bon sens et passage en integer
  Arc_final1:=new Point_liste_type(Squel_dilate'range);
  D1:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'first),
       (float(Arcin1(Arcin1'first).coor_X),float(Arcin1(Arcin1'first).coor_Y)));
  D2:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'last),
       (float(Arcin1(Arcin1'first).coor_X),float(Arcin1(Arcin1'first).coor_Y)));

  if D1 < D2 then
    -- arc dans le bon sens
    for i in Squel_dilate'range loop
      Arc_final1(i) 
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  else
    -- arc en sens inverse
    for i in Squel_dilate'range loop
      Arc_final1(Squel_dilate'last-i+1) 
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  end if;

--test pour verifier si dilatation_gauche est la bonne dilatation pour TP1
  D3:=GEOMETRIE.DISTANCE_A_LIGNE((float(Arcin1(Arcin1'first).coor_X),
                                      float(Arcin1(Arcin1'first).coor_Y)),
                                Squel_dilate(Squel_dilate'first),
                                Squel_dilate(Squel_dilate'last),
                                                 C_segment_type);
  D4:=GEOMETRIE.DISTANCE_A_LIGNE((float(Arcin2(Arcin2'last).coor_X),
                                      float(Arcin2(Arcin2'last).coor_Y)),
                                 Squel_dilate(Squel_dilate'first),
                                 Squel_dilate(Squel_dilate'last),
                                                 C_segment_type);
  if D3<D4 then
     GR_free_point_liste_reel(Squel_dilate);
     Arcout1 := Arc_final1;

--traitement du 2eme arc par dilatation droite
     Morpholigne.Dilatation_droite(Squel_lisse_reel.all, Largeur, Squel_dilate,
                                                            Parent, false,0.2);
    GR_FREE_POINT_LISTE(Squel_lisse);
    GR_FREE_POINT_LISTE_REEL(Squel_lisse_reel); 
    GR_FREE_LIENS_ARRAY(Parent);

  -- mise dans le bon sens et passage en integer
    Arc_final2 := new Point_liste_type(Squel_dilate'range);
    D1:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'first),
       (float(Arcin2(Arcin2'first).coor_X),float(Arcin2(Arcin2'first).coor_Y)));
    D2:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'last),
       (float(Arcin2(Arcin2'first).coor_X),float(Arcin2(Arcin2'first).coor_Y)));
    if D1 < D2 then
    -- arc dans le bon sens
      for i in Squel_dilate'range loop
        Arc_final2(i)
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
      end loop;
    else
    -- arc en sens inverse
      for i in Squel_dilate'range loop
        Arc_final2(Squel_dilate'last-i+1) := (integer(Squel_dilate(i).coor_X),
                                           integer(Squel_dilate(i).coor_Y));
      end loop;
    end if;
    GR_free_point_liste_reel(Squel_dilate);

    Arcout2 := Arc_final2;
--fin du cas ou dilatation_gauche pour TP1 est le bon choix
--------------------------------------------------------------------  


   else
--il faut traiter TP1 par droit et TP2 par gauche
     GR_FREE_POINT_LISTE_REEL(Squel_dilate);
 Morpholigne.Dilatation_droite(Squel_lisse_reel.all, Largeur, Squel_dilate,
                                                            Parent, false,0.2);
  GR_free_liens_array(Parent);

  -- mise dans le bon sens et passage en integer
  D1:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'first),
       (float(Arcin1(Arcin1'first).coor_X),float(Arcin1(Arcin1'first).coor_Y)));
  D2:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'last),
       (float(Arcin1(Arcin1'first).coor_X),float(Arcin1(Arcin1'first).coor_Y)));

  Arc_final1:=new Point_liste_type(Squel_dilate'range);
  if D1 < D2 then
    -- arc dans le bon sens
    for i in Squel_dilate'range loop
      Arc_final1(i)
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  else
    -- arc en sens inverse
  for i in Squel_dilate'range loop
      Arc_final1(Squel_dilate'last-i+1)
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
    end loop;
  end if;
  GR_FREE_POINT_LISTE_REEL(Squel_dilate);
  Arcout1:=Arc_final1;


--traitement du 2eme arc par dilatation gauche
     Morpholigne.Dilatation_gauche(Squel_lisse_reel.all, Largeur, Squel_dilate,
                                                            Parent, false,0.2);
    GR_FREE_POINT_LISTE(Squel_lisse);
    GR_FREE_POINT_LISTE_REEL(Squel_lisse_reel);
    GR_FREE_LIENS_ARRAY(Parent);

  -- mise dans le bon sens et passage en integer
    Arc_final2 := new Point_liste_type(Squel_dilate'range);
    D1:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'first),
       (float(Arcin2(Arcin2'first).coor_X),float(Arcin2(Arcin2'first).coor_Y)));
    D2:=Geometrie.Distance_de_points(Squel_dilate(Squel_dilate'last),
       (float(Arcin2(Arcin2'first).coor_X),float(Arcin2(Arcin2'first).coor_Y)));
    if D1 < D2 then
    -- arc dans le bon sens
      for i in Squel_dilate'range loop
        Arc_final2(i)
        := (integer(Squel_dilate(i).coor_X), integer(Squel_dilate(i).coor_Y));
      end loop;
    else
    -- arc en sens inverse  
      for i in Squel_dilate'range loop
        Arc_final2(Squel_dilate'last-i+1) := (integer(Squel_dilate(i).coor_X),
                                           integer(Squel_dilate(i).coor_Y));
     end loop;
   end if;
    GR_free_point_liste_reel(Squel_dilate);

  -- filtrage de l'arc decale
  -- Par Douglas
    Arcout2 := Arc_final2; 
end if;

end DECALAGE_SQUELETTE_TROU;


------------------------------------------------------------------------------
-- FAILLE_MAX
------------------------------------------------------------------------------
procedure Faille_max(Arc        : in Point_liste_type;
                     Largeur    : float;
                     Arc_traite : out Point_access_type) is

  Tabfin : Point_access_type;
  Tabfinf,
  Tabinif : Point_access_reel;
  Parent  : Liens_access_type;
  Surf : float;

begin
  Tabinif := new Point_liste_reel(Arc'range);
  Tabinif.all := Reel(Arc);
  Surf:=surface_arc(Arc&Arc(1),Arc'length+1);
  if Surf<0.0 then
    Dilatation_droite(Tabinif.all,Largeur,
                      Tabfinf,Parent,False,0.5);
  else
    Dilatation_gauche(Tabinif.all,Largeur,
                      Tabfinf,Parent,False,0.5);
  end if;
  Tabfin := new Point_liste_type(Tabfinf'range);
  Tabfin.all := Entier(Tabfinf.all);

  GR_free_point_liste_reel(Tabfinf);
  GR_free_point_liste_reel(Tabinif);
  GR_free_liens_array(Parent);

  Arc_traite := Tabfin;

end Faille_max;


end Caricature;
