with Gen_io; use Gen_io;
                     
Package caricature is

-----------------------------------------------------------------------
---- Procedures pour l'algorithme de l'Accordeon
---- Ecrit par Corinne Plazanet pour la 1er version en 1995 (sauvegarde
---- du code dans la procedure ACCOREDON_ANCIEN)
---- Revise et ameliore par Hugues Mauffrey en avril 1998
---- But : traiter les virages en lacets pour les ecarter comme un accordeon
-----------------------------------------------------------------------

procedure direction_principale (ligne_in : point_liste_type ; 
                                angle     : out float ; 
                                confiance : out float );
       

procedure ACCORDEON (   Ligne           : IN point_liste_type;
                        Largeur         : IN float;
                        Ligne_acc       : OUT point_access_type;
                        Auto            : IN boolean := TRUE;
                        Sigma           : IN float := 20.0 );
                                                               

 procedure ACCORDEON_ANCIEN (	ligne		  : IN point_liste_type;
  				n_points	  : IN integer;
				EPSILON		  : IN OUT float;
				SIGMA		  : IN float;
                        	e                 : IN float;
				ligne_caricature  : IN OUT point_liste_type;
				np		  : IN OUT integer);

-----------------------------------------------------------------------
---- Procedures pour l'algorithme de Lowe
---- Ecrit par Xavier Barillot en 1996.
---- But : traiter les virages mollement sinueux pour lisser et caricaturer
-----------------------------------------------------------------------
Procedure Der_lowe ( Ligne                      : in point_liste_type;
                     n_points                   : in natural;
                     sigma                      : in float;
                     sigma_lowe                 : in float;
                     coe                        : in float;
                     Ligne_lisse2_dessin_lowe   : out point_liste_type);
           

-----------------------------------------------------------------------
---- Procedures pour l'algorithme de Fritsch
---- Ecrit par Emmanuel Fritsch en 1995
---- But : Ecarter chaque virage (procedure en abandon)
-----------------------------------------------------------------------
procedure elargit(arc_tab_in: point_liste_type; 
                  nb_point: natural;
                  arc_tab_out: out point_liste_type; 
                  rayon_min: float);

-----------------------------------------------------------------------
---- Procedures pour l'algorithme de la Baudruche
---- Ecrit par Francois Lecordix en 1996
---- But : Ecarter chaque virage au niveau des points d'inflexion
---- afin de desempater l'extremite du virage comme un ballon baudruche
---- (procedure en abandon et remplace Faille_min et Faille_max
-----------------------------------------------------------------------
Procedure Baudruche_1_virage( PI1 : in point_type;
                              PI2 : in point_type;
                              Indice_sommet : natural;
                              Ligne : in out point_liste_type;
                              n_points : in natural;
                              Gonflement : in float);

Procedure BAUDRUCHE (   ligne             : in point_liste_type;
                        n_points          : in natural;
                        GONFLEMENT        : in float;
                        SIGMA             : in float;
                        ligne_caricature  : in out point_liste_type;
                        np                : in out natural) ;

                                                    
-----------------------------------------------------------------------
---- Procedures pour detecter les empatements
---- Ecrit par Francois Lecordix en 1996
---- But : Detecter les rangs des points d'empatement a gauche et a droite
---- (procedure en cours d'abandon remplace par le package Morpholigne)
-----------------------------------------------------------------------
Procedure Rang_Pts_Empate ( Ligne 	: in point_liste_type;
			    n_points	: in natural;
                            Demi_largeur: in float;
                            Max_surplus : in float;
                            Rang_pts_dr : out liens_array_type;
                            Nb_rangs_dr : out natural;
                            Rang_pts_ga : out liens_array_type;
                            Nb_rangs_ga : out natural)   ;



-----------------------------------------------------------------
-- Algorithme de la Faille_minimum (DECALAGE_SQUELETTE)
-- Sebastien Mustiere
-- Janv 98
----------------------------------------
-- Arcin : l'arc en entree
-- Largeur : la largeur du decalage du squelette (demi largeur du symbole)
-- Courbure : le rayon de courbure minimal assure lors du lissage du squelette
-- Arcout : l'arc en sortie
----------------------------------------

procedure decalage_squelette(Arcin     : in Point_liste_type;
                             Largeur   : in float;
                             Courbure  : in float;
                             Arcout    : out Point_access_type);
                                                                
-----------------------------------------------------------------
-- Algorithme de la Faille_maximum
-- Sebastien Mustiere
-- Janv 98
-----------------------------------------------------------------
-- Arc : l'arc en entree
-- Largeur : la largeur du decalage du squelette (demi largeur du symbole)
-- Arc_traite : l'arc en sortie
----------------------------------------
procedure Faille_max(Arc        : in Point_liste_type;
                     Largeur    : float;
                     Arc_traite : out Point_access_type);




----------------------------------------------------------------------------
-- faille_min adapte a un trou dans le symbole
-----------------------------------------------------------------------------
-- Arcin : l'arc en entree
-- Largeur : la largeur du decalage du squelette (demi largeur du symbole)
-- Courbure : le rayon de courbure minimal assure lors du lissage du squelette
-- Arcout : l'arc en sortie
---------------------------------------------------------------------------

procedure DECALAGE_SQUELETTE_TROU(Arcin1     : in Point_liste_type;
                                  Arcin2    : in Point_liste_type;
                                  Largeur   : in float;
                                  Courbure  : in float;
                                  Arcout1   : out Point_access_type;
                                  Arcout2   : out Point_access_type);
----------------------------------------------------------------------------                     

end caricature;                    

