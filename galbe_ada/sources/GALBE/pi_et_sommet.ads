-- ****************************************************************
-- ** Package de detection des points d'inflexion et des sommets **
-- ****************************************************************
-- auteur SM  09/98
-- a partir des travaux de CP, FL, CD, XB

with Gen_IO; use Gen_IO;
with Geomxav;

Package PI_et_sommet is

 --**************************************************************************
 --                     MODE D'EMPLOI
 --
 -- TYPE_DETECTION_PI
 -- Chgt_d_angle <=> PI si chgt d'angle sur la ligne lissee; les PI sont
 --                  alors localises necessairement au milieu d'un segment
 --                  de la ligne originale
 -- Chgt_d_angle_precis <=> Idem mais PI pas necessairement au milieu
 --                         d'un segment de la ligne originale
 -- Milieu_des_sommets <=> les PI sont localises au milieu (au sens abscisse
 --                        curviligne) de deux sommets
 -- Milieu_des_zones_plates <=> prend en consideration la notion de zone plate
 --                             en plus de celle de virage
 -- Courbure_xavier <=> detection des PI en modelisant la ligne par cubiques
 --
 -- TYPE_DETECTION_SOMMETS
 -- Eloignement_a_la_base <=> sommet = point le plus eloigne de la base
 --                           du virage
 -- Max_courbure_MB <=> sommet = max de courbure (1 sommet entre 2 PI)
 --                     courbure calculee par la methode de Mathieu
 -- Pas_de_sommet <=> ne calcule pas les sommets
 --
 -- INFLEXIONS_SOMMETS
 -- Cette procedure recherche a la fois les PI et les
 -- sommets sur la ligne lissee par Gauss avec sigma en entree donne
 -- Ligne : la ligne en entree (Tableau dimensionne exactement a son nombre
 --         de points)
 --
 -- Points_pred_PI [resp. Points_pred_sommets]: la liste des rangs des
 --      points de la ligne originale precedant les PI [resp. sommets]
 --
 -- Coord_PI [resp. Coord_sommets]: la liste des coordonnees precises 
 --      des PI [resp. sommets]
 -- 
 -- INFLEXIONS_SOMMETS_AUTO
 -- Meme principe, mais au lieu de fixer sigma on fournit un intervalle 
 -- de recherche [Sigma_min, Sigma_max] dans lequel chercher sigma qui
 -- satisfait le critere "dans chacun des virages l'angle (PI,Sommet,PI)
 -- est superieur a Angle_max"
 -- Si aucune solution n'est trouvee, Sigma_calcule est renvoye a 0 (et
 -- aucun des pointeurs n'est initialise). Sinon la valeur trouvee
 -- est renvoyee dans Sigma_calcule
 --
 -- HIERARCHIE_DES_PI  
 -- Recherche les PI pour deux niveaux de lissage Sigma_faible et Sigma_fort
 -- Tniveau_PI : un tableau des niveaux des PI (1 pour les PI importants,
 --              2 pour les PI secondaires)
 -- NB : il n'existe pas de methode de determination automatique des sigmas
 --      pour la recherche de plusieurs niveaux d'inflexion
 --
 -- ATTENTION POUR TOUTES LES PROCEDURES
 -- Preciser sigma = 0 ne signifie pas "determination automatique de sigma"
 -- mais "pas de lissage"
 -- NB: si on met en entree Det_sommets=pas_de_sommets, les parametres
 -- en sortie ##_sommets sont renvoyes a NULL. 
 -- Les pointeurs non renvoyes a NULL sont initialises a l'interieur des
 -- procedures, il ne faut donc pas les initialiser avant mais il faut penser
 -- a les desallouer apres utilisation.
 -- Si une procedure a besoin des sommets pour effectuer des calculs alors
 -- ceux-ci sont faits par la methode Chgt_d_angle_precis si jamais la valeur
 -- Pas_de_sommets est en entree (mais les pointeurs ##_sommets seront
 -- quand meme renvoyes a NULL 
 -- En clair Pas_de_sommet n'est pas conseille pour:
 -- 1/ Toute utilisation de Inflexions_sommet_auto
 -- 2/ Une determination des PI avec Det_PI = Milieu_des_sommets
--**************************************************************************

-- Nb min de chgt d'angle dans le meme sens pour determiner que ca tourne
Seuil_micro_inflex : integer := 2; 

Type type_detection_PI is (Chgt_d_angle,
                           Chgt_d_angle_precis,
                           Milieu_des_sommets,
                           Milieu_des_zones_plates,
                           Courbure_xavier);

Type type_detection_sommets is (Eloignement_a_la_base,
                                Max_courbure_MB,
                                Max_courbure_JGA,
                                Pas_de_sommet);


Procedure Inflexions_sommets(Ligne               : IN Point_liste_type;
                             Flag_auto		 : IN OUT boolean;
			     Sigma_entree	 : IN Float;
                             Points_pred_PI      : OUT Liens_access_type;
                             Coord_PI            : OUT Point_access_type;
                             Points_pred_sommets : OUT Liens_access_type;
                             Coord_sommets       : OUT Point_access_type;
                             Sigma_calcule       : OUT float;
                             Det_PI              : IN Type_detection_PI 
                                                 := Chgt_d_angle_precis;
                             Det_sommets         :IN Type_detection_sommets 
                                                 := Eloignement_a_la_base;
                             Sigma_min           : IN float 
                                                 := 2.0;
                             Sigma_max           : IN float 
                                                 := 100.0;
                             Angle_max           : IN float 
                                                       := 100.0);

Procedure Hierarchie_des_PI(Ligne               : IN  Point_liste_type;
                            Det_PI              : IN  Type_detection_PI
                                                 := Chgt_d_angle_precis;
                            Sigma_faible        : IN  float;
                            Sigma_fort          : IN  float;
                            Points_pred_PI      : OUT Liens_access_type;
                            Coord_PI            : OUT Point_access_type;
			    Tniveau_pi   	: OUT Liens_access_type );
 


------------------------------------------------------------------------------
-- Calcul des pts d'inflexion d'une ligne a partir d'interpolation par cubique
-- Auteur : XB
------------------------------------------------------------------------------
Procedure Pt_infl (	Ligne_ini	: in point_liste_type;
			n_pt_ini	: in natural;
			PTINFL		: out point_liste_reel;
			n_infl		: out natural;
			POS		: out geomxav.int_tableau_type;
			APTI		: out reel_tableau_type);
--------------------------------------------------------------------------------
-- Ligne_ini    = polyligne en entree
-- n_pt_ini     = nombre de points de Ligne_ini
-- PTINFL       = Coordonnees des points d'inflexion
-- n_infl       = nombre de points d'inflexion (dimension de PTINFL)
-- POS          = rang du point de Ligne_ini immediatement inferieur a
--                chaque point d'inflexion (dimension = n_infl)
-- APTI         = pente en chaque point d'inflexion (dimension = n_infl)
--------------------------------------------------------------------------------


end PI_et_Sommet;
