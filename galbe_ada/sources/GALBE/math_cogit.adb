With geometrie; use geometrie;
With math_int_basic; use math_int_basic;
--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95
with lissage_filtrage;

package body MATH_COGIT is
-- ================== --
-- Calculs de MINIMUM --
-- ================== --
------------------------------------------------------------------------------
function MIN (a, b : in integer) return integer is
begin  
	if a < b then
		return a;
	else
		return b;
	end if;
end;
------------------------------------------------------------------------------
function MIN (a, b : in float) return float is begin  
 if a < b then     return a;
   else     return b;
   end if;
 end ;
------------------------------------------------------------------------------
function MIN (tab : in liens_array_type) return integer is
 min: integer:= integer'last;
 begin  
 for i in tab'range loop
 if tab(i)< min then
 min:= tab(i);
 	end if;
     end loop;
 	     return min;
 end ;
  ------------------------------------------------------------------------------
function MIN (tab : in reel_tableau_type) return float is 
min: float:= float'last;
 begin  
 for i in tab'range loop	
 if tab(i)< min then	
 min:= tab(i);
 	end if;
     end loop;
 	     return min;
 end ;
  --++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  -- Renvoie le point min en x et y d'un tableau de points:
  ------------------------------------------------------------------------------
function MIN( tableau	: point_liste_type ) return point_type IS 
 res	: point_type:=tableau(tableau'first);
 Begin 
 for i in tableau'range loop 
 if tableau(i).coor_x<res.coor_x then   
 res.coor_x:=tableau(i).coor_x;
    end if;
	    if tableau(i).coor_y<res.coor_y then  
		res.coor_y:=tableau(i).coor_y;
    end if;
	  end loop;
  return (res.coor_x,res.coor_y);
 end MIN;
-- ================== 
---- Calculs de MAXIMUM 
---- ================== 
--------------------------------------------------------------------------------
function MAX (a, b : in integer) return integer is 
begin  
 if a < b then     return b;
   else    
   return a;
   end if;
 end ;
  ------------------------------------------------------------------------------
function MAX (a, b : in float) return float is 
begin   
if a < b then 
    return b;
   else  
   return a;
   end if;
 end ;
  ------------------------------------------------------------------------------
function MAX (tab: in liens_array_type) return integer is max: integer:= 0;
 begin     for i in tab'range loop	if tab(i) > max then	  max:= tab(i);
 	end if;
     end loop;
 	     return max;
 end ;
------------------------------------------------------------------------------
function MAX (tab: in reel_tableau_type) return float is max: float:= 0.0;
 begin     for i in tab'range loop	if tab(i) > max then	  max:= tab(i);
 	end if;
     end loop;
 	     return max;
 end ;
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--Renvoie le point max en x et y d'un tableau de points:
------------------------------------------------------------------------------
function MAX( tableau	: point_liste_type ) return point_type is    res	:point_type:=tableau(tableau'first);
 begin    for i in tableau'range loop	if tableau(i).coor_x>res.coor_x then		res.coor_x:=tableau(i).coor_x;
	end if;
		if tableau(i).coor_y>res.coor_y then		res.coor_y:=tableau(i).coor_y;
	end if;
	    end loop;
    return (res.coor_x,res.coor_y);
 end MAX;
-- ================== 
---- Calculs de MOYENNE --               
-- ================== 
--------------------------------------------------------------------------------
function MOYENNE (tab: in reel_tableau_type ) return float is 
moyenne: float:= 0.0;
 begin 
 for i in tab'range loop
 moyenne := moyenne + tab(i);
     end loop;
      return (moyenne / float(tab'length));
 end ;
------------------------------------------------------------------------------
function MOYENNE (tab: in reel_tableau_type; n: in integer ) return float is 
moyenne: float:= 0.0;
 begin     
 for i in 1..n loop	
 moyenne := moyenne + tab(i);
     end loop;
      return (moyenne / float(n));
 end ;
------------------------------------------------------------------------------
function MOYENNE (tab: in liens_array_type) return float is 
moyenne: integer := 0;
 begin 
 for i in tab'range loop
 moyenne := moyenne + tab(i);
     end loop;
      return (float(moyenne) / float(tab'length));
 end ;
------------------------------------------------------------------------------
function MEDIANE (tab: in reel_tableau_type ) return float is 
tabtri,tabtmp:reel_tableau_type(1..tab'length)
			:=(others=>float'last);
 -- Indices de TAB --
 indice,ntri,borne:integer;
 begin     
 -- Tri du tableau dans l'ordre croissant -- 
 for i in tab'range loop
 tabtmp(i):= tab(i);
     end loop;
      ntri:= tab'length;
     for j in tab'range loop
	 for i in 1..ntri loop	if tabtmp(i) < tabtri(j) then
	 indice   := i;
	  tabtri(j):= tabtmp(i);
 	end if;
      end loop;
                             borne:= ntri;
      for i in indice+1..borne loop
	  ntri:= ntri-1;
	 tabtmp(i-1):=tabtmp(i);
      end loop;
     end loop;
                      -- mediane = valeur ou 50% des individus au dessus et 50% dessous --
					  -- Calcul de la mediane different selon n pair ou impair --
					  if (tab'length mod 2) = 0 then 
					  -- n PAIR --
					  return tabtri(tab'length/2);
     else 
	 return tabtri( (tab'length-1)/2 + 1);
     end if;
 end ;
------------------------------------------------------------------------------
function MEDIANE (tab: in reel_tableau_type;
 n:in integer ) return float is
 tabtri,tabtmp:reel_tableau_type(1..n)			:=(others=>float'last);
 -- Indices de TAB --
 indice,ntri,borne:integer;
 min: float;
 begin
 -- Tri du tableau dans l'ordre croissant --
 for i in 1..n loop
 tabtri(i):= tab(i);
     end loop;
      for i in 1..n loop
      for j in 1..n-1 loop
	  if tabtri(j+1) < tabtri(j) then 
	  min := tabtri(j);
	  tabtri(j):= tabtri(j+1);
	  tabtri(j+1):= min;
 	end if;
      end loop;
                            end loop;
                                       
-- mediane = valeur ou 50% des individus au dessus et 50% dessous --     
-- Calcul de la mediane different selon n pair ou impair --     
if (n mod 2) = 0 then 
-- n PAIR --
       return tabtri(n/2);
     else
	 return tabtri( (n-1)/2 + 1);
     end if;
 end ;
------------------------------------------------------------------------------
function MEDIANE (tab: in liens_array_type ) return integer is
 tabtri,tabtmp:liens_array_type(1..tab'length)			:=(others=>integer'last);
 -- Indices de TAB -- 
 indice,ntri,borne:integer;
 begin
 -- Tri du tableau dans l'ordre croissant --
 for i in tab'range loop
 tabtmp(i):=tab(i);
     end loop;
      ntri:=tab'length;
     for j in tab'range loop
	 for i in 1..ntri loop
	 if tabtmp(i) < tabtri(j) then
	 indice   := i;
	  tabtri(j):= tabtmp(i);
 	end if;
      end loop;
       borne:= ntri;
      for i in indice+1..borne loop 
	  ntri:= ntri-1;
	 tabtmp(i-1):= tabtmp(i);
      end loop;
     end loop;
     -- mediane = valeur ou 50% des individus au dessus et 50% dessous --
     -- Calcul de la mediane different selon n pair ou impair -- 
	 if (tab'length mod 2) = 0 then
	 -- n PAIR --
	 return tabtri(tab'length/2);
     else
	 return tabtri( (tab'length-1)/2 + 1);
     end if;
 end ;
------------------------------------------------------------------------------
function ECART_TYPE (tab: in reel_tableau_type ) return float is 
moy: float;
 ec: float:=0.0;
 begin     moy := moyenne(tab);
     for i in tab'range loop
	 ec := ec + (tab(i)-moy)**2;
     end loop;
      ec := SQRT(ec / float(tab'length));
     return ec;
 end;
------------------------------------------------------------------------------
function ECART_TYPE (tab: in reel_tableau_type; n: in integer ) return float is
 moy: float;
 ec: float:=0.0;
 begin 
 moy := moyenne(tab,n);
     for i in 1..n loop	
	 ec := ec + (tab(i)-moy)**2;
     end loop;
      ec := SQRT(ec / float(n));
     return ec;
 end;
------------------------------------------------------------------------------
function ECART_TYPE (tab: in liens_array_type ) return float is
 moy: float;
 ec: float:=0.0;
 begin
 moy := moyenne(tab);
     for i in tab'range loop
	 ec := ec + (float(tab(i))-moy)**2;
     end loop;
      ec := SQRT(ec / float(tab'length));
     return ec;
 end;
------------------------------------------------------------------------
function Centrage( Tab : in point_liste_type; c   : in point_type := (0,0) ) return point_liste_type is
P	: point_type:=(0,0);
  Tab_r	:point_liste_type(tab'range);
 Begin  p := Point_moyen(tab);
  for i in tab'range loop
  Tab_r(i) := ( tab(i).coor_x-p.coor_x + c.coor_x, 		  tab(i).coor_y-p.coor_y + c.coor_y);
  end loop;
  return tab_r;
 End Centrage;
-------------------------------------------------------------------------------
procedure DROITE_REGRESSION(	tab	: in point_liste_type;
				a,b	: out float) is begin  DROITE_REGRESSION_GENERALE(tab, Point_moyen(tab), a, b);
 end ;
 -- ---------------------------------------------------------
 --  calcule la pente de la droite des MC pour les points 
 -- de tab passant par le premier point de tab
 procedure DROITE_REGRESSION_GENERALE(	tab	: in point_liste_type;
					p	: in point_type;
					a,b	: out float) is  
g,h		  : float;
  sumx,sumy,sumxy : float := 0.0;
  moyen		  : point_type;
 begin
 for k in tab'range loop
 sumx:=sumx+"**"(float(tab(k).coor_x-p.coor_x),2);
     sumy:=sumy+"**"(float(tab(k).coor_y-p.coor_y),2);
     sumxy:=sumxy+float(tab(k).coor_x-p.coor_x)*			float(tab(k).coor_y-p.coor_y);
  end loop;
  sumxy:=sumxy;
  if (sumx=0.0)  then
  a:=1.0;
    b:=0.0;
  else if sumy=0.0 then
  a:=0.0;
         b:=1.0;
       else         a:=sumxy/NORME(sumx,sumxy,0.0,0.0);
         b:=-sumx/NORME(sumx,sumxy,0.0,0.0);
        end if;
  end if;
 end ;
  -- ====================================================================== --
  -- Methode du compas -- 
  -- Ou encore "Walking Divider" [Shelberg,82] [Muller,87] -- 
  -- ====================================================================== -- 
  procedure Walking_Divider( 	arc_courant 	: in point_access_type;
 				ncourant 	: in integer;
				arc_resultat 	: in out point_access_type;
 				nresultat  	: out integer;
				epsilon 	: in float;
				reste 		: out float) is  
n_aux		        : integer := 0;
  Ligne		   	: point_liste_type(1..2000);
   Ligne_aux		: point_access_reel;
  Ligne_auxi		: point_access_type;
  lgr_segments		: liens_array_type(1..2000);
  DX, DY        	: float;
  n_points 		: natural := 0;
  pt1,pt2 		: integer;
  nres			: integer;
  distance		: float;
 begin  
 -- ================================= --  
 -- Decoupage de la ligne en pas de 1 --
 -- ================================= --
 ligne_aux := new Point_liste_reel(1..2000);
  ligne_auxi:= new Point_liste_type(1..2000);
  Decompose_ligne( arc_courant(1..ncourant), ncourant, ligne_aux, n_aux,		   1.0, lgr_segments);
  for j in 1..n_aux loop
  ligne_auxi(j).coor_x := integer(ligne_aux(j).coor_x);
        ligne_auxi(j).coor_y := integer(ligne_aux(j).coor_y);
  end loop;
  -- ================================== --  
  -- Decoupage par la methode du compas --  
  -- ================================== --
  nres:= 1;
  arc_resultat(1) := ligne_auxi(1);
  pt1:= 1;
  for i in 2..n_aux loop
  pt2:= i;
     distance:= Norme ( ligne_auxi(pt1), ligne_auxi(pt2));
     if distance > epsilon then
	 nres:= nres + 1;
	arc_resultat(nres) := ligne_auxi(i);
	pt1:= i;
     end if;
  end loop;
  if pt1 /= n_aux then
  reste:= Norme ( ligne_auxi(pt1), ligne_auxi(n_aux) );
  else    reste:= 0.0;
  end if;
  nresultat:= nres;
 end Walking_Divider ;
 ---------------------------------------------------- 
 -- =========================================================================== 
 Function Dim_fractale( arc_courant      : Point_Access_type;
			ncourant         : natural;
			pas              : integer;
			methode_decoupage: methode_type  ) return float is
	arc_resultat 		: Point_Access_type;
  xrep, yrep   		: integer;
  DX, DY        	: float;
  Nresultat, n_points 	: natural := 0;
  a, b, D 		: float;
  sumx, sumy, sumxy 	: float := 0.0;
  moyen 		: point_type_reel;
  epsilon		: float;
  n_aux		        : integer := 0;
  Ligne		   	: point_liste_type(1..2000);
   Ligne_aux		: point_access_reel;
  Ligne_auxi		: point_access_type;
  lgr_segments		: liens_array_type(1..2000);
  reste 		: float := 0.0;
  f			: text_io.file_type;
  -- N(EPSILON) en fonction de EPSILON dans un espace LOG-LOG --
  Log_N  : point_liste_reel(1..2000) := (others=>(0.0,0.0));
  ilog_N  : point_liste_type(1..2000) := (others=>(0,0));
   package flo_io is new float_io(float); use flo_io;
 begin 
 -- Dimension_fractale --  
 Arc_resultat := new Point_liste_type(1..5000);
           create(f,out_file,"dim_fractale.splus");
  for p in 1..pas loop  
  -- Sur EPSILON --   
  epsilon:= float(p);
   case methode_decoupage is
   when VANHORN =>       Lissage_filtrage.VAN_HORN(
   arc_courant,
   ncourant,
   -- Gr_arc(graphe,numero).encombr.minimum.coor_x, 
   -- X origine du repere
   -- Gr_arc(graphe,numero).encombr.minimum.coor_y, 
   -- Y origine du repere
   arc_courant(1).coor_X, 
   arc_courant(1).coor_y,
   integer(epsilon),
   arc_resultat, 
   nresultat );
    when COMPAS =>     Walking_Divider( 	arc_courant,  ncourant,			arc_resultat, nresultat, epsilon, reste);
     when others => NULL;
   end CASE;
   Log_N(p).coor_x:= log( epsilon );
   Log_N(p).coor_y:= log( float(Nresultat-1)   );
   ilog_N(p).coor_x:= integer( log_n(p).coor_x * 10000000.0);
   ilog_N(p).coor_y:= integer( log_n(p).coor_y * 10000000.0);
 flo_io.put(f, epsilon, 1,5,0);
 put(f,"  ");
  flo_io.put(f, float(nresultat-1)*epsilon, 1,5,0);
 new_line(f,1);
  end loop;
 -- Sur EPSILON --
 close(f);
  -- Calcul de la pente de la droite de regression :  
  moyen:= POINT_MOYEN( Log_N(1..pas));
  for k in 1..pas loop
  sumx:= sumx + (Log_N(k).coor_x - moyen.coor_x)**2;
     sumy:= sumy + (Log_N(k).coor_y - moyen.coor_y)**2;
     sumxy:=sumxy+ (Log_N(k).coor_x - moyen.coor_x)		 *(Log_N(k).coor_y - moyen.coor_y);
  end loop;
  if (sumx=0.0)  then
  a:=1.0;
    b:=0.0;
  else if sumy=0.0 then
  a:=0.0;
         b:=1.0;
       else         a:=sumxy/NORME(sumx,sumxy,0.0,0.0);
         b:=-sumx/NORME(sumx,sumxy,0.0,0.0);
        end if;
  end if;
-- taille_f(ilog_N(1..pas),1.0);
-- dessin_semi(ilog_N(1..pas),1);
-- m_a_j;
-- attente;
-- attente;
  return (1.0 - a);
 end Dim_fractale;
 ---------------------------------------------------------- 
 -- ========================================================================= 
 -- =========================================================================== 
 Function Dim_fractale( ligne_c      	 : Point_liste_type;
			ncourant         : natural;
			pas              : integer;
			methode_decoupage: methode_type  ) return float is
arc_courant 		: Point_access_type;
  arc_resultat 		: Point_access_type;
  xrep, yrep   		: integer;
  DX, DY        	: float;
  Nresultat, n_points 	: natural := 0;
  a, b, D 		: float;
  sumx, sumy, sumxy 	: float := 0.0;
  moyen 		: point_type_reel;
  epsilon		: float;
  n_aux		        : integer := 0;
  Ligne		   	: point_liste_type(1..2000);
   Ligne_aux		: point_access_reel;
  Ligne_auxi		: point_access_type;
  lgr_segments		: liens_array_type(1..2000);
  reste 		: float := 0.0;
  f			: text_io.file_type;
  -- N(EPSILON) en fonction de EPSILON dans un espace LOG-LOG --
  Log_N  : point_liste_reel(1..2000) := (others=>(0.0,0.0));
  ilog_N  : point_liste_type(1..2000) := (others=>(0,0));
  begin 
  -- Dimension_fractale --
  Arc_courant := new Point_liste_type(1..ncourant);
  for i in 1..ncourant loop
  arc_courant(i).coor_x:= ligne_c(i).coor_x;
        	arc_courant(i).coor_y:= ligne_c(i).coor_y;
         end loop;
  Arc_resultat := new Point_liste_type(1..5000);
            for p in 1..pas loop
			-- Sur EPSILON --
			epsilon:= float(p);
   case methode_decoupage is	    
   when VANHORN =>                                       
   Lissage_filtrage.VAN_HORN(    arc_courant, ncourant,
   -- Gr_arc(graphe,numero).encombr.minimum.coor_x, 
   -- X origine du repere
   -- Gr_arc(graphe,numero).encombr.minimum.coor_y, 
   -- Y origine du repere     	
   arc_courant(1).coor_X, 
   arc_courant(1).coor_y,
   integer(epsilon),
   arc_resultat, 
   nresultat );
    when COMPAS => 
    Walking_Divider( 	arc_courant,  ncourant,	
						arc_resultat, nresultat, epsilon, reste);
     when others => NULL;
   end CASE;
   Log_N(p).coor_x:= log( epsilon );
   Log_N(p).coor_y:= log( float(Nresultat-1)   );
   ilog_N(p).coor_x:= integer( log_n(p).coor_x * 10000000.0);
   ilog_N(p).coor_y:= integer( log_n(p).coor_y * 10000000.0);
  end loop;
 -- Sur EPSILON --
 -- Calcul de la pente de la droite de regression :  
 moyen:= POINT_MOYEN( Log_N(1..pas));
  for k in 1..pas loop 
  sumx:= sumx + (Log_N(k).coor_x - moyen.coor_x)**2;
     sumy:= sumy + (Log_N(k).coor_y - moyen.coor_y)**2;
     sumxy:=sumxy+ (Log_N(k).coor_x - moyen.coor_x)		 *(Log_N(k).coor_y - moyen.coor_y);
  end loop;
  if (sumx=0.0)  then
  a:=1.0;
    b:=0.0;
  else if sumy=0.0 then
  a:=0.0;
         b:=1.0;
       else         a:=sumxy/NORME(sumx,sumxy,0.0,0.0);
         b:=-sumx/NORME(sumx,sumxy,0.0,0.0);
        end if;
  end if;
  return (1.0 - a);
 end Dim_fractale;
 ---------------------------------------------------------- 
 -- ========================================================================= 
 -- ====================================================================== -- 
 -- Procedure de classification d'un vecteur de mesures sur des arcs 
 --      = simple partage en 2 a partir d'une valeur a fournir 
 -- ====================================================================== --
 Procedure Class_vect( vecteur    	: in reel_tableau_type;
                       narcs      	: in integer;
		       seuil		: in float;
                       cluster         	: in out liens_array_type;
                       nb_arcs_cluster 	: in out liens_array_type) is begin
nb_arcs_cluster(1):=0;
  nb_arcs_cluster(2):=0;
  for a in 1..narcs loop
  if vecteur(a) < seuil then
  cluster(a):= 1;
  	nb_arcs_cluster(1):= nb_arcs_cluster(1)+1;
     else
	 cluster(a):= 2;
  	nb_arcs_cluster(2):= nb_arcs_cluster(2)+1;
     end if;
  end loop;
	 end;
--((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((-- 
--)(  L'algorithme du K-MEANS de A.Hartigan et M.A.Wong permet de diviser )(-- 
--)( narcs arcs  dans un espace a nmesures dimensions en k clusters en re )(-- 
--)( cherchant la somme minimale des carres des distances entre points d' )(-- 
--)( un meme cluster.                                                     )(-- 
 --)(		En entree : 	nombre d'arcs a classer				          )(-- 
 --)(				nombre de mesures pour chaque arc					  )(-- 
 --)(				matrice des mesures			   						  )(-- 
 --)( 				nombre de clusters souhaites en sortie    			  )(--
 --)(		En sortie :	appartenance de chaque arc						  )(-- 
 --)(				nombre d'arcs par cluster		 					  )(-- 
--))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))--
Procedure KMEANS ( matrice_mesures     : in matrice_type;
                    narcs               : in integer;
                    nmesures            : in integer;
                    k                   : in integer;
   		    matrice_centres     : in out matrice_type;
                    cluster             : in out liens_array_type;
                    nb_arcs_cluster     : in out liens_array_type;
                    ifault              :    out natural          ) is
cluster_tmp          : liens_array_type(1..narcs);
   -- IC2 : cluster de transfert possible de chaque arc   
   ncp                  : liens_array_type(1..k);
       -- PAS pour lequel le cluster est mis a jour en dernier   
	itran                : liens_array_type(1..k);
   live                 : liens_array_type(1..k);
   dist_arc_clus        : reel_tableau_type(1..narcs);
  -- DT   
   an1                   : reel_tableau_type(1..k);
   an2                  : reel_tableau_type(1..k);
   wss                  : reel_tableau_type(1..k);
   iter                 : integer       := 10;
    	-- Nb max d'iterations   
	l                    : integer;
   index                : integer       := 0;
   arc                  : integer;
   big                  : float         := 10.0**10;
   tmp                  : float;
   db                   : float;
   ii                   : float;
   flag                 : boolean;
   -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   
   -- OPTIMAL TRANSFERT ------------------------------------------------
   -- ncp(l) indique le pas pour lequel  le cluster l est maj en dernier   
   -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   
   procedure OPTRA (    matrice_mesures : in matrice_type;
                        narcs           : in integer;
                        nmesures        : in integer;
                        matrice_centres : in out matrice_type;
                        k               : in integer;
                        cluster         : in out liens_array_type;
                        cluster_tmp     : in out liens_array_type;
                        nb_arcs_cluster : in out liens_array_type;
                        an1             : in out reel_tableau_type;
                        an2             : in out reel_tableau_type;
                        ncp             : in out liens_array_type;
                        dist_arc_clus   : in out reel_tableau_type;
                        itran           : in out liens_array_type;
                        live            : in out liens_array_type;
                        index           : in out integer)               is
de, da, dc, r2, rr, alw, alt, al1, al2 : float;
 l1, l2, ll                             : natural;
    begin
	for l in 1..k loop
	if itran(l) = 0 then
	live(l) := narcs + 1;
            end if;
        end loop;
        index := 0;
        for i in 1..narcs loop
		index := index + 1;
            l1 := cluster(i);
            l2 := cluster_tmp(i);
            ll := l2;
            -- SI LE CLUSTER EST COMPOSE D'UN SEUL ARC i, PAS DE TRANSFERT            
			if nb_arcs_cluster(l1) /= 1 then
			if ncp(l1) /= 0 then
			de := 0.0;
                  for j in 1..nmesures loop
				  de := de+(matrice_mesures(i,j)-matrice_centres(l1,j))**2;
                  end loop;
                  dist_arc_clus(i) := de * an1(l1);
               end if;
               da := 0.0;
               for j in 1..nmesures loop
			   da:=da+(matrice_mesures(i,j)-matrice_centres(l2,j))**2;
               end loop;
               r2 := da * an2(l2);
               for l in 1..k loop
			   if (i>=live(l1) and i>=live(l)) or l=l1 or l=ll then
			   rr := r2 / an2(l);
                        dc := 0.0;
                        for j in 1..nmesures loop
						dc:=dc+( matrice_mesures(i,j)  - matrice_centres(l,j))**2;
                               if dc >= rr then
							   return;
                               end if;
                        end loop;
                        if dc < rr then
						r2 := dc * an2(l);
                              l2 := l;
                        end if;
                   end if;
               end loop;
               if r2 >= dist_arc_clus(i) then
			   cluster_tmp(i) := l2;
                  else  index := 0;
                        live(l1) := narcs + i;
                        live(l2) := narcs + i;
                        ncp(l1) := i;
                        ncp(l2) := i;
                        al1 := float(nb_arcs_cluster(l1));
                        alw := al1 - 1.0;
                        al2 := float(nb_arcs_cluster(l2));
                        alt := al2 + 1.0;
                        for j in 1..nmesures loop
						matrice_centres(l1,j):=(matrice_centres(l1,j)*al1   - matrice_mesures(i,j))/alw;
                            matrice_centres(l1,j):=(matrice_centres(l2,j)*al2    + matrice_mesures(i,j))/alt;
                        end loop;
                        nb_arcs_cluster(l1) := nb_arcs_cluster(l1) - 1;
                        nb_arcs_cluster(l2) := nb_arcs_cluster(l2) + 1;
                        an2(l1) := alw / al1;
                        an1(l1) := big;
                        if alw > 1.0 then 
						an1(l1) := alw / (alw - 1.0);
                        end if;
                        an1(l2) := alt / al2;
                        an2(l2) := alt / (alt + 1.0);
                        cluster(i) := l2;
                        cluster_tmp(i) := l1;
               end if;
            end if;
        if index = narcs then 
		return;
        end if;
        end loop;
        for l in 1..k loop 
		itran(l):= 0;
            live(l) := live(l) - narcs;
        end loop;
    end OPTRA;
 -----------------------------------------------------------------   
 -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   
 -- QUICK TRANSFERT ----------------------------------------------------------   
 -- ncp(l) indique le pas pour lequel le cluster l est maj en dernier + narcs   
 -- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^   
 procedure QTRAN (    matrice_mesures : in matrice_type;
                        narcs           : in integer;
                        nmesures        : in integer;
                        matrice_centres : in out matrice_type;
                        k               : in integer;
                        cluster         : in out liens_array_type;
                        cluster_tmp     : in out liens_array_type;
                        nb_arcs_cluster : in out liens_array_type;
                        an1             : in out reel_tableau_type;
                        an2             : in out reel_tableau_type;
                        ncp             : in out liens_array_type;
                        dist_arc_clus   : in out reel_tableau_type;
                        itran           : in out liens_array_type;
                        live            : in out liens_array_type;
                        index           : in out integer        ) is 
						da, dd, r2 ,alw, alt, al1, al2         : float;
 icoun, istep, l1, l2                   : natural;
    begin        icoun := 0;
        istep := 0;
        for i in 1..narcs loop
		icoun := icoun + 1;
            istep := istep + 1;
            l1 := cluster(i);
            l2 := cluster_tmp(i);
            -- SI LE CLUSTER EST COMPOSE D'UN SEUL ARC i, PAS DE TRANSFERT
            if nb_arcs_cluster(l1) /= 1 then
			if istep <= ncp(l1) then
			da := 0.0;
                  for j in 1..nmesures loop
				  da := da + (matrice_mesures(i,j)-matrice_centres(l1,j))**2;
                  end loop;
                  dist_arc_clus(i) := da * an1(l1);
               end if;
               if (istep < ncp(l1) or else istep < ncp(l2)) then
			   r2 := dist_arc_clus(i) / an2(l2);
                   dd := 0.0;
                   for j in 1..nmesures loop
                   dd := dd + (matrice_mesures(i,j) - matrice_centres(l2,j))**2;
                   end loop;
                   if dd /= r2 then 
				   icoun := 0;
                      index := 0;
                      itran(l1) := 1;
                      itran(l2) := 1;
                      ncp(l1) := istep + narcs;
                      ncp(l2) := istep + narcs;
                      al1 := float(nb_arcs_cluster(l1));
                      alw := al1 - 1.0;
                      al2 := float(nb_arcs_cluster(l2));
                      alt := al2 + 1.0;
                      for j in 1..nmesures loop
					  matrice_centres(l1,j):=(matrice_centres(l1,j)*al1-matrice_mesures(i,j))/alw;
                          matrice_centres(l2,j):=(matrice_centres(l2,j)*al2-matrice_mesures(i,j))/alt;
                      end loop;
                      nb_arcs_cluster(l1) := nb_arcs_cluster(l1) - 1;
                      nb_arcs_cluster(l2) := nb_arcs_cluster(l2) + 1;
                      an2(l1) := alw / al1;
                      an1(l1) := big;
                      if alw > 1.0 then
					  an1(l1) := alw / (alw - 1.0);
                      end if;
                      an1(l2) := alt / al2;
                      an2(l2) := alt / (alt + 1.0);
                      cluster(i) := l2;
                      cluster_tmp(i) := l1;
                   end if;
               end if;
            end if;
            if icoun = narcs then
			return;
            end if;
        end loop;
    end QTRAN;
------------------------------------------------------------------ 
begin    
-- KMEANS
        if (k < 3 or else k >= narcs) then
		ifault := 3;
           return;
        end if;
        for i in 1..narcs loop
		cluster(i) := 1;
            cluster_tmp(i) := 2;
            for il in 1..2 loop
			dist_arc_clus(il) := 0.0;
                for j in 1..nmesures loop 
				dist_arc_clus(il) := ( dist_arc_clus(il) + (matrice_mesures(i,j)-matrice_centres(il,j))**2 );
                end loop;
            end loop;
            if dist_arc_clus(1) > dist_arc_clus(2) then
			cluster(i) := 2;
               cluster_tmp(i) := 1;
               tmp := dist_arc_clus(1);
               dist_arc_clus(1) := dist_arc_clus(2);
               dist_arc_clus(2) := tmp;
            end if;
            for l in 3..k loop
			flag := false;
                db := 0.0;
                for j in 1..nmesures loop
				db :=  db + (matrice_mesures(i,j)-matrice_centres(l,j))**2 ;
                    if db >= dist_arc_clus(2) then
					flag := true;
                       exit;
                    end if;
                end loop;
                if flag = false then
				if db < dist_arc_clus(1) then
				dist_arc_clus(2) := dist_arc_clus(1);
                      cluster_tmp(i) := cluster(i);
                      dist_arc_clus(1) := db;
                      cluster(i) := l;
                   end if;
                end if;
            end loop;
        end loop;
        -- MISE A JOUR DES CENTRES DES CLUSTERS A LA MOYENNE DES POINTS        
		-- DE CHACUN --------------------------------------------------
        for l in 1..k loop
		nb_arcs_cluster(l) := 0;
            for j in 1..nmesures loop
			matrice_centres(l,j) := 0.0;
            end loop;
        end loop;
        for i in 1..narcs loop
		l := cluster(i);
            nb_arcs_cluster(l) := nb_arcs_cluster(l) +  1;
            for j in 1..nmesures loop
			matrice_centres(l,j):= matrice_centres(l,j) + matrice_mesures(i,j);
            end loop;
        end loop;
        -- VERIFICATION QU'IL N'Y AIT PAS DE CLUSTER VIDE
        ifault := 1;
        for l in 1..k loop
		if nb_arcs_cluster(l) = 0 then
		return;
            end if;
        end loop;
        ifault := 0;
        for l in 1..k loop
		for j in 1..nmesures loop
		matrice_centres(l,j) := matrice_centres(l,j)    / float(nb_arcs_cluster(l));
            end loop;
            -- INITIALISATIONS ...
            an2(l) := float(nb_arcs_cluster(l)) / float(nb_arcs_cluster(l)+1);
            an1(l) := big;
            if nb_arcs_cluster(l) > 1 then
			an1(l):= float(nb_arcs_cluster(l)) / float(nb_arcs_cluster(l)-1);
            end if;
            itran(l) := 1;
  -- 1 si le cluster l est maj dans un Quick Transfert                            
  -- 0 sinon...            
  ncp(l) := -1;
 -- Indique le pas du dernier transfert 
 end loop;
        -- =============================================================        
		-- CHAQUE ARC EST REALLOUE SI NECESSAIRE AU CLUSTER QUI INDUIRA        
		-- LA PLUS GRANDE REDUCTION DE LA SOMME DES CARRES DES DISTANCES        
		-- DANS LES CLUSTERS ---------------------------------------------        
		put("Quel est le nombre maximum d'iterations autorisees ? ");
--        int_io.get(iter);
--        new_line(1);
        for ij in 1..iter loop
		OPTRA(matrice_mesures, narcs, nmesures, matrice_centres, k, cluster, 
		cluster_tmp, nb_arcs_cluster, an1, an2, ncp, dist_arc_clus,
		itran, live, index );
            if index /= narcs then
			QTRAN(matrice_mesures, narcs, nmesures, matrice_centres, k, 
			cluster, cluster_tmp, nb_arcs_cluster, an1, an2, ncp,
			dist_arc_clus, itran, live, index );
               for l in 1..k loop
			   ncp(l) := 0;
               end loop;
            end if;
        end loop;
        if index /= narcs then
		ifault := 2;
        end if;
        -- CALCUL DE LA SOMME DES CARRES POUR CHAQUE CLUSTER
        for l in 1..k loop            wss(l) := 0.0;
            for j in 1..nmesures loop
			matrice_centres(l,j) := 0.0;
            end loop;
        end loop;
        for i in 1..narcs loop
		for j in 1..nmesures loop
		matrice_centres(cluster(i),j) := matrice_centres(cluster(i),j)
		+ matrice_mesures(i,j);
            end loop;
        end loop;
        for j in 1..nmesures loop
		for l in 1..k loop
		matrice_centres(l,j) := matrice_centres(l,j)
		/ float(nb_arcs_cluster(l));
            end loop;
            for i in 1.. narcs loop
			wss(cluster(i)) := wss(cluster(i))
			+ (matrice_mesures(i,j)-matrice_centres(cluster(i),j))**2;
            end loop;
        end loop;
 end KMEANS;
 -------------------------------------------------------------------
 end ;
