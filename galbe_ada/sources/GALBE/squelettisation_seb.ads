-------------------------------------------------------------------
-- SQUELETTISATION
-------------------------------------------------------------------
-- Package permettant la construction d'un Squelette de Surface
-------------------------------------------------------------------
-- d'apres Bruno Hergott
-- Octobre 97
-------------------------------------------------------------------
-- Revu et corrige par Sebastien Mustiere
-- Jan 98
-------------------------------------------------------------------
--
-------------------------------------------------------------------

-- Librairies COGIT
with Gen_io;use Gen_io;
with PArbre; use PArbre;

package Squelettisation_Seb is

-----------------------------------------------------------------
-- SQUELETTISE                        
-----------------------------------------------------------------
-- Procedure globale de construction du squelette d'un contour
-----------------------------------------------------------------
-- en entree:
--  * les points du polygone (Surface sans trou)
-- en option:
--  * le seuil du lissage de Wall_Danielsson (Wall Danielson a ete choisi
--    car c'est un bon filtrage de surface)
--  * le pas de decomposition (Plus la polyligne est decomposee et plus le
--    squelette est precis)
--  * la methode de construction (Mediatrices, Milieux)
-- en sortie
--  * l'arbre representant le squelette
-----------------------------------------------------------------
function Squelettise(Surface           : Point_access_type;   
		     Pas_decomposition : float :=0.0;
                     Seuil_filtrage    : float := 0.0) 
                     return arbre;

end Squelettisation_Seb;
