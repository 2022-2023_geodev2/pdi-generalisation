-- ***********************************************************************
 ---- Package de calcul de la courbure pour detection des maximas de courbure
 -- Voir la procedure SOMMETS dans le package MESURE_LIGNE
 -- Methode de Mathieu pour le calcul de courbure
 -- *********************************************************************** --
 With Gen_io; Use Gen_io;
 
 Package Courbure is
 -----------------------------------------------------------------------------
 Function Theta(	tab	:point_liste_reel;
 		i 	: integer) return float;
-----------------------------------------------------------------------------
Procedure Tangente_Simple(T	: point_liste_reel;
			  Tg	: in out reel_tableau_type);
-----------------------------------------------------------------------------
Function Symetrie_aux_extremites(t	: reel_tableau_type;
				Ajout	: integer) return reel_tableau_type ;
------------------------------------------------------------------------------
Function convolution_par_gaussienne( 	T	: reel_tableau_type;
					i	: integer;
					sig	: float;
					k	: integer;
				dist_min	: float:=1.0				    ) return float;
------------------------------------------------------------------------------
Procedure Courbure_Mathieu (	tab_in	: point_liste_reel;
				sig	: float;
				trunc	: float;
				Courb	: out reel_tableau_type);
End Courbure;
------------------------------------------------------------------------------