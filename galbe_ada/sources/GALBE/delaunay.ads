-------------------------------------------------------------------
-- Tous les packages de la Triangulation de Delaunay sur des points
-- ecrits par Hergott + Barrault + Duchene
-------------------------------------------------------------------
With Gen_io; 		Use Gen_io;
With Unchecked_deallocation;
Package delaunay is
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
------------------------------------------------------------------------------------------------------------------------------------------------------ Ancien package Ptriangle----------------------------------------------------------------------------------------------------------------------------------------------------
MAXDIAM   : constant float := 10.0**4;
--------------------------------------------------------------------------
Type Triangle is Record 
-- Trois pointeurs pointant dans le semis de points
  p1	: point_type;
  p2	: point_type;
  p3	: point_type;
end Record;
--Definition de la liste dynamique de triangles.
type Liste_triangle;
type a_liste_triangle is access Liste_triangle;
--------------------------------------------------------------------------
Type Type_categorie_triangle is (Normal,Plat,Quasi_plat);
----------------------------------------------------------------------------
Function Compte( Tr	: in a_Liste_triangle) return integer;
--------------------------------------------------------------------------
Function Norme(	p1 : point_type_reel;
		p2 : point_type) return float;
--------------------------------------------------------------------------
Function Norme(	p1 : point_type;
		p2 : point_type_reel) return float;
--------------------------------------------------------------------------
Procedure Permute(P1 : in out Point_type;
 P2: in out Point_type);
--------------------------------------------------------------------------
Procedure Permute(F1: in out float;
 F2: in out float);
--------------------------------------------------------------------------
Type Liste_triangle is record  Tr		: triangle;
  Cat     	: Type_categorie_triangle;
  Centre  	: Point_type_reel:=(0.0, 0.0);
  Rayon2   	: float:=0.0;
  A,  C     	: Point_type:=(0,0);
  sinBABC,  cosBABC 	: float:=0.0;
  suiv		: a_liste_triangle;
End record;
Procedure RECUPERE_T is new UNCHECKED_DEALLOCATION(Liste_triangle,a_liste_triangle);
--------------------------------------------------------------------------
Procedure Enlever_triangle_suivant(T	: in out a_liste_triangle);
--------------------------------------------------------------------------
Procedure Colle_2listes(L1	: in out a_liste_triangle;
			L2	: a_liste_triangle		       );
----------------------------------------------------------------------------
Procedure ajoute_triangles(TS	: a_liste_triangle;
			   TD	: in out A_liste_triangle);
--------------------------------------------------------------------------
Procedure Vide(l	: in out a_liste_triangle) ;
-------------------------------------------------------------------------
Procedure Accessoires_triangle(	Tr 	: triangle;
                               	Cat    	: in out Type_categorie_triangle;
			       	Centre 	: in out Point_type_reel;
			       	Rayon2 	: in out float;
			       	A,				C    	: in out Point_type;
			       	sinBABC,				cosBABC	: in out float);
--------------------------------------------------------------------------
Procedure Ajout_triangle( Tr	: in out a_Liste_triangle;
			  T	: triangle);
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- Ancien package Parete --------------------------------------------------------------------------
Type Arete is record  p1	: point_type;
  p2	: point_type;
end Record;
--definition de la liste dynamique d'aretes.
Type Liste_aretes;
Type a_liste_aretes is access Liste_aretes;
Type Liste_aretes is record  a		: arete;
  suiv		: a_Liste_aretes;
end record;
Procedure RECUPERE_A is new UNCHECKED_DEALLOCATION(Liste_aretes,a_liste_aretes);
--------------------------------------------------------------------------
Procedure ajout_arete(L	: in out a_liste_aretes;
		      a	: arete);
--------------------------------------------------------------------------
Function Egale(	a1	: arete;
	        a2	: arete) return boolean;
--------------------------------------------------------------------------
Function  Recherche_arete(l	: a_Liste_aretes;
			  a	: arete			 ) return boolean;
--------------------------------------------------------------------------
Procedure Vide(l	: in out a_liste_aretes);
--------------------------------------------------------------------------
Procedure Vire_les_Doublons(l_a	: in out A_liste_aretes);
--------------------------------------------------------------------------
Procedure Enleve_arete( l_a	: in out a_liste_aretes;
		        art	: arete;
			ENLEVE	: in out Boolean);
--------------------------------------------------------------------------
Procedure Elager_aretes(l_a	: in out A_liste_aretes);
--------------------------------------------------------------------------
Procedure Enleve_arete(l_a	: in out a_liste_aretes;
		       art	: arete);
--------------------------------------------------------------------------
Procedure Supprime_aretes_vides(L	: in out a_liste_aretes);
--------------------------------------------------------------------------
Function Compte_Liste(	L	: a_liste_aretes) return integer;
--------------------------------------------------------------------------
Procedure range_aretes(l	: a_liste_aretes;
			tab	: in out point_liste_type;
			crs	: integer);
--------------------------------------------------------------------------
Procedure Lire_a(ar	: a_liste_aretes) ;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Point---------------------------------------------------
-----------------------
Type liste_point;
Type a_liste_point is access liste_point;
Type liste_point is record	P	: point_type;
	suiv	: a_liste_point;
End record;
Procedure RECUPERE is new UNCHECKED_DEALLOCATION(Liste_point,a_liste_point);
--------------------------------------------------------------------------
Procedure Vide(l	: in out a_liste_point);
--------------------------------------------------------------------------
Procedure Enlever_Point_suivant(P		: in out a_liste_point);
-------------------------------------------------------------------------- 
Procedure Ajout_Point( 	L	: in out a_Liste_Point;
			P	: Point_type);
--------------------------------------------------------------------------    
Function dernier_point(	L 	: a_liste_point) return a_liste_point;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
---------------------------------------------------------------------------
----------------------------------------------------------------------------
---------------------------------------------------------------------------
----------------------------------------------------------------------
-- Ancien package Convexite------------------------------------------------
----------------------------------------------------------------------------
------------------------
Function Point_interieur_arete(M  : Point_type;
                               AB : arete) return boolean;
--------------------------------------------------------------------------
Function Offset(	a	: arete;
			p	: point_type) return Float;
--------------------------------------------------------------------------
Procedure Convex(ar		: in out a_liste_aretes;
		 Tab_points	: point_liste_type);
--------------------------------------------------------------------------
Procedure quatre_points_convexes(Tab	: point_liste_type;
				 ar	: in out a_liste_aretes);
--------------------------------------------------------------------------
Procedure Lance_convex(l	: in out a_liste_aretes;
		       T	: point_liste_type);
--------------------------------------------------------------------------
Procedure Enveloppe_convexe( TP		: point_liste_type;
			     L_a	: in out a_liste_aretes);
--------------------------------------------------------------------------
Procedure Ensemble_Convexe(Semi		: in out point_liste_type;
			   Nb_pts	: out integer);
--------------------------------------------------------------------------
Function Ensemble_convexe(t	: point_liste_type) return boolean;
--------------------------------------------------------------------------
Function CONVEXE(p1,p2,p3,c	: point_type) return boolean;
--------------------------------------------------------------------------
Procedure Convexe_Concave( L		: in out a_liste_point;
			   centre	: point_type;
			   T		: a_liste_triangle;
			   l_a		: in out a_liste_point;
			   repere	: in out point_type;
			   Modif	: in out boolean);
 --------------------------------------------------------------------------
Procedure EC(	L	: in out a_liste_point;
		Centre	: point_type;
		T	: a_liste_triangle;
		L_a	: in out a_liste_point;
		repere	: in out point_type;
          		Modif	: in out boolean);
--------------------------------------------------------------------------
Procedure EC_TOTALE(    L	: in out a_liste_point;
			Centre	: point_type;
			T	: a_liste_triangle;
			L_a	: in out a_liste_point);
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Topologie-----------------------------------------------
--------------------------------------------------------------------------
--------------------------

Function point_appartient_triangle(	T	: triangle;
					p	: Point_type) return Boolean;
--------------------------------------------------------------------------
Function Arete_appartient_triangle(	a	: arete;
					T	: triangle) return boolean;
--------------------------------------------------------------------------
Function Adjacent(T1	: Triangle;
		  T2	: triangle) return arete;
--------------------------------------------------------------------------
Function Cote_oppose(t	: triangle;
		     p	: point_type) return arete;
--------------------------------------------------------------------------
Function Sommet_oppose(	a	: arete;
			T	: triangle) return point_type;
----------------------------------------------------------
Procedure triangles_opposes(	l_tri	: a_liste_triangle;
				l_t_p	: a_liste_triangle;
				p	: point_type;
			    	l_out	: in out a_liste_triangle);
--------------------------------------------------------------------------
Procedure Extrait_triangles_contenant_point(	p	: point_type;
				 	    	T	: a_liste_triangle;
				 	 	t_out	: in out 							a_liste_triangle);
--------------------------------------------------------------------------
Procedure Extrait_triangles_contenant_arete(	a	: arete;
				 	    	T	: a_liste_triangle;
				 	 	t_out	: in out 							a_liste_triangle);
--------------------------------------------------------------------------
Function Triangle_oppose(T	: triangle;
			 P	: point_type;
			 L_T	: a_liste_triangle) return triangle;
--------------------------------------------------------------------------
Procedure triangles_voisins_de_point(T		: a_liste_triangle;
				     p		: point_type;
				     t_out	: in out a_liste_triangle);
--------------------------------------------------------------------------
Procedure colle_triangle(t1	: in out a_liste_triangle;
			 t2	: in out a_liste_triangle);
--------------------------------------------------------------------------
Procedure Range_triangles(l_t	: in out a_liste_triangle);
--------------------------------------------------------------------------
Function triangle_complementaire( t1	: triangle;
				  t2	: triangle;
				  p	: point_type) return triangle;
--------------------------------------------------------------------------
Procedure Recherche_triangles_complementaires( lt	: a_liste_triangle;
						p	: point_type;
				     		l_o	: in out a_liste_triangle					     );
--------------------------------------------------------------------------
Procedure POINTS_VOISINS(Lt	: a_Liste_triangle;
			 p	: point_type;
			 T_Tmp	: in out a_liste_triangle;
			 Tab	: in out point_liste_type;
 			 Nb	: in out integer);
-----------------------------------------------------------
Procedure POINTS_VOISINS_CD(Lt	: a_Liste_triangle;
			 p	: point_type;
			 Tab	: in out point_liste_type;
 			 Nb	: in out integer);
--------------------------------------------------------------------------
Procedure RANGE_POINT_UNIQUE(lp	: in out a_liste_point;
			     p	: point_type);
--------------------------------------------------------------------------
Procedure EXTRAIT_POINTS_VOISINS(	lt	: a_liste_triangle;
					p	: point_type;
					lp	: in out a_liste_point);
--------------------------------------------------------------------------
Procedure POINTS_VOISINS(Lt	: a_Liste_triangle;
			 p	: point_type;
			 T_Tmp	: in out a_liste_triangle;
			 Lp	: in out a_liste_point);
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
---------------------------------------------------------------------------
--------------------------------------------------------------------------
-------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Delaunay 
--------------------------------------------------------------------------
Type Type_position_point_cercle is (Dans_le_cercle, Hors_du_cercle,
							Sur_le_cercle);
--------------------------------------------------------------------------
Procedure Stocker_aretes(l_a	: in out A_liste_aretes;
			 T	: Triangle);
-------------------------------------------------------------------------
Function Position_point_cercle(LT : Liste_Triangle;
			       M  : Point_type			       ) return Type_position_point_cercle;
-------------------------------------------------------------------------
Function Position_point_cercle(Centre 	: Point_type_reel;
			       Rayon2  	: float;
			       M      	: Point_type			       ) return Type_position_point_cercle;
-------------------------------------------------------------------------
Function Position_point_cercle(A,C     : Point_type;
                               sinBABC,			       cosBABC : float;
			       M       : Point_type			       ) return Type_position_point_cercle;
--------------------------------------------------------------------------
Procedure Ajout_point_Triangulation(triangles	: in out a_liste_triangle;
				    P		: point_type);
--------------------------------------------------------------------------
Procedure Extraire_triangles(L	: in out a_liste_triangle;
			     p	: point_type;
			     a	: in out a_liste_aretes);
--------------------------------------------------------------------------
Procedure Creer_nouveaux_triangles(Lt	: in out a_liste_triangle;
				   L_a	: in out a_liste_aretes;
				   p	: point_type);
--------------------------------------------------------------------------
Procedure TEC (Tab	: point_liste_type;
	     i		: integer;
	     j		: integer;
	     l_t	: in out a_liste_triangle);
--------------------------------------------------------------------------
Procedure Delaunay_Tsai(Semi_Ini	: point_liste_type;
			L		: in out a_liste_triangle;
			pts_convex	: integer:=0);
                                                          
--------------------------------------------------------------------------
End Delaunay;
