with courbure;
with Lissage_filtrage;
With geometrie;
 use Geometrie;
package body pts_caracteristique is 
-- ===========================================================================
-- Detection des changements de courbure 
--	et des pts pred les pts d'inflexion Tpoints_pred_pi : 
-- Version utilisee couramment (i.e. appelee dans INFLEXIONS) 
-- =========================================================================== 
 Procedure DET_POSITION_PI ( ligne_lisse    : in     point_liste_reel;
							 np		    : in     natural;
							 nb_pts_inflex  : in out natural;
							 Tpoints_pred_pi: in out liens_access_type) is
 npi,ntotal,nbis: natural;
 Tprove   	  : reel_tableau_type(1..np);
 tab_condition  : array(1..np) of boolean:=(others=>TRUE);
 tpi: liens_array_type(1..np+2);
 -- <=> Tpoints_pred_pi ! --
 cpt: liens_array_type(1..np):=(others=>1);
 cptbis: liens_array_type(1..np):=(others=>0);
 signeA, signe: boolean;
 ------------------------------------------------------------------------------- 
 Begin
 for i in 1..np loop
     Tprove(i):=p_vectoriel(ligne_lisse(i),ligne_lisse(i+1),ligne_lisse(i+2));
  end loop;
  for i in 1..np loop
  	tab_condition(i):= (Tprove(i) >= 0.0);
  end loop;
  nb_pts_inflex:= 1;
  tpi(1):= 1;
  npi:= 1;
  cpt(1):= 1;
  for i in 2..np loop
     if tab_condition(i) = tab_condition(i-1) then
		cpt(npi):= cpt(npi) + 1;
	else
		npi:= npi + 1;
     end if;
  end loop;
--  for i in 1..npi loop --    
--		int_io.put(cpt(i));
--  	if (i mod 2)=0 then put(" -"); else put(" +"); end if;
--		new_line(1);
--  end loop;
  nbis:= 1;
  cptbis(1):= cpt(1);
  signeA:= TRUE;
  signe:= signeA;
  for i in 2..npi loop
     signe:= NOT(signe);
     if cpt(i) >= seuil_micro_inflex then  
	 -- | > ou bien >= |	
		if signe = signeA then
			cptbis(nbis):= cptbis(nbis)+cpt(i)   ;
		else	
			nbis:= nbis+1;
			cptbis(nbis):= cpt(i);
			signeA:= NOT(signeA);
		end if;
     else 	
		cptbis(nbis):= cptbis(nbis)+cpt(i)   ;
     end if;
  end loop;
--  new_line(2);
--  for i in 1..nbis loop
--     int_io.put(cptbis(i));
 --     if (i mod 2)=0 then put(" -");  else put(" +");  end if;
--     new_line(1);
--  end loop;
  ntotal:= 0;
   for i in 1..nbis-1 loop
     ntotal:= ntotal + cptbis(i);
     nb_pts_inflex:= nb_pts_inflex + 1;
     tpi(nb_pts_inflex):= ntotal+1;
  end loop;
   nb_pts_inflex:= nb_pts_inflex + 1;
  tpi(nb_pts_inflex):= np+2;
	  Tpoints_pred_pi:= new liens_array_type(1..nb_pts_inflex);
  Tpoints_pred_pi(1..nb_pts_inflex):= tpi(1..nb_pts_inflex);
 end DET_POSITION_PI ;
 -- =========================================================================== 
 -- Detection des changements de courbure  
 --	et des pts pred les pts d'inflexion Tpoints_pred_pi : 
 -- =========================================================================== 
 Procedure DET_POSITION_PI ( ligne_lisse    : in     point_liste_reel;
							np		    : in     natural;
							Tprove   	    : in     reel_tableau_type;
							nb_pts_inflex  : in out natural;
							Tpoints_pred_pi: in out liens_access_type) is   
npi,ntotal,nbis: natural;
tab_condition: array(1..np) of boolean:=(others=>TRUE);
tpi: liens_array_type(1..np+2);
 -- <=> Tpoints_pred_pi ! --   
 cpt: liens_array_type(1..np):=(others=>1);
   cptbis: liens_array_type(1..np):=(others=>0);
   signeA, signe: boolean;
 ------------------------------------------------------------------------------- 
 Begin
  -- --------------------------------------------------------------------------  
  -- Ou solution proposee par Mathieu :   
  --	detecter les intersections entre la ligne originale et son lissage!	--
  -- declare	
  -- tpts_intersec : point_access_type;
	-- nbpts_intersec: integer;
	-- ptinter:gen_io.point_type;
	--Begin
	-- Geometrie.intersections_de_polylignes( ligne,    ligne_lisse_dessin,	--					n_points, N_points,	--					tpts_intersec, nbpts_intersec);
	-- couleur(22);
	-- for i in 1..nbpts_intersec loop
	--    disque(tpts_intersec(i),3);
	-- end loop;
	--end;
  -- ne marche que pour des SIGMA tres faibles !!!  -- --------------------------------------------------------------------------
  for i in 1..np loop
  	tab_condition(i):= (Tprove(i) >= 0.0);
  end loop;
  nb_pts_inflex:= 1;
   tpi(1):= 1;
  npi:= 1;
  cpt(1):= 1;
  for i in 2..np loop
     if tab_condition(i) = tab_condition(i-1) then
		cpt(npi):= cpt(npi) + 1;
     else	
		npi:= npi + 1;
     end if;
  end loop;
--  for i in 1..npi loop
--     int_io.put(cpt(i));
 --     if (i mod 2)=0 then put(" -"); else put(" +"); end if;
--     new_line(1);
--  end loop;
  nbis:= 1;
  cptbis(1):= cpt(1);
  signeA:= TRUE;
  signe:= signeA;
  for i in 2..npi loop
     signe:= NOT(signe);
     if cpt(i) >= seuil_micro_inflex then  
	 -- | > ou bien >= |	
		if signe = signeA then
			cptbis(nbis):= cptbis(nbis)+cpt(i)   ;
		else
			nbis:= nbis+1;
			cptbis(nbis):= cpt(i);
			signeA:= NOT(signeA);
		end if;
     else
		cptbis(nbis):= cptbis(nbis)+cpt(i)   ;
     end if;
  end loop;
     --  new_line(2);
--  for i in 1..nbis loop
--     int_io.put(cptbis(i));
 --     if (i mod 2)=0 then put(" -"); else put(" +"); end if;
--     new_line(1);
--  end loop;
  ntotal:= 0;
   for i in 1..nbis-1 loop
     ntotal:= ntotal + cptbis(i);
     nb_pts_inflex:= nb_pts_inflex + 1;
     tpi(nb_pts_inflex):= ntotal+1;
  end loop;
   nb_pts_inflex:= nb_pts_inflex + 1;
  tpi(nb_pts_inflex):= np+2;
	  Tpoints_pred_pi:= new liens_array_type(1..nb_pts_inflex);
  Tpoints_pred_pi(1..nb_pts_inflex):= tpi(1..nb_pts_inflex);
 end DET_POSITION_PI ;
 -- ========================================================================== 
 -- Determination des coordonnees des PI : 
 -- ========================================================================== 
 Procedure  DET_COORD_PI ( ligne                   : in     point_liste_type;
							ligne_lisse 		   : in     point_liste_reel;
							n_points		   : in     integer;
							nb_pts_inflex 	   : in     natural;
							Tpoints_pred_pi	   : in     liens_access_type;
							tpinflex, tpinflex_lisse: in out point_liste_type) is
Tpoints_inflex : point_liste_type(1..1000);
  Tpoints_lisse_inflex : point_liste_type(1..1000);
  inflex, x1,y1,x2,y2, pred_i1, pred_i2: integer;
   x1f,y1f,x2f,y2f, DX, DY: float;
 ------------------------------------------------------------------------------- 
 Begin
  x1:= ligne(1).coor_x;
  y1:= ligne(1).coor_y;
  x2:= ligne(2).coor_x;
  y2:= ligne(2).coor_y;
  Tpoints_inflex(1):= (x1,y1);
  for rang in 2..nb_pts_inflex-1 loop
       inflex:= Tpoints_pred_pi(rang);
		-- Estimation des coord. du pt d'inflex au milieu du segment du point pred
		-- au point suivant:
		Tpoints_inflex(rang).coor_x:=(ligne(inflex).coor_x+ligne(inflex+1).coor_x)/2;
		Tpoints_inflex(rang).coor_y:=(ligne(inflex).coor_y+ligne(inflex+1).coor_y)/2;
-- Partie visiblement inutile : CD 14/12/98   
-- Si au moins 4 points de la ligne ( PARAMETRE ???) separent    
-- le pt d'inflex pred ou le suivant du pt d'inflex considere, alors    
-- l'angle est moyenne sur 4 points au lieu de 2 (pred et succ.)
--   if (Tpoints_pred_pi(rang+1)-inflex<=4) OR (inflex-Tpoints_pred_pi(rang-1)<=4) then
--	x1:=ligne(inflex).coor_x;
--	y1:=ligne(inflex).coor_y;
--	x2:=ligne(inflex+1).coor_x;
--	y2:=ligne(inflex+1).coor_y;
--   else--	x1:=(ligne(inflex).coor_x+ligne(inflex-1).coor_x)/2;
		--	y1:=(ligne(inflex).coor_y+ligne(inflex-1).coor_y)/2;
		--	x2:=(ligne(inflex+1).coor_x+ligne(inflex+2).coor_x)/2;
		--	y2:=(ligne(inflex+1).coor_y+ligne(inflex+2).coor_y)/2;
--   end if;
  end loop;
  x2:=ligne(n_points).coor_x;
	  y2:=ligne(n_points).coor_y;
  Tpoints_inflex(nb_pts_inflex):=(x2,y2);
  x1:=ligne(n_points-1).coor_x;
	  y1:=ligne(n_points-1).coor_y;
  for i in 1..nb_pts_inflex loop
  	tpinflex(i):= Tpoints_inflex(i);
  end loop;
  -- -----------------------------------------------------------------------  
  -- Determination des coordonnees des PI sur la ligne lissee (pour dessin):  
  -- -----------------------------------------------------------------------
  x1f:= ligne_lisse(1).coor_x;
  y1f:= ligne_lisse(1).coor_y;
  x2f:= ligne_lisse(2).coor_x;
  y2f:= ligne_lisse(2).coor_y;
  Tpoints_lisse_inflex(1):= (integer(x1f),integer(y1f));
  for rang in 2..nb_pts_inflex-1 loop
      inflex:= Tpoints_pred_pi(rang);
     -- Estimation des coord. du pt d'inflex au milieu du segment du point pred      
	 -- au point suivant:     
	 Tpoints_lisse_inflex(rang).coor_x:=integer((ligne_lisse(inflex).coor_x+ligne_lisse(inflex+1).coor_x)/2.0);
     Tpoints_lisse_inflex(rang).coor_y:=integer((ligne_lisse(inflex).coor_y+ligne_lisse(inflex+1).coor_y)/2.0);
     -- Si au moins 4 points de la ligne ( PARAMETRE ???) separent      
	 -- le pt d'inflex pred ou le suivant du pt d'inflex considere, alors      
	 -- l'angle est moyenne sur 4 points au lieu de 2 (pred et succ.)
     if (Tpoints_pred_pi(rang+1)-inflex<=4) OR (inflex-Tpoints_pred_pi(rang-1)<=4) then
		x1f:=ligne_lisse(inflex).coor_x;
		y1f:=ligne_lisse(inflex).coor_y;
		x2f:=ligne_lisse(inflex+1).coor_x;
		y2f:=ligne_lisse(inflex+1).coor_y;
     else
		x1f:=(ligne_lisse(inflex).coor_x+ligne_lisse(inflex-1).coor_x)/2.0;
		y1f:=(ligne_lisse(inflex).coor_y+ligne_lisse(inflex-1).coor_y)/2.0;
		x2f:=(ligne_lisse(inflex+1).coor_x+ligne_lisse(inflex+2).coor_x)/2.0;
		y2f:=(ligne_lisse(inflex+1).coor_y+ligne_lisse(inflex+2).coor_y)/2.0;
     end if;
  end loop;
  x2f:=ligne_lisse(n_points).coor_x;
  y2f:=ligne_lisse(n_points).coor_y;
  Tpoints_lisse_inflex(nb_pts_inflex):=(integer(x2f),integer(y2f));
  x1f:=ligne_lisse(n_points-1).coor_x;
  y1f:=ligne_lisse(n_points-1).coor_y;
  for i in 1..nb_pts_inflex loop
  	tpinflex_lisse(i):= Tpoints_lisse_inflex(i);
  end loop;
 end;
 --============================================================================----============================================================================-- 
 Procedure INFLEXIONS(  ligne 		: in point_liste_type;
						npts		: in natural;
						sigma 		: in float;
						Ligne_lisse 	: in out point_liste_reel;
						nb_pts_inflex	: in out natural;
						Tpoints_pred_pi	: in out liens_access_type;
						Tpinflex,Tpinflex_lisse  : in out point_liste_type) is
pas		: float := 1.0;
 -- Decoupage de l'arc en pts equidistants  
  l: natural := 0;
  n_aux : natural := 1;
--  Ligne_aux, Ligne_aux_lisse 	: point_liste_reel(1..n_aux);
  Ligne_aux, Ligne_aux_lisse    : point_access_reel;
  lgr_segments		     	: liens_array_type(1..npts);
 Begin
	if npts > 3 then
		------------------------------------------------------------------------------ 
		-- Rasterisation de la ligne en segments egaux de lgr PAS (soit la resolution)
		------------------------------------------------------------------------------  
		Decompose_ligne(ligne,npts,ligne_aux,n_aux,pas,lgr_segments);
		--------------------------------------------------------------
		-- Lissage de la ligne par convolution avec un filtre Gaussien--
		------------------------------------------------------------
		ligne_aux_lisse:= new Point_liste_reel(1..n_aux+100);
		Lissage_filtrage.FILTRE_GAUSSIEN_R(ligne_aux.all,n_aux,sigma,ligne_aux_lisse.all);
		-------------------------------------------------------------------------
		-- Extraction des points correspondant aux homologues des points initiaux--
		-----------------------------------------------------------------------  
		l:= 1;
		Ligne_lisse(1):= Ligne_aux_lisse(1);
		for i in 2..npts loop
			l:= l + lgr_segments(i-1);
			Ligne_lisse(i):= Ligne_aux_lisse(l);
		end loop;
		----------------------------------------------
		-- Liberation des allocations dynamiques-----
		-----------------------------------------
		Gr_free_point_liste_reel(Ligne_aux_lisse);
		Gr_free_point_liste_reel(Ligne_aux);
		-------------------------------------------------------------------
		-- Det. des points de la ligne correspondants a des pts pred des PI--
		----------------------------------------------------------------- 
		DET_POSITION_PI ( ligne_lisse, npts-2, nb_pts_inflex, Tpoints_pred_pi );
		---------------------------------------
		-- Determination des coordonnees des PI--
		-------------------------------------
		DET_COORD_PI (ligne, ligne_lisse, npts, nb_pts_inflex, Tpoints_pred_pi,tpinflex, tpinflex_lisse);
	else  
		nb_pts_inflex:=0;
	end if;
 end INFLEXIONS;
--============================================================================--
--============================================================================--
-----------------------------------------------------------------
-- Procedure de determination de 2 niveaux de points d'inflexion :
-- 	les inflexions de niveau 1 delimitant un virage 'complexe'
-- 	les inflexions de niveau 2 dans les virages
-- Le premier sigma vaut arbitrairement 2.0 et le 2eme 100.0 
-- Apres tests, les distances euclidiennes ne suffisent pas sur les 
-- virages tres complexes du type 4524
-- 			=> essai avec les distances curvilignes ...
------------------------------------------------------------------
Procedure HIERARCHIE_DES_PI (  	ligne : in point_liste_type;
							npts		: in natural;
						sigma_faible	: in out float;
						sigma_fort	: in out float;
						nb_pts_inflex	: IN Out natural;
						Tpoints_pred_pi	: In out liens_access_type;
						Tpinflex	: In Out point_liste_type;
						Tniveau_pi	: In Out liens_array_type ) is
 sigma: float;
 Ligne_lisse 	: point_liste_reel(1..1000);
 nb_pts_inflex_virages: natural;
 Tpoints_pred_pi_virages: liens_access_type;
 Tpinflex_virages,  Tpinflex_lisse : point_liste_type(1..300);
 min, dpi : float;
 pi_corres: integer;
 indice1, indice2: integer;
 point1, point2 : gen_io.point_type;
 borne1: integer;
 lgr_ligne: float;
 
Begin
 -- Detection des inflexions sur la ligne quasi brute: nb_pts_inflex:=0;
 INFLEXIONS( ligne, npts, Sigma_faible, ligne_lisse,	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);
 -- Detection des inflexions sur la ligne fortement lissee: nb_pts_inflex_virages:=0;
 INFLEXIONS( ligne, npts, Sigma_fort, ligne_lisse,	 nb_pts_inflex_virages, Tpoints_pred_pi_virages, 	Tpinflex_virages, Tpinflex_lisse);
 -- ====================================================================== 
 -- Relocalisation des PI en fonction de la position a sigma tres faible : 
 --   			<=> au point le plus proche en abscisse curviligne 
 -- ======================================================================
 for i in 1..nb_pts_inflex loop
    tniveau_pi(i):= 2;
 end loop;
 borne1:= 2;
 for i in 2..nb_pts_inflex_virages-1 loop
    min := float'last;
    for j in borne1..nb_pts_inflex-1 loop
      if tpoints_pred_pi(j) = tpoints_pred_pi_virages(i) then 
			pi_corres:= j;
			exit;
      else     --
			dpi:= norme(tpinflex_virages(i),tpinflex(j));
			--dist euclid insuffisante!
			-- Calcul de la distance curviligne :
			if tpoints_pred_pi(j) > tpoints_pred_pi_virages(i) then
				indice1:= tpoints_pred_pi_virages(i);
				point1 := tpinflex_virages(i);
				indice2:= tpoints_pred_pi(j);
				point2 := tpinflex(j);
			elsif tpoints_pred_pi(j) < tpoints_pred_pi_virages(i) then
					indice2:= tpoints_pred_pi_virages(i);
					point2 := tpinflex_virages(i);
					indice1:= tpoints_pred_pi(j);
					point1 := tpinflex(j);
			end if;
			dpi:= norme(point1,ligne(indice1+1));
			for d in indice1+1..indice2-1 loop
					dpi:= dpi+norme(ligne(d),ligne(d+1));
			end loop;
			dpi:= dpi + norme(ligne(indice2),point2);
			if dpi < min then
				min:= dpi;
				pi_corres:= j;
			end if;
  	    end if;
    end loop;
    tniveau_pi(pi_corres):=1;
 -- Pi de niveau 1
	borne1:= pi_corres+1;
 end loop;
end ;
-- ============================================================
-- Procedure de detection des maximas de courbure, i.e. Sommets
-- Methode de Mathieu pour le calcul de courbure
-- Dans cette version, il y a un seul sommet par virage
-- ============================================================
procedure SOMMETS (tabIni 		: IN Point_liste_type;
					nb_points 	: IN natural ;
					SIGMA		: IN float;
					nb_pts_inflex   : IN natural ;
					Tpoints_pred_pi : IN liens_access_type;
					Tab_sommets	: OUT point_liste_type;
					Nb_sommets 	: OUT integer			) is
  Tabreel:point_liste_reel(1..5000);
  Tcourbure: reel_tableau_type(1..500);
  Tcourbure_aux: reel_tableau_type(1..5000);
  Tind_sommets:liens_array_type(1..5000);
  pt_max: integer;
  max_courb: float;
  ns: integer:=0;
  pas		: float := 1.0;
 -- Decoupage de l'arc en pts equidistants
  n_aux 	: natural := 1;
  Ligne_aux 	: point_access_reel;
  nb_segments   : liens_array_type(1..1000);
  l		: integer;
  trouve	: boolean;
 Begin
  Decompose_ligne(tabini,nb_points,ligne_aux,n_aux,pas,nb_segments);
  for p in 1..n_aux loop
	tabreel(p).coor_x:=float(ligne_aux(p).coor_x);
	tabreel(p).coor_y:=float(ligne_aux(p).coor_y);
  end loop;
  Courbure.courbure_mathieu(tabreel(1..n_aux),sigma,4.0,Tcourbure_aux);
  l:= 1;
  Tcourbure(1):= Tcourbure_aux(1);
  for i in 2..nb_points loop
    l:= l + nb_segments(i-1);
    Tcourbure(i):= Tcourbure_aux(l);
  end loop;
  -- Calcul du sommet sur la courbe originale non decomposee
  for npi in 1..nb_pts_inflex-1 loop
	ns:= ns +1;
	max_courb:=0.0;
	trouve:= false;
    ------ Correction FL 28/01/97 "-1" pour que le PI ne soit pas pris	
	for z in Tpoints_pred_pi(npi)+1..Tpoints_pred_pi(npi+1)-1 loop
		if ABS(Tcourbure(z)) > max_courb then
			max_courb:= ABS(Tcourbure(z));
	     	pt_max:= z;
			trouve:= true;
		end if;
	end loop;
	if trouve=true then
		Tind_sommets(ns):= pt_max;
		Tab_sommets(ns):= tabini(pt_max);
	else
		pt_max:= Tpoints_pred_pi(npi)+1 + (Tpoints_pred_pi(npi+1)-(Tpoints_pred_pi(npi)+1))/2;
		Tind_sommets(ns):= pt_max;
 		Tab_sommets(ns):= tabini(pt_max);
	end if;
  	-- Sommets intermediaires ...--	
	for p in Tpoints_pred_pi(npi)+1..pt_max-1 loop -- 
		if Tcourbure(p)>Tcourbure(p-1) AND Tcourbure(p)>Tcourbure(p+1) then--
			ns:= ns +1;--
        	Tind_sommets(ns):= p;--
			Tab_sommets(ns):= tabini(p);--
		end if;--
	end loop;
--	
	for p in pt_max+1..Tpoints_pred_pi(npi+1) loop--
		if Tcourbure(p)>Tcourbure(p-1) AND Tcourbure(p)>Tcourbure(p+1) then--
			ns:= ns +1;--        
			Tind_sommets(ns):= p;--
			Tab_sommets(ns):= tabini(p);--
		end if;--
	end loop;
  end loop;
  nb_sommets:= Ns;
  -- Calcul du sommet sur la courbe decomposee
  -- Plus precis ???
  -- NON parce qu'on des courbes digitalisees => 1 sommet = 1 pt intermediaire  
  -- A suivre ????????????????????????????????????????????????????????????????-- l:= 0;
--  for npi in 1..nb_pts_inflex-1 loop
--	ns:= ns +1;
--	max_courb:=0.0;
--	for z in Tpoints_pred_pi(npi)+1..Tpoints_pred_pi(npi+1)-1 loop
--	   for w in 1..nb_segments(z) loop
--	   	l:= l+1;
	--		if ABS(Tcourbure_aux(l)) > max_courb then--			max_courb:= ABS(Tcourbure_aux(l));
--			pt_max:= l;
--		end if;
--	   end loop;
--	end loop;
--        Tind_sommets(ns):= pt_max;
--	Tab_sommets(ns):= ( integer(tabreel(pt_max).coor_x),--			    integer(tabreel(pt_max).coor_y) );
--  end loop;
--  nb_sommets:= Ns;
  -- Trace des valeurs des courbures:--  new_line(3);
--  for i in 1..nb_points loop
--     	int_io.put(i); put("  ");
--     	flo_io.put(ABS(Tcourbure(i)),1,8,0); put("  ");
--     	flo_io.put(Tcourbure(i),1,1,0);
--     	for j in 1..nb_pts_inflex-1 loop
	--	   if tpoints_pred_pi(j)=i then--	      	new_line(1);  put("  PI"); new_line(1);
		-- 	   end if;
--	   if tind_sommets(j)=i then--	      	put("  S");
		-- 	   end if;
--	end loop;
--	new_line(1);
--  end loop;
--  l:= 1;
--  flo_io.put(Tcourbure_aux(1),1,6,0);
 --  for i in 2..nb_points loop
----     int_io.put(i); new_line(1);
--     for k in 1..nb_segments(i-1) loop
----        l:= l+1;
--     	flo_io.put(Tcourbure_aux(l),1,6,0);
 ----     	for j in 1..nb_pts_inflex-1 loop
	--	   if tpoints_pred_pi(j)=i then--	      	put("  PI");
-- 	   end if;
--	   if tind_sommets(j)=i then--	      	put("  S");
		-- 	   end if;
--	end loop;
--	new_line(1);
--     end loop;
	--  end loop;
        -- Correction FL 02/02/98 : liberation de memeoir
		Gr_free_point_liste_reel(Ligne_aux);
 end Sommets;
-- ==================================================================
-- Procedure de detection des maximas de courbure, i.e. Sommets
-- Approximation au point le plus eloigne (en projection orthogonale)
-- de la droite joignant les points d'inflexion
-- ==================================================================
procedure SOMMETS ( ligne		: IN point_liste_type;
					ligne_lisse	: IN point_liste_type;
					nb_points 	: IN natural ;
					SIGMA		: IN float;
					Tpinflex	: IN point_liste_type;
					nb_pts_inflex   : IN natural ;
					Tpoints_pred_pi : IN liens_access_type;
					Tsommet		: OUT tsommet_type) is
h,hauteur,pv   : float:= 0.0;
 sommet         : gen_io.point_type;
 ligne_entre_pi : point_access_type;
  tresultat      : point_access_type;
  nresultat      : integer;
 np_entre_pi    : integer;
 choix		: integer;
 indice_sommet	: integer;
 signe		: boolean;
 Tprove   	: reel_tableau_type(1..nb_points-2);
 seuil		: float :=  0.1; -- Seuil Douglas en mm 
 resolution	: integer := 16;-- ou info.resolution ! 
 n		: integer;
Begin
   ligne_entre_pi   := new Point_liste_type(1..500);
   tresultat        := new Point_liste_type(1..500);
   for i in 1..nb_pts_inflex-1 loop
		np_entre_pi:= 1;
		ligne_entre_pi(1):= tpinflex(i);
		for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1) loop
			np_entre_pi:= np_entre_pi+1;
			ligne_entre_pi(np_entre_pi):= ligne(j);
		end loop;
		np_entre_pi:= np_entre_pi+1;
		ligne_entre_pi(np_entre_pi):= tpinflex(i+1);
		choix:=2;
		if choix=1 then
			-- DOUGLAS --    
			-- SOMMETS : Douglas / Peucker entre les points d'inflexion -- 
			-- -------------------------------------------------------- --
			Lissage_filtrage.douglas_peucker ( 	ligne_entre_pi, np_entre_pi, tresultat, nresultat,seuil, resolution );
			if nresultat >= 2 then 
				-- Au moins un sommet !      
				tsommet(i).ns := nresultat-2;
				for j in 1..nresultat-2 loop
					tsommet(i).ts(j) := tresultat(j+1);
				end loop;
			else -- Pas de sommet trouvé avec D&P !
				for i in 1..nb_pts_inflex-1 loop
					h:= 0.0;
					hauteur:= 0.0;
					------ Correction FL 28/01/97 "-1" pour que le PI ne soit pas pris
					for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1)-1 loop
						h:= distance_a_ligne(ligne(j),tpinflex(i),tpinflex(i+1),C_ligne_type);
						if h > hauteur then
							hauteur:= h;
							sommet := ligne(j);
						end if;
					end loop;
					tsommet(i).ns := 1;
					tsommet(i).ts(1):= sommet;
				end loop;
			end if;
		elsif choix=2 then
			for i in 1..nb_points-2 loop
				Tprove(i):=p_vectoriel(ligne_lisse(i),ligne_lisse(i+1),ligne_lisse(i+2));
			end loop;
			-- Recherche du max de courbure --    
			-- -------------------------------------------------------- --
			for i in 1..nb_pts_inflex-1 loop
				pv:=0.0;
				for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1)-1 loop
					if ABS(tprove(j)) > pv 		and (tprove(indice_sommet)<=0.0) = (tprove(j)>0.0) then
						pv := ABS(tprove(j));
						sommet:=ligne(j+1);
						indice_sommet:= j    + 1;
						-- !!!     
					end if;
				end loop;
				tsommet(i).ns := 1;
				tsommet(i).ts(1):= sommet;
				tsommet(i).indice_sommet(1):= indice_sommet;
			end loop;
		end if;
	-- Sur le choix --
	end loop;
end sommets;
end pts_caracteristique;
