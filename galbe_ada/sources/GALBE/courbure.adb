-- *********************************************************************** 
---- Package de calcul de la courbure pour detection des maximas de courbure
-- Voir la procedure SOMMETS dans le package MESURE_LIGNE
-- Methode de Mathieu pour le calcul de courbure
-- *********************************************************************** 
--
With math_int_basic;
With Geometrie; use Geometrie;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95


Package Body Courbure is 

  PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)
  
-----------------------------------------------------------------------------
Function Theta(	tab	:point_liste_reel;
 		i 	: integer) return float is 
Sol : float:=0.0;
 p1,p2	: point_type_reel;
	Begin
	if i<tab'last then
	p1:=tab(i);
   p2:=tab(i+1);
 else
 P1:=tab(i-1);
   P2:=tab(i);
 end if;
 if p1.coor_x=p2.coor_x then
 if p1.coor_y<p2.coor_y  then
 return PI/2.0;
     else
	 return -PI/2.0;
     end if;
 elsif p2.coor_x<p1.coor_x then
 sol:= arctan((p2.coor_y-p1.coor_y)/(p2.coor_x-p1.coor_x));
   if sol<=0.0 then
   return sol+Pi;
   else
   return sol-Pi;
   end if;
 else
 sol:= arctan((p2.coor_y-p1.coor_y)/(p2.coor_x-p1.coor_x));
   return sol;
 End if;
End theta;
-----------------------------------------------------------------------------
Procedure Tangente_Simple(T	: point_liste_reel;
			  Tg	: in out reel_tableau_type) is
Begin
    for i in t'first..t'last loop
	Tg(i):=theta(t,i);
    end loop;
End Tangente_Simple;
-----------------------------------------------------------------------------
Function Symetrie_aux_extremites(t	: reel_tableau_type;
				Ajout	: integer) return reel_tableau_type is
tab_med	: reel_tableau_type(t'first..t'last+2*Ajout);
Begin
  tab_med(t'first+ajout..t'last+ajout):=t;
  for i in 0..ajout-1 loop
  tab_med(i+1):=t(ajout-i);
    tab_med(t'last+ajout+i+1):=t(t'last-i-1);
  end loop;
  return tab_med;
End Symetrie_aux_extremites;
------------------------------------------------------------------------------
Function convolution_par_gaussienne( 	T	: reel_tableau_type;
					i	: integer;
					sig	: float;
					k	: integer;
				dist_min	: float:=1.0				    ) return float is
Sol	: float:=0.0;
 i_j	: float;
 Spond	: float:=0.0;
 pond	: float:=0.0;
Begin
  for j in i-k..i+k loop 
  i_j:=float(i-j)*dist_min;
    pond:=exp(-(i_j/sig)**2/2.0)/(sig*sqrt(2.0*Pi));
    Spond:=Spond+pond;
    Sol:=sol+T(j)*pond;
  end loop;
  return sol;
End convolution_par_gaussienne;
------------------------------------------------------------------------------
Procedure courbure_mathieu (	tab_in	: point_liste_reel;
				sig	: float;
				trunc	: float;
				Courb	: out reel_tableau_type) is

dist_min: float:=geometrie.norme(tab_in(1),tab_in(2));
voisins	: integer;
tg,tmp	: Reel_tableau_type(tab_in'range):=(others=>0.0);
Begin
  tangente_simple(tab_in,tg);
  for i in Tab_in'first+1..tab_in'last-1 loop
  if abs(tg(i+1)-tg(i-1))<=Pi then
  tmp(i):=(tg(i+1)-tg(i-1))/(2.0*dist_min);
      elsif (tg(i+1)-tg(i-1))>Pi then
	  tmp(i):=(tg(i+1)-tg(i-1)-2.0*Pi)/(2.0*dist_min);
      else
	  tmp(i):=(tg(i+1)-tg(i-1)+2.0*Pi)/(2.0*dist_min);
      end if;
    end loop;
  tmp(tab_in'first):= tmp(tab_in'first+1);
  tmp(tab_in'last):=  tmp(tab_in'last-1);
  voisins :=integer(sig*trunc/dist_min);
  -- controle pour que voisins < nombre de points de la ligne sinon plantage  
  -- dans la la symetrie_aux_extremites   
  -- traduisant que la valeur de sigma est aberrante pour cette ligne  
  voisins:=Math_int_basic.Min(voisins,tab_in'last-1);
  declare
  tgl : Reel_tableau_type(tab_in'first..tab_in'last+2*voisins)							:=(others=>0.0);
  begin
  tgl:=symetrie_aux_extremites(tmp,voisins);
    for i in tab_in'range loop
	Courb(i):=convolution_par_gaussienne(tgl,i+voisins,sig,voisins,dist_min);
    end loop;
  end;
End courbure_mathieu;
            End Courbure;
