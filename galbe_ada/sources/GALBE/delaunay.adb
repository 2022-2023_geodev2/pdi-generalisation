-------------------------------------------------------------------
-- Tous les packages de la Triangulation de Delaunay sur des points
-- ecrits par Hergott + Barrault + Duchene
-------------------------------------------------------------------
With Text_io;
With Geometrie; use Geometrie;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

Package Body Delaunay is
Package real_io is new text_io.Float_io(float);
use real_io;
Package int_io is new text_io.Integer_io(integer);
use int_io;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Ptriangle-----------------------------------------------
-------------------------------------------------------------------------------------------------------
Function Compte( Tr	: in a_Liste_triangle) return integer is
Begin
  If tr/=Null
  then return Compte(tr.suiv)+1;
    else return 0;
  End if;
End Compte;
--------------------------------------------------------------------------
Function Norme(p1 : point_type_reel;
p2 : point_type) return float is
Begin
  return sqrt((p1.coor_x-float(p2.coor_x))*(p1.coor_x-float(p2.coor_x))	     +(p1.coor_y-float(p2.coor_y))*(p1.coor_y-float(p2.coor_y)));
exception
  when constraint_error=>return float'last;
  --put_line("erreur de contraintes");
End norme;
--------------------------------------------------------------------------
Function Norme(p1 : point_type;
 p2 : point_type_reel) return float is
 Begin
 return sqrt((float(p1.coor_x)-p2.coor_x)**2+(float(p1.coor_y)-p2.coor_y)**2);
exception
  when constraint_error=>return float'last;
  --put_line("erreur de contraintes");
End norme;
-------------------------------------------------------------------------
-- Procedure qui permute deux points-------------------------------------
------------------------------------
Procedure Permute(P1 : in out Point_type;
 P2: in out Point_type) is
 Sauv_point : Point_type;
Begin
  Sauv_point:=P1;
  P1 := P2;
  P2 := Sauv_point;
End Permute;
-------------------------------------------------------------------------
-- Procedure qui permute deux reels--------------------------------------
-----------------------------------
Procedure Permute(F1: in out float;
 F2: in out float) is
 Sauv_float : float;
Begin
  Sauv_float:=F1;
  F1 := F2;
  F2 := Sauv_float;
End Permute;
--------------------------------------------------------------------------
Procedure Vide(l	: in out a_liste_triangle) is 
Ttmp	: a_liste_triangle;
Begin
--  if l/=null then 
--    vide(l.suiv);
--    recupere_t(l);
--  end if;
  While l/=Null loop
  Ttmp:=l;
    L:=L.suiv;
    Recupere_t(Ttmp);
  End Loop;
End vide;
--------------------------------------------------------------------------
Procedure Colle_2listes(L1	: in out a_liste_triangle;
			L2	: a_liste_triangle		       ) is
Begin
  if l1/=null then
  colle_2listes(l1.suiv,l2);
  else
  l1:=l2;
  end if;
End Colle_2listes;
----------------------------------------------------------------------------
Procedure ajoute_triangles(TS	: a_liste_triangle;
			   TD	: in out A_liste_triangle) is
Begin  IF TD/=NUll then
    Ajoute_triangles(Ts, TD.suiv);
  Else
  If Ts/=Null then
  ajout_triangle(TD, Ts.tr);
	Ajoute_triangles(Ts.suiv, TD);
    End if;
    End if;
End Ajoute_triangles;
--------------------------------------------------------------------------
Procedure Enlever_triangle_suivant(T		: in out a_liste_triangle			  	) is 
Tmp	: a_liste_triangle;
Begin
  If (T/=null) then
  if (T.suiv/=null) then
  Tmp:=T.suiv;
      T.suiv:=T.suiv.suiv;
      Recupere_t(Tmp);
    else
	Recupere_t(T);
    end if;
  end if;
End Enlever_triangle_suivant;
-------------------------------------------------------------------------
-- Procedure qui renvoie, pour un triangle :
--   - Sa categorie(Normal,Plat ou Quasi-plat)
--   - Les elements qui permettront de situer un point qcq par rapport a
--     son cercle circonscrit : * Centre + Rayon pour un triangle normal
--                              * 2 sommets aigus + sin,cos de l'angle plat
--                                pour un triangle quasi-plat...
--     Les element non significatifs por sa categorie etant mis a zero 
-------------------------------------------------------------------------
Procedure Accessoires_triangle(
	Tr      : triangle;
	Cat     :in out Type_categorie_triangle;
	Centre  :in out Point_type_reel;
	Rayon2  :in out float;
	A,C     :in out Point_type;
	sinBABC,cosBABC :in out float)
IS
	XBA,XBC,XCA,  YBA,YBC,YCA 		: float;
	B  			:Point_type;
	K,BA,BC,CA,invD,  sinB,cote_max   	: float:=0.0;
	Tmp             	: float;
Begin
  A:=Tr.P1;
  B:=Tr.P2;
  C:=Tr.P3;
   XBA :=Float(A.coor_x-B.coor_x);
  XBC :=Float(C.coor_X-B.coor_X);
  XCA :=Float(A.coor_X-C.coor_X);
  YBA :=Float(A.coor_y-B.coor_y);
  YBC :=Float(C.coor_y-B.coor_y);
  YCA :=Float(A.coor_y-C.coor_y);
  Centre :=(0.0,0.0);
  Rayon2 := 0.0;
  sinBABC :=0.0;
  cosBABC :=0.0;
  K:=XBA*YBC-YBA*XBC;
           -- Determinant(BA,BC)  
		   If (K=0.0) then               
		   -- Triangle plat, on ne va pas plus 
		   Cat:=Plat;
    A:=(0,0);
    C:=(0,0);
  Else                          
  -- Tr non_plat, calcul des parametres    
  BA:=Norme(B,A);
     BC:=Norme(B,C);
     CA:=Norme(C,A);
     sinB := abs(K/(BA*BC));
    invD := sinB/CA;
            -- Inverse du diametre du cercle circonscrit
			If (invD < (1.0/MAXDIAM)) then
			-- diametre trop grand, tr. quasi-plat      
			-- Permutations pour avoir: B le sommet aplati      
			--                          angle (BA,BC) compris entre 0 et +PI
			Cat:=Quasi_plat;
      cote_max := CA;
       If BC>cote_max then
	   -- permutation de A et B + coord et dist corresp        
	   Permute(A,B);
	XBA:=-XBA;
	YBA:=-YBA;
        Permute(BC,CA);
        tmp := XCA;
	XCA := -XBC;
	XBC := -tmp;
	tmp := YCA;
	YCA := -YBC;
	YBC := -tmp;
 	cote_max := CA;
       end if;
      If BA>cote_max then
	  -- permutation de C et B + coord et dist corresp
	  Permute(C,B);
	XBC:=-XBC;
	YBC:=-YBC;
        Permute(BA,CA);
        Permute(XBA,XCA);
	Permute(YBA,YCA);
      end if;
      -- A ce stade B est le sommet aplati, reste a realiser 0<(BA,BC)<PI
	  sinBABC := (XBA*YBC-YBA*XBC)/(BA*BC);
  -- sin(BA,BC)      
  If sinBABC<0.0 then 
  -- permutation de A et C + coord et dist corresp	Permute(A,C);
	XCA:=-XCA;
	YCA:=-YCA;
	        Permute(BA,BC);
	Permute(XBA,XBC);
	Permute(YBA,YBC);
        sinBABC:=-sinBABC;
      End if;
      -- Fin des permutations
      cosBABC := (XBA*XBC+YBA*YBC)/(BA*BC);
  -- cos(BA,BC)
  Else 
  -- Triangle normal
  Cat:=Normal;
 --On n'a donc pas effectue de permutations 
 K:=(XCA*XBC+YCA*YBC)/K;
                 -- (BC.CA)/det(BA,BC)
	Centre.coor_x:=0.5*(Float(A.coor_x+B.coor_x)+YBA*K);
      Centre.coor_y:=0.5*(Float(A.coor_y+B.coor_y)-XBA*K);
      Rayon2:=(Centre.coor_x-Float(A.coor_x))*(Centre.coor_x-Float(A.coor_x))+
	  (Centre.coor_y-Float(A.coor_y))*(Centre.coor_y-Float(A.coor_y));
      A:=(0,0);
      C:=(0,0);
    End if;
 -- Triangle normal ou quasi_plat
End if;
 -- Triangle plat ou non
End Accessoires_triangle;
--------------------------------------------------------------------------    
Procedure Ajout_triangle( Tr	: in out a_Liste_triangle;
			  T	: triangle			) is
Begin
  If Tr=null then
  Tr:= New Liste_triangle;
    tr.all.tr:=t;
    Accessoires_triangle(TR.ALL.tr, TR.ALL.Cat, TR.ALL.Centre,  TR.ALL.Rayon2,
	TR.ALL.A, TR.ALL.C, TR.ALL.SinBABC, TR.ALL.CosBABC);
  else 
  Ajout_triangle(Tr.suiv,t);
  end if;
End Ajout_triangle;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************

---------------------------------------------------------------------------
-------------------------------------------------------------------------
-- Ancien package Parete---------------------------------------------------
-------------------------------------------------------------------------------------------------
Function Egale(	a1	: arete;
	        a2	: arete) return boolean is
Begin
  if ((a1.p1=a2.p1) and (a1.p2=a2.p2))    or ((a1.p2=a2.p1) and (a1.p1=a2.p2))	then
  return True;
  	else  return False;
  end if;
End Egale;
--------------------------------------------------------------------------
Function  Recherche_arete(l	: a_Liste_aretes;
			  a	: arete			 ) return boolean is
Atmp	: a_Liste_aretes;
  Sol	: Boolean:=False;
begin
  Atmp:=l;
  While (Atmp/=Null) loop
  If Egale(Atmp.a, a) then
  Sol:=TRUE;
	Exit;
    End If;
    Atmp:=Atmp.suiv;
  End Loop;
  Return Sol;
--  if l=null then return false;
--  else if egale(l.a,a) --	  then return true;
--    	  else return Recherche_arete(l.suiv,a);
--       end if;
--  end if;
end recherche_arete;
--------------------------------------------------------------------------
Procedure Vide(l	: in out a_liste_aretes) is
  Atmp	: a_liste_aretes;
Begin
--  if l/=null then
 --    vide(l.suiv);
--    recupere_a(l);
--  end if;
  While l/=Null loop
  Atmp:=l;
   l:=l.suiv;
   recupere_a(Atmp);
  End loop;
  End vide;
----------------------------------------------------------------------------Enleve les aretes qui sont en double : passe de 2 a 0--
Procedure Elager_aretes(l_a	: in out A_liste_aretes) is Tp	: arete;
Begin
  if l_a/=null and then l_a.suiv/=null then
  if Recherche_arete(L_a.suiv.suiv,L_a.suiv.a) then
  tp:=l_a.suiv.a;
       Enleve_arete(L_a,tp);
       Elager_aretes(l_a);
    else
	Elager_aretes(l_a.suiv);
    end if;
  end if;
End elager_aretes;
--------------------------------------------------------------------------
Procedure Vire_les_Doublons(l_a	: in out A_liste_aretes) is
  ENLEVE	: Boolean;
  Tmp		: a_liste_aretes;
Begin
  if l_a/=null and then l_a.suiv/=null then
  ENLEVE:=FALSE;
     Enleve_Arete(L_A.suiv, L_a.suiv.a, ENLEVE);
     IF ENLEVE then
	 Tmp:=L_A.suiv;
 L_a.suiv:=L_a.suiv.suiv;
       recupere_a(Tmp);
     Vire_les_Doublons(L_a);
     End IF;
     Vire_les_Doublons(L_a.suiv);
  End If;
End Vire_les_Doublons;
--------------------------------------------------------------------------
Procedure Enleve_arete( l_a	: in out a_liste_aretes;
		        art	: arete;
			ENLEVE	: in out Boolean		     ) is 
			Tmp	: a_liste_aretes;
Begin
  if l_a/=null and then l_a.suiv/=null then
  if Egale(l_a.suiv.a,art) then
  ENLEVE:=TRUE;
        Tmp:=l_a.suiv;
        l_a.suiv:=l_a.suiv.suiv;
        Recupere_a(tmp);
      else
	  Enleve_arete(l_a.suiv, art, ENLEVE);
       end if;
  end if;
End Enleve_arete;
--------------------------------------------------------------------------
Procedure Enleve_arete(l_a	: in out a_liste_aretes;
		       art	: arete		     ) is
Tmp	: a_liste_aretes;
Begin
  if (l_a/=null) and then (l_a.suiv/=null) then
  if Egale(l_a.suiv.a,art) then
  Tmp:=l_a.suiv;
        l_a.suiv:=l_a.suiv.suiv;
        recupere_a(tmp);
        enleve_arete(l_a,art);
      else
	  enleve_arete(l_a.suiv,art);
       end if;
  end if;
End Enleve_arete;
--------------------------------------------------------------------------
Procedure Supprime_aretes_vides(L	: in out a_liste_aretes) is
  tmp	: a_liste_aretes;
Begin
  if l/=null then
  if l.suiv/=null then
  if l.suiv.a.p1=l.suiv.a.p2 then
  tmp:=l.suiv;
        l.suiv:=l.suiv.suiv;
	recupere_a(tmp);
      end if;
      supprime_aretes_vides(l.suiv);
    end if;
  end if;
end supprime_aretes_vides;
--------------------------------------------------------------------------
Function Compte_Liste(	L	: a_liste_aretes) return integer is
Begin
  if l/=null then
  return compte_liste(l.suiv)+1;
  else   return 0;
  end if;
End Compte_liste;
-------------------------------------------------------------------------- 
Procedure range_aretes(l	: a_liste_aretes;
			tab	: in out point_liste_type;
			crs	: integer) is
begin
   if l/=null then
   for h in crs..tab'last loop
   if tab(h)=l.a.p2 then
   tab(h):=tab(crs);
	tab(crs):=l.a.p2;
        range_aretes(l.suiv,tab,crs+1);
        exit;
      end if;
    end loop;
   end if;
 End Range_aretes;
--------------------------------------------------------------------------
Procedure ajout_arete(L	: in out a_liste_aretes;
		      a	: arete) is
Begin
  If l/=null
  then ajout_arete(l.suiv,a);
    else
	l:=new liste_aretes;
      l.a:=a;
  end if;
End ajout_arete;
--------------------------------------------------------------------------
Procedure Lire_a(ar	: a_liste_aretes) is
  Atmp	: a_liste_aretes:=ar;
Begin
  While Atmp/=Null loop
  int_io.put(Atmp.a.p1.coor_x);
int_io.put(Atmp.a.p1.coor_y);
    int_io.put(Atmp.a.p2.coor_x);
int_io.put(Atmp.a.p2.coor_y);
    Text_io.put_line("");
    Atmp:=Atmp.suiv;
  end Loop;
end Lire_a;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Point---------------------------------------------------
--------------------------------------------------------------------------
----------------------
Procedure Vide(l	: in out a_liste_point) is
Begin
  if l/=null then
  vide(l.suiv);
    recupere(l);
  end if;
End vide;
--------------------------------------------------------------------------
Procedure Enlever_Point_suivant(P		: in out a_liste_point) is
 Tmp	: a_liste_point;
Begin
  If (p/=null) then
  if (p.suiv/=null) then
  Tmp:=p.suiv;
      p.suiv:=p.suiv.suiv;
      Recupere(Tmp);
    else
	Recupere(p);
    end if;
  end if;
End Enlever_point_suivant;
--------------------------------------------------------------------------    
Procedure Ajout_Point( 	L	: in out a_Liste_Point;
			P	: Point_type			) is
Begin
			If L=null then
			L:= New Liste_Point;
  L.p:=p;
else  Ajout_Point(L.suiv,p);
end if;
End Ajout_Point;
--------------------------------------------------------------------------    
Function dernier_point(	L 	: a_liste_point) return a_liste_point is
Begin
  if l/=null then
  if l.suiv/=null then
  return dernier_point(l.suiv);
    else
	return l;
    end if;
  else
  return l;
  End if;
End Dernier_point;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
------------------------------------------------------------------------
-- Ancien package Convexite-----------------------------------------------
---------------------------------------------------------------------------------------------------
-- Fonction qui renvoie true si la projection orthogonale du point M sur 
-- le support de l'arete AB est strictement comprise entre A et B
--------------------------------------------------------------------------
Function Point_interieur_arete(
	M  : Point_type;
	AB : arete)
return boolean
is
--	Vecteur MA
	MA   : Point_type := ((AB.p1.coor_x-M.coor_x),(AB.p1.coor_y-M.coor_y));
--	Vecteur MB
	MB   : Point_type := ((AB.p2.coor_x-M.coor_x),(AB.p2.coor_y-M.coor_y));
	scal : integer;
Begin
	  scal := (MA.coor_x)*(MB.coor_x)+(MA.coor_y)*(MB.coor_y);
   If scal<0 then
   return true;
  Else
  return false;
  End if;
End Point_interieur_arete;

   --------------------------------------------------------------------------
Function Offset(	a	: arete;
			p	: point_type) return Float is 
Perp	: point_type:=(-(a.p2.coor_y-a.p1.coor_y),a.p2.coor_x-a.p1.coor_x);
 vect	: point_type:=(p.coor_x-a.p1.coor_x,p.coor_y-a.p1.coor_y);
begin
  return  Float(vect.coor_x*perp.coor_x+vect.coor_y*perp.coor_y);
end Offset;
    
--------------------------------------------------------------------------
Procedure Enveloppe_convexe( TP		: point_liste_type;
			     L_a	: in out a_liste_aretes) is
Tmp	: a_liste_aretes;
  Sol	: integer:=0;
begin             
   quatre_points_convexes(TP,L_a);
  tmp:=new liste_aretes;
 tmp.suiv:=l_a;
 l_a:=tmp;
  supprime_aretes_vides(L_a);
  l_a:=tmp.suiv;
 recupere_a(tmp);
  Lance_convex(l_a,TP);
End enveloppe_convexe;
--------------------------------------------------------------------------
Procedure quatre_points_convexes(Tab	: point_liste_type;
				 ar	: in out a_liste_aretes) is
 xmya,  xmyi	: integer:=tab(tab'first).coor_x-tab(tab'first).coor_y;
  xpya,  xpyi	: integer:=tab(tab'first).coor_x+tab(tab'first).coor_y;
  pxmya,  pxmyi,  pxpya,  pxpyi	: integer:=tab'first;
  mn	: integer;
  pl	: integer;
Begin
  for i in tab'range loop
  mn:=tab(i).coor_x-tab(i).coor_y;
    if xmya<mn then
	xmya:=mn;
       pxmya:=i;
    elsif xmyi>mn then
	xmyi:=mn;
       pxmyi:=i;
    end if;
    pl:=tab(i).coor_x+tab(i).coor_y;
    if xpya<pl then
	xpya:=pl;
       pxpya:=i;
    elsif xpyi>pl then
	xpyi:=pl;
       pxpyi:=i;
    end if;
  end loop;
  ar:=new liste_aretes;
  ar.a.p1:=tab(pxmyi);
  ar.a.p2:=tab(pxpya);
    ar.suiv:=new liste_aretes;
  ar.suiv.a.p1:=tab(pxpya);
  ar.suiv.a.p2:=tab(pxmya);
  ar.suiv.suiv:=new liste_aretes;
  ar.suiv.suiv.a.p1:=tab(pxmya);
  ar.suiv.suiv.a.p2:=tab(pxpyi);
  ar.suiv.suiv.suiv:=new liste_aretes;
  ar.suiv.suiv.suiv.a.p1:=tab(pxpyi);
  ar.suiv.suiv.suiv.a.p2:=tab(pxmyi);
    ar.suiv.suiv.suiv.suiv:=Null;
   End quatre_points_convexes;
--------------------------------------------------------------------------
procedure Lance_convex(l	: in out a_liste_aretes;
		       T	: point_liste_type) is
Begin
 if l/=null then
 lance_convex(l.suiv,T);
   convex(l,T);
 end if;
End Lance_convex;
--------------------------------------------------------------------------
Procedure Convex(ar		: in out a_liste_aretes;
		 Tab_points	: point_liste_type) is
Offset_max	: float:=-1.0;
  Offset_tmp	: float:=0.0;
  Pt_max	: integer:=1;
  Tmp		: a_liste_aretes;
  sol		: integer:=0;
Begin
  If ar/=null then
  for i in Tab_points'range loop
  Offset_tmp:=Offset(ar.a,tab_points(i));
      If Offset_tmp>=offset_max and	   Point_interieur_arete(tab_points(i),ar.a) then
	  pt_max:=i;
	Offset_max:=Offset_tmp;
      End if;
    End loop;
    If	(offset_max>=0.0) and 	(Point_interieur_arete(tab_points(pt_max),ar.a)) then
	Tmp:=ar.suiv;
      ar.suiv:=new liste_aretes;
      ar.suiv.a:=(tab_points(pt_max),ar.a.p2);
      ar.suiv.suiv:=tmp;
      ar.a:=(ar.a.p1,tab_points(pt_max));
      convex(ar.suiv,tab_points);
      convex(ar,tab_points);
    End if;
  end if;
End Convex;
--------------------------------------------------------------------------
Procedure Ensemble_Convexe(Semi		: in  out point_liste_type;
			   Nb_pts	: out integer) is  
l_aretes	: a_liste_aretes;
Begin
      enveloppe_convexe(semi,l_aretes);
           Range_aretes(l_aretes,semi,1);
      nb_pts:=compte_liste(l_Aretes);
      Vide(l_aretes);
End Ensemble_Convexe;
--------------------------------------------------------------------------
Function Ensemble_convexe(t	: point_liste_type) return boolean is
  la_tmp	: a_liste_aretes;
  Nbre		: Integer;
Begin
 If T'length>2 then
 Enveloppe_convexe(t,la_tmp);
   Nbre:=compte_liste(la_tmp);
 vide(la_tmp);
   if Nbre<t'length	then
   return False;
   	else return True;
   end if;
 Else Return False;
 End If;
End Ensemble_Convexe;
--------------------------------------------------------------------------
--On cherche a rendre (a,b,c) concave en (a,p,c) convexe.-----------------
--ou a, b, c sont des voisins de centre.----------------------------------
Procedure Convexe_Concave( L		: in out a_liste_point;
			   centre	: point_type;
			   T		: a_liste_triangle;
			   l_a		: in out a_liste_point;
			   repere	: in out point_type;
			   Modif 	: in out boolean) is
t1,t2	: triangle;
  p_tmp	: a_liste_point;
  pTmp	: point_type;
  a	: arete;
Begin
  If (l/=null) and then (l.suiv/=null) and then (l.suiv.suiv/=null)
  and then not(convexe(l.p,l.suiv.p,l.suiv.suiv.p,centre)) then
  modif:=true;
    t1:=Triangle_Oppose((L.p,L.suiv.p,centre),centre,T);
    t2:=Triangle_Oppose((L.suiv.p,L.suiv.suiv.p,centre),centre,T);
    a:=Adjacent(t1,t2);
    If (a.p1/=a.p2) then
	ajout_point(l_a,l.suiv.p);
      If l.suiv.p=repere
	  then repere:=l.p;
	      End if;
      If Sommet_Oppose(a,t1)=Sommet_Oppose(a,t2)	then
	  enlever_point_suivant(l);
	else
	if a.p1=l.suiv.p	  then
	l.suiv.p:=a.p2;
	  else l.suiv.p:=a.p1;
        End if;
      End if;
    Else      ptmp:=l.suiv.suiv.p;
      p_tmp:=l.suiv.suiv;
      l.suiv.suiv:=new liste_point;
      l.suiv.suiv.p:=Sommet_Oppose((ptmp,l.suiv.p),t2);
      l.suiv.suiv.suiv:=p_tmp;
      ptmp:=l.suiv.p;
      p_tmp:=l.suiv;
      l.suiv:=new liste_point;
      l.suiv.p:=Sommet_Oppose((ptmp,l.p),t1);
      l.suiv.suiv:=p_tmp;
      convexe_concave(l.suiv,centre, t,l_a, repere,modif);
    End if;
  End if;
End Convexe_concave;
  ------------------------------------------------------------------------
  --
Function CONVEXE(p1,p2,p3,c	: point_type) return boolean is
  a1,a2	: float;
Begin  
a1:=Geometrie.Angle_3Points(p1,p2,p3);
  a2:=Geometrie.Angle_3Points(p1,p2,c);
  If (a1*a2>0.0) and (abs(a1)>abs(a2))  Then
  return true;
  Else return false;
  End if;
End CONVEXE;
--------------------------------------------------------------------------
Procedure EC(	L	: in out a_liste_point;
		Centre	: point_type;
		T	: a_liste_triangle;
		L_a	: in out a_liste_point;
		repere	: in out point_type;
		Modif	: in out boolean) is
P		: point_type;
begin
  If (l.suiv.p/=repere) Then
  EC(l.suiv,centre,t,l_a,repere,modif);
  End if;
    convexe_concave(l,centre,t,l_a,repere,modif);
End Ec;
--------------------------------------------------------------------------
Procedure EC_TOTALE(    L	: in out a_liste_point;
			Centre	: point_type;
			T	: a_liste_triangle;
			L_a	: in out a_liste_point) is
tmp		: a_liste_point;
 modifie	: boolean:=true;
 pt_repere	: point_type;
-------------------------------------------------------
procedure decroche_boucle_points(lt	: in out a_liste_point;
				 pt	: point_type) is
Begin
  If (lt/=null) and then (lt.suiv/=null) then 
  If lt.suiv.p=pt    Then
  lt.suiv:=null;
    Else decroche_boucle_points(lt.suiv,pt);
    End if;
  End if;
End decroche_boucle_points;
---------------------------
Begin 
pt_repere:=l.p;
 tmp:=dernier_point(l);
 tmp.suiv:=l;
 Text_io.put_line("Recherche d'une EC");
 While modifie loop   text_io.put("Recherche");
  	int_io.put(pt_repere.coor_x);
   int_io.put(pt_repere.coor_y);
text_io.put_line("");
   modifie:=false;
   EC(l,centre,t,l_a,pt_repere,modifie);
 End Loop;
 decroche_boucle_points(l,pt_repere);
End EC_TOTALE;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Topologie-----------------------------------------------
---------------------------------------------------------------------------
------------------------
Function point_appartient_triangle(	T	: triangle;
					p	: Point_type) Return Boolean is
Begin
  if (t.p1=p) or (t.p2=p) or (t.p3=p)   then
  Return true;
  else Return false;
  end if;
End point_appartient_triangle;
        --------------------------------------------------------------------------
Function Arete_appartient_triangle(	a	: arete;
					T	: triangle) Return boolean is
begin
  If Egale(a,(T.p1,T.p2)) or      Egale(a,(T.p1,T.p3)) or     Egale(a,(T.p2,T.p3))   then
  Return True;
  else Return False;
  end If;
End Arete_appartient_triangle;
--------------------------------------------------------------------------
Function Adjacent(T1	: Triangle;
		  T2	: triangle) Return arete is
Begin
  If Arete_appartient_triangle((T1.p1,T1.p2),T2) then
  Return (T1.p1,T1.p2);
  elsif Arete_appartient_triangle((T1.p2,T1.p3),T2) then
  Return (T1.p2,T1.p3);
  elsif Arete_appartient_triangle((T1.p1,T1.p3),T2) then
  Return (T1.p1,T1.p3);
  else
  Return (T1.p1,T1.p1);
  end if;
End Adjacent;
--------------------------------------------------------------------------
Function Cote_oppose(t	: triangle;
		     p	: point_type) Return arete is
Begin
    if p=t.p1 then
	Return (t.p2,t.p3);
    elsif p=t.p2 then
	Return (t.p1,t.p3);
    elsif p=t.p3 then
	Return (t.p1,t.p2);
    else      Return (p,p);
    end if;
end Cote_oppose;
--------------------------------------------------------------------------
Function Sommet_oppose(	a	: arete;
			T	: triangle) Return point_type is
begin
  If Egale(a,(T.p1,T.p2)) then    Return t.p3;
   Elsif Egale(a,(T.p1,T.p3)) then    Return t.p2;
   Elsif Egale(a,(T.p2,T.p3)) then    Return t.p1;
  else    Return a.p1;
  end If;
End Sommet_oppose;
--------------------------------------------------------------------------
Procedure Extrait_triangles_contenant_point(					p	: point_type;
				T	: a_liste_triangle;
				T_out	: in out a_liste_triangle) is          
Begin
  If t_out/=Null then
  Extrait_triangles_contenant_point(p,t,t_out.suiv);
  Else
  If t/=null then
  If point_appartient_triangle(t.tr,p) then
  Ajout_Triangle(t_out,t.tr);
      End if;
      Extrait_triangles_contenant_point(p,t.suiv,t_out);
    end if;
  End if;
End Extrait_triangles_contenant_point;
--------------------------------------------------------------------------
Procedure Extrait_triangles_contenant_arete(	a	: arete;
				 	    	T	: a_liste_triangle;
				 	 	t_out	: in out 							a_liste_triangle) is
Begin
  if T/=null then
  if Arete_appartient_triangle(a,T.tr)then
  Ajout_Triangle(T_out,T.tr);
    end if;
    extrait_triangles_contenant_Arete(a,T.suiv,T_out);
  end if;
End Extrait_triangles_contenant_arete;
--------------------------------------------------------------------------
Function Triangle_oppose(T	: triangle;
			 P	: point_type;
			 L_T	: a_liste_triangle) Return triangle is
a	: arete;
 t_o	: a_liste_triangle;
Begin
  if point_appartient_triangle(t,p) then
  a:=cote_oppose(t,p);
    extrait_triangles_contenant_arete(a,l_t,t_o);
    --Pour delaunay, il y a au max
	--2 triangles qui contiennent une arete.
    if point_appartient_triangle(t_o.tr,p) then
	if t_o.suiv/=null then         Return t_o.suiv.tr;
      else        Return (p,p,p);
      end if;
    else      Return t_o.tr;
    end if;
  else    Return (p,p,p);
  end if;
End Triangle_oppose;
----------------------------------------------------------
Procedure triangles_opposes(	l_tri	: a_liste_triangle;
				l_t_p	: a_liste_triangle;
				p	: point_type;
			    	l_out	: in out a_liste_triangle) is
tri	: triangle;
begin
  if l_t_p/=null then
  tri:=triangle_oppose(l_t_p.tr,p,l_tri);
    if tri.p1/=tri.p2 then
	ajout_triangle(l_out,tri);
    end if;
    triangles_opposes(l_tri,l_t_p.suiv,p,l_out);
  end if;
End triangles_opposes;
--------------------------------------------------------------------------
Procedure triangles_voisins_de_point(T	: a_liste_triangle;
				     p	: point_type;
				     t_out	: in out a_liste_triangle) is
T_tmp	: a_liste_triangle;
begin
  Extrait_triangles_contenant_point(p,T,T_tmp);
  Triangles_opposes(T,t_tmp,p,t_out);
End triangles_voisins_de_point;
          --------------------------------------------------------------------------
procedure colle_triangle(t1	: in out a_liste_triangle;
			 t2	: in out a_liste_triangle) is
tmp	: triangle;
a	: arete;
begin
if t1/=null then  if t1.suiv/=null then
    if t2/=null then
	a:=adjacent(t1.tr,t2.tr);
      if a.p1/=a.p2 then
	  tmp:=t1.suiv.tr;
        t1.suiv.tr:=t2.tr;
        t2.tr:=tmp;
      else
	  colle_triangle(t1,t2.suiv);
      end if;
    end if;
  end if;
end if;
End colle_triangle;
--------------------------------------------------------------------------
--Range les triangles par face commune. 
Procedure Range_triangles(l_t	: in out a_liste_triangle) is
Begin
  if l_t.suiv/=null then
  colle_triangle(l_t,l_t.suiv);
    range_triangles(l_t.suiv);
  end if;
End Range_triangles;
--------------------------------------------------------------------------
Function triangle_complementaire( t1	: triangle;
				  t2	: triangle;
				  p	: point_type) Return triangle is
a	: arete;
Convexe	: boolean;
p_tmp	: point_type;
Begin
  a:=adjacent(t1,t2);
  if a.p1/=a.p2 then
  Geometrie.intersection_de_lignes(sommet_oppose(a,t1),sommet_oppose(a,t2),
  a.p1,a.p2,C_SEGMENT_TYPE,convexe,p_tmp);
    if convexe then
	if a.p1=p then
	Return (sommet_oppose(a,t1),sommet_oppose(a,t2),a.p2);
      else
	  Return (sommet_oppose(a,t1),sommet_oppose(a,t2),a.p1);
      end if;
    else     Return (p,p,p);
    end if;
  else   Return (p,p,p);
  end if;
End Triangle_complementaire;
--------------------------------------------------------------------------
Procedure Recherche_triangles_complementaires( lt	: a_liste_triangle;
						p	: point_type;
				     		l_o	: in out a_liste_triangle					     ) is 
T	: triangle;
Tmp	: a_liste_triangle;
------------------------------------------------
Procedure triangles_complementaires( 	lt	: a_liste_triangle;
					p	: point_type;
				     	lo	: in out a_liste_triangle) is
tmp	:a_liste_triangle;
  T	: triangle;
Begin
  if lt/=null then
  if lt.suiv/=null then
  T:=triangle_complementaire(lt.tr,lt.suiv.tr,p);
      if T.p1/=T.p2 then
	  ajout_triangle(lo,T);
      end if;
      triangles_complementaires(lt.suiv,p,lo);
    else
	tmp:=lo;
      lo:=new liste_triangle;
      lo.tr:=lt.tr;
      lo.suiv:=tmp;
    end if;
  end if;
End triangles_complementaires;
------------------------------------------------
Begin
  Triangles_complementaires(lt,p,l_o);
  T:=triangle_complementaire(lt.tr,l_o.tr,p);
  if t.p1/=t.p2 then    l_o.tr:=t;
  else    tmp:=l_o;
    l_o:=l_o.suiv;
    recupere_t(tmp);
  end if;
End Recherche_triangles_complementaires;
-----------------------------------------------------------
Procedure POINTS_VOISINS(Lt	: a_Liste_triangle;
			 p	: point_type;
			 T_Tmp	: in out a_liste_triangle;
			 Tab	: in out point_liste_type;
 			 Nb	: in out integer) is  
Procedure Range_points(L	: a_liste_triangle;
    			P	: point_type;
			 			 Tab	: in out point_liste_type;
 			 Nb	: in out integer) is     
a			: arete;
     P1_EST_NOUVEAU,     P2_EST_NOUVEAU	: boolean:=true;
    Begin
	if L/=null then
	a:=cote_oppose(l.tr,p);
        if a.p1/=p then
		if nb=0 then
		nb:=nb+1;
 tab(nb):=a.p1;
            nb:=nb+1;
 tab(nb):=a.p2;
          end if;
          for i in 1..nb loop
		  if tab(i)=a.p1 then
		  P1_EST_NOUVEAU:=false;
 Exit;
	    end if;
          end loop;
          if P1_EST_NOUVEAU then
		  nb:=nb+1;
 Tab(nb):=a.p1;
          end if;
          for i in 1..nb loop
		  if tab(i)=a.p2 then
		  P2_EST_NOUVEAU:=false;
 Exit;
	    end if;
          end loop;
          if P2_EST_NOUVEAU  then
		  nb:=nb+1;
tab(nb):=a.p2;
          end if;
          end if;
        Range_points(l.suiv,p,tab,nb);
     end if;
  End Range_points;
  
Begin  
Extrait_triangles_contenant_point(p,Lt,T_tmp);
      nb:=0;
  range_points(t_tmp,p,tab,nb);
End POINTS_VOISINS;
-----------------------------------------------------------
Procedure POINTS_VOISINS_CD(Lt	: a_Liste_triangle;
			 p	: point_type;
			 Tab	: in out point_liste_type;
 			 Nb	: in out integer) is
T_Tmp	:  a_liste_triangle;
  -----------------------------------------------------------  
  Procedure Range_points(L	: a_liste_triangle;
    			 P	: point_type;
			 			 Tab	: in out point_liste_type;
 			 Nb	: in out integer) is
a			: arete;
      P1_EST_NOUVEAU	: boolean:=true;
      P2_EST_NOUVEAU	: boolean:=true;
    Begin
	If L/=null then
	a:=cote_oppose(l.tr,p);
	If a.p1=a.p2 then
	Text_Io.put_line("pas de cote oppose");
	End IF;
        If a.p1/=p then
		if nb=0 then
		nb:=nb+1;
tab(nb):=a.p1;
            nb:=nb+1;
tab(nb):=a.p2;
          end if;
          for i in 1..nb loop
		  if tab(i)=a.p1 then
		  P1_EST_NOUVEAU:=False;
Exit;
	    end if;
          end loop;
          if P1_EST_NOUVEAU then
		  nb:=nb+1;
Tab(nb):=a.p1;
          end if;
          for i in 1..nb loop
		  if tab(i)=a.p2 then
		  P2_EST_NOUVEAU:=false;
 Exit;
	    end if;
          end loop;
          if P2_EST_NOUVEAU then 
		  nb:=nb+1;
 Tab(nb):=a.p2;
          end if;
          End if;
        Range_points(l.suiv,p,tab,nb);
      End if;
  End Range_points;
  
Begin  
Extrait_triangles_contenant_point(p,Lt,T_tmp);
      nb:=0;
  range_points(t_tmp,p,tab,nb);
  Vide(T_tmp);
End POINTS_VOISINS_CD;
------------------------------------------------------------------------
Procedure range_point_unique(lp	: in out a_liste_point;
			     p	: point_type) is
begin
  if lp/=null then
  if lp.p/=p then
  range_point_unique(lp.suiv,p);
    end if;
  else
  lp:=new liste_point;
    lp.p:=p;
  end if;
End range_point_unique;
------------------------------------------------------------------------
Procedure Extrait_points_voisins(	lt	: a_liste_triangle;
					p	: point_type;
					lp	: in out a_liste_point) is
a: arete;
---------------------------------------------------------
Procedure Range_point(  lt	: a_liste_triangle;
			p	: point_type;
			lp	:in out  a_liste_point) is
a	: arete;
Begin
  if lt/=null then
  if lt.suiv/=null then
  a:=adjacent(lt.tr,lt.suiv.tr);
      if a.p1=a.p2 then
	  Text_Io.put_line("putain, pas adjacent");
      else
      if a.p1=p then
	  ajout_point(lp,a.p2);
      else        ajout_point(lp,a.p1);
      end if;
    end if;
    range_point(lt.suiv,p,lp);
  else    a:=cote_oppose(lt.tr,p);
    range_point_unique(lp,a.p1);
          range_point_unique(lp,a.p2);
  end if;
  end if;
End Range_point;
  ----------------
  begin
  if lt/=null then
  if lt.suiv/=null then
  a:=adjacent(lt.tr,lt.suiv.tr);
      if a.p1=a.p2 then
	  Text_Io.put_line("pas adjacent");
      else        ajout_point(lp,sommet_oppose(a,lt.tr));
        range_point(lt,p,lp);
      end if;
    else      ajout_point(lp,lt.tr.p1);
      ajout_point(lp,lt.tr.p2);
      ajout_point(lp,lt.tr.p3);
    end if;
  end if;
End Extrait_points_voisins;
------------------------------------------------------------------------
Procedure POINTS_VOISINS(Lt	: a_Liste_triangle;
			 p	: point_type;
			 T_Tmp	: in out a_liste_triangle;
			 Lp	: in out a_liste_point) is
Begin
  Extrait_triangles_contenant_point(p,Lt,T_tmp);
  Range_triangles(T_tmp);
  Extrait_points_voisins(T_tmp,p,lp);
End Points_voisins;
-- ***********************************************************************
-- ***********************************************************************
-- ***********************************************************************
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- Ancien package Delaunay------------------------------------------------
--------------------------------------------------------------------------
-------------------------
Procedure Stocker_aretes(l_a	: in out a_liste_aretes;
			 T	: Triangle			) is
Begin
  if l_a=null then
  l_a:=new Liste_aretes;
    l_a.a.p1:=t.p1;
    l_a.a.p2:=t.p2;
    l_a.suiv:=new Liste_aretes;
    l_a.suiv.a.p1:=t.p1;
    l_a.suiv.a.p2:=t.p3;
    l_a.suiv.suiv:=new Liste_aretes;
    l_a.suiv.suiv.a.p1:=t.p3;
    l_a.suiv.suiv.a.p2:=t.p2;
  else
  Stocker_aretes(l_a.suiv,t);
  end if;
End Stocker_aretes;
--------------------------------------------------------------------------
Procedure Ajout_point_Triangulation(triangles	: in out a_liste_triangle;
				    P		: point_type				   ) is
aretes	: a_liste_aretes;
  tmp		: a_liste_triangle;
  nb		: Integer;
Begin
  aretes:=new liste_aretes'(((0,0),(0,0)),null);
  tmp:=triangles;
  triangles:=new liste_triangle;
  triangles.suiv:=tmp;
  Extraire_triangles(Triangles,p,Aretes);
  nb:=compte(triangles);
  vire_les_doublons(Aretes);
  creer_nouveaux_triangles(triangles,aretes.suiv,p);
  tmp:=triangles;
  triangles:=triangles.suiv;
  recupere_t(tmp);
  if compte(triangles)=nb then
  Text_io.put_line(" Et merde, il a pas rajoute de triangles");
  End If;
  vide(Aretes);
End Ajout_point_triangulation;
--------------------------------------------------------------------------
Procedure Extraire_triangles(L	: in out a_liste_triangle;
			     p	: point_type;
			     a	: in out a_liste_aretes	      			    ) is
Begin
   if L/=null then
   if L.suiv/=null then
   Extraire_triangles(l.suiv, p, a);
      If  Not(Position_point_cercle(l.suiv.all,p)=Hors_du_cercle) then
	  stocker_aretes(a,l.suiv.tr);
        Enlever_triangle_suivant(l);
      End if;
    end if;
  end if;
End Extraire_triangles;
      -------------------------------------------------------------------------
	  -- Fonction qui determine la position d'un point M par rapport a un cercle 
	  -- decrit par son centre et son rayon
	  -------------------------------------------------------------------------
Function Position_point_cercle(Centre : Point_type_reel;
			       Rayon2  : float;
			       M      : Point_type			       ) return Type_position_point_cercle is 
CM : float :=(Centre.coor_X-Float(M.coor_X))*(Centre.coor_X-Float(M.coor_X))+
	      (Centre.coor_y-Float(M.coor_y))*(Centre.coor_y-Float(M.coor_y));
Begin  
If abs(CM-Rayon2) < 0.01 then 
--    If Not(CM=RAYON2) then 
--      Text_io.Put_line("*** Attention arrondi pour points cocycliques ****");
--    End If;
    return Sur_le_cercle;
  Elsif CM < Rayon2 then
  return Dans_le_cercle;
  Else
  return Hors_du_cercle;
  End if;
End Position_point_cercle;
-------------------------------------------------------------------------
-- Fonction qui determine la position d'un point M par rapport a un cercle 
-- circonscrit a un tr ABC quasi_plat en B, decrit par les deux points A 
-- et C et les sinus et cosinus de l'angle en B compris entre 0 et PI  
-------------------------------------------------------------------------
Function Position_point_cercle(A,C     : Point_type;
                               sinBABC,			       cosBABC : float;
			       M       : Point_type			       ) return Type_position_point_cercle is  
XMA :float;
  XMC :float;
  YMA :float;
  YMC :float;
  MA  :float;
   MC  :float;
   sinMAMC :float;
  cosMAMC :float;
Begin
  If ((M=A) or(M=C)) then
  return Sur_le_cercle;
  End if;
  XMA :=Float(A.coor_X-M.coor_X);
  XMC :=Float(C.coor_X-M.coor_X);
  YMA :=Float(A.coor_y-M.coor_y);
  YMC :=Float(C.coor_y-M.coor_y);
  MA  :=Geometrie.Norme(M,A);
   MC  :=Geometrie.Norme(M,C);
   sinMAMC := (XMA*YMC-YMA*XMC)/(MA*MC);
  -- sin(MA,MC)
  cosMAMC := (XMA*XMC+YMA*YMC)/(MA*MC);
  -- cos(MA,MC)  
  If (((cosBABC=cosMAMC) and (sinBABC=sinMAMC))
  or ((cosBABC=-cosMAMC) and (sinBABC=-sinMAMC))) then
  return Sur_le_cercle;
  elsif ((sinMAMC<-sinBABC) or (cosMAMC<=cosBABC)) then
  return Dans_le_cercle;
  else     return Hors_du_cercle;
  End if;
  End Position_point_cercle;
-------------------------------------------------------------------------
-- Fonction qui renvoie la position d'un point M par rapport au cercle 
-- circonscrit au triangle TR --------------------------------------------
------------------------------------------------------------------------------------------------------
Function Position_point_cercle(LT : Liste_Triangle;
			       M  : Point_type			       ) return Type_position_point_cercle is
Begin 
If LT.Cat=Normal then
   return Position_point_cercle(LT.Centre,LT.Rayon2,M);
 Else
 If LT.Cat=Quasi_plat then
 return Position_point_cercle(LT.A,LT.C,LT.sinBABC,LT.cosBABC, M);
   Else
   return Dans_le_cercle;
   End if;
 End if;
End Position_point_cercle;
--------------------------------------------------------------------------
Procedure Creer_nouveaux_triangles(lt	: in out a_liste_triangle;
				   L_a	: in out a_liste_aretes;
				   p	: point_type				  ) is
Begin
  if Lt/=null then
  Creer_nouveaux_triangles(Lt.suiv,L_a,p);
  else
  if l_a/=null then
  ajout_triangle(Lt, (l_A.a.p1, l_a.a.p2, p));
      Creer_nouveaux_triangles(Lt.suiv,L_a.suiv,p);
      vide(l_a);
    end if;
  end if;
end Creer_nouveaux_triangles;
--------------------------------------------------------------------------
-- Procedure de Triangulation d'une enveloppe convexe
-- Les affichages en commentaire sont la pour reperer un probleme du a
-- l'arrondi du rayon du cercle circonscrit pour les pts cocycliques
-- (arrondi dans les procedures Delaunay.Position_point_cercle)
--------------------------------------------------------------------------
Procedure TEC(Tab	: point_liste_type;
	     i		: integer;
	     j		: integer;
	     l_t	: in out a_liste_triangle) is  
Position   	: Type_position_point_cercle;
  Tr_ijk     	: triangle;
  On_garde_k 	: boolean;
  Etape_terminee: boolean:=false;
  cocyclik   	: boolean:=false;
  k_cocyclik 	: integer;
  Cat        	: Type_categorie_triangle;
  A,C       	: Point_type;
  Centre     	: Point_type_reel;
  Rayon2,  sinBABC,  cosBABC    	: float;
Begin
If i+1<j then  
For k in i+1..j-1 loop
    If Etape_terminee then Exit;
 -- Sort de la boucle k 
	End if;
	Tr_ijk := (tab(i),tab(j),tab(k));
    Accessoires_triangle(Tr_ijk,Cat,Centre,Rayon2,A,C,sinBABC,cosBABC);
    If (Cat=Plat)       then 
	On_garde_k :=false; --Triangle plat
		Else                       --Triangle normal ou quasi_plat      
			On_garde_k := true;
		If (Cat=Normal) then
			--On va utiliser Centre et Rayon      
			For l in tab'range loop
				If (l/=k) and (l/=i) and (l/=j) then
					Position := Position_point_cercle(Centre,Rayon2,tab(l));
					If (Position=Sur_le_cercle) then
						If not(cocyclik) then
							cocyclik:=true;--On note qu'il y a des pts cocycliques
							k_cocyclik:=k;--et on se reserve le pt tab(k), qu'on 
						End if;--recuperera si aucun tab(l) Dans_le_cercle              
						On_garde_k:=false;
					Elsif (Position=Dans_le_cercle) then 
						On_garde_k:=false;
						If k_cocyclik=k then
							cocyclik:=false; --On abandonne definitivement le pt tab(k)
						End if;-- "k_cocyclik=k"
						Exit;--Boucle "For l"            
					End if;--Position
				End if;-- "l/=ijk"
			End loop; --For l
		Else         --triangle quasi-plat, on va utiliser A,C,sinBABC,cosBABC
      	For l in tab'range loop
		If (l/=k) and (l/=i) and (l/=j) then	
			Position := Position_point_cercle(A,C,sinBABC,cosBABC,tab(l));
            If (Position=Sur_le_cercle) then
				If not(cocyclik) then
					cocyclik:=true;--On note qu'il y a des pts cocycliques
					k_cocyclik:=k;--et on se reserve le pt tab(k), qu'on		
					On_garde_k:=false;--recup. si aucun tab(l) Dans_le_cercle
				End if;
            Elsif (Position=Dans_le_cercle) then
				On_garde_k:=false;
				If k_cocyclik=k then
					cocyclik:=false;--On abandonne definitivement le pt tab(k)
				End if;--"k_cocyclik=k"
				Exit;--Boucle "For l"
            End if;--Position
			End if;-- "l/=ijk"
		End loop;--For l
		End if;-- Cat=normal ou quasi-plat
	End if;-- Cat=plat
	If On_garde_k then
	Cocyclik:=false;-- Le point tab(k_cocyclik) ne sert plus a rien
		Etape_terminee:=true;
      Ajout_triangle(l_t,Tr_ijk);
      TEC(tab,i,k,l_t);
      TEC(tab,k,j,l_t);
    End if;
 --On_garde_k
 End loop;--For k
 If cocyclik then 
 -- Il n'y avait pas de meilleur point que tab(k_cocyclik) pour     
 -- continuer la triangulation : on le recupere...    
 Cocyclik:=false;
    Etape_terminee:=true;
    Ajout_triangle(l_t,(tab(i),tab(j),tab(k_cocyclik)));
    TEC(tab,i,k_cocyclik,l_t);
    TEC(tab,k_cocyclik,j,l_t);
  End if;
 --cocyclik
 End if;
 --"i+1<j"
 End TEC;
--------------------------------------------------------------------------
Procedure Delaunay_Tsai(Semi_ini	: point_liste_type;
			L		: in out a_liste_triangle;
			pts_convex	: integer:=0) is  
Semi		: point_liste_type(Semi_ini'range):=Semi_ini;
  nb_sommets_ec	: integer:=0;
  Ds		: Integer;
Begin
  if semi'length>=3 then
  if semi'length=3 then
  l:=new liste_triangle;
      l.tr:=(semi(1),semi(2),semi(3));
      Accessoires_triangle(L.ALL.tr, L.ALL.Cat, L.ALL.Centre,  L.ALL.Rayon2,
	  L.ALL.A, L.ALL.C, L.ALL.SinBABC, L.ALL.CosBABC);
    else      --Semi modifie : les nb_sommets_ec
	If pts_Convex=0 	--Met les nb_sommets_ec en haut du tableau
	then Ensemble_Convexe(Semi, nb_sommets_Ec);
        Else Ensemble_Convexe(Semi(1..pts_convex), nb_sommets_Ec);
      End If;
      TEC(semi(1..nb_sommets_ec),1,nb_sommets_Ec,L);
      for g in nb_sommets_Ec+1..Semi'last loop
	  Ajout_point_Triangulation(L,semi(g));
      end loop;
    end if;
  end if;
End Delaunay_Tsai;
End Delaunay;
