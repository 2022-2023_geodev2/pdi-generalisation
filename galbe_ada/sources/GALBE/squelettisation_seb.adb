-------------------------------------------------------------------
-- SQUELETTISATION
-------------------------------------------------------------------
-- Package permettant la construction d'un Squelette de Surface
-------------------------------------------------------------------
-- Sebastien Mustiere
-- Jan 98
-------------------------------------------------------------------

-- Librairie COGIT
With Geometrie; use Geometrie;
with Lissage_filtrage;
-- Librairies Mathieu
with Delaunay; use Delaunay;
-- Librairie Hergott
with PArbre; use PArbre;


package body Squelettisation_Seb is

Type Type_voisins is array(1..3) of a_liste_triangle;
Type Type_milieux is array(1..3) of Point_type;

------------------------------------
-- ENLEVER_TRIANGLE
------------------------------------
-- enleve T_a_enlever de la liste et libere sa place memoire
------------------------------------
Procedure Enlever_triangle(T_liste     : in out a_liste_triangle;  
                           T_a_enlever : in out a_liste_triangle) is

T_cur : a_liste_triangle;
begin
  if T_a_enlever = null then
    return;
  end if;
  -- si le triangle a enlever est le premier
  if T_liste = T_a_enlever then
    T_liste := T_liste.suiv;
    recupere_t(T_a_enlever);
    return;
  end if; 
  -- sinon
  T_cur := T_liste;
  while T_cur /= null loop
    if T_cur.suiv = T_a_enlever then
      T_cur.suiv := T_a_enlever.suiv;
      recupere_t(T_a_enlever);
      return;
    end if;
    T_cur := T_cur.suiv;
  end loop;
  -- si on passe la c'est que le triangle ne faisait pas partie de la liste
end Enlever_triangle;


------------------------------------
-- TRIANGLES_VOISINS
------------------------------------
-- Retourne le nombre de triangles de la liste adjacents (cote commun)
-- a un triangle donne, ces triangles et le milieux des cotes communs 
-- correspondants 
------------------------------------
Procedure Triangles_voisins(Liste_trg,
                            Trg       : in a_liste_triangle;
                            Nbvoisins : out natural;
                            Voisins   : out Type_voisins;
                            Milieux   : out Type_milieux ) is
           

Pc,
Pt      : Triangle;
Current : a_liste_triangle;
N       : natural := 0;

begin
  Pt := Trg.Tr;
  Current := Liste_Trg;
  while Current /= null and N /= 2 loop
    Pc := Current.Tr;
    if Current = Trg then
      null;
    elsif (Pc.p1 = Pt.p1 and then Pc.p2 = Pt.p2) or else
       (Pc.p1 = Pt.p2 and then Pc.p2 = Pt.p1) or else
       (Pc.p1 = Pt.p1 and then Pc.p2 = Pt.p3) or else
       (Pc.p1 = Pt.p3 and then Pc.p2 = Pt.p1) or else
       (Pc.p1 = Pt.p2 and then Pc.p2 = Pt.p3) or else
       (Pc.p1 = Pt.p3 and then Pc.p2 = Pt.p2) then
      N := N+1;
      Voisins(N) := Current;
      Milieux(N):=((Pc.p1.coor_X+Pc.p2.coor_X)/2,(Pc.p1.coor_Y+Pc.p2.coor_Y)/2);
    elsif (Pc.p1 = Pt.p1 and then Pc.p3 = Pt.p2) or else
       (Pc.p1 = Pt.p2 and then Pc.p3 = Pt.p1) or else
       (Pc.p1 = Pt.p1 and then Pc.p3 = Pt.p3) or else
       (Pc.p1 = Pt.p3 and then Pc.p3 = Pt.p1) or else
       (Pc.p1 = Pt.p2 and then Pc.p3 = Pt.p3) or else
       (Pc.p1 = Pt.p3 and then Pc.p3 = Pt.p2) then
      N := N+1;
      Voisins(N) := Current;
      Milieux(N):=((Pc.p1.coor_X+Pc.p3.coor_X)/2,(Pc.p1.coor_Y+Pc.p3.coor_Y)/2);
    elsif (Pc.p2 = Pt.p1 and then Pc.p3 = Pt.p2) or else
       (Pc.p2 = Pt.p2 and then Pc.p3 = Pt.p1) or else
       (Pc.p2 = Pt.p1 and then Pc.p3 = Pt.p3) or else
       (Pc.p2 = Pt.p3 and then Pc.p3 = Pt.p1) or else
       (Pc.p2 = Pt.p2 and then Pc.p3 = Pt.p3) or else
       (Pc.p2 = Pt.p3 and then Pc.p3 = Pt.p2) then
      N := N+1;
      Voisins(N) := Current;
      Milieux(N):=((Pc.p2.coor_X+Pc.p3.coor_X)/2,(Pc.p2.coor_Y+Pc.p3.coor_Y)/2);
    end if;
    Current := Current.suiv;
  end loop;
  Nbvoisins := N;

end Triangles_voisins;


------------------------------------
-- TRIANGLES_TO_SQUELETTE
------------------------------------
-- PROCEDURE INTERNE
------------------------------------
-- en entree : une liste de Triangles partitionnant l'INTERIEUR d'une surface
-- en sortie : le squelette de cette surface
------------------------------------
function Triangles_to_squelette(Liste_trg : a_liste_triangle)
                                return arbre is

Max_pts    : constant natural := Compte(Liste_trg);
Squel      : arbre;
Liste_trg2 : a_liste_triangle := null;
Nvx_points : Point_liste_type(1..Max_Pts);
Nvx_points_access : point_access_type;
N          : natural;

------------------------------------
-- BRANCHE_MILIEUX
------------------------------------
-- PROCEDURE INTERNE A TRIANGLES_TO_SQUELETTE
------------------------------------
-- Procedure recursive permettant de construire branche par branche le squelette
-- a partir de la triangulation en reliant les milieux des cotes
-- Aux jonctions (triangle ayant 3 voisins) la connection est faite au
-- barycentre du triangle
-- Aux extremites (triangle ayant 1 voisin), on s'arrete au milieu du cote
-- commun avec le voisin
------------------------------------
procedure Branche_milieux(Trg : in triangle;
                          Noeud_ini   : in natural) is

Trg_current : a_liste_triangle;
Noeud_fin, 
arc        : positive;
Ptfin      : point_type;
Voisins    : Type_voisins;
Nbvoisins  : natural;
Milieux    : Type_milieux;
Triangle_present : boolean := false;

begin
--  if Trg_current = null then 
--    return;
--  end if;
  Trg_current := Liste_trg2;
  while Trg_current /= null loop
    if Trg = Trg_current.tr then
      Triangle_present := true; 
      exit;
    end if;
    Trg_current := Trg_current.suiv;
  end loop;
  if not(Triangle_present) then
    return;
  end if;

  loop
    Triangles_voisins(Liste_trg2,Trg_current,Nbvoisins,Voisins,Milieux);
    -- s'il n'y a pas de triangle voisin : fin de branche
    if Nbvoisins = 0 then
      -- creer un noeud
      creer_noeud(squel, Noeud_fin);
      -- creer l'arc courant
      Nvx_points_access := new Point_liste_type(1..N);
      Nvx_points_access(1..N) := Nvx_points(1..N);
      creer_arc(squel, Noeud_ini, Noeud_fin, Nvx_points_access, arc);
      -- enlever le triangle courant
      enlever_triangle(Liste_trg2, Trg_current);
      exit;
    end if;

    -- si il y a 1 seul voisin : on continue dans la progressiom
    if Nbvoisins = 1 then
      -- ajouter le milieu du triangle commun
      N := N + 1;
      Nvx_points(N) := Milieux(1);
      -- enlever le triangle courant
      Enlever_triangle(Liste_trg2,Trg_current);
      -- le courant devient le suivant
      Trg_current := Voisins(1);
    end if;

    -- si il y a 2 voisins : biffurcation
    if Nbvoisins = 2 then
      -- ajouter le barycentre du triangle
      N := N + 1;
      Ptfin := Geometrie.Point_moyen((Trg_current.Tr.p1,Trg_current.Tr.p2,
                                      Trg_current.Tr.p3));
      Nvx_points(N) := Ptfin;
      -- creer un noeud
      creer_noeud(squel, Noeud_fin);
      -- creer l'arc courant
      Nvx_points_access := new Point_liste_type(1..N);
      Nvx_points_access(1..N) := Nvx_points(1..N);
      creer_arc(squel, Noeud_ini, Noeud_fin, Nvx_points_access, arc);
      Enlever_triangle(Liste_trg2, Trg_current);
      -- relancer la procedure avec comme triangle courant successivement
      -- les deux voisins
      N:=2;
      Nvx_points(1) := Ptfin;
      Nvx_points(2) := Milieux(1);
      Branche_milieux(Voisins(1).Tr, Noeud_fin);
      N:=2;
      Nvx_points(1) := Ptfin;
      Nvx_points(2) := Milieux(2);
      Branche_milieux(Voisins(2).Tr, Noeud_fin);
      exit;
    end if;
  end loop;


end Branche_milieux;

------------------------------------
-- TRIANGLES_TO_SQUELETTE
------------------------------------
-- CORPS DE LA fonction
-- Triangles_to_squelette(Liste_trg : a_liste_triangle)
--                        return arbre is
-- squel      : arbre;
-- Liste_trg2 : a_liste_triangle := null;
-- racine     : positive;
-- Max_pts    : natural := Compte(Liste_trg);
------------------------------------
begin
  declare 
    Trgcur : a_liste_triangle;
    Trg : Triangle;
    Nbvoisins : natural;
    Voisins : Type_voisins;
    Milieux : Type_milieux;
  begin
  -- copie de la liste des triangles Liste_trg dans Liste_trg2
    Trgcur := Liste_trg;
    while Trgcur /= null loop
      Ajout_triangle(Liste_trg2,Trgcur.Tr);
      Trgcur := Trgcur.suiv;
    end loop;

  -- classement de l'ordre des triangles (pour optimisation des recherches)
--  Topologie.Range_triangles(Liste_trg2);

  -- On veut commencer avec un triangle extremite
    Trgcur := Liste_trg2;
    while trgcur /= null loop  
      Triangles_voisins(Liste_trg2, Trgcur, Nbvoisins, Voisins, Milieux);
      if Nbvoisins = 1 then
        Trg := Liste_trg2.Tr;
        Liste_trg2.Tr := Trgcur.Tr;
        Trgcur.Tr := Trg;
        exit;
      end if;
      Trgcur := Trgcur.suiv;
    end loop;
  end;

  -- fabrication du squelette vide
  vide_arbre(Squel);

  -- On part du premier triangle
  declare
    racine : positive;
    Nbvoisins : natural;
    Milieux : Type_milieux;
    Voisins : Type_voisins;
    Trg_cur : a_liste_triangle := Liste_trg2;
  begin
    -- NB: dans les arbres definis dans le package Parbre, un noeud n'a pas
    -- de coordonnees, c'est juste un numero identifiant
    creer_noeud(Squel, racine);
    Triangles_voisins(Liste_trg2,Trg_cur,Nbvoisins,Voisins,Milieux);
    Enlever_triangle(Liste_trg2,Trg_cur);
    N := 1;
    Nvx_points(1) := Milieux(1);
    Branche_milieux(Voisins(1).Tr,racine);
    vide(Liste_trg2); 
  end;

  return Squel;

end Triangles_to_squelette;


------------------------------------
-- TRIANGLE_DANS_SURFACE
------------------------------------
-- PROCEDURE INTERNE
------------------------------------
-- Triangle dans surface =
-- Le barycentre triangle est il dans la surface?
-- NB:
-- le triangle en entree est suppose etre entierement en dedans ou en 
-- dehors de la surface
-- La liste des points en entree est supposee delimiter une surface sans trous
------------------------------------
function Triangle_dans_surface( Ligne : in Point_liste_type;
				Trg   : in Triangle) return boolean is

Surface	 : Surf_type(1, Ligne'length);
Bary     : Point_type_reel;

begin
  Surface.ne := 1;
  Surface.na(1) := Ligne'length;
  Surface.pts := Ligne;
  Bary := Geometrie.Point_moyen(Reel((Trg.p1,Trg.p2,Trg.p3)));

  if Geometrie.Est_il_dans_surface(Bary, Surface) then
    return True;
  else
    return false;
  end if;

end Triangle_dans_surface;

------------------------------------
-- TRIANGULATION_INTERNE
------------------------------------
-- PROCEDURE INTERNE
------------------------------------
-- en entree : une surface sans trous (i.e. un polygone)
-- en sortie : une liste de triangles a l'INTERIEUR de cette surface
------------------------------------
-- PROCEDURE ASSEZ BIDOUILLE ET PAS EXACTE (mais qui suffira pour l'instant)
-- L'ideal serait une triangulation contrainte de Delaunay, mais pour
-- l'instant c'est une TD un peu amelioree
-- DONC NB: dans quelques cas de formes bizzaroides ca peut renvoyer quelques 
-- erreurs
------------------------------------
function Triangulation_interne(Surface : Point_liste_type) 
                               return a_liste_triangle is

T_a_enlever,
Liste_trg,
Trgcur : a_liste_triangle;
Trg : Triangle;
Poly_trg      : Point_liste_type(1..4);
Intersections : Point_liste_type(1..Surface'length*10); -- bidouille!
Inter      : Point_access_type;
Nbinter,
N             : natural := 0;
Existe        : boolean;
Nbvoisins : natural;
Milieux : Type_milieux;
Voisins : Type_voisins;

begin
  -- un Delaunay sur tous les points de la surface
  Delaunay.Delaunay_Tsai(Surface(Surface'first..Surface'last-1), Liste_trg);

  -- ajout au contour des points intersection du contour et des triangles
  -- calcules ci-dessus
--if false then
--  Trgcur := Liste_trg;
--  while Trgcur /= null loop
--    Poly_trg := (Trgcur.Tr.p1,Trgcur.Tr.p2,Trgcur.Tr.p3,Trgcur.Tr.p1);
--    Geometrie.Intersections_de_polylignes(Poly_trg,Surface,4,Surface'length,
--                                          Inter,Nbinter);  
--    for i in 1..Nbinter loop
--      -- ajout des points d'inter en assurant de ne pas faire de doublons
--      Existe := false;
--      for j in 1..N loop
--        if Intersections(j) = Inter(i) then
--          Existe := True;
--          exit;
--        end if;
--      end loop;
--      if not(Existe) then
--        N := N+1;
--        Intersections(N) := Inter(i);
--      end if;
--    end loop;
--    GR_free_point_liste(Inter);
--    Trgcur := Trgcur.suiv;
--  end loop;
--  Vide(Liste_trg);
--  -- nouvelle TD sur le contour et les intersections --> on est alors 
--  -- assure que PRESQUE TOUS les triangles sont soit completement dans la 
--  -- surface, soit completement en dehors
--  Delaunay.Delaunay_Tsai(Intersections(1..N), Liste_trg);
--end if;  

  Trgcur := Liste_trg;
  while Trgcur /= null loop
    if not(Triangle_dans_surface(Surface,Trgcur.Tr)) then
      T_a_enlever := Trgcur;
      Trgcur := Trgcur.suiv;
      enlever_triangle(Liste_trg,T_a_enlever);
    else
      Trgcur := Trgcur.suiv;
    end if;
  end loop;

  return Liste_trg;
end;

------------------------------------
-- SQUELETTISE
------------------------------------
-- PROCEDURE EXPORTEE
------------------------------------
-- Procedure globale de construction du squelette d'un contour sans trous
------------------------------------
function Squelettise(Surface           : Point_access_type;   
		     Pas_decomposition : float := 0.0;
                     Seuil_filtrage    : float := 0.0) 
                     return arbre is

Liste_trg : a_liste_triangle;
Sfiltree,  
Sdecomp   : Point_access_type;
Nfiltree,
Ndecomp,
Nmax      : natural;
Liens     : Liens_access_type;
tree      : arbre;

begin
  Sfiltree := new Point_liste_type(1..Surface'length+1);
  if Seuil_filtrage /= 0.0 then
    Lissage_filtrage.Wall_danielsson(Surface,  Surface'length, Sfiltree, 
                                     Nfiltree, Seuil_filtrage, 1);
    Nfiltree := Nfiltree + 1;
    Sfiltree(Nfiltree) := Sfiltree(1);

  else
    Nfiltree := Surface'length+1;
    Sfiltree(1..Nfiltree) := Surface.all&Surface(1);
  end if;

  if Pas_decomposition /= 0.0 then
    Nmax :=integer(Geometrie.Taille_ligne(Surface.all,Surface'length)
                    /(Pas_decomposition+1.0)); 
    Sdecomp:=new point_liste_type(1..Nmax);
    Liens := new liens_array_type(1..Nmax);
    Geometrie.Decompose_ligne(Sfiltree.all, Nfiltree, Sdecomp.all, Ndecomp, 
                              Pas_decomposition, Liens.all);
    GR_free_liens_array(Liens);
  else
    Ndecomp := Nfiltree;
    Sdecomp := new Point_liste_type'(Sfiltree(1..Nfiltree));
  end if;

  GR_free_point_liste(Sfiltree);

  Liste_trg := Triangulation_interne(Sdecomp(1..Ndecomp)); 
  GR_free_point_liste(SDecomp);
  Tree := Triangles_to_squelette(Liste_trg);
  vide(Liste_trg);
  return Tree;
end Squelettise;

end Squelettisation_seb;
