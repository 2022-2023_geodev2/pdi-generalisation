with unchecked_deallocation;
with Morpholigne; use Morpholigne;
with math_cogit; use math_cogit;
with geometrie;use geometrie;
with lissage_filtrage; 


--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95



-----------------------------------------------
--       MESURES_COMPARAISON                 --
-----------------------------------------------
--         BODY                              --
-----------------------------------------------
--     02 / 10 / 95                          --
-----------------------------------------------


package body Mesures_Comparaison is

PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)


-----------------------------------------------
-- POINTS DOUBLES                            --
--                                           --
-- Nombre de points doubles de l'arc         --
-- Un point double = deux points consecuitfs --
-- confondus                                 --
-----------------------------------------------
-- EN REELS
  function points_doubles(arc : point_liste_reel; 
			  N: natural) return float is

  pts_doubles : natural := 0;

  begin

     for i in 2..N loop
       if arc(i) = arc(i-1) then 
         pts_doubles := pts_doubles + 1;
       end if;
     end loop;

     return float(pts_doubles);

  end;

-- EN ENTIERS
  function points_doubles(arc : point_liste_TYPE; 
			  N: natural) return natural is

  pts_doubles : natural := 0;

  begin

     for i in 2..N loop
       if arc(i) = arc(i-1) then 
         pts_doubles := pts_doubles + 1;
       end if;
     end loop;

     return pts_doubles;

  end;

----------------------------------------
-- Filtrage des pts doubles d'un arc
----------------------------------------
function Filtrage(Tab : Point_liste_type) return Point_access_type is

Tab_filtre : Point_access_type;
Tab_filtre2 : Point_access_type;
Nfiltre    : Natural;

begin
  Tab_filtre := new Point_liste_type'(Tab);
  Tab_filtre(1) := Tab(Tab'first);
  Nfiltre := 1;
  for j in Tab'first+1..Tab'last loop
    if Tab(j) /= Tab_filtre(Nfiltre) then
      Nfiltre := Nfiltre + 1;
      Tab_filtre(Nfiltre) := Tab(j);
    end if;
  end loop;
  Tab_filtre2 := new Point_liste_type'(Tab_filtre(1..Nfiltre));
  GR_free_point_liste(Tab_filtre);

  return Tab_filtre2;
end Filtrage; 

-----------------------------------------------
-- INTERSECTIONS INTERNES                    --
--                                           --
-- Nombre d'intersections internes de l'arc  --
-- Deux points consecutifs egaux ne sont pas --
-- comptabilises ici                         --
-----------------------------------------------
-- EN REELS
  function intersections(arc : point_liste_reel; 
			 N: natural) return float is

    nb, nint : natural := 0;
    lres : point_liste_reel(1..2); 
    
    begin
      for i in 1..N-2 loop
        for j in i+2..N-1 loop
          intersection_de_lignes(arc(i),arc(i+1),arc(j),arc(j+1),
                                 C_segment_type,nint,lres);
          nb := nb+nint; 
        end loop;
      end loop;
      if Arc(1) = Arc(N) and Nb /= 0 then
        Nb := Nb-1;
      end if;
      return float(nb);

    end intersections;

-- EN ENTIERS
  function intersections(arc : point_liste_type; 
			 N: natural) return natural is

    nb, nint : natural := 0;
    lres : point_liste_reel(1..2); 
    arcf : Point_access_type;
    begin
      arcf := Filtrage(Arc);
      for i in 1..Arcf'last-2 loop
        for j in i+2..Arcf'last-1 loop
          intersection_de_lignes(arcf(i),arcf(i+1),arcf(j),arcf(j+1),
                                 C_segment_type,nint,lres);
          nb := nb+nint; 
        end loop;
      end loop;
      if Arc(1) = Arc(N) and Nb /= 0 then
        Nb := Nb-1;
      end if;
      GR_free_point_liste(Arcf);
      return nb;

    end intersections;


--------------------------------------------------------
-- CONFLITS INTERNES                                  --
--                                                    --
-- Cette fonction retourne la longueur de conflit     --
-- de l'arc generalise divisee par sa longueur        --
--                                                    --
-- la longueur de conflit est definie par la          --
-- longueur de l'arc ou l'un des deux bords du        --
-- symbole se trouve eloigne de l'autre bord          --
-- de plus d'un seuil defini ci dessous               --
--                                                    -- 
--------------------------------------------------------
-- SM 6/98: MODIF POUR UTILISER LA MESURE INDICATEUR-CONFLIT DE MORPHOLIGNE 

-- EN REELS
  function conflit(arc : point_liste_reel;
                   N : natural;
                   l : float) return float is

  Arc_entier : Point_liste_type(1..N); 
  Conf : Force_conflit;

  begin   
    Arc_entier := Entier(arc(1..N));    
    Conf := Indicateur_conflits(Arc_entier(1..N), l);
    return Conf(Droite) + Conf(Gauche);
  end conflit;

-- EN REELS
  function conflit(arc : point_liste_type;
                   N : natural;
                   l : float) return float is

  Conf : Force_conflit;

  begin   
    Conf := Indicateur_conflits(Arc(1..N), l);
    return Conf(Droite) + Conf(Gauche);
  end conflit;
--------------------------------------------------------
-- RECTANGLE_ENGLOBANT                                --
--                                                    --
-- procedure interne (qui serait mieux dans GEOMETRIE)                                   -- 
--------------------------------------------------------

  Procedure Rectangle_englobant(Arc : in point_liste_type;
                                Pt_bas_gauche : out Point_type;
                                Pt_haut_droite : out Point_type) is

  Xmin, Xmax, Ymin, Ymax : Integer;
  begin
    Xmin := Arc(Arc'first).Coor_X;
    Ymin := Arc(Arc'first).Coor_Y;
    Xmax := Arc(Arc'first).Coor_X;
    Ymax := Arc(Arc'first).Coor_Y;

    for i in Arc'first+1..Arc'last loop
      if Arc(i).coor_X < Xmin then
        Xmin := Arc(i).coor_X;
      end if;
      if Arc(i).coor_X > Xmax then
        Xmax := Arc(i).coor_X;
      end if;
      if Arc(i).coor_Y < Ymin then
        Ymin := Arc(i).coor_Y;
      end if;
      if Arc(i).coor_Y > Ymax then
        Ymax := Arc(i).coor_Y;
      end if;
    end loop;
    Pt_bas_gauche := (Xmin, Ymin);
    Pt_haut_droite := (Xmax, Ymax);
  end Rectangle_englobant;

--------------------------------------------------------
-- INTERSECTIONS EXTERNES                             --
--                                                    --
-- Fonction exportee                                  -- 
--------------------------------------------------------
  function intersection_externe(Arc : Natural;
                                Graphe : Graphe_type) 
                                return Natural is

  Nint, Ntot, Narcs,  N1, N2 : Natural := 0;
  Info : Info_type;
  Arc1_tab, Arc2_tab, Lres  : Point_access_type;
  Tab_arcs : Liens_access_type;
  Pt1,Pt2 : Point_type;
  Arc1_inf, Arc2_inf : Arc_type;

  begin
    Info := GR_infos(Graphe);
    Arc1_inf := GR_arc(Graphe, Arc);
    Arc1_tab := new Point_liste_type(1..Info.Max_pts_arc);
    -- recuperation de la geometrie de l'arc a considerer
    GR_points_d_arc(Graphe, Arc, Arc1_tab.all, N1);
    -- recuperation des arcs dans le rectangle englobant de l'arc a considerer
    Tab_arcs := new Liens_array_type(1..Info.Nb_arcs);
    Rectangle_englobant(Arc1_tab(1..N1),Pt1,Pt2);
    GR_arcs_in_rectangle(Graphe, Pt1, Pt2, Tab_arcs.all, Narcs); 

    -- boucle sur les arcs potentiellement intersectants
    Arc2_tab := new Point_liste_type(1..Info.Max_pts_arc);
    for i in 1..Narcs loop
      if Tab_arcs(i) /= Arc then
        -- recuperation de la geometrie de l'arc intersectant
        Arc2_inf := GR_arc(Graphe, Tab_arcs(i));
        GR_points_d_arc(Graphe, Tab_arcs(i), Arc2_tab.all, N2);
  
        -- calcul du nb d'intersection avec l'arc a considerer
        Geometrie.Intersections_de_polylignes(Arc1_tab(1..N1),Arc2_tab(1..N2), N1, N2, Lres, Nint);
        if Lres /= Null then
          GR_free_point_liste(Lres);
        end if;

        -- calcul du nb d'intersections "normales", i.e. a un noeud
        if Arc1_inf.Noeud_ini = Arc2_inf.Noeud_ini and Nint > 0 then
          Nint := Nint - 1;
        end if;
        if Arc1_inf.Noeud_fin = Arc2_inf.Noeud_ini and Nint > 0 then
          Nint := Nint - 1;
        end if;
        if Arc1_inf.Noeud_ini = Arc2_inf.Noeud_fin and Nint > 0 then
          Nint := Nint - 1;
        end if;
        if Arc1_inf.Noeud_fin = Arc2_inf.Noeud_fin and Nint > 0 then
          Nint := Nint - 1;
        end if;
        Ntot := Ntot + Nint;
      end if;
    end loop;
    GR_free_liens_array(Tab_arcs);
    GR_free_point_liste(arc1_tab);
    GR_free_point_liste(arc2_tab);
    return Ntot;
  end;
-------------------------------------------------
-- RECOUVREMENT                                --
--                                             --
-- Cette fonction retourne le nombre de trous  --
-- dans la surface couverte par le symbole     --
-------------------------------------------------

  function recouvrement(arc : point_liste_reel;
                        N : natural;
                        l : float) return float is

  Arcd, Arcg : Point_Access_reel; 
  Parentd, Parentg : Liens_access_type;
  Nd, Ng : natural;               
  nb_jonction: natural := 0;


  begin  
    Dilatation_arc(Arc(1..N),l,arcg,Parentg,arcd,Parentd); 
    Nd := arcd'length;
    Ng := arcg'length;

    for i in 1..nd-3 loop
      for j in i+3..nd loop
         if arcd(i) = arcd(j) and arcd(i+1) = arcd(j-1) then 
            nb_jonction:=nb_jonction + 1;
         end if;
      end loop;
    end loop;

    for i in 1..ng-3 loop
      for j in i+3..ng loop
          if arcg(i) = arcg(j) and arcg(i+1) = arcg(j-1) then 
            nb_jonction:=nb_jonction + 1; 
          end if;
      end loop;
    end loop;  
    GR_free_point_liste_reel(Arcd);
    GR_free_point_liste_reel(Arcg);
    GR_free_liens_array(Parentd);
    GR_free_liens_array(Parentg);

    return float(nb_jonction);

  end recouvrement;


-----------------------------------------------
-- SURFACE DE DEPLACEMENT                    --
--                                           --
-- Cette fonction retourne la surface de     --
-- deplacement delimite par les deux arcs    --
-- comme definie par McMaster                --
-- Pour calculer cette surface pour deux     --
-- arcs ayant des extremites disjointes      --
-- celles-ci sont reliees pour fermer la     --
-- surface                                   --
-----------------------------------------------

-- EN REELS
  function surface_deplacement(arc_or, arc_gen : point_liste_reel;
                               N_or, N_gen : natural) return float is

 
  type Pt_inter is record          -- un point d'intersection
          P : point_type_reel;          -- les coord. du point
          Pt_or, Pt_gen : Natural; -- les no des points situes juste
                                   -- avant le Pt inter sur les deux arcs
          end record;

  Inter : array (1..N_or*2) of Pt_inter; -- la liste des Pts d'inter.
  Ninter : natural := 0;                 -- le nombre d'intersections
  sous_N_or, sous_N_gen : natural; -- un sous arc situe entre deux pts d'inter.
  sous_arc_or, sous_arc_gen : point_access_reel;
  surf_tot, surf_droite : float := 0.0; -- la surface totale de deplacement et
                                        -- la surface a droite de l'arc original
  surf, long : array (1..N_or) of float := (others => 0.0);
  Pt_intersection : point_type_reel;
  intersection : boolean;
 
  begin
    -- generation de la liste d'intersections entre les arcs 
    -- NB: 1 suite de segments confondus => seulement 2 pts d'inbtersection
    if Arc_or(1) /= Arc_gen(1) then
       Ninter := 1;
       Inter(1):=(Arc_or(1),1,0);  -- pour raccorder au debut
       end if;
    for j in 1..N_gen-1 loop
      for i in 1..N_or-1 loop
         intersection_de_lignes(Arc_gen(j),Arc_gen(j+1),Arc_or(i),Arc_or(i+1),
                               C_segment_type,intersection,Pt_intersection);
         if intersection and then (Ninter = 0 or else Pt_intersection /= Inter(Ninter).P) then 
           Ninter := Ninter + 1;
           Inter(Ninter).P := Pt_intersection;
           Inter(Ninter).Pt_or := i;
           Inter(Ninter).Pt_gen := j; 
          end if;
      end loop;
    end loop;
    if Arc_or(N_or) /= Arc_gen(N_gen) then 
       Ninter := Ninter + 1;
       inter(Ninter):=(Arc_or(N_or),N_or-1,N_gen);  -- pour raccorder a la fin
       end if;

    -- calculs lies aux surfaces de deplacement  
   
   for i in 1..Ninter-1 loop -- boucle sur les surfaces delimitees
                             -- par deux points d'intersection
     
          -- determination du sous_arc de l'arc or. entre deux points d'intersection
          -- ce sous arc peut etre dans un sens quelconque
          -- calcul de sa "surface"
     if inter(i).Pt_or < inter(i+1).Pt_or then 
           sous_N_or := inter(i+1).Pt_or-inter(i).Pt_or+2;
           sous_arc_or := new point_liste_reel(1..sous_N_or);
           sous_arc_or(2..sous_N_or-1) := Arc_or(inter(i).Pt_or+1..inter(i+1).Pt_or);
           sous_arc_or(1) := inter(i).P;
           sous_arc_or(sous_N_or) := inter(i+1).P;
           surf(i) := surf(i) + surface_arc(sous_arc_or.all,sous_N_or);
        else
           sous_N_or := inter(i).Pt_or-inter(i+1).Pt_or+2;
           sous_arc_or := new point_liste_reel(1..sous_N_or);
           sous_arc_or(1) := inter(i+1).P;
           sous_arc_or(2..sous_N_or-1) := Arc_or(inter(i+1).Pt_or+1..inter(i).Pt_or );
           sous_arc_or(sous_N_or) := inter(i).P;
           surf(i) := surf(i) - surface_arc(sous_arc_or.all,sous_N_or);
      end if;

          -- determination du sous_arc de l'arc gen. entre deux points d'intersection
	  -- ce sous arc est necessairement dans le sens de parcours de pts d'inter.
          -- calcul de sa "surface"
     sous_N_gen := inter(i+1).Pt_gen-inter(i).Pt_gen+2;
     sous_arc_gen := new point_liste_reel(1..sous_N_gen);
     sous_arc_gen(2..sous_N_gen-1) := Arc_gen(inter(i).Pt_gen+1..inter(i+1).Pt_gen);
     sous_arc_gen(1) := inter(i).P;
     sous_arc_gen(sous_N_gen) := inter(i+1).P;
     surf(i) := surf(i) - surface_arc(sous_arc_gen.all,sous_N_gen);
 
          -- mise a jour des variables surface totale et surface a droite
     if surf(i) > 0.0 then 
        surf_droite := surf_droite + surf(i); 
     end if;

     surf_tot := surf_tot + abs(surf(i));
 
     -- deallocations
     GR_free_point_liste_reel(sous_arc_or);
     GR_free_point_liste_reel(sous_arc_gen);

   end loop; -- boucle sur les surfaces entre deux points d'intersection

    return Surf_tot;

  end Surface_deplacement;


-- EN REELS
  function surface_deplacement(arc_or, arc_gen : point_liste_type;
                               N_or, N_gen : natural) return float is


    
  surf : long_float;
  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);

     return Surface_deplacement(Arc_or_f,Arc_gen_f,N_or,N_gen);
 
  end surface_deplacement;    

-------------------------------------------------------
-- COEFFICIENT DE CENTRAGE                           --
--                                                   --
-- Cette fonction retourne la mesure                 --
-- define par :                                      --
-- 2*min(Surf_droite/Surf_tot,Surf_gauche/surf_tot)  --
-------------------------------------------------------

-- EN REELS
  function coef_centrage(arc_or, arc_gen : point_liste_reel;
                         N_or, N_gen : natural) return float is
 
  type Pt_inter is record          -- un point d'intersection
          P : point_type_reel;          -- les coord. du point
          Pt_or, Pt_gen : Natural; -- les no des points situes juste
                                   -- avant le Pt inter sur les deux arcs
          end record;

  Inter : array (1..N_or*2) of Pt_inter; -- la liste des Pts d'inter.
  Ninter : natural := 0;                 -- le nombre d'intersections
  sous_N_or, sous_N_gen : natural; -- un sous arc situe entre deux pts d'inter.
  sous_arc_or, sous_arc_gen : point_access_reel;
  surf_tot, surf_droite : float := 0.0; -- la surface totale de deplacement et
                                        -- la surface a droite de l'arc original
  surf, long : array (1..N_or) of float := (others => 0.0);
  Pt_intersection : point_type_reel;
  intersection : boolean;
 
  begin
    -- generation de la liste d'intersections entre les arcs 
    -- NB: 1 suite de segments confondus => seulement 2 pts d'inbtersection
    if Arc_or(1) /= Arc_gen(1) then
       Ninter := 1;
       Inter(1):=(Arc_or(1),1,0);  -- pour raccorder au debut
       end if;
    for j in 1..N_gen-1 loop
      for i in 1..N_or-1 loop
         intersection_de_lignes(Arc_gen(j),Arc_gen(j+1),Arc_or(i),Arc_or(i+1),
                               C_segment_type,intersection,Pt_intersection);
         if intersection and then (Ninter = 0 or else Pt_intersection /= Inter(Ninter).P) then 
           Ninter := Ninter + 1;
           Inter(Ninter).P := Pt_intersection;
           Inter(Ninter).Pt_or := i;
           Inter(Ninter).Pt_gen := j; 
          end if;
      end loop;
    end loop;
    if Arc_or(N_or) /= Arc_gen(N_gen) then 
       Ninter := Ninter + 1;
       inter(Ninter):=(Arc_or(N_or),N_or-1,N_gen);  -- pour raccorder a la fin
       end if;

    -- calculs lies aux surfaces de deplacement  
   
   for i in 1..Ninter-1 loop -- boucle sur les surfaces delimitees
                             -- par deux points d'intersection
     
          -- determination du sous_arc de l'arc or. entre deux points d'intersection
          -- ce sous arc peut etre dans un sens quelconque
          -- calcul de sa "surface"
     if inter(i).Pt_or < inter(i+1).Pt_or then 
           sous_N_or := inter(i+1).Pt_or-inter(i).Pt_or+2;
           sous_arc_or := new point_liste_reel(1..sous_N_or);
           sous_arc_or(2..sous_N_or-1) := Arc_or(inter(i).Pt_or+1..inter(i+1).Pt_or);
           sous_arc_or(1) := inter(i).P;
           sous_arc_or(sous_N_or) := inter(i+1).P;
           surf(i) := surf(i) + surface_arc(sous_arc_or.all,sous_N_or);
        else
           sous_N_or := inter(i).Pt_or-inter(i+1).Pt_or+2;
           sous_arc_or := new point_liste_reel(1..sous_N_or);
           sous_arc_or(1) := inter(i+1).P;
           sous_arc_or(2..sous_N_or-1) := Arc_or(inter(i+1).Pt_or+1..inter(i).Pt_or );
           sous_arc_or(sous_N_or) := inter(i).P;
           surf(i) := surf(i) - surface_arc(sous_arc_or.all,sous_N_or);
      end if;

          -- determination du sous_arc de l'arc gen. entre deux points d'intersection
	  -- ce sous arc est necessairement dans le sens de parcours de pts d'inter.
          -- calcul de sa "surface"
     sous_N_gen := inter(i+1).Pt_gen-inter(i).Pt_gen+2;
     sous_arc_gen := new point_liste_reel(1..sous_N_gen);
     sous_arc_gen(2..sous_N_gen-1) := Arc_gen(inter(i).Pt_gen+1..inter(i+1).Pt_gen);
     sous_arc_gen(1) := inter(i).P;
     sous_arc_gen(sous_N_gen) := inter(i+1).P;
     surf(i) := surf(i) - surface_arc(sous_arc_gen.all,sous_N_gen);
 
          -- mise a jour des variables surface totale et surface a droite
     if surf(i) > 0.0 then 
        surf_droite := surf_droite + surf(i); 
     end if;

     surf_tot := surf_tot + abs(surf(i));
 
     -- deallocations
     GR_free_point_liste_reel(sous_arc_or);
     GR_free_point_liste_reel(sous_arc_gen);

   end loop; -- boucle sur les surfaces entre deux points d'intersection

   if surf_tot /=0.0 then 
     return min((surf_droite/surf_tot , 1.0-surf_droite/surf_tot))*2.0;
   else return 1.0;
   end if;   

  end coef_centrage;

-- EN ENTIERS
  function coef_centrage(arc_or, arc_gen : point_liste_type;
                         N_or, N_gen : natural) return float is

  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);
     return Coef_centrage(Arc_or_f, Arc_gen_f, N_or, N_gen);
  end;

-----------------------------------------------
-- DISTANCE DE HAUSDORFF                     --
--                                           --
-- Cette fonction retourne l'approximation   --
-- de la distance de Hausdorff calculee      --
-- comme le maximum des distances d'un       --
-- point intermediaire d'un des arcs a       --
-- l'autre arc (ce qui differe dans quelques --
-- cas de la definition theorique )          --
-----------------------------------------------

-- EN REELS
  function Hausdorff(arc_or, arc_gen : point_liste_reel;
                     N_or, N_gen : natural) return float is
    
  dist, dist_cour : float := 0.0;

  begin  
    -- distance de l'arc generalise a l'arc original
    for i in 1..N_gen loop
       dist_cour := distance_a_polylignes(arc_gen(i),arc_or(1..N_or),N_or,C_segment_type);
       if dist_cour>dist then dist := dist_cour;
          end if;
    end loop; 

    -- distance de l'arc original a l'arc generalise
    for i in 1..N_or loop
       dist_cour := distance_a_polylignes(arc_or(i),arc_gen(1..N_gen),N_gen,C_segment_type);
       if dist_cour>dist then dist := dist_cour;
          end if;
    end loop;       
    return dist;
  end Hausdorff;

-- EN ENTIERS
  function Hausdorff(arc_or, arc_gen : point_liste_type;
                     N_or, N_gen : natural) return float is

  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);
     return Hausdorff(Arc_or_f, Arc_gen_f, N_or, N_gen);
  end;


---------------------------------
-- NB VIRAGES D'UN ARC         --
-- fonction interne            --
---------------------------------

function nb_virages(arc : point_liste_reel;
		    N : natural) return natural is

    Nb : natural := 0;
    angle1, angle2 : float;
  begin
     angle1 := Angle_3points(arc(1),arc(2),arc(3));
    if angle1 = pi then angle1 := 0.0; end if;
    for i in 2..N-1 loop   
       angle2 := Angle_3points(arc(i-1),arc(i),arc(i+1));
       if angle2 /= pi then  
         if angle1 * angle2 < 0.0 then nb := nb + 1; end if;
         angle1 := angle2;
       end if;
    end loop;
    return nb + 1;
  end nb_virages;

---------------------------------
-- NB VIRAGES D'UN ARC LISSE   --
-- fonction interne           --
---------------------------------
function nb_virages_lisses(	arc : point_liste_reel; 
		  		N : natural;
                  		sigma : float) return natural is

Type liens_neg_type is array(integer range <>) of integer;

n_aux, nb, l : natural :=1;
pas : float:=1.0;  --- pas de decoupage de la ligne en pts equidistants
k1 : integer := 4*integer(sigma);
c1,c2 : float;

Ligne_aux, Ligne_aux_lisse : point_access_reel;
ligne_lisse                : point_liste_reel(1..N);
Nb_segments                : liens_neg_type(1-k1..2000+k1);
lgr_segments               : liens_array_type(1..2000);

begin
   if N < 4 then return 1; end if; 
   Decompose_ligne(arc(1..N),N,ligne_aux,n_aux,pas,lgr_segments);
   k1:= 4 * natural(sigma);
   if k1 >= n_aux then k1 := n_aux-1;  end if;

   for i in 1..N-1 loop
        nb_segments(i):=lgr_segments(i);
   end loop;
   for i in 1..k1 loop
       nb_segments(1-i):=nb_segments(i);
       nb_segments(N+i-1):=nb_segments(N-i);
   end loop;    

   ligne_aux_lisse:= new point_liste_reel(1..n_aux);
   Lissage_filtrage.Filtre_Gaussien_r(ligne_aux(1..n_aux), 
                   n_aux,sigma,ligne_aux_lisse(1..n_aux));

   l:= 1;
   Ligne_lisse(1):= Ligne_aux_lisse(1);
   for i in 2..N loop
     l:= l + nb_segments(i-1);
     Ligne_lisse(i):= Ligne_aux_lisse(l);
   end loop;

   nb := nb_virages(ligne_lisse(1..n),n);
   Gr_Free_point_liste_reel(Ligne_aux);
   Gr_Free_point_liste_reel(Ligne_aux_lisse);
   return nb;

end nb_virages_lisses;

---------------------------------
-- NB VIRAGES D'UN ARC LISSE   --
-- fonction interne           --
---------------------------------
function nb_virages_lisses(	arc : point_liste_type; 
		  		N : natural;
                  		sigma : float) return natural is

  Arc_f : Point_liste_reel(1..N);
  
  begin 
     Arc_f := Reel(Arc(1..N));
     return Nb_virages_lisses(Arc_f, N, sigma);
  end;

--------------------------------------------------------
-- NOMBRE DE VIRAGES APRES LISSAGE FORT               --
--                                                    --
-- Cette fonction retourne le nombre de virages       --
-- apres lissage de l'arc                             --
-- La force de ce lissage est: 8 * largeur du symbole --
--------------------------------------------------------

-- EN REELS
  function virages_lissage_fort(arc_or, arc_gen : point_liste_reel;
                                N_or, N_gen : natural;
				l_gen : float) return float is          


  begin

    return  float(nb_virages_lisses(arc_gen,N_gen,l_gen*16.0)) 
		/ float(nb_virages_lisses(arc_or,N_or,l_gen*16.0));

  end virages_lissage_fort;

-- EN ENTIERS
  function virages_lissage_fort(arc_or, arc_gen : point_liste_type;
                                N_or, N_gen : natural;
				l_gen : float) return float is          


  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);

    return  float(nb_virages_lisses(arc_gen_f,N_gen,l_gen*16.0)) 
		/ float(nb_virages_lisses(arc_or_f,N_or,l_gen*16.0));

  end virages_lissage_fort;

--------------------------------------------------------
-- NOMBRE DE VIRAGES APRES LISSAGE FAIBLE             --
--                                                    --
-- Cette fonction retourne le nombre de virages       --
-- apres lissage de l'arc                             --
-- La force de ce lissage est: 1 * largeur du symbole --
--------------------------------------------------------

-- EN REELS
  function virages_lissage_faible(arc_or, arc_gen : point_liste_reel;
                                 N_or, N_gen : natural;
				 l_gen : float) return float is          

  begin

    return  float(nb_virages_lisses(arc_gen,N_gen,l_gen*2.0))
		/ float(nb_virages_lisses(arc_or,N_or,l_gen*2.0));

  end virages_lissage_faible;

-- EN ENTIERS
  function virages_lissage_faible(arc_or, arc_gen : point_liste_type;
                                N_or, N_gen : natural;
				l_gen : float) return float is          


  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);

    return  float(nb_virages_lisses(arc_gen_f,N_gen,l_gen*2.0)) 
		/ float(nb_virages_lisses(arc_or_f,N_or,l_gen*2.0));

  end virages_lissage_faible;

-------------------------------------------------------
-- MOYENNE DE LA VALEUR ABSOLUE DES ANGLES           --
--                                                   --
-- Cette fonction retourne la moyenne en radians de  --
-- la valeur absolue des angles                      --
-------------------------------------------------------

-- EN REELS
  function moyenne_angles(arc_or, arc_gen : point_liste_reel;
                          N_or, N_gen : natural ) return float is
                     
  moy_or, moy_gen : float := 0.0;   
  begin 

    for i in 2..N_gen-1 loop
      moy_gen := moy_gen + pi 
                 - abs(angle_3points(arc_gen(i-1),arc_gen(i),arc_gen(i+1)));
    end loop;
    if N_gen < 3 then moy_gen := 0.0 ;
                      else moy_gen := moy_gen / float(N_gen-2);
    end if;

    for i in 2..N_or-1 loop
      moy_or := moy_or + pi 
                 - abs(angle_3points(arc_or(i-1),arc_or(i),arc_or(i+1)));
    end loop;
    if N_or < 3 then moy_or := 0.0 ;
                      else moy_or := moy_or / float(N_or-2);
    end if;
   
    return moy_gen - moy_or;

  end moyenne_angles;

-- EN ENTIERS

  function moyenne_angles(arc_or, arc_gen : point_liste_type;
                          N_or, N_gen : natural ) return float is
  Arc_or_f : Point_liste_reel(1..N_or);
  Arc_gen_f : Point_liste_reel(1..N_gen);
  
  begin 
     Arc_or_f := Reel(Arc_or);
     Arc_gen_f := Reel(Arc_gen);
     return Moyenne_angles(Arc_or_f,Arc_gen_f,N_or,N_gen);
  end;

-- EN REELS
  function moyenne_angles(arc : point_liste_reel;
                          N   : natural ) return float is
                     
  moy : float := 0.0;   
  begin 

    for i in 2..N-1 loop
      moy := moy + pi 
                 - abs(angle_3points(arc(i-1),arc(i),arc(i+1)));
    end loop;
    if N < 3 then 
      moy := 0.0 ;
    else 
      moy := moy / float(N-2);
    end if;
   
    return moy;

  end moyenne_angles;

-- EN ENTIERS
  function moyenne_angles(arc : point_liste_type;
                          N   : natural ) return float is
                     
  moy : float := 0.0;   
  begin 

    for i in 2..N-1 loop
      moy := moy + pi 
                 - abs(angle_3points(arc(i-1),arc(i),arc(i+1)));
    end loop;
    if N < 3 then 
      moy := 0.0 ;
    else 
      moy := moy / float(N-2);
    end if;
   
    return moy;

  end moyenne_angles;

-------------------------------------------------
-- Ajout de 2 mesures Xavier CHEIPPE  : 18/04/97
-------------------------------------------------


---------------------------------------
-- Direction privilegiee
--------------------------------------
-- ma_ligne : ligne dont on veut connaitre la direction principale
-- nb_points : son nombre de points
-- angle : l'angle de la direction principale
-- coef_confiance : coefficient de confiance, entre 0 et 1, caracterisant
--                  le degre de signification de la l'angle calcule. Plus
--                  il est proche de 1 plus l'ensemble des segments a la
--                  meme orientation generale.

procedure direction_principale(ma_ligne : point_liste_reel; nb_points: natural;
                                        angle,coef_confiance : out float) 
is

a,a2,b,b2,p,alpha2,L : float;
direction : point_type_reel;

begin
direction.coor_x := 0.0;
direction.coor_y := 0.0;
L := 0.0;
for i in 1..(nb_points-1) loop
  a := ma_ligne(i+1).coor_x - ma_ligne(i).coor_x;
  b := ma_ligne(i+1).coor_y - ma_ligne(i).coor_y;
  if a/=0.0 and b/=0.0 then
    p := sqrt(a*a+b*b);
    if a/=0.0 then
      alpha2 := 2.0 * Arctan(b/a); 
    else 
      alpha2 := pi;
    end if;
    direction.coor_x := direction.coor_x + p * COS(alpha2);
    direction.coor_y := direction.coor_y + p * SIN(alpha2);
    L := L + p;     -- longueur de la ligne
  end if;
end loop;
a := direction.coor_x;
b := direction.coor_y;
p := sqrt(a*a+b*b);
if a > 0.0 then angle := Arctan(b/a)/2.0;
elsif a < 0.0 then
  angle := (Arctan(b/a)+pi)/2.0;
else if b/=0.0 then angle := pi/4.0;  -- a=0 et b<>0
               else angle := 0.0;     -- a=b=0
     end if;
end if;   
coef_confiance := p/L;
end direction_principale;

---------------------
-- Ecart de direction
---------------------
-- remarque : ne caracterise pas l'opposition de deux courbes entre elles

procedure ecart_direction(ma_ligne,ma_ligne_g : point_liste_reel;
                          nb_points,nb_points_g: natural;
                          ecart_angle,coef_confiance : out float)
is

angle_o,confiance_o,angle_g,confiance_g : float;

begin
direction_principale(ma_ligne,nb_points,angle_o,confiance_o);
direction_principale(ma_ligne_g,nb_points_g,angle_g,confiance_g);
if abs(angle_g - angle_o) <= (pi/2.0) then
  ecart_angle := 2.0*100.0*(abs(angle_g - angle_o))/pi; -- ramene en grades
else
  ecart_angle := 2.0*100.0*(pi - abs(angle_g - angle_o))/pi;
end if;
coef_confiance := confiance_o * confiance_g;
end ecart_direction;



-------------------------------------------------------------------
-- FONCTION EXPORTEE                                             --
-------------------------------------------------------------------
-- MESURE_UN_COUPLE                                              --
-- retourne les 11 mesures (type_mesure)                         --
-- calculees sur un couple arc original / arc generalise         --
-- les coordonnees sont codees en INTEGER                        --
-------------------------------------------------------------------

  function mesure_un_couple(
            arc_or, arc_gen: point_liste_type;  -- les deux arcs
            N_or,N_gen: natural;                 -- leur nombre de points
	    l_gen : float                   -- largeur du symbole pour la redaction
                                            -- de la generalisation (en points) 
                   ) return  type_mesures is  -- les resultats des mesures

  arc_or_float : point_liste_reel (1..N_or);
  arc_gen_float : point_liste_reel (1..N_gen);

  begin

    for i in 1..N_or loop
      arc_or_float(i) := ( float(arc_or(i).coor_X) , float(arc_or(i).coor_Y) );
    end loop;
    for i in 1..N_gen loop
      arc_gen_float(i) := ( float(arc_gen(i).coor_X) , float(arc_gen(i).coor_Y) );
    end loop;

    return mesure_un_couple(arc_or_float,arc_gen_float, N_or, N_gen, l_gen);

  end mesure_un_couple;



-------------------------------------------------------------------
-- FONCTION EXPORTEE                                             --
-------------------------------------------------------------------
-- MESURE_UN_COUPLE                                              --
-- retourne les 10 mesures (type_mesure)                         --
-- calculees sur un couple arc original / arc generalise         --
-- les coordonnees sont codees en FLOAT                          --
-------------------------------------------------------------------
 function mesure_un_couple(
            arc_or, arc_gen: point_liste_reel;   -- les deux arcs
            N_or,N_gen: natural;                 -- leur nombre de points
	    l_gen : float                   -- largeur du symbole pour la redaction
                                            -- de la generalisation (en points) 
                  ) return  type_mesures is   -- les resultats des mesures

  mesures : type_mesures;

  begin

    mesures(1) := points_doubles(arc_gen, N_gen);

    mesures(2) := intersections(arc_gen, N_gen);
  
    mesures(3) := conflit(arc_gen, N_gen, l_gen);

    mesures(4) := recouvrement(arc_gen, N_gen ,l_gen);
 
    mesures(5) := surface_deplacement(arc_or, arc_gen, N_or, N_gen);

    mesures(6) := coef_centrage(arc_or, arc_gen, N_or, N_gen);

    mesures(7) := Hausdorff(arc_or, arc_gen, N_or, N_gen);

    mesures(8) := virages_lissage_fort(arc_or,arc_gen,N_or,N_gen,l_gen);

    mesures(9) := virages_lissage_faible(arc_or,arc_gen,N_or,N_gen,l_gen);

    mesures(10) := moyenne_angles(arc_or, arc_gen, N_or, N_gen);

    mesures(11) := float(N_gen) / float(N_or);

    return mesures;

  end mesure_un_couple;

end Mesures_Comparaison;
