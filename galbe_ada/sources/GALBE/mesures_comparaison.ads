--------------------------------
--  MESURES_COMPARAISON       --
--------------------------------
-- SPECIFICATIONS             --
--------------------------------
-- AJOUTS SM 6/98; surcharge des mesures pour des point_liste_type

with Gen_io;use gen_io;

package Mesures_Comparaison is


--------------------------------------------------
-- Chacune des mesures est implementee dans     --
-- mesures_B de maniere INDEPENDANTE des autres --
-- Ceci est un peu au detriment de la rapidite  --
-- mais permettera une eventuelle modification  --
-- de la liste des mesures plus aisee           --
--------------------------------------------------
-- SM 6/98: TYPE_MESURES ET MESURE-UN-COUPLE PEUVENT ETRE VIRES A MON AVIS (INUTILE)
-- OU DOIVENT ETRE MODIFIES
  type type_mesures is array (1..11) of float ;

  -- 1 nombre de points doubles de l'arc
  -- 2 nombre d'intersections internes de l'arc generalise
  -- 3 longueur de conflit interne / longueur de l'arc generalise
  -- 4 nombre de trous de la representation carto du symbole
  -- 5 surface totale de deplacement / longueur de l'arc original
  -- 6 coefficient de centrage
  -- 7 distance de Hausdorff entre les arcs (approchee)
  -- 8 rapport du nombre de virages de l'arc generalise sur le nombre de 
  --   virages de l'arc original apres un lissage fort des deux arcs
  -- 9 rapport du nombre de virages de l'arc generalise sur le nombre de 
  --   virages de l'arc original sans lissage  
  -- 10 Difference entre la moyenne des valeurs absolues de angles de 
  --    l'arc generalise et celle de l'arc original
  -- 11 taux de compression du nombre de coordonnees

  -- 12 Mesure de Xavier CHEIPPE (a ajouter ulterieurement)
 
  
-------------------------------------------------------------------
-- MESURE_UN_COUPLE                                              --
-- retourne les 11 mesures (type_mesures)                         --
-- calculees sur un couple arc original / arc generalise         --
-- les coordonnees sont codees en INTEGER                        --
-------------------------------------------------------------------
 function mesure_un_couple(
     arc_or, arc_gen: point_liste_type;  -- les deux arcs
     N_or,N_gen: natural;                -- leur nombre de points
     l_gen : float                       -- largeur du symbole pour la redaction
                                         -- de la generalisation (en points) 
               ) return  type_mesures ;  -- les resultats des mesures


-------------------------------------------------------------------
-- MESURE_UN_COUPLE                                              --
-- retourne les 11 mesures (type_mesures)                         --
-- calculees sur un couple arc original / arc generalise         --
-- les coordonnees sont codees en FLOAT                          --
-------------------------------------------------------------------
 function mesure_un_couple(
     arc_or, arc_gen: point_liste_reel;  -- les deux arcs
     N_or,N_gen: natural;                -- leur nombre de points
     l_gen : float                       -- largeur du symbole pour la redaction
                                         -- de la generalisation (en points) 
               ) return  type_mesures ;  -- les resultats des mesures


-----------------------------------------------------------------------
--                 LES MESURES ISOLEES                               --
-----------------------------------------------------------------------

-----------------------------------------------
-- POINTS DOUBLES                            --
--                                           --
-- Nombre de points doubles de l'arc         --
-- Un point double = deux points consecuitfs --
-- confondus                                 --
-----------------------------------------------

  function points_doubles(arc : point_liste_reel; 
			  N: natural) return float;

  function points_doubles(arc : point_liste_type; 
			  N: natural) return natural;
-----------------------------------------------
-- INTERSECTIONS INTERNES                    --
--                                           --
-- Nombre d'intersections internes de l'arc  --
-- Deux points consecutifs egaux ne sont pas --
-- comptabilises ici                         --
-----------------------------------------------

  function intersections(arc : point_liste_reel; 
			 N: natural) return float;

  function intersections(arc : point_liste_type; 
			 N: natural) return natural;

-----------------------------------------------
-- INTERSECTIONS INTERNES                    --
--                                           --
-- Nombre d'intersections externes de l'arc  --
-- autres que celles aux extremites          --
-----------------------------------------------
  function intersection_externe(Arc : Natural;
                                Graphe : Graphe_type) 
                                return Natural;

--------------------------------------------------------
-- CONFLITS INTERNES                                  --
--                                                    --
-- Cette fonction retourne la longueur de conflit     --
-- de l'arc generalise (en pts)                       --
--                                                    --
-- la longueur de conflit est definie par la          --
-- longueur de l'arc ou l'un des deux bords du        --
-- symbole se trouve eloigne de l'autre bord          --
-- de plus d'un seuil defini ci dessous               --
--                                                    -- 
--------------------------------------------------------
-- SM 6/98 : modif du body pour que cette mesure s'appuie sur
-- Indicateur_conflit de Morpholigne 
-- NB: la version precedente calculait la longueur de conflit / longueur de la ligne

  function conflit(arc : point_liste_reel;
                   N : natural;
                   l : float) return float;

  function conflit(arc : point_liste_type;
                   N : natural;
                   l : float) return float;

-------------------------------------------------
-- RECOUVREMENT                                --
--                                             --
-- Cette fonction retourne le nombre de trous  --
-- dans la surface couverte par le symbole     --
-------------------------------------------------
-- SM 6/98: MESURE COMPLETEMENT SANS INTERET
  function recouvrement(arc : point_liste_reel;
                        N : natural;
                        l : float) return float;

-----------------------------------------------
-- SURFACE DE DEPLACEMENT                    --
--                                           --
-- Cette fonction retourne la surface de     --
-- deplacement delimite par les deux arcs    --
-- comme definie par McMaster                --
-- Pour calculer cette surface pour deux     --
-- arcs ayant des extremites disjointes      --
-- celles-ci sont reliees pour fermer la     --
-- surface                                   --
-----------------------------------------------

  function surface_deplacement(arc_or, arc_gen : point_liste_reel;
                               N_or, N_gen : natural) return float;

  function surface_deplacement(arc_or, arc_gen : point_liste_type;
                               N_or, N_gen : natural) return float;

-------------------------------------------------------
-- COEFFICIENT DE CENTRAGE                           --
--                                                   --
-- Cette fonction retourne la mesure                 --
-- define par :                                      --
-- 2*min(Surf_droite/Surf_tot,Surf_gauche/surf_tot)  --
-------------------------------------------------------

  function coef_centrage(arc_or, arc_gen : point_liste_reel;
                         N_or, N_gen : natural) return float;

  function coef_centrage(arc_or, arc_gen : point_liste_type;
                         N_or, N_gen : natural) return float;

-----------------------------------------------
-- DISTANCE DE HAUSDORFF                     --
--                                           --
-- Cette fonction retourne l'approximation   --
-- de la distance de Hausdorff calculee      --
-- comme le maximum des distances d'un       --
-- point intermediaire d'un des arcs a       --
-- l'autre arc (ce qui differe dans quelques --
-- cas de la definition theorique )          --
-----------------------------------------------

  function Hausdorff(arc_or, arc_gen : point_liste_reel;
                     N_or, N_gen : natural) return float;

  function Hausdorff(arc_or, arc_gen : point_liste_type;
                     N_or, N_gen : natural) return float;

--------------------------------------------------------
-- NOMBRE DE VIRAGES APRES LISSAGE FORT               --
--                                                    --
-- Cette fonction retourne le rapport du nombre de virages       --
-- apres lissage de l'arc                             --
-- La force de ce lissage est: 8 * largeur du symbole --
--------------------------------------------------------
  function nb_virages_lisses(	arc : point_liste_reel; 
		  		N : natural;
                  		sigma : float) return natural;

  function nb_virages_lisses(	arc : point_liste_type; 
		  		N : natural;
                  		sigma : float) return natural;

  function virages_lissage_fort(arc_or, arc_gen : point_liste_reel;
                                N_or, N_gen : natural;
				l_gen : float) return float;

  function virages_lissage_fort(arc_or, arc_gen : point_liste_type;
                                N_or, N_gen : natural;
				l_gen : float) return float;

--------------------------------------------------------
-- NOMBRE DE VIRAGES APRES LISSAGE FAIBLE             --
--                                                    --
-- Cette fonction retourne le rapport du nombre de virages       --
-- apres lissage de l'arc                             --
-- La force de ce lissage est: 1 * largeur du symbole --
--------------------------------------------------------
  function virages_lissage_faible(arc_or, arc_gen : point_liste_reel;
                                 N_or, N_gen : natural;
				 l_gen : float) return float;

  function virages_lissage_faible(arc_or, arc_gen : point_liste_type;
                                 N_or, N_gen : natural;
				 l_gen : float) return float;

-------------------------------------------------------
-- MOYENNE DE LA VALEUR ABSOLUE DES ANGLES           --
--                                                   --
-- Cette fonction retourne la moyenne en radians de  --
-- la valeur absolue des angles                      --
-------------------------------------------------------
  function moyenne_angles(arc_or, arc_gen : point_liste_reel;
                          N_or, N_gen : natural ) return float;


  function moyenne_angles(arc_or, arc_gen : point_liste_type;
                          N_or, N_gen : natural ) return float;

  function moyenne_angles(arc : point_liste_reel;
                          N   : natural ) return float;

  function moyenne_angles(arc : point_liste_type;
                          N   : natural ) return float;

-------------------------------------------------------------------
-- AJout 2 mesures :Xavier CHEIPPE 18/04/97
-------------------------------------------------------------------
------------------------------------------------------------------------
-- Direction privilegiee d'une ligne
--------
-- ma_ligne : ligne dont on veut connaitre la direction principale
-- nb_points : son nombre de points
-- angle : l'angle de la direction principale
-- coef_confiance : coefficient de confiance, entre 0 et 1, caracterisant
--                  le degre de signification de la l'angle calcule. Plus
--                  il est proche de 1 plus l'ensemble des segments a la
--                  meme orientation generale.
-------------------------------------------------------------------------

procedure direction_principale(ma_ligne : point_liste_reel;
                               nb_points: natural;
                               angle,coef_confiance : out float);

---------------------------------------------------------------
-- Ecart de direction
-- Ecart d'angle entre la ligne origine et la ligne generalisee
-- Ecart de confiance entre celles-ci
---------------------------------------------------------------
procedure ecart_direction(ma_ligne,ma_ligne_g : point_liste_reel;
                          nb_points,nb_points_g: natural;
                          ecart_angle,coef_confiance : out float);
                                                                  


end Mesures_Comparaison;
