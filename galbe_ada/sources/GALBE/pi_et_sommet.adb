-- ****************************************************************
-- ** Package de detection des points d'inflexion et des sommets **
-- ****************************************************************
-- auteurs: FL, CP, CD
-- derniere retouche SM 02/99
-- Inflexions par zones plates pas encore retouche
  
with Courbure;
with Geometrie; use Geometrie;
with Lissage_filtrage; use Lissage_filtrage;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

with math_int_basic; 

package body PI_et_sommet is

  PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)

--*****************************************************************************
-- Procedure Det_position_PI - CP modif CD
--*****************************************************************************
-- Pour une ligne en coord. reelles Ligne_lisse, renvoie un tableau des 
-- positions des points precedant un changement de direction. Si on veut
-- determiner les inflexions avec un certain niveau de lissage, la ligne en
-- entree doit deja etre lissee.

Procedure Det_position_PI(Ligne_lisse	: IN Point_liste_reel;
			Tpoints_pred_pi	: OUT Liens_access_type) is

   npi,ntotal,nbis,
	nb_pts_inflex	: natural;
   Tprove   	  : reel_tableau_type(Ligne_lisse'first..(Ligne_lisse'last-2)); 
   tab_condition  : array(Ligne_lisse'first..Ligne_lisse'last-2) of boolean
			:= (others=>TRUE);
   tpi: liens_array_type(1..Ligne_lisse'length); -- <=> Tpoints_pred_pi ! --
   cpt: liens_array_type(1..Ligne_lisse'length-2) := (others=>1);
   cptbis: liens_array_type(1..Ligne_lisse'length-2) := (others=>0);
   signeA, signe: boolean;
   seuil_micro_inflex : integer := 2;
   vect1,vect2	: Point_type_reel;

Begin

-- Determination signe du virage associe a chaque vertex
  For i in Ligne_lisse'first..(Ligne_lisse'last-2) loop

    vect1 := (Ligne_lisse(i).Coor_x-Ligne_lisse(i+1).Coor_x,
		Ligne_lisse(i).Coor_y-Ligne_lisse(i+1).Coor_y);
    vect2 := (Ligne_lisse(i+2).Coor_x-Ligne_lisse(i+1).Coor_x,
		Ligne_lisse(i+2).Coor_y-Ligne_lisse(i+1).Coor_y);

    Tprove(i) := 
	Geometrie.Vectoriel(vect1,vect2);
  End loop;

  For i in Ligne_lisse'first..(Ligne_lisse'last-2) loop
    tab_condition(i) := (Tprove(i) >= 0.0);
  End Loop;

-- Determination alternance des signes
  nb_pts_inflex := 1;
  tpi(1) := 1;
  npi := 1;
  cpt(1) := 1;
  For i in (Ligne_lisse'first+1)..(Ligne_lisse'last-2) loop
    If tab_condition(i) = tab_condition(i-1) then
      cpt(npi) := cpt(npi) + 1;
    Else
      npi := npi + 1;
    End If;
  End loop;

-- Detection et elimination des micro-inflexions;
  nbis := 1;
  cptbis(1) := cpt(1);
  signeA := true;
  signe := signeA;
  For i in 2..npi loop
    signe := not(signe);
    If cpt(i) >= seuil_micro_inflex then
      If signe = signeA then -- concatenation de 2 virages ds le meme sens
        cptbis(nbis) := cptbis(nbis) + cpt(i);
      Else -- changement de sens
        nbis := nbis + 1;
        cptbis(nbis) := cpt(i);
        signeA := not(signeA);
      End if;
    Else -- elimination d'1 micro-inflexion par concatenation avec le virage
	 -- qui precede 
      cptbis(nbis) := cptbis(nbis) + cpt(i);
    End if;
  End loop;

-- Elimination eventuelle d'une micro-inflexion en tout debut de ligne:
-- dans le cas ou la premiere inflexion est une micro-inflexion isolee,
-- elle n'a pu etre eliminee par le filtre precedent. On va donc 
-- la concatener avec l'inflexion qui la suit et qui elle, n'est pas
-- (par construction) une micro-inflexion

  If (nbis > 1) and then (cptbis(1) = 1) then
    cptbis(1) := cptbis(1) + cptbis(2);
    nbis := nbis-1;
    For i in 2..nbis-1 loop
      cptbis(i) := cptbis(i+1);
    End Loop;
  End If;

-- Recapitulation : numeros des points precedant les PI
-- Rappel : le 1er et le dernier point de la ligne sont des PI
  ntotal := 0;
  For i in 1..nbis-1 loop
    ntotal := ntotal + cptbis(i);
    nb_pts_inflex := nb_pts_inflex + 1;
    tpi(nb_pts_inflex) := ntotal + 1;
  End loop;
  nb_pts_inflex := nb_pts_inflex + 1;
  tpi(nb_pts_inflex) := Ligne_lisse'last;

  Tpoints_pred_pi := New Liens_array_type'(tpi(1..nb_pts_inflex));
End Det_position_PI;

--*****************************************************************************
-- Procedure Det_coord_PI - CP modif CD
--*****************************************************************************
-- Determine les coordonnees des points d'inflexion de la ligne Ligne,
-- a partir de numeros des points precedant ces inflexions

Procedure Det_coord_PI(ligne            : in point_liste_type;
			Tpoints_pred_pi	: in liens_array_type;
			Coord_PI	: out point_access_type) is

  inflex,npi	: integer; 
  Coord_PI2	: Point_liste_type(1..Tpoints_pred_PI'length);

Begin

  npi := 1;
  Coord_PI2(npi):= Ligne(Ligne'first);
 
  for i in (Tpoints_pred_PI'first+1)..(Tpoints_pred_PI'last-1) loop  
    npi := npi + 1;
    inflex:= Tpoints_pred_pi(i);
    -- Estimation des coord. du pt d'inflex au milieu du segment du point pred 
    -- au point suivant:              
    Coord_PI2(npi).coor_x:=(ligne(inflex).coor_x+ligne(inflex+1).coor_x)/2;
    Coord_PI2(npi).coor_y:=(ligne(inflex).coor_y+ligne(inflex+1).coor_y)/2;
  end loop;

  npi := npi + 1;
  Coord_PI2(npi) := Ligne(Ligne'last);

  Coord_PI := New Point_liste_type'(Coord_PI2);
End Det_coord_PI;


------------------------------------------------------------------------
--                            INFLEXIONS
------------------------------------------------------------------------
--*****************************************************************************
-- Procedure Inflexions  - methode CP
--*****************************************************************************
-- Calcule les points d'inflexion sur une ligne avec un niveau de lissage
-- donne en entree (sigma). Sont renvoyes, les numeros des points precedant
-- les PI et les coordonnees des PI.

Procedure Inflexions(Ligne	: IN Point_liste_type;
		Sigma		: IN Float;
		Tpoints_pred_PI	: OUT Liens_access_type;
		Coord_PI	: OUT Point_access_type;
		Pas_segm	: IN Float := 1.0) is

Ligne_reelle,
Ligne_lisse	: Point_liste_reel(1..Ligne'length);
Tpoints_pred_PI2: Liens_access_type;

Begin
  If Ligne'length > 3 then

--  Transformation de la ligne en coord. reelles
    For i in Ligne'range loop
      Ligne_reelle(i).Coor_x := Float(Ligne(i+ligne'first-1).Coor_x);
      Ligne_reelle(i).Coor_y := Float(Ligne(i+ligne'first-1).Coor_y);
    End loop;

--  Lissage
    Lissage_Filtrage.Filtre_Gaussien_R(Ligne_reelle,Ligne_reelle'length,
	sigma,Ligne_lisse);
    
    Det_position_PI(Ligne_lisse,Tpoints_pred_PI2);
    Det_coord_PI(Ligne,Tpoints_pred_PI2.all,Coord_PI);
    
    Tpoints_pred_PI := Tpoints_pred_PI2;
  Else
    Tpoints_pred_PI := null;
    Coord_PI := null;
  End if;
End Inflexions;



------------------------------------------------------------------------
--                         DET_COORD_PI_PRECIS
------------------------------------------------------------------------
-- CD / retouche SM 09/98
Procedure Det_coord_PI_precis(Ligne		: IN point_liste_type;
		              Lgr_segment	: IN Liens_array_type;
		              Tpoints_pred_PI	: IN Liens_Array_type;
                              Pred_PI_final    : OUT Liens_access_type;
		              Coord_PI_final   : OUT Point_access_type) is



 No_pred : natural;
 Reste : float;
 Mil  : Point_liste_type(Tpoints_pred_PI'range);
 PI : liens_array_type(Tpoints_pred_PI'range);
 P : point_type_reel;

begin
  PI(1):= 1;
  Mil(1) := ligne(1);
  for i in Tpoints_pred_PI'first+1..Tpoints_pred_PI'last-1 loop 
    Situe_sur_ini(Lgr_segment,Lgr_segment'last,Tpoints_pred_PI(i),no_pred,reste);
    PI(i) := No_pred; 
    P := Reel(Ligne(No_pred)) + reste*Reel(ligne(no_pred+1) - ligne(no_pred));
    Mil(i) := Entier(P);
  end loop;
  PI(Tpoints_pred_PI'last) := Ligne'last;
  Mil(Tpoints_pred_PI'last) := Ligne(Ligne'last);

  Pred_Pi_final := new Liens_array_type'(PI);
  Coord_PI_final := new Point_liste_type'(Mil);
end Det_coord_PI_precis; 


------------------------------------------------------------------------
--                            INFLEXIONS_ANGLES_PRECIS
------------------------------------------------------------------------
-- Idem inflexions_angle mais recale le PI plus precisement
-- CD / retouche SM 09/98
Procedure Inflexions_angle_precis( ligne 	    : in point_liste_type;
                                   sigma 	    : in float;
  			           Tpoints_pred_pi : out liens_access_type;
  			           Coord_PI        : out point_access_type) is

  Lisse_access     : Point_access_reel;
  Pred_PI          : Liens_access_type;
  Decomp	   : Point_access_reel;
  N_decomp         : natural;
  Pas              : float := 1.0;
  lgr_segments	   : liens_array_type(Ligne'range);
  
Begin
  Decompose_ligne(Ligne,Ligne'last,Decomp,N_decomp,Pas,Lgr_segments);
  if Sigma = 0.0 then
    Lisse_access := new Point_liste_reel'(Decomp(1..N_decomp));
  else
    Lisse_access := new Point_liste_reel(1..N_decomp);
    Filtre_Gaussien_R(decomp.all,n_decomp,sigma,lisse_access.all);
  end if;
  Gr_Free_Point_liste_reel(decomp);

  Det_position_pi(Lisse_access.all,Pred_PI);
  GR_free_point_liste_reel(Lisse_access);

  -- a ce stade on a les positions des pi sur la ligne decomposee,
  -- avec lgr_segments on va repasser a la ligne originale
  Det_coord_pi_precis(Ligne,Lgr_segments,Pred_PI.all,Tpoints_pred_PI,Coord_PI);
  GR_free_liens_array(Pred_PI);

end Inflexions_angle_precis;

------------------------------------------------------------------------
--                            DET_POSITION_PI_PLAT
------------------------------------------------------------------------
-- Determine les positions des PI de la ligne reelle "ligne"
-- Tpoints_pi_plus et Tpoints_pi_moins ont le meme nb d'elts, qui
-- sont 2 a deux egaux a 1 pres (corresp. sens de parcours)
-- Le ieme PI est le milieu du segment "T+(i)T-(i)"
-- CD / retouche SM 09/98
------------------------------------------------------------------------
Procedure Det_position_PI_plat(Ligne	        : in Point_liste_reel;
			       Seuil            : in float;
			       Tpoints_pi_plus  : OUT Liens_access_type;
			       Tpoints_pi_moins : OUT Liens_access_type) is

Tpoints_PI_plus2,
Tpoints_PI_moins2 : Liens_access_type;
Tprove		: Reel_tableau_type(1..Ligne'last-2);
Tab_condition	: Array(1..Ligne'last-2) of integer; -- rempli de 1, 0, -1...
tpiplus,
tpimoins,
tpi             : Liens_array_type(1..Ligne'last); -- pi fn du sens de parcours
signe		: integer; -- 1 ou -1
npiplus,
npimoins	: natural;
premier		: natural;
p1,p2		: natural;
npinflex	: natural;
compt,
signe_prec,
signe_act	: integer;

Begin
  -- Calcul des produits vectoriels entre les segments de la ligne
  for i in 1..Ligne'last-2 loop
    if Ligne(i) = Ligne(i+1) or Ligne(i+1) = Ligne(i+2) then
      Tprove(i) := 0.0;
    else
-- EST-CE LE BON PRODUIT VECTORIEL DE GEOMETRIE??????
      Tprove(i) := p_vectoriel(ligne(i),ligne(i+1),ligne(i+2));
    end if;
  end loop;
  -- Traduction en signe de la courbure avec intro de zones quasi-plates
  for i in 1..Ligne'last-2 loop
    if Tprove(i) >= seuil then
      Tab_condition(i):=1;
    elsif Tprove(i) <= -seuil then
      Tab_condition(i):=-1;
    else
      Tab_condition(i):=0;
    end if;
  end loop;

  -- Lissage des micro-inflexions
  compt := 0; -- compteur des produits vectoriels significatifs successifs
  signe_prec := 0;
  for i in 1..Ligne'last-2 loop
    signe_act := Tab_condition(i);
    if signe_act=0 then
      -- Sequence +0 ou -0 ou 00
      compt := 0;
    elsif signe_act=signe_prec then
      -- Sequence ++ ou --
      compt := compt+1;
    elsif compt = 1 then
      -- Sequence +- ou -+ isolee, on reaffecte a Tab_condition(i) et
        -- Tab_condition(i-1) (et a signe_act) la valeur de 
      -- Tab_condition(i-2), sauf si i=2 auquel cas on leur affecte 0.
      -- Et on remet compt a 0.
      if i=2 then
        Tab_condition(1):=0;
        Tab_condition(2):=0;
        signe_act := 0;
      else
        Tab_condition(i-1):=Tab_condition(i-2);
        Tab_condition(i):=Tab_condition(i-2);
        signe_act := Tab_condition(i-2);
      end if;
      compt :=0;
    else
      -- Sequence 0+ ou 0- ou --+ ou ++-, on prend acte du changt de signe
      compt := 1;
    end if;

    -- Et dans tous les cas, avant d'iterer...
    signe_prec := signe_act;
  end loop;


  -----------------------------------
  -- Parcours de la ligne sens positif (devrait etre en procedure exterieure)
  -- Detection du premier produit vect significatif
  signe := 0;
  for i in 1..Ligne'last-2 loop 
    if Tab_condition(i)/=0 then
      signe:=Tab_condition(i); -- 1 ou -1
      premier := i;
      exit;
    end if;
  end loop;
  --Puis detection des points precedant des inflexions, sens positif
  if signe = 0 then
    -- Alors on n'a pas de point d'inflexion
    npiplus := 0;
  else
    -- Alors signe vaut 1 ou -1 et premier existe
    npiplus := 0;
    for i in premier+1..Ligne'last-2 loop
      if Tab_condition(i)=-signe then
        npiplus:=npiplus+1;
        tpiplus(npiplus):=i;
        signe:=-signe;
      end if;
    end loop;
  end if;

  -----------------------------------
  -- Parcours de la ligne sens negatif (devrait etre en procedure exterieure)
  -- Detection du premier produit vect significatif
  signe := 0;
  for i in reverse 1..Ligne'last-2 loop
    if Tab_condition(i)/=0 then
      signe:=Tab_condition(i); -- 1 ou -1
      premier := i;
      exit;
    end if;
  end loop;
  --Puis detection des points precedant des inflexions, sens negatif
  if signe = 0 then
    -- Alors on n'a pas de point d'inflexion
    npimoins := 0;
  else
    -- Alors signe vaut 1 ou -1 et premier existe
    npimoins := 0;
    for i in reverse 1..premier-1 loop
      if Tab_condition(i)=-signe then
        npimoins:=npimoins+1;
        tpimoins(npimoins):=i;
        signe:=-signe;
      end if;
    end loop;
  end if;

  ------------------------------
  -- On recolle les morceaux: les Pi sont recales entre les
  -- chgt de direction dans un sens et dans l'autre
  if npiplus /= npimoins then
    -- Probleme : Nombre de points d'inflexion dans un sens puis d'ans l'autre differents
    Null;
  else
    npinflex := npiplus;
    Tpoints_PI_plus := New Liens_array_type(1..npinflex);
    Tpoints_PI_moins := New Liens_array_type(1..npinflex);
    if npinflex /= 0 then
    -- les points tpiplus(i) et tpimoins(nb+1-i) sont homologues
      for i in 1..npinflex loop
        p1 := tpiplus(i);
        p2 := tpimoins(npinflex+1-i);
        if (Float(p1+p2)/2.0 = Float((p1+p2)/2)) then
        -- (p1+p2) pair
          Tpoints_PI_plus2(i) := (p1+p2)/2;
          Tpoints_PI_moins2(i):= Tpoints_PI_plus2(i);
        else
        -- (p1+p2) impair
          Tpoints_PI_plus2(i) := (p1+p2-1)/2;
          Tpoints_PI_moins2(i):= Tpoints_pi_plus2(i)+1;
        end if; --pair/impair
      end loop; --npinflex
    end if; --npinflex/=0
  end if; -- npiplus et npimoins differents

  Tpoints_PI_plus := Tpoints_PI_plus2;
  Tpoints_PI_moins := Tpoints_PI_moins2;

end Det_position_pi_plat;


------------------------------------------------------------------------
--                            DET_COORD_PI_PLAT
------------------------------------------------------------------------
-- Determine les coordonnees des PI de la ligne initiale
-- Methode des zones quasi-plates
-- CD / retouche SM 09/98
------------------------------------------------------------------------
Procedure Det_coord_pi_plat(Ligne		: IN Point_liste_type;
			    Lgr_segment	        : IN Liens_array_type;
			    Tpoints_PI_plus	: IN Liens_array_type;
			    Tpoints_PI_moins    : IN Liens_array_type;
			    Coord_PI	        : OUT Point_access_type) is

Coord_PI2 : Point_liste_type(Tpoints_PI_plus'range);
p1, p2	  : Point_type_reel;
Numero	  : natural;
Reste	  : float;
                
Begin
  for i in Tpoints_PI_plus'range loop
    if Tpoints_PI_plus(i) = Tpoints_PI_moins(i) then
      Situe_sur_ini(Lgr_segment,Lgr_segment'last,Tpoints_pi_plus(i),numero,reste);
      if numero=Ligne'last then
        Coord_PI2(i) := ligne(Ligne'last);
      else
        Coord_PI2(i) := Entier(Reel(Ligne(Numero)) + Reste* Reel(Ligne(Numero+1)-Ligne(Numero))); 
      end if;
    else
      Situe_sur_ini(lgr_segment,Ligne'last,Tpoints_pi_plus(i),numero,reste);
      if numero=Ligne'last then
        p1 := Reel(Ligne(Ligne'last));
      else
        p1 := Reel(Ligne(Numero)) + Reste*Reel(Ligne(Numero+1)-Ligne(Numero));
      end if;
      Situe_sur_ini(lgr_segment,Ligne'last,Tpoints_pi_moins(i),numero,reste);
      if numero=Ligne'last then
        p2 := Reel(Ligne(Ligne'last));
      else
        p2 := Reel(Ligne(Numero)) + Reste*Reel(Ligne(Numero+1)-Ligne(Numero));
      end if;
      Coord_PI2(i) := Entier((p1+p2)/2.0); 
    end if; --T+(i) /= T-(i)
  end loop;
  
  Coord_PI := new Point_liste_type'(Coord_PI2);
end Det_coord_pi_plat;


------------------------------------------------------------------------
--                            INFLEXIONS_PLAT
------------------------------------------------------------------------
-- CD / retouche SM 09/98
Procedure Inflexions_plat(Ligne	          : IN Point_liste_type;
			  Sigma		  : IN float;
                          Tpoints_pred_PI : OUT Liens_access_type;
			  Coord_PI        : OUT Point_access_type) is

Pas : float := 1.0;
Decomp_access    : Point_access_reel;
N_decomp         : natural;
Lisse_access     : Point_access_reel;
Lgr_segments     : Liens_array_type(Ligne'range);
Tpoints_pi_plus,
Tpoints_pi_moins : Liens_Access_type;
Seuil            : float := pas*pas*0.01; -- soit angle tq sinus < 0.1

Begin

  Decompose_ligne(Ligne,Ligne'last,decomp_access,n_decomp,pas,lgr_segments);
  if Sigma  = 0.0 then
    Lisse_access := Decomp_access;
  else
    Lisse_access := new Point_liste_reel(Decomp_access'range);
    Filtre_Gaussien_R(decomp_access.all,N_decomp,sigma,lisse_access.all);
    GR_free_point_liste_reel(Decomp_access);
  end if;

  Det_Position_PI_plat(Lisse_access.all,Seuil,
	               Tpoints_pi_plus,Tpoints_pi_moins);

  -- a ce stade on a les positions des pi dans les deux sens
  -- sur la ligne decomposee,
  -- avec lgr_segments on va repasser a la ligne originale
  Det_coord_pi_plat(Ligne,Lgr_segments,Tpoints_pi_plus.all,
                    Tpoints_pi_moins.all, Coord_PI);

  Tpoints_pred_PI := Tpoints_pi_plus;
  Gr_Free_liens_array(Tpoints_pi_moins);

End Inflexions_plat; 

------------------------------------------------------------------------
--                            RECALAGE_PI_MILIEU_SOMMETS
------------------------------------------------------------------------
-- Cette procedure devrait etre adaptee a la caracterisation des virages
-- faisant partie d'une serie.
-- Elle determine une premiere fois les points d'inflexion, puis les sommets
-- entre ces points d'inflexion, et enfin recale chaque PI a egale distance
-- des deux sommets qui l'entourent
--  SM 09/98 d'apres CD
------------------------------------------------------------------------
Procedure Recalage_PI_milieu_sommets(ligne                 : IN point_liste_type;
  				     Tpoints_pred_pi       : OUT liens_access_type;
  				     Coord_PI              : OUT point_access_type;
				     Tpoints_pred_sommets  : IN liens_array_type;
				     Coord_sommets	   : IN Point_liste_type) is
Reste,
Abs_curv1,
Abs_curv2,
Abs_PI     : float;
Pred_PI    : natural;
Coord_PI2  : Point_access_type;
Tpoints_pred_PI2 : Liens_access_type;

Begin
  Coord_PI2 := new Point_liste_type(Coord_sommets'first..Coord_sommets'last+1);
  Tpoints_pred_PI2 := new Liens_array_type(Coord_sommets'first..Coord_sommets'last+1);

  Coord_PI2(Coord_PI2'first) := Ligne(Ligne'first);
  Tpoints_pred_PI2(Tpoints_pred_PI2'first) := Ligne'first;
  for i in Coord_sommets'first..Coord_sommets'last-1 loop
    if Ligne(Tpoints_pred_sommets(i)) /= Ligne(Tpoints_pred_sommets(i)+1) then
      Reste := Norme(Ligne(Tpoints_pred_sommets(i)),Coord_sommets(i)) /
               Norme(Ligne(Tpoints_pred_sommets(i)),Ligne(Tpoints_pred_sommets(i)+1));
    else
      Reste := 0.0;
    end if;
    Position_to_abs_curv(Ligne, Ligne'last, Tpoints_pred_sommets(i), Reste, Abs_curv1);

    if Tpoints_pred_sommets(i+1) /= Ligne'last and then
       Ligne(Tpoints_pred_sommets(i+1)) /= Ligne(Tpoints_pred_sommets(i+1)+1) then
      Reste := Norme(Ligne(Tpoints_pred_sommets(i+1)),Coord_sommets(i+1)) /
               Norme(Ligne(Tpoints_pred_sommets(i+1)),Ligne(Tpoints_pred_sommets(i+1)+1));
    else 
      Reste := 0.0;
    end if;
    Position_to_abs_curv(Ligne, Ligne'last, Tpoints_pred_sommets(i+1), Reste, Abs_curv2);

    Abs_PI := (Abs_curv1 + Abs_curv2)/ 2.0;
    Abs_curv_to_position(Ligne, Ligne'last, Abs_PI, Pred_PI, Reste);
    Tpoints_pred_PI2(i+1) := Pred_PI;
    Coord_PI2(i+1) := Entier(Reel(Ligne(Pred_PI))+reste*Reel(Ligne(Pred_PI+1)-Ligne(Pred_PI)));
  end loop;

  Coord_PI2(Coord_PI2'last) := Ligne(Ligne'last);
  Tpoints_pred_PI2(Tpoints_pred_PI2'last) := Ligne'last;

  Coord_PI := Coord_PI2;
  Tpoints_pred_PI := Tpoints_pred_PI2;

End Recalage_PI_milieu_sommets;

-- ****************************************************************
-- PROCEDURES DE DETECTION DES SOMMETS
-- ****************************************************************

------------------------------------------------------------------------
--                            SOMMETS_COURBURE_MB
------------------------------------------------------------------------
-- Procedure de detection des maximas de courbure, i.e. Sommets
-- Methode de Mathieu pour le calcul de courbure
-- Dans cette version, il y a un seul sommet par virage
-- CP
-- Attention: Sigma est le parametre du lissage des courbures
-- et non pas celui de la ligne comme pour les autres procedures
-- de ce package
------------------------------------------------------------------------
procedure Sommets_courbure_MB (Tabini               : IN  Point_liste_type;
 		               Sigma	            : IN  float;
  		               Tpoints_pred_pi      : IN  Liens_array_type;
                               Tpoints_pred_sommets : OUT Liens_access_type;
  		               Tab_sommets	    : OUT Point_access_type ) is

  pas		: float := 1.0; -- Decoupage de l'arc en pts equidistants

  Ligne_aux     : Point_access_reel;
  Tcourbure_aux : Reel_access_type;
  nb_segments   : Liens_array_type(Tabini'first..Tabini'last-1);
  Tcourbure     : Reel_tableau_type(Tabini'range);
  l,
  N_aux         : natural;
  
  Dmax,
  D             : float;
  Ns            : natural;
  Tpoints_pred_sommets2 : Liens_array_type(Tpoints_pred_PI'first..Tpoints_pred_PI'last-1);
  Tab_sommets2  : Point_liste_type(Tpoints_pred_PI'first..Tpoints_pred_PI'last-1);

 begin
  -- calcul des courbures sur la ligne decomposee
  Decompose_ligne(tabini,tabini'last,ligne_aux,N_aux,pas,nb_segments);
  Tcourbure_aux := new Reel_tableau_type(1..N_aux);
  Courbure.courbure_mathieu(Ligne_aux(1..n_aux),15.0,4.0,Tcourbure_aux.all);
-- NB 15.0 et 4.0 sont pour l'instant mis au pif!!!
-- voir avec la nouvelle version de la courbure de MB
  -- correspondance avec les courbures sur la ligne originale
  l:= 1;
  Tcourbure(1):= Tcourbure_aux(1);
  for i in 2..Tabini'last loop
    l:= l + nb_segments(i-1);
    Tcourbure(i):= Tcourbure_aux(l);
  end loop;
  GR_free_point_liste_reel(Ligne_aux);
  GR_free_reel_tableau(Tcourbure_aux);

  -- Calcul du max de courbure sur le virage original non decompose
  for i in Tpoints_pred_PI'first..Tpoints_pred_PI'last-1 loop 
    -- boucle sur les virages
    Dmax := 0.0;
    -- Correcion FL 07/04/99 : pour que le dernier point de la ligne,
    -- qui est PI et point_pred_pi par definition, ne soit pas aussi un sommet
    for j in Tpoints_pred_PI(i)+1..math_int_basic.Min(Tpoints_pred_PI(i+1),
                                                   Tpoints_pred_pi'last-1) loop 
      -- boucle sur les points du virage
      D := Abs(Tcourbure(j));
      if D > Dmax then 
        Dmax := D;
        Ns := j;
      end if;
    end loop;
    if Dmax = 0.0 then -- virage tout plat
      -- le sommet est alors mis au milieu du virage
      Ns:= Tpoints_pred_pi(i)+1+(Tpoints_pred_pi(i+1)-(Tpoints_pred_pi(i)+1))/2;
    end if;
    Tab_sommets2(i) := Tabini(Ns);
    Tpoints_pred_sommets2(i) := Ns;
  end loop;
  Tab_sommets := new Point_liste_type'(Tab_sommets2);
  Tpoints_pred_sommets := new Liens_array_type'(Tpoints_pred_sommets2);


end Sommets_courbure_MB;

------------------------------------------------------------------------
--                            SOMMETS_BASE
------------------------------------------------------------------------
-- Procedure de detection des sommets
-- Approximation au point le plus eloigne (en projection orthogonale)
-- de la droite joignant les points d'inflexion
------------------------------------------------------------------------
procedure Sommets_base (Tabini               : IN  Point_liste_type;
  		        Tpoints_pred_PI      : IN  Liens_array_type;
                        Tab_PI               : IN  Point_liste_type;
                        Tpoints_pred_sommets : OUT Liens_access_type;
                        Tab_sommets	     : OUT Point_access_type ) is

 Dmax,
 D           : float;
 Ns : natural;
 Tpoints_pred_sommets2 : Liens_array_type(Tab_PI'first..Tab_PI'last-1);
 Tab_sommets2 : Point_liste_type(Tab_PI'first..Tab_PI'last-1);

begin
  for i in Tab_PI'first..Tab_PI'last-1 loop -- boucle sur les virages
    Dmax := 0.0;
    for j in Tpoints_pred_PI(i)+1..Tpoints_pred_PI(i+1) loop 
      -- boucle sur les points du virage
      D := distance_a_ligne(Tabini(j),Tab_PI(i),Tab_PI(i+1),C_ligne_type);
      if D > Dmax then 
        Dmax := D;
        Ns := j;
      end if;
    end loop;
    if Dmax = 0.0 then -- virage tout plat
      -- le sommet est alors mis au milieu du virage
      Ns:= Tpoints_pred_pi(i)+1+(Tpoints_pred_pi(i+1)-(Tpoints_pred_pi(i)+1))/2;
    end if;
    Tab_sommets2(i) := Tabini(Ns);
    Tpoints_pred_sommets2(i) := Ns;
  end loop;
  Tab_sommets := new Point_liste_type'(Tab_sommets2);
  Tpoints_pred_sommets := new Liens_array_type'(Tpoints_pred_sommets2);

end Sommets_base;



-- ****************************************************************
-- PROCEDURE RECAPITULATIVE DES DIFFERENTES APPROCHES
-- PROCEDURES INTERNES
-- (voir mode d'emploi dans les specifs)
-- SM 09/98
-- ****************************************************************

------------------------------------------------------------------------
--                            INFLEXIONS_SOMMETS_CASE
------------------------------------------------------------------------
-- VALIDE SM
Procedure Inflexions_sommets_case(Ligne               : IN Point_liste_type;
                                  Points_pred_PI      : OUT Liens_access_type;
                                  Coord_PI            : OUT Point_access_type;
                                  Points_pred_sommets : OUT Liens_access_type;
                                  Coord_sommets       : OUT Point_access_type;
                                  Det_PI              : IN Type_detection_PI 
                                                      := Chgt_d_angle_precis;
                                  Det_sommets         : IN Type_detection_sommets 
                                                      := Eloignement_a_la_base;
                                  Sigma               : IN float 
                                                      := 20.0) is

Points_pred_PI2,
Points_pred_sommets2 : Liens_access_type;
Coord_PI2,
Coord_sommets2       : Point_access_type;
-- pour xav:
PIx : Point_liste_reel(1..2000);
N_PIx : natural;
Pred_PIx : geomxav.int_tableau_type(1..2000);
apti : reel_tableau_type(1..2000);

begin
  -- Detection des points d'inflexion
  case Det_PI is
    when Chgt_d_angle =>
      Inflexions(Ligne, Sigma, Points_pred_PI2, Coord_PI2,1.0);
    when Chgt_d_angle_precis =>
      Inflexions_angle_precis(Ligne, Sigma, Points_pred_PI2, Coord_PI2);
    when Milieu_des_sommets =>
      -- une premiere approximation des PI est faite par la methode des angles
      Inflexions(Ligne, Sigma, Points_pred_PI2, Coord_PI2);
    when Milieu_des_zones_plates =>
      Inflexions_plat(Ligne, Sigma, Points_pred_PI2, Coord_PI2);
    when Courbure_xavier =>
      Pt_infl(Ligne, Ligne'last,PIx,N_PIx, Pred_PIx, apti);
      Points_pred_PI2 := new Liens_array_type(1..N_PIx+2);
      Points_pred_PI2(Points_pred_PI2'first) := Ligne'first;
      for i in 1..N_PIx loop
        Points_pred_PI2(i+1) := Pred_PIx(i);
      end loop;
      Points_pred_PI2(Points_pred_PI2'last) := Ligne'last;
      Coord_PI2 := new Point_liste_type'(Ligne(Ligne'first)&Entier(PIx(1..N_PIx))
                                         &Ligne(Ligne'last));      
  end case;

  -- Modif SM 20/04/99 pour traiter le cas des arcs sans PI intermediaire
  if points_pred_PI2 = null then
    Points_pred_PI2 := new Liens_array_type(1..2);
    Coord_PI2 := new Point_liste_type(1..2);
    Points_pred_PI2(1..2) := (Ligne'first,Ligne'last);
    Coord_PI2(1..2) := (Ligne(Ligne'first),Ligne(Ligne'last));
  end if;


  -- Detection des sommets
  case Det_sommets is
    when Eloignement_a_la_base =>
      Sommets_base(Ligne, Points_pred_PI2.all, Coord_PI2.all, 
                   Points_pred_sommets2, Coord_sommets2);
    when Max_courbure_MB =>
      Sommets_courbure_MB(Ligne, Sigma, Points_pred_PI2.all,  
                          Points_pred_sommets2, Coord_sommets2);
    when Pas_de_sommet => 
      if Det_PI = Milieu_des_sommets then -- on a besoin de sommets quand meme
        Sommets_courbure_MB(Ligne, Sigma, Points_pred_PI2.all, 
                            Points_pred_sommets2, Coord_sommets2);
      end if;
    when others =>
      null;
  end case;

  -- Recalage eventuel des points d'inflexion
  case Det_Pi is
    when Milieu_des_sommets =>
      GR_free_liens_array(Points_pred_PI2);
      GR_free_point_liste(Coord_PI2); 
      Recalage_PI_milieu_sommets(Ligne, Points_pred_PI2, Coord_PI2,
                                 Points_pred_sommets2.all, Coord_sommets2.all);
    when others =>
      null;
  end case;

  -- Menage
  if Det_sommets = Pas_de_sommet and Det_PI = Milieu_des_sommets then
    GR_free_liens_array(Points_pred_sommets2);
    GR_free_point_liste(Coord_sommets2); 
  end if;
  Points_pred_PI := Points_pred_PI2;
  Coord_PI := Coord_PI2;
  if Det_sommets /= Pas_de_sommet then
    Points_pred_sommets := Points_pred_sommets2;
    Coord_sommets := Coord_sommets2;
  end if;

end Inflexions_sommets_case;

------------------------------------------------------------------------
--                         INFLEXIONS_SOMMETS
------------------------------------------------------------------------
-- VALIDE SM
Procedure Inflexions_sommets(Ligne               : IN Point_liste_type;
                             Flag_auto		 : IN OUT Boolean;
			     Sigma_entree	 : IN float;
                             Points_pred_PI      : OUT Liens_access_type;
                             Coord_PI            : OUT Point_access_type;
                             Points_pred_sommets : OUT Liens_access_type;
                             Coord_sommets       : OUT Point_access_type;
                             Sigma_calcule       : OUT float;
                             Det_PI              : IN Type_detection_PI 
                                                 := Chgt_d_angle_precis;
                             Det_sommets         : IN Type_detection_sommets 
                                                 := Eloignement_a_la_base;
                             Sigma_min           : IN float 
                                                 := 2.0;
                             Sigma_max           : IN float 
                                                 := 100.0;
                             Angle_max           : IN float 
                                                 := 100.0) is
Points_pred_sommets2,
Points_pred_PI2 : Liens_access_type;
Coord_PI2,
Coord_sommets2	: Point_access_type;
Det_sommets2    : Type_detection_sommets;
Flag_termine	: boolean;
Alpha_PI_SOM	: float;
Sigma           : float;
 
begin

if flag_auto then
  -- on va determiner automatiquement sigma en bouclant de sigma_min
  -- a sigma_max en 50 passes
  -- des qu'on n'a plus de configuration ou Pi Sommeti Pi+1 
  -- fait un angle superieur a Angle_max degres, on a atteint le bon sigma

  -- Dans le cas ou Det_somets est mis a Pas_de_sommet, on lui affecte la valeur 
  -- Eloignement_a_la_base le temps de verifier la condition sur les angles.
  if Det_sommets = Pas_de_sommet then
    Det_sommets2 := Eloignement_a_la_base;
  else
    Det_sommets2 := Det_sommets;
  end if;
  for i in 0..50 loop
    Sigma:= Sigma_min+float(i)*(Sigma_max-Sigma_min)/50.0;
    Inflexions_sommets_case(Ligne, Points_pred_PI2, Coord_PI2, 
               Points_pred_sommets2,Coord_sommets2, Det_PI, Det_sommets2, Sigma);
    -- verification de la condition d'angles assez fermes
    Flag_termine:=true;
    for j in 1..Coord_PI2'last-1 loop
      Alpha_PI_SOM:=Angle_3points(Coord_PI2(j),Coord_sommets2(j),Coord_PI2(j+1));
      if abs(Alpha_PI_SOM)>Angle_max*pi/180.0 then 
        Flag_termine:=false;
        exit;
      end if;
    end loop;

    if Flag_termine=true then
      Sigma_calcule := Sigma;
      Points_pred_PI := Points_pred_PI2;
      Coord_PI := Coord_PI2;
      if Det_sommets /= Pas_de_sommet then
        Points_pred_sommets := Points_pred_sommets2;
        Coord_sommets := Coord_sommets2;
      else
        GR_free_point_liste(Coord_sommets2);
        GR_free_liens_array(Points_pred_sommets2);        
      end if;
      return;
    else
      GR_free_liens_array(Points_pred_PI2);
      GR_free_point_liste(Coord_PI2);
      GR_free_liens_array(Points_pred_sommets2);
      GR_free_point_liste(Coord_sommets2);
    end if;
  end loop;
end if;

Inflexions_sommets_case(Ligne, Points_pred_PI2, Coord_PI2,Points_pred_sommets2,
                        Coord_sommets2, Det_PI, Det_sommets, Sigma_entree);
Sigma_calcule := Sigma_entree;
Points_pred_PI := Points_pred_PI2;
Coord_PI := Coord_PI2;
if Det_sommets /= Pas_de_sommet then
   Points_pred_sommets := Points_pred_sommets2;
   Coord_sommets := Coord_sommets2;
else
   GR_free_point_liste(Coord_sommets2);
   GR_free_liens_array(Points_pred_sommets2);        
end if;

Flag_auto:=false;

end Inflexions_sommets;


------------------------------------------------------------------------
--                            HIERARCHIE_DES_PI
------------------------------------------------------------------------
-- CP / retouche SM 09/98
Procedure Hierarchie_des_PI(Ligne               : IN  Point_liste_type;
                            Det_PI              : IN  Type_detection_PI
                                                 := Chgt_d_angle_precis;
                            Sigma_faible        : IN  float;
                            Sigma_fort          : IN  float;
                            Points_pred_PI      : OUT Liens_access_type;
                            Coord_PI            : OUT Point_access_type;
			    Tniveau_pi   	: OUT Liens_access_type ) is
 Coord_PI2,
 Coord_PI_fort,
 Sommets         : Point_access_type;
 Tpoints_pred_PI_fort,
 Tpoints_pred_PI2,
 Pred_sommets,
 Tniveau_PI2     : Liens_access_type;
 nb_pts_inflex,
 nb_pts_inflex_fort,
 indice1, 
 indice2,
 borne1 ,
 pi_corres       : natural;
 min, 
 dpi             : float;
 point1, 
 point2          : Point_type;
 Det_sommets     : Type_detection_sommets;

begin
  if Det_PI = Milieu_des_sommets then
    Det_sommets := Max_courbure_MB;
  else
    Det_sommets := Pas_de_sommet;
  end if;
  -- Detection des inflexions et sommets sur la ligne faiblement lissee
  Inflexions_sommets_case(Ligne, Tpoints_pred_PI2, Coord_PI2, Pred_sommets,
                          Sommets, Det_PI, Det_sommets, Sigma_faible);
  Nb_pts_inflex := Coord_PI2'last;
  if Det_sommets /= Pas_de_sommet then
    GR_free_point_liste(Sommets);
    GR_free_liens_array(Pred_sommets);
  end if;
  -- Detection des inflexions sur la ligne fortement lissee
  Inflexions_sommets_case(Ligne, Tpoints_pred_PI_fort, Coord_PI_fort, Pred_sommets, Sommets,
                     Det_PI, Det_sommets, Sigma_fort);
  Nb_pts_inflex_fort := Coord_PI_fort'last;
  if Det_sommets /= Pas_de_sommet then
    GR_free_point_liste(Sommets);
    GR_free_liens_array(Pred_sommets);
  end if;
  
  Tniveau_PI2 := new Liens_array_type(Coord_PI2'range);

  Tniveau_PI2(1) := 1;
  Tniveau_PI2(Nb_pts_inflex) := 1;
  for i in 2..nb_pts_inflex-1 loop
    Tniveau_pi2(i):= 2;
  end loop;

  borne1:= 2;
  for i in 2..nb_pts_inflex_fort-1 loop
    -- on va rechercher pour chaque PI de la ligne lissee fortement
    -- un homologue sur la ligne faiblement lissee qui seront mis 
    -- au niveau 1 (homologue = le plus proche par abscisse curviligne)
    -- Tous les autres PI de la ligne faiblement lissee seront mis
    -- au niveau 2
    min := float'last;
    for j in borne1..nb_pts_inflex-1 loop
      if Tpoints_pred_PI2(j) = Tpoints_pred_PI_fort(i) then
        -- les deux homologues sont en fait confondus
        PI_corres:= j;
        exit;
      else
 	-- Calcul de la distance curviligne :
	if Tpoints_pred_PI2(j) > Tpoints_pred_PI_fort(i) then
          Indice1:= Tpoints_pred_PI_fort(i);
          Point1 := Coord_PI_fort(i);
          Indice2:= Tpoints_pred_PI2(j);
          Point2 := Coord_PI2(j);
        elsif Tpoints_pred_PI2(j) < Tpoints_pred_PI_fort(i) then
          Indice2:= Tpoints_pred_PI_fort(i);
          Point2 := Coord_PI_fort(i);
          Indice1:= Tpoints_pred_PI2(j);
          Point1 := Coord_PI2(j);
        end if;
	dpi:= norme(Point1,Ligne(Indice1+1));
 	for d in Indice1+1..Indice2-1 loop
          dpi:= dpi+norme(Ligne(d),Ligne(d+1));
	end loop;
	dpi:= dpi + norme(Ligne(Indice2),Point2);
	if dpi < min then
	  min:= dpi;
   	  pi_corres:= j;
	end if;  	
      end if;
    end loop;
    Tniveau_pi2(pi_corres):=1; -- Pi de niveau 1
    borne1:= pi_corres+1;
  end loop;

  GR_free_liens_array(Tpoints_pred_PI_fort);
  GR_free_point_liste(Coord_PI_fort);
  Points_pred_PI := Tpoints_pred_PI2;
  Coord_PI := Coord_PI2;
  Tniveau_PI := Tniveau_PI2;   

end Hierarchie_des_PI ;



------------------------------------------------------------------------------
-- Calcul des pts d'inflexion d'une ligne a partir d'interpolation par cubique
------------------------------------------------------------------------------
Procedure Pt_infl (	Ligne_ini	: in point_liste_type;
			n_pt_ini	: in natural;
			PTINFL		: out point_liste_reel;
			n_infl		: out natural;
			POS		: out Geomxav.int_tableau_type;
			APTI		: out reel_tableau_type) is
 Ligne,ligne_1				: point_liste_reel(1..2000);
 pente,P_seg				: reel_tableau_type(1..2000);
 prog					: Geomxav.int_tableau_type(1..2000);
 n_points,i,j,k,kk			: integer;
 X1,Y1,DX,DY,a,b,c,d,x,y,r,TH,X2_T,Y2_T	: float;	
 X2,Y2,P1,P2,CP,SP,TX,TY,diff1,diff2	: float;
Begin
-- Passage en coordonnees reelles
  n_points:=n_pt_ini;
  for i in 1..n_points loop
    ligne_1(i).coor_X:=float(ligne_ini(i).coor_X);
    ligne_1(i).coor_Y:=float(ligne_ini(i).coor_Y);
  end loop;
-- Lissage de ligne par convolution avec un leger filtre Gaussien
  Lissage_filtrage.Filtre_Gaussien_r(ligne_1(1..n_points),n_points,
                            float(n_points)/200.0,ligne(1..n_points));
-- Calcul de la pente
  pente:=Geomxav.Calpente(ligne,n_points);
-- Calcul de l'evolution de la pente de Ligne
 -- Calcul des pentes (P_seg) de chaque segment de Ligne :
  for i in 1..n_points-1 loop
    P_seg(i):=geomxav.Angpente(Ligne(i).coor_X,Ligne(i).coor_Y,
			Ligne(i+1).coor_X,Ligne(i+1).coor_Y);
  end loop;
 -- Prog = progression de l'angle en chaque point (a partir du 2eme)
	--  1 si progression constante
	-- -1 si changement de signe de la progression (=> pt d'inflexion)
	--  0 si ligne droite
  for i in 1..n_points loop prog(i):=-3; end loop;
  diff1:=Geomxav.Difference_angle(P_seg(2),P_seg(1)); i:=1; k:=1;
  while (diff1=0.0) and (i<n_points-1) loop prog(i):=0; i:=i+1;--put("ALIGNE !!");
    diff1:=Geomxav.Difference_angle(P_seg(i+1),P_seg(i)); k:=i;
  end loop;
  prog(k):=1;
  for i in k+1..n_points-1 loop
    diff2:=Geomxav.Difference_angle(P_seg(i+1),P_seg(i));
    if diff2/=0.0 then
      if diff1*diff2<0.0 then prog(i):=-1; else prog(i):=1; end if;
      diff1:=diff2;
    else prog(i):=0;--put("ALIGNE !!");
    end if;
  end loop;
  i:=0;
  while i<n_points-1 loop i:=i+1;
    if prog(i)=0 then k:=i;
      while (i<n_points-1) and (prog(i)=0) loop i:=i+1; end loop;
      prog(k):=-1; prog(i):=-1;
    end if;
  end loop;
-- Modelisation de ligne cubiques + determination des points d'inflexion
 -- Initialisations
  X1:=0.0; Y1:=0.0; P1:=0.0; kk:=0;
 -- Boucle sur ligne : recherche des points d'inflexion
  For i in 2..n_points loop
   -- Calcul des pentes :
    P1:=pente(i-1); -- pente au 1er point dans le repere absolu (dans ]-PI,PI])
    P2:=pente(i); -- pente au 2eme point dans le repere absolu (dans ]-PI,PI])
   --- Un point d'inflexion (si prog=-1) => modelisation par CUBIQUE
    if prog(i-1)=-1 then  -- 1 point d'inflexion => cubique interpolation
     -- Calcul de la rotation du repere
      TH:=(P1+P2)/2.0; -- angle du repere aux / repere absolu
      if (P1*P2<0.0) and (abs(P1)+abs(P2)>PI) then -- Correction de TH
	if TH>0.0 then TH:=TH-PI; else TH:=TH+PI; end if;
      end if;
      TH:=-TH; CP:=cos(TH); SP:=sin(TH);
     -- Changement de repere (translation et rotation de TH)
      TX:=ligne(i-1).Coor_X; -- origine du nouveau repere = 1er point
      TY:=ligne(i-1).Coor_Y;
      X2_T:=ligne(i).Coor_X-TX; -- translation du 2eme point
      Y2_T:=ligne(i).Coor_Y-TY;
      P1:=tan(P1+TH); -- tangente au 1er point dans le nouveau repere
      P2:=tan(P2+TH); -- tangente au 2eme point dans le nouveau repere
      X2:=CP*X2_T-SP*Y2_T; -- abscisse du 2eme point dans le nouveau repere
      Y2:=CP*Y2_T+SP*X2_T; -- ordonnee du 2eme point dans le nouveau repere
      DX:=X2-X1; DY:=Y2-Y1;
     -- Calcul des parametres de la cubique :
      if X2=0.0 then null;--put(" Probleme d'orthogonalite !!!  ");
      elsif (Y2=0.0) and (P2=0.0) then a:=0.0; b:=0.0; c:=0.0; d:=0.0;
      else a:=-2.0*Y2/DX/DX/DX+(P1+P2)/DX/DX;
	b:=(P2-P1)/2.0/X2-1.5*a*X2; c:=P1; d:=0.0;
      end if;
     -- Recherche des points d'inflexion et de la pente en ces points :
      if b*(3.0*a*X2+b)<=0.0 then -- normalement c'est toujours le cas !
	x:=-b/3.0/a; y:=a*x*x*x+b*x*x+c*x+d; kk:=kk+1; 
	PTINFL(kk).coor_x:=x*CP+y*SP+TX; PTINFL(kk).coor_y:=-x*SP+y*CP+TY;
	POS(kk):=i-1; r:=3.0*a*x*x+2.0*b*x+c; APTI(kk):=Arctan(r)-Th;
      else  null;--put(" Probleme !!! ");
      end if;
    end if;
  end loop;
  n_infl:=kk;
End Pt_infl;


end PI_et_sommet;
