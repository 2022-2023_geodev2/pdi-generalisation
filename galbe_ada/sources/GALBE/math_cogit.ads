with Gen_io; use Gen_io;
With text_io; use text_io;

package MATH_COGIT is 
 function MIN (tab: in liens_array_type) return integer;
 function MIN (tab: in reel_tableau_type) return float;
 function MIN( tableau : point_liste_type ) return point_type;
 function MAX (tab: in liens_array_type) return integer;
 function MAX (tab: in reel_tableau_type) return float;
 function MAX( tableau : point_liste_type) return point_type;
 Function MOYENNE (tab: in reel_tableau_type) return float;
 Function MOYENNE (tab: in reel_tableau_type;
 n: in integer) return float;
 Function MOYENNE (tab: in liens_array_type ) return float;
 Function MEDIANE (tab: in reel_tableau_type) return float;
 Function MEDIANE (tab: in reel_tableau_type;
 n: in integer) return float;
 Function MEDIANE (tab: in liens_array_type ) return integer;
 Function ECART_TYPE (tab: in reel_tableau_type ) return float;
 Function ECART_TYPE (tab: in reel_tableau_type;
 n: in integer ) return float;
 Function ECART_TYPE (tab: in liens_array_type  ) return float;
 Function CENTRAGE( Tab : in point_liste_type;
 		    c   : in point_type := (0,0) ) return point_liste_type;
 procedure DROITE_REGRESSION (		tab	: in point_liste_type;
					a,b	: out float) ;
 procedure DROITE_REGRESSION_GENERALE(	tab	: in point_liste_type;
					p	: IN point_type ;
					a,b	: out float) ;
 Type methode_type is (COMPAS, VANHORN);
 
 package met_io is new text_io.enumeration_io(methode_type); use met_io;
 
 procedure Walking_Divider( 	arc_courant 	: in point_access_type;
 				ncourant 	: in integer;
				arc_resultat 	: in out point_access_type;
 				nresultat  	: out integer;
				epsilon 	: in float;
				reste 		: out float);
				
 Function Dim_fractale( ligne_c 	  : Point_liste_type;
			ncourant          : natural;
			pas               : integer;
			methode_decoupage : methode_type  ) return float;
			
 Function Dim_fractale( arc_courant	  : Point_Access_type;
			ncourant          : natural;
			pas               : integer;
			methode_decoupage : methode_type  ) return float;
			
 Procedure Class_vect( vecteur    	: in reel_tableau_type;
                       narcs      	: in integer;
						seuil		: in float;
                       cluster         	: in out liens_array_type;
                       nb_arcs_cluster 	: in out liens_array_type);
					   
 Type matrice_type is array (integer range <>, integer range <>) of float;
 Type matrice_access_type is access matrice_type;
 
 Procedure KMEANS ( matrice_mesures     : in matrice_type;
                    narcs               : in integer;
                    nmesures            : in integer;
                    k                   : in integer;
					matrice_centres     : in out matrice_type;
                    cluster             : in out liens_array_type;
                    nb_arcs_cluster     : in out liens_array_type;
                    ifault              :    out natural          );
end ;
