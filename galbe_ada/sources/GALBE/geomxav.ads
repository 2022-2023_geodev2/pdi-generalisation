with Gen_io;
		Use Gen_io;
With Manipboite;
        Use Manipboite;
Package Geomxav is
   Type Point_4 is record
         x1  : float;
         x2  : float;
         x3  : float;
         x4  : float;
      End record;
      Type Point_liste_4 is array(natural range<>) of Point_4;
   Type Int_tableau_type is array(natural range <>) of integer;
-----------------------------------
-- Distance (A B) -> coord entieres
-----------------------------------
Function Distance(A,B	: in gen_io.point_type) return float;
----------------------------------
-- Distance (A B) -> coord reelles
----------------------------------
Function Distance(A,B	: in gen_io.point_type_reel) return float;
----------------------------------------------
-- Distance (A B) -> coord reelles et entieres
----------------------------------------------
Function Distance(A	: in gen_io.point_type;
		  B	: in gen_io.point_type_reel) return float;
-----------------------------------------------------------
-- Produit scalaire vect(A B) * vect(C D) -> coord entieres
-----------------------------------------------------------
Function Prodscal(A,B,C,D       : in gen_io.point_type) return float;
---------------------------------------------------------
--- Produit scalaire vect(A B) * vect(C D) -> coord reelles
----------------------------------------------------------
Function Prodscal(A,B,C,D       : in gen_io.point_type_reel) return float;
---------------------------------------------------------------------------
-- teste si l'angle a1 est strictement superieur a a2 (1 en sortie sinon 0)
-- les angles sont donnes en entree dans [-PI,PI] modulo 2PI et
-- leur difference n'excede jamais PI-- Ex : -PI+e=PI+e donc -PI+e>PI-e, si e<PI/2.
---------------------------------------------------------------------------
Function Superieur_angle(a1,a2: in float) return integer;
------------------------------------------------------------------------------
-- Calcule la difference d'angle entre a1 et a2 (= a1-a2) donnee dans ]-PI,PI]
-- les angles sont donnes en entree dans [-PI,PI] modulo 2PI et
-- leur difference absolue n'excede jamais PI-- Ex : -PI+e=PI+e donc (-PI+e)-(PI-e)=2*e si e<PI/2.
------------------------------------------------------------------------------
Function Difference_angle(a1,a2: in float) return float;
--------------------------------------------------------------------------
-- Calcule la valeur du secteur angulaire de [a1,a2] (~ a2-a1) donnee dans-- ]0,2*PI], les angles sont donnes en entree dans [-PI,PI] modulo PI
-- Ex :[a2,a1]: [-PI+e,PI-e]=2*PI-2*e mais [PI-e,-PI+e]=2*e
--------------------------------------------------------------------------
Function Valeur_angle(a1,a2: in float) return float;
-----------------------------------------------------------------------------
-- Determine si un angle appartient ou non a un secteur delimite par 2 angles
-- Les valeurs de ces 3 angles doivent etre dans un intervalle [k,k+2PI[
-----------------------------------------------------------------------------
Function Appartient(x,a1,a2: in float) return integer;
------------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord reelles
------------------------------------------------------------------------------
Function CoordCenCer(A,B,C : in gen_io.point_type_reel)                                 return gen_io.point_type_reel;
-------------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord entieres
-------------------------------------------------------------------------------
Function CoordCenCer(A,B,C : in gen_io.point_type)                                 return gen_io.point_type_reel;
-----------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord mixtes
-----------------------------------------------------------------------------
Function CoordCenCer(A,B : in gen_io.point_type;
                     C   : in gen_io.point_type_reel)                                 return gen_io.point_type_reel;
-----------------------------------------------------------------------------
-- Calcul des coord. du centre du cercle passant par 3 points -> coord mixtes
-----------------------------------------------------------------------------
Function CoordCenCer(A   : in gen_io.point_type;
		     B,C : in gen_io.point_type_reel)				return gen_io.point_type_reel;
------------------------------------------
-- Vecteur (M(i)-M(i-1)) : ligne en reels
------------------------------------------
Function Vecteur(	ligne_w	 : in point_liste_reel;
			AL	 : in reel_tableau_type;
			i	 : in integer;
			n_points : in natural) return point_type_reel;
-------------------------------------------
-- Vecteur (M(i)-M(i-1)) : ligne en entiers
-------------------------------------------
Function Vecteur(	ligne_w	 : in point_liste_type;
			AL	 : in reel_tableau_type;
			i	 : in integer;
			n_points : in natural) return point_type_reel;
------------------------
-- Vecteur (M(i)+M(i-1))
------------------------
Function Somme(	ligne_w	 : in point_liste_reel;
		AL	 : in reel_tableau_type;
		i	 : in integer;
		n_points : in natural) return point_type_reel;
------------------------------------------------------------
-- Remplissage du tableau des points alignes : coord reelles
------------------------------------------------------------
Function Aligne(ligne_w	 : in point_liste_reel;
		n_points : in natural) return reel_tableau_type;
-------------------------------------------------------------
-- Remplissage du tableau des points alignes : coord entieres
-------------------------------------------------------------
Function Aligne(ligne_w  : in point_liste_type;
		n_points : in natural) return reel_tableau_type;
-------------------------------------------------
-- Calcul des coordonnees des centres de courbure
-------------------------------------------------
Function CoorCenCourb(ligne_w    : in point_liste_reel;
		      n_points : in natural) return point_liste_reel;
----------------------------
-- Orientation de la normale
----------------------------
Procedure Orient_norm(ligne_w    : in point_liste_reel;
	 	      n_points : in natural;
		      theta    : in out reel_tableau_type;
		      ray      : out point_liste_reel);
---------------------------------------------------------
-- Calcul aire algebrique comprise entre les deux courbes
---------------------------------------------------------
Function Aire_alg( ligne	: in point_liste_type;
		   n		: in natural;
		   ligne_lowe	: in point_liste_type;
		   n_lowe	: in natural) return float;
-------------------------------------
-- Calcul du maximum entre deux reels
-------------------------------------
Function Max (a,b : in float) return float;
-------------------------------------
-- Calcul du minimum entre deux reels
-------------------------------------
Function Min (a,b : in float) return float;
-----------------------------------------------------
-- Calcul du lieu d'un point par rapport a une droite
-----------------------------------------------------
Function Lieu (A,B,C : in gen_io.point_type_reel) return float;
-----------------------------------------------------
-- Calcul du lieu d'un point par rapport a une droite
-----------------------------------------------------
Function Lieu (A,B,C : in gen_io.point_type) return float;
-----------------------------------------------------
-- Calcul du lieu d'un point par rapport a une droite
-----------------------------------------------------
Function Lieu ( A   : in gen_io.point_type_reel;
                B,C : in gen_io.point_type) return float;
----------------------------------------------------------------------
-- Renvoie 1 ssi les points A et B sont du meme cote de la droite (CD)
--         0 ssi un des points est situe sur la droite
--        -1 ssi les points ne sont pas du meme cote de la droite
----------------------------------------------------------------------
Function Meme_cote (A,B,C,D   : in gen_io.point_type) return integer;
----------------------------------------------------------------------
-- Renvoie 1 ssi les points A et B sont du meme cote de la droite (CD)
--         0 ssi un des points est situe sur la droite
--        -1 ssi les points ne sont pas du meme cote de la droite
----------------------------------------------------------------------
Function Meme_cote (A,B   : in gen_io.point_type_reel;
                    C,D   : in gen_io.point_type) return integer;
----------------------------------------------------------
-- Calcul de la pente d'une droite passant par deux points
----------------------------------------------------------
Function Pente (A,B : in gen_io.point_type) return float;
----------------- Idem en reel---------------
Function Pente (A,B : in gen_io.point_type_reel) return float;
---------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees entieres)-- dans [0,PI] (=> ordre indifferent)
---------------------------------------------------------------------
Function Angle_3pts	(A,B,C : in gen_io.point_type) return float;
--------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees reelles)-- dans [0,PI] (=> ordre indifferent)
--------------------------------------------------------------------
Function Angle_3pts	(A,B,C : in gen_io.point_type_reel) return float;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [0,PI] (=> ordre indifferent)
-------------------------------------------------------------------
Function Angle_3pts    (A   : in gen_io.point_type_reel;
			B,C : in gen_io.point_type) return float;
---------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees entieres)
-- dans [-PI,PI]
---------------------------------------------------------------------
Function Angle_3pts_alg (A,B,C : in gen_io.point_type) return float;
--------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees reelles)
-- dans [-PI,PI]--------------------------------------------------------------------
Function Angle_3pts_alg (A,B,C : in gen_io.point_type_reel) return float;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A : in gen_io.point_type_reel;
			B : in gen_io.point_type;
			C : in gen_io.point_type_reel) return float;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A   : in gen_io.point_type_reel;
			B,C : in gen_io.point_type) return float;
-------------------------------------------------------------------
-- Angle forme par 3 points A,B,C (sommet = B) (coordonnees mixtes)
-- dans [-PI,PI]
-------------------------------------------------------------------
Function Angle_3pts_alg(A,B : in gen_io.point_type;
			C   : in gen_io.point_type_reel) return float;
-----------------------------------------------
-- Calcul de l'intersection entre deux segments
-----------------------------------------------
Function Inters( A,B,C,D : in gen_io.point_type) return point_liste_type;
-----------------------------------------------------------------
-- Calcul de l'intersection entre deux segments [AB] et [CD]
-- Retourne le point intersection = PINT-- Retourne ali=4 si les 4 points sont alignes
-- Retourne ali=3 si 3 points sont aligne
-- Retourne ali=2 si pas de points aligne
-- Retourne typ=0 si intersection = vide
-- Retourne typ=-1 si intersection vide mais droite coupe un segment
-- Retourne typ=-2 si intersection vide et droites paralleles (trapeze)
-- Retourne typ=-3 si intersection vide et droites paralleles (parallelogramme)
-- Retourne typ=1 si intersection = 1 point different des 4 points
-- Retourne typ=2 si intersection = 1 point des 4 points (tous differents)
-- Retourne typ=3 si intersection = 1 point (2 points sur les 4 sont confondus)
-- Retourne typ=4 si intersection = segment sans inclusion
-- Retourne typ=5 si intersection = segment avec inclusion (points differents)
-- Retourne typ=6 si intersection = segment avec inclusion (2 points egaux)
-- Retourne typ=7 si intersection = segment avec confusion
-----------------------------------------------------------------
Procedure Inters_2seg (A,B,C,D : in gen_io.point_type;
		       ali,typ : out integer;
		       PINT    : out gen_io.point_type);
----------------- Idem en reel---------------
Procedure Inters_2seg (A,B,C,D : in gen_io.point_type_reel;
		       ali,typ : out integer;
		       PINT    : out gen_io.point_type_reel);
-------------------------------------
-- Calcul de l'aire d'un quadrilatere
-------------------------------------
Function Quadrigone(A,B,C,D	: in gen_io.point_type) return float;
------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes
------------------------------------------------------
Function Aire_abs2( ligne	: point_liste_type;
		    ligne_lowe	: point_liste_type;
		    n_points	: natural) return float;
------------------------------------------------------------------
-- Confusion des extremites des deux lignes si ce n'est pas le cas
------------------------------------------------------------------
Procedure Homogena( ligne	 : in point_liste_type;
		    ligne_lowe	 : in point_liste_type;
		    n_points	 : in natural;
		    ligne_c	 : in out point_liste_type;
		    ligne_lowe_c : in out point_liste_type;
		    kk,ll	 : out natural);
--------------------------------------------------------------------------
-- Confusion des extremites des deux lignes si ce n'est pas le cas
-- et suppression des points doubles (nbre de points des lignes different)
--------------------------------------------------------------------------
Procedure Homogena1 (ligne	   : in point_liste_type;
		     n_points	   : in natural;
		     ligne_lowe	   : in point_liste_type;
		     n_points_lowe : in natural;
		     ligne_c	   : in out point_liste_type;
		     ligne_lowe_c  : in out point_liste_type;
		     kk,ll	   : out natural);
--------------------------------------------------------------------------
-- Tri des elements d'un tableau en fonction de la distance a un point (A)
--------------------------------------------------------------------------
Procedure Tri	(Tab	: in out point_liste_type;
		 IInt	: in out point_liste_type;
		 IO,IL	: in out int_tableau_type;
		 lg,kk	: in natural;
		 A,B	: in gen_io.point_type);
------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes
------------------------------------------------------
Function Aire_abs1( lign	: point_liste_type;
		    lign_lowe	: point_liste_type;
		    n_points	: natural) return float;
------------------------------------------------------
-- Calcul aire absolue comprise entre les deux courbes
-- nombre de points different pour les deux courbes
------------------------------------------------------
Function Aire_abs3( lign	: point_liste_type;
		    n_p		: natural;
		    lign_lowe	: point_liste_type;
		    n_p_lowe	: natural) return float;
-----------------------------------
-- Choix de la ligne : 1ere version
-----------------------------------
Procedure Choix_ligne  (Ligne    : out point_liste_type;
			n_points : out natural);
-----------------------------------
-- Choix de la ligne : 2eme version
-----------------------------------
Procedure Choix_ligne1  (Ligne    : in out point_liste_type;
			 n_points : out natural;
			 rp,rp1   : out character);
---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. entieres) (de M1 a M2)
---------------------------------------------------------------------------
Function Angpente (X1,Y1,X2,Y2  : in integer) return float;
---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. mixtes) (de M1 a M2)
---------------------------------------------------------------------------
Function Angpente (X1,Y1  : in integer;
                   X2,Y2  : in float) return float;
---------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. mixtes) (de M1 a M2)
---------------------------------------------------------------------------
Function Angpente (X1,Y1  : in float;
                   X2,Y2  : in integer) return float;
--------------------------------------------------------------------------
-- Calcul de l'angle de pente entre 2 points (coord. reelles) (de M1 a M2)
--------------------------------------------------------------------------
Function Angpente (X1,Y1,X2,Y2  : in float) return float;
------------------------------------------------------------------------------
-- Verification et correction par insertion ou suppression de points (entiers)
------------------------------------------------------------------------------
Procedure Verif(ligne		: in out point_liste_type;
		rapall		: in float;
		ectm		: in float;
		n_points	: in out natural);
-------------------------------------------
-- Detection des points anguleux et alignes
-------------------------------------------
Procedure Verif1(ligne		 : in point_liste_type;
		 angle		 : in float;
		 n_points	 : in natural;
		 n_ang,n_ali	 : out integer;
		 tab_ang,tab_ali : out int_tableau_type);
----------------------------------------------------------------------------
-- Verification et correction par insertion ou suppression de points (reels)
----------------------------------------------------------------------------
Procedure Verif(ligne_flo	: in out point_liste_reel;
		rapall		: in float;
		ectm		: in float;
		n_points	: in out natural);
-----------------------------------------------------
-- Calcul des pentes en chaque point (coord entieres)
-----------------------------------------------------
Function Calpente (ligne	: in point_liste_type;
		   n_points	: in natural) return reel_tableau_type;
----------------------------------------------------
-- Calcul des pentes en chaque point (coord reelles)
----------------------------------------------------
Function Calpente (ligne	: in point_liste_reel;
		   n_points	: in natural) return reel_tableau_type;
-------------------------------------------------------------------------------- Calcul des pentes des pts (coord entieres) d'une ligne (dans ]-3.14,+3.14])   --> pente au point Mi = ponderation en 1/k des droites (Mi-k Mi+k)------------------------------------------------------------------------------
Function Calpente_multi (ligne	    : in point_liste_type;
			 n_points,n : in natural) return reel_tableau_type;
-----------------------------------------------------------------------------
-- Calcul des pentes des pts (coord reelles) d'une ligne (dans ]-3.14,+3.14])   --> pente au point Mi = ponderation en 1/k des droites (Mi-k Mi+k)-----------------------------------------------------------------------------
Function Calpente_multi (ligne	    : in point_liste_reel;
			 n_points,n : in natural) return reel_tableau_type;
------------------------------------------------------------------------------ Decomposition d'une ligne en segments irregulers (ligne reelle en entree)----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant    : in Point_liste_reel;
			  navant     : in natural;
			  l_apres    : out Point_liste_reel;
			  napres     : out natural;
			  pas        : in float;
			  nb_segments: in out Liens_array_type);
------------------------------------------------------------------------------ Decomposition d'une ligne en segments irregulers (ligne reelle en entree)----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant : in Point_liste_reel;
			navant : in natural;
			l_apres : out Point_liste_type;
			napres : out natural;
			pas : in float;
			nb_segments : in out Liens_array_type);
------------------------------------------------------------------------------ Decomposition d'une ligne en segments irregulers (ligne reelle en entree)----------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant    : in Point_liste_reel;
			  navant     : in natural;
			  l_apres    : in out Point_Access_reel;
			  napres     : out natural;
			  pas        : in float;
			  nb_segments: in out Liens_array_type);
---------------------------------------------
-- Teste si un entier est pair (1) ou non (0)---------------------------------------------
Function Paire (i : integer) return integer;
-------------------------------------------------------------------
-- Calcul de l'ordonnee d'une cubique (a,b,c,d) pour une abscisse x
-------------------------------------------------------------------
Function Y_cubique ( a,b,c,d,x :float) return float;
---------------------------------------------------------------
-- Calcul de la difference de deux angles (a2-a1) a modulo 2*pi
---------------------------------------------------------------
Function Deltatrigo (a1,a2 : float) return float;
--------------------------------------------------------------------------
-- Calcul du rang de la valeur minimale d'un extrait d'un tableau de reels
-- On considere une suite extraite (par P entre 2 indices ri et rf) de T
--------------------------------------------------------------------------
Function Mintab ( T	: reel_tableau_type;
		  P	: Int_tableau_type;
		  ri,rf	: integer) return integer;
--------------------------------------------------------------------------
-- Calcul du rang de la valeur maximale d'un extrait d'un tableau de reels
-- On considere une suite extraite (par P entre 2 indices ri et rf) de T
--------------------------------------------------------------------------
Function Maxtab ( T	: reel_tableau_type;
		  P	: Int_tableau_type;
		  ri,rf : integer) return integer;
--------------------------------------------------------------------
-- Detection des maxima d'une courbe entre deux valeurs nini et nfin
-- pos=1 <=> on commence a partir de nini-1
--------------------------------------------------------------------
Procedure RechMax (fct		: in reel_tableau_type;
		   nini,nfin	: in natural;
		   pos		: in integer;
		   P_MAX	: out Int_tableau_type;
		   n_max	: out integer);
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord entieres)
--------------------------------------------------------------------------------
Function Rayon_3pts (A,B,C : in gen_io.point_type) return float;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord. reelles)
--------------------------------------------------------------------------------
Function Rayon_3pts (A,B,C : in gen_io.point_type_reel) return float;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord entieres)
--------------------------------------------------------------------------------
Function Ray_3pts (A,B,C : in gen_io.point_type) return float;
--------------------------------------------------------------------------------
-- Calcul du rayon de courbure d'un cercle passant par 3 points (coord. reelles)
--------------------------------------------------------------------------------
Function Ray_3pts (A,B,C : in gen_io.point_type_reel) return float;
----------------------------------------------------------------------
-- Calcul du determinant entre les vecteurs MN et MP (coord. entieres)
----------------------------------------------------------------------
Function Det (M,N,P : Gen_io.point_type) return float;
---------------------------------------------------------------------
-- Calcul du determinant entre les vecteurs MN et MP (coord. reelles)
---------------------------------------------------------------------
Function Det (M,N,P : Gen_io.point_type_reel) return float;
-----------------------------------------------------------
-- Teste l'appartenance de X au segment MN (coord. reelles)
-----------------------------------------------------------
Function Appseg (M,N,X : Gen_io.point_type_reel) return boolean;
------------------------------------------------------------
-- Teste l'appartenance de X au segment MN (coord. entieres)
------------------------------------------------------------
Function Appseg (M,N,X : Gen_io.point_type) return boolean;
------------------------------------------------------------
-- Teste si les segments MN et PX se coupent (coord.reelles)
------------------------------------------------------------
Function Coupseg (M,N,P,X : Gen_io.point_type_reel) return boolean;
--------------------------------------------------------------
-- Teste si les segments MN et PX se coupent (coord. entieres)
--------------------------------------------------------------
Function Coupseg (M,N,P,X : Gen_io.point_type) return boolean;
-----------------------------------------------
-- Calcul de l'intersection entre deux segments
-----------------------------------------------
Function Inters( A,B,C,D : in gen_io.point_type) return gen_io.point_type;
------------------------------------------------------
-- Translation du polygone TAB pour que TAB(im):=(0,0)
-- Permutation de TAB pour que TAB(1):=TAB(im)
------------------------------------------------------
Function Transl(TAB	: point_liste_reel;
		nbpt,im	: integer) return point_liste_reel;
-----------------------------------------------------------------
-- Translation d'un polygone en TAB(i0)=0
-- Rotation autour de TAB(i0) pour que TAB(i1).x<0 et TAB(i1).y=0
-----------------------------------------------------------------
Function Rot2 (	TAB1		: point_liste_reel;
		nbpt,i0,i1	: integer) return point_liste_reel;
-----------------------------------
-- Polygone mis dans le sens direct
-----------------------------------
Procedure SensDirect (	TAB0	: in point_liste_reel;
			nbpt	: in integer;
			sd	: out integer);
-----------------------------------------------------------------------------
-- Supprime les intersections d'un polygone (polyligne fermee par la base)
-- en creant un nouveau polygone en partant du point initial jusqu'au point
-- final en restant toujours au dessus de la base ou confondu avec cette
-- derniere et en revenant au point initial en restant au dessous ou confondu
-----------------------------------------------------------------------------
Procedure Supcoup(TABI          : in point_liste_type;
                  nbpt_ini      : in integer;
                  TABF          : out point_liste_type;
                  nbpt_fin      : out integer);
------------------------------------------------------------------
-- Calcul de l'enveloppe convexe d'un polygone qui ne se coupe pas
------------------------------------------------------------------
Procedure Enveloppe ( TABL	: in point_liste_type;
		      np	: in integer;
		      TABC	: out point_liste_reel;
		      nc	: out integer);
--------------------------------------------------------------------------
-- Calcul de l'enveloppe convexe d'un polygone qui eventuellement se coupe
-- REM = les tableaux de points sont fixes a 500 ...
--------------------------------------------------------------------------
Procedure Envconvligne (TABL    : in point_liste_type;
                        np      : in integer;
                        TABC    : out point_liste_reel;
                        nc      : out integer);
-----------------------------------------------
-- Recherche du point ayant l'abscisse maximale
-----------------------------------------------
Procedure Rechminxa (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Mxas  : out Gen_io.point_type_reel;
                        rMxas : out integer);
-----------------------------------------------
-- Recherche du point ayant l'abscisse minimale
-----------------------------------------------
Procedure Rechminxi (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Mxis  : out Gen_io.point_type_reel;
                        rMxis : out integer);
-----------------------------------------------
-- Recherche du point ayant l'ordonnee minimale
-----------------------------------------------
Procedure Rechminyi (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Myis  : out Gen_io.point_type_reel;
                        rMyis : out integer);
-----------------------------------------------
-- Recherche du point ayant l'ordonnee maximale
-----------------------------------------------
Procedure Rechminya (   TAB   : in point_liste_reel;
                        nbpt  : in integer;
                        Myas  : out Gen_io.point_type_reel;
                        rMyas : out integer);
-----------------------------------------------------------------
-- Rotation du polygone TAB autour de TAB(1) pour que TAB(im).x=0
-- Translation du polygone TAB pour que TAB(im):=(0,0)
-- Permutation de TAB pour que TAB(1):=TAB(im)
-----------------------------------------------------------------
Function Rot(   TAB0    : point_liste_reel;
                nbpt,im : integer) return point_liste_reel;
-----------------------------------------------------------------
-- Rotation du polygone TAB autour de TAB(1) pour que TAB(im).x=0
-----------------------------------------------------------------
Function Rot1(  TAB0    : point_liste_reel;
                nbpt,im : integer) return point_liste_reel;
 -------------------------------------
-- Calcul des parametres du rectangle
-------------------------------------
Procedure Parect(TABC           : in point_liste_reel;
                 nconv,rmin     : in integer;
                 RECT           : in out point_liste_reel;
                 PTS            : out Int_tableau_type;
                 angle          : out float);
----------------------------------------
-- Calcul du rectangle englobant minimal
----------------------------------------
Procedure Rectanglob (TABC              : in point_liste_reel;
                      nconv             : in integer;
                      RECTANGLE         : out bloc_type;
                      airemin           : out float;
                      orientprinc       : out float);
End Geomxav;
