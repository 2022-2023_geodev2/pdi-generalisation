with sequential_io;with geometrie; use geometrie;with float_math_lib; use float_math_lib;with pts_caracteristique;Package BODY mesure_ligne is  package int_io is new integer_io(integer);    use int_io;  package flo_io is new float_io(float);        use flo_io;
-- ======================================================================
-- Procedure de mesure de resistance au lissage
--   Methode suggeree dans Rosin 93 (multiscale...)
--   Mais S(Sigma) (= significance en anglais) est ici
--   approximee a S(Sigma) = sigma * nb de pts d'inflexion
-- Le resultat est un tableau de valeurs de significance
-- Sigma varie par pas de racine de 2 jusqu'a ce qu'il n'y ait plus de PI
-- ======================================================================
Function resistence (  	ligne 		: in point_liste_type;		       	npts		: in natural; 			sigma_ini	: in float   ) return float is sigma		 : float; Ligne_lisse 	 : point_liste_reel(1..1000); nb_pts_inflex,s : natural; Tpoints_pred_pi : liens_access_type; Tpinflex,  Tpinflex_lisse  : point_liste_type(1..300); lgr_ligne	 : float; indicateur	 : float; echelle	 : float := 100.0;begin sigma := sigma_ini; Pts_caracteristique.INFLEXIONS( ligne, npts, sigma, ligne_lisse,	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse); lgr_ligne  := geometrie.taille_ligne (ligne, npts); indicateur := float(nb_pts_inflex) * echelle / lgr_ligne; return indicateur;end ; 
-------------------------------------------------------------------
Procedure resistence (  ligne 		: in point_liste_type;		        npts		: in natural;			sigma_max	: in out float;			ncourb		: In Out natural;			Tcourb		: In Out reel_tableau_type ) is sigma		 : float:=1.0; Ligne_lisse 	 : point_liste_reel(1..1000); nb_pts_inflex,s : natural; Tpoints_pred_pi : liens_access_type; Tpinflex,  Tpinflex_lisse  : point_liste_type(1..300); root2		 : float := SQRT(2.0); lgr_ligne	 : float; indicateur	 : float;begin ncourb:= 0; sigma := 1.0; Pts_caracteristique.INFLEXIONS( ligne, npts, sigma, ligne_lisse,	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse); lgr_ligne  := geometrie.taille_ligne (ligne, npts); indicateur := float(nb_pts_inflex) / lgr_ligne; indicateur := indicateur * 100.0;flo_io.put(indicateur,1,2,0);new_line(1); s:=1; for s in 1..natural'last loop    sigma:= float(s) * indicateur;   Pts_caracteristique.INFLEXIONS( ligne, npts, Sigma, ligne_lisse,	 nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);   if nb_pts_inflex < 7 then 
-- 2 pour eviter le PI de debut et fin !!!	
sigma_max:= sigma;     	return;   else   	ncourb:= ncourb+1;   	Tcourb(ncourb):= float(nb_pts_inflex); -- * sigma;
   end if; end loop;end resistence;  
-- ========================================================================
-- 
-- Test d'homogeneite 
--  
-- Ou encore mesure de REGULARITY suite aux reunions avec Mcmaster  
-- ========================================================================
Procedure TEST_HOMOGENEITE (  nb_virages	: IN  integer;				Tpinflex  	: IN  point_liste_type;				HOMOGENEITE	: OUT boolean ) is    	dist_entre_PI, ecart : reel_tableau_type(1..nb_virages):= (others=>0.0);    	xm, ec, med: float; 	npi,ntotal,nbis: natural:=0; 	cpt: liens_array_type(1..50*nb_virages):=(others=>1); 	cptbis: liens_array_type(1..50*nb_virages):=(others=>0); 	signeA, signe: boolean;	flag: boolean:= FALSE;	f:text_io.file_type;        produit: float:= 1.0;
-----------------------------------------------------------------------------  
BEGIN   flag := FALSE;   create(f,out_file,"homogene.tem");   
-- ---------------------------------------------- --   
-- Det. des ecarts a la moyenne des dist entre PI --   
-- ---------------------------------------------- --   
put(f,"Distances entre PI :"); new_line(f,1);   for i in 1..nb_virages loop     dist_entre_pi(i) := Norme ( tpinflex(i), tpinflex(i+1) );     int_io.put(f,integer(dist_entre_pi(i)));new_line(f,1);   end loop;   xm := MOYENNE(dist_entre_pi(1..nb_virages));   med:= MEDIANE(dist_entre_pi(1..nb_virages));   ec := ECART_TYPE(dist_entre_pi(1..nb_virages));   new_line(f,1);   put(f,"Moyenne = ");int_io.put(f,integer(xm));new_line(f,1);   put(f,"Mediane = ");int_io.put(f,integer(med));new_line(f,1);   put(f,"moyenne/mediane = ");flo_io.put(f,xm/med,1,3,0);new_line(f,1);   put(f,"Ecart_type = ");int_io.put(f,integer(ec));new_line(f,1);   put(f,"Ecart_type/moyenne = ");flo_io.put(f,ec/xm,1,3,0);new_line(f,1);   put(f,"Ecart_type/mediane = ");flo_io.put(f,ec/med,1,3,0);new_line(f,1);   new_line(f,1);   put(f,"Ecarts a la moyenne :"); new_line(f,1);   for i in 1..nb_virages loop      ecart(i):= dist_entre_pi(i) - xm;      int_io.put(f,integer(ecart(i)));new_line(f,1);   end loop;   produit := 1.0;   for i in 1..nb_virages loop      produit := produit * (ecart(i)/ec);   end loop;   put("Henri = ");flo_io.put(float(produit));new_line(1);   homogeneite:= flag;   close(f);  END Test_homogeneite;  
-- ==========================================================================  
-- Decoupages en segments DROITS / SINUEUX :  --   
-- On regarde la variation des distances entre PI  --   
-- Essai avec le ratio entre la hauteur et la distance entre PI  
-- Cet essai n'a marche pas !!!  --  
-- Autre essai en regardant la variation des surfaces  
-- ==========================================================================  
Procedure DECOUPAGE ( nb_virages	 : in     integer;			Tpinflex  	 : in     point_liste_type;		ligne 		   	 : in     point_liste_type;		Tpoints_pred_pi	  	 : in     liens_access_type;			nb_pts_critiques : in out integer;			Tpcritiques  	 : in out liens_array_type ) is    	dist_entre_PI, ecart : reel_tableau_type(1..nb_virages):= (others=>0.0);    	xm, ec: float; 	npi,ntotal,nbis: natural:=0; 	cpt: liens_array_type(1..50*nb_virages):=(others=>1); 	cptbis: liens_array_type(1..50*nb_virages):=(others=>0); 	signeA, signe: boolean;        IP1, IP2: point_type_reel;  	n_aux 	: natural := 1;  	Ligne_aux 	: point_access_reel;  	lgr_segments	: liens_array_type(1..200);	nb_pts_virage	: integer;	ligne_virage	: point_liste_type(1..200);   	h, hauteur	: float;   	surface		: float;  -----------------------------------------------------------------------------  
BEGIN   
-- ---------------------------------------------- --   
-- Det. des ecarts a la moyenne des dist entre PI --   
-- ---------------------------------------------- --   
for i in 1..nb_virages loop     dist_entre_pi(i) := Norme ( tpinflex(i), tpinflex(i+1) );     
-- -------------------------------------------------------------     
-- Essai de decoupage avec la surface      
-- approximee a la somme des distances de chaque point a la base
--     IP1:= (float(tpinflex(i).coor_x), float(tpinflex(i).coor_y)); 
--     IP2:= (float(tpinflex(i+1).coor_x), float(tpinflex(i+1).coor_y)); 
--     nb_pts_virage:=0;
--     for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1) loop
--    	nb_pts_virage:= nb_pts_virage+1;-- 	ligne_virage(nb_pts_virage):= ligne(j); 
--     end loop;
--     Decompose_ligne(ligne_virage,nb_pts_virage,ligne_aux,n_aux,1.0,lgr_segments);
--     surface:= 0.0;
--     for j in 1..n_aux loop
--        h:= distance_a_ligne(ligne_aux(j),IP1,IP2,C_ligne_type);  
--	surface:= surface + h;
--     end loop;
--    dist_entre_pi(i) :=  surface ;
--     flo_io.put(surface);
  new_line(1);   end loop;   xm:= MOYENNE(dist_entre_pi(1..nb_virages));   ec:= ECART_TYPE(dist_entre_pi(1..nb_virages));   for i in 1..nb_virages loop      ecart(i):= dist_entre_pi(i) - xm;   end loop;   
-- --------------------   
-- Test d'homogeneite :   
-- --------------------   
-- Regarder si les ecart a la moyenne sont violents!   
-- -------------------------------- --   
-- Det. des series de "+" et de "-" --	      
-- -------------------------------- --
   cpt(1):= 1;   npi:=1;   for i in 2..nb_virages loop     
-- -----------------------------------     
-- Les ecarts sont-ils de meme signe ?
     if (ecart(i) >= 0.0) = (ecart(i-1) >= 0.0) then	cpt(npi):= cpt(npi) + 1;     else	npi:= npi + 1;         end if;   end loop;   
-- -------------------------------------------------------- --   
-- Choix des Pts critiques entre les series en fct du seuil --   
-- -------------------------------------------------------- --   
nbis:= 1;   cptbis(1):= cpt(1);   signeA:= TRUE;   signe:= signeA;   for i in 2..npi loop     signe:= NOT(signe);     if cpt(i) >= Seuil_dist_entre_pi_homogenes  then 	if signe = signeA then	    cptbis(nbis):= cptbis(nbis)+cpt(i);	else	    nbis:= nbis+1;	    cptbis(nbis):= cpt(i);	    signeA:= NOT(signeA);	end if;     else 	cptbis(nbis):= cptbis(nbis)+cpt(i);     end if;   end loop;        ntotal:= 0;    nb_pts_critiques:= 0;   for i in 1..nbis-1 loop     ntotal:= ntotal + cptbis(i);     nb_pts_critiques:= nb_pts_critiques + 1;     tpcritiques(nb_pts_critiques):= ntotal+1;   end loop;   End DECOUPAGE; 
-- ========================================================================== 
-- Choix automatique de la valeur de SIGMA -- 
-- --------------------------------------- -- 
-- - Proportionnel a la longueur de la ligne : lgr/100 * LN(lgr/100)  
-- - log = log neperien en ADA 
-- - log10 pour log decimal 
-- ========================================================================== 
Procedure CHOIX_SIGMA( ligne		: IN point_liste_type;			nb_points	: IN integer;			Sigma		: IN OUT float ) is  lgr_ligne: float ; begin   lgr_ligne:= geometrie.taille_ligne (ligne, nb_points);   SIGMA:= lgr_ligne/100.0 * log(lgr_ligne/100.0) ;   put("Longueur de la ligne      = ");flo_io.put(lgr_ligne,1,2,0); new_line(1);   put("SIGMA propose en l.log(l) = ");flo_io.put(sigma,1,2,0); new_line(1); end;  
-- ============================================================================ 
-- Mesures simples basees sur les PI et les sommets 
-- ============================================================================ 
Procedure MESURES( ligne 		: in point_liste_type;		    npts		: in natural;  		    nb_pts_inflex	: in natural;  		    Tpoints_pred_pi	: in liens_access_type;  		    Tpinflex	 	: in point_liste_type;    		    largeur_symbole	: in float;		    f			: in file_type) is    
-- Mesures sur la ligne
     LONGUEUR_ARC : float := 0.0;     Tab_angles: reel_tableau_type(1..5000);    
-- Variables relatives aux mesures GLOBALES:    
SURFACE_GLOBALE, HB_GLOBALE, SURF_MIN, SURF_MAX, ECART_SURF_TYPE								: float:=0.0;    TAB_ANGLES_ENTRE_PI: reel_tableau_type(1..nb_pts_inflex-1);    TAB_ANGLES_abs_ENTRE_PI: reel_tableau_type(1..nb_pts_inflex-1);    Longueur_ligne_PI : float:= 0.0;    
-- Variables relatives aux mesures localement sur un virage:    
Type TRIANGLE_type is record	base     : float:= 0.0;	Sommet   : gen_io.point_type := (0,0);        position_sommet: integer;       	hauteur  : float:= 0.0; 
-- Hauteur corresp. (en proj. orthogonale)        
hb	 : float:= 0.0; 
-- hauteur/base
surface  : float:= 0.0; 
-- surface du triangle
    end record;    surface,	hb,	haut,	dpi,	abs_curv,	abs_curv_b,	pos_sommet: reel_tableau_type(1..200):= (others=>0.0);    h,hauteur: float:= 0.0;    sommet: gen_io.point_type;     Type VIRAGE_type is record	dist_entre_PI  : float:= 0.0;	abs_curv       : float:= 0.0;        nb_triangles   : integer := 0;        triangle       : triangle_type;        triangle1      : triangle_type;        triangle2      : triangle_type;        triangle3      : triangle_type;        triangle4      : triangle_type;    end record;     Package virage_io is new sequential_io(virage_type); use virage_io;    type TAB_VIRAGE_TYPE is array(integer range <>) of virage_type;    Virage,virage1,virage2,virage3,virage4:TAB_VIRAGE_TYPE(1..200);    REGULARITY : float := 0.0;     ANGULARITY  : float := 0.0;     BLACKNESS  : float := 0.0;     X,Y : liens_array_type(1..2000);    ABR : float; 
-- Aire du "boundary rectangle" (rect. englobant)
begin   
-- LONGUEUR de la ligne --   
Longueur_arc := taille_ligne(ligne, npts);   
-- ANGULARITE de la ligne :--
   for i in 1..npts-2 loop
         TAB_ANGLES(i):= ABS(Angle_3points(ligne(i),
   				   	ligne(i+1),
   				   	ligne(i+2) ));
      end loop;   
   -- ***************************************   
   -- == MESURES LOCALES sur chaque virage ==   
   -- ***************************************
      for i in 1..nb_pts_inflex-1 loop     Virage(i).dist_entre_pi := Norme ( tpinflex(i), tpinflex(i+1) );     
      -- ABSCISSES CURVILIGNES --     
virage(i).ABS_CURV:=Norme(tpinflex(i),ligne(tpoints_pred_pi(i)+1));     for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1)-1 loop        virage(i).abs_curv:= virage(i).abs_curv + norme(ligne(j),ligne(j+1));     end loop;     virage(i).abs_curv := virage(i).abs_curv 			 + norme(ligne(tpoints_pred_pi(i+1)-1),tpinflex(i+1));     virage(i).abs_curv := virage(i).abs_curv ;      --------------------------------------     
-- Mesures sur le triangle principal :     
--------------------------------------     
Virage(i).triangle.base := Virage(i).dist_entre_pi;     
-- Recherche du pseudo-sommet principal     
--	= pt le plus eloigne de la ligne des PI en projection orthogonale :     
h:= 0.0;     hauteur:= 0.0;     for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1) loop        h:= distance_a_ligne(ligne(j),tpinflex(i),tpinflex(i+1),C_ligne_type);        if h > hauteur then   	  hauteur:= h;          sommet := ligne(j);	end if;     end loop;     virage(i).triangle.hauteur := hauteur;     virage(i).triangle.sommet  := sommet;     virage(i).triangle.hb      := hauteur / virage(i).triangle.base;      if angle_3points(tpinflex(i+1), tpinflex(i), sommet) > PI/2.0 then       virage(i).triangle.position_sommet := 1; -- -1;
elsif angle_3points(sommet, tpinflex(i+1), tpinflex(i)) < PI/2.0 then       virage(i).triangle.position_sommet := 0;     else       virage(i).triangle.position_sommet := 1;     end if;     
-- SURFACES ----     
virage(i).triangle.surface := virage(i).triangle.base * hauteur / 2.0;      for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1) loop        h:= distance_a_ligne(ligne(j),tpinflex(i),tpinflex(i+1),C_ligne_type);     	virage(i).triangle.surface := virage(i).triangle.surface + h;      end loop;       
-- Pour tester HAUTEUR/BASE a la place de la surface !!!!!!     
surface(i) := virage(i).triangle.surface;     dpi(i)     := virage(i).dist_entre_pi;     abs_curv(i):= virage(i).abs_curv;     abs_curv_b(i):= virage(i).abs_curv / Virage(i).dist_entre_pi;     haut(i)	:= virage(i).triangle.hauteur;     hb(i)      := virage(i).triangle.hb;     pos_sommet(i):= float(virage(i).triangle.position_sommet)/float(nb_pts_inflex-1);   end loop; 
-- sur le nb de virages --   
-- ***********************************   
-- == MESURES GLOBALES sur la ligne ==   
-- ***********************************   
for i in 1..nb_pts_inflex-1 loop      LONGUEUR_LIGNE_PI := LONGUEUR_LIGNE_PI + virage(i).dist_entre_pi;   end loop;   for i in 1..nb_pts_inflex-1 loop      SURFACE_GLOBALE:= SURFACE_GLOBALE + virage(i).triangle.surface;   end loop;   for i in 1..nb_pts_inflex-1 loop      HB_GLOBALE:= HB_GLOBALE + virage(i).triangle.HB;   end loop;   HB_GLOBALE:= HB_GLOBALE / longueur_arc;   
-- ANGLES de la ligne des PI --   
for i in 1..nb_pts_inflex-1-1 loop      TAB_ANGLES_ENTRE_PI(i):= Angle_3points (  tpinflex(i),						tpinflex(i+1),						tpinflex(i+2) );      TAB_ANGLES_ABS_ENTRE_PI(i):= ABS(Angle_3points (	tpinflex(i),							tpinflex(i+1),							tpinflex(i+2) ));   end loop;   
-- ============================================================ --   
-- Mesures produites lors des reunions de travail avec MCMASTER --    
REGULARITY := ecart_type( dpi(1..nb_pts_inflex-1))  / longueur_ligne_pi;     ANGULARITY  := ecart_type( TAB_ANGLES_ABS_ENTRE_PI(1..nb_pts_inflex-1-1)) 							/ longueur_ligne_pi;      for l in 1..npts loop       X(l):=ligne(l).coor_x;       Y(l):=ligne(l).coor_y;    end loop;    ABR := float( max(X(1..npts)) - min(X(1..npts)) ) 	 * float( max(Y(1..npts)) - min(Y(1..npts)) );     BLACKNESS  := longueur_arc * largeur_symbole / ABR;    
-- ======================== ECRITURES FICHIER ==============================   
-- ======================== ECRITURES FICHIER ==============================   
-- ======================== ECRITURES FICHIER ==============================
--   flo_io.put(f,float(Nb_pts_inflex-1)/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,regularity); put(f," ");
--   flo_io.put(f,angularity ); put(f," ");
--   flo_io.put(f,ABR ); put(f," ");
--   flo_io.put(f,blackness ); put(f," ");
--   flo_io.put(f,min(pos_sommet(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(pos_sommet(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(pos_sommet(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(pos_sommet(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(pos_sommet(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,min(dpi(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(dpi(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(dpi(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(dpi(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(dpi(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f,"  ");
--   flo_io.put(f,min(Haut(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(haut(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(haut(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(haut(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(haut(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,min(ABS_CURV(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(abs_curv(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(abs_curv(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(abs_curv(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(abs_curv(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,min(Hb(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(hb(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(hb(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(hb(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(hb(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,min(ABS_CURV_b(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,max(abs_curv_b(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(abs_curv_b(1..nb_pts_inflex-1)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(abs_curv_b(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(abs_curv_b(1..nb_pts_inflex-1))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,min(tab_ANGLES_ENTRE_PI(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,max(tab_angles_entre_pi(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(tab_angles_entre_pi(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(tab_angles_entre_pi(1..nb_pts_inflex-2))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(tab_angles_entre_pi(1..nb_pts_inflex-2))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,min(tab_ANGLES_abs_ENTRE_PI(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,max(tab_angles_abs_entre_pi(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,moyenne(tab_angles_abs_entre_pi(1..nb_pts_inflex-2)),1,5,0); put(f," ");
--   flo_io.put(f,mediane(tab_angles_abs_entre_pi(1..nb_pts_inflex-2))/longueur_arc,1,5,0); put(f," ");
--   flo_io.put(f,ecart_type(tab_angles_abs_entre_pi(1..nb_pts_inflex-2))/longueur_arc,1,5,0);   NEW_LINE(f,1);
end mesures; 
-- ============================================================================ 
-- Mesures simples basees sur les PI et les sommets 
-- Surcharge de la procedure precedante ... 
-- ============================================================================ 
Procedure MESURES( ligne 		: in point_liste_type;		    npts		: in natural;  		    nb_pts_inflex	: in natural;  		    Tpoints_pred_pi	: in liens_access_type;  		    Tpinflex	 	: in point_liste_type;  		    Tab_sommets		: in point_liste_type;  		    Nb_sommets 		: in integer;    		    largeur_symbole	: in float;		    arc			: in integer;		    vect_mesures	: in vect_mesures_type;		    nb_mesures_total	: in integer;		    matrice_mesures	: in out matrice_type) is    
-- Mesures sur la ligne     
LONGUEUR_ARC : float := 0.0;     Tab_angles: reel_tableau_type(1..npts-1);    
-- Variables relatives aux mesures 
GLOBALES:    SURFACE_GLOBALE, HB_GLOBALE, SURF_MIN, SURF_MAX, ECART_SURF_TYPE								: float:=0.0;    TAB_ANGLES_ENTRE_PI: reel_tableau_type(1..nb_pts_inflex-1-1);    TAB_ANGLES_abs_ENTRE_PI: reel_tableau_type(1..nb_pts_inflex-1-1);    Longueur_ligne_PI : float:= 0.0;    
-- Variables relatives aux mesures localement sur un virage:    
Type TRIANGLE_type is record	base     : float:= 0.0;	Sommet   : gen_io.point_type := (0,0);        position_sommet: integer;       	hauteur  : float:= 0.0; 
-- Hauteur corresp. (en proj. orthogonale)        
hb	 : float:= 0.0; -- hauteur/base	
surface  : float:= 0.0; -- surface du triangle    
end record;    surface,	hb,	haut,	dpi,	abs_curv,	abs_curv_b,	pos_sommet: reel_tableau_type(1..nb_pts_inflex-1):= (others=>0.0);    h,hauteur: float:= 0.0;    sommet: gen_io.point_type;     Type VIRAGE_type is record	dist_entre_PI  : float:= 0.0;	abs_curv       : float:= 0.0;        nb_triangles   : integer := 0;        triangle       : triangle_type;        triangle1      : triangle_type;        triangle2      : triangle_type;        triangle3      : triangle_type;        triangle4      : triangle_type;    end record;     Package virage_io is new sequential_io(virage_type); use virage_io;    type TAB_VIRAGE_TYPE is array(integer range <>) of virage_type;    Virage,virage1,virage2,virage3,virage4:TAB_VIRAGE_TYPE(1..nb_pts_inflex-1);    REGULARITY 	: float := 0.0;     ANGULARITY 	: float := 0.0;     BLACKNESS  	: float := 0.0;     Resist	: float := 0.0;    X,Y : liens_array_type(1..npts);    ABR : float; -- "Area of boundary rectangle" (rect. englobant)    
cpt: integer := 0;    Type sorte_de_mesure_type is (min,max,moy,med,ect);    pas_df: integer; 
-- pour la dimension fractale    
--------------------------------------------------------------------------    
procedure test_presence (	vect_mesures 	: IN vect_mesures_type;			    	mesure 		: IN string7;				m 		: IN integer;				arc 		: IN integer;				sorte_de_mesure : IN sorte_de_mesure_type;				tab		: IN reel_tableau_type;				ntab		: IN integer;				cpt 		: IN OUT integer;				matrice_mesures : IN OUT matrice_type) 				is	valeur: float;    begin      if Vect_mesures(m).nom_mesure = mesure 		AND Vect_mesures(m).presence = True then         case sorte_de_mesure is 	  	When min => valeur:= min(tab(1..ntab));	  	When max => valeur:= max(tab(1..ntab));	  	When moy => valeur:= moyenne(tab(1..ntab));	  	When med => valeur:= mediane(tab(1..ntab));	  	When ect => valeur:= ecart_type(tab(1..ntab));	end case;        cpt:=cpt+1;        matrice_mesures(arc,cpt):= valeur;      end if;    end ; --------------------------------------------------------------------
BEGIN  
-------------------------------------------------------------------------   
-- LONGUEUR de la ligne --   
Longueur_arc := taille_ligne(ligne, npts);   -- ANGULARITE de la ligne :--   
for i in 1..npts-2 loop
--      TAB_ANGLES(i):= ABS(Angle_3points(ligne(i),
--				   	ligne(i+1),
--				   	ligne(i+2) ));
--   end loop;   
-- ***************************************   
-- == MESURES LOCALES sur chaque virage ==   
-- ***************************************   
for i in 1..nb_pts_inflex-1 loop     Virage(i).dist_entre_pi := Norme(tpinflex(i),tpinflex(i+1));     
-- ABSCISSES CURVILIGNES --     
virage(i).ABS_CURV:= Norme(tpinflex(i),ligne(tpoints_pred_pi(i)+1));     for j in tpoints_pred_pi(i)+1..tpoints_pred_pi(i+1)-1 loop        virage(i).abs_curv:= virage(i).abs_curv + norme(ligne(j),ligne(j+1));     end loop;     virage(i).abs_curv := virage(i).abs_curv 			 + norme(ligne(tpoints_pred_pi(i+1)-1),tpinflex(i+1));     virage(i).abs_curv := virage(i).abs_curv ;      
--------------------------------------     
-- Mesures sur le triangle principal :     
--------------------------------------     
Virage(i).triangle.base := Virage(i).dist_entre_pi;     hauteur:=distance_a_ligne(tab_sommets(i),tpinflex(i),tpinflex(i+1),C_ligne_type);     virage(i).triangle.hauteur := hauteur;     virage(i).triangle.sommet  := sommet;     virage(i).triangle.hb      := hauteur / virage(i).triangle.base;     
-- Calcul de la position du sommet (mesure de symetrie) :    
-- BUG en l'etat a l'execution
--     if angle_3points(tpinflex(i+1), tpinflex(i), sommet) > PI/2.0 then
--       virage(i).triangle.position_sommet := 1; -- -1;
--     elsif angle_3points(sommet, tpinflex(i+1), tpinflex(i)) < PI/2.0 then
--       virage(i).triangle.position_sommet := 0;
--     else
--       virage(i).triangle.position_sommet := 1;
--     end if;     
-- SURFACES --
virage(i).triangle.surface := virage(i).triangle.base * hauteur / 2.0;    
end loop; 
-- sur le nb de virages --   
for i in 1..nb_pts_inflex-1 loop       
-- Pour tester HAUTEUR/BASE a la place de la surface !!!!!!     
surface(i) := virage(i).triangle.surface; 			-- /longueur_arc;     
dpi(i)     := virage(i).dist_entre_pi ; 			-- /longueur_arc;     
abs_curv(i):= virage(i).abs_curv; 			-- /longueur_arc;     
abs_curv_b(i):= (virage(i).abs_curv / Virage(i).dist_entre_pi) ;		     	-- /longueur_arc;     
haut(i)	:= virage(i).triangle.hauteur ; 			-- /longueur_arc;     
hb(i)      := virage(i).triangle.hb ; 			-- /longueur_arc;     
pos_sommet(i):= (float(virage(i).triangle.position_sommet)			/float(nb_pts_inflex-1)) ;			-- /longueur_arc;   
end loop;   
-- ***********************************   
-- == MESURES GLOBALES sur la ligne ==   
-- ***********************************                            
for i in 1..nb_pts_inflex-1 loop      LONGUEUR_LIGNE_PI := LONGUEUR_LIGNE_PI + virage(i).dist_entre_pi;   end loop;   
-- LONGUEUR_LIGNE_PI:= LONGUEUR_LIGNE_PI / longueur_arc;   
for i in 1..nb_pts_inflex-1 loop      SURFACE_GLOBALE:= SURFACE_GLOBALE + virage(i).triangle.surface;   end loop;   for i in 1..nb_pts_inflex-1 loop      HB_GLOBALE:= HB_GLOBALE + virage(i).triangle.HB;   end loop;   
-- HB_GLOBALE:= HB_GLOBALE / longueur_arc;   
-- ANGLES de la ligne des PI --   
for i in 1..nb_pts_inflex-1-1 loop      
TAB_ANGLES_ENTRE_PI(i):= Angle_3points (  tpinflex(i),						tpinflex(i+1),						tpinflex(i+2) );      TAB_ANGLES_ABS_ENTRE_PI(i):= ABS(Angle_3points (	tpinflex(i),							tpinflex(i+1),							tpinflex(i+2) ));   end loop;   
-- ============================================================ --   
-- Mesures produites lors des reunions de travail avec MCMASTER --   
REGULARITY:= ecart_type(dpi(1..nb_pts_inflex-1)) 					/moyenne(dpi(1..nb_pts_inflex-1));
--							/ longueur_arc;    
ANGULARITY:= ecart_type(TAB_ANGLES_ABS_ENTRE_PI(1..nb_pts_inflex-1-1))
 							/ longueur_ligne_pi;     for l in 1..npts loop       X(l):=ligne(l).coor_x;       Y(l):=ligne(l).coor_y;   end loop;   ABR:=float(MAX(X(1..npts))-MIN(X(1..npts)))*float(MAX(Y(1..npts))-MIN(Y(1..npts)));    BLACKNESS  := longueur_arc * largeur_symbole / ABR;    cpt:=0;   for m in 1..nb_mesures_total loop    test_presence(vect_mesures,"min_dpi",m,arc,min,dpi(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_dpi",m,arc,max,dpi(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_dpi",m,arc,moy,dpi(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_dpi",m,arc,med,dpi(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_dpi",m,arc,ect,dpi(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_sur",m,arc,min,surface(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_sur",m,arc,max,surface(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_sur",m,arc,moy,surface(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_sur",m,arc,med,surface(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_sur",m,arc,ect,surface(1..nb_pts_inflex-1),nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_hte",m,arc,min,haut,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_hte",m,arc,max,haut,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_hte",m,arc,moy,haut,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_hte",m,arc,med,haut,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_hte",m,arc,ect,haut,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_abs",m,arc,min,abs_curv,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_abs",m,arc,max,abs_curv,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_abs",m,arc,moy,abs_curv,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_abs",m,arc,ect,abs_curv,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_abs",m,arc,med,abs_curv,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_htb",m,arc,min,hb,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_htb",m,arc,max,hb,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_htb",m,arc,moy,hb,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_htb",m,arc,ect,hb,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_htb",m,arc,med,hb,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_abb",m,arc,min,abs_curv_b,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_abb",m,arc,max,abs_curv_b,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_abb",m,arc,moy,abs_curv_b,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_abb",m,arc,med,abs_curv_b,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_abb",m,arc,ect,abs_curv_b,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"min_api",m,arc,min,tab_angles_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"max_api",m,arc,max,tab_angles_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_api",m,arc,moy,tab_angles_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"med_api",m,arc,med,tab_angles_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_api",m,arc,ect,tab_angles_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"min_spi",m,arc,min,tab_angles_abs_entre_pi,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"max_spi",m,arc,max,tab_angles_abs_entre_pi,nb_pts_inflex-1,cpt,matrice_mesures);    test_presence(vect_mesures,"med_spi",m,arc,med,tab_angles_abs_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"moy_spi",m,arc,moy,tab_angles_abs_entre_pi,nb_pts_inflex-2,cpt,matrice_mesures);    test_presence(vect_mesures,"ect_spi",m,arc,ect,tab_angles_abs_entre_pi,nb_pts_inflex-1,cpt,matrice_mesures);
--    test_presence(vect_mesures,"min_pso",m,arc,min,pos_sommet,nb_pts_inflex-1,cpt,matrice_mesures);
--    test_presence(vect_mesures,"max_pso",m,arc,max,pos_sommet,nb_pts_inflex-1,cpt,matrice_mesures);
--    test_presence(vect_mesures,"moy_pso",m,arc,moy,pos_sommet,nb_pts_inflex-1,cpt,matrice_mesures);
--    test_presence(vect_mesures,"med_pso",m,arc,med,pos_sommet,nb_pts_inflex-1,cpt,matrice_mesures);
--    test_presence(vect_mesures,"ect_pso",m,arc,ect,pos_sommet,nb_pts_inflex-1,cpt,matrice_mesures);
if Vect_mesures(m).nom_mesure="npi_lgr" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= float(Nb_pts_inflex-1)/longueur_arc;    end if;    if Vect_mesures(m).nom_mesure="lgr_lpi" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= longueur_ligne_pi;    end if;    if Vect_mesures(m).nom_mesure="regular" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= regularity;    end if;    if Vect_mesures(m).nom_mesure="angular" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= angularity;    end if;    if Vect_mesures(m).nom_mesure="ab_rect" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= ABR / longueur_arc;    end if;    if Vect_mesures(m).nom_mesure="blackne" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= blackness;    end if;    if Vect_mesures(m).nom_mesure="resiste" AND Vect_mesures(m).presence=True then         cpt:=cpt+1;        matrice_mesures(arc,cpt):= resistence(ligne, npts, 1.0);    end if;    if Vect_mesures(m).nom_mesure="dimfrac" AND Vect_mesures(m).presence=True then         cpt:=cpt+1; 	pas_df:= integer( Norme(ligne(1),ligne(npts)) / 2.0);        matrice_mesures(arc,cpt):= Dim_fractale( ligne, npts, pas_df, COMPAS);    end if;   end loop;end mesures;
-- ==========================================================================
-- Procedure de classification d'un ensemble de troncons d'arcs d'un graphe
-- (donc deja segmentes) 
--                       -> La procedure MESURES calcule les mesures choisies 
--                          dans le fichier PRESENCE_MESURES.DAT
-- 			 -> Les troncons sont classes par KMEANS
-- ==========================================================================
procedure Classification ( graphe 	: in graphe_type;			   legende 	: in legende_type;			   tab_arcs	: in liens_array_type;			   narc 	: in integer;			   nb_clusters 	: in integer;	                           fic_mesure	: in out file_type; 			   cluster      : in out liens_array_type ) is tpoints	  : point_liste_type(1..1000); nb_points	  : integer; ligne		  : point_access_type; ligne_lisse	  : point_liste_reel(1..1000):= (others=>(0.0,0.0)); nb_pts_inflex	  : natural:= 0; nb_virages	  : integer:= 0; 
-- = nb_pts_inflex-1 ! 
Tpoints_pred_pi  : liens_access_type; 
-- Indices des pts de la ligne pred le PI 
Tpinflex,  Tpinflex_lisse   : point_liste_type(1..1000); 
-- Coordonnees des PI -- 
Tab_sommets	  : point_liste_type(1..1000); -- Coordonnees des sommets -- 
Nb_sommets 	  : integer := 0; nb_pts_critiques : integer := 0; Tpcritiques      : liens_array_type(1..1000); -- Coordonnees des pts Critiques -- 
nbf  		  : integer; nomf 		  : string(1..80); cpt,npt  	  : integer; lgn		  : string(1..700); c		  : string(1..7) := "mesures"; ncar,i,last	  : integer; nom_arc	  : string(1..6); nnom_arc	  : integer; lgr_ligne	  : float; affich		  : affichage_type; largeur_symbole  : float := 3.0; SIGMA	 	  : float := 20.0; arc		  : integer; nb_mesures_total : integer:= 53; -- nombre total de mesures 
nb_mesures       : integer:= nb_mesures_total; -- nombre de mesures 
Matrice_mesures  : Matrice_Type (1..narc,1..100); Vect_num_arcs	  : Vect_num_arcs_Type(1..narc):=(others=>0); Vect_noms_arcs	  : Vect_noms_arcs_Type(1..narc):=(others=>"       "); matrice_centres  : matrice_type(1..nb_clusters,1..100);-- C 
nb_arcs_cluster  : liens_array_type (1..nb_clusters):=(others=>0); ifault           : natural :=0; fclust,fmes,f	  : text_io.file_type; moy		  : float; vecteur_mesures  : reel_tableau_type(1..narc); presence, cptr	  : integer; vecteur    	  : reel_tableau_type(1..narc); seuil		  : float ; Vect_mesures 	  : Vect_mesures_Type(1..nb_mesures_total)		  := (	 			("min_dpi",False), -- Bases
("max_dpi",False),			("med_dpi",False), 			("moy_dpi",False),			("ect_dpi",false), 			("min_sur",False), -- Surfaces			
("max_sur",False),			("med_sur",False),  			("moy_sur",False),			("ect_sur",False),     			("min_hte",False), -- Hauteurs			
("max_hte",False),			("med_hte",False),  			("moy_hte",False),			("ect_hte",False),  			("min_abs",False), -- Abscisses curvilignes			
("max_abs",False),			("med_abs",False),  			("moy_abs",False),			("ect_abs",False),  			("min_htb",False), -- Hauteurs sur bases			
("max_htb",False),			("med_htb",false), 			("moy_htb",False),			("ect_htb",false), 			("min_abb",False), -- Abscisses curvilignes sur bases			
("max_abb",False), 			("med_abb",false), 			("moy_abb",False),			("ect_abb",false), 			("min_api",False), -- Valeurs des angles entre PI			
("max_api",False),			("med_api",false), 			("moy_api",False),			("ect_api",false), 			("min_spi",False), -- Valeurs absolues des angles entre PI			
("max_spi",False),			("med_spi",false),  			("moy_spi",False),			("ect_spi",false),  			("min_pso",False), -- Positions des sommets			
("max_pso",False),			("med_pso",False),			("moy_pso",False),			("ect_pso",False),			("npi_lgr",False),  -- X Nb de PI rapporte a la longueur			
("lgr_lpi",False),  -- X Longueur de la ligne des PI			
("regular",False),			("angular",False),			("ab_rect",False),			("blackne",False),			("resiste",False),			("dimfrac",False) );begin  
-- ----------------------------------------- 
-- Lecture du fichier des mesures a calculer 
-- ------------------------------------------- 
open (fcl, in_file, "d$plazanet:[plazanet.mesures]Presence_mesures.dat"); cptr:=0; while not end_of_file(fic_mesure) loop    cptr:= cptr+1;--    skipandget(fic_mesure, presence);    
loop       begin         int_io.get(fic_mesure,presence);         exit;         exception when text_io.data_error=>skip_line(fic_mesure);       end;    end loop;    if presence = 1 then 	Vect_mesures(cptr).presence := TRUE;    end if; end loop;	     		  close(fic_mesure); -- Nb de mesures reellement calculees : 
nb_mesures:= 0; for m in 1..nb_mesures_total loop    if Vect_mesures(m).presence = true then	nb_mesures:= nb_mesures+1;    end if; end loop; for a in 1..narc loop    for m in 1..nb_mesures loop	matrice_mesures(a,m):= (0.0);    end loop; end loop;
-- legende := Gr_load_from_disk_legende("d$plazanet:[plazanet.graphes]R2");
-- ===================================================== --
-- -- Calcul de la matrice des mesures sur chaque arc -- --
-- ===================================================== --
for a in 1..narc loop  GR_POINTS_D_ARC(graphe,tab_arcs(a),tpoints,Nb_points);   ligne:= new point_liste_type(1..nb_points);  for i in 1..nb_points loop	ligne(i).coor_x:= tpoints(i).coor_x;	ligne(i).coor_y:= tpoints(i).coor_y;  end loop;  lgr_ligne := taille_ligne(ligne(1..nb_points),nb_points);  SIGMA:= lgr_ligne/100.0 * log(lgr_ligne/100.0);   if sigma<1.0 then sigma:=1.0; end if;
--  if nb_points > 5 then   
Pts_caracteristique.INFLEXIONS (  ligne(1..nb_points), nb_points, Sigma,       ligne_lisse,nb_pts_inflex, Tpoints_pred_pi, Tpinflex, Tpinflex_lisse);   nb_virages := nb_pts_inflex-1;               Pts_caracteristique.SOMMETS (ligne(1..nb_points),nb_points, SIGMA,       nb_pts_inflex,Tpoints_pred_pi,Tab_sommets, Nb_sommets);   if nb_virages > 1 then    Affich:= GR_LEGENDE_L(legende, GR_ARC(graphe,tab_arcs(a)).att_graph);    Largeur_symbole := affich.largeur ; 
-- Car c'est en mm !!!    
MESURES(ligne(1..nb_points),nb_points,nb_pts_inflex,Tpoints_pred_pi,		Tpinflex, Tab_sommets, Nb_sommets, Largeur_symbole, a, 		vect_mesures, nb_mesures_total, matrice_mesures );   else     put("ATTENTION : un arc non mesure !"); new_line(1);   end if;
--  end if;-- 
if sigma > 5.0 then  int_io.put(tab_arcs(a));
--  put("  Sigma = ");	flo_io.put(sigma,1,2,0);  new_line(1);-- 
end if;end loop; 
-- sur une serie d'arcs -- 
-- ============================= -- 
-- | Normalisation des mesures | -- 
-- ============================= -- 
-- Calcul du centre de gravite du nuage (moyenne de chaque colonne) 
-- Puis recentrage de chaque mesure par rapport a la mesure moyenne:
-- for m in 1..nb_mesures loop
--	for a in 1..narc loop
--	      	vecteur_mesures(a):= matrice_mesures(a,m);
--	end loop;	
--	moy:= moyenne(vecteur_mesures(1..narc));
--	for a in 1..narc loop
--	      	matrice_mesures(a,m):= matrice_mesures(a,m)-moy;
--	end loop;		
-- end loop;
if nb_mesures = 1 then   for a in 1..narc loop     	vecteur(a):= matrice_mesures(a,1);   end loop;   seuil:= 1.0; 
-- Pour complexite(1.2) ou regularite(0.5) ou dim fract (1.0)   
Class_vect( vecteur, narc, seuil, cluster, nb_arcs_cluster );else 
-- ========================================================= -- 
-- | Initialisation de la matrice des centres des clusters | -- 
-- ========================================================= -- 
-- Ce calcul est indispensable en entree de l'algorithme KMEANS 
-- Le nombre de lignes doit correspondre au nombre de clusters souhaites 
-- Le nombre de colonnes doit correspondre au nombre de mesures : 
create(fclust,out_file,"matrice_centres.splus"); for k in 1..nb_clusters loop    	arc:= 1 + (k-1)*narc/nb_clusters; 	for m in 1..nb_mesures loop	   matrice_centres(k,m):= matrice_mesures(arc,m);	   flo_io.put(fclust,matrice_centres(k,m),1,5,0);put(fclust," ");	end loop;	new_line(fclust,1); end loop;  	 close(fclust);                       KMEANS (matrice_mesures, narc, nb_mesures, nb_clusters,	 matrice_centres, cluster, nb_arcs_cluster, ifault); if ifault = 0 then	   	put("Aucune erreur n'a ete detectee ");	   	new_line(1); elsif ifault = 1 then		put("Au moins un cluster est vide lors de l'affectation initiale, il y aura donc en sortie moins de clusters que prevu ");		new_line(1); elsif ifault = 2 then		put("Le nombre maximum d'iterations est depasse ");		new_line(1); elsif ifault = 3 then		put("Le nombre de clusters est inferieur a 3 ou superieur au nombre d'arcs, veuillez relancer le programme "); 	     	new_line(1); end if;	
-- new_line(1);-- put("Cluster d'appartenance de chaque arc : "); new_line(1);-- 
for a in 1..narc loop
--  int_io.put(tab_arcs(a)); int_io.put(cluster(a)); --  new_line(1);-- 
end loop; put("Centres des clusters : ");new_line(1); for l in 1..nb_clusters loop	for j in 1..nb_mesures loop		flo_io.put(matrice_centres(l,j));new_line(1);	        put (" ");	end loop;	new_line(1); end loop;end if; 
-- sur le nb de mesures -- 
put("Nb d'arcs par cluster : "); new_line(1); for c in 1..nb_clusters loop	   int_io.put(c);	   int_io.put(nb_arcs_cluster(c));	   new_line(1); end loop; 
-- ---------------------------------------- 
-- Ecriture fichier des mesures calculees : 
-- ---------------------------------------- 
create(f,    out_file, "matrice_mesures.splus"); for a in 1..narc loop    for m in 1..nb_mesures loop  	int_io.put(f,tab_arcs(a)); 	int_io.put(f,cluster(a));  put(f," "); 	flo_io.put(f,matrice_mesures(a,m),1,7,0);        put(f," ");    end loop;		    new_line(f,1); end loop; close(f); 
-- ------------------------------------------------ 
-- Ecriture fichier des noms de mesures calculees : 
-- ------------------------------------------------ 
create(fmes,out_file,"Noms_de_mesures.splus"); for m in 1..nb_mesures_total loop    if Vect_mesures(m).presence = true then 	put(fmes,Vect_mesures(m).nom_mesure);new_line(fmes,1);    end if; end loop; close(fmes); put("Voir les resultats dans le fichier --> ");put("Matrice_mesures.splus"); new_line(1); if nb_mesures > 1 then     put("Matrice des centres des clusters   --> ");put("Matrices_centres.splus"); new_line(1); end if; put("Mesures dans                       --> ");put("Noms_de_mesures.splus"); new_line(1); new_line(1);end classification; 
------------------------------------------------------------
end ;
