with gen_io; use gen_io;

--With float_math_lib; use float_math_lib;	-- pour VMS avant ADA 95
with Ada.Numerics; use Ada.Numerics;		-- depuis ADA 95
with Ada.Numerics.Elementary_Functions; use  Ada.Numerics.Elementary_Functions;	-- depuis ADA 95

with math_int_basic; use math_int_basic;

with unchecked_deallocation;
    

package body vect is 

  PI : Constant Float := 3.1415926535 ;		-- déclaration de PI (depuis ADA 95)

-------------------------------------------------

function "+"(a,b : vector) return vector is
begin
  return(a.x+b.x,a.y+b.y);
end "+";
  
-------------------------------------------------

function "-"(a,b : vector) return vector is
begin
  return(a.x-b.x,a.y-b.y);
end "-";

------------------------------------------------- 
        
function "*" (a: float; b : vector) return vector is
begin
  return(a*b.x,a*b.y);
end "*";

--------------------------------------------------

function "*"(a,b : vector) return vector is
begin
  return(a.x*b.x-a.y*b.y,a.x*b.y+a.y*b.x);
end "*";

-------------------------------------------------- 
        
function "/" ( b : vector; a:float ) return vector is
begin
  return(b.x/a,b.y/a);
end "/";
        
-------------------------------------------------- 
                                                  
function "/"(a,b : vector) return vector is
d:float;
begin
  d:=b.x*b.x+b.y*b.y;
  return((a.x*b.x+a.y*b.y)/d,(a.y*b.x-a.x*b.y)/d);   
end "/";

------------------------------------------------- 

function prod_scal(a,b : vector ) return float is
begin
  return(a.x*b.x+a.y*b.y); 
end prod_scal;

--------------------------------------------------     

function prod_vect(a,b: vector) return float is

begin
  return(a.x*b.y-a.y*b.x);
end prod_vect;

---------------------------------------------------

function unitaire(alpha: float) return vector is

begin
  return( (cos(alpha),sin(alpha)) );
end unitaire;


---------------------------------------------------

function norme(a: vector) return float is
begin
  return(sqrt(a.x*a.x+a.y*a.y));
end norme;

--------------------------------------------------
 
function argument(a: vector) return float is
 costeta,sinteta: float; 
 lg: float :=norme(a);
begin
  if lg<epsilon then return(100.0);
  elsif a.x>=0.0 then return(arcsin(a.y/lg));
  elsif a.y>0.0 then return(pi-arcsin(a.y/lg));
  else return(-pi-arcsin(a.y/lg));
  end if;
end argument;

--------------------------------------------------

function deviation(a,b: vector) return float is
 den: float :=norme(a)*norme(b);
 sinus: float :=prod_vect(a,b)/den;
begin          
  if den>epsilon then
    if prod_scal(a,b)>0.0 then return(arcsin(sinus));
    elsif sinus>0.0 then 
      return(pi-arcsin(sinus));
    else  
      return(-pi-arcsin(sinus));
    end if;
  else return(0.0); 
  end if;
end deviation;

--------------------------------------------------

function length(line_in: line) return float is
l:float :=0.0;
prec,interm:line :=line_in;
begin
  if (interm/=null) then interm:=interm.suiv; end if;
  while (interm/=null) loop
    l:=l+norme(prec.vect-interm.vect);
    prec:=interm;
    interm:=interm.suiv;
  end loop;
return(l);
end length;
   
----------------------------------------------------

function dist_point_segm(point,extrem0,extrem1: vector; 
                         epsilon: float:= 0.000000001) 
                                        return float is
  extrem2: vector :=extrem0;
  base: vector;
  lg_base,proj,d: float;
begin                           
  base:=extrem1-extrem0;
  lg_base:=norme(base);
  if (lg_base>epsilon) then
    proj:=prod_scal(base,point-extrem0);
    if (proj>0.0) then
      if (proj<lg_base*lg_base) then
        d:=prod_vect(base,point-extrem0)/lg_base;
        return(abs(d));
      else
        extrem2:= extrem1;
      end if;
    end if;
  end if; 
  return(norme(extrem2-point));
end dist_point_segm;
   
----------------------------------------------------

function dist_point_segm(point: vector; segment: line;
                         epsilon: float:= 0.000000001) 
                                        return float is
begin
  if (segment.suiv/=null) then 
    return(dist_point_segm(point,segment.vect,segment.suiv.vect));
  else
    return(norme(point-segment.vect));
  end if;
end dist_point_segm;                                                          

-------------------------------------------------            

function duplicata(line_in: line) return line is
 line1: line :=line_in;
 line2,germe: line;
begin
  germe:= new elemline;
  line2:=germe;
  while (line1/=null) loop
    line2.suiv:=new elemline'(line1.vect,line2,null);
    line2:=line2.suiv;
    line1:=line1.suiv;
  end loop;
  line2:=germe.suiv;
  recupere(germe);
  line2.prec:=null;
  return(line2);
end duplicata;
                                                             
-------------------------------------------------            
          
procedure dissoudre(line_in: in out line) is 
line1: line;
begin
  while(line_in/=null) loop
    line1:=line_in;
    line_in:=line_in.suiv;
    recupere(line1);
  end loop;
end;
----------------------------------------------------

function hauteur(pt: line) return float is
f:float;
begin
  f:=abs(prod_vect(pt.vect-pt.suiv.vect,pt.vect-pt.prec.vect));
  if f/=0.0 then 
    return(f/norme(pt.prec.vect-pt.suiv.vect)) ;
  else
   return(0.0);
  end if;
end hauteur;

-------------------------------------------------            
                 
procedure comprime(line_in: line; seuil: in out float ;
                   nbpoint_out,mode:in out integer) is
                 
 type dist_intern;
 type dist_list is access dist_intern;
 type dist_intern is record
                       val: float;
                       prec,suiv: dist_list;
                     end record;  
 min:float;
 nbpoint: integer :=2;
 vectsuiv,vect,vectpre: vector;
 line1,marq,compt: line;                        
 chaine,marqdist,comptdist,comptdist2: dist_list;     
          
 procedure recupere is new unchecked_deallocation (dist_intern, dist_list); 
                
 function test_arret return boolean is 
 begin
  if (mode=1 or mode=3) and min>seuil then
    mode:=-1;
    nbpoint_out:=nbpoint;
    return(true);
  elsif (mode=2 or mode=3) and nbpoint<=nbpoint_out then
    mode:=-2;
    seuil:=min;
    return(true);
  elsif chaine=null then
    mode:=0;
    nbpoint_out:=nbpoint;
    seuil:=min;
    return(true);
  end if;
  return (false);
 end test_arret;

begin
  if (line_in=null) then null;               -- ensemble vide
  elsif (line_in.suiv=null) then null;       -- un point 
  elsif (line_in.suiv.suiv=null) then null;  -- un segment. Ce cas est il necessaire ?
  else
    chaine :=new dist_intern;
    comptdist:=chaine;
    compt:=line_in.suiv; 
    while compt.suiv/=null loop
      nbpoint:=nbpoint+1;
      chaine.suiv:=new dist_intern'(hauteur(compt),chaine,null);
      chaine:=chaine.suiv;
      compt:=compt.suiv;
    end loop;
    chaine:=comptdist.suiv;
    chaine.prec:=null;
    recupere(comptdist);                                  
    loop
      min:=chaine.val;
      marqdist:=chaine;
      comptdist:=marqdist;
      marq:=line_in.suiv;
      compt:=marq;
      while (comptdist/=null) loop
        if comptdist.val<min then 
          min:=comptdist.val;
          marqdist:= comptdist;
          marq:= compt; 
        end if;                   
        comptdist:=comptdist.suiv;
        compt:=compt.suiv;
      end loop;
      if test_arret then
        return;-- fin de la procedure: La conpression est suffisante.
      else
        compt:=marq.suiv;
        compt.prec:=marq.prec;
        compt.prec.suiv:=compt;
        recupere(marq);          
        comptdist:=marqdist.prec;
        comptdist2:=marqdist.suiv;
        if comptdist/=null then 
          comptdist.val:=hauteur(compt.prec);
          comptdist.suiv:=comptdist2;
        else
          chaine:=comptdist2;
        end if;
        if comptdist2/=null then
          comptdist2.val:=hauteur(compt);
          comptdist2.prec:=comptdist; 
        end if;
        recupere(marqdist);
        nbpoint:=nbpoint-1;                                      
      end if;
    end loop;
  end if;
end comprime;

-------------------------------------------------            

                                                                  
procedure convert(line_in: line; tableau: point_access_type; 
					nbpoint_out :out integer) is
  line1: line:=line_in;			     -- tableau doit etre alloue
  indic,mode,nbpoint: integer:=0;            -- avant l'entree de convert.
  ecart: float :=0.0;
begin                    
   while line1/=null loop 
    line1:=line1.suiv;                    
    indic:=indic+1;                       
  end loop;         
  if indic>tableau.all'length then
    nbpoint:=tableau.all'length;
    mode:=2;
    comprime(line_in,ecart,nbpoint,mode);
--    alerte_comprime(ecart);
  else nbpoint:=indic;
  end if;                    
  line1:=line_in;
  indic:=tableau'first;
  while line1/=null loop 
    tableau.all(indic).coor_x:=integer(line1.vect.x);
    tableau.all(indic).coor_y:=integer(line1.vect.y);       
    indic:=indic+1;           
    line1:=line1.suiv;                    
  end loop;
  nbpoint_out:=nbpoint;                               
end;


-------------------------------------------------            
                                                                
procedure convert(line_in: line; tableau: out point_liste_type; 
					nbpoint_out :out integer) is
  line1: line:=line_in;
  indic,mode,nbpoint: integer:=0;
  ecart: float :=0.0;
begin                    
   while line1/=null loop 
    line1:=line1.suiv;                    
    indic:=indic+1;                       
  end loop;         
  if indic>tableau'length then
    nbpoint:=tableau'length;
    mode:=2;
    comprime(line_in,ecart,nbpoint,mode);
--    alerte_comprime(ecart);
  else nbpoint:=indic;
  end if;                    
  line1:=line_in;
  indic:=tableau'first;
  while line1/=null loop 
    tableau(indic).coor_x:=integer(line1.vect.x);
    tableau(indic).coor_y:=integer(line1.vect.y);       
    indic:=indic+1;                       
    line1:=line1.suiv;                    
  end loop;
  nbpoint_out:=nbpoint;                               
end;

-------------------------------------------------
                                                               
procedure convert(tableau: point_liste_type; nbpoint:integer; 
                                             line_out:out line) is

  line1: line := new elemline'(vectnul,null,null);
  line2: line :=line1;
  deb: integer :=tableau'first;
  vect: vector;
begin                
  for i in deb..deb+nbpoint-1 loop
    vect.x:=float(tableau(i).coor_x);
    vect.y:=float(tableau(i).coor_y);
    line1.suiv:=new elemline'(vect,line1,null);
    line1:=line1.suiv;
  end loop;
  line_out:=line2.suiv;
  line2.suiv.prec:=null;
  recupere(line2);
end convert;

-------------------------------------------------

procedure convert(tableau: point_access_type; nbpoint:integer; 
                                             line_out:out line) is

  line1: line := new elemline'(vectnul,null,null);
  line2: line :=line1;
  deb: integer :=tableau.all'first;
  vect: vector;
begin                
  for i in deb..deb+nbpoint-1 loop
    vect.x:=float(tableau.all(i).coor_x);
    vect.y:=float(tableau.all(i).coor_y);
    line1.suiv:=new elemline'(vect,line1,null);
    line1:=line1.suiv;
  end loop;
  line_out:=line2.suiv;
  line2.suiv.prec:=null;
  recupere(line2);
end convert;

-------------------------------------------------
end vect;                          

