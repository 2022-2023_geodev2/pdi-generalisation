---------------------------------------------------------------------------------
-- Specifications du package de gestion de la structure memorisant le squelette.
-- Il s'agit d'un arbre aux particularites suivantes:
--  * Il n'y a pas de racine mais on maintient une liste des extremites
--  * Divers champs et methodes d'acces associees facilitent la gestion des
--    caracteristiques topologiques du squelette.
--  * La structure intime de TOUS les composants de l'arbre est VRAIMENT
--    cachee a l'utilisateur du package qui ne manipule que des
--    identificateurs et le type prive Arbre
--  * La gestion de la memoire permet les creations/destructions intenses
--    sans problemes et est COMPLETEMENT dynamique et propre:
--    quand on doit creer un 11eme noeud alors que la structure possede un
--    pointeur allouant seulement 10 noeud, on libere cette memoire et on
--    realloue pour 20 noeuds.
---------------------------------------------------------------------------------
-- Bruno Hergott
-- Octobre 97
-------------------

-- Librairies COGIT
With gen_io; use gen_io;

Package PArbre is

-- Cela ne vous regarde pas..
type Arbre is private;

-- tableau de reservations utilise pour memoriser les marquages des arcs
Type Resas is array(natural range <>) of boolean;

-- une liste vide utilisee comme parametre par defaut
point_liste_vide : point_liste_type(1..0);

--------------------------
-- Diverses extractions --
--------------------------

-- donne le tableau des 3 identificateurs d'arcs du noeud
-- un identificateur a 0 = absence d'arc, mais il peut etre suivi d'un non nul
function arcs_du_noeud(	lArbre : in Arbre;
			idNoeud: in Positive) return liens_array_type;

-- donne l'arc entre les noeuds ou 0 s'il n'existe pas
function arc_entre_noeuds(lArbre	: in Arbre;
			  noeudIni	: in positive;
			  noeudFin	: in positive) return natural;

-- donne un pointeur sur le tableau des identificateurs des extremites
function extrait_extremites(lArbre	: in Arbre) return liens_access_type;

-- donne un pointeur sur le tableau des points de l'arc
function points_arc(	lArbre : in Arbre;
			idArc  : in positive) return point_access_type;

-- donne un pointeur sur le tableau des points du chemin
-- un chemin est une suite de noeuds voisins
function points_chemin( lArbre : in Arbre;
			chemin : in liens_access_type) return point_access_type;

-- donne les deux noeuds composant l'arcs
procedure noeuds_arc(	lArbre : in Arbre;
			idArc  : in positive;
			noeud_ini, noeud_fin : out positive);

------------------------
-- Diverses Creations --
------------------------

-- creation d'un noeud independant
procedure creer_noeud(	lArbre : in out Arbre;
			idNoeud: out positive);

-- creation d'un arc entre deux noeuds
procedure creer_arc(	lArbre : in out Arbre;
			noeudIni, noeudFin : in positive;
			tabPoints : in point_access_type;
			idArc : out positive);

-- mets a jour les points constituant un arc
procedure set_points_arc(lArbre : in Arbre;
			idArc  : in positive;
			tabPoints: in point_access_type);

-- Copie de l'arbre
procedure copie_arbre(	Arba : in Arbre;
		     	Arbb : in out Arbre);

-----------------
-- Conversions --
-----------------

-- procedure testee (reseau hydrographique) mais non utilisee dans mon stage.
-- sert a la traduction d'un graphe de gen_io en un arbre utilise ici.
-- noeudebut indique un noeud du graphe appartenant a la composante connexe
-- que l'on veut traduire.
-- (!) on suppose qu'il y a au maximum 3 arcs par noeuds
procedure graphe2arbre (graphe : in graphe_type;
			lArbre : in out Arbre; 
			noeudebut : in positive := 1);

-- Ajoute au graphe un ensemble de noeuds et d'arcs representant l'arbre
-- noeudRef donne un noeud du graphe appartenant a cette extension
procedure arbre2graphe (lArbre		: in arbre;
			graphe		: in out graphe_type;
			noeudRef	: out positive);

-----------------------------------------------------------------------------
-- methodes utiles par exemple pour la memorisation du parcours de l'arbre --
-----------------------------------------------------------------------------

-- donne la taille actuelle du tableau contenant les arcs du graphe
-- fonction necessaire pour sauver les Resas avant de les utiliser
-- (!) il ne s'agit pas du nombre d'arcs mais il varie aussi
function nb_max_arcs(	lArbre : in Arbre) return positive;

-- libere les reservations sur tous les arcs
procedure init_arcs_pris(lArbre : in out Arbre);

-- sauve les reservations dans le tableau Resas
procedure sauve_resas(lArbre: in Arbre;
		      rep:    out Resas);

-- charge les reservations venant du tableau Resas
procedure charge_resas( lArbre: in out Arbre;
			flags:  in Resas);

-- reserve l'arc
procedure prends_arc(	lArbre : in out Arbre;
			idArc  : in positive);

-- etat de la reservation de l'arc
function arc_pris(	lArbre : in Arbre;
			idArc  : in natural) return boolean;

----------------------------
-- Diverses Destructions  --
----------------------------

-- Detruis le noeud et ses arcs
procedure detruire_noeud(lArbre : in out Arbre;
			idNoeud : in Positive);

-- Detruis tout les noeuds et arcs de l'arbre
procedure vide_arbre( lArbre : in out Arbre);

-----------------------------------------------------------------------------------
-- transformation et consultation des caracteristiques geometriques du squelette --
-----------------------------------------------------------------------------------

-- l'ecartement d'un noeud x est max(des noeuds y) de d(x,y)
procedure ecartement(	lArbre : in out Arbre;
			idNoeud: in positive;
			noeudMax: out positive;
			distants: out natural;
			chemin  : out liens_access_type);

-- longueur de l'arc
function longueur_arc(	lArbre : in Arbre;
			idArc  : in positive) return natural;

-- distance entre deux noeuds
procedure distance (	lArbre : in out Arbre;
			idNa   : in positive;
			idNb   : in positive;
			dist   : out natural;
			etapes : out liens_access_type);

-- Epure le squelette en supprimant les extremites trop petites
-- et, en option, les points des extremites proches du contour
procedure epure(	lArbre : in out Arbre;
			dNoeud : in float;   -- distance minimale inter noeuds
			dSurf  : in float := 0.0;   -- distance minimale au contour
			ptSurf : in point_liste_type := point_liste_vide );  

private
-- meme en private on en dit le moins possible..
-- de toute facon, cela ne vous regarde pas..

  Type tab_arcs;
  Type a_arcs is access tab_arcs;
  Type tab_noeuds;
  Type a_Noeuds is access tab_noeuds;

  Type Arbre is record
    nbExtremites	: natural := 0;
    extremites    	: liens_access_type := null;
    nbNoeuds		: natural := 0;
    noeuds		: a_Noeuds := null;
    nbArcs		: natural := 0;
    arcs		: a_arcs := null;
  end record;

end PArbre;
