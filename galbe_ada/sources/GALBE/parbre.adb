-----------------------------------------------------------------------------
-- Body du package de gestion de la structure memorisant le squelette.
-- Il s'agit d'un arbre aux particularites suivantes:
--  * Il n'y a pas de racine mais on maintient une liste des extremites
--  * Divers champs et methodes d'acces associees facilitent la gestion des
--    caracteristiques topologiques du squelette.
-----------------------------------------------------------------------------
-- Bruno hergott
-- Octobre 97
----------------------

-- Librairies Systeme
with UNCHECKED_DEALLOCATION;
with text_io ; use text_io ;

-- Librairies COGIT
with gen_io; use gen_io;
with Geometrie;

package body PArbre is


Package real_io is new Float_io(float);use real_io;
Package int_io is new Integer_io(integer);use int_io;

-- Definition intime de l'arbre.
-- tab_arcs, qui est un access(tableau dynamique), est compose d'access(les arcs)
-- eux meme compose d'access(les points). Tout ca pour une gestion dynamique.

Type arc is record
  noeud_ini, noeud_fin : natural := 0;
  tabPoints : point_access_type := null;  -- les points composant l'arc
  distance : natural := 0;
  flag : boolean := false;  -- entre autre pour prends_arc(..) et arc_pris(..)
end record;

Type a_arc is access arc;
Type tab_arcs is array(natural range <>) of a_arc;

-- Un noeud d'un arbre a au maximum 3 arcs.
Type noeud is record
  arcs : liens_array_type(1..3);
end record;

Type a_noeud is access noeud;
Type tab_noeuds is array(natural range <>) of a_noeud;

-- Au depart, l'arbre peut contenir 10 noeuds, 10 extremites et 10 arcs
NB_NOEUDS_INI : constant positive := 10;
NB_BOUTS_INI : constant positive := 10;
NB_ARCS_INI : constant positive := 10;

procedure Libere_access_tab_arcs is
	new UNCHECKED_DEALLOCATION(tab_arcs, a_arcs);
procedure Libere_access_tab_noeuds is
	new UNCHECKED_DEALLOCATION(tab_noeuds, a_noeuds);
procedure Libere_access_arc is
	new UNCHECKED_DEALLOCATION(arc, a_arc);
procedure Libere_noeud is
	new UNCHECKED_DEALLOCATION(noeud, a_noeud);

procedure Libere_arc(A : in out a_arc) is
begin
  GR_free_point_liste(A.tabPoints);
  Libere_access_arc(A);
end;

Procedure Libere_tab_arcs(T : in out a_arcs) is
begin
  for i in T'range loop
    if T(i) /= null then
      Libere_arc(T(i));
    end if;
  end loop;
  Libere_access_tab_arcs(T);
end;

Procedure Libere_tab_noeuds(T : in out a_noeuds) is
begin
  for i in T'range loop
    if T(i) /= null then
      Libere_noeud(T(i));
    end if;
  end loop;
  Libere_access_tab_noeuds(T);
end;


------------------------------------------------------------------
-- outils specifiques au body utilisees seulement par le package -
------------------------------------------------------------------

function place_noeud_libre( arb : in Arbre) return natural is
pos : natural := 0;
begin
  for i in arb.noeuds.all'range loop
    if arb.noeuds(i)=null then
      pos := i;
      exit;
    end if;
  end loop;
  return pos;
end;

function place_arc_libre( arb : in Arbre) return natural is
pos : natural := 0;
begin
  for i in arb.arcs.all'range loop
    if arb.arcs(i)=null then
      pos := i;
      exit;
    end if;
  end loop;
  return pos;
end;

function place_extremite_libre( arb : in Arbre) return natural is
pos : natural := 0;
begin
  for i in arb.extremites.all'range loop
    if arb.extremites(i)=0 then
      pos := i;
    end if;
  end loop;
  return pos;
end;

procedure ajoute_extremite(lArbre : in out Arbre;
			idnoeud : in positive) is
tabExt : liens_access_type;
ide, tailleTab : natural;
begin
  lArbre.nbExtremites := lArbre.nbExtremites + 1;
  if lArbre.nbExtremites/=1 then
    tailleTab := lArbre.extremites.all'length;
    if lArbre.nbExtremites>tailleTab then
      tabExt := new liens_array_type(1..tailleTab);
      tabExt(1..tailleTab) := lArbre.extremites(1..tailleTab);
      GR_Free_liens_array(lArbre.extremites);
      lArbre.extremites := new liens_array_type(1..tailleTab*2);
      lArbre.extremites.all := (1..tailleTab*2 => 0);
      lArbre.extremites(1..tailleTab) := tabExt(1..tailleTab);
    end if;
  else
    lArbre.extremites := new liens_array_type(1..NB_BOUTS_INI);
    lArbre.extremites.all := (1..NB_BOUTS_INI => 0);
  end if;
  ide := place_extremite_libre(lArbre);
  lArbre.extremites(ide) := idnoeud;
end;

procedure enleve_extremite(lArbre : in out Arbre;
			idnoeud : in positive) is
begin
  for i in 1..lArbre.extremites.all'length loop
    if lArbre.extremites(i)=idnoeud then
      lArbre.extremites(i) := 0;
    end if;
  end loop;
  lArbre.nbExtremites := lArbre.nbExtremites - 1;
end;

procedure ajoute_arc_a_noeud(	lArbre	: in out Arbre;
				arc	: in positive;
				noeud	: in positive) is
lesArcs : liens_array_type(1..3);
pos, cpt : natural := 0;
begin
  lesArcs := lArbre.noeuds(noeud).arcs;
  for i in lesArcs'range loop
    if lesArcs(i)=0 then
      pos := i;
      cpt := cpt + 1;
    end if;
  end loop;
  if cpt=3 then
    ajoute_extremite(lArbre, noeud);
  end if;
  if cpt=2 then
    enleve_extremite(lArbre, noeud);
  end if;
  lArbre.noeuds(noeud).arcs(pos) := arc;
end;

procedure enleve_arc_a_noeud(	lArbre	: in out Arbre;
				arc	: in positive;
				noeud	: in positive) is
lesArcs : liens_array_type(1..3);
pos, cpt : natural := 0;
begin
  lesArcs := lArbre.noeuds(noeud).arcs;
  for i in lesArcs'range loop
    if lesArcs(i)/=0 then
      cpt := cpt + 1;
      if lesArcs(i)=arc then
        pos := i;
      end if;
    end if;
  end loop;
  if cpt=1 then
    enleve_extremite(lArbre, noeud);
  end if;
  if cpt=2 then
    ajoute_extremite(lArbre, noeud);
  end if;
  lArbre.noeuds(noeud).arcs(pos) := 0;
end;

procedure detruire_arc(	lArbre : in out Arbre;
			idArc : in Positive) is
begin
  lArbre.nbArcs := lArbre.nbArcs - 1;
  GR_Free_point_liste(lArbre.arcs(idArc).tabPoints);
  enleve_arc_a_noeud(lArbre, idArc, lArbre.arcs(idArc).noeud_Ini);
  enleve_arc_a_noeud(lArbre, idArc, lArbre.arcs(idArc).noeud_Fin);
  Libere_arc(lArbre.arcs(idArc));
  lArbre.arcs(idArc) := null;
end;

function calcul_lg_arc(	lArbre : in Arbre;
			idArc  : in positive) return natural is
dist	: Float := 0.0;
begin
  for i in 1..points_arc(LArbre, idArc).all'last-1 loop
    dist := dist +
	Geometrie.Norme(points_arc(LArbre, idArc)(i),
			points_arc(LArbre, idArc)(i+1));
  end loop;
  return natural(dist);
end;

--------------------------------
-- Fonctions pour l'utilisateur-
--------------------------------

-- donne un pointeur sur le tableau des points de l'arc
function points_arc(	lArbre : in Arbre;
			idArc  : in positive) return point_access_type is
begin
  return lArbre.arcs(idArc).tabPoints;
end;

-- donne les deux noeuds composant l'arc
procedure noeuds_arc(	lArbre : in Arbre;
			idArc  : in positive;
			noeud_ini, noeud_fin : out positive) is
begin
  noeud_ini := lArbre.arcs(idArc).noeud_ini;
  noeud_fin := lArbre.arcs(idArc).noeud_fin;
end;

-- donne le tableau des 3 identificateurs d'arcs du noeud
-- un identificateur a 0 = absence d'arc, mais il peut etre suivi d'un non nul
function arcs_du_noeud(	lArbre : in Arbre;
			idNoeud: in Positive) return liens_array_type is
begin
  return(lArbre.noeuds(idNoeud).arcs);
end;

-- donne l'arc entre les noeuds ou 0 s'il n'existe pas
function arc_entre_noeuds(lArbre	: in Arbre;
			  noeudIni	: in positive;
			  noeudFin	: in positive) return natural is
arcsTab: Liens_array_type(1..3);
Ini, Fin: positive;
begin
  arcsTab := arcs_du_noeud(lArbre, noeudIni);
  for i in arcsTab'RANGE loop
    if arcsTab(i)/=0 then
      noeuds_arc(lArbre, arcsTab(i), Ini, Fin);
      if (Ini=noeudFin) or (Fin=noeudFin) then
	return arcsTab(i);
      end if;
    end if;
  end loop;
  return 0;
end;

-- donne un pointeur sur le tableau des points ordonnes du chemin
-- un chemin est une suite de noeuds voisins
function points_chemin( lArbre : in Arbre;
			chemin : in liens_access_type) return point_access_type is
type pointsChemin is array(natural range <>) of point_access_type;
pointsArc	: pointsChemin(1..chemin.all'length-1);
arc		: positive := 1;
nbPoints, ofs, lg	: natural := 1;
rep		: point_access_type;
derPt		: point_type;
begin
  for i in pointsArc'range loop
    arc := arc_entre_noeuds(lArbre, chemin(i), chemin(i+1));
    pointsArc(i) := points_arc(lArbre, arc);
    nbPoints := nbPoints + pointsArc(i).all'length -1;
  end loop;
  rep := new point_liste_type(1..nbPoints);
  lg := pointsArc'length;
  for i in integer(1)..lg-1 loop
    if pointsArc(i)(1)=pointsArc(i+1)(1) or
	pointsArc(i)(1)=pointsArc(i+1)(pointsArc(i+1).all'length) then
      for j in reverse 2..pointsArc(i).all'length loop
	rep(ofs) := pointsArc(i)(j);
	ofs := ofs + 1;
      end loop;
      derPt := pointsArc(i)(1);
    else
      if pointsArc(i)(pointsArc(i).all'length)=pointsArc(i+1)(1) or
	  pointsArc(i)(pointsArc(i).all'length)=
	  pointsArc(i+1)(pointsArc(i+1).all'length) then
	rep(ofs..ofs+pointsArc(i).all'length-2) :=
	  pointsArc(i)(1..pointsArc(i).all'length-1);
	ofs := ofs + pointsArc(i).all'length - 1;
	derPt := pointsArc(i)(pointsArc(i).all'length);
      end if;
    end if;
  end loop;
  if lg>1 then
    if pointsArc(lg)(1)/=derPt then
      for j in reverse 1..pointsArc(lg).all'length loop
	rep(ofs) := pointsArc(lg)(j);
	ofs := ofs + 1;
      end loop;
    else
      rep(ofs..ofs+pointsArc(lg).all'length-1):=pointsArc(lg).all;
    end if;
  else
    rep(ofs..ofs+pointsArc(1).all'length-1):=pointsArc(1).all;
  end if;
  return rep;
end;

-- donne un pointeur sur le tableau des identificateurs des extremites
function extrait_extremites(lArbre: in Arbre) return liens_access_type is
repExt : liens_access_type := new liens_array_type(1..lArbre.nbExtremites);
cpt : natural := 0;
begin
  if lArbre.nbExtremites=0 then
    return null;
  end if;

  for i in 1..lArbre.extremites.all'length loop
    if lArbre.extremites(i)/=0 then
      cpt := cpt + 1;
      repExt(cpt) := lArbre.extremites(i);
    end if;
  end loop;

  return repExt;
end;

-- Diverses Creations..

-- creation d'un noeud independant
procedure creer_noeud(	lArbre: in out Arbre;
			idNoeud : out positive) is
tabNoeuds : a_Noeuds;
idn, tailleTab : natural;
begin
  lArbre.nbNoeuds := lArbre.nbNoeuds + 1;
  if lArbre.nbNoeuds/=1 then
    tailleTab := lArbre.noeuds.all'length;
    tabNoeuds := new tab_noeuds(1..tailleTab);
    if lArbre.nbNoeuds>tailleTab then
      tabNoeuds(1..tailleTab) := lArbre.noeuds(1..tailleTab);
      Libere_tab_noeuds(lArbre.noeuds);
      lArbre.noeuds := new tab_noeuds(1..tailleTab*2);
      lArbre.noeuds(1..tailleTab) := tabNoeuds(1..tailleTab);
    end if;
  else
    lArbre.noeuds := new tab_noeuds(1..NB_NOEUDS_INI);
  end if;
  
  idn := place_noeud_libre(lArbre);
  lArbre.noeuds(idn) := new noeud;
  lArbre.noeuds(idn).arcs := (1..3 => 0);
  idNoeud := idn;
end;

-- donne la taille actuelle du tableau contenant les arcs du graphe
function nb_max_arcs(	lArbre : in Arbre) return positive is
begin
  return lArbre.arcs.all'length;
end;

-- creation d'un arc entre deux noeuds
procedure creer_arc(	lArbre : in out Arbre;
			noeudIni, noeudFin : in positive;
			tabPoints : in point_access_type;
			idArc : out positive) is
tabArcs : a_arcs;
ida, tailleTab : natural;
begin
  lArbre.nbArcs := lArbre.nbArcs + 1;
  if lArbre.nbArcs/=1 then
    tailleTab := nb_max_arcs(lArbre);
    if lArbre.nbNoeuds>tailleTab then
      tabArcs := new tab_arcs(1..tailleTab);
      tabArcs(1..tailleTab) := lArbre.arcs(1..tailleTab);
      Libere_tab_arcs(lArbre.arcs);
      lArbre.arcs := new tab_arcs(1..tailleTab*2);
      lArbre.arcs(1..tailleTab) := tabArcs(1..tailleTab);
    end if;
  else
    lArbre.arcs := new tab_arcs(1..NB_ARCS_INI);
  end if;

  ida := place_arc_libre(lArbre);
  ajoute_arc_a_noeud(lArbre, ida, noeudIni);
  ajoute_arc_a_noeud(lArbre, ida, noeudFin);

  lArbre.arcs(ida) := new arc;
  lArbre.arcs(ida).noeud_Ini := noeudIni;
  lArbre.arcs(ida).noeud_Fin := noeudFin;
  lArbre.arcs(ida).tabPoints := tabPoints;
  lArbre.arcs(ida).distance := calcul_lg_arc(lArbre, ida);
  idArc := ida;
end;

-- mets a jour les points constituant un arc
procedure set_points_arc(lArbre : in Arbre;
			idArc  : in positive;
			tabPoints: in point_access_type) is
begin
  if lArbre.arcs(idArc).tabPoints/=null then
    GR_Free_point_liste(lArbre.arcs(idArc).tabPoints);
  end if;
  lArbre.arcs(idArc).tabPoints := tabPoints;
  lArbre.arcs(idArc).distance := calcul_lg_arc(lArbre, idArc);
end;

-- Detruis le noeud et ses arcs
procedure detruire_noeud(lArbre : in out Arbre;
			idNoeud : in Positive) is
lesArcs : liens_array_type(1..3);
begin
  if lArbre.noeuds(idNoeud)/=null then
    lArbre.nbNoeuds := lArbre.nbNoeuds - 1;
    lesArcs := lArbre.noeuds(idNoeud).arcs;
    for i in lesArcs'range loop
      if lesArcs(i)/=0 then
        detruire_arc(lArbre, lesArcs(i));
      end if;
    end loop;
    Libere_noeud(lArbre.noeuds(idNoeud));
    lArbre.noeuds(idNoeud) := null;
  end if;
end;

-- Detruis tout les noeuds et arcs de l'arbre
procedure vide_arbre( lArbre : in out Arbre) is
begin
  if lArbre.nbNoeuds/=0 then
    for i in 1..lArbre.noeuds.all'length loop
      detruire_noeud(lArbre, i);
    end loop;
    GR_Free_liens_array(lArbre.extremites);
    Libere_tab_noeuds(lArbre.noeuds);
    Libere_tab_arcs(lArbre.arcs);
  end if;
end;

-- procedure testee (reseau hydrographique) mais non utilisee dans mon stage.
-- sert a la traduction d'un(e partie de) graphe de gen_io en un arbre utilise ici.
-- noeudebut indique un noeud du graphe appartenant a la composante connexe
-- que l'on veut traduire.
-- (!) on suppose qu'il y a au maximum 3 arcs par noeuds
procedure graphe2arbre( graphe : in graphe_type;
			lArbre : in out Arbre;
			noeudebut : in positive := 1) is
max_pts_arc	: natural := GR_INFOS(graphe).max_pts_arc;
-- on prevoit au maximum 100% d'extension
arcsLibres	: array(1..GR_INFOS(graphe).nb_arcs*2) of boolean;
idNArbre, idNoeud: positive := 1;
uNoeud		: noeud_type;
procedure Traduis_arc(idNoeud : in positive;
		      idNArbre: in positive) is
arcsTab		: Liens_array_type(1..GR_NOEUD(graphe, idNoeud).Nombre_arc);
idNArbresuiv, idArc, noeudSuiv	: positive;
arc		: arc_type;
points 		: Point_liste_type(1..max_pts_arc);
nbPoints	: natural;
ptPoints	: point_access_type;
begin
  arcsTab := GR_ARCS_DU_NOEUD(graphe, idNoeud);
  if arcsTab'length<4 then
    for i in arcsTab'RANGE loop
      if (arcsTab(i)<0) then
        arcsTab(i) := -arcsTab(i);
      end if;
      if arcsLibres(arcsTab(i)) then
        arcsLibres(arcsTab(i)) := false;
        GR_POINTS_D_ARC(graphe, arcsTab(i), points, nbPoints);
        arc := GR_ARC(graphe, arcsTab(i));
        if arc.noeud_ini=idNoeud then
          noeudSuiv := arc.noeud_fin;
        else
          noeudSuiv := arc.noeud_ini;
        end if;
	creer_noeud(lArbre, idNArbresuiv);
	ptPoints := new point_liste_type(1..nbPoints);
	ptPoints(1..nbPoints) := points(1..nbPoints);
	creer_arc(lArbre, idNArbre, idNArbresuiv, ptPoints, idArc);
        Traduis_arc(noeudSuiv, idNArbresuiv);
      end if;
    end loop;
  else
    put("Erreur: un arbre a au plus 3 arcs par noeud");
  end if;
end;

begin
  for i in  arcsLibres'RANGE loop
    arcsLibres(i) := true;
  end loop;
  idNoeud := noeudebut;
  if idNoeud=1 then
    loop
      uNoeud := GR_NOEUD(graphe, idNoeud);
      if uNoeud.nombre_arc/=0 then exit; end if;
      idNoeud := idNoeud + 1;
    end loop;
  end if;
  creer_noeud(lArbre, idNArbre);
  Traduis_arc(idNoeud, idNArbre);
end;

-- Ajoute au graphe un ensemble de noeuds et d'arcs representant l'arbre
-- noeudRef donne un noeud du graphe appartenant a cette extension
-- (!) ne pas depasser les LIMITES FIXES de la taille du graphe (qui est - souple)
procedure arbre2graphe (lArbre		: in arbre;
			graphe		: in out graphe_type;
			noeudRef	: out positive) is
idNoeuds	: liens_array_type(1..lArbre.noeuds.all'length);
arcsTab		: liens_array_type(1..3);
noeudG, ndIni, ndFin, arc: positive;
points		: point_access_type;
ptNoeud		: point_type;
begin
  for i in 1..lArbre.noeuds.all'length loop
    if lArbre.noeuds(i)/=null then
      arcsTab := arcs_du_noeud(lArbre, i);
      for j in arcsTab'range loop
	if arcsTab(j)/=0 then
	  noeuds_arc(lArbre, arcsTab(j), ndIni, ndFin);
	  points := points_arc(lArbre, arcsTab(j));
	  if ndIni=i then
	    ptNoeud := points(1);
	  else
	    ptNoeud := points(points.all'length);
	  end if;
	  exit;
	end if;
      end loop;
      GR_CREER_NOEUD_SUR_RIEN(graphe, ptNoeud, false, 0.0, 0, noeudG);
      idNoeuds(i) := noeudG;
    end if;
  end loop;
  for i in 1..lArbre.arcs.all'length loop
    if lArbre.arcs(i)/=null then
      noeuds_arc(lArbre, i, ndIni, ndFin);
      points := points_arc(lArbre, i);
      
      
      GR_CREER_ARC(graphe, idNoeuds(ndIni), idNoeuds(ndFin), points.all, points.all'length, Ligne, 0, arc);
    end if;
  end loop;
  noeudRef := noeudG;
end;

-- Copie de l'arbre (du contenu pointe, pas des pointeurs!)
procedure copie_arbre(Arba : in Arbre;
		      Arbb : in out Arbre) is
tailleTab : natural;
begin
  vide_arbre(Arbb);
  Arbb.nbExtremites := Arba.nbExtremites;
  tailleTab := Arba.extremites.all'length;
  Arbb.extremites := new liens_array_type(1..tailleTab);
  Arbb.extremites(1..tailleTab) := Arba.extremites(1..tailleTab);

  Arbb.nbNoeuds := Arba.nbNoeuds;
  tailleTab := Arba.noeuds.all'length;
  Arbb.noeuds := new tab_noeuds(1..tailleTab);
  for i in 1..tailleTab loop
    if Arba.noeuds(i)=null then
      Arbb.noeuds(i) := null;
    else
      Arbb.noeuds(i) := new noeud;
      Arbb.noeuds(i).arcs(1..3) := Arba.noeuds(i).arcs(1..3);
    end if;
  end loop;

  Arbb.nbArcs := Arba.nbArcs;
  tailleTab := Arba.arcs.all'length;
  Arbb.arcs := new tab_arcs(1..tailleTab);
  for i in 1..tailleTab loop
    if Arba.arcs(i)=null then
      Arbb.arcs(i) := null;
    else
      Arbb.arcs(i) := new arc;
      Arbb.arcs(i).noeud_Ini := Arba.arcs(i).noeud_Ini;
      Arbb.arcs(i).noeud_Fin := Arba.arcs(i).noeud_Fin;
      tailleTab := Arba.arcs(i).tabPoints.all'length;
      Arbb.arcs(i).tabPoints := new point_liste_type(1..tailleTab);
      Arbb.arcs(i).tabPoints(1..tailleTab) :=
	Arba.arcs(i).tabPoints(1..tailleTab);
      Arbb.arcs(i).distance := Arba.arcs(i).distance;
    end if;
  end loop;
end;

-- methodes utiles par exemple pour la memorisation du parcours de l'arbre
procedure init_arcs_pris(lArbre : in out Arbre) is
begin
  for i in 1..nb_max_arcs(lArbre) loop
    if lArbre.arcs(i)/=null then
      lArbre.arcs(i).flag := false;
    end if;
  end loop;
end;

-- sauve les reservations dans le tableau Resas
procedure sauve_resas(lArbre: in Arbre;
		      rep:    out Resas) is
begin
  for i in 1..nb_max_arcs(lArbre) loop
    if lArbre.arcs(i)/=null then
      rep(i) := lArbre.arcs(i).flag;
    end if;
  end loop;
end;

-- charge les reservations venant du tableau Resas
procedure charge_resas( lArbre: in out Arbre;
			flags:  in Resas) is
begin
  for i in 1..nb_max_arcs(lArbre) loop
    if lArbre.arcs(i)/=null then
      lArbre.arcs(i).flag := flags(i);
    end if;
  end loop;
end;

-- reserve l'arc
procedure prends_arc(	lArbre : in out Arbre;
			idArc : in positive) is
begin
  lArbre.arcs(idArc).flag := true;
end;

-- etat de la reservation de l'arc
function arc_pris(	lArbre : in Arbre;
			idArc  : in natural) return boolean is
begin
  if idArc=0 then
    return true;
  else
    return lArbre.arcs(idArc).flag;
  end if;
end;

-- longueur de l'arc
function longueur_arc(lArbre : in Arbre;
		      idArc  : in positive) return natural is
begin
  return lArbre.arcs(idArc).distance;
end;

-- Distance entre deux noeuds voisins
function distance_elementaire( lArbre : in Arbre;
				idNa   : in positive;
				idNb   : in positive) return natural is
arc	: natural;
begin
  arc :=  arc_entre_noeuds(lArbre, idNa, idNb);
  if arc/=0 then
    return lArbre.arcs(arc).distance;
  else
    return integer'last;
  end if;
end;

-- distance entre deux noeuds pas forcement voisins
procedure distance (	lArbre : in out Arbre;
			idNa   : in positive;
			idNb   : in positive;
			dist   : out natural;
			etapes : out liens_access_type) is -- noeuds du chemin
rep : liens_array_type(1..lArbre.nbNoeuds);
nivTop	: positive := 1;
disTmp	: natural := 0;
repEtap : liens_access_type;
contexte: Resas(1..nb_max_arcs(lArbre));

function chemin( idNeu : in positive;
		  nivo  : in positive) return boolean is
noeudIni, noeudFin	: positive;
arcsTab: Liens_array_type(1..3);
begin
  rep(nivo) := idNeu;
  arcsTab := arcs_du_noeud(lArbre, idNeu);
  for i in arcsTab'RANGE loop
    if arcsTab(i)/=0 and arc_pris(lArbre, arcsTab(i))=false then
      noeuds_arc(lArbre, arcsTab(i), noeudIni, noeudFin);
      prends_arc(lArbre, arcsTab(i));
      if noeudIni/=idNeu then
	noeudFin := noeudIni;
	noeudIni := idNeu;
      end if;
      if noeudFin=idNb then
	nivTop := nivo+1;
	rep(nivTop) := idNb;
	return true;
      else
        if chemin(noeudFin, nivo+1)=true then
	  return true;
	end if;
      end if;
    end if;
  end loop;
  return false;
end;

begin
  sauve_resas(lArbre, contexte);
  init_arcs_pris(lArbre);
  if chemin(idNa, 1)=true then
    for i in 1..nivTop-1 loop
      disTmp := disTmp + distance_elementaire(lArbre, rep(i), rep(i+1));
    end loop;
  end if;
  dist := disTmp;
  repEtap := new liens_array_type(1..nivTop);
  repEtap(1..nivTop) := rep(1..nivTop);
  etapes := repEtap;
  charge_resas(lArbre, contexte);
end;

-- l'ecartement d'un noeud x est max(des noeuds y) de d(x,y)
procedure ecartement(	lArbre : in out Arbre;
			idNoeud: in positive;
			noeudMax: out positive;  -- noeud produisant le max
			distants: out natural; -- la distances entre eux
			chemin  : out liens_access_type) is -- noeuds du chemin
rep, dist, idN: natural := 0;
etapes, memEtaps : liens_access_type;
begin
  for i in 1..lArbre.extremites.all'length loop
    idN := lArbre.extremites(i);
    if idN/=0 then
      distance(lArbre, idNoeud, idN, dist, etapes);
      if dist>rep then
	if memEtaps/=null then
	  GR_Free_liens_array(memEtaps);
	end if;
	memEtaps := etapes;
	noeudMax := idN;
	rep := dist;
      else
	GR_Free_liens_array(etapes);
      end if;
    end if;
  end loop;
  distants := rep;
  chemin := memEtaps;
end;

-- Epure le squelette en supprimmant les extremites trop petites et,
-- en option, les points proches du contour
procedure epure(	lArbre : in out Arbre;
			dNoeud : in float; -- distance minimale inter noeuds
			dSurf  : in float := 0.0; -- distance minimale au contour
			ptSurf : in point_liste_type := point_liste_vide) is
fini : boolean;
pas: integer := 1;
idN, tailleTab, lgArc, dEcar, premPt, derPt, pt : natural;
arcsTab: Liens_array_type(1..3);
noeudIni, noeudFin, noeudMax: positive := 1;
ptsArc, tabTmp	: point_access_type;
lstNoeuds	: liens_access_type;
ptmp : point_type;
contexte: Resas(1..nb_max_arcs(lArbre));

begin
  put("* EPURATION *"); new_line;
  sauve_resas(lArbre, contexte);
  init_arcs_pris(lArbre);
  loop
    fini := true;
    EXTREMITES:
    for i in 1..lArbre.extremites.all'length loop
      idN := lArbre.extremites(i);
      if idN/=0 then
	-- On consulte les arcs de chacune des extremites
	arcsTab := arcs_du_noeud(lArbre, idN);
	ARCS:
	for j in arcsTab'RANGE loop
          if arcsTab(j)/=0 and arc_pris(lArbre, arcsTab(j))=false then
	    prends_arc(lArbre, arcsTab(j));
	    -- Si l'arc est trop court on le supprimme et on recommence tout
	    lgArc := longueur_arc(lArbre, arcsTab(j));
	    if lgArc<natural(dNoeud) then
	      detruire_noeud(lArbre, idN);
	      put("Arc trop petit");int_io.put(idN);new_line;
	      fini := false;
	      exit EXTREMITES;
	    end if;
	    if dSurf/=0.0 then
	      noeuds_arc(lArbre, arcsTab(j), noeudIni, noeudFin);
	      ptsArc := points_arc(lArbre, arcsTab(j));
	      tailleTab := ptsArc.all'length;
	      if noeudIni/=idN then
	        premPt := 1;
	        derPt := tailleTab+1;
	        pas := 1;
	        ecartement(lArbre, noeudIni, noeudMax, dEcar, lstNoeuds);
	      else
	        premPt := tailleTab;
	        derPt := 0;
	        pas := -1;
	        ecartement(lArbre, noeudFin, noeudMax, dEcar, lstNoeuds);
	      end if;
	      -- Les operations suivantes ne se font que si l'arc peut etre traite
	      if noeudMax/=idN then
	        -- Si, dans l'arc, le noeud oppose a celui de l'extremite est trop
	        -- proche du contour on supprimme tout l'arc et on recommence tout
	        if Geometrie.Distance_a_polylignes(
		  ptsArc(premPt), ptSurf,
		  ptSurf'length, c_segment_type) < dSurf then
	          put("d(noeud_avant_extremite, surface) trop petite");
	          int_io.put(idN);new_line;
		  detruire_noeud(lArbre, idN);
	          fini := false;
	          exit EXTREMITES;
	        end if;
	        pt := premPt;
	        while Geometrie.Distance_a_polylignes(ptsArc(pt),
	  	  ptSurf, ptSurf'length, c_segment_type) > dSurf
	        loop
	          pt := pt + pas;
		  if pt=derPt then exit; end if;
	        end loop;
	        -- On raccourci le bout de l'arc trop proche du contour
	        if pt/=derPt then
	          put("Arc raccourci");int_io.put(idN);new_line;
	          if noeudIni=idN then
	  	    tabTmp := new point_liste_type(1..tailleTab-pt);
		    tabTmp(1..tailleTab-pt) := ptsArc(pt+1..tailleTab);
	          else
		    tabTmp := new point_liste_type(1..pt-1);
		    tabTmp(1..pt-1) := ptsArc(1..pt-1);
	          end if;
	          set_points_arc(lArbre, arcsTab(j), tabTmp);
	          -- Si cet arc racourci est trop court on le supprimme
	          if longueur_arc(lArbre, arcsTab(j))<natural(dNoeud) then
		    detruire_noeud(lArbre, idN);
	            put("Arc devenu trop petit");int_io.put(idN);new_line;
	            fini := false;
	            exit EXTREMITES;
	          end if;
	        end if; -- de pt/=derPt
	      end if; -- de noeudMax/=idN
	      exit ARCS;
            end if; -- de dSurf/=0
	  end if; -- de arcsTab(j)/=0 and arc_pris(lArbre, arcsTab(j))=false
	end loop ARCS;
      end if; -- de idN/=0
    end loop EXTREMITES;
    exit when fini;
  end loop;
  charge_resas(lArbre, contexte);
end;

end PArbre;
