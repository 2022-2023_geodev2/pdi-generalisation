--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with math_int_basic; use math_int_basic;
with ada.strings.unbounded; use ada.strings.unbounded;
with gb;

package body detpoi is
  
--=========================================================================
-- Fonction determinant le poids de direction d'une �criture
------------------------------------------------------------
function POIDIR(dir:in typdir;poi:in typpoidir) return typpoi is
begin
  case dir is
	when ne => return poi.ne;
	when se => return poi.se;
	when no => return poi.no;
	when so => return poi.so;
	when n  => return poi.n;
	when s  => return poi.s;
	when e  => return poi.e;
	when o  => return poi.o;
    when int=> return poi.int;
    when zi => return poi.zi;
    when zm => return poi.zm;
    when ze => return poi.ze;
  end case;
end poidir;

--===============================================================
-- Procedure qui determine le poids de mutilation de la boite 
-- et renvoie un flag dans le cas ou 1 pixel interdit est ecras�:
-----------------------------------------------------------------
Procedure POIMUB(tpoi   :in typtipoi; -- interpretation de l'image de mut sur la zone d'influence
                 origine:in point_type; -- origine de la boite dans tpoi
                 npx,npy:in positive;  -- nb de col et lig de tpoi occup�es par la boite 
                 ecrase :out boolean; -- flag pixel interdit
                 poimux :out typpoi) is  -- poids de mutilation
                   
  poipix:typpoi:=0.0;
  poim_temp:typpoi:=0.0;
  npix:natural;  
  ErrSub : boolean:=false;
begin
  ecrase:=false;
  npix:=npx*npy; -- nb de pixels occup�s par la boite
  for ypix in origine.coor_y+1..origine.coor_y+npy loop -- col
    for xpix in origine.coor_x+1..origine.coor_x+npx loop -- lig

      if tpoi(xpix,ypix)=255 then
	    Errsub:=true;
      end if;
      if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
        poimux:=10.0;
        ecrase:=true;
        return;
      else
        poipix:=float(tpoi(xpix,ypix))/float(npix);
        poim_temp:=poim_temp+poipix;
      end if;
    end loop;
  end loop;
  poimux:=poim_temp;
  exception
     when  Event : others => 
     if Errsub=true then
	   Msg_Erreur:=To_Unbounded_String("L'interpr�tation de certains indexes de l'image de mutilation"
	   &eol&"est manquante dans le fichier de substitution");
     end if;
     raise;
end poimub;


--=============================================================================
-- Fonction determinant le poids de distance d'une �criture
---------------------------------------------------------------------------
function POIDNC(dncmax:in natural;dnc:in natural) return typpoi is
  poid:float;
begin
  poid:=10.0*float(dnc)/float(dncmax);
  if poid<10.0 then
    return poid;
  else
    return 10.0;
  end if;
end poidnc ;

--=========================================================================
end detpoi;
