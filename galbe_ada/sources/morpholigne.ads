with gen_IO;use gen_IO;
with Unchecked_deallocation;


Package morpholigne is




--------------------------------------------------------------------------
--            TYPES ET CONSTANTES INTERNES AU PACKAGE                   --
--------------------------------------------------------------------------

-- Les types associes aux segments
----------------------------------
-- Dans ce Package, on appelle segment un arc represente soit par
-- un segment de droite, Soit par un arc de cercle et un Poly_segment
-- un ensemble de segments

type Interpolation_type is (Cercle,Ligne);

type Segment is record
  Deb, Fin, Centre : Point_type_reel;
  Rayon, Angle : float;
  Interpolation : interpolation_type;
end record;
-- les donnees sont redondantes mais utiles pour minimiser les erreurs de
-- calcul
-- Deb [resp. fin]: le premier [resp.dernier] point du segment, le segment
--                  est oriente de Deb vers Fin
-- Centre : si le segment est un arc de cercle, le centre de ce cercle
-- Rayon : si le segment est un arc de cercle, le rayon de ce cercle
-- Angle : si le segment est un arc de cercle, l'angle (Deb, Centre, Fin)
--         cet angle est dans [-2Pi,2Pi], positif si le segment est parcouru
--         dans le sens trigonometrique, negatif sinon
-- Interpolation : pour savoir si c'est un arc de cercle ou un segment de droite


type Poly_segment is array (natural range <>) of Segment;

type Poly_segment_access is access Poly_segment;

Type Intersection is record
  Seg : Segment;
  Cardinal : Natural;
end record;
-- Cardinal = 0 => intersection vide
-- Cardinal = 1 => intersection reduite a un point
-- Cardinal = 2 => intersection reduite a deux points
-- Cardinal = 3 => intersection egale a un arc de cercle ou un segment de
--                 droite


-- Un type pour regrouper un arc, la demi-largeur du symbole, LES arcs
-- representant les bords a droite et a gauche et leur parente
type Arc_bords is record
  Arc       : Point_access_reel;
  Largeur   : float;
  Bordg     : Point_access_reel;
  Parentg   : Liens_access_type;
  Bordd     : Point_access_reel;
  Parentd   : Liens_access_type;
end record;


Pt_infini : constant Point_type_reel := (float'last,float'last);
Seg_vide : constant Segment := (Pt_infini,Pt_infini,Pt_infini,
                                float'last,float'last,Ligne);







Erreur : constant float := 0.03;
-- une estimation de l'imprecision des calculs dans le package
-- si on diminue erreur, on diminue la precision du resultat
-- si on augmente erreur, on risque d'ajouter des petites vibrations parasites

Precision_defaut : constant float := 0.1;
-- dans l'unite de l'arc, decalage planimetrique maxi par rapport a la
-- position reelle de la ligne dilatee (approximation des arcs de cercle)
-- A titre d'exemple: 0,1 correspond sur un cercle de rayon 10 a un
-- point place tout les 16 degres d'arc.
-- Attention, ceci n'a rien a voir avec la precision des calculs dans l'algo
-- mais avec la precision de l'interpolation d'un cercle par des segments

--------------------------------------------------------------------
-- Dans les procedures de dilatation et fermetures qui suivent:
--
-- Arc: l'arc en entree
--
-- D : la distance de decalage pour une dilatation
--
-- D1 et D2 : les distance de dilatation et d'erosion pour une fermeture
--
-- Arcg ou Arcd : les arcs decales en sortie. Si un des arcs n'existe pas
-- le pointeur Null est renvoye
--
-- Parent: le segment de Arc ayant cree un segment de arcd 
--         [Arcd(i),Arcd(i+1)] est le segment [Arc(Parent(i)),Arc(Parent(i)+1)]
--
-- Precision : l'erreur planimetrique maxi entre la vraie dilatation
--             mathematique et l'approximation par la polyligne
--             Ceci n'a rien a voir avec les erreurs de calculs mais
--             avec la precision souhaitee de l'approximation d'un cercle 
--             par une polyligne
--------------------------------------------------------------------


--------------------------------------------------------------------
-- LES PROCEDURES SUIVANTES "RAPIDES" NE CALCULENT LA DILATATION QUE D'UN
-- SEUL COTE DE L'ARC, LEUR PROBLEME EST DE NE PAS ASSURER DANS TOUS LES
-- CAS DE FIGURE UNE BONNE GESTION DES EXTREMITES
--------------------------------------------------------------------

Procedure Dilatation_gauche( Arc       : in  Point_liste_reel;
                             D         : in  float;
                             Arcg      : out Point_access_reel;
                             Parent    : out Liens_access_type;
                             Arrondi   : in  boolean := true;
                             Precision : in  float := Precision_defaut);  

Procedure Fermeture_gauche( Arc       : in  Point_liste_reel;
                            D1        : in  float;
                            D2        : in  float;
                            Arcg      : out Point_access_reel;
                            Parent    : out Liens_access_type;
                            Arrondi   : in  boolean := true;
                            Precision : in  float := Precision_defaut);  

Procedure Dilatation_droite( Arc       : in  Point_liste_reel;
                             D         : in  float;
                             Arcd      : out Point_access_reel;
                             Parent    : out Liens_access_type;
                             Arrondi   : in  boolean := true;
                             Precision : in  float := Precision_defaut);  

Procedure Fermeture_droite( Arc       : in  Point_liste_reel;
                            D1        : in  float;
                            D2        : in  float;
                            Arcd      : out Point_access_reel;
                            Parent    : out Liens_access_type;
                            Arrondi   : in  boolean := true;
                            Precision : in  float := Precision_defaut);  

---------------------------------------------------------------
-- LES PROCEDURES SUIVANTES PERMETTENT DANS TOUS LES CAS DE FIGURE
-- UNE BONNE GESTION DES EXTREMITES DE L'ARC
----------------------------------------------------------------

-- Pour renvoyer un seul arc pour la dilatation/fermeture a gauche et a droite:
----------------------------------
Procedure Dilatation_arc( Arc       : in  Point_liste_reel;
                          D         : in  float;
                          Arcd      : out Point_access_reel;
                          Parent    : out Liens_access_type;
                          Precision : in  float := Precision_defaut);  

Procedure Fermeture_arc( Arc       : in  Point_liste_reel;
                         D1        : in  float;
                         D2        : in  float;
                         Arcf      : out Point_access_reel;
                         Parent    : out Liens_access_type;
                         Precision : in  float := Precision_defaut);

-- Pour renvoyer deux arcs pour la dilatation/fermeture a gauche et a droite:
----------------------------------
Procedure Dilatation_arc( Arc       : in  Point_liste_reel;
                          D         : in  float;
                          Arcg      : out Point_access_reel;
                          Parentg   : out Liens_access_type;
                          Arcd      : out Point_access_reel;
                          Parentd   : out Liens_access_type;
                          Precision : in  float := Precision_defaut);  

Procedure Fermeture_arc( Arc       : in  Point_liste_reel;
                         D1        : in  float;
                         D2        : in  float;
                         Arcg      : out Point_access_reel;
                         Parentg   : out Liens_access_type;
                         Arcd      : out Point_access_reel;
                         Parentd   : out Liens_access_type;
                         Precision : in  float := Precision_defaut);


---------------------       	
-- CONFLIT_SYMBO :
-- PARTIE SUR LA RECHERCHE ET LA CARACTERISATION DES CONFLITS
-- CARTOGRAPHIQUES DUS A LA SYMBOLISATION D'UNE LIGNE
--
---------------------
-- Sebastien Mustiere
-- Octobre 97
---------------------

-- LES TYPES ASSOCIES AUX CONFLITS

type Type_cote is (Gauche,Droite);

-- Force_conflit caracterise l'importance de chaque type de conflit
type Force_conflit is array(Gauche..Droite) of float;

Conflit_nul : constant Force_conflit:=(0.0,0.0);

type Liste_flag is array(natural range<>) of boolean;
type Access_flag is access Liste_flag;
Procedure Free_flag is new unchecked_deallocation(Liste_flag,Access_flag);

-- LES PROCEDURES ASSOCIEES AUX CONFLITS

-------------------------------------------------------------
-- CETTE FONCTION RENVOIE UN INDICATEUR DE L'IMPORTANCE DES
-- CONFLITS DANS L'ENSEMBLE D'UNE POLYLIGNE :
-- Longueur a droite et a gauche de conflit
------------------------------------------------------------
-- Arc : le point_liste_type a la bonne taille (par ex: mettre Tabini(1..Nbini)
-- Largeur : la largeur du symbole (brute ou modifiee pour tenir compte du type de symbole)
-- Tolerance : plus la tolerance est forte, moins de conflits sont detectes
------------------------------------------------------------
Function Indicateur_conflits(Arc          : Point_liste_type;
                             Largeur      : float;
                             Tolerance    : float:=1.7) 
			     return Force_conflit;

-------------------------------------------------------------
-- CETTE PROCEDURE RENVOIE LES POINTS DE DECOUPAGE DE LA POLYLIGNE
-- EN SOUS-ARCS HOMOGENES VIS A VIS DE LA PRESENCE DE CONFLITS
-------------------------------------------------------------
-- Arc : le point_liste_type a la bonne taille (par ex: mettre Tabini(1..Nbini)
-- Largeur : la largeur du symbole (brute ou modifiee pour tenir compte du type de symbole)
-- Separabilite : deux zones de conflits, separees par une zone sans conflit de longueur Largeur*Separabilite 
--                sont fusionnees
-- Tolerance : plus la tolerance est forte, moins de conflits sont detectes
-- En retour:  Si on decoupe l'arc en N sous-arc, alors decoupage est un tableau 
--             de N+1 elements, les numeros des points de decoupage
-------------------------------------------------------------


-----------------------------------------------------------------
-- 3 procédures adaptées des précédentes pour gérer le cas des trous 
-- dans le symbole; développé par Bruneau en mars 2001

Function Decoupage_conflits(Arc          : Point_liste_type;
                            Largeur      : float;
                            Separabilite : float := 1.7;
                            Tolerance    : float := 1.7) 
                            return Point_access_type;

Procedure Decoupage_conflits_trou(Arc    : Point_liste_type;
                            Largeur      : in float;
                            Separabilite : in float := 1.7;
                            Tolerance    : in float := 1.7;
                            Pts_dec      : out Point_access_type;
                            Conf3        : out Access_flag);

Procedure DECOUPAGE_CONFLITS_TROU_GD(GD : in type_cote;
                            Arc    : Point_liste_type;
                            Largeur      : in float ;
                            Separabilite : in float := 1.7;
                            Tolerance    : in float := 1.7;
                            Pts_dec2      : out Point_access_type;
                            Conf4        : out Access_flag);

Function Arc_proche_trou(TP1 : in Point_liste_type;
                         TP2 : in Point_liste_type;
                         ecart : in float := 0.05)
                         return boolean;


Procedure Cercle_to_ligne(PS        : in Poly_segment;
                          ParentS   : in Liens_array_type;
                          PL        : out Point_access_reel;
                          ParentL   : out Liens_access_type;
                          Precision : in float := Precision_defaut);



-------------------------------------------------------------

end morpholigne;
