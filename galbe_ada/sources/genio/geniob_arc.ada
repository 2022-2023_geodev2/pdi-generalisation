-- ===========================================================================
Separate(Gen_io)
Function  GR_ARC (graphe : Graphe_type; arc : positive ) return arc_type is
begin
   return Graphe.Tarc(arc);
end;


-- ===========================================================================
Separate(Gen_io)
Procedure  GR_POINTS_D_ARC (graphe : graphe_type;
                            arc : positive;
                            tab_points : out point_liste_type;
                            n : out natural) is

My_prov : Cubique_liste_type(1..graphe.info.max_cub_arc);

begin
   if graphe.tarc(arc).mode_met=Ligne then
      n:=graphe.tarc(arc).nombre_met;
      tab_points(1..graphe.tarc(arc).nombre_met):=Polyligne.LIRE(graphe,arc);
  else
      n:=graphe.tarc(arc).nombre_met;
      my_prov(1..graphe.tarc(arc).nombre_met):= Polycubique.LIRE(graphe,arc);
      -- ??????? Tranferer les cubiques en liste de points
  end if;
end;


-- ===========================================================================
Separate(Gen_io)
Procedure  GR_CUBIQUES_D_ARC (graphe : graphe_type;
                              arc : positive;
                              tab_cubiques : out cubique_liste_type;
                              n_cubiques : out natural) is

begin
  n_cubiques:= graphe.tarc(arc).nombre_met;
  tab_cubiques(1..graphe.tarc(arc).nombre_met):= Polycubique.LIRE(graphe,arc);
end;



-- =====================================================================
Separate(Gen_io) 
Procedure GR_ARCS_IN_RECTANGLE(graphe : graphe_type;
                               F_point : Point_type;
                               S_point : Point_type;
                               tab_arcs : out liens_array_type;
                               n : out natural) is

flags : array(1..graphe.info.nb_arcs) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_a,i_a : integer;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_a:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_a.all(ir+j).count-1 loop
            i_a:=graphe.rblk_a.all(graphe.radr_a.all(ir+j).first+k);
            if not flags(i_a) then
               flags(i_a):=true;
               if graphe.tarc(i_a).encombr.minimum.coor_x <= upper.coor_x and 
                  graphe.tarc(i_a).encombr.minimum.coor_y <= upper.coor_y and 
                  graphe.tarc(i_a).encombr.maximum.coor_x >= lower.coor_x and
                  graphe.tarc(i_a).encombr.maximum.coor_y >= lower.coor_y and
                  graphe.tarc(i_a).nombre_met/=0 then
                  n_a:=n_a+1;
                  tab_arcs(n_a):=i_a;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_a;
end;



-- =======================================================================
-- Modif. des points intermediaires d'un arc remplaces par n nouveaux pts.
-- =======================================================================
Separate(Gen_io) 
Procedure GR_MOD_POINTS_D_ARC(graphe:in out graphe_type;
                              arc:positive; 
                              tab_points:point_liste_type;
                              n:natural) is

A_lire,P_lire,First,K,reste,Min_x,Min_y,Max_x,Max_y : integer;

rx1,rx2,ry1,ry2,ir,Nbreg : integer;
flag_mod : boolean;
tab_points_prov : point_liste_type(1..n);
n_prov : integer;

begin

   -- Elimination des points doubles eventuels
   tab_points_prov(1):=tab_points(1);
   n_prov:=1;
   for i in 2..n loop
      if tab_points(i)/=tab_points(i-1) then 
         n_prov:=n_prov+1;
         tab_points_prov(n_prov):=tab_points(i);
      end if;
   end loop;


   if n_prov<=graphe.tarc(arc).nombre_met then
   --
   -- puisqu on diminue ou egale le nombre de points intermediaires, on peut
   -- reecrire dans les memes positions du buffer de point
   --
      Polyligne.ECRIRE(graphe,tab_points_prov,n_prov,graphe.tarc(arc).premier_met);

   else
   --
   -- puisqu on augmente le nombre de points intermediaires, on ne peut pas
   -- reecrire dans les memes positions du buffer de point; on reecrira en
   -- fin de fichier du buffer de point
   --
      Polyligne.ECRIRE_FIN(graphe,tab_points_prov,n_prov);
      graphe.tarc(arc).premier_met:=graphe.info.nb_points+1;
      graphe.info.nb_points:=graphe.info.nb_points+n;
      if n_prov>graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:=n_prov;
      end if;
   end if;
   graphe.tarc(arc).nombre_met:=n_prov;

   -- Mise a jour de l'encombrement de l arc.
   --
   min_x:=tab_points_prov(1).coor_x;
   max_x:=tab_points_prov(1).coor_x;
   min_y:=tab_points_prov(1).coor_y;
   max_y:=tab_points_prov(1).coor_y;
   for i in 2..n_prov loop
       Min_x:=Min(tab_points_prov(i).coor_x,Min_x);
       Max_x:=Max(tab_points_prov(i).coor_x,Max_x);
       Min_y:=Min(tab_points_prov(i).coor_y,Min_y);
       Max_y:=Max(tab_points_prov(i).coor_y,Max_y);
   end loop;
   if Min_x < graphe.tarc(arc).encombr.minimum.coor_x or
      Max_x > graphe.tarc(arc).encombr.maximum.coor_x or
      Min_y < graphe.tarc(arc).encombr.minimum.coor_y or
      Max_y > graphe.tarc(arc).encombr.maximum.coor_y then
      --  si graphe regionalise, il faudra peut-etre ajouter des arcs dans les
      --  regions avoisinantes
      flag_mod:=true;
   end if;
   graphe.tarc(arc).encombr.minimum.coor_x:=Min_x;
   graphe.tarc(arc).encombr.maximum.coor_x:=Max_x;
   graphe.tarc(arc).encombr.minimum.coor_y:=Min_y;
   graphe.tarc(arc).encombr.maximum.coor_y:=Max_y;

   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if flag_mod=true and graphe.pasx/=0 then
      rx1:=ceil(graphe.tarc(arc).encombr.minimum.coor_x,graphe.pasx);
      rx2:=ceil(graphe.tarc(arc).encombr.maximum.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tarc(arc).encombr.minimum.coor_y,graphe.pasy);
      ry2:=ceil(graphe.tarc(arc).encombr.maximum.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for j in ry1..ry2 loop
          for k in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_a,graphe.rblk_a,
                                         graphe.rfin_a,graphe.rvid_a,arc,ir);
              if graphe.tarc(arc).face_g/=0 then
                 AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                       graphe.rfin_f,graphe.rvid_f,graphe.tarc(arc).face_g,ir);
              end if;
              if graphe.tarc(arc).face_d/=0 then
                 AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                       graphe.rfin_f,graphe.rvid_f,graphe.tarc(arc).face_d,ir);
              end if;
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;




-- =====================================================================
Separate(Gen_io)
Procedure GR_CREER_ARC(graphe : in out graphe_type;
                       noeud_ini : positive;
                       noeud_fin : positive;
                       tab_points : point_liste_type;
                       n : positive;
                       mode : metrique_type;                       
                       att_graph : natural;
 					   OV : in short_short_integer;
                       arc : out positive;
					   Exit_sans_creer : out boolean) is


-- creation d un arc s appuyant sur 2 noeuds et dont on a la liste des 
-- points intermediares

Type tablo_float is array(integer range<>) of float;
my_noeud : noeud_type;
my_arc : arc_type;
rx1,rx2,ry1,ry2,ir,min_x,min_y,max_x,max_y : integer;
tab_points_prov : point_liste_type(1..n);
n_prov : integer;

liste_arcs: liens_array_type(1..graphe.info.max_arc_neu+3);
liste_points: point_liste_type(1..max(graphe.info.max_pts_arc,n));
liste_angles : tablo_float(1..graphe.info.max_arc_neu+2);
tab_cubiques : cubique_liste_type(1..n-1);
n_cub : positive;
nombre_pt : natural;
minarc : integer;
minangle : float;

Function ANGLE(pini,pfin : point_type) return float is 
-- Calcul de l arctangente de 2 points
ang,v1,v2 : float;
                      
begin
  if pini.coor_x/=pfin.coor_x then
     v1:=float(pfin.coor_y-pini.coor_y);
     v2:=float(pfin.coor_x-pini.coor_x);
     --ang:=ATAN2(V1,V2);
     ang:=ArcTAN(V1,V2);
  elsif pfin.coor_y>pini.coor_y then 
        ang:=3.14159/2.0;
     else
        ang:=-3.14159/2.0;
  end if;
  return ang;
end ANGLE;    



begin

   -- Elimination des points doubles eventuels
   tab_points_prov(1):=tab_points(1);
   n_prov:=1;
   for i in 2..n loop
      if tab_points(i)/=tab_points(i-1) then 
         n_prov:=n_prov+1;
         tab_points_prov(n_prov):=tab_points(i);
      end if;
   end loop;

   Exit_sans_creer:=false;
   if n_prov=1 then
     -- tous les points de l'arc etaient confondus
	 Exit_sans_creer:=true;
	 return;
   end if;

   -- initialisations faciles 
   graphe.info.nb_arcs:=graphe.info.nb_arcs + 1;
   arc:=graphe.info.nb_arcs;
   graphe.tarc(graphe.info.nb_arcs).noeud_ini:=noeud_ini;
   graphe.tarc(graphe.info.nb_arcs).noeud_fin:=noeud_fin;
   graphe.tarc(graphe.info.nb_arcs).face_g:=0;
   graphe.tarc(graphe.info.nb_arcs).face_d:=0;
   graphe.tarc(graphe.info.nb_arcs).mode_met:=mode;
   graphe.tarc(graphe.info.nb_arcs).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_l then
      graphe.info.nb_graph_l:=att_graph;
   end if;
   graphe.tarc(graphe.info.nb_arcs).Octet_vms:=oV;

   -- ecriture des points intermediaires en mode ligne ou cubique
   Polyligne.ECRIRE_FIN(graphe,tab_points_prov,n_prov);
   graphe.tarc(graphe.info.nb_arcs).mode_met:= ligne;
   graphe.tarc(graphe.info.nb_arcs).premier_met:=graphe.info.nb_points+1;
   graphe.tarc(graphe.info.nb_arcs).nombre_met:=n_prov;
   graphe.info.nb_points:=graphe.info.nb_points+n_prov;
   if n_prov>graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:=n_prov;
   end if;

   -- Mise a jour de l'encombrement de l arc.
   min_x:=tab_points_prov(1).coor_x;
   max_x:=tab_points_prov(1).coor_x;
   min_y:=tab_points_prov(1).coor_y;
   max_y:=tab_points_prov(1).coor_y;
   for i in 2..n_prov loop
       Min_x:=Min(tab_points_prov(i).coor_x,Min_x);
       Max_x:=Max(tab_points_prov(i).coor_x,Max_x);
       Min_y:=Min(tab_points_prov(i).coor_y,Min_y);
       Max_y:=Max(tab_points_prov(i).coor_y,Max_y);
   end loop;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x:=Min_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x:=Max_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y:=Min_y;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y:=Max_y;


   -- Mise a jour du lien avec le noeud initial 
   --
   my_noeud:=GR_NOEUD(graphe,noeud_ini);
   if my_noeud.nombre_arc/=0 then
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_ini);
   end if;

   -- calcul de l angle de chaque arc issu du noeud initial
   for i in 1..my_noeud.nombre_arc loop
       if liste_arcs(i)>0 then
          my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else
          my_arc:=GR_ARC(graphe,-1*liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-1*liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),
                                liste_points(nombre_pt-1));
       end if;
   end loop;

   -- ajout de l arc au noeud initial
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(1),tab_points(2));
   liste_arcs(my_noeud.nombre_arc+1):=graphe.info.nb_arcs;
   graphe.tnod(noeud_ini).nombre_arc:=graphe.tnod(noeud_ini).nombre_arc + 1;
   graphe.tnod(noeud_ini).premier_arc:=graphe.info.nb_liens+1;

   -- tri suivant l ordre croissant des angles
   for i in 1..graphe.tnod(noeud_ini).nombre_arc-1 loop
       for j in  i+1..graphe.tnod(noeud_ini).nombre_arc loop
           if liste_angles(j)<liste_angles(i) then
              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_ini).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_ini).nombre_arc loop
       if liste_arcs(i)>0 then
          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else
          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;

   -- ecriture dans le fichier des liens 
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_ini).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_ini).nombre_arc;
   if graphe.tnod(noeud_ini).nombre_arc>graphe.info.max_arc_neu then
      graphe.info.max_arc_neu:=graphe.tnod(noeud_ini).nombre_arc;
   end if;   


   -- Mise a jour du lien avec le noeud final 
   --
   my_noeud:=GR_NOEUD(graphe,noeud_fin);
   if my_noeud.nombre_arc/=0 then
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_fin);
   end if;

   -- calcul de l angle de chaque arc issu du noeud final
   for i in 1..my_noeud.nombre_arc loop
       if liste_arcs(i)>0 then
          my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else
          my_arc:=GR_ARC(graphe,-liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),
                                liste_points(nombre_pt-1));
       end if;
   end loop;

   -- ajout de l arc au noeud final
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(n_prov),
                                              tab_points(n_prov-1));
   liste_arcs(my_noeud.nombre_arc+1):=-graphe.info.nb_arcs;
   graphe.tnod(noeud_fin).nombre_arc:=graphe.tnod(noeud_fin).nombre_arc + 1;
   graphe.tnod(noeud_fin).premier_arc:=graphe.info.nb_liens+1;

   -- tri suivant l ordre croissant des anlges
   for i in 1..graphe.tnod(noeud_fin).nombre_arc-1 loop
       for j in  i+1..graphe.tnod(noeud_fin).nombre_arc loop
           if liste_angles(j)<liste_angles(i) then
              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_fin).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_fin).nombre_arc loop
       if liste_arcs(i)>0 then
          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else
          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;

   -- ecriture dans le fichier des liens 
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_fin).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_fin).nombre_arc;
   if graphe.tnod(noeud_fin).nombre_arc>graphe.info.max_arc_neu then
      graphe.info.max_arc_neu:=graphe.tnod(noeud_fin).nombre_arc;
   end if;   


   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x,
                graphe.pasx);
      rx2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x,
                graphe.pasx);
      ry1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y,
                graphe.pasy);
      ry2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y,
                graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for i in ry1..ry2 loop
          for j in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_a,graphe.rblk_a,
                            graphe.rfin_a,graphe.rvid_a,graphe.info.nb_arcs,ir);
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;


-- =====================================================================
-- Destruction d'un arc
Separate(Gen_io)
Procedure GR_DETRUIRE_ARC(graphe : in out graphe_type;
                          arc : positive) is
my_noeud : noeud_type;
my_arc : arc_type;

liste_arcs: liens_array_type(1..graphe.info.max_arc_neu);

begin
   my_arc:=GR_ARC( graphe,arc);

   -- elimination de l arc
   graphe.tarc(arc).nombre_met:=0;

   -- mise a jour des arcs issus du noeud initial de l arc a eliminer
   my_noeud:=GR_NOEUD(graphe,my_arc.noeud_ini);
   if my_noeud.nombre_arc>1 then -- il y a d autres arcs que celui detruit 
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,
                                                           my_arc.noeud_ini);
      -- mise a jour de l arc precedent
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)<0 then --(arc entrant)
             if graphe.tarc(-liste_arcs(i)).arc_sui=arc then
                graphe.tarc(-liste_arcs(i)).arc_sui:=graphe.tarc(arc).arc_pre;
                exit;
             end if;
          else --(arc sortant)
             if graphe.tarc(liste_arcs(i)).arc_pre=arc then
                graphe.tarc(liste_arcs(i)).arc_pre:=graphe.tarc(arc).arc_pre;
                exit;
             end if;
          end if;
      end loop;
      -- mise a jour du fichier des liens pour le noeud initial de l arc elimine
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)=arc then
             for j in i+1..my_noeud.nombre_arc loop
                 liste_arcs(j-1):=liste_arcs(j);
             end loop;
             exit;
          end if;
      end loop;
      ECRIRE_LIENS(graphe,liste_arcs,my_noeud.nombre_arc-1,my_noeud.premier_arc);
   end if;
   graphe.tnod(my_arc.noeud_ini).nombre_arc:=my_noeud.nombre_arc-1;

   -- mise a jour des arcs issus du noeud final de l arc a eliminer
   my_noeud:=GR_NOEUD(graphe,my_arc.noeud_fin);
   if my_noeud.nombre_arc>1 then -- il y a d autres arcs que celui detruit 
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,
                                                           my_arc.noeud_fin);
      -- mise a jour de l arc suivant
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)<0 then --(arc entrant)
             if graphe.tarc(-liste_arcs(i)).arc_sui=-arc then
                graphe.tarc(-liste_arcs(i)).arc_sui:=graphe.tarc(arc).arc_sui;
                exit;
             end if;
          else --(arc sortant)
             if graphe.tarc(liste_arcs(i)).arc_pre=-arc then
                graphe.tarc(liste_arcs(i)).arc_pre:=graphe.tarc(arc).arc_sui;
                exit;
             end if;
          end if;
      end loop;
      -- mise a jour du fichier des liens pour le noeud final de l arc elimine
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)=-arc then
             for j in i+1..my_noeud.nombre_arc loop
                 liste_arcs(j-1):=liste_arcs(j);
             end loop;
             exit;
          end if;
      end loop;
      ECRIRE_LIENS(graphe,liste_arcs,my_noeud.nombre_arc-1,my_noeud.premier_arc);
   end if;
   graphe.tnod(my_arc.noeud_fin).nombre_arc:=my_noeud.nombre_arc-1;
end;


-- =====================================================================
Separate(Gen_io)
Procedure GR_MOD_ATT_ARC (graphe : In out graphe_type; arc : positive;
                          att_graph : natural) is

begin
   graphe.tarc(arc).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_l then
      graphe.info.nb_graph_l:=att_graph;
   end if;
end;



-- =====================================================================
Separate(Gen_io)
Procedure GR_LIGNE_CUBIQUES( graphe                : in out graphe_type; 
			     arc                   : in positive;
			     sigma                 : in float;
			     pas 	           : in float;
			     tolerance             : in float;
			     demi_tolerance_max    : in float;
 			     produit_vectoriel_min : in float;
			     part_angle            : in integer ;
 			     tab_cubiques          : out cubique_liste_type;
 			     n_cub                 : out positive;
			     mode_stockage         : in metrique_type ) is


  ligne   : point_liste_type(1..graphe.info.max_pts_arc);
  npoints : positive;
  tab_cub : cubique_liste_type(1..graphe.info.max_cub_arc+300);
  ncub    : positive;

begin                                    

  Gr_points_d_arc(graphe,arc,ligne,npoints);

  polycubique.CHANG_PTS_CUB(ligne,npoints,tab_cub,ncub,
      sigma,pas,demi_tolerance_max,tolerance,produit_vectoriel_min,part_angle); 

  if mode_stockage = cubique then
      polycubique.ECRIRE_FIN(graphe, tab_cub, ncub);
      graphe.tarc(arc).mode_met:= cubique;
      graphe.tarc(arc).premier_met:=graphe.info.nb_cubiques+1;
      graphe.tarc(arc).nombre_met:=ncub;
      graphe.info.nb_cubiques:=graphe.info.nb_cubiques+ncub;
      if ncub>graphe.info.max_cub_arc then
         graphe.info.max_cub_arc:=ncub;
      end if;
  end if;
  
  tab_cubiques(1..ncub):= tab_cub(1..ncub);
  n_cub:= ncub;

end;

-- =====================================================================
Separate(Gen_io)
Procedure GR_CUBIQUES_LIGNE( graphe        : in out graphe_type; 
			     arc           : in positive;
			     pas	   : in integer;
			     tab_points	   : out point_liste_reel;
			     n_points      : out positive;
			     mode_stockage : in metrique_type ) is
                   
 tab_cubiques : cubique_liste_type(1..graphe.info.max_cub_arc+300);
 n_cub        : positive;
 tabpoints    : point_access_type;
 lgr          : float := 0.0;

begin                                                                         

  Gr_cubiques_d_arc(graphe,arc,tab_cubiques,n_cub);

  for cub in 1..n_cub loop
    lgr := lgr + SQRT(float(tab_cubiques(cub).Xlocal_sommet**2 
                  +(integer(tab_cubiques(cub).a)*(tab_cubiques(cub).Xlocal_sommet)**3)**2));
  end loop;

  declare
    tab_pts_temp : point_liste_reel(1..integer(lgr)+300);
    npoints      : positive;
    tab_pts_filt : point_access_type;
    npoints_filt : positive:=1;
  begin

   polycubique.CHANG_CUB_PTS(tab_cubiques,n_cub,pas,tab_pts_temp,npoints); 

   tabpoints:= new point_liste_type(1..npoints);

   for i in 1..npoints loop
       tabpoints(i).coor_x:= integer(tab_pts_temp(i).coor_x);
       tabpoints(i).coor_y:= integer(tab_pts_temp(i).coor_y);
   end loop;

   -- Filtrage de la ligne des points correspondants aux cubiques :
   tab_pts_filt := new point_liste_type(1..npoints);
   Lissage_filtrage.Douglas_Peucker( tabpoints,npoints,
					tab_pts_filt,npoints_filt,
					-- seuil de filtrage en mm !!!
					1.0/float(graphe.info.resolution),
					graphe.info.resolution);

   if mode_stockage = ligne then
    polyligne.ECRIRE_FIN( graphe, tab_pts_filt(1..npoints_filt),npoints_filt);
    graphe.tarc(arc).mode_met:= ligne;
    graphe.tarc(arc).premier_met:= graphe.info.nb_points+1;
    graphe.tarc(arc).nombre_met:= npoints_filt;
    graphe.info.nb_points:= graphe.info.nb_points + npoints_filt;
    if npoints_filt > graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:= npoints_filt;
    end if;
   end if;

   n_points:= npoints_filt;
   for i in 1..npoints_filt loop
    tab_points(i).coor_x:=float(tab_pts_filt(i).coor_x);
    tab_points(i).coor_y:=float(tab_pts_filt(i).coor_y);
   end loop;

   free_pliste(tabpoints);
   free_pliste(tab_pts_filt);

  end; -- Declare --
end;



-- =======================================================================
With geometrie; Use Geometrie;
Separate(Gen_io) Function GR_DIST_ARC(graphe : graphe_type; P : Point_type;
     A : Positive) return float is

dmin,d : float;
myarc: Arc_type;
Mypts: Point_liste_type(1..Graphe.all.info.Max_pts_arc);
nb_points : natural;


begin
   dmin:=float'last;
   MyArc:=GR_ARC(graphe,A);
   Gr_points_d_arc(Graphe,A,Mypts,nb_points);
   for i in 2..nb_points loop
      d:=Distance_a_ligne(P,Mypts(i-1),Mypts(i),C_segment_type);
      if d<dmin then dmin:=d; end if;
      exit when dmin=0.0;
   end loop;
   return dmin;
end;


-- =======================================================================

Separate(Gen_io) Procedure GR_ARC_PROCHE(Graphe : graphe_type; P : point_type;
       A : out natural; Distance : out float) is
d,dm : float;
di,sn,na : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_arcs);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   loop
      Gr_arcs_in_rectangle(graphe,Haut,Bas,Table,Na);
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Graphe.all.Pasx;
      Haut.Coor_y:=Haut.Coor_y + Graphe.all.Pasy;
      BAs.Coor_x:=BAs.Coor_X - Graphe.all.Pasx;
      Bas.Coor_y:=Bas.Coor_y - Graphe.all.Pasy;
   end loop;
   -- ----- 1 er calcul de distance ----
   Dm:=Float'last;
   for i in 1..na loop
      begin
         D:=GR_DIST_ARC(graphe,p,table(i));
      exception
         when others => D:=Float'last;
      end;
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   -- ----- Calcul de l'encombrement ----
   if dm>0.0 then
      DI:=integer(Sqrt(dm)+1.0);
      Haut.Coor_x:=P.coor_x + di;
      Haut.Coor_y:=P.coor_y + di;
      Bas.Coor_x:=P.coor_x - di;
      Bas.Coor_y:=P.coor_y - di;
      Gr_ARCs_in_rectangle(graphe,Bas,Haut,Table,Na);
      
      for i in 1..na loop
         begin
            d:=Gr_dist_arc(graphe,P,Table(i));
         exception
            when others => D:=Float'last;
         end;
         if d<dm then dm:=d; Sn:=Table(i); end if;
         exit when dm=0.0;
      end loop;
   end if;
   A:=Sn;
   Distance:=sqrt(dm);
end;      
