Package Math_int_basic is
   Function Ceil(a,b : natural) return natural;
   Function Floor(a,b : natural) return natural;
   Function Max(a,b : integer) return integer;
   Function Min(a,b : integer) return integer;
   Function Max(a,b : float) return float;
   Function Min(a,b : float) return float;
   Function Modulo(a,b: integer) return integer;
   Function ent(x:float) return integer;
   Function Decimal(x:float) return float;
end;
