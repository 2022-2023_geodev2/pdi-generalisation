-- ===========================================================================
Separate(Gen_io)
Function  GR_NOEUD (graphe : Graphe_type; noeud : positive) return noeud_type is
begin
   return Graphe.Tnod(noeud);
end;

-- ===========================================================================
Separate(Gen_io)
Function  GR_ARCS_DU_NOEUD (graphe : graphe_type; noeud : positive) 
             return liens_array_type is
A_lire,P_lire,First,N,reste : integer;
My_liste : Liens_array_type(1..graphe.info.max_arc_neu);
begin
   A_lire:=ceil(graphe.tnod(noeud).premier_arc,128);
   if A_lire/=graphe.lbll then
      read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(graphe.tnod(noeud).premier_arc-1,128);
   Reste:=graphe.tnod(noeud).nombre_arc;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,128-p_lire);
      My_liste(First+1..First+N):=Graphe.Blnk(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_lnk,graphe.blnk);
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;      
   return my_liste(1..Graphe.tnod(noeud).nombre_arc);
end;


-- =====================================================================
Separate(Gen_io)
Procedure ECRIRE_LIENS_FIN(graphe : in out graphe_type;
                       tab_arcs : liens_array_type;
                       n : positive) is

-- ecriture des liens (liste de numero d arcs) en fin de fichier des liens

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_liens+1,128);
   if A_lire/=graphe.lbll then
      if modulo(graphe.info.nb_liens,128)/=0 then
         read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      end if;
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_liens,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Blnk(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;  
end;


-- =====================================================================
Separate(Gen_io)
Procedure ECRIRE_LIENS(graphe : in out graphe_type;
                       tab_arcs : liens_array_type;
                       n : positive;
                       premier : positive ) is

-- reecriture des liens (liste de numero d arcs) a la meme adresse du buffer de
-- lien (on ecrit donc une liste dont le nombre d element est inferieur ou egal
-- a la liste de depart)

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,128);
   if A_lire/=graphe.lbll then
      read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(premier-1,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Blnk(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_lnk,graphe.blnk);
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;  
end;


-- ===========================================================================
Separate(Gen_io)
Procedure GR_NOEUDS_IN_RECTANGLE(graphe : graphe_type;
                                 F_point : Point_type;
                                 S_point : Point_type;
                                 tab_noeuds : out liens_array_type;
                                 n : out natural) is

flags : array(1..graphe.info.nb_noeuds) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_n,i_n : integer;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_n:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_n.all(ir+j).count-1 loop
            i_n:=graphe.rblk_n.all(graphe.radr_n.all(ir+j).first+k);
            if not flags(i_n) then
               flags(i_n):=true;
               if graphe.tnod(i_n).coords.coor_x <= upper.coor_x and 
                  graphe.tnod(i_n).coords.coor_y <= upper.coor_y and 
                  graphe.tnod(i_n).coords.coor_x >= lower.coor_x and
                  graphe.tnod(i_n).coords.coor_y >= lower.coor_y and
                  (graphe.tnod(i_n).nombre_arc/=0 or
                   graphe.tnod(i_n).point_seul=true) then
                  n_n:=n_n+1;
                  tab_noeuds(n_n):=i_n;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_n;
end;



-- =====================================================================
Separate(Gen_io)
Procedure GR_MOD_NOEUD(graphe:in out graphe_type;
                       noeud:positive; 
                       point:point_type) is
-- Deplacement des coordonnées d un noeud (!! SANS DEPLACEMENT DES ARCS !! )


rx1,ry1,ir : integer;

begin

   graphe.tnod(noeud).coords:=point;
   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tnod(noeud).coords.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tnod(noeud).coords.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1;
      AJOUT_ELEMENT_IN_REGION(graphe.radr_n,graphe.rblk_n,
                                         graphe.rfin_n,graphe.rvid_n,noeud,ir);
   end if;

end;
   


-- =====================================================================
Separate(Gen_io)
Procedure GR_CREER_NOEUD_SUR_RIEN(graphe : in out graphe_type;
                                  point : point_type;
                                  point_seul : boolean;
                                  orientation : angle_type;
                                  att_graph : natural; 
                                  noeud : out positive) is

-- Creation d un noeud sur rien

rx1,ry1,ir : integer;

begin
   -- etudier ulterieurement s il faut ajouter un test au cas on ne peut 
   -- ajouter un noeud au graphe pour des problemes de taille de tableau
   graphe.info.nb_noeuds:=graphe.info.nb_noeuds + 1;
   graphe.tnod(graphe.info.nb_noeuds).coords:=point;
   graphe.tnod(graphe.info.nb_noeuds).premier_arc:=graphe.info.nb_liens+1;
   graphe.tnod(graphe.info.nb_noeuds).nombre_arc:=0;
   graphe.tnod(graphe.info.nb_noeuds).point_seul:=point_seul;
   graphe.tnod(graphe.info.nb_noeuds).orientation:=orientation;
   graphe.tnod(graphe.info.nb_noeuds).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_p then
      graphe.info.nb_graph_p:=att_graph;
   end if;

   noeud:=graphe.info.nb_noeuds;

   -- mise a jour eventuelle de la regionalisation
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tnod(graphe.info.nb_noeuds).coords.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tnod(graphe.info.nb_noeuds).coords.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1;
      AJOUT_ELEMENT_IN_REGION(graphe.radr_n,graphe.rblk_n,
                          graphe.rfin_n,graphe.rvid_n,graphe.info.nb_noeuds,ir);
   end if;

end;



-- =====================================================================
With Geometrie;use Geometrie;
Separate(Gen_io)
Procedure GR_CREER_NOEUD_SUR_ARC(graphe : in out graphe_type;
                                 arc : in positive; 
                                 point : in point_type;
                                 point_seul : boolean;
                                 orientation : angle_type;
                                 att_graph : natural; 
                                 noeud : out positive;
                                 arc_debut : out positive;
                                 arc_fin : out positive) is

-- Creation d un noeud sur un arc et donc decoupe de l'arc initial en 2 arcs

my_arc : arc_type;
my_face_g,my_face_d : face_type;
--type point_access_type is access point_liste_type;
liste_points,tab1,tab2 : point_liste_type(1..graphe.info.max_pts_arc);
infx,infy,maxx,maxy,inter : integer;
n1,n2,nouveau_noeud : positive;
troncon_inter : natural :=0;
nombre_pt : natural;
dist,distance_min : float;
tarc_face_g,tarc_face_d : liens_array_type(1..graphe.info.max_arc_fac+1);
position_arc_face_g,position_arc_face_d : natural;
face : natural;
arc_d,arc_f : positive;
Point_hors_ligne : boolean :=false;
esc : boolean;

begin
   my_arc:=GR_ARC(graphe,arc);
   -- determination de la position du point sur l'arc
   GR_POINTS_D_ARC(graphe,arc,liste_points,nombre_pt);
   for i in 1..nombre_pt-1 loop
       dist:=Distance_a_ligne(point,liste_points(i),liste_points(i+1),
                              c_segment_type);
       if dist=0.0 then
          troncon_inter:=i;
          exit;
       end if;
   end loop;

   if troncon_inter=0 then 
      -- il n y a aucun point rigoureusement sur un segment de droite de l'arc
      -- on modifie un peu la geometrie du troncon et on peut le signaler
      distance_min:=Distance_a_ligne(point,liste_points(1),liste_points(1+1),
                              c_segment_type);
      troncon_inter:=1;
      Point_hors_ligne:=true;
      for i in 2..nombre_pt-1 loop
          dist:=Distance_a_ligne(point,liste_points(i),liste_points(i+1),
                                 c_segment_type);
          if dist < distance_min then
             distance_min:=dist;
             troncon_inter:=i;
          end if;
      end loop;
   end if;


   if liste_points(troncon_inter+1)=point then
      -- le point intermediaire existe deja
      n1:=troncon_inter+1;
      tab1(1..n1):=liste_points(1..n1);
      n2:=nombre_pt-n1+1;
      tab2(1..n2):=liste_points(n1..nombre_pt);
   else
      -- le point intermediaire n existe pas : il faut le creer comme un noeud
      n1:=troncon_inter+1;
      tab1(1..n1-1):=liste_points(1..n1-1);
      tab1(n1):=point;
      n2:=nombre_pt-n1+2;
      tab2(1):=point;
      tab2(2..n2):=liste_points(n1..nombre_pt);
   end if;

   if my_arc.face_g/=0 then
-- Si l arc est limite de face_g, il faut stocker la liste des arcs constituant
-- cette face pour pouvoir modifier la face;
      face:=my_arc.face_g;
      position_arc_face_g:=0;
      loop
         my_face_g:=GR_FACE(graphe,face);
         tarc_face_g(1..my_face_g.nombre_arc):=GR_ARCS_DE_FACE(graphe,face);
         for i in 1..my_face_g.nombre_arc loop
             if tarc_face_g(i)=-arc then
                position_arc_face_g:=i;
                exit;
             end if;
         end loop;
         exit when(position_arc_face_g/=0);
         face:=my_face_g.trou_sui;
      end loop;
   end if;

   if my_arc.face_d/=0 then
-- Si l arc est limite de face_d, il faut stocker la liste des arcs constituant
-- cette face pour pouvoir modifier la face;
      face:=my_arc.face_d;
      position_arc_face_d:=0;
      loop
         my_face_d:=GR_FACE(graphe,face);
         tarc_face_d(1..my_face_d.nombre_arc):=GR_ARCS_DE_FACE(graphe,face);
         for i in 1..my_face_d.nombre_arc loop
             if tarc_face_d(i)=arc then
                position_arc_face_d:=i;
                exit;
             end if;
         end loop;
         exit when(position_arc_face_g/=0);
         face:=my_face_d.trou_sui;
      end loop;
   end if;
                                            
   GR_DETRUIRE_ARC(graphe,arc);
   GR_CREER_NOEUD_SUR_RIEN(graphe,point,point_seul,orientation,att_graph,
                           nouveau_noeud);
   noeud:=nouveau_noeud;
   GR_CREER_ARC(graphe,my_arc.noeud_ini,nouveau_noeud,tab1,n1,Ligne,my_arc.att_graph,0,arc_d,esc);
   GR_CREER_ARC(graphe,nouveau_noeud,my_arc.noeud_fin,tab2,n2,Ligne,my_arc.att_graph,0,arc_f,esc);
   arc_debut:=arc_d;
   arc_fin:=arc_f;
     
-- il faut alors ajouter la modification des faces eventuelles avec le remplace-
-- ment de l arc coupe par les deux nouveaux arcs
   if my_arc.face_g/=0 then
      graphe.tarc(arc_d).face_g:=my_arc.face_g;
      graphe.tarc(arc_f).face_g:=my_arc.face_g;
      for i in reverse position_arc_face_g+1..my_face_g.nombre_arc loop
          tarc_face_g(i+1):=tarc_face_g(i);
      end loop;
      tarc_face_g(position_arc_face_g):=-arc_f;
      tarc_face_g(position_arc_face_g+1):=-arc_d;
      GR_MOD_ARCS_DE_FACE(graphe,my_arc.face_g,tarc_face_g,my_face_g.nombre_arc
                                                           +1);
   end if;

   if my_arc.face_d/=0 then
      graphe.tarc(arc_d).face_d:=my_arc.face_d;
      graphe.tarc(arc_f).face_d:=my_arc.face_d;
      for i in reverse position_arc_face_d+1..my_face_d.nombre_arc loop
          tarc_face_d(i+1):=tarc_face_d(i);
      end loop;
      tarc_face_d(position_arc_face_d):=arc_d;
      tarc_face_d(position_arc_face_d+1):=arc_f;
      GR_MOD_ARCS_DE_FACE(graphe,my_arc.face_d,tarc_face_d,my_face_d.nombre_arc
                                                           +1);
   end if;


end;

  
-- ====================================================================
Separate(Gen_io)
Procedure GR_DETRUIRE_NOEUD(graphe : in out graphe_type;
                            noeud : positive) is

-- Destruction d'un noeud 

begin
  graphe.tnod(noeud).point_seul:=false ;
end;


-- =====================================================================
Separate(Gen_io)
Procedure GR_MOD_ATT_NOEUD (graphe : In out graphe_type; noeud : positive;
                            att_graph : natural) is

begin
   graphe.tnod(noeud).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_p then
      graphe.info.nb_graph_p:=att_graph;
   end if;
end;


-- =====================================================================
Separate(Gen_io)
Procedure GR_FUSION_AU_NOEUD(graphe : In out Graphe_type;
                             noeud : positive;
                             arc : out natural) is

Tab_arcs : Liens_array_type(1..2);
liste_points1,liste_points2 : point_liste_type(1..graphe.info.max_pts_arc);
liste_points : point_liste_type(1..2*graphe.info.max_pts_arc);
arc_prov : integer;
arc1,arc2 : positive;
my_arc1,my_arc2 : arc_type;
nombre_pts1,nombre_pts2,nombre_pts : natural;
noeud_ini,noeud_fin : positive;
my_noeud_ini,my_noeud_fin : noeud_type;
tab_arcs_noeud_fin,tab_arcs_noeud_ini : liens_array_type(1..graphe.info.Max_arc_neu);
flag : boolean;
nb : integer;

begin

   if graphe.tnod(noeud).point_seul=false and
       graphe.tnod(noeud).nombre_arc=2  then

       Tab_arcs(1..2):=GR_ARCS_DU_NOEUD(graphe,noeud);

       -- on fait en sorte de conserver le numero d'arc le plus petit
       if abs(tab_arcs(2)) < abs(Tab_arcs(1)) then
          arc_prov:=tab_arcs(1);
          tab_arcs(1):=tab_arcs(2);
          tab_arcs(2):=arc_prov;
       end if;

       arc1:=abs(Tab_arcs(1));
       arc2:=abs(Tab_arcs(2));

       my_arc1:=GR_arc(graphe,arc1);
       my_arc2:=GR_arc(graphe,arc2);

       if my_arc1.att_graph=my_arc2.att_graph and arc1/=arc2 then 

          -- Recuperation de la liste des points
          GR_POINTS_D_ARC(graphe,arc1,liste_points1,nombre_pts1);
          GR_POINTS_D_ARC(graphe,arc2,liste_points2,nombre_pts2);
       
          -- Destruction de l'arc2 et du noeud intermediaire
          graphe.tarc(arc2).nombre_met:=0;
          graphe.tnod(noeud).nombre_arc:=0;


          nombre_pts:=nombre_pts1 + nombre_pts2 -1;
          if Tab_arcs(1)<0 then
             -- arc1 arrive au noeud
             liste_points(1..nombre_pts1):=liste_points1(1..nombre_pts1);
             if Tab_arcs(2)>0 then   -- arc2 part du noeud
                -- mise a jour geometrique
                liste_points(nombre_pts1..nombre_pts):=liste_points2(1..nombre_pts2);

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_fin:=my_arc2.noeud_fin;
                if my_arc2.arc_sui=-arc2 then
                   graphe.tarc(arc1).arc_sui:=-arc1;
                else
                   graphe.tarc(arc1).arc_sui:=my_arc2.arc_sui;
                end if;

                -- mise a jour topologique au niveau du noeud_fin 
                noeud_fin:=my_arc2.noeud_fin;
                my_noeud_fin:=GR_NOEUD(graphe,noeud_fin);
                tab_arcs_noeud_fin(1..my_noeud_fin.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_fin);
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)=-arc2 then
                       tab_arcs_noeud_fin(i):=-arc1;
                       exit;
                    end if;
                end loop;
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre=-arc2 then
                          graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre:=-arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui=-arc2 then
                          graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui:=-arc1;
                          exit;
                       end if;
                    end if; 
                end loop;
             else  -- arc2 arrive au noeud
                -- mise a jour geometrique
                for i in 1..nombre_pts2-1 loop
                    liste_points(nombre_pts1+i):=liste_points2(nombre_pts2-i);
                end loop;

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_fin:=my_arc2.noeud_ini;
                if my_arc2.arc_pre=arc2 then
                   graphe.tarc(arc1).arc_sui:=-arc1;
                else
                   graphe.tarc(arc1).arc_sui:=my_arc2.arc_pre;
                end if;

                -- mise a jour topologique au niveau du noeud_fin 
                noeud_fin:=my_arc2.noeud_ini;
                my_noeud_fin:=GR_NOEUD(graphe,noeud_fin);
                tab_arcs_noeud_fin(1..my_noeud_fin.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_fin);
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)=arc2 then
                       tab_arcs_noeud_fin(i):=-arc1;
                       exit;
                    end if;
                end loop;

                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre=arc2 then
                          graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre:=-arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui=arc2 then
                          graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui:=-arc1;
                          exit;
                       end if;
                    end if; 
                end loop;
             end if;
             -- reecriture des bons liens au noeud
             Ecrire_liens(graphe,tab_arcs_noeud_fin,my_noeud_fin.nombre_arc,
                               my_noeud_fin.premier_arc);

          else
             -- arc1 part du noeud; Pour avoir le nouvel arc dans le meme sens
             --  que'arc1, il faut inverser le sens du nouve arc

             liste_points(nombre_pts2..nombre_pts):=liste_points1(1..nombre_pts1);


             if Tab_arcs(2)<0 then   -- arc2 arrive au noeud (arc2, arc1 meme orientation)
                -- mise a jour geometrique
                liste_points(1..nombre_pts2):=liste_points2(1..nombre_pts2);

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_ini:=my_arc2.noeud_ini;
                if my_arc2.arc_pre=arc2 then
                   graphe.tarc(arc1).arc_pre:=arc1;
                else
                   graphe.tarc(arc1).arc_pre:=my_arc2.arc_pre;
                end if;

                -- mise a jour topologique au niveau du noeud_ini 
                noeud_ini:=my_arc2.noeud_ini;
                my_noeud_ini:=GR_NOEUD(graphe,noeud_ini);
                tab_arcs_noeud_ini(1..my_noeud_ini.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_ini);
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)=arc2 then
                       tab_arcs_noeud_ini(i):=arc1;
                       exit;
                    end if;
                end loop;
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre=arc2 then
                          graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre:=arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui=arc2 then
                          graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui:=arc1;
                          exit;
                       end if;
                    end if; 
                end loop;
             else  -- arc2 part du noeud (arc1 et arc2 en sens opposes)
                -- mise a jour geometrique
                for i in 1..nombre_pts2 loop
                    liste_points(i):=liste_points2(nombre_pts2+1-i);
                end loop;

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_ini:=my_arc2.noeud_fin;
                if my_arc2.arc_sui=-arc2 then
                   graphe.tarc(arc1).arc_pre:=arc1;
                else
                   graphe.tarc(arc1).arc_pre:=my_arc2.arc_sui;
                end if;

                -- mise a jour topologique au niveau du noeud_ini 
                noeud_ini:=my_arc2.noeud_fin;
                my_noeud_ini:=GR_NOEUD(graphe,noeud_ini);
                tab_arcs_noeud_ini(1..my_noeud_ini.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_ini);
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)=-arc2 then
                       tab_arcs_noeud_ini(i):=arc1;
                       exit;
                    end if;
                end loop;

                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre=-arc2 then
                          graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre:=arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui=-arc2 then
                          graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui:=arc1;
                          exit;
                       end if;
                    end if; 
                end loop;
             end if;
             -- reecriture des bons liens au noeud
             Ecrire_liens(graphe,tab_arcs_noeud_ini,my_noeud_ini.nombre_arc,
                               my_noeud_ini.premier_arc);

          end if;

          GR_MOD_POINTS_D_ARC(graphe,arc1,liste_points,nombre_pts);
          arc:=arc1;
                      
       else
         arc:=0;
       end if;
    else
       arc:=0;
    end if;
end;
 --******************************************************************************
-- Modifie le 21/06/1999 pour portage sous WindowsNT
--******************************************************************************

-- ==========================================================================

With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
--With FLoat_MAth_lib; Use Float_math_lib;
With Geometrie; Use geometrie;
Separate(Gen_io) Procedure GR_NOEUD_PROCHE(Graphe : graphe_type; P : point_type;
       N : out natural; Distance : out float) is

d,dm : float;
di,sn,na,pas  : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_noeuds);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   if graphe.all.pasx > graphe.all.pasy then pas:=graphe.all.pasx; 
   else pas:=graphe.all.pasy; end if;
   loop
      Pas:=Pas+1;
	  Gr_noeuds_in_rectangle(graphe,Haut,Bas,Table,Na);
      Pas:=Pas-1;
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Pas;
      Haut.Coor_y:=Haut.Coor_y + Pas;
      BAs.Coor_x:=BAs.Coor_X - Pas;
      Bas.Coor_y:=Bas.Coor_y - Pas;
   end loop;
   -- ----- calcul de distance ----
   Dm:=Float'last;
   for i in 1..na loop
      D:=DISTANCE_DE_POINTS(P,Graphe.all.Tnod(table(i)).Coords);
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   N:=Sn;
   Distance:=sqrt(dm);
end;      

Separate(Gen_io) Procedure GR_NOEUD_PROCHE_LONG(Graphe : graphe_type; P : point_type;
       N : out natural; Distance : out long_float) is

Function Distance_de_points_long( M : in Point_type; N : in point_type) return long_float is
D2 : long_float;
begin 
   D2:=long_float(m.coor_x-n.coor_x)*long_float(m.coor_x-n.coor_x)+long_float(m.coor_y-n.coor_y)*long_float(m.coor_y-n.coor_y);
   return long_float(D2);
end;

d,dm : long_float;
di,sn,na,pas  : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_noeuds);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   if graphe.all.pasx > graphe.all.pasy then pas:=graphe.all.pasx; 
   else pas:=graphe.all.pasy; end if;
   loop
	  Gr_noeuds_in_rectangle(graphe,Haut,Bas,Table,Na);
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Pas;
      Haut.Coor_y:=Haut.Coor_y + Pas;
      BAs.Coor_x:=BAs.Coor_X - Pas;
      Bas.Coor_y:=Bas.Coor_y - Pas;
   end loop;
   -- ----- calcul de distance ----
   Dm:=long_Float'last;
   for i in 1..na loop
      D:=DISTANCE_DE_POINTS_lONG(P,Graphe.all.Tnod(table(i)).Coords);
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   N:=Sn;
   Distance:=dm;
end;      

