With Unchecked_deallocation;

Package Gen_io is


-- ==== Les types associes aux points
-- -----------------------------------
Type Point_type  is record
   Coor_x : integer;
   Coor_y : integer;
end record;

Pragma pack(Point_type);-- Pour compatibilite VMS

Type Point_liste_type is array(natural range<>) of Point_type;
Type Point_access_type is access Point_liste_type;
-- Procedure GR_Free_point_liste pour deallocation;

-- Types associes aux points (Coordonnees reelles)
Type Point_type_reel is record
	Coor_x : float;
	Coor_y : float;
end record;

Type Point_liste_reel is array(natural range <>) of Point_type_reel;
Type Point_Access_reel is access Point_liste_reel;
-- Procedure GR_Free_point_liste_reel pour deallocation;

Type Reel_tableau_type is array(natural range <>) of float;
Type Reel_Access_type is access Reel_tableau_type;
-- Procedure GR_Free_reel_tableau pour deallocation;

-- ==== Les types associes aux cubiques
-- -----------------------------------
Type Cubique_type  is record
   a        	   : float:= 0.0;
   Phi             : float := 0.0;
   Point_inflexion : point_type;
   Xlocal_sommet   : integer;
end record;
Type Cubique_liste_type is array(natural range<>) of cubique_type;
Type Cubique_access_type is access Cubique_liste_type;
-- Procedure GR_Free_cubique_liste pour deallocation;

-- ==== Les types associes aux arcs
-- ---------------------------------
Type Boite_type is record
   Minimum : Point_type;
   Maximum : Point_type;
end record;
Pragma pack(Boite_type);-- Pour la compatibilite avec graphes VMS

Type Metrique_type is (Ligne,Cubique);
For Metrique_type'Size use 8;-- Pour la compatbilite avec graphes VMS

Type Arc_type is record
   Noeud_ini   : positive;
   Noeud_fin   : positive;
   Face_g      : integer;
   Face_d      : integer;
   Arc_pre     : integer;
   Arc_sui     : integer;
   Encombr     : Boite_type;
   Att_graph   : natural;
   Mode_met    : Metrique_type;
   Premier_met : positive;
   Nombre_met  : natural;
   Octet_vms   : short_short_integer :=0;-- octet supplementaire pour compatibilite VMS
end record;
Pragma Pack(Arc_type);-- Pour la compatbilite avec graphes VMS


-- ==== Les types associes aux noeuds
-- -----------------------------------
Type Angle_type is delta 0.1 range -18000.0..18000.0;
Type Noeud_type is record
   Coords     : Point_type;
   Premier_arc: positive;
   Nombre_arc : natural;
   Point_seul : boolean;
   Orientation: Angle_type;
   Att_graph  : natural;
end record;

Type Liens_array_type is array(natural range <>) of integer;
Type Liens_access_type is access Liens_array_type;

-- ==== Les types associes aux faces
-- ----------------------------------
Type Face_type is record
   Premier_arc : positive;
   Nombre_arc  : natural;
   Trou_sui    : natural;
   Zone_englob : natural;
   Att_graph   : natural;
end record;


Type Surf_type(MXE,MXP : integer) is record
     NE  : Natural;
     NA  : Liens_array_type(1..Mxe);
     PTS : Point_liste_type(1..MXP);
end record;
-- Ne : Nombre de face + trou
-- Na : Nombre de points par arcs pour decrire la face compete (avec trou)


-- ==== Le Type information
-- -------------------------
Type Info_type is record
   Delta_x    : positive;
   Delta_y    : positive;
   Nb_arcs    : natural;
   Nb_noeuds  : natural;
   Nb_faces   : natural;
   Nb_points  : natural;
   Nb_cubiques: natural;
   Nb_liens   : natural;
   Nb_domaines: natural;
   Nb_graph_l : natural;
   Nb_graph_p : natural;
   Nb_graph_z : natural;
   Max_pts_arc: natural;
   Max_cub_arc: natural;
   Max_arc_neu: natural;
   Max_arc_fac: natural;
   Echelle    : positive;
   Resolution : positive;
end record;

-- ==== Les Types associes a l'affichage
-- ---------------------------------------
Type Affichage_type is record
   Largeur : float;    -- en mm, largeur exterieure
   Rouge   : float;    -- entre 0.0 (noir) et 1.0  (rouge)
   Vert    : float;    -- entre 0.0 (noir) et 1.0  (vert)
   Bleu    : float;    -- entre 0.0 (noir) et 1.0  (bleu)
   Larg_int : float;    -- en mm, largeur interne sans bordure noire
   Theme   : string(1..30):="                              ";
   Priorite: integer range 1..10 :=1;  -- comprise entre 1 et 10
end record;

Type Affichage_array_type is array(natural range <>) of Affichage_type;


Type Graphe_type is private;
Type Graphe_mode_type is (In_graphe, Inout_graphe);
Type Distance_type is (C_segment_type, C_ligne_type);

Type Legende_type is private;


-- Fonctions pour la gestion globale du graphe ---
Function GR_DISK_INFOS (nom : string) return Info_type;
Function GR_LOAD_FROM_DISK ( nom : string; extension : natural:=10;
            mode: graphe_mode_type:=In_graphe ) return Graphe_type;
Procedure GR_SAVE_TO_DISK ( graphe : graphe_type; nom : string);
Function  GR_CREATE_NEW ( deltax : Positive; deltay : positive;
             Resolution : integer; Echelle : integer :=1;
             Narcs : Positive :=100;  Nnoeuds : positive :=100;
             Nfaces : Positive :=100;  Nom_tmp : string :="Unnamed_graphe")
             return graphe_type;
Function  GR_INFOS (graphe : Graphe_type) return Info_type;
Procedure GR_DELETE ( graphe : in out Graphe_type);

-- Fonctions de base sur les arcs ---
Function  GR_ARC (graphe : Graphe_type; arc : positive ) return arc_type;
Procedure  GR_POINTS_D_ARC (graphe : graphe_type; arc : positive;
                            tab_points : out point_liste_type; n : out natural);
Procedure  GR_CUBIQUES_D_ARC (graphe : graphe_type; arc : positive;
                              tab_cubiques : out cubique_liste_type;
                              n_cubiques : out natural);
Procedure GR_ARCS_IN_RECTANGLE (graphe : graphe_type; F_point : Point_type;
   S_point : Point_type; tab_arcs : out liens_array_type; n : out natural);
Procedure GR_ARC_PROCHE(graphe : graphe_type; P : point_type; A : out natural;
   distance : out float);
Function  GR_DIST_ARC(graphe : graphe_type; P : Point_type; A : Positive)
   return float;

Procedure GR_LIGNE_CUBIQUES( graphe            : in out graphe_type;
			     arc               : in positive;
			     sigma             : in float;
			     pas	       : in float;
			     tolerance         : in float;
			     demi_tolerance_max: in float;
 			     produit_vectoriel_min: in float;
			     part_angle        : in integer ;
 			     tab_cubiques      : out cubique_liste_type;
 			     n_cub             : out positive;
			     mode_stockage     : in metrique_type ) ;

Procedure GR_CUBIQUES_LIGNE( graphe            : in out graphe_type;
			     arc               : in positive;
			     pas	       : in integer;
			     tab_points	       : out point_liste_reel;
			     n_points          : out positive;
			     mode_stockage     : in metrique_type ) ;




-- Fonctions de base sur les noeuds ---
Function  GR_NOEUD (graphe : Graphe_type; noeud : positive) return noeud_type;
Function  GR_ARCS_DU_NOEUD (graphe : graphe_type; noeud : positive)
             return liens_array_type;
Procedure GR_NOEUDS_IN_RECTANGLE (graphe : graphe_type; F_point : Point_type;
   S_point : Point_type; tab_noeuds : out liens_array_type; n : out natural);
Procedure GR_NOEUD_PROCHE(graphe : graphe_type; P : point_type; N : out natural;
   distance : out float);
Procedure GR_NOEUD_PROCHE_LONG(graphe : graphe_type; P : point_type; N : out natural;
   distance : out long_float);


-- Fonctions de base sur les faces ---
Function  GR_FACE  (graphe : Graphe_type; face : positive) return face_type;
Function  GR_ARCS_DE_FACE (graphe : graphe_type; face : positive)
             return liens_array_type;
Procedure GR_FACES_IN_RECTANGLE (graphe : graphe_type; F_point : Point_type;
   S_point : Point_type; tab_faces : out liens_array_type; n : out natural);
Function GR_QUELLE_FACE(Graphe  : graphe_type; P : Gen_io.Point_type) return natural;
Function GR_ENCOMBREMENT_FACE(Graphe : graphe_type;face : positive) return boite_type;
Procedure GR_CONTOURS_DE(Graphe : graphe_type; F : Positive; S : in out Surf_type);
Function GR_DIST_FACE(graphe : graphe_type; P : Point_type; F : Positive)
   return float;



-- Fonctions pour la regionalisation ---
Function  GR_IS_REGION (graphe : Graphe_type) return boolean;
Procedure GR_REGION (graphe :In out Graphe_type; pasx : positive:=256;
             Pasy : positive:=256);
Procedure GR_ANNULE_REGION(graphe : In out Graphe_type);


 -- Fonctions pour la legende ---
Function  GR_CREATE_NEW_LEGENDE ( leg_l : natural;
                                  leg_p : natural;
                                  leg_z : natural)   return legende_type;
Function GR_LOAD_FROM_DISK_LEGENDE ( nom : string) return Legende_type;
Procedure GR_SAVE_TO_DISK_LEGENDE (Graphe : graphe_type; legende : Legende_type;
                                   nom : string);
Function GR_LEGENDE_L (legende : Legende_type; att_graph : natural)
           return affichage_type;
Function GR_LEGENDE_P (legende : Legende_type; att_graph : natural)
           return affichage_type;
Function GR_LEGENDE_Z (legende : Legende_type; att_graph : natural)
           return affichage_type;
Procedure GR_MOD_LEGENDE_L(legende : in out Legende_type; att_graph : natural;
                           Affichage : affichage_type);
Procedure GR_MOD_LEGENDE_P(legende : in out Legende_type; att_graph : natural;
                           Affichage : affichage_type);
Procedure GR_MOD_LEGENDE_Z(legende : in out Legende_type; att_graph : natural;
                           Affichage : affichage_type);
Procedure GR_AJOUT_LEGENDE_L(legende : in out Legende_type;
                      att_graph : out natural ;Affichage : affichage_type);
Procedure GR_AJOUT_LEGENDE_P(legende : in out Legende_type;
                      att_graph : out natural ;Affichage : affichage_type);
Procedure GR_AJOUT_LEGENDE_Z(legende : in out Legende_type;
                      att_graph : out natural ;Affichage : affichage_type);

-- Fonctions plus complexes de manipulation sur  les arcs, les noeuds, les faces
Procedure GR_MOD_POINTS_D_ARC (graphe : In out Graphe_type; arc : positive;
              tab_points : point_liste_type; n : natural);
Procedure GR_MOD_NOEUD (graphe : In out Graphe_type; noeud : positive;
              point : point_type);

Procedure GR_CREER_ARC(graphe : in out graphe_type;
                       noeud_ini : positive;
                       noeud_fin : positive;
                       tab_points : point_liste_type;
                       n : positive;
                       mode : metrique_type;
                       att_graph : natural;
                       arc : out positive);

Procedure GR_CREER_ARC_bis(graphe : in out graphe_type;
                       noeud_ini : positive;
                       noeud_fin : positive;
                       tab_points : point_liste_type;
                       n : positive;
                       mode : metrique_type;
                       att_graph : natural;
 					   OV : in short_short_integer;
                       arc : out positive;
					   Exit_sans_creer : out boolean);

Procedure GR_DETRUIRE_ARC(graphe : in out graphe_type;
                          arc : positive);
Procedure GR_CREER_NOEUD_SUR_RIEN(graphe : in out graphe_type;
                                  point : point_type;
                                  point_seul : boolean;
                                  orientation : angle_type;
                                  att_graph : natural;
                                  noeud : out positive);
Procedure GR_CREER_NOEUD_SUR_ARC(graphe : in out graphe_type;
                                 arc : in positive;
                                 point : in point_type;
                                 point_seul : boolean;
                                 orientation : angle_type;
                                 att_graph : natural;
                                 noeud : out positive;
                                 arc_debut : out positive;
                                 arc_fin : out positive);
Procedure GR_DETRUIRE_NOEUD(graphe : in out graphe_type;
                            noeud : positive);
Procedure GR_CREER_FACE(graphe : in out graphe_type;
                        tab_arcs : liens_array_type;
                        n : positive;
                        att_graph : natural;
                        face : out positive);
Procedure GR_DETRUIRE_FACE(graphe : in out graphe_type;
                          face : positive);
Procedure GR_MOD_ARCS_DE_FACE(graphe : In out Graphe_type;
                              face : positive;
                              tab_arcs : liens_array_type;
                              n : natural);
Procedure GR_FUSION_AU_NOEUD(graphe : In out Graphe_type;
                             noeud : positive;
                             arc : out natural);



-- modification de l'attribut graphique
Procedure GR_MOD_ATT_ARC (graphe : In out graphe_type; arc : positive;
                          att_graph : natural);
Procedure GR_MOD_ATT_NOEUD (graphe : In out graphe_type; noeud : positive;
                          att_graph : natural);
Procedure GR_MOD_ATT_FACE (graphe : In out graphe_type; face : positive;
                          att_graph : natural);


-- Deallocation des variables de type pointeur utilis�e dans ce package
Procedure GR_Free_point_liste is new Unchecked_deallocation(
                                            Point_liste_type,Point_access_type);
Procedure GR_Free_point_liste_reel is new Unchecked_deallocation(
                                            Point_liste_reel,Point_access_reel);
Procedure GR_Free_reel_tableau is new Unchecked_deallocation(
                                            Reel_tableau_type,Reel_access_type);
Procedure GR_Free_cubique_liste is new Unchecked_deallocation(
                                        Cubique_liste_type,Cubique_access_type);
Procedure GR_Free_liens_array is new Unchecked_deallocation(
                                            Liens_array_type,Liens_access_type);

Private
   Type Graphe_struc_type( Na : natural; Nn : natural; Nf : natural);
   Type Graphe_type is access Graphe_struc_type;

   Type Legende_struc_type(Nlegl : natural; Nlegp : natural; Nlegz : natural);
   Type Legende_type is access Legende_struc_type;
end;
