--******************************************************************************
-- Modifie le 21/06/1999 pour portage sous WindowsNT
--******************************************************************************

Package Body geometrie is

-- ------------------------------------------------------------------------
Function Distance_de_points( M : in Point_type; N : in point_type) 
         return float is

D2 : integer;
begin 
   D2:=(m.coor_x-n.coor_x)*(m.coor_x-n.coor_x) + 
       (m.coor_y-n.coor_y)*(m.coor_y-n.coor_y);
   return float(D2);
end;

-- ------------------------------------------------------------------------
Function Distance_de_points_long( M : in Point_type; N : in point_type) 
         return long_float is

D2 : long_float;
begin 
   D2:=long_float(m.coor_x-n.coor_x)*long_float(m.coor_x-n.coor_x) + 
       long_float(m.coor_y-n.coor_y)*long_float(m.coor_y-n.coor_y);
   return D2;
end;

-- ------------------------------------------------------------------------
Function Distance_de_points( M : in Point_type; N : in point_type) 
         return integer is

D2 : integer;
begin 
   D2:=(m.coor_x-n.coor_x)*(m.coor_x-n.coor_x) + 
       (m.coor_y-n.coor_y)*(m.coor_y-n.coor_y);
   return D2;
end;

-- ------------------------------------------------------------------------
Function Distance_de_points( M : in Point_type_reel; 
			     N : in point_type_reel) return float is

D2 : float;
begin 
   D2:=(m.coor_x-n.coor_x)*(m.coor_x-n.coor_x) + 
       (m.coor_y-n.coor_y)*(m.coor_y-n.coor_y);
   return D2;
end;

-- ------------------------------------------------------------------------
Function Distance_de_points( M : in Point_type_reel; 
			     N : in point_type_reel) return integer is
D2 : integer;

begin 
   D2:=integer((m.coor_x-n.coor_x)*(m.coor_x-n.coor_x) + 
               (m.coor_y-n.coor_y)*(m.coor_y-n.coor_y));
   return D2;
end;


-- ------------------------------------------------------------------------
Function norme_v2( M : in Point_type; N : in point_type) return float is
 D : Long_float;
 D2 : float;
begin -- norme
   D:=((long_float(m.coor_x-n.coor_x)*long_float(m.coor_x-n.coor_x) + 
        long_float(m.coor_y-n.coor_y)*long_float(m.coor_y-n.coor_y)));
   if D>long_float(float'last) then
   	 -- D2:=sqrt(float'last);
	 d2:=0.0;
   else
     D2:=sqrt(float(D));
   end if;
   return D2;
end norme_v2;


-- ------------------------------------------------------------------------
Function norme( M : in Point_type; N : in point_type) return float is
 D2 : float;
begin -- norme
   D2:=sqrt(float((m.coor_x-n.coor_x)*(m.coor_x-n.coor_x) + 
       (m.coor_y-n.coor_y)*(m.coor_y-n.coor_y)));
   return D2;
end norme;

-- ------------------------------------------------------------------------
Function norme(m,n : in point_type_reel) return float is

Begin
  return norme(m.coor_x,m.coor_y,n.coor_x,n.coor_y);
end Norme;

-- ------------------------------------------------------------------------
Function norme( x1,y1,x2,y2 : float) return float is

begin 
   return sqrt( (x1-x2)**2 + (y1-y2)**2 );
end norme;



-- ------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type; 
			  A : in Point_type; 
                    	  B : in Point_type; 
		       Mode : Distance_type) return float is

D2,D 		: float;
unitaireX,
unitaireY 	: float;
lambda 		: float;
mX,mY 		: float;
projete 	: Point_type;

begin -- distance_a_ligne

 -- Norme du vecteur AB
 D2 := norme_v2(A,B);

 if (D2 = 0.0) then 

  -- A et B sont confondus  
  D := norme_v2(A,M);
  return D;

 else
 
  -- Calcul du vecteur unitaire de support AB
  unitaireX := float(B.Coor_x - A.Coor_x) / D2;
  unitaireY := float(B.Coor_y - A.Coor_y) / D2;

  -- Calcul du projete de M sur AB = Produit scalaire AM AB
  lambda := float(M.Coor_x - A.Coor_x)*unitaireX + float(M.Coor_y - A.Coor_y)*unitaireY; 
  
  -- Calcul du point projete
  if ((mode = C_segment_type) and then (lambda <= 0.0)) then

   -- On renvoie la distance AM
   D := norme_v2(A,M);
   return D;

  elsif ((mode = C_segment_type) and then (lambda >= D2)) then

   -- On renvoie la distance BM
   D := norme_v2(B,M);
   return D;

  else
   
   -- Calcul du point projete 
   mX := lambda*unitaireX + float(A.Coor_x);
   mY := lambda*unitaireY + float(A.Coor_y);
   projete := (Coor_x => integer(mX),Coor_y => integer(mY));
   D := norme(float(M.Coor_x),float(M.Coor_y),mX,mY);
   return D;

  end if;
 end if;

end distance_a_ligne;


-- ------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type; 
			  A : in Point_type; 
                    	  B : in Point_type; 
		       Mode : Distance_type) return integer is

D2,D 		: float;
unitaireX,
unitaireY 	: float;
lambda 		: float;
mX,mY 		: float;
projete 	: Point_type;

begin -- distance_a_ligne

 -- Norme du vecteur AB
 D2 := norme_v2(A,B);

 if (D2 = 0.0) then 

  -- A et B sont confondus  
  D := norme_v2(A,M);
  return integer(D+0.49);

 else
 
  -- Calcul du vecteur unitaire de support AB
  unitaireX := float(B.Coor_x - A.Coor_x) / D2;
  unitaireY := float(B.Coor_y - A.Coor_y) / D2;

  -- Calcul du projete de M sur AB = Produit scalaire AM AB
  lambda := float(M.Coor_x - A.Coor_x)*unitaireX + float(M.Coor_y - A.Coor_y)*unitaireY; 
  
  -- Calcul du point projete
  if ((mode = C_segment_type) and then (lambda <= 0.0)) then

   -- On renvoie la distance AM
   D := norme_v2(A,M);
   return integer(D+0.49);

  elsif ((mode = C_segment_type) and then (lambda >= D2)) then

   -- On renvoie la distance BM
   D := norme_v2(B,M);
   return integer(D+0.49);

  else
   
   -- Calcul du point projete 
   mX := lambda*unitaireX + float(A.Coor_x);
   mY := lambda*unitaireY + float(A.Coor_y);
   projete := (Coor_x => integer(mX),Coor_y => integer(mY));
   D := norme(float(M.Coor_x),float(M.Coor_y),mX,mY);
   return integer(D+0.49);

  end if;
 end if;

end distance_a_ligne;


-- ------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type_reel; 
			  A : in Point_type_reel; 
                    	  B : in Point_type_reel; 
		       Mode : Distance_type) return float is

D2,D 		: float;
unitaireX,
unitaireY 	: float;
lambda 		: float;
mX,mY 		: float;
projete 	: Point_type_reel;

begin -- distance_a_ligne

 -- Norme du vecteur AB
 D2 := norme(A,B);

 if (D2 = 0.0) then 

  -- A et B sont confondus  
  D := norme(A,M);
  return D;

 else
 
  -- Calcul du vecteur unitaire de support AB
  unitaireX := (B.Coor_x - A.Coor_x) / D2;
  unitaireY := (B.Coor_y - A.Coor_y) / D2;

  -- Calcul du projete de M sur AB = Produit scalaire AM AB
  lambda := (M.Coor_x - A.Coor_x)*unitaireX + (M.Coor_y - A.Coor_y)*unitaireY; 
  
  -- Calcul du point projete
  if ((mode = C_segment_type) and then (lambda <= 0.0)) then

   -- On renvoie la distance AM
   D := norme(A,M);
   return D;

  elsif ((mode = C_segment_type) and then (lambda >= D2)) then

   -- On renvoie la distance BM
   D := norme(B,M);
   return D;

  else
   
   -- Calcul du point projete 
   mX := lambda*unitaireX + (A.Coor_x);
   mY := lambda*unitaireY + (A.Coor_y);
   projete := (Coor_x => mX,Coor_y => mY);
   D := norme(M.Coor_x,M.Coor_y,mX,mY);
   return D;

  end if;
 end if;

end distance_a_ligne;


-- ------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type_reel; 
			  A : in Point_type_reel; 
                    	  B : in Point_type_reel; 
		       Mode : Distance_type) return integer is

D2,D 		: float;
unitaireX,
unitaireY 	: float;
lambda 		: float;
mX,mY 		: float;
projete 	: Point_type_reel;

begin -- distance_a_ligne

 -- Norme du vecteur AB
 D2 := norme(A,B);

 if (D2 = 0.0) then 

  -- A et B sont confondus  
  D := norme(A,M);
  return integer(D+0.49);

 else
 
  -- Calcul du vecteur unitaire de support AB
  unitaireX := (B.Coor_x - A.Coor_x) / D2;
  unitaireY := (B.Coor_y - A.Coor_y) / D2;

  -- Calcul du projete de M sur AB = Produit scalaire AM AB
  lambda := (M.Coor_x - A.Coor_x)*unitaireX + (M.Coor_y - A.Coor_y)*unitaireY; 
  
  -- Calcul du point projete
  if ((mode = C_segment_type) and then (lambda <= 0.0)) then

   -- On renvoie la distance AM
   D := norme(A,M);
   return integer(D+0.49);

  elsif ((mode = C_segment_type) and then (lambda >= D2)) then

   -- On renvoie la distance BM
   D := norme(B,M);
   return integer(D+0.49);

  else
   
   -- Calcul du point projete 
   mX := lambda*unitaireX + (A.Coor_x);
   mY := lambda*unitaireY + (A.Coor_y);
   projete := (Coor_x => mX,Coor_y => mY);
   D := norme(M.Coor_x,M.Coor_y,mX,mY);
   return integer(D+0.49);

  end if;
 end if;

end distance_a_ligne;


-- ------------------------------------------------------------------------
Function Distance_a_polylignes( M 	: in Point_type; 
				PL 	: in Point_liste_type;
				taille	: in natural; 
				Mode 	: in Distance_type) return float is

dist,distmin : float; -- distances du point M a un des segments de la ligne

begin 		      -- Distance_a_polylignes

 if ( taille = 0 ) then 
  return 0.0;
 end if;

 if ( taille = 1) then 
  return norme(PL(1),M);
 end if;

 For I in 2..taille loop

  dist := distance_a_ligne(M,PL(I-1),PL(I),Mode);
  if ((I = 2) or else (dist < distmin)) then       --- bizarre...
   distmin := dist;
  end if;
 
 end loop;

 return distmin;
end Distance_a_polylignes;




Function Distance_a_polylignes_v2( M : in Point_type; 
				                 PL : in Point_liste_type;
								 Indice_ini : natural;
				                 taille : in natural; 
				                 Mode : in Distance_type) return float is

dist,distmin : float; -- distances du point M a un des segments de la ligne

begin 		      -- Distance_a_polylignes

 if ( taille = 0 ) then 
  return 0.0;
 end if;

 if ( taille = 1) then 
  return norme(PL(1+Indice_ini),M);
 end if;

 For I in 2..taille loop

  dist := distance_a_ligne(M,PL(I-2+Indice_ini),PL(I-1+Indice_ini),Mode);
  if ((I = 2) or (dist < distmin)) then
   distmin := dist;
  end if;
 
 end loop;

 return distmin;
end Distance_a_polylignes_v2;




-- ------------------------------------------------------------------------
Function Distance_a_polylignes(	M 	: in Point_type; 
				PL	: in Point_liste_type;
				taille	: in natural; 
				Mode 	: in Distance_type) return integer is
dist,distmin : float; 	-- distances du point M a un des segments de la ligne

begin 			-- Distance_a_polylignes

 if ( taille = 0 ) then 
  return 0;
 end if;

 if ( taille = 1) then 
  dist := norme_v2(PL(1),M);
  return integer(dist+0.49);
 end if;

 For I in 2..taille loop

  dist := distance_a_ligne(M,PL(I-1),PL(I),Mode);
  if ((I = 2) or else (dist < distmin)) then
   distmin := dist;
  end if;
 
 end loop;

 return integer(distmin+0.49);
end Distance_a_polylignes;

-- ------------------------------------------------------------------------
Function Distance_a_polylignes( M 	: in Point_type_reel; 
				PL 	: in Point_liste_reel;
				taille	: in natural; 
				Mode 	: in Distance_type) return float is

dist,distmin : float; -- distances du point M a un des segments de la ligne

begin 		      -- Distance_a_polylignes

 if ( taille = 0 ) then 
  return 0.0;
 end if;

 if ( taille = 1) then 
  return norme(PL(1),M);
 end if;

 For I in 2..taille loop

  dist := distance_a_ligne(M,PL(I-1),PL(I),Mode);
  if ((I = 2) or else (dist < distmin)) then
   distmin := dist;
  end if;
 
 end loop;

 return distmin;
end Distance_a_polylignes;

-- ------------------------------------------------------------------------
Function Angle_3points(A : in Point_type; B : in Point_type; C : in Point_type) return angle_type is 
D1,D2 : float; 
cosinus : float;
pv : float;
BAx,BAy,BCx,BCy : float;

begin -- Angle_3points
 
 -- Calcul des vecteurs unitaires 
 D1 := norme_v2(B,A);
 D2 := norme_v2(B,C);
 
 if ((D1 = 0.0) or else (D2 = 0.0)) 
  then return 0.0;
 end if;

 BAx := float(A.Coor_x-B.Coor_x) / D1; BAy := float(A.Coor_y-B.Coor_y) /D1;
 BCx := float(C.Coor_x-B.Coor_x) / D2; BCy := float(C.Coor_y-B.Coor_y) /D2;
 cosinus := BAx*BCx + BAy*BCy;

 -- Correction 02/04/96 : plantage si suite a des problemes d'arrondi,
 -- la valeur est <--1  >1 
 if cosinus < -1.0 then
    cosinus := -1.0 ;
 elsif 1.0 < cosinus then
    cosinus := 1.0 ;
 end if ;

 pv := BAx*BCy - BAy*BCx; -- Calcul du determinant 
 if (pv < 0.0) 
 
 -- modif du 21/06/99
  --then  return angle_type(-ACOSD(cosinus));
	 then  return angle_type(-ArcCOS(cosinus)*180.0/pi);
  --else  return angle_type(ACOSD(cosinus));
	 else  return angle_type(ArcCOS(cosinus)*180.0/pi);
 end if;

end Angle_3points;

-- ------------------------------------------------------------------------
Function Angle_Horizontal(A : in Point_type; B : in Point_type) return angle_type is
D1,ABx,cosinus : float;

begin -- Angle_Horizontal

 -- Calcul du vecteur unitaire
 D1 := norme_v2(A,B);                    
 if (D1 = 0.0) 
  then return 0.0;
 end if;

 ABx := float(B.Coor_x-A.Coor_x) / D1; 
 cosinus := ABx;
 if (A.Coor_y > B.Coor_y) 
  --then return angle_type(-ACOSD(cosinus));
  --else return angle_type(ACOSD(cosinus));
	 then return angle_type(-ArcCOS(cosinus)*180.0/pi);
	 else return angle_type(ArcCOS(cosinus)*180.0/pi);
 end if;

end Angle_Horizontal;


--***************************************************************
-- Angle_horizontal
--***************************************************************
-- Entree : A et B, point_type_reel
-- Sortie : l'angle du vecteur AB par rapport a l'horizontale,
-- en radians (entre -pi et pi)

Function Angle_horizontal(A,B	: in Point_type_reel) return Float is

D1,cosinus	: Float; 

Begin
  -- Calcul vecteur unitaire
  D1 := norme(A,B);
  If (D1 = 0.0) then 
    return 0.0;
  End If;

  cosinus := (B.Coor_x - A.Coor_x) / D1;
  If (A.Coor_y > B.Coor_y) then
    return(-ArcCOS(cosinus));
  Else
    return ArcCOS(cosinus);
  End If;
End Angle_horizontal;

-- ------------------------------------------------------------------------
Function Angle_3points(A : in Point_type; B : in Point_type; C : in Point_type) return float is 
D1,D2 : float; 
cosinus : float;
pv : float;
BAx,BAy,BCx,BCy : float;

begin -- Angle_3points
 
 -- Calcul des vecteurs unitaires 
 D1 := norme_v2(B,A);
 D2 := norme_v2(B,C);
 
 if ((D1 = 0.0) or else (D2 = 0.0)) 
  then return 0.0;
 end if;

 BAx := float(A.Coor_x-B.Coor_x) / D1; BAy := float(A.Coor_y-B.Coor_y) /D1;
 BCx := float(C.Coor_x-B.Coor_x) / D2; BCy := float(C.Coor_y-B.Coor_y) /D2;
 cosinus := BAx*BCx + BAy*BCy;

 -- NB : la fonction ACOS plante pour les valeurs 1.0 et -1.0 !!!
 if (cosinus >= 1.0) then
   return 0.0;
 elsif (cosinus <= -1.0) then
   return 3.14159265;
 end if;

 pv := BAx*BCy - BAy*BCx; -- Calcul du determinant 
 if (pv < 0.0) then  
   return (-ArcCOS(cosinus));
 else  
   return ArcCOS(cosinus);
 end if;

end Angle_3points;

-- ------------------------------------------------------------------------
Function Angle_Horizontal(A : in Point_type; B : in Point_type) return float is
D1,ABx,cosinus : float;

begin -- Angle_Horizontal

 -- Calcul du vecteur unitaire
 D1 := norme_v2(A,B);                    
 if (D1 = 0.0) 
  then return 0.0;
 end if;

 ABx := float(B.Coor_x-A.Coor_x) / D1;
 cosinus := ABx;
 if (A.Coor_y > B.Coor_y) 
  then return (-ArcCOS(cosinus));
  else return ArcCOS(cosinus);
 end if;

end Angle_Horizontal;


----------------------------------------------
-- ANGLE_3POINTS sur des reels 		    --

Function Angle_3points(A, B, C : Point_type_reel) return float is 
D1,D2 : float; 
cosinus : float;
pv : float;
BAx,BAy,BCx,BCy : float;

begin -- Angle_3points
 
 -- Calcul des vecteurs unitaires 
 D1 := norme(B,A);
 D2 := norme(B,C);
 
 if ((D1 = 0.0) or else (D2 = 0.0)) 
  then return 0.0;
 end if;

 BAx := (A.Coor_x-B.Coor_x) / D1; BAy := (A.Coor_y-B.Coor_y) /D1;
 BCx := (C.Coor_x-B.Coor_x) / D2; BCy := (C.Coor_y-B.Coor_y) /D2;
 cosinus := BAx*BCx + BAy*BCy;

 -- NB : la fonction ACOS plante pour les valeurs 1.0 et -1.0 !!!
 if (cosinus >= 1.0) then
   return 0.0;
 elsif (cosinus <= -1.0) then
   return pi;
 end if;

 pv := BAx*BCy - BAy*BCx; -- Calcul du determinant 
 if (pv < 0.0) then  
   return (-ArcCOS(cosinus));
 else  
   return ArcCOS(cosinus);
 end if;

end Angle_3points;  




-- ------------------------------------------------------------------------
Function p_vectoriel(M,O,N : in Point_type) return float is

DX1,DY1,DX2,DY2 : float;
res,l1,l2  : float;
begin -- p_vectoriel
 DX1 := float(M.Coor_x - O.Coor_x);
 DY1 := float(M.Coor_y - O.Coor_y);
 DX2 := float(N.Coor_x - O.Coor_x);
 DY2 := float(N.Coor_y - O.Coor_y);
 res := DX1*DY2 - DY1*DX2;
 l1 := sqrt(DX1*DX1 + DY1*DY1);
 l2 := sqrt(DX2*DX2 + DY2*DY2);
 res := res / l1/l2*(l1+l2);
 return res; 
end p_vectoriel;

-- ------------------------------------------------------------------------
Function p_vectoriel(M,O,N : in Point_type_reel) return float is

DX1,DY1,DX2,DY2 : float;
res,l1,l2  : float;
begin -- p_vectoriel
 DX1 := M.Coor_x - O.Coor_x;
 DY1 := M.Coor_y - O.Coor_y;
 DX2 := N.Coor_x - O.Coor_x;
 DY2 := N.Coor_y - O.Coor_y;
 res := DX1*DY2 - DY1*DX2;
 l1 := sqrt(DX1*DX1 + DY1*DY1);
 l2 := sqrt(DX2*DX2 + DY2*DY2);
 res := res / (l1*l2*(l1+l2));
 return res; 
end p_vectoriel;

-- ------------------------------------------------------------------------
Function p_vectoriel_polylignes(ligne : in Point_liste_type; N : natural) 
						return reel_tableau_type is 
lres : reel_tableau_type(1..N-2);
borne_sup : natural;

begin -- p_vectoriel_polylignes

 borne_sup := N-2;

 if (N < 3) 
  then return lres;
 end if;

 For I in 1..borne_sup loop
  lres(I) := p_vectoriel(ligne(I),ligne(I+1),ligne(I+2));
 end loop;
 
 return lres;
end p_vectoriel_polylignes;

-- ------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 Flags : out boolean; M : out Point_type) is

D1,N1,N2 : float;
T : float;
s : float;
AF : Point_type_reel:=(float(A.coor_x),float(A.coor_y));
BF : Point_type_reel:=(float(B.coor_x),float(B.coor_y));
CF : Point_type_reel:=(float(C.coor_x),float(C.coor_y));
DF : Point_type_reel:=(float(D.coor_x),float(D.coor_y));

begin

   -- calcul de t1
   D1:=(bF.coor_x-aF.coor_x)*(dF.coor_y-cF.coor_y) - (bF.coor_y-aF.coor_y)*(dF.coor_x-cF.coor_x);
   if d1=0.0 then      -- Droite parallele;
      Flags :=False;
      return;
   end if;
   S:=d1/abs(d1);

   N1:=(cF.coor_x-aF.coor_x)*(dF.coor_y-cF.coor_y) - (cF.coor_y-aF.coor_y)*(dF.coor_x-cF.coor_x);
   
   if Mode=c_segment_type then
      if s*n1 <0.0 or else abs(n1)>abs(d1) then
         flags :=false;
         return;
      end if;
      N2:=(aF.coor_x-cF.coor_x)*(bF.coor_y-aF.coor_y) - (aF.coor_y-cF.coor_y)*(bF.coor_x-aF.coor_x);
      if s*n2 >0.0 or else abs(n2) > abs(d1) then 
         flags :=false;
         return;
      end if;
   end if;
   Flags:=true;
   T:=float(n1)/float(d1);
   M.coor_x:=integer(((bF.coor_x-aF.coor_x)*t)+aF.coor_x);
   M.coor_y:=integer(((bF.coor_y-aF.coor_y)*t)+aF.coor_y);

end;


--Procedure Test_Intersection_de_segments(A,B,C,D : Point_type; Flags : out boolean) is
--
--D1,N1,N2 : float;
--T : float;
--s : float;
--AF : Point_type_reel:=(float(A.coor_x),float(A.coor_y));
--BF : Point_type_reel:=(float(B.coor_x),float(B.coor_y));
--CF : Point_type_reel:=(float(C.coor_x),float(C.coor_y));
--DF : Point_type_reel:=(float(D.coor_x),float(D.coor_y));
--
--begin
--
--   -- condition preliminaire
--   if min(A.Coor_X,B.Coor_x)>max(C.Coor_X,D.Coor_X)
--   or min(C.Coor_X,D.Coor_x)>max(A.Coor_X,B.Coor_X)
--   or min(A.Coor_Y,B.Coor_Y)>max(C.Coor_Y,D.Coor_Y)
--   or min(C.Coor_Y,D.Coor_Y)>max(A.Coor_Y,B.Coor_Y) then
--     Flags :=False;
--     return;
--   end if;
--
--   -- calcul de t1
--   D1:=(bF.coor_x-aF.coor_x)*(dF.coor_y-cF.coor_y) - (bF.coor_y-aF.coor_y)*(dF.coor_x-cF.coor_x);
--   if d1=0.0 then      -- Droite parallele;
--      Flags :=False;
--      return;
--   end if;
--   S:=d1/abs(d1);
--
--   N1:=(cF.coor_x-aF.coor_x)*(dF.coor_y-cF.coor_y) - (cF.coor_y-aF.coor_y)*(dF.coor_x-cF.coor_x);
--   
--   if s*n1 <0.0 or else abs(n1)>abs(d1) then
--     flags :=false;
--     return;
--   end if;
--   N2:=(aF.coor_x-cF.coor_x)*(bF.coor_y-aF.coor_y) - (aF.coor_y-cF.coor_y)*(bF.coor_x-aF.coor_x);
--   if s*n2 >0.0 or else abs(n2) > abs(d1) then 
--     flags :=false;
--     return;
--   end if;
--   Flags:=true;
--
--end;


-- ------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 Flags : out boolean; M : out Point_type_reel) is

D1,N1,N2 : integer;
T : float;
s : Integer;
begin
   -- calcul de t1
   D1:=(b.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (b.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   if d1=0 then      -- Droite parallele;
      Flags :=False;
      return;
   end if;
   S:=d1/abs(d1);

   N1:=(c.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (c.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   
   if Mode=c_segment_type then
      if s*n1 <0 or else abs(n1)>abs(d1) then
         flags :=false;
         return;
      end if;
      N2:=(a.coor_x-c.coor_x)*(b.coor_y-a.coor_y) - 
          (a.coor_y-c.coor_y)*(b.coor_x-a.coor_x);
      if s*n2 >0 or else abs(n2) > abs(d1) then 
         flags :=false;
         return;
      end if;
   end if;
   Flags:=true;
   T:=float(n1)/float(d1);
   M.coor_x:=float(b.coor_x-a.coor_x)*t + float(a.coor_x);
   M.coor_y:=float(b.coor_y-a.coor_y)*t + float(a.coor_y);
end;

-----------------------------------------------------------------------------------
--		 INTERSECTIONS DE LIGNES EN COORD EN FLOAT                       --
-----------------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type_reel;
				 Mode : Distance_type;
                                 Flags : out boolean;
				 M : out Point_type_reel) is

D1,N1,N2 : float;
T : float;
s : float;
begin
   -- calcul de t1
   D1:=(b.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (b.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   if d1=0.0 then      -- Droite parallele;
      Flags :=False;
      return;
   end if;
   S:=d1/abs(d1);

   N1:=(c.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (c.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   
   if Mode=c_segment_type then
      if s*n1 <0.0 or else abs(n1)>abs(d1) then
         flags :=false;
         return;
      end if;
      N2:=(a.coor_x-c.coor_x)*(b.coor_y-a.coor_y) - 
          (a.coor_y-c.coor_y)*(b.coor_x-a.coor_x);
      if s*n2 > 0.0 or else abs(n2) > abs(d1) then 
         flags :=false;
         return;
      end if;
   end if;
   Flags:=true;
   T:=n1/d1;
   M.coor_x:=(b.coor_x-a.coor_x)*t + a.coor_x;
   M.coor_y:=(b.coor_y-a.coor_y)*t + a.coor_y;
end;

-- ------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 N : out natural; lres : in out Point_Liste_type) is
D1,N1,N2 : integer;
T : float;
s : Integer;
count : natural := 0;
maxx,minx,maxy,miny : integer;

begin
   -- calcul de t1
   D1:=(b.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (b.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   if d1=0 then      -- Droite parallele;
     -- Il reste a determiner si les deux segments sont reellement confondus
     
     if ((distance_a_ligne(C,A,B,C_ligne_type) /= 0.0) and then (distance_a_ligne(D,A,B,C_ligne_type) /= 0.0)) then 
        N := 0;
        return; -- les segments sont strictement paralleles
     else -- les segments sont confondus
        if (C.coor_X /=  D.coor_X) then  -- les segments ne sont pas "verticaux"
          if (C.Coor_x >= D.Coor_x) then	
              maxx := C.Coor_x; minx := D.Coor_x; 
          else 
              maxx := D.Coor_x;minx := C.Coor_x;
          end if;
          if (A.Coor_x >= minx and then A.Coor_x <= maxx) then 
              -- A est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := A.Coor_x;lres(count).Coor_y := A.Coor_y;
          end if;
          if (B.Coor_x >= minx and then B.Coor_x <=maxx) then 
              -- B est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := B.Coor_x;lres(count).Coor_y := B.Coor_y;
          end if;
      
          if (A.Coor_x >= B.Coor_x) then 
      	    maxx := A.Coor_x; minx := B.Coor_x; 
          else
            maxx := B.Coor_x;minx := A.Coor_x;
          end if;
          if (C.Coor_x > minx and then C.Coor_x < maxx) then 
              -- C est-il entre A et B ?
              count:=count+1;
              lres(count).Coor_x := C.Coor_x;
              lres(count).Coor_y := C.Coor_y;
          end if;
          if (D.Coor_x > minx and then D.Coor_x < maxx) then 
              -- D est-il entre A et B ?
              count:=count+1;
              lres(count).Coor_x := D.Coor_x;
              lres(count).Coor_y := D.Coor_y;      
          end if;
          n := count;
          return;
        else  -- les segments sont "verticaux"
          if (C.Coor_y >= D.Coor_y) then	maxy := C.Coor_y; miny := D.Coor_y; 
          else maxy := D.Coor_y;miny := C.Coor_y;
          end if;
          if (A.Coor_y >= miny and then A.Coor_y <= maxy) then -- A est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := A.Coor_x;lres(count).Coor_y := A.Coor_y;
          end if;
          if (B.Coor_y >= miny and then B.Coor_y <= maxy) then -- B est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := B.Coor_x;lres(count).Coor_y := B.Coor_y;
          end if;
      
          if (A.Coor_y >= B.Coor_y) then 
      	    maxy := A.Coor_y; miny := B.Coor_y; 
          else maxy := B.Coor_y;miny := A.Coor_y;
          end if;
          if (C.Coor_y > miny and then C.Coor_y < maxy) then -- C est-il strictement entre A et B?
           count:=count+1;
           lres(count).Coor_x := C.Coor_x;
           lres(count).Coor_y := C.Coor_y;
          end if;
          if (D.Coor_y > miny and then D.Coor_y < maxy) then -- D est-il strictement entre A et B?
           count:=count+1;
           lres(count).Coor_x := D.Coor_x;
           lres(count).Coor_y := D.Coor_y;      
          end if;
          n := count;
          return;
         end if;
       end if;
     end if;

   S:=d1/abs(d1);

   N1:=(c.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (c.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   
   if Mode=c_segment_type then
      if s*n1 <0 or else abs(n1)>abs(d1) then
         N := 0;
         return;
      end if;
      N2:=(a.coor_x-c.coor_x)*(b.coor_y-a.coor_y) - 
          (a.coor_y-c.coor_y)*(b.coor_x-a.coor_x);
      if s*n2 >0 or else abs(n2) > abs(d1) then 
         N := 0;
         return;
      end if;
   end if;
   N := 1;
   T:=float(n1)/float(d1);
   lres(1).Coor_x := integer(float(b.coor_x-a.coor_x)*t) + a.coor_x;
   lres(1).Coor_y := integer(float(b.coor_y-a.coor_y)*t) + a.coor_y;
end;

-- ------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 N : out natural; lres : in out Point_Liste_reel) is
D1,N1,N2 : integer;
T : float;
s : Integer;
count : natural := 0;
maxx,minx,maxy, miny : integer;

begin
   -- calcul de t1
   D1:=(b.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (b.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   if d1=0 then      -- Droite parallele;
     -- Il reste a determiner si les deux segments sont reellement confondus
     
     if ((distance_a_ligne(C,A,B,C_ligne_type) /= 0.0) and then (distance_a_ligne(D,A,B,C_ligne_type) /= 0.0)) then 
       N := 0;
       return; -- les segments sont strictement paralleles
     else -- les segments sont confondus
        if (C.coor_X /=  D.coor_X) then  -- les segments ne sont pas "verticaux"
          if (C.Coor_x >= D.Coor_x) then	maxx := C.Coor_x; minx := D.Coor_x; 
          else maxx := D.Coor_x;minx := C.Coor_x;
          end if;
          if (A.Coor_x >= minx and then A.Coor_x <= maxx) then -- A est-il entre C et D ?
           count := count + 1;
           lres(count).Coor_x := float(A.Coor_x);
           lres(count).Coor_y := float(A.Coor_y);
          end if;
          if (B.Coor_x >= minx and then B.Coor_x <=maxx) then -- B est-il entre C et D ?
           count := count + 1;
           lres(count).Coor_x := float(B.Coor_x);
           lres(count).Coor_y := float(B.Coor_y);
          end if;
      
          if (A.Coor_x >= B.Coor_x) then 
      	    maxx := A.Coor_x; minx := B.Coor_x; 
          else maxx := B.Coor_x;minx := A.Coor_x;
          end if;
          if (C.Coor_x > minx and then C.Coor_x < maxx) then -- C est-il strictement entre A et B ?
           count:=count+1;
           lres(count).Coor_x := float(C.Coor_x);
           lres(count).Coor_y := float(C.Coor_y);
          end if;
          if (D.Coor_x > minx and then D.Coor_x < maxx) then -- D est-il strictement entre A et B ?
           count:=count+1;
           lres(count).Coor_x := float(D.Coor_x);
           lres(count).Coor_y := float(D.Coor_y);      
          end if;
          n := count;
          return;
        else  -- les segments sont "verticaux"
          if (C.Coor_y >= D.Coor_y) then	maxy := C.Coor_y; miny := D.Coor_y; 
          else maxy := D.Coor_y;miny := C.Coor_y;
          end if;
          if (A.Coor_y >= miny and then A.Coor_y <= maxy) then -- A est-il entre C et D ?
           count := count + 1;
           lres(count).Coor_x := float(A.Coor_x);
           lres(count).Coor_y := float(A.Coor_y);
          end if;
          if (B.Coor_y >= miny and then B.Coor_y <= maxy) then -- B est-il entre C et D ?
           count := count + 1;
           lres(count).Coor_x := float(B.Coor_x);
           lres(count).Coor_y := float(B.Coor_y);
          end if;
      
          if (A.Coor_y >= B.Coor_y) then 
      	    maxy := A.Coor_y; miny := B.Coor_y; 
          else maxy := B.Coor_y;miny := A.Coor_y;
          end if;
          if (C.Coor_y > miny and then C.Coor_y < maxy) then -- C est-il strictement entre A et B?
           count:=count+1;
           lres(count).Coor_x := float(C.Coor_x);
           lres(count).Coor_y := float(C.Coor_y);
          end if;
          if (D.Coor_y > miny and then D.Coor_y < maxy) then -- D est-il strictement entre A et B?
            count:=count+1;
            lres(count).Coor_x := float(D.Coor_x);
            lres(count).Coor_y := float(D.Coor_y);      
          end if;
          n := count;
          return;
         end if;
       end if;
     end if;

   S:=d1/abs(d1);

   N1:=(c.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (c.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   
   if Mode=c_segment_type then
      if s*n1 <0 or else abs(n1)>abs(d1) then
         N := 0;
         return;
      end if;
      N2:=(a.coor_x-c.coor_x)*(b.coor_y-a.coor_y) - 
          (a.coor_y-c.coor_y)*(b.coor_x-a.coor_x);
      if s*n2 >0 or else abs(n2) > abs(d1) then 
         N := 0;
         return;
      end if;
   end if;
   N := 1;
   T:=float(n1)/float(d1);
   lres(1).Coor_x := float(b.coor_x-a.coor_x)*t + float(a.coor_x);
   lres(1).Coor_y := float(b.coor_y-a.coor_y)*t + float(a.coor_y);
end;

-----------------------------------------------------------------------------------
--		 INTERSECTIONS DE LIGNES EN COORD EN FLOAT               	   --
-- cette procedure est une adaptation de celle du package GEOMETRIE              --
-----------------------------------------------------------------------------------

  Procedure Intersection_de_lignes(A,B,C,D : Point_type_reel; 
				   Mode : Distance_type;
                                   Ni : out natural; 
				   lres : in out Point_Liste_reel) is

  N1,N2 : float;
  D1, T : float;
  s : float;
  count : natural := 0;
  maxx,minx, maxy, miny : float;

  begin
   -- calcul de t1
     D1:=(b.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (b.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
     if d1=0.0 then      -- Droite parallele;
       -- Il reste a determiner si les deux segments sont reellement confondus
     
       if ((distance_a_ligne(C,A,B,C_ligne_type) /= 0.0) and then (distance_a_ligne(D,A,B,C_ligne_type) /= 0.0)) then 
        ni := 0;
        return; -- les segments sont strictement paralleles
       else -- les segments sont confondus
        if (C.coor_X /=  D.coor_X) then  -- les segments ne sont pas "verticaux"
          if (C.Coor_x >= D.Coor_x) then	maxx := C.Coor_x; minx := D.Coor_x; 
          else maxx := D.Coor_x;minx := C.Coor_x;
          end if;
          if (A.Coor_x >= minx and then A.Coor_x <= maxx) then -- A est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := A.Coor_x;lres(count).Coor_y := A.Coor_y;
          end if;
          if (B.Coor_x >= minx and then B.Coor_x <=maxx) then -- B est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := B.Coor_x;lres(count).Coor_y := B.Coor_y;
          end if;
      
          if (A.Coor_x >= B.Coor_x) then 
      	    maxx := A.Coor_x; minx := B.Coor_x; 
          else maxx := B.Coor_x;minx := A.Coor_x;
          end if;
          if (C.Coor_x > minx and then C.Coor_x < maxx) then -- C est-il entre A et B ?
           if (count < 2) then count := count + 1; end if;
           lres(count).Coor_x := C.Coor_x;
           lres(count).Coor_y := C.Coor_y;
          end if;
          if (D.Coor_x > minx and then D.Coor_x < maxx) then -- D est-il entre A et B ?
           if (count < 2) then count := count + 1; end if;
           lres(count).Coor_x := D.Coor_x;
           lres(count).Coor_y := D.Coor_y;      
          end if;
          ni := count;
          return;
        else  -- les segments sont "verticaux"
          if (C.Coor_y >= D.Coor_y) then	maxy := C.Coor_y; miny := D.Coor_y; 
          else maxy := D.Coor_y;miny := C.Coor_y;
          end if;
          if (A.Coor_y >= miny and then A.Coor_y <= maxy) then -- A est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := A.Coor_x;lres(count).Coor_y := A.Coor_y;
          end if;
          if (B.Coor_y >= miny and then B.Coor_y <= maxy) then -- B est-il entre C et D ?
           count := count + 1;lres(count).Coor_x := B.Coor_x;lres(count).Coor_y := B.Coor_y;
          end if;
      
          if (A.Coor_y >= B.Coor_y) then 
      	    maxy := A.Coor_y; miny := B.Coor_y; 
          else maxy := B.Coor_y;miny := A.Coor_y;
          end if;
          if (C.Coor_y > miny and then C.Coor_y < maxy) then -- C est-il strictement entre A et B?
           if (count < 2) then count := count + 1; end if;
           lres(count).Coor_x := C.Coor_x;
           lres(count).Coor_y := C.Coor_y;
          end if;
          if (D.Coor_y > miny and then D.Coor_y < maxy) then -- D est-il strictement entre A et B?
           if (count < 2) then count := count + 1; end if;
           lres(count).Coor_x := D.Coor_x;
           lres(count).Coor_y := D.Coor_y;      
          end if;
          ni := count;
          return;
         end if;
       end if;
     end if;

     S:=d1/abs(d1);

     N1:=(c.coor_x-a.coor_x)*(d.coor_y-c.coor_y) - (c.coor_y-a.coor_y)*(d.coor_x-c.coor_x);
   
     if Mode=c_segment_type then
        if s*n1 <0.0 or else abs(n1)>abs(d1) then
           Ni := 0;
           return;
        end if;
        N2:=(a.coor_x-c.coor_x)*(b.coor_y-a.coor_y) - 
            (a.coor_y-c.coor_y)*(b.coor_x-a.coor_x);
        if s*n2 >0.0 or else abs(n2) > abs(d1) then 
           Ni := 0;
           return;
        end if;
     end if;
     Ni := 1;
     T:=n1/d1;
     lres(1).Coor_x := (b.coor_x-a.coor_x)*t + a.coor_x;
     lres(1).Coor_y := (b.coor_y-a.coor_y)*t + a.coor_y;
  end intersection_de_lignes;

Procedure Intersections_de_polylignes(  L1,L2 : in Point_liste_type; 
					N1,N2 : in natural; 
					Lres : in out Point_Access_type; 
					N : out integer ) is
M : Point_type;
flag : boolean;
count : natural := 0;
temp : Point_liste_type(1..((N1-1)*(N2-1)));
cpt : integer := 0;

begin -- Intersection_de_polylignes
 
 For I in 2..N1 loop
  For J in 2..N2 loop

   Intersection_de_lignes(L1(I-1),L1(I),L2(J-1),L2(J),C_segment_type,flag,M);  

   If (flag) then 
    count := count + 1;
    temp(count) := M;    
    flag := false;
   end if;

  end loop;
 end loop;

 if (count > 0) then 
 
  Lres := new Point_liste_type(1..count);
  cpt := 1;
  Lres(1) := temp(1);

  if (count > 1) then  
   
   For I in 2..count loop
    if ((temp(I-1).Coor_x /= temp(I).Coor_x) or else (temp(I-1).Coor_y /= temp(I).Coor_y)) then
     cpt := cpt + 1;
     Lres(cpt) := temp(I);
    end if; 
   end loop;

  else
   null;
 end if;

 end if;

 N := cpt;

end Intersections_de_polylignes;

-- ------------------------------------------------------------------------

-- ------------------------------------------------------------------------
Procedure Intersections_de_polylignes(  L1,L2 : in Point_liste_type; 
					N1,N2 : in natural; 
					Lres : in out Point_Access_type; 
					N : out integer;
					Option : in boolean) is
M : Point_type;
flag : boolean;
count : natural := 0;
temp : Point_liste_type(1..((N1-1)*(N2-1)));
cpt : integer := 0;

begin -- Intersection_de_polylignes
 
 For I in 2..N1 loop
  For J in 2..N2 loop

   Intersection_de_lignes(L1(I-1),L1(I),L2(J-1),L2(J),C_segment_type,flag,M);  

   -- debut ajout 25.04.2005
   -- on ne considere pas les intersections qui se confondent avec une extrémité de L1
   if (option=true) then
   	 if (i=2 and M=L1(1)) or (i=n1 and m=L1(n1)) then
  	   flag:=false;
     end if;
   end if;
   -- fin ajout 25.04.2005

   If (flag) then 
    count := count + 1;
    temp(count) := M;    
    flag := false;
   end if;

  end loop;
 end loop;

 if (count > 0) then 
 
  Lres := new Point_liste_type(1..count);
  cpt := 1;
  Lres(1) := temp(1);

  if (count > 1) then  
   
   For I in 2..count loop
    if ((temp(I-1).Coor_x /= temp(I).Coor_x) or else (temp(I-1).Coor_y /= temp(I).Coor_y)) then
     cpt := cpt + 1;
     Lres(cpt) := temp(I);
    end if; 
   end loop;

  else
   null;
 end if;

 end if;

 N := cpt;

end Intersections_de_polylignes;

-- ------------------------------------------------------------------------
Procedure Intersections_de_polylignes(L1,L2 : in Point_liste_type; N1,N2 : in natural; Lres : in out Point_Access_reel; N : out integer) is
M : Point_type_reel;
flag : boolean;
count : natural := 0;
temp : Point_liste_reel(1..((N1-1)*(N2-1)));
cpt : integer := 0; 

begin -- Intersection_de_polylignes

 For I in 2..N1 loop
  For J in 2..N2 loop

   Intersection_de_lignes(L1(I-1),L1(I),L2(J-1),L2(J),C_segment_type,flag,M);  

   If (flag) then 
    count := count + 1;
    temp(count) := M;    
    flag := false;
   end if;

  end loop;
 end loop;

 if (count > 0) then 
 
  Lres := new Point_liste_reel(1..count);
  cpt := 1;
  Lres(1) := temp(1);

  if (count > 1) then  
   
   For I in 2..count loop
    if ((temp(I-1).Coor_x /= temp(I).Coor_x) or else (temp(I-1).Coor_y /= temp(I).Coor_y)) then
     cpt := cpt + 1;
     Lres(cpt) := temp(I);
    end if; 
   end loop;

  else
   null;
 end if;

 end if;

 N := cpt;

end Intersections_de_polylignes;

-- ------------------------------------------------------------------------
Procedure Intersections_de_polylignes(L1,L2 : in Point_liste_type; N1,N2 : in natural; Lres : in out Point_Access_type; N : out integer; Seg_Conf : in out Point_Access_type; NConf : in out natural) is
M : Point_type;
t : natural;
count : natural := 0;
temp : Point_liste_type(1..((N1-1)*(N2-1))); -- Tableau temporaire destine a recevoir les points d'intersections
tempconf : Point_liste_type(1..((N1-1)*(N2-1))); -- Tableau temporaire destine a recevoir les portions communes
cpt : integer := 0;
lpoints : Point_liste_type(1..2);

begin -- Intersection_de_polylignes
 
 NConf := 0;
 

 For I in 2..N1 loop
  For J in 2..N2 loop

   Intersection_de_lignes(L1(I-1),L1(I),L2(J-1),L2(J),C_segment_type,t,lpoints);  

   If (t = 1) then -- Il y a 1 point d'intersection
    count := count + 1;
    temp(count) := lpoints(1);    
   elsif (t = 2) then -- les 2 segments sont confondus. On enregistre cela dans le tableau Seg_Conf
    NConf := NConf + 1; 
    tempconf(NConf) := lpoints(1);
    NConf := NConf + 1;
    tempconf(NConf) := lpoints(2);
   end if;

  end loop;
 end loop;

 Seg_Conf := new Point_liste_type(1..NConf);
 Seg_Conf(1..NConf) := tempconf(1..NConf);

 if (count > 0) then 
 
  Lres := new Point_liste_type(1..count);
  cpt := 1;
  Lres(1) := temp(1);

  if (count > 1) then  
   
   For I in 2..count loop
    if ((temp(I-1).Coor_x /= temp(I).Coor_x) or else (temp(I-1).Coor_y /= temp(I).Coor_y)) then
     cpt := cpt + 1;
     Lres(cpt) := temp(I);
    end if; 
   end loop;

  else
   null;
 end if;

 end if;

 N := cpt;

end Intersections_de_polylignes;

-- ------------------------------------------------------------------------
Procedure Intersections_de_polylignes(L1,L2 : in Point_liste_type; N1,N2 : in natural; Lres : in out Point_Access_reel; N : out integer; Seg_Conf : in out Point_Access_type; NConf : in out natural) is
M : Point_type_reel;
t : natural;
count : natural := 0;
temp : Point_liste_reel(1..((N1-1)*(N2-1)));
tempconf : Point_liste_type(1..((N1-1)*(N2-1)));
cpt : integer := 0; 
lpoints : Point_liste_reel(1..2);


begin -- Intersection_de_polylignes

 For I in 2..N1 loop
  For J in 2..N2 loop

   Intersection_de_lignes(L1(I-1),L1(I),L2(J-1),L2(J),C_segment_type,t,lpoints);  

   If (t = 1) then -- Il y a 1 point d'intersection
    count := count + 1;
    temp(count) := lpoints(1);
   elsif (t = 2) then -- les 2 segments sont confondus.
    NConf := NConf + 1;
    tempconf(NConf).Coor_x := integer(lpoints(1).Coor_x);
    tempconf(NConf).Coor_y := integer(lpoints(1).Coor_y);
    NConf := NConf + 1;
    tempconf(NConf).Coor_x := integer(lpoints(2).Coor_x);
    tempconf(NConf).Coor_y := integer(lpoints(2).Coor_y);
   end if;

  end loop;
 end loop;

 Seg_Conf := new Point_liste_type(1..NConf);
 Seg_Conf(1..NConf) := tempconf(1..NConf);

 if (count > 0) then 
 
  Lres := new Point_liste_reel(1..count);
  cpt := 1;
  Lres(1) := temp(1);

  if (count > 1) then  
   
   For I in 2..count loop
    if ((temp(I-1).Coor_x /= temp(I).Coor_x) or else (temp(I-1).Coor_y /= temp(I).Coor_y)) then
     cpt := cpt + 1;
     Lres(cpt) := temp(I);
    end if; 
   end loop;

  else
   null;
 end if;

 end if;

 N := cpt;

end Intersections_de_polylignes;

-- --------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_type; N : out integer) is
L1,L2 : Point_liste_type(1..5);

begin -- Intersections_rectangles 
 
 L1(1) := A;L1(2) := B;L1(3) := C;L1(4) := D;L1(5) := A;
 L2(1) := E;L2(2) := F;L2(3) := G;L2(4) := H;L2(5) := E;

 Intersections_de_polylignes(L1,L2,5,5,Lres,N,false);

end Intersection_rectangles; 

-- --------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_reel; N : out integer) is
L1,L2 : Point_liste_type(1..5);

begin -- Intersections_rectangles 

 L1(1) := A;L1(2) := B;L1(3) := C;L1(4) := D;L1(5) := A;
 L2(1) := E;L2(2) := F;L2(3) := G;L2(4) := H;L2(5) := E;

 Intersections_de_polylignes(L1,L2,5,5,Lres,N);

end Intersection_rectangles; 

-- --------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_type; N : out integer;Seg_Conf : in out Point_Access_type; NConf : in out natural) is
L1,L2 : Point_liste_type(1..5);

begin -- Intersections_rectangles 
 
 L1(1) := A;L1(2) := B;L1(3) := C;L1(4) := D;L1(5) := A;
 L2(1) := E;L2(2) := F;L2(3) := G;L2(4) := H;L2(5) := E;

 Intersections_de_polylignes(L1,L2,5,5,Lres,N,Seg_Conf,NConf);

end Intersection_rectangles; 

-- --------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_reel; N : out integer;Seg_Conf : in out Point_Access_type; NConf : in out natural) is
L1,L2 : Point_liste_type(1..5);


begin -- Intersections_rectangles 

 L1(1) := A;L1(2) := B;L1(3) := C;L1(4) := D;L1(5) := A;
 L2(1) := E;L2(2) := F;L2(3) := G;L2(4) := H;L2(5) := E;

 Intersections_de_polylignes(L1,L2,5,5,Lres,N,Seg_Conf,NConf);

end Intersection_rectangles; 

-- --------------------------------------------------------------------------
Function Est_il_dans_surface(M : Point_type; S : Surf_type) return boolean is

Ni,np : integer;
Flag : boolean;
Min_x,Min_y : integer :=integer'last;
Max_x,Max_y : integer :=-integer'last;
dx1,dx2,dy1,dy2 : integer;

ps,Pv : long_float;

alpha,somme_angle : float;
somme : integer;

begin

   -- optimisation : recherche si le point est dans la boite d'encombrement de 
   -- la liste des points
             
   for k in 1..S.na(1) loop
       if S.pts(k).coor_x < Min_x then
          Min_x:=S.pts(k).coor_x;
       end if;
       if S.pts(k).coor_y < Min_y then
          Min_y:=S.pts(k).coor_y;
       end if;
       if S.pts(k).coor_x > Max_x then
          Max_x:=S.pts(k).coor_x;
       end if;
       if S.pts(k).coor_y > Max_y then
          Max_y:=S.pts(k).coor_y;
       end if;
   end loop;
   if M.coor_x<Min_x or else M.coor_x>Max_x or else 
      M.coor_y<Min_y or else M.coor_y>Max_y then
      return false;
   end if;
            
   -- le point etant dans la boite d'encombrement, affinons la recherche
   -- pour savoir s'il est effectivement dans la surface
   np:=0;
   somme:=0;
   for i in 1..S.NE loop
      dx1:=S.pts(np+1).coor_x - M.coor_x;
      dy1:=S.pts(np+1).coor_y - M.coor_y;
      somme_angle:=0.0;
             
      for j in 2..S.NA(i) loop
          dx2:=S.pts(np+j).coor_x - M.coor_x;
          dy2:=S.pts(np+j).coor_y - M.coor_y;
          Ps:=long_float(dx1)*long_float(dx2) + long_float(dy1)*long_float(dy2);
          Pv:=long_float(dx1)*long_float(dy2) - long_float(dy1)*long_float(dx2);
          if (Pv=0.0 and Ps<=0.0) then
             -- on est sur le bord de la surface (considere comme interieur)
            return true;
          end if;
             
          if Ps=0.0 then 
             if Pv<0.0 then
                alpha:=-90.0; 
             else
                alpha:=90.0;
             end if;
          else
             --alpha:=Atan2d(float(Pv),float(Ps));
             alpha:=(Arctan(float(Pv),float(Ps)))*180.0/pi; 
             

--             if Ps<0 then
--                alpha:=alpha-180.0;
--             end if;
--             if alpha<-PI then
--                alpha:=alpha + 360.0;
--             end if;
          end if;
          somme_angle:=somme_angle + alpha;
          dx1:=dx2;
          dy1:=dy2;
      end loop;
      np:=np+s.na(i);
      somme_angle:=abs(somme_angle);
      if somme_angle > 180.0 then
         somme:=somme + 360;
      end if;   
   end loop;
   if somme=360 then
      return true;
   else
      return false;
   end if;

end ;


--------------------------------------------------------------------------
-- VERSION REELLE
Function Est_il_dans_surface(M : Point_type_reel; S : Surf_type) return boolean is

Ni,np : integer;
Flag : boolean;
Min_x,Min_y : integer :=integer'last;
Max_x,Max_y : integer :=-integer'last;
dx1,dx2,dy1,dy2,Ps,Pv : float;
alpha,somme_angle : float;
somme : integer;

begin

   -- optimisation : recherche si le point est dans la boite d'encombrement de 
   -- la liste des points
   for k in 1..S.na(1) loop
       if S.pts(k).coor_x < Min_x then
          Min_x:=S.pts(k).coor_x;
       end if;
       if S.pts(k).coor_y < Min_y then
          Min_y:=S.pts(k).coor_y;
       end if;
       if S.pts(k).coor_x > Max_x then
          Max_x:=S.pts(k).coor_x;
       end if;
       if S.pts(k).coor_y > Max_y then
          Max_y:=S.pts(k).coor_y;
       end if;
   end loop;
   if M.coor_x<float(Min_x) or else M.coor_x>float(Max_x) or else 
      M.coor_y<float(Min_y) or else M.coor_y>float(Max_y) then
      return false;
   end if;

   -- le point etant dans la boite d'encombrement, affinons la recherche
   -- pour savoir s'il est effectivement dans la surface
   np:=0;
   somme:=0;
   for i in 1..S.NE loop
      dx1:=float(S.pts(np+1).coor_x) - M.coor_x;
      dy1:=float(S.pts(np+1).coor_y) - M.coor_y;
      somme_angle:=0.0;
      for j in 2..S.NA(i) loop
          dx2:=float(S.pts(np+j).coor_x) - M.coor_x;
          dy2:=float(S.pts(np+j).coor_y) - M.coor_y;
          Ps:=dx1*dx2 + dy1*dy2;
          Pv:=dx1*dy2 - dy1*dx2;
          if (Pv=0.0 and Ps<=0.0) then
             -- on est sur le bord de la surface (considere comme interieur)
            return true;
          end if;
          if Ps=0.0 then 
             if Pv<0.0 then
                alpha:=-90.0; 
             else
                alpha:=90.0;
             end if;
          else
             --alpha:=Atan2d(Pv,Ps);
               alpha:=(Arctan(Pv,Ps))*180.0/pi;
--             if Ps<0.0 then
--                alpha:=alpha-180.0;
--             end if;
--             if alpha<-PI then
--                alpha:=alpha + 360.0;
--             end if;
          end if;
          somme_angle:=somme_angle + alpha;
          dx1:=dx2;
          dy1:=dy2;
      end loop;
      np:=np+s.na(i);
      somme_angle:=abs(somme_angle);
      if somme_angle > 180.0 then
         somme:=somme + 360;
      end if;   
   end loop;
   if somme=360 then
      return true;
   else
      return false;
   end if;

end ;

-- --------------------------------------------------------------------------
-- modif Seb 02/99
Function est_il_dans_rectangle(M : Point_type; A,B,C,D : in Point_type) return boolean is
res : boolean;
pv1,pv2,pv3,pv4 : float;

begin -- est_il_dans_rectangle

 pv1 := Vectoriel(Reel(A-M),Reel(B-M));
 pv2 := Vectoriel(Reel(B-M),Reel(C-M));
 pv3 := Vectoriel(Reel(C-M),Reel(D-M));
 pv4 := Vectoriel(Reel(D-M),Reel(A-M));

 res := ( (pv1 <= 0.0) AND (pv2 <= 0.0) AND (pv3 <= 0.0) AND (pv4 <= 0.0) )
    or ( (pv1 >= 0.0) AND (pv2 >= 0.0) AND (pv3 >= 0.0) AND (pv4 >= 0.0) );
 return res;
end est_il_dans_rectangle;

-- --------------------------------------------------------------------------
--Procedure Equation2(A,B,C : in integer; N : out natural; x1,x2 : out integer) is
--begin -- Equation2
-- null;
--end Equation2;

-- --------------------------------------------------------------------------
Procedure Equation2(A,B,C : in float; N : out natural; x1,x2 : out float) is
discr : float;

begin -- Equation2
 
 if (A = 0.0) then 

  if (B = 0.0) then 
   N := 0;
  else
   N := 1;
   x1 := - (C / B);
  end if;

 else -- ( A different de 0)
 
  discr := B*B - 4.0*A*C;
  if (discr < 0.0) then
   N := 0;
  elsif (discr = 0.0) then 
   N := 1;
   x1 := - B / (2.0*A);
  else 
   N := 2;
   x1 := (- B + sqrt(discr)) / 2.0*A; 
   x2 := (- B - sqrt(discr)) / 2.0*A;   
  end if;

 end if;
end Equation2;

-- --------------------------------------------------------------------------
Procedure EquationY(A,B : in integer; X : in integer; Y : out integer) is
begin -- EquationY
 Y := A*X + B;
end EquationY;

-- --------------------------------------------------------------------------
Procedure EquationY(A,B : in float; X : in float; Y : out float) is
begin -- EquationY
 Y := A*X + B;
end EquationY;

-- --------------------------------------------------------------------------
Procedure EquationX(A,B : in integer; Y : in integer; flag : in out boolean; X : out integer) is
begin -- EquationX
 flag := (A /= 0);
 if (flag) then 
  X := (Y - B) / A;
 end if;
end EquationX;

-- --------------------------------------------------------------------------
Procedure EquationX(A,B : in float; Y : in float; flag : in out boolean; X : out float) is
begin -- EquationX
 flag := (A /= 0.0);
 if (flag) then 
  X := (Y - B) / A;
 end if;
end EquationX;

-- --------------------------------------------------------------------------
Function Transforme_repere(M 		: in Point_type;
			   Vt		: in Point_type:=(0,0);
			   alpha 	: in angle_type) return Point_type is

  Ptres 	: Point_type;
  cosa,sina 	: float;
  x,y 		: integer;

begin -- Transforme_repere

 -- On commence par effectuer la translation 
 Ptres.Coor_x := M.Coor_x - Vt.Coor_x;
 Ptres.Coor_y := M.Coor_y - Vt.Coor_y;

 -- modif du 21/06/1999
 --cosa := cosd(float(alpha));
 --sina := sind(float(alpha));
 
 cosa := cos(float(alpha)*pi/180.0);
 sina := sin(float(alpha)*pi/180.0);
 
 -- On effectue la rotation
 x := integer(cosa*float(Ptres.Coor_x) + sina*float(Ptres.Coor_y));
 y := integer(-sina*float(Ptres.Coor_x) + cosa*float(Ptres.Coor_y));

  Ptres := (Coor_x => x,Coor_y => y);

 return Ptres;
end Transforme_repere;

-- --------------------------------------------------------------------------
Function Transforme_repere(M 	 : in Point_type_reel;
			   Vt	 : in Point_type_reel:=(0.0,0.0);
			   alpha : in angle_type) return Point_type_reel is

  Ptres 	: Point_type_reel;
  cosa,sina 	: float;
  x,y 		: float;

begin -- Transforme_repere

 -- On commence par effectuer la translation 
 Ptres.Coor_x := M.Coor_x - Vt.Coor_x;
 Ptres.Coor_y := M.Coor_y - Vt.Coor_y;

 -- modif du 21/06/1999
 --cosa := cosd(float(alpha));
 --sina := sind(float(alpha));
 cosa := cos(float(alpha)*pi/180.0);
 sina := sin(float(alpha)*pi/180.0);
 

 -- On effectue la rotation
 x :=  cosa*float(Ptres.Coor_x) + sina*float(Ptres.Coor_y);
 y := -sina*float(Ptres.Coor_x) + cosa*float(Ptres.Coor_y);

  Ptres := (Coor_x => x,Coor_y => y);

 return Ptres;
end Transforme_repere;

-- --------------------------------------------------------------------------
Function Transforme_repere(M 	: in Point_type; 
			   Vt 	: in Point_type:=(0,0); 
			alpha   : in float:=0.0) return Point_type is

  Ptres 	: Point_type;
  cosa,sina 	: float;
  x,y 		: float;

begin -- Transforme_repere

 -- On commence par effectuer la translation 
 Ptres.Coor_x := M.Coor_x - Vt.Coor_x;
 Ptres.Coor_y := M.Coor_y - Vt.Coor_y;

 cosa := cos(alpha);
 sina := sin(alpha);

 -- On effectue la rotation
 x := cosa*float(Ptres.Coor_x) + sina*float(Ptres.Coor_y);
 y := -sina*float(Ptres.Coor_x) + cosa*float(Ptres.Coor_y);

 Ptres := (Coor_x => integer(x),Coor_y => integer(y));

 return Ptres;
end Transforme_repere;

-- --------------------------------------------------------------------------
Function Transforme_repere(M 	: in Point_type_reel; 
			   Vt 	: in Point_type_reel:=(0.0,0.0); 
			alpha   : in float:=0.0) return Point_type_reel is

  Ptres 	: Point_type_reel;
  cosa,sina 	: float;
  x,y 		: float;

begin -- Transforme_repere

 -- On commence par effectuer la translation 
 Ptres.Coor_x := M.Coor_x - Vt.Coor_x;
 Ptres.Coor_y := M.Coor_y - Vt.Coor_y;

 cosa := cos(alpha);
 sina := sin(alpha);

 -- On effectue la rotation
 x := cosa*float(Ptres.Coor_x) + sina*float(Ptres.Coor_y);
 y := -sina*float(Ptres.Coor_x) + cosa*float(Ptres.Coor_y);

 Ptres := (Coor_x => x,Coor_y => y);

 return Ptres;
end Transforme_repere;

-- --------------------------------------------------------------------------
function ROTATION(     	p	: point_type;
			an	: float) return point_type is

begin    

   return (integer(float(p.coor_x)*cos(an)+
		   float(p.coor_y)*sin(an)),
	  integer(float(p.coor_y)*cos(an)-
		    float(p.coor_x)*sin(an)));

end ROTATION;

-- --------------------------------------------------------------------------
function ROTATION(     	p	: point_type_reel;
			an	: float) return point_type_reel is

begin    

   return (p.coor_x*cos(an)+p.coor_y*sin(an),
	   p.coor_y*cos(an)-p.coor_x*sin(an));

end ROTATION;

-- --------------------------------------------------------------------------
Function "+"(a,b : point_type) return point_type is
begin
     return (a.coor_x+b.coor_x,a.coor_y+b.coor_y);
end "+";

-- --------------------------------------------------------------------------
Function "+"(a,b : point_type_reel) return point_type_reel is
begin
     return (a.coor_x+b.coor_x,a.coor_y+b.coor_y);
end "+";
-- -------------------------------------------------------------------------

Function "-"(a,b : point_type) return point_type is
begin
     return (a.coor_x-b.coor_x,a.coor_y-b.coor_y);
end "-";

-- --------------------------------------------------------------------------
Function "-"(a,b : point_type_reel) return point_type_reel is
begin
     return (a.coor_x-b.coor_x,a.coor_y-b.coor_y);
end "-";

-- --------------------------------------------------------------------------
function "*" (A: point_type; k : integer) return point_type is
begin
  return (k*A.coor_X,k*A.coor_Y);
end;

-- --------------------------------------------------------------------------
function "*" (A: point_type_reel; k : float) return point_type_reel is
begin
  return (k*A.coor_X,k*A.coor_Y);
end;

-- --------------------------------------------------------------------------
function "*" (k : integer; A: point_type) return point_type is
begin
  return (k*A.coor_X,k*A.coor_Y);
end;

-- --------------------------------------------------------------------------
function "*" (k : float; A: point_type_reel) return point_type_reel is
begin
  return (k*A.coor_X,k*A.coor_Y);
end;

-- --------------------------------------------------------------------------
function "/" (A: point_type_reel; k : float) return point_type_reel is
begin
  return(A.coor_X/k,A.coor_Y/k);
end;

-----------------------------------------
-- Renvoie le produit scalaire de deux vecteurs
-----------------------------------------
function Scalaire(U,V : point_type) return integer is
begin
  return U.coor_X*V.coor_X+U.coor_Y*V.coor_Y;
end Scalaire;

function Scalaire(U,V : point_type_reel) return float is
begin
  return U.coor_X*V.coor_X+U.coor_Y*V.coor_Y;
end Scalaire;

-----------------------------------------
-- Renvoie le produit vectoriel de deux vecteurs
-----------------------------------------
function Vectoriel(U,V : point_type) return integer is
begin
  return U.coor_X*V.coor_Y-U.coor_Y*V.coor_X;
end Vectoriel;

function Vectoriel(U,V : point_type_reel) return float is
begin
  return U.coor_X*V.coor_Y-U.coor_Y*V.coor_X;
end Vectoriel;



-- -----------------------------------------------------------------
function Reel(Arc : Point_liste_type) return Point_liste_reel is
Arcf : Point_liste_reel(Arc'range);
begin
  for i in Arc'range loop
    Arcf(i) := (float(arc(i).coor_X),float(arc(i).coor_Y));
  end loop;
  return Arcf;
end Reel;
-- -----------------------------------------------------------------
function Reel(A : Point_type) return Point_type_reel is
begin
  return (float(a.coor_X),float(a.coor_Y));
end Reel;

-- -----------------------------------------------------------------
function Entier(Arc : Point_liste_reel) return Point_liste_type is
Arcf : Point_liste_type(Arc'range);
begin
  for i in Arc'range loop
    Arcf(i) := (integer(arc(i).coor_X),integer(arc(i).coor_Y));
  end loop;
  return Arcf;
end Entier;
-- -----------------------------------------------------------------
function Entier(A : Point_type_reel) return Point_type is
begin
  return (integer(a.coor_X),integer(a.coor_Y));
end Entier;


-- --------------------------------------------------------------------------
Function rang_point_plus_proche(P 	: in Point_type; 
				ligne 	: in Point_liste_type; 
				N 	: in natural) return natural is
rang : natural := 0; 
dist,distmin : long_float;
begin -- rang_point_plus_proche
 
 For I in 1..N loop
  dist := distance_de_points_long(ligne(I),P);
  if (I = 1) or else (dist < distmin) then 
   distmin := dist;
   rang := I;
  end if;
 
 end loop;

 return rang; 
end rang_point_plus_proche;

-- --------------------------------------------------------------------------
Function rang_point_plus_proche(P 	: in Point_type_reel; 
				ligne 	: in Point_liste_reel; 
				N 	: in natural) return natural is
rang	: natural := 0; 
dist,
distmin	: float;
begin -- rang_point_plus_proche
 
 For I in 1..N loop
  dist := distance_de_points(ligne(I),P);
  if (I = 1) or else (dist < distmin) then 
   distmin := dist;
   rang := I;
  end if;
 
 end loop;

 return rang; 
end rang_point_plus_proche;


-- ***********************************************************************
-- POINT_MOYEN : Calcule le point moyen d'un ensemble de N_points defini 
--		 dans un tableau TAB_POINTS  <=> au centre de gravite !
-- =======================================================================
function POINT_MOYEN(tab_points	: point_liste_type ) return point_type is

  myn	: point_type := (0,0);

 begin

  for k in tab_points'range loop
    myn.coor_x:= myn.coor_x+tab_points(k).coor_x;
    myn.coor_y:= myn.coor_y+tab_points(k).coor_y;
  end loop;

  myn.coor_x:= myn.coor_x/tab_points'length;
  myn.coor_y:= myn.coor_y/tab_points'length;

  return myn;

 end  POINT_MOYEN;

function POINT_MOYEN(tab_points	: point_liste_reel ) return point_type_reel is

  myn	: point_type_reel := (0.0,0.0);

 begin

  for k in tab_points'range loop
    myn.coor_x := myn.coor_x + tab_points(k).coor_x;
    myn.coor_y := myn.coor_y + tab_points(k).coor_y;
  end loop;

  myn.coor_x := myn.coor_x / float(tab_points'length);
  myn.coor_y := myn.coor_y / float(tab_points'length);

  return myn;

 end  POINT_MOYEN;





-- ============================================================================
-- ============================================================================
-- -- Fonctions plus specifiques de CALCUL sur des LIGNES --
-- ============================================================================

-- ============================================================================
Function taille_ligne(	lg : in Point_liste_type; 
			N  : natural:=0) return float is
res 	: float := 0.0;
c	: natural:=n;

begin -- taille_ligne

 if c=0 then
  c:=lg'last;
 end if;

 For I in 2..c loop
  res := res + norme_v2(lg(I-1),lg(I));
 end loop; 
 return res;

end taille_ligne;

-- ============================================================================
Function taille_ligne(lg : in Point_liste_reel; 
		      N : natural:=0) return float is
res : float := 0.0;
c: natural:=n;

begin -- taille_ligne
 if c=0 then
  c:=lg'last;
 end if;
 For I in 2..c loop
  res := res + norme(lg(I-1),lg(I));
 end loop; 
 return res;

end taille_ligne;

-- ============================================================================
Function taille_ligne2(lg : in Point_liste_reel; 
		      N1 : natural:=0; N2 : natural:=0) return float is
res : float := 0.0;
c: natural:=n2;

begin -- taille_ligne
 if c=0 then
  c:=lg'last;
 end if;
 For I in n1+1..c loop
  res := res + norme(lg(I-1),lg(I));
 end loop; 
 return res;

end taille_ligne2;
 

-- ============================================================================
Function taille_ligne2(lg : in Point_liste_Type; 
		      N1 : natural:=0; N2 : natural:=0) return float is
res : float := 0.0;
c: natural:=n2;

begin -- taille_ligne
 if c=0 then
  c:=lg'last;
 end if;
 For I in n1+1..c loop
  res := res + norme(lg(I-1),lg(I));
 end loop; 
 return res;

end taille_ligne2;

-- ============================================================================

Function taille_arc(arc		: integer;
		    graphe	: graphe_type) return float is
  tab	: point_liste_type(1..gr_arc(graphe,arc).nombre_met);
  n	: natural;
begin
  gr_points_d_arc(graphe,arc,tab,n);
  return taille_ligne(tab,n);
end;


-----------------------------------------------------
------ SURFACE D'UN ARC                         ----- 
------ pour un arc en coordonnees reelles       -----
--              ou en coordonnees entieres      ----
------ (positive ou negative suivant le sens)   -----
-----------------------------------------------------
  function surface_arc(Arc : point_liste_reel; 
		       N : natural) return float is

    surface : float := 0.0;
    begin
      for i in 1..N-1 loop
        surface := surface 
		     + (Arc(i+1).coor_Y + Arc(i).coor_Y) 
          	     * (Arc(i+1).coor_X - Arc(i).coor_X);
      end loop;
      return (surface / 2.0);
    end surface_arc;


  function surface_arc(Arc : point_liste_type; 
                     N : natural) return float is
  surface : float := 0.0;
  begin
     for i in 1..N-1 loop
         surface := surface 
            + float((Arc(i+1).coor_Y + Arc(i).coor_Y)) 
            * float((Arc(i+1).coor_X - Arc(i).coor_X));
     end loop;
     return (surface / 2.0);
  end surface_arc;


-- ============================================================================
-- Decomposition d'une ligne en segments irregulers
-- ------------------------------------------------
Procedure Decompose_ligne(l_avant    : in Point_liste_type; 
			  navant     : in natural;
			  l_apres    : out Point_liste_reel; 
			  napres     : out natural; 
			  pas        : in float; 
			  nb_segments: in out Liens_array_type) is
NNP : natural; 
X1,Y1,X2,Y2 : float;
DX,DY,L : float;
n_seg : integer; 

begin -- Decompose_ligne
 
 NNP := 1;
 X1 := float(l_avant(1).Coor_x);
 Y1 := float(l_avant(1).Coor_y);
 l_apres(1).Coor_x := X1;
 l_apres(1).Coor_y := Y1;

 For I in 2..navant loop

  X2 := float(l_avant(I).Coor_x);
  Y2 := float(l_avant(I).Coor_y);
  DX := X2 - X1;
  DY := Y2 - Y1;
  L := sqrt(DX*DX + DY*DY)/Pas ;
  n_seg := integer(L);
  nb_segments(I-1) := n_seg;
  if n_seg/=0 then 
     DX := DX/float(n_seg);
     DY := DY/float(n_seg);
 
     For J in 1..n_seg loop
         NNP := NNP + 1;
         l_apres(NNP).Coor_x := X1 + float(J)*DX;
         l_apres(NNP).Coor_y := Y1 + float(J)*DY;
     end loop;
  end if;

  X1 := X2;
  Y1 := Y2;

 end loop;
 napres := NNP;
end Decompose_ligne;

-- --------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant : in Point_liste_type; 
			navant : in natural;
			l_apres : out Point_liste_type; 
			napres : out natural; 
			pas : in float; 
			nb_segments : in out Liens_array_type) is
NNP : natural; 
X1,Y1,X2,Y2 : float;
DX,DY,L : float;
n_seg : integer; 

begin -- Decompose_ligne
 
 NNP := 1;
 X1 := float(l_avant(1).Coor_x);
 Y1 := float(l_avant(1).Coor_y);
 l_apres(1).Coor_x := l_avant(1).Coor_x;
 l_apres(1).Coor_y := l_avant(1).Coor_y;

 For I in 2..navant loop

  X2 := float(l_avant(I).Coor_x);
  Y2 := float(l_avant(I).Coor_y);
  DX := X2 - X1;
  DY := Y2 - Y1;
  L := sqrt(DX*DX + DY*DY)/Pas ;
  n_seg := integer(L);
  nb_segments(I-1) := n_seg; 
  if n_seg/=0 then
     DX := DX/float(n_seg);
     DY := DY/float(n_seg);
 
     For J in 1..n_seg loop
         NNP := NNP + 1;
         l_apres(NNP).Coor_x := integer(X1 + float(J)*DX);
         l_apres(NNP).Coor_y := integer(Y1 + float(J)*DY);
     end loop;
  end if;

  X1 := X2;
  Y1 := Y2;

 end loop;
 napres := NNP;

end Decompose_ligne;

-- --------------------------------------------------------------------------
Procedure Decompose_ligne(l_avant    : in Point_liste_type; 
			  navant     : in natural;
			  l_apres    : in out Point_Access_reel; 
			  napres     : out natural; 
			  pas        : in float; 
			  nb_segments: in out Liens_array_type) is
NNP : natural; 
X1,Y1,X2,Y2 : float;
DX,DY,L : float;
n_seg : integer; 
taille : float;

begin -- Decompose_ligne

 -- Calcul de la longueur totale de la ligne
 taille := taille_ligne(l_avant,navant);
 n_seg := integer(taille/Pas); -- Arrondi a l'entier le plus proche
 
 -- Allocation memoire de la ligne resultat

 l_apres := new Point_liste_reel(1..(n_seg+100));
 
 NNP := 1;
 X1 := float(l_avant(1).Coor_x);
 Y1 := float(l_avant(1).Coor_y);
 l_apres(1).Coor_x := X1;
 l_apres(1).Coor_y := Y1;

 For I in 2..navant loop

  X2 := float(l_avant(I).Coor_x);
  Y2 := float(l_avant(I).Coor_y);
  DX := X2 - X1;
  DY := Y2 - Y1;
  L := sqrt(DX*DX + DY*DY)/Pas ;
  n_seg := integer(L);
  nb_segments(I-1) := n_seg;
  if n_seg/=0 then 
     DX := DX/float(n_seg);
     DY := DY/float(n_seg);
 
     For J in 1..n_seg loop
         NNP := NNP + 1;
         l_apres(NNP).Coor_x := X1 + float(J)*DX;
         l_apres(NNP).Coor_y := Y1 + float(J)*DY;
     end loop;
  end if;

  X1 := X2;
  Y1 := Y2;

 end loop;
 napres := NNP;
end Decompose_ligne;


-----------------------------------------------------
------ DECOMPOSE LIGNE                          ----- 
------ adaptation du paquetage geometrie        -----
-----------------------------------------------------
Procedure Decompose_ligne(l_avant    : in Point_liste_reel; 
			  navant     : in natural;
			  l_apres    : in out Point_Access_reel; 
			  napres     : out natural; 
			  pas        : in float; 
			  nb_segments: in out Liens_array_type) is
NNP : natural; 
X1,Y1,X2,Y2 : float;
DX,DY,L : float;
n_seg : integer; 
taille : float;

begin -- Decompose_ligne

 -- Calcul de la longueur totale de la ligne
 taille := Taille_ligne(l_avant,navant);
 n_seg := integer(taille/Pas); -- Arrondi a l'entier le plus proche
 
 -- Allocation memoire de la ligne resultat

 l_apres := new Point_liste_reel(1..(n_seg+100));
 
 NNP := 1;
 X1 := l_avant(1).Coor_x;
 Y1 := l_avant(1).Coor_y;
 l_apres(1).Coor_x := X1;
 l_apres(1).Coor_y := Y1;

 For I in 2..navant loop

  X2 := float(l_avant(I).Coor_x);
  Y2 := float(l_avant(I).Coor_y);
  DX := X2 - X1;
  DY := Y2 - Y1;
  L := sqrt(DX*DX + DY*DY)/Pas ;
  n_seg := integer(L);
  nb_segments(I-1) := n_seg;
  if n_seg/=0 then 
     DX := DX/float(n_seg);
     DY := DY/float(n_seg);
 
     For J in 1..n_seg loop
         NNP := NNP + 1;
         l_apres(NNP).Coor_x := X1 + float(J)*DX;
         l_apres(NNP).Coor_y := Y1 + float(J)*DY;
     end loop;
  end if;

  X1 := X2;
  Y1 := Y2;

 end loop;
 napres := NNP;
end Decompose_ligne;



--*****************************************************************************
-- Procedure Decomposition_ligne 
--*****************************************************************************
-- Cf "geometrie" avec pointeur en sortie + gestion cas ou des segments 
-- de la ligne sont moins longs que le pas de decomposition
-- En entree une ligne a coord. entieres Ligne, en sortie :
-- * un pointeur sur une ligne decomposee a coord. reelles Ligne_dec
-- * un tableau des nb de segments sur Ligne_dec corresp a chaque segment
--   de Ligne

Procedure Decomposition_ligne(Ligne	: IN Point_liste_type;
			Pas		: IN Float;
			Ligne_dec	: OUT Point_access_reel;
			lgr_segments	: OUT Liens_array_type) is

Ligne_dec2	: Point_liste_reel(1..2000);
x1,y1,x2,y2,
  DX,DY		: Float;
npts, n_seg	: integer;
n_seg_real	: Float;

Begin
  npts := 1;
  x1 := Float(Ligne(Ligne'first).Coor_x);
  y1 := Float(Ligne(Ligne'first).Coor_y);
  Ligne_dec2(1) := (x1,y1);

  For i in Ligne'first+1..Ligne'last loop
    x2 := Float(Ligne(i).Coor_x);
    y2 := Float(Ligne(i).Coor_y);
    DX := x2 - x1;
    DY := y2 - y1;
    n_seg_real := sqrt(DX*DX + DY*DY)/Pas;
    n_seg := Integer(n_seg_real);
    If n_seg = 0 then
       n_seg := 1; -- il y a au moins un segment : (x1,y1), (x2,y2)
    End If;
    lgr_segments(i-1) := n_seg;
    -- On divise le segment...
    DX := DX / Float(n_seg);
    DY := DY / Float(n_seg);
    -- Recup des (n_seg-1) premiers pts du segment
    For j in 1..n_seg-1 Loop
      npts := npts + 1;
      Ligne_dec2(npts).Coor_x := x1 + Float(j)*DX;
      Ligne_dec2(npts).Coor_y := y1 + Float(j)*DY;
    End Loop; -- j
    -- Recup du dernier point (qui est le ieme point de la ligne initiale) 
    npts := npts + 1;
    Ligne_dec2(npts) := (x2,y2);
    x1 := x2;
    y1 := y2;
  End loop; -- i
  Ligne_dec := New Point_liste_reel'(Ligne_dec2(1..npts));
End Decomposition_ligne;


--*****************************************************************************
-- Procedure Decomposition_ligne - coord. reelles
--*****************************************************************************
-- Cf "geometrie" avec pointeur en sortie + gestion cas ou des segments 
-- de la ligne sont moins longs que le pas de decomposition
-- En entree une ligne a coord. reelles Ligne, en sortie :
-- * un pointeur sur une ligne decomposee a coord. reelles Ligne_dec
-- * un tableau des nb de segments sur Ligne_dec corresp a chaque segment
--   de Ligne

Procedure Decomposition_ligne(Ligne	: IN Point_liste_reel;
			Pas		: IN Float;
			Ligne_dec	: OUT Point_access_reel;
			lgr_segments	: OUT Liens_array_type) is

Ligne_dec2	: Point_access_reel;
x1,y1,x2,y2,
  DX,DY		: Float;
npts, n_seg	: integer;
n_seg_real	: Float;
max_pts		: integer;
longueur	: Float;

Begin

-- estimation du nb de points de la ligne en sortie et instanciation
-- du pointeur associe
  longueur := taille_ligne(Ligne);
  max_pts := Integer(longueur/pas) + Ligne'length;  

  Ligne_dec2 := new Point_liste_reel(1..max_pts);

  npts := 1;
  x1 := Ligne(Ligne'first).Coor_x;
  y1 := Ligne(Ligne'first).Coor_y;
  Ligne_dec2(1) := (x1,y1);

  For i in Ligne'first+1..Ligne'last loop
    x2 := Ligne(i).Coor_x;
    y2 := Ligne(i).Coor_y;
    DX := x2 - x1;
    DY := y2 - y1;
    n_seg_real := sqrt(DX*DX + DY*DY)/Pas;
    n_seg := Integer(n_seg_real);
    If n_seg = 0 then
       n_seg := 1; -- il y a au moins un segment : (x1,y1), (x2,y2)
    End If;
    lgr_segments(i-1) := n_seg;
    -- On divise le segment...
    DX := DX / Float(n_seg);
    DY := DY / Float(n_seg);
    -- Recup des (n_seg-1) premiers pts du segment
    For j in 1..n_seg-1 Loop
      npts := npts + 1;
      Ligne_dec2(npts).Coor_x := x1 + Float(j)*DX;
      Ligne_dec2(npts).Coor_y := y1 + Float(j)*DY;
    End Loop; -- j
    -- Recup du dernier point (qui est le ieme point de la ligne initiale) 
    npts := npts + 1;
    Ligne_dec2(npts) := (x2,y2);
    x1 := x2;
    y1 := y2;
  End loop; -- i
  Ligne_dec := New Point_liste_reel'(Ligne_dec2(1..npts));
  GR_Free_point_liste_reel(Ligne_dec2);
End Decomposition_ligne;



  
-- ----------------------------------------------------------------------
procedure NOEUD_COMMUN( graphe  : graphe_type;
                        a,b     : in integer;
                        ni,nf   : out integer) is
begin
    if (gr_arc(graphe,abs(a)).noeud_ini=gr_arc(graphe,abs(b)).noeud_ini) OR
        (gr_arc(graphe,abs(a)).noeud_ini=gr_arc(graphe,abs(b)).noeud_fin)  THEN
          ni:=gr_arc(graphe,abs(a)).noeud_ini;
    else
      ni:=0;
    end if;

    if (gr_arc(graphe,abs(a)).noeud_fin=gr_arc(graphe,abs(b)).noeud_ini) OR
        (gr_arc(graphe,abs(a)).noeud_fin=gr_arc(graphe,abs(b)).noeud_fin) THEN
          nf:=gr_arc(graphe,abs(a)).noeud_fin;
    else
      nf:=0;
    end if;
end NOEUD_COMMUN;

--function NOEUD_COMMUN(	graphe	: graphe_type;
--			a	: integer;
--			b	: integer) return integer is

--begin 
--    if (gr_arc(graphe,abs(a)).noeud_ini=gr_arc(graphe,abs(b)).noeud_ini) OR
--	(gr_arc(graphe,abs(a)).noeud_ini=gr_arc(graphe,abs(b)).noeud_fin)  THEN
--	  return gr_arc(graphe,abs(a)).noeud_ini;                  
--    else
--        if (gr_arc(graphe,abs(a)).noeud_fin=gr_arc(graphe,abs(b)).noeud_ini) OR
--          (gr_arc(graphe,abs(a)).noeud_fin=gr_arc(graphe,abs(b)).noeud_fin) THEN
--	    return gr_arc(graphe,abs(a)).noeud_fin; 
--        else
--	  return 0;
--        end if;  
--   end if;
--end NOEUD_COMMUN;		 

--******************************************************************************
--SURFACE_VECT
--retourne la surface d'un vecteur (/axe des X)
--******************************************************************************

function SURFACE_VECT(	p1,p2	: point_type) return float is
 
  px	: float; 

begin

  if ((p1.coor_y>0) and (P2.coor_y>0)) or 
     ((p1.coor_y<0) and (P2.coor_y<0)) then
    return float(abs(p1.coor_x-p2.coor_x))*
		((float(min(abs(p1.coor_y),abs(p2.coor_y)))+
		float(abs(p1.coor_y-p2.coor_y))/2.0)
		);
  else
    if (p1.coor_y=0) and (p2.coor_y=0) then
      return 0.0;
    else
      px:=float(p2.coor_y*p1.coor_x-p1.coor_y*p2.coor_x)/
					float(p2.coor_y-p1.coor_y);
      return abs((float(p1.coor_y)/2.0)*(float(p1.coor_x)-px))+
           abs((float(p2.coor_y)/2.0)*(float(p2.coor_x)-px));
    end if;
  end if;

end SURFACE_VECT;
--******************************************************************************
--SURFACE_VECT
--retourne la surface d'un vecteur (/axe des X)
--******************************************************************************

function SURFACE_VECT(	p1,p2	: point_type_reel) return float is
 
  px	: float; 

begin

  if ((p1.coor_y>0.0) and (P2.coor_y>0.0)) or 
     ((p1.coor_y<0.0) and (P2.coor_y<0.0)) then
    return abs(p1.coor_x-p2.coor_x)*
	   (min(abs(p1.coor_y),abs(p2.coor_y))+abs(p1.coor_y-p2.coor_y)/2.0);
  else
    if (p1.coor_y=0.0) and (p2.coor_y=0.0) then
      return 0.0;
    else
      px:=(p2.coor_y*p1.coor_x-p1.coor_y*p2.coor_x)/(p2.coor_y-p1.coor_y);
      return abs((p1.coor_y/2.0)*(p1.coor_x-px))+
           abs((p2.coor_y/2.0)*(p2.coor_x-px));
    end if;
  end if;

end SURFACE_VECT;

------------------------------------------------------------------------
--PROJECTION_SEGMENT----------------------------------------------------
--Renvoie la projection du point sur le segment-------------------------
--Si, hors du segment, renvoie l'extremite la plus proche---------------


function PROJECTION_SEGMENT( p	: point_type;
			     a	: point_type;
			     b 	: point_type
			  ) return point_type is

Tmp	: float;
begin
  tmp:=Distance_a_ligne(p,a,b,C_Ligne_Type);
  if Distance_a_ligne(p,a,b,C_SEGMENT_TYPE)>tmp then
    if norme_v2(p,a)<norme_v2(p,b) then
      return a;
    else
      return b;
    end if;
  else
    return Projection_droite(p,a,b);
  end if;

end PROJECTION_SEGMENT;

------------------------------------------------------------------------
Function PROJECTION_SEGMENT(  p	: point_type_reel;
			      a	: point_type_reel;
			      b : point_type_reel) return point_type_reel is

  Tmp	: float;
begin
  tmp:=Distance_a_ligne(p,a,b,C_Ligne_Type);
  if distance_a_ligne(p,a,b,C_SEGMENT_TYPE)>tmp then
    if norme(p,a)<norme(p,b) then
      return a;
    else
      return b;
    end if;
  else
    return Projection_droite(p,a,b);
  end if;

end PROJECTION_SEGMENT;

------------------------------------------------------------------------
--PROJECTION_POLYLIGNES-------------------------------------------------
--Renvoie le point pf,projete sur la polyligne--------------------------
--ou renvoie j, le rang du point de la polyligne qui le precede----------

Function  PROJECTION_POLYLIGNES(TAB	: point_liste_type;
				I	: integer;
				P	: point_type) return point_type is

  k_min	: integer;
  pf	: point_type;
  Tmp,Tmp_min	: float;

begin

-- Corrrection FL 24-03-97 : changement du principe de l'algorithme
-- car le resultat est faux dans certains cas : le rang_point_plus proche
-- ne donne pas forcement le segment le plus proche.

 -- J:=RANG_POINT_PLUS_PROCHE(P,TAB,I);
 -- if J=1 then
 --  pf:=Projection_segment(p,tab(j), tab(j+1));
 -- else
 --   if j=i then
 --     pf:=Projection_segment(p,tab(j-1), tab(j));
 --     j:=j-1;
 --   else
 --     Tmp:=distance_a_ligne(p,tab(j),tab(j+1),C_SEGMENT_TYPE);
 --     if distance_a_ligne(p,tab(j-1),tab(j),C_SEGMENT_TYPE)<tmp  then
 --       pf:=Projection_segment(p,tab(j-1), tab(j));
 --       j:=j-1;
 --     else
 --       pf:=Projection_segment(p,tab(j), tab(j+1));               
 --     end if;
 --   end if;
 -- end if;
 -- return pf;

  -- correction SM 02/99 pur la cas d'une ligne a 1 point!
  if I = 1 then
    return Tab(1);
  end if;
  Tmp_min:=Float'last;
  for k in 1..i-1 loop
      Tmp:=distance_a_ligne(p,tab(k),tab(k+1),C_SEGMENT_TYPE);
      if Tmp<Tmp_min then
         Tmp_min:=Tmp;
         k_min:=k;
      end if;
  end loop;
  pf:=Projection_segment(p,tab(k_min), tab(k_min+1));
  return pf;

end PROJECTION_POLYLIGNES;


procedure PROJECTION_POLYLIGNES_v2(TAB	: point_liste_type;
                Indice_ini : in integer;
				I	: integer;
				P	: point_type;
                No_pred : out natural;
				reste : out float) is

  k_min	: integer;
  pf	: point_type;
  Tmp,Tmp_min	: float;
  Dist : float;

begin

--  if I = 1 then
--    return Tab(1);
--  end if;
  Tmp_min:=Float'last;
  for k in 1..i-1 loop
      Tmp:=distance_a_ligne(p,tab(k+Indice_ini-1),tab(k+Indice_ini),C_SEGMENT_TYPE);
      if Tmp<Tmp_min then
         Tmp_min:=Tmp;
         k_min:=k-1;
      end if;
  end loop;
  pf:=Projection_segment(p,tab(k_min+Indice_ini), tab(k_min+1+Indice_ini));
  No_Pred:=K_min;
  Dist:=distance_de_points(tab(k_min+Indice_ini),tab(k_min+1+Indice_ini));
  if dist/=0.0 then
    reste:=sqrt(distance_de_points(tab(k_min+Indice_ini),Pf))/sqrt(Dist);
  else
	reste:=0.0;
  end if;
end PROJECTION_POLYLIGNES_v2;


------------------------------------------------------------------------
Function  PROJECTION_POLYLIGNES(TAB	: point_liste_type;
				I	: integer;
				P	: point_type) return integer is

  pf	: point_type;
  k_min	: integer;
  Tmp,Tmp_min	: float;

begin
  -- correction SM 02/99 pur la cas d'une ligne a 1 point!
  if I = 1 then
    return 1;
  end if;

  Tmp_min:=Float'last;
  for k in 1..i-1 loop
      Tmp:=distance_a_ligne(p,tab(k),tab(k+1),C_SEGMENT_TYPE);
      if Tmp<Tmp_min then
         Tmp_min:=Tmp;
         k_min:=k;
      end if;
  end loop;
--  pf:=Projection_segment(p,tab(k_min), tab(k_min+1));
  return k_min;


end PROJECTION_POLYLIGNES;

------------------------------------------------------------------------
--PROJECTION_POLYLIGNES-------------------------------------------------
--Renvoie le point pf,projete sur la polyligne--------------------------
--et renvoie k_min, le rang du point de la polyligne qui le precede----------

Function  PROJECTION_POLYLIGNES(TAB : Point_Liste_reel;
				I   : integer;
				P   : point_type_reel) return point_type_reel is

  k_min	: integer;
  pf	: point_type_reel;
  Tmp,Tmp_min	: float;

begin
  -- correction SM 02/99 pur la cas d'une ligne a 1 point!
  if I = 1 then
    return Tab(1);
  end if;

  Tmp_min:=Float'last;
  for k in 1..i-1 loop
      Tmp:=distance_a_ligne(p,tab(k),tab(k+1),C_SEGMENT_TYPE);
      if Tmp<Tmp_min then
         Tmp_min:=Tmp;
         k_min:=k;
      end if;
  end loop;
  pf:=Projection_segment(p,tab(k_min), tab(k_min+1));
  return pf;

end PROJECTION_POLYLIGNES;

------------------------------------------------------------------------
Function  PROJECTION_POLYLIGNES(TAB	: point_liste_reel;
				I	: integer;
				P	: point_type_reel) return integer is

  pf	: point_type_reel;
  k_min	: integer;
  Tmp,Tmp_min	: float;

begin
  -- correction SM 02/99 pur la cas d'une ligne a 1 point!
  if I = 1 then
    return 1;
  end if;

  Tmp_min:=Float'last;
  for k in 1..i-1 loop
      Tmp:=distance_a_ligne(p,tab(k),tab(k+1),C_SEGMENT_TYPE);
      if Tmp<Tmp_min then
         Tmp_min:=Tmp;
         k_min:=k;
      end if;
  end loop;
--  pf:=Projection_segment(p,tab(k_min), tab(k_min+1));
  return k_min;
end PROJECTION_POLYLIGNES;

--*****************************************************************************
--PROJ
--projette un point sur une droite (1 point 1 vecteur)
--*****************************************************************************
         
Function PROJECTION_DROITE(p,x0	: point_type;
			   x,y	: float) return point_type is

  pt		:point_type;
  nr		:float;
  a,b		:float;
  cosa,sina	:float;

begin

  nr:=norme(x,y,0.0,0.0);
  if nr/=0.0 then 
    cosa:=x/nr;
    sina:=y/nr;
    a:=float(p.coor_x-x0.coor_x)*cosa+float(p.coor_y-x0.coor_y)*sina;
    b:=0.0;
    sina:=0.0-sina;          
    pt.coor_x:=integer(a*cosa+b*sina)+x0.coor_x;
    pt.coor_y:=integer(b*cosa-a*sina)+x0.coor_y;
  else
    pt:=p;
  end if;
  return pt;  

end PROJECTION_DROITE;

Function PROJECTION_DROITE(p	: point_type;
			   p1	: point_type:=(0,0);
			   p2	: point_type) return point_type is
Begin
  return PROJECTION_DROITE(p,p1,float(p2.coor_X-p1.coor_X),
				float(p2.coor_y-p1.coor_y));
end PROJECTION_DROITE;


Function PROJECTION_DROITE(p,x0	: point_type_reel;
			   x,y	: float) return point_type_reel is

  pt		:point_type_reel;
  nr		:float;
  a,b		:float;
  cosa,sina	:float;

begin

  nr:=norme(x,y,0.0,0.0);
  if nr/=0.0 then 
    cosa:=x/nr;
    sina:=y/nr;
    a:=(p.coor_x-x0.coor_x)*cosa+(p.coor_y-x0.coor_y)*sina;
    b:=0.0;
    sina:=0.0-sina;          
    pt.coor_x:=(a*cosa+b*sina)+x0.coor_x;
    pt.coor_y:=(b*cosa-a*sina)+x0.coor_y;
  else
    pt:=p;
  end if;
  return pt;  

end PROJECTION_DROITE;

Function PROJECTION_DROITE(p	: point_type_reel;
			   p1	: point_type_reel:=(0.0,0.0);
			   p2	: point_type_reel) return point_type_reel is
Begin
  return PROJECTION_DROITE(p,p1,(p2.coor_X-p1.coor_X),(p2.coor_y-p1.coor_y));
end PROJECTION_DROITE;



--***************************************************************
--	"Complement" du package Geometrie, CD		     ****
--***************************************************************

-------------------------------------------------------------------------
-- PROCEDURES PERMETTANT DE PASSER DES COORDONNEES D'UN POINT
-- DU PLAN A SA POSITION SUR UNE LIGNE OU INVERSEMENT ET PERMETTANT
-- DE FAIRE LE LIEN ENTRE LIGNE ORIGINALE ET LIGNE DECOMPOSEE
-------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Procedure Situe_sur_ini
------------------------------------------------------------------------
--  Pour une ligne decomposee decrite par le tableau de decomposition 
--  lgr_segment de cardinal npoints-1 (npoints nb de points de la ligne 
--  initiale),
--  et pour un point P de no pos_dec sur la ligne decomposee, renvoir le no 
--  no_pred du point de la ligne initiale precedent P et un reel compris entre
--  0 et 1 representant la position de P sur le segment no_pred, no_pred+1
-- CD
------------------------------------------------------------------------
Procedure Situe_sur_ini(lgr_segments	: in Liens_array_type;
			npoints		: in natural;
			pos_dec		: in natural;
			no_pred		: out natural;
			reste		: out float) is

nb_dec	: natural;
sortie	: boolean := false;

Begin

  nb_dec := 1;
  For i in 1..npoints-1 Loop
    If pos_dec < nb_dec + lgr_segments(i) Then
      no_pred := i;
      reste := Float(pos_dec-nb_dec)/Float(lgr_segments(i));
      sortie := true;
      Exit;
    End If;
    nb_dec := nb_dec + lgr_segments(i);
  End Loop;
  If sortie=false Then
    no_pred := npoints;
    reste := 0.0;
  End If;
End Situe_sur_ini;


Procedure Situe_sur_ini_V2(lgr_segments	: in Liens_array_type;
			Indice_ini		: in integer;
			npoints		: in natural;
			pos_dec		: in natural;
			no_pred		: out natural;
			reste		: out float) is

nb_dec	: natural;
sortie	: boolean := false;

Begin

  nb_dec := 1;
  For i in 1..npoints-1 Loop
    If pos_dec < nb_dec + lgr_segments(i+Indice_ini) Then
      no_pred := i;
      reste := Float(pos_dec-nb_dec)/Float(lgr_segments(i+Indice_ini));
      sortie := true;
      Exit;
    End If;
    nb_dec := nb_dec + lgr_segments(i+Indice_ini);
  End Loop;
  If sortie=false Then
    no_pred := npoints;
    reste := 0.0;
  End If;
End Situe_sur_ini_V2;


------------------------------------------------------------------------
-- Procedure Situe_sur_segm
------------------------------------------------------------------------
--  Pour une ligne decomposee decrite par le tableau de decomposition 
--  lgr_segment de cardinal npoints-1 (npoints nb de points de la ligne 
--  initiale),
--  et pour un point P, pas forcement un point intermediaire, de la ligne
--  initiale: en entree, P est decrit par le no no_pred du point de la ligne 
--  initiale qui le precede et un reel compris entre 0 et 1 representant la 
--  position de P sur le segment no_pred, no_pred+1.
--  En sortie, le no no_dec du point precedent P sur la ligne decomposee.

Procedure Situe_sur_segm(lgr_segments	: in Liens_array_type;
			npoints		: in natural;
			no_pred		: in natural;
			reste		: in float;
			pos_dec		: in out natural) is

reste_max : float;

Begin

  pos_dec := 0;
  -- On compte d'abord les micro-segments pour arriver a no_pred
  For i in 2..no_pred Loop
    pos_dec := pos_dec + lgr_segments(i-1);
  End Loop;
  -- Puis les micro-segments entre no_pred et P
  reste_max := 0.0;
  If no_pred < npoints then
    For i in 1..lgr_segments(no_pred) Loop
      reste_max := reste_max + Float(i)/Float(lgr_segments(no_pred));
      If reste < reste_max Then
        pos_dec := pos_dec + i - 1;
        Exit; 
      End If;
    End Loop;
  End If;
End Situe_sur_segm;

------------------------------------------------------------------------
-- Procedure Position_to_Abs_curv
------------------------------------------------------------------------
-- renvoie l'abscisse curviligne d'un point sur une ligne, pas forcement un
-- point intermediaire. Le point est decrit en entree par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Position_to_Abs_curv(ligne	: in Point_liste_type;
				npts	: in integer;
				no_pred	: in integer;
				reste	: in float;
				abscurv	: in out float) is

Begin
  abscurv := 0.0;
  For i in 2..no_pred Loop
    abscurv := abscurv + Norme_v2(ligne(i-1),ligne(i));
  End Loop;
  If no_pred < npts then
    abscurv := abscurv + reste*Norme_v2(ligne(no_pred), ligne(no_pred+1));
  End If;
End Position_to_Abs_curv;


Procedure Position_to_Abs_curv_V2(ligne	: in Point_liste_type;
                Indice_ini : in integer;
				npts	: in integer;
				no_pred	: in integer;
				reste	: in float;
				abscurv	: in out float) is

Begin
  abscurv := 0.0;
  For i in 1..no_pred Loop
    abscurv := abscurv + Norme_v2(ligne(i-1+Indice_ini),ligne(i+Indice_ini));
  End Loop;
  If no_pred < npts and reste/=0.0 then
    abscurv := abscurv + reste*Norme_v2(ligne(no_pred+Indice_ini), ligne(no_pred+1+Indice_ini));
  End If;
End Position_to_Abs_curv_V2;


------------------------------------------------------------------------
-- Procedure Abs_curv_to_Position
------------------------------------------------------------------------
-- en entree l'abscisse curviligne d'un point sur une ligne, pas forcement 
-- un point intermediaire. Le point est decrit en sortie par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Abs_curv_to_position(ligne	: in Point_liste_type;
				npts	: in integer;
				abscurv	: in float;
				no_pred	: in out integer;
				reste	: in out float) is

longueur : float;

Begin
  no_pred := 0;
  longueur :=0.0;
  For i in 2..npts Loop
    If ((longueur + norme_v2(ligne(i-1),ligne(i))) > abscurv) Then
      no_pred := i-1;
      reste := (abscurv-longueur)/norme_v2(ligne(i-1),ligne(i));
      Exit;
    End If;
    longueur := longueur + norme_v2(ligne(i-1),ligne(i));
  End Loop;
  If (no_pred = 0) Then
    no_pred := npts;
    reste := 0.0;
  End If;
End Abs_curv_to_Position;

------------------------------------------------------------------------
-- Procedure Position_to_Coord
------------------------------------------------------------------------
-- renvoie les coordonnees d'un point sur une ligne, pas forcement un
-- point intermediaire. Le point est decrit en entree par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Position_to_Coord(ligne	: in Point_liste_type;
			npts	: in integer;
			no_pred	: in integer;
			reste	: in float;
			point	: in out Point_type) is

dx,dy : integer;

Begin
  If (reste = 0.0) Then
    point := ligne(no_pred);
  Elsif (no_pred >= npts) Then
    point := ligne(npts);
  Else
    dx := ligne(no_pred+1).Coor_x - ligne(no_pred).Coor_x;    
    dy := ligne(no_pred+1).Coor_y - ligne(no_pred).Coor_y;    
    point.Coor_x := ligne(no_pred).Coor_x + Integer(reste*Float(dx));
    point.Coor_y := ligne(no_pred).Coor_y + Integer(reste*Float(dy));
  End If;
End Position_to_Coord;



--*****************************************************************************
-- Procedure Ajouter_un_vertex
--*****************************************************************************
-- Cette procedure ajoute un point Vertex a la ligne Ligne, apres le
-- point de position Position_pred (ie sur le Position_pred-ieme segment)

Procedure Ajouter_un_vertex(Ligne		: IN Point_liste_type;
			Vertex		: IN Point_type;
			Position_pred	: IN positive;
			Ligne_out	: OUT Point_access_type) is

Ligne_out2	: Point_liste_type(Ligne'first..Ligne'last+1);

Begin
  For i in Ligne'first..Position_pred loop
    Ligne_out2(i) := Ligne(i);
  End loop; -- i
  Ligne_out2(Position_pred+1) := Vertex;
  For i in (Position_pred+2)..(Ligne'last+1) loop
    Ligne_out2(i) := Ligne(i-1);
  End loop; -- i
  Ligne_out := New Point_liste_type'(Ligne_out2);
End Ajouter_un_vertex;

--*****************************************************************************
-- Procedure Enlever_un_vertex
--*****************************************************************************
-- Cette procedure enleve le Position-ieme point de la ligne Ligne

Procedure Enlever_un_vertex(Ligne	: IN Point_liste_type;
			Position	: IN positive;
			Ligne_out	: OUT Point_access_type) is

Ligne_out2	: Point_liste_type(Ligne'first..Ligne'last-1);

Begin
  For i in Ligne'first..(Position-1) Loop
    Ligne_out2(i) := Ligne(i);
  End loop; -- i
  For i in Position..(Ligne'last-1) Loop
    Ligne_out2(i) := Ligne(i+1);
  End loop; -- i
  Ligne_out := New Point_liste_type'(Ligne_out2);
End Enlever_un_vertex;

--**********************************************************************
-- FILTRAGE DES POINTS DOUBLES
--**********************************************************************
function Filtrage(Tab : Point_liste_type) return Point_access_type is

Tab_filtre : Point_access_type;
Tab_filtre2 : Point_access_type;
Nfiltre    : Natural;

begin
  if Tab'length = 2 then
    return new Point_liste_type'(Tab);
  end if;
  Tab_filtre := new Point_liste_type'(Tab);
  Tab_filtre(1) := Tab(Tab'first);
  Nfiltre := 1;
  for j in Tab'first+1..Tab'last loop
    if Tab(j) /= Tab_filtre(Nfiltre) then
      Nfiltre := Nfiltre + 1;
      Tab_filtre(Nfiltre) := Tab(j);
    end if;
  end loop;
  Tab_filtre2 := new Point_liste_type'(Tab_filtre(1..Nfiltre));
  GR_free_point_liste(Tab_filtre);
  return Tab_filtre2;
end Filtrage; 

--*****************************************************************************
-- Procedure Trier_tableau_croissant
--*****************************************************************************
-- Cette procedure trie le tableau Tab par ordre croissant

Procedure Trier_tableau_croissant(Tab		: IN Liens_array_type;
				 Tab_trie	: OUT Liens_array_type) is

Tab2		: Liens_array_type(Tab'range);
min, j_min	: integer;

Begin
  Tab2 := Tab;
  For i in Tab2'range Loop
    min := Tab2(i);
    j_min := i;
    For j in (i+1)..Tab2'last loop
      If (Tab2(j) < min) Then
        j_min := j;
        min := Tab2(j);
      End if;
    End loop; -- j
    Tab2(j_min) := Tab2(i);
    Tab2(i) := min;
  End Loop; -- i
  Tab_trie := Tab2;
End Trier_tableau_croissant;

--*****************************************************************************
-- Procedure Enlever_des_vertex
--*****************************************************************************
-- Cette procedure enleve les points de la ligne Ligne dont les positions
-- figurent dans le tableau Positions. Si on cherche a enlever des points
-- qui n'existent pas, la ligne Ligne est renvoyee

Procedure Enlever_des_vertex(Ligne	: IN Point_liste_type;
			Positions	: IN Liens_array_type;
			Ligne_out	: OUT Point_access_type) is

Positions_trie	: Liens_array_type(Positions'range);
Ligne_out2	: Point_access_type;
Borne_inf,
Borne_sup	: integer;
cpt_tours	: integer := 0;

Begin
  Trier_tableau_croissant(Positions,Positions_trie);
  Borne_inf := Ligne'first;
  Ligne_out2 := New Point_liste_type(Ligne'first..Ligne'last-(Positions'length));
  For i in Positions'first..Positions'last loop
    Borne_sup := Positions(i) - 1;
    For j in (Borne_inf-cpt_tours)..(Borne_sup-cpt_tours) loop
      Ligne_out2(j) := Ligne(j+cpt_tours);
    End loop; -- j
    cpt_tours := cpt_tours + 1;
    Borne_inf := Positions(i)+1;
  End loop; -- i
  -- Dernier troncon
  Borne_sup := Ligne'last;
    For j in (Borne_inf-cpt_tours)..(Borne_sup-cpt_tours) loop
      Ligne_out2(j) := Ligne(j+cpt_tours);
    End loop; -- j
  Ligne_out := Ligne_out2;
  EXCEPTION
    When others =>
      If (Ligne_out2 /= null) then 
	GR_Free_point_liste(Ligne_out2);
        Ligne_out := New Point_liste_type'(Ligne);
      End If;
--      Text_io.put_line("Plantage dans PI_accordeon.Enlever_des_vertex"); 
End Enlever_des_vertex;


--*****************************************************************************
-- Procedure Ajouter_des_vertex
--*****************************************************************************
-- Cette procedure ajoute a la ligne Ligne les points figurant dans le tableau
-- Vertex. Ces points sont intercales respectivements apres les points de la 
-- ligne dont les positions figurent dans le tableau Positions.
-- ATTENTION, on suppose que les positions sont triees par ordre croissant

--Procedure Ajouter_des_vertex(Ligne	: IN Point_liste_type;
--			Vertex		: IN Point_liste_type;
--			Positions	: IN Liens_array_type;
--			Ligne_out	: OUT Point_access_type;
--			Positions_apres	: OUT Liens_access_type) is
--
--Ligne_out2	: Point_access_type;
--Positions_apres2: Liens_access_type;
--Borne_inf,
--Borne_sup	: integer;
--cpt_tours	: integer := 0;

--Begin
--  Borne_inf := Ligne'first;
--  Ligne_out2 := New Point_liste_type(Ligne'first..Ligne'last+(Positions'length));
--  Positions_apres2 := New Liens_array_type(1..0+Positions'length);
--  For i in Positions'first..Positions'last loop
--    Borne_sup := Positions(i);
--    For j in (Borne_inf+cpt_tours)..(Borne_sup+cpt_tours) loop
--      Ligne_out2(j) := Ligne(j-cpt_tours);
--    End loop; -- j
--    Positions_apres2(1+cpt_tours) := Borne_sup+1+cpt_tours;
--    Ligne_out2(Positions_apres2(1+cpt_tours)) := Vertex(Vertex'first+cpt_tours);
--    cpt_tours := cpt_tours + 1;
--    Borne_inf := Positions(i)+1;
--  End loop; -- i
--  -- Dernier troncon
--  Borne_sup := Ligne'last;
--  For j in (Borne_inf+cpt_tours)..(Borne_sup+cpt_tours) loop
--    Ligne_out2(j) := Ligne(j-cpt_tours);
--  End loop; -- j
--  Ligne_out := Ligne_out2;
--  Positions_apres := Positions_apres2;
--  EXCEPTION
--    When others =>
--      If (Ligne_out2 /= null) then 
--	GR_Free_point_liste(Ligne_out2);
--        Ligne_out := New Point_liste_type'(Ligne);
--      End If;
--      Text_io.put_line("Plantage dans PI_accordeon.Ajouter_des_vertex"); 
--End Ajouter_des_vertex;

--------------------------------------
-- GEOMETRIE_SURFACE
-- BODY
--------------------------------------
-- Sebastien Mustiere
-- 02/99
--------------------------------------

--------------------------------------
-- Procedure TRIER_PTS_PAR_LIENS
-- 
-- Cette procedure trie les tableaux Tab_pts et Tab_liens, de la meme
-- maniere, afin que Tab_liens soit classe en ordre croissant.
-- Tab_pts et Tab_liens sont donc de meme taile
-- Position_apres est un tableau (de la taille de Tab_pts) qui fait
-- le lien entre la position dans le tableau des pts de tab_pts et des
-- pts de tab_pts_trie.
-- Exemple:
-- Tab_pts = (X1,X2,X3);
-- Tab_liens = (5,1,3)
-- Tab_pts_trie = (X2,X3,X1);
-- Tab_liens_trie = (1,3,5); 
-- Position_apres = (3,1,2);
--------------------------------------

Procedure Trier_pts_par_liens(Tab_pts         : IN  Point_liste_type;
                              Tab_liens       : IN  Liens_array_type;
                              Tab_pts_trie    : OUT Point_liste_type;
                              Tab_liens_trie  : OUT Liens_array_type) is

Tab_pts2    : Point_liste_type(Tab_pts'range);
Tab_liens2  : Liens_array_type(Tab_liens'range);
Min, 
j_min       : integer;
Pt          : Point_type;

begin
  Tab_pts2 := Tab_pts;
  Tab_liens2 := Tab_liens;

  for i in Tab_liens2'range loop
    min := Tab_liens2(i);
    j_min := i;
    for j in (i+1)..Tab_liens2'last loop
      if Tab_liens2(j) < min then
        j_min := j;
        min := Tab_liens2(j);
      end if;
    end loop; -- j
    Tab_liens2(j_min) := Tab_liens2(i);
    Tab_liens2(i) := min;
    Pt := Tab_pts2(j_min);
    Tab_pts2(j_min) := Tab_pts2(i);
    Tab_pts2(i) := Pt;
  end loop; -- i
  Tab_liens_trie := Tab_liens2;
  Tab_pts_trie := Tab_pts2;
end Trier_pts_par_liens;

Procedure Trier_pts_par_reels(Tab_pts         : IN  Point_liste_type;
                              Tab_reels       : IN  Reel_tableau_type;
                              Tab_pts_trie    : OUT Point_liste_type;
                              Tab_reels_trie  : OUT Reel_tableau_type) is

Tab_pts2    : Point_liste_type(Tab_pts'range);
Tab_reels2  : Reel_tableau_type(Tab_reels'range);
Min         : float; 
j_min       : integer;
Pt          : Point_type;

begin
  Tab_pts2 := Tab_pts;
  Tab_reels2 := Tab_reels;

  for i in Tab_reels2'range loop
    min := Tab_reels2(i);
    j_min := i;
    for j in (i+1)..Tab_reels2'last loop
      if Tab_reels2(j) < min then
        j_min := j;
        min := Tab_reels2(j);
      end if;
    end loop; -- j
    Tab_reels2(j_min) := Tab_reels2(i);
    Tab_reels2(i) := min;
    Pt := Tab_pts2(j_min);
    Tab_pts2(j_min) := Tab_pts2(i);
    Tab_pts2(i) := Pt;
  end loop; -- i
  Tab_reels_trie := Tab_reels2;
  Tab_pts_trie := Tab_pts2;
end Trier_pts_par_reels;


--***********************************************************************
-- Procedure Ajouter_des_vertex
-- MODIF DE GEOMETRIE PAR SEB POUR QU'IL GERE CORRECTEMENT LES CAS OU
-- IL Y A PLUSIEURS POINTS D'AJOUTES SUR UN MEME SEGMENT, ET LES CAS 
-- OU LE TABLEAU POSITION N'EST PAS CLASSE DANS L'ORDRE CROISSANT
-- 5/2/99
--***********************************************************************
-- Cette procedure ajoute a la ligne Ligne les points figurant dans le
-- tableau Vertex. Ces points sont intercales respectivements apres les 
-- points de la ligne dont les positions figurent dans le tableau Positions.

Procedure Ajouter_des_vertex(Ligne	     : IN Point_liste_type;
                             Vertex          : IN Point_liste_type;
			     Positions	     : IN Liens_array_type;
			     Ligne_out	     : OUT Point_access_type;
			     Positions_apres : OUT Liens_access_type) is

Ligne_out2	: Point_access_type;
Debut_cluster,
Fin_cluster,
Borne_inf,
Borne_sup,	
cpt_tours	: natural;
Pos_trie,
Pos_apres       : Liens_array_type(Positions'range);
Pts_trie,
Pts_trie2       : Point_liste_type(Vertex'range);
Dist,
Dist_trie       : Reel_tableau_type(Vertex'range);

Begin
  -- Tri des tableaux en entree
  -- repmier tri par le numro du point precedent sur l'arc
  Trier_pts_par_liens(Vertex, Positions, Pts_trie, Pos_trie);

  -- Tri des points parmi les clusters de points de Pts_trie ayant le meme
  -- predecesseur sur la ligne initiale. Ce tri se fait sur la distance
  -- a ce predecesseur.
  Debut_cluster := Pos_trie'first;
  loop
    Fin_cluster := Debut_cluster;
    for i in Debut_cluster+1..Pos_trie'last loop
      if Pos_trie(i) = Pos_trie(Debut_cluster) then
        Fin_cluster := i;
      else
        exit;
      end if;
    end loop;
    if Fin_cluster /= Debut_cluster then
      -- on a plusieurs points ayant le meme predecesseur
      -- calcul de leur distance a leur predecesseur
      for i in Debut_cluster..Fin_cluster loop
        Dist(i) := Distance_de_points(Pts_trie(i),Ligne(Pos_trie(Debut_cluster)));
      end loop;
      Trier_pts_par_reels(Pts_trie(Debut_cluster..Fin_cluster),
                          Dist(Debut_cluster..Fin_cluster),
                          Pts_trie2(Debut_cluster..Fin_cluster),
                          Dist_trie(Debut_cluster..Fin_cluster));
    else
      Pts_trie2(Debut_cluster) := Pts_trie(Debut_cluster);
    end if;
    if Fin_cluster = Pos_trie'last then 
      exit;
    else
      Debut_cluster := Fin_cluster+1;
    end if;
  end loop;

  -- Creation des tableaux de sortie
  Ligne_out2 := New
                Point_liste_type(Ligne'first..Ligne'last+Positions'length);

  Cpt_tours := 0;  
  Borne_inf := Ligne'first;
  for i in Pos_trie'range loop
    Borne_sup := Pos_trie(i);
    for j in (Borne_inf+cpt_tours)..(Borne_sup+cpt_tours) loop
      Ligne_out2(j) := Ligne(j-cpt_tours);
    end loop; -- j
    Ligne_out2(Borne_sup+cpt_tours+1) :=
         Pts_trie2(Pts_trie2'first+cpt_tours);
    cpt_tours := cpt_tours + 1;
    Borne_inf := Pos_trie(i)+1;
  end loop; -- i
  -- Dernier troncon
  Borne_sup := Ligne'last;
  For j in (Borne_inf+cpt_tours)..(Borne_sup+cpt_tours) loop
    Ligne_out2(j) := Ligne(j-cpt_tours);
  End loop; -- j

  Ligne_out := Ligne_out2;

  -- PROVISOIREMENT
  Positions_apres := null;

  EXCEPTION -- GESTION DES BUGS
    When others =>
      If (Ligne_out2 /= null) then 
	GR_Free_point_liste(Ligne_out2);
        Ligne_out := New Point_liste_type'(Ligne);
      End If;
End Ajouter_des_vertex;


--------------------------------------
-- SEGMENTS_DANS_SURFACE
-- renvoie la liste des segments de surface1
-- entierement dans surface2.

function Segments_dans_surface(Ligne1, 
                               Ligne2 : point_liste_type)
                               return Liens_access_type is

Surface	    : Surf_type(1, ligne2'length);
Segm_dedans : boolean;
Pt_milieu   : Point_type_reel;
Nb_seg      : natural := 0;
Seg         : Liens_array_type(Ligne1'range);

begin
  Surface.ne := 1;
  Surface.na(1) := Ligne2'length;
  Surface.pts := Ligne2;

  for i in Ligne1'first..Ligne1'last-1 loop
    Pt_milieu := (Reel(Ligne1(i))+Reel(Ligne1(i+1)))/2.0;
    if Est_il_dans_surface(Pt_milieu,Surface) then
      Nb_seg := Nb_seg +1;
      Seg(Nb_seg) := i;
    end if;
  end loop;
  return new Liens_array_type'(Seg(1..Nb_seg));
end Segments_dans_surface;

--------------------------------------
-- RETOURNE
-- inverse le sens d'un arc

Function Retourne( Ligne : Point_liste_type) 
                   return Point_liste_type is

Ligne_retourne : Point_liste_type(Ligne'range);

begin
  for i in Ligne'range loop
    Ligne_retourne(i) := Ligne(Ligne'last-i+Ligne'first);
  end loop;
  return ligne_retourne;
end Retourne;

--------------------------------------
-- POINT_AUX_INTERSECTIONS
-- procedure qui rajoute des points sur ligne1 et ligne2
-- a chaque intersection de ligne_1 et ligne_2

Procedure Point_aux_intersections(Ligne1,
                                  Ligne2 : IN Point_liste_type;
                                  L_dec1,
                                  L_dec2 : OUT Point_access_type) is

M        : Point_type_reel;
Flag     : boolean;
Pt_inter : Point_liste_type(1..Ligne1'length*Ligne2'length);
Pred1,
Pred2    : Liens_array_type(1..Ligne1'length*Ligne2'length);
Count,
Cpt      : natural := 0; 
Pos_apres1,
Pos_apres2 : Liens_access_type;
Ligne_dec1,
Ligne_dec2 : Point_access_type;

begin
  -- recherche des points d'intersections des deux lignes
  -- en notant de quels segments ca vient.
  -- inspire de Intersections_de_polylignes de GEOMETRIE
 for I in Ligne1'first+1..Ligne1'last loop
  for J in Ligne2'first+1..Ligne2'last loop
    Flag := false;
    Intersection_de_lignes(Ligne1(I-1),Ligne1(I),Ligne2(J-1),Ligne2(J),
                           C_segment_type,Flag,M);  
    if (Flag) then 
      Count := Count + 1;
      Pt_inter(Count) := Entier(M);    
      Pred1(Count) := i-1;
      Pred2(Count) := j-1;
     end if;
  end loop;
 end loop;

  -- ajoute les points aux 2 arcs
  if (count > 0) then 
    Ajouter_des_vertex(Ligne1,Pt_inter(1..Count),Pred1(1..Count),
                       Ligne_dec1, Pos_apres1);
    Ajouter_des_vertex(Ligne2,Pt_inter(1..Count),Pred2(1..Count),
                       Ligne_dec2, Pos_apres2);
    GR_free_liens_array(Pos_apres1);
    GR_free_liens_array(Pos_apres2);
  else
    Ligne_dec1 := new Point_liste_type'(ligne1);
    Ligne_dec2 := new Point_liste_type'(ligne2);
  end if;

  L_dec1 := Filtrage(Ligne_dec1.all);
  L_dec2 := Filtrage(Ligne_dec2.all);
  GR_free_point_liste(Ligne_dec1);
  GR_free_point_liste(Ligne_dec2);

end Point_aux_intersections;
 

--------------------------------------
-- SURFACE_UNION
--------------------------------------
Function Surface_union(Surface1,
                       Surface2 : Point_liste_type)
                       return float is
Surf : float;

begin
  -- test pour savoir si on a bien des arcs fermes
  -- sinon on arrete tout et on renvoie -1.0
  if Surface1(Surface1'first) /= Surface1(Surface1'last) then
    return -1.0;
  end if;
  if Surface2(Surface2'first) /= Surface2(Surface2'last) then
    return -1.0;
  end if;

  -- la surface de l'union est egale a la somme des deux surfaces moins
  -- la surface de l'intersection.
  Surf := Abs(Surface_arc(Surface1,Surface1'length)) 
          + Abs(Surface_arc(Surface2,Surface2'length))
          - Surface_inter(Surface1, Surface2);
  return Surf;
end Surface_union;


--------------------------------------
-- SURFACE_INTER
--------------------------------------
Function Surface_inter(Surface1,
                       Surface2 : Point_liste_type)
                       return float is

Surface2_bis : Point_liste_type(Surface2'range);
Surf1,
Surf2        : Point_access_type;
Seg1,
Seg2         : Liens_access_type;
Surf_diff,
Surf_id   : float;
Identique1,
Identique2  : Liens_access_type;

begin
  -- test pour savoir si on a bien des arcs fermes
  -- sinon on arrete tout et on renvoie -1.0
  if Surface1(Surface1'first) /= Surface1(Surface1'last) then
    return -1.0;
  end if;
  if Surface2(Surface2'first) /= Surface2(Surface2'last) then
    return -1.0;
  end if;

  -- met les deux arcs dans le meme sens (trigo ou non)
  if Surface_arc(Surface1, Surface1'last) 
     * Surface_arc(Surface2, Surface2'last) >= 0.0 then
     -- les deux arcs sont dans le meme sens
     Surface2_bis := Surface2;
  else
    -- les deux arcs sont en sens inverse
    Surface2_bis := Retourne(Surface2);
  end if;

  -- ajoute a chacun des arcs les points d'interction avec l'autre arc
  Point_aux_intersections(Surface1, Surface2_bis, Surf1, Surf2);

  -- Liste des segments de Surf1 dans surf2
  Seg1 := Segments_dans_surface(Surf1.all,Surf2.all);

  -- Liste des segemnts de Surf2 dans Surf1
  Seg2 := Segments_dans_surface(Surf2.all,Surf1.all);

  -- Recherche des segments communs aux deux precedentes listes
  Identique1 := new Liens_array_type(Seg1'range);
  Identique1.all := (others => 0);
  Identique2 := new Liens_array_type(Seg2'range);
  Identique2.all := (others => 0);
  for i in Seg1'range loop
    for j in Seg2'range loop
      if Surf1(Seg1(i)) = Surf2(Seg2(j)) and
         Surf1(Seg1(i)+1) = Surf2(Seg2(j)+1) then
         -- les segments sont idenetiques et dans le meme sens
         Identique1(i) := 1;      
         Identique2(j) := 1;
         exit;   
      elsif Surf1(Seg1(i)) = Surf2(Seg2(j)+1) and
         Surf1(Seg1(i)) = Surf2(Seg2(j)) then
         -- les segments sont identiques mais en sens inverse
         Identique1(i) := 1;
         Identique2(j) := 1;
         exit;
      end if;
    end loop;
  end loop;
    
  Surf_diff := 0.0;
  Surf_id := 0.0;
  -- calcul de la surface
  for i in Seg1'range loop
    if Identique1(i) = 0 then
      Surf_diff := Surf_diff
              + float((Surf1(Seg1(i)+1).coor_Y + Surf1(Seg1(i)).coor_Y)) 
              * float((Surf1(Seg1(i)+1).coor_X - Surf1(Seg1(i)).coor_X));
    else
      Surf_id := Surf_id
              + float((Surf1(Seg1(i)+1).coor_Y + Surf1(Seg1(i)).coor_Y)) 
              * float((Surf1(Seg1(i)+1).coor_X - Surf1(Seg1(i)).coor_X));

    end if;
  end loop;

  for i in Seg2'range loop
    if Identique2(i) = 0 then
      Surf_diff := Surf_diff
              + float((Surf2(Seg2(i)+1).coor_Y + Surf2(Seg2(i)).coor_Y)) 
              * float((Surf2(Seg2(i)+1).coor_X - Surf2(Seg2(i)).coor_X));
    else
      Surf_id := Surf_id
              + float((Surf2(Seg2(i)+1).coor_Y + Surf2(Seg2(i)).coor_Y)) 
              * float((Surf2(Seg2(i)+1).coor_X - Surf2(Seg2(i)).coor_X));

    end if;
  end loop;
  GR_free_point_liste(Surf1);
  GR_free_point_liste(Surf2);
  GR_free_liens_array(Seg1);
  GR_free_liens_array(Seg2);
  GR_free_liens_array(Identique1);
  GR_free_liens_array(Identique2);

  return Abs((Surf_diff+Surf_id/2.0)/2.0);
end Surface_inter;

end;

