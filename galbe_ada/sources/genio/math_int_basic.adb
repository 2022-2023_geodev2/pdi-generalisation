Package Body Math_int_basic is

-- ------------------------------------------------------------------------
Function Ceil(a,b : natural) return natural is
begin
   if b=0 then	-- modification !! ancien: `if a=0 then`
      return a;
   else 
      return (a-1)/b+1;
   end if;
end;

-- ------------------------------------------------------------------------
Function Floor(a,b : natural) return natural is
begin
   return a/b;
end;

-- ------------------------------------------------------------------------
Function Max(a,b : integer) return integer is
begin
   if a>b then
      return a;
   else
      return b;
   end if;
end;

-- ------------------------------------------------------------------------
Function Max(a,b : float) return float is
begin
   if a>b then
      return a;
   else
      return b;
   end if;
end;

-- ------------------------------------------------------------------------
Function Min(a,b : integer) return integer is
begin
   if a>b then
      return b;
   else
      return a;
   end if;
end;

-- ------------------------------------------------------------------------
Function Min(a,b : float) return floAT is
begin
   if a>b then
      return b;
   else
      return a;
   end if;
end;

-- ------------------------------------------------------------------------
Function Modulo(a,b : integer) return integer is
  q,r: integer;
begin
   q:=a/b;
   r:=a-b*q;
   if r<0 then 
     r:=r+b;
   end if;
   return r;
end;

-- ------------------------------------------------------------------------
function ent(x:float) return integer is
  m	:integer;
begin
 m:=integer(x);
 if float(m)>x then
  return m-1;
 else
  return m;
 end if;

end ent;

-- ------------------------------------------------------------------------
Function Decimal(x:float) return float is

Begin
  return x-float(Ent(x));
End Decimal;

end;
