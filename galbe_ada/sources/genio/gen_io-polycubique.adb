--******************************************************************************
--Package de traitement des Polycubiques,travaillant sur le fichier des cubiques
--******************************************************************************
-- Modifie le 21/06/1999 pour portage sous WindowsNT

With Geometrie; use Geometrie;
With lissage_filtrage;use lissage_filtrage;
With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;

Separate(Gen_io) Package body Polycubique is


-- ============================================================================
Function  LIRE(graphe : graphe_type; 
               arc : positive) return Cubique_liste_type is

My_liste : Cubique_liste_type(1..graphe.info.max_cub_arc);
A_lire,P_lire,First,N,reste : integer; 

begin                   
   A_lire:=ceil(graphe.tarc(arc).premier_met,50);
   if A_lire/=graphe.lblc then
      read(graphe.f_cub,graphe.bcub,Cubique_io.count(a_lire));
      graphe.lblc:=a_lire;
   end if;
   P_lire:=modulo(graphe.tarc(arc).premier_met-1,50);
   Reste:=graphe.tarc(arc).nombre_met;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,50-p_lire);
      My_liste(First+1..First+N):=Graphe.Bcub(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_cub,graphe.bcub);
         Graphe.lblc:=graphe.lblc+1;
         P_lire:=0;
      end if;
   end loop;      
   return my_liste(1..graphe.tarc(arc).nombre_met);
end;

-- =====================================================================
-- Ecriture des cubiques  en fin de fichier des cubiques
-- =====================================================================
Procedure ECRIRE_FIN(graphe : in out graphe_type;
                     tab_cubiques : cubique_liste_type;
                     n : positive) is


A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_cubiques+1,50);
   if A_lire/=graphe.lblc then
      if modulo(graphe.info.nb_cubiques,50)/=0 then
         read(graphe.f_cub,graphe.bcub,Cubique_io.count(a_lire));
      end if;
      graphe.lblc:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_cubiques,50);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,50-p_lire);
      Graphe.Bcub(P_lire+1..P_lire+K):=tab_cubiques(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_cub,graphe.bcub,Cubique_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lblc:=graphe.lblc+1;
         P_lire:=0;
      end if;
   end loop;  
end;
 

-- ==========================================================================
-- Reecriture des cubiques a la meme adresse du buffer des cubiques (on ecrit 
-- donc une liste dont le nombre d elements est inferieur ou egal a la liste
-- de depart)
-- ==========================================================================
Procedure ECRIRE(graphe : in out graphe_type;
                 tab_cubiques : cubique_liste_type;
                 n : positive;
                 premier : positive ) is

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,50);
   if A_lire/=graphe.lblc then
      read(graphe.f_cub,graphe.bcub,Cubique_io.count(a_lire));
      graphe.lblc:=a_lire;
   end if;
   P_lire:=modulo(premier-1,50);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,50-p_lire);
      Graphe.Bcub(P_lire+1..P_lire+K):=tab_cubiques(First+1..First+K);
      Reste:=Reste-K;
      First:=first+K;
      write(graphe.f_cub,graphe.bcub,Cubique_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_cub,graphe.bcub);
         Graphe.lblc:=graphe.lblc+1;
         P_lire:=0;
      end if;
   end loop;  
end;


-- ============================================================================
Procedure CHANG_CUB_PTS(tab_cubiques : in cubique_liste_type;
                        n_cub        : in positive;
			pas          : in integer;
                        tab_points   : out point_liste_reel;
                        n_pts        : out positive) is
 
np, npts: integer := 0;
temp : point_liste_reel(1..5000);
tab_temp : point_liste_reel(1..5000*n_cub);
intervalle: integer := 0;
xi, yi, xl: integer;
x, y   : float;
a, phi : float ;

begin            

  for cub in 1..n_cub loop

     xi := tab_cubiques(cub).point_inflexion.coor_x; 
     yi := tab_cubiques(cub).point_inflexion.coor_y;
     a  := tab_cubiques(cub).a;
     phi:= tab_cubiques(cub).phi;

     intervalle:= tab_cubiques(cub).Xlocal_sommet/pas;
     np:=0;
     for i in 0..intervalle loop
	   np:= np+1;
	   xl:= pas * i;
	   x := float(xi) + float(xl)*cos(phi) - a*float(xl)**3 * sin(phi);
	   y := float(yi) + float(xl)*sin(phi) + a*float(xl)**3 * cos(phi);
           temp(np).coor_x:= x; temp(np).coor_y:= y;	
     end loop;

   if ( (float(cub)/float(2)) - float(cub/2) ) /= 0.0 then -- cubique AVANT --
     for i in 1..np loop
        tab_temp(npts+i):= temp(i);
     end loop;
     npts:= npts + np;
   else -- cubique arriere --
     for i in 1..np-1 loop
        tab_temp(npts+i):= temp(np-i);
     end loop;
     npts:= npts + np-1;
   end if;                

  end loop;

  n_pts:= npts;
  for i in 1..npts loop
     tab_points(i):= tab_temp(i);
  end loop;

end CHANG_CUB_PTS;

-- ============================================================================
-- Detection des changements de courbures et creation du tableau des points 
-- de la ligne precedant chaque pt d'inflexion :
-- Les pts initial et final sont par defaut assimiles a des points d'inflexion
-- ( On suppose les micro-inflexion eliminees au prealable par lissage )
------------------------------------------------------------------------------
Procedure Det_points_inflexion( Tprove_lisse   : in reel_tableau_type; 
			        np             : in natural; 
				produit_vectoriel_min : in float;
			        Tpoints_pred_pi: out liens_array_type;
				nb_pts_inflex  : out natural) is 

condition1, condition2: boolean;
npi: natural:=1; -- <=> nb_pts_inflex ! --
--tpi: liens_array_type(1..np+2); -- <=> Tpoints_pred_pi ! --

begin

  condition1:= (Tprove_lisse(1) >= 0.0);
  Tpoints_pred_pi(1):= 1;

  for i in 2..np loop
     if abs(Tprove_lisse(i)) > produit_vectoriel_min then
  	condition2:= (Tprove_lisse(i) >= 0.0);
	if (not condition1 and condition2) 
	   or (condition1 and not condition2) then -- les 2 pv. de signe <> !!!
  	    npi:= npi + 1;
  	    Tpoints_pred_pi(npi):= i;
	    condition1:= condition2;	  
	end if;	
     end if;
  end loop;

  npi:= npi + 1;
  Tpoints_pred_pi(npi):= np+2; -- et pas : np-2 !!!??

--  Tpoints_pred_pi:= new liens_array_type(1..npi);
  nb_pts_inflex:= npi;
--  Tpoints_pred_pi(1..npi):= tpi(1..npi);
  
end;


-- ===========================================================================
-- Determination des coordonnees des pts d'inflexion, des angles
-- et de leur tolerance :
------------------------------------------------------------------------------
Procedure Det_angles_phi(Tab_points        : in point_liste_type;
			 n_pts             : in natural;
			 Tpoints_pred_pi   : in liens_array_type;
			 nb_pts_inflex     : in natural;
			 tolerance         : in float;
			 demi_tolerance_max: in float;
			 Tpoints_inflex    : out point_liste_type;
	      		 PTphi		   : in out reel_tableau_type;
			 Ttolphi	   : in out reel_tableau_type ) is
x1, y1, x2, y2: integer;
D, phi, tolphi: float := 0.0;
inflex: integer;
--PI:constant float:=3.14159265;

begin                      
  x1:= tab_points(1).coor_x;
  y1:= tab_points(1).coor_y;
  x2:= tab_points(2).coor_x;
  y2:= tab_points(2).coor_y;

  if x1 /= x2 then
    -- modif du 21/06/1999  
    --phi:= Atan2(float(y2-y1),float(x2-x1));
    phi:= Arctan(float(y2-y1),float(x2-x1));
  else
    if y2>y1 then phi:= PI/2.0; else phi:= - PI/2.0; end if;  
  end if;
  D:= Sqrt( (float(x2-x1))**2 + (float(y2-y1))**2 );
  -- modif du  21/06/99 Atan devient Arctan
  tolphi:= min ( Arctan(tolerance/(2.0*D)) , demi_tolerance_max ); 
  ptphi(1):= phi;
  Ttolphi(1):= tolphi;

  Tpoints_inflex(1):= (x1,y1);

  for rang in 2..nb_pts_inflex-1 loop  
                  
     inflex:= Tpoints_pred_pi(rang);

     -- Estimation des coord. du pt d'inflex au milieu du segment du point pred 
     -- au point suivant:
     Tpoints_inflex(rang).coor_x:=(Tab_points(inflex).coor_x+Tab_points(inflex+1).coor_x)/2;
     Tpoints_inflex(rang).coor_y:=(Tab_points(inflex).coor_y+Tab_points(inflex+1).coor_y)/2;

     -- Si au moins 4 points de la ligne ( PARAMETRE ???) separent 
     -- le pt d'inflex pred ou le suivant du pt d'inflex considere, alors 
     -- l'angle est moyenne sur 4 points au lieu de 2 (pred et succ.)
     if (Tpoints_pred_pi(rang+1)-inflex<=4) OR (inflex-Tpoints_pred_pi(rang-1)<=4) then
	x1:=Tab_points(inflex).coor_x;	
	y1:=Tab_points(inflex).coor_y;
	x2:=Tab_points(inflex+1).coor_x;	
	y2:=Tab_points(inflex+1).coor_y;
     else
	x1:=(Tab_points(inflex).coor_x+Tab_points(inflex-1).coor_x)/2;	
	y1:=(Tab_points(inflex).coor_y+Tab_points(inflex-1).coor_y)/2;
	x2:=(Tab_points(inflex+1).coor_x+Tab_points(inflex+2).coor_x)/2;	
	y2:=(Tab_points(inflex+1).coor_y+Tab_points(inflex+2).coor_y)/2;
     end if;	

     if x1 /= x2 then
       	--phi:= Atan2(float(y2-y1),float(x2-x1));
       phi:= Arctan(float(y2-y1),float(x2-x1));
     else
    	if y2>y1 then phi:= PI/2.0; else phi:= - PI/2.0; end if;  
     end if;

     D:= Sqrt( (float(x2-x1))**2 + (float(y2-y1))**2 );
     -- modif du 21/06 atan devient arctan
     tolphi:= min ( Arctan(tolerance/(2.0*D)) , demi_tolerance_max ); 
     ptphi(rang):= phi;
     Ttolphi(rang):= tolphi;
		
  end loop;

  x2:=Tab_points(n_pts).coor_x;	
  y2:=Tab_points(n_pts).coor_y;

  Tpoints_inflex(nb_pts_inflex):=(x2,y2);

  x1:=Tab_points(n_pts-1).coor_x;	
  y1:=Tab_points(n_pts-1).coor_y;


  if x1 /= x2 then
       	--phi:= Atan2(float(y2-y1),float(x2-x1));
          phi:= Arctan(float(y2-y1),float(x2-x1));
  else
    	if y2>y1 then phi:= PI/2.0; else phi:= - PI/2.0; end if;  
  end if;
  D:= Sqrt( (float(x2-x1))**2 + (float(y2-y1))**2 );
  -- modif du 21/06/1999 atan devient Arctan
  tolphi:= min (Arctan(tolerance/(2.0*D)) ,demi_tolerance_max ); 
  ptphi(nb_pts_inflex):= phi;
  Ttolphi(nb_pts_inflex):= tolphi;

end Det_angles_phi;


-- ============================================================================
-- Determination des parametres des 2 cubiques et du meilleur sommet
-- en faisant varier l'angle selon la tolerance precedamment calculee :
-- ============================================================================
Procedure Recherche_sommet( Tab_points	    : in point_liste_type;
                            n_pts           : in natural;
			    pred_i1, pred_i2: in integer;
			    pt_i1, pt_i2    : in gen_io.point_type;
			    phiav1, phiar1  : in float;
			    Dphiav, Dphiar  : in float;
			    part_angle      : in integer;
			    a1a, a2a        : out float;
			    phi1a, phi2a    : out float;
			    x1a, x2a        : out integer;
			    Davmaxa, Darmaxa: out float;
			    Surface         : out float ) is

Type point_point_liste_type is 
		array(natural range <>,natural range <>) of gen_io.point_type;
Type pt_pt_access_type is access point_point_liste_type;
Slav, Slar: pt_pt_access_type; -- Coord. des sommets potentiels ds le syst local
procedure free_ppliste is new unchecked_deallocation(
		point_point_liste_type, pt_pt_access_type);
                          
p1, p2: gen_io.point_type;
Sommin, sommina, S1min, S2min, S1, S2: float:= float'last;
phicav, phicar, phi1, phi2: float;
x1, x2: integer;
a1, a2, av, ar: float;
D, dmax, davmax, darmax, dx, dy: float:= 0.0;

Tarc: point_liste_type(1..4*n_pts);
np_arc: integer:= 0;
--PI:constant float:=3.14159265;

-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
Function distance_max ( p1, p2: in gen_io.point_type; 
			y1, y2: in float ) return float is
phi: float;
D1, D2: float;

begin
  if p1.coor_x /= p2.coor_x then
  	--phi:= atan2( (y2-y1), float(p2.coor_x-p1.coor_x) );
    phi:= arctan( (y2-y1), float(p2.coor_x-p1.coor_x) );
  else
    	if y2>y1 then phi:= PI/2.0; else phi:= - PI/2.0; end if;  
  end if;
  D1:= abs( cos(phi)*(float(p1.coor_y)-y1) );
  D2:= abs( - sin(phi)*float(p2.coor_x-p1.coor_x) 
	    + cos(phi)*(float(p2.coor_y)-y1) );
  return max(D1, D2);
end;
-- ------------------------------------------------------------------------

begin -- Recherche sommet --

  -- Decoupage de l'arc en pts intermed. pour affiner la recherche de sommet:
  P1:= tab_points(pred_i1+1);
  for i in pred_i1+2..pred_i2 loop
    P2:= tab_points(i);      
    for j in 1..4 loop
     np_arc:= np_arc+1;
     tarc(np_arc).coor_x:=integer((float(part_angle-j+1)/float(part_angle))*float(p1.coor_x)
		    +(float(j-1)/float(part_angle))*float(p2.coor_x));
     tarc(np_arc).coor_y:=integer((float(part_angle-j+1)/float(part_angle))*float(p1.coor_y)
		    +(float(j-1)/float(part_angle))*float(p2.coor_y));
    end loop;	
    P1:= P2;
  end loop;
  np_arc:= np_arc+1;
  tarc(np_arc):= P2;

  Davmaxa:= 0.0; Davmaxa:= 0.0;
  a1a:= 0.0; a2a:= 0.0;
  Phi1a:= 0.0; Phi2a:= 0.0;
  x1a:= 0; x2a:= 0;

  -- Coordonnees des sommets dans le repere local de chaque cubique : 
  Slav := new point_point_liste_type(1..np_arc,1..part_angle+1);
  Slar := new point_point_liste_type(1..np_arc,1..part_angle+1);

  -- Calcul prealable des coordonnees SLAR !!!
  for i in 1..part_angle+1 loop 
     Phicar:= phiar1 + float(i-1)*Dphiar;
     for k in 1..np_arc-1 loop
        Slar(k,i):= Transforme_repere(Tarc(k), pt_i2, Phicar);
     end loop;     
     slar(np_arc,i):= (0,0);
  end loop;

  -- --------------------------------------------------------
  -- Chaque point de l'arc est test� comme sommet potentiel :
  -- --------------------------------------------------------
  for k in 1..np_arc loop

     S1min := float'last;

     if k=1 then -- cas trivial: on ne calcule que la cubique arriere !
	
	-- La cubique avant est assimil�e � un segment de droite :
        DX:= float(Tarc(k).coor_x - pt_i1.coor_x);
        DY:= float(Tarc(k).coor_y - pt_i1.coor_y);
        if DX/=0.0 then
          --phi1:= Atan2(DY,DX);
            phi1:= Arctan(DY,DX);
	else
	  if DY	> 0.0 then phi1:= PI/2.0; else phi1:= - PI/2.0; end if;  
	end if;
        a1:= 0.0;
        x1:= integer(Sqrt(DX**2 + DY**2));
        for i in 1..part_angle+1 loop Slav(k,i):= (x1,0); end loop;
        S1:= 0.0;
        S1min:= 0.0;

     else
      -- Calcul d'une CUBIQUE AVANT pour chaque variation d'angle Dphiav
      for i in 1..part_angle+1 loop 

      	Phicav:= phiav1+float(i-1)*Dphiav;
	Slav(k,i):= Transforme_repere(Tarc(k), pt_i1, Phicav);
	if slav(k,i).coor_x < 1 then -- Risque de pt de rebroussement !!???
	  s1:= float'last;
	else
	  av:= float(slav(k,i).coor_y) / float(slav(k,i).coor_x)**3;

	  -- Calcul de l'aire (en valeur absolue) entre la cubique avant
	  -- et le troncon qu'elle approxime :
	  s1:= 0.0;
          p1:= (0,0);
	  for kk in 1..k loop
	     p2:=slav(kk,i);
             s1:= s1 + Abs( (av*(float(p2.coor_x)**4-float(p1.coor_x)**4)/4.0) 
		 	-float((p2.coor_x-p1.coor_x)*(p2.coor_y+p1.coor_y)/2));
	     D:= Distance_max(p1,p2,av*float(p1.coor_x)**3,av*float(p2.coor_x)**3);
	     if S1>S1min then
		exit;
	     else
	     	if D>Dmax then Dmax:= D; end if;
		p1:=p2;
	     end if;
	  end loop;
	end if;

        if s1<s1min then
	  s1min:= s1;
	  phi1:= phicav;
	  x1:= slav(k,i).coor_x;
	  a1:= av;
	  davmax:= dmax;
	end if;
      end loop; -- sur i --
     end if;

     S2min := float'last;

     if k=np_arc then -- cas trivial: seule la cubique avant est calcul�e !

	-- La cubique arriere est assimil�e � un segment de droite :
        DX:= float(Tarc(k).coor_x - pt_i2.coor_x);
        DY:= float(Tarc(k).coor_y - pt_i2.coor_y);
        if DX/=0.0 then
          --phi2:= Atan2(DY,DX);
            phi2:= Arctan(DY,DX);
	else
	  if DY	> 0.0 then phi2:= PI/2.0; else phi2:= - PI/2.0; end if;  
	end if;
        a2:= 0.0;
        x2:= integer(Sqrt(DX**2 + DY**2));
        for i in 1..part_angle+1 loop Slar(k,i):= (x2,0); end loop;
        S2:= 0.0;
        S2min:= 0.0;

     else
      -- Calcul d'une CUBIQUE ARRIERE pour chaque variation d'angle Dphiav
      for i in 1..part_angle+1 loop 

        Phicar:= phiar1 + float(i-1)*Dphiar;
	if Slar(k,i).coor_x < 1 then -- Risque de pt de rebroussement !!???
	  s2:= float'last;
	else
	  ar:= float(slar(k,i).coor_y) / float(slar(k,i).coor_x)**3;

	  -- Calcul de l'aire (en valeur absolue) entre la cubique arriere
	  -- et le troncon qu'elle approxime :
	  s2:= 0.0;
          p1:= (0,0);
	  for kk in reverse k..np_arc-1 loop
	     p2:=slar(kk,i);
             s2:= s2 + abs( (ar*(float(p2.coor_x)**4-float(p1.coor_x)**4)/4.0) 
			-float((p2.coor_x-p1.coor_x)*(p2.coor_y+p1.coor_y)/2));
	     D:= Distance_max(p1,p2,ar*float(p1.coor_x)**3,ar*float(p2.coor_x)**3);
	     if S2>S2min then
		exit;
	     else
	     	if D>Dmax then Dmax:= D; end if;
		p1:=p2;
	     end if;
	  end loop;
	end if;

        if s2<s2min then
	  s2min:= s2;
	  phi2:= phicar;
	  x2:= slar(k,i).coor_x;
	  a2:= ar;
	  darmax:= dmax;
	end if;
      end loop; -- sur i --
     end if;

     if s1min = float'last or s2min = float'last then
	sommin:= float'last;
     else
        Sommin:= S1min + S2min;
     end if;

     if Sommin < sommina then
	sommina:= sommin;
	phi1a:=phi1;
	phi2a:=phi2;
 	x1a:= x1;
 	x2a:= x2;
	a1a:=a1;
	a2a:=a2;
	Davmaxa:= Davmax;
	Darmaxa:= Darmax;
     end if;

  end loop; -- sur k --

  surface:= sommina;

  free_ppliste(slav);
  free_ppliste(slar);

end Recherche_sommet;




-- ============================================================================
-- Procedure qui prend une ligne en entree TAB_POINTS et renvoie une liste 
-- de cubiques en sortie TAB_CUBIQUES. Les parametres sont :
-- - SIGMA (0..150) qui permet de fixer le nb de points pour le lissage
-- - TOLERANCE (en metres): tol. sur l'angle polaire, tangente au point d'infl.
-- ============================================================================
Procedure CHANG_PTS_CUB(arc_courant          : in point_liste_type;
                        ncourant             : in natural;
                        tab_cubiques         : out cubique_liste_type;
                        n_cub                : out positive;
			sigma                : in float;
			pas 		     : in float;
			tolerance            : in float;
			demi_tolerance_max   : in float;
 			produit_vectoriel_min: in float;
			part_angle           : in integer) is


Type liens_neg_type is array(integer range <>) of integer;
Type reel_neg_type is array(integer range <>) of float;
Type reel_access_neg_type is access reel_neg_type;

 -- ################################
 -- Variables relatives aux CUBIQUES
 -- ################################
 c1, c2: float;
 sigma_prime: float:= sigma;
 k1: natural:= 4 * natural(sigma);

 n_points,n_points_lisses: natural;
 n_aux: natural := 1;
 lgr_segments: liens_array_type(1..2000);
 l,k: natural := 0;
 alm, alp: float := 0.0;
 inflex, x1,y1,x2,y2, pred_i1, pred_i2: integer; 
 DX, DY: float;
 pt_i1, pt_i2: gen_io.point_type;
 -- !!! Les angles sont en RAD !!!
 phi1,phi2, tolphi1,tolphi2, phiav1,phiav2, phiar1,phiar2, Dphiav,Dphiar: float;
                 
 -- Param�tres des 2 cubiques avant (1) et arriere (2) :
 av, ar         : float:= 0.0;
 phiav, phiar   : float:= 0.0;
 xav, xar       : integer:= 0;
 Davmax, Darmax : float:= 0.0;             
 Sommin         : float:= 0.0;
                        
 Tcub : cubique_liste_type(1..ncourant+100);
 ncub : positive := 1;


-- Tableau des poids pour moyenner les points GS et les produits vectoriels GSI:
-- Tableau des produits vectoriels TPROVE et des prod. vect. lisses TPROVES:
-- Tableau des angles polaires etre les segments TPHI:
-- Tableau des tolerances de ces angles en fonction des lgr. des segm. TTOLPHI:
-- Tableau des coordonnees des points d'inflexion TAB_POINTS_INFLEX:
-- Tableau de rang du point de la ligne precedant le pt d'inflex. TAB_PRED_PI:

Ligne			  : point_liste_type(1..ncourant);      -- X,Y
Ligne_lisse		  : point_liste_reel(1..ncourant);      -- XS,YS
Nb_segments		  : liens_neg_type(1-k1..ncourant+k1);  -- ILONG
Gsi 			  : reel_tableau_type(0..k1);           -- GS, GSI
np: natural := ncourant - 2; ----------------------------------------------
nb_pts_inflex: natural:= 0; ----------------------------------- -- NKINF --
Tpoints_pred_pi	  	  : liens_array_type(1..ncourant);      -- TKINF

--PI:constant float:=3.14159265;

BEGIN
  ------------------------------------------------------------------------------
  -- Elimination de points trop proches pour le pas de d�coupage
  ------------------------------------------------------------------------------
  -- ATTENTION � ne pas supprimer comme �a tous les pts de la ligne !!! ??
  n_points:=1;
  Ligne(1):=arc_courant(1);
 
  For i in 2..ncourant-1 loop
    DX := float(arc_courant(i).coor_x - arc_courant(i-1).coor_x);
    DY := float(arc_courant(i).coor_y - arc_courant(i-1).coor_y);
    if Sqrt(DX*DX + DY*DY) >= Pas then
       	n_points:= n_points + 1;
	ligne(n_points):= arc_courant(i);		
    end if;
  end loop;

  -- Cas ou le dernier segment de la ligne est trop petit :
  DX := float(arc_courant(ncourant).coor_x - arc_courant(ncourant-1).coor_x);
  DY := float(arc_courant(ncourant).coor_y - arc_courant(ncourant-1).coor_y);
  if Sqrt(DX*DX + DY*DY) >= Pas then
       	n_points:= n_points + 1;
  end if;
  ligne(n_points):= arc_courant(ncourant);		


 declare
    ligne_aux: point_liste_reel(1..(10+n_points)*100);
 begin             
  ------------------------------------------------------------------------------
  -- Decoupage de la ligne en segments equidistants :
  ------------------------------------------------------------------------------
  Decompose_ligne(ligne,n_points,ligne_aux,n_aux,pas,lgr_segments);

  ------------------------------------------------------------------------------
  -- Prolongement de la ligne aux extremites par les symetriques :
  ------------------------------------------------------------------------------
  for i in 1..n_points-1 loop
     nb_segments(i):=lgr_segments(i);
  end loop;

  for i in 1..k1 loop
     nb_segments(1-i):=nb_segments(i);
     nb_segments(n_points+i-1):=nb_segments(n_points-i);
  end loop;    


  ------------------------------------------------------------------------------
  -- Lissage de la ligne par convolution avec un filtre Gaussien :
  ------------------------------------------------------------------------------

 declare
    ligne_aux_lisse: point_liste_reel(1..n_aux);
 begin

  Filtre_Gaussien_r(ligne_aux,n_aux,sigma,ligne_aux_lisse);

  ------------------------------------------------------------------------------
  -- Extraction des points correspondant aux homologues des points initiaux :
  ------------------------------------------------------------------------------
  l:= 1;
  Ligne_lisse(1):= Ligne_aux_lisse(1);

  for i in 2..n_points loop
    l:= l + nb_segments(i-1);
    Ligne_lisse(i):= Ligne_aux_lisse(l);
  end loop;

  end; -- declare pour ligne_aux_lisse
 end; -- declare pour ligne_aux



  -- Calcul de k1 : nb de points en jeu pour le lissage --
  k1:= 4 * natural(sigma);
  if k1 >= n_points then
     	sigma_prime:= float(n_points/4);
	k1:= 4 * natural(sigma_prime) - 1;
  end if;

  c1:=1.0/(sigma_prime*sqrt(2.0*PI));
  c2:=-1.0/(2.0*(sigma_prime**2));
  for k in 0..k1 loop
     Gsi(k):=c1*exp(c2*float(k**2)); -- = Gs(k) !!!
  end loop;

  ------------------------------------------------------------------------------
  -- Calcul des produits vectoriels et lissage par filtre Gaussien des pv :
  -- ! Division des pv par la longueur sinon le filtre est trop puissant !
  -----------------------------------------------------------------------------
 Declare
    np : integer := n_points - 2;              -- n_points_lisses - 2;
    Tprove_aux   : reel_tableau_type(1..np); 
    Tprove       : reel_neg_type(1-k1..np+k1); -- TPROVE 
    Tprove_lisse : reel_tableau_type(1..np);   -- TPROVES
    Tpoints_inflex : point_liste_type(1..n_points);
 begin

  for i in 1..np loop   -- ATTENTION AUX POINTS DOUBLES !!!
   Tprove_aux(i):=p_vectoriel(ligne_lisse(i),ligne_lisse(i+1),ligne_lisse(i+2));
  end loop;

  -----------------------------------------------------------------------------
  -- Prolongement des produits vectoriels aux extremites par symetrie ---------
  -----------------------------------------------------------------------------
  for i in 1..np loop 
	Tprove(i):= Tprove_aux(i); 
  end loop;
  for i in 1..k1 loop
     Tprove(1-i) := 2.0*Tprove(1)  - Tprove(i+1);
     Tprove(np+i):= 2.0*Tprove(np) - Tprove(np-i);
  end loop;
          
  -----------------------------------------------------------------------------
  -- Lissage des pv. par une convolution avec une Gaussienne ------------------
  -----------------------------------------------------------------------------
  for i in 1..np loop
     Tprove_lisse(i):= Gsi(0)*Tprove(i);
     for k in 1..k1 loop
	alp:= alp+float(nb_segments(i+k));
	alm:= alm+float(nb_segments(i-k+1));
	Tprove_lisse(i):= Tprove_lisse(i)
		      	+ (Tprove(i+k)/alp+Tprove(i-k)/alm) * Gsi(k);
     end loop;
  end loop;


  ------------------------------------------------------------------------------
  -- Detection des changements de courbure et des pts pred les pts d'inflexion :
  ------------------------------------------------------------------------------
  Det_points_inflexion(Tprove_lisse, np, produit_vectoriel_min, 
			Tpoints_pred_pi, nb_pts_inflex); 

  x1:= ligne(1).coor_x;
  y1:= ligne(1).coor_y;
  x2:= ligne(2).coor_x;
  y2:= ligne(2).coor_y;
  Tpoints_inflex(1):= (x1,y1);

  if nb_pts_inflex > 2 then -- La ligne a au moins une inflexion !!!

   for rang in 2..nb_pts_inflex-1 loop  
                  
     inflex:= Tpoints_pred_pi(rang);

     -- Estimation des coord. du pt d'inflex au milieu du segment du point pred 
     -- au point suivant:
     Tpoints_inflex(rang).coor_x:=(ligne(inflex).coor_x+ligne(inflex+1).coor_x)/2;
     Tpoints_inflex(rang).coor_y:=(ligne(inflex).coor_y+ligne(inflex+1).coor_y)/2;

     -- Si au moins 4 points de la ligne ( PARAMETRE ???) separent 
     -- le pt d'inflex pred ou le suivant du pt d'inflex considere, alors 
     -- l'angle est moyenne sur 4 points au lieu de 2 (pred et succ.)
     if (Tpoints_pred_pi(rang+1)-inflex<=4) OR (inflex-Tpoints_pred_pi(rang-1)<=4) then
	x1:=ligne(inflex).coor_x;	
	y1:=ligne(inflex).coor_y;
	x2:=ligne(inflex+1).coor_x;	
	y2:=ligne(inflex+1).coor_y;
     else
	x1:=(ligne(inflex).coor_x+ligne(inflex-1).coor_x)/2;	
	y1:=(ligne(inflex).coor_y+ligne(inflex-1).coor_y)/2;
	x2:=(ligne(inflex+1).coor_x+ligne(inflex+2).coor_x)/2;	
	y2:=(ligne(inflex+1).coor_y+ligne(inflex+2).coor_y)/2;
     end if;	
   end loop;

  end if;                

  x2:=ligne(n_points).coor_x;	
  y2:=ligne(n_points).coor_y;
  Tpoints_inflex(nb_pts_inflex):=(x2,y2);
  x1:=ligne(n_points-1).coor_x;	
  y1:=ligne(n_points-1).coor_y;

 end ; -- Declare --


 -- ============================================================================
 -- ------------------------ Calcul des cubiques -------------------------------
 -- ============================================================================
 Declare
    Tphi   : reel_tableau_type(1..nb_pts_inflex);  
    Ttolphi : reel_tableau_type(1..nb_pts_inflex);  
    Tpoints_inflex : point_liste_type(1..nb_pts_inflex); -- TXI,TYI
 begin

  ------------------------------------------------------------------------------
  -- Determination des coordonnees des pts d'inflexion, des angles
  -- et de leur tolerance :
  ------------------------------------------------------------------------------
  Det_angles_phi(ligne, n_points, Tpoints_pred_pi, nb_pts_inflex, tolerance,
		 demi_tolerance_max, Tpoints_inflex, Tphi, Ttolphi); 


  ------------------------------------------------------------------------------
  -- Determination des parametres des 2 cubiques et du meilleur sommet 
  -- entre les points d'inflexion :
  ------------------------------------------------------------------------------
  For inflex in 1..nb_pts_inflex - 1 loop

    pred_i1:= Tpoints_pred_pi(inflex);   -- pt precedant la premiere inflexion
    pred_i2:= Tpoints_pred_pi(inflex+1); -- pt precedant l'inflexion suivante
    pt_i1:= Tpoints_inflex(inflex);      -- premier point d'inflexion
    pt_i2:= Tpoints_inflex(inflex+1);    -- point d'inflexion suivant

    -- Cas ou les 2 P.I. sont separes par 1 seul sommet => 2 demi-segments
    -- Traitement de ce cas comme une cubique "d�g�n�r�e" !!!
    If pred_i2 = pred_i1+1 then

     DX:= float(ligne(pred_i2).coor_x - pt_i1.coor_x);
     DY:= float(ligne(pred_i2).coor_y - pt_i1.coor_y);
     if DX/=0.0 then
       --phiav:= Atan2(DY,DX);
         phiav:= Arctan(DY,DX);
     else
       if DY>0.0 then phiav:= PI/2.0; else phiav:= - PI/2.0; end if;  
     end if;
     av:= 0.0;
     xav:= integer(Sqrt(DX**2 + DY**2));

     DX:= float(ligne(pred_i2).coor_x - pt_i2.coor_x);
     DY:= float(ligne(pred_i2).coor_y - pt_i2.coor_y);
     if DX/=0.0 then
       --phiar:= Atan2(DY,DX);
         phiar:= Arctan(DY,DX);
     else         
       if DY>0.0 then phiar:= PI/2.0; else phiar:= - PI/2.0; end if;  
     end if;
     ar:= 0.0;
     xar:= integer(Sqrt(DX**2 + DY**2));

    Else

     phi1:= Tphi(inflex);
     if Tphi(inflex+1) < 0.0 then
     	phi2:= Tphi(inflex+1) + PI;
     else
	phi2:= Tphi(inflex+1) - PI;
     end if;
     Tolphi1:= Ttolphi(inflex);
     Tolphi2:= Ttolphi(inflex+1);
     Phiav1:= phi1 - tolphi1; 
     Phiav2:= phi1 + tolphi1; 
     Phiar1:= phi2 - tolphi2; 
     Phiar2:= phi2 + tolphi2; 
     Dphiav:=(phiav2-phiav1)/float(part_angle);--Partage de l'intervalle de tol.
     Dphiar:=(phiar2-phiar1)/float(part_angle);--des angles phiav et phiar --

     --------------------------------------------------------------------------
     -- Determination des parametres des 2 cubiques et du meilleur sommet
     -- en faisant varier l'angle selon la tolerance precedamment calculee :
     Recherche_sommet( ligne, n_points, pred_i1, pred_i2, pt_i1, pt_i2, 
		phiav1, phiar1, Dphiav, Dphiar, part_angle,
		av, ar, phiav, phiar, xav, xar, Davmax, Darmax, Sommin);

     --------------------------------------------------------------------------
     -- Detection et traitement des zones critiques :
     -- Zones_critiques (  );

    End if;

    ---------------------------------------------------------------------------
    -- Initialisation du tableau de cubiques TAB_CUBIQUES :
    Tcub(ncub).a:= av;
    Tcub(ncub).point_inflexion:= pt_i1;
    Tcub(ncub).Xlocal_sommet:= xav;
    Tcub(ncub).phi:= phiav;

    ncub:= ncub+1;
    Tcub(ncub).a:= ar;
    Tcub(ncub).point_inflexion:= pt_i2;
    Tcub(ncub).Xlocal_sommet:= xar;
    Tcub(ncub).phi:= phiar;

    if inflex/=nb_pts_inflex-1 then ncub:= ncub+1; end if;
    
  end loop; -- sur inflex --

 end ; -- Declare --

 for i in 1.. ncub loop 
	tab_cubiques(i):=Tcub(i);
 end loop;
 n_cub:= ncub;

end CHANG_PTS_CUB;

-- ============================================================================
end Polycubique;
