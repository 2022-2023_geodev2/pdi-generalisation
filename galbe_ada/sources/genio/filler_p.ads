Package Filler_p is

Type Octet_type is range 0..255;
For  Octet_type'Size use 8;

Type Filler_type is array(natural range<>) of Octet_type;

end Filler_p;
