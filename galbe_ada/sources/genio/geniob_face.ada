-- ===========================================================================
Separate(gen_io) 
Function  GR_FACE (graphe : Graphe_type; face : positive) return face_type is
begin
   return Graphe.Tfac(face);
end;

-- ===========================================================================
Separate(gen_io) 
Function  GR_ARCS_DE_FACE (graphe : graphe_type; face : positive) 
             return liens_array_type is
A_lire,P_lire,First,N,reste : integer;
My_liste : Liens_array_type(1..graphe.info.max_arc_fac);
ch : string(1..10);
nch : integer;
 package int_io is new text_io.integer_io(integer); use int_io;    

begin
    
   A_lire:=ceil(graphe.tfac(face).premier_arc,128);
  
   begin
   if A_lire/=graphe.lbld then
      read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      graphe.lbld:=a_lire;
   end if;
   
exception when liens_io.end_error=>
   put_line("fin de fichier");
   end;
   
   P_lire:=modulo(graphe.tfac(face).premier_arc-1,128);
   Reste:=graphe.tfac(face).nombre_arc;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,128-p_lire);
      My_liste(First+1..First+N):=Graphe.Bdom(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_dom,graphe.bdom);
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop; 
   return my_liste(1..Graphe.tfac(face).nombre_arc);
end;


-- =====================================================================
Separate(gen_io) 
Procedure ECRIRE_DOMAINES_FIN(graphe : in out graphe_type;
                              tab_arcs : liens_array_type;
                              n : positive ) is

-- ecriture des domaines (liste de numero d arcs) en fin de fichier des domaines

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_domaines+1,128);
   if A_lire/=graphe.lbld then
      if modulo(graphe.info.nb_domaines,128)/=0 then
         read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      end if;
      graphe.lbld:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_domaines,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bdom(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop;  
end;

-- =====================================================================
Separate(gen_io) 
Procedure ECRIRE_DOMAINES(graphe : in out graphe_type;
                          tab_arcs : liens_array_type;
                          n : positive;
                          premier : positive ) is

-- reecriture des domaines (liste de numero d arcs) a la meme adresse du buffer
-- de lien (on ecrit donc une liste dont le nombre d element est inferieur ou 
-- egal a la liste de depart)


A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,128);
   if A_lire/=graphe.lbld then
      read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      graphe.lbld:=a_lire;
   end if;
   P_lire:=modulo(premier-1,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bdom(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_dom,graphe.bdom);
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop;  
end;


-- ===========================================================================
Separate(Gen_io)
Function GR_ENCOMBREMENT_FACE(Graphe : graphe_type;
                           face : positive) 
                           return boite_type is

my_liste : Liens_array_type(1..graphe.info.max_arc_fac);
encombr_face : boite_type;
  package int_io is new text_io.integer_io(integer); use int_io;    

begin
   my_liste(1..Graphe.tfac(face).nombre_arc):=GR_ARCS_DE_FACE(Graphe,face);
   encombr_face.minimum.coor_x:=Graphe.info.delta_x;
   encombr_face.minimum.coor_y:=Graphe.info.delta_y;
   encombr_face.maximum.coor_x:=0;
   encombr_face.maximum.coor_y:=0;
   for i in 1..Graphe.tfac(face).nombre_arc loop
      encombr_face.minimum.coor_x:=MIN(graphe.tarc(ABS(my_liste(i))).encombr.
                                   minimum.coor_x,encombr_face.minimum.coor_x);
      encombr_face.minimum.coor_y:=MIN(graphe.tarc(ABS(my_liste(i))).encombr.
                                   minimum.coor_y,encombr_face.minimum.coor_y);
      encombr_face.maximum.coor_x:=MAX(graphe.tarc(ABS(my_liste(i))).encombr.
                                   maximum.coor_x,encombr_face.maximum.coor_x);
      encombr_face.maximum.coor_y:=MAX(graphe.tarc(ABS(my_liste(i))).encombr.
                                   maximum.coor_y,encombr_face.maximum.coor_y);
   end loop;
   return encombr_face;
end;


-- ===========================================================================
Separate(Gen_io)
Procedure GR_FACES_IN_RECTANGLE(graphe : graphe_type;
                                F_point : Point_type;
                                S_point : Point_type;
                                tab_faces : out liens_array_type;
                                 n : out natural) is

flags : array(1..graphe.info.nb_faces) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_f,i_f : integer;
face : face_type;
encombr_face : boite_type;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_f:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_f.all(ir+j).count-1 loop
            i_f:=graphe.rblk_f.all(graphe.radr_f.all(ir+j).first+k);
            if not flags(i_f) then
               flags(i_f):=true;
               encombr_face:=GR_ENCOMBREMENT_FACE(graphe,i_f);
               face:=GR_FACE(graphe,i_f);
               if encombr_face.minimum.coor_x <= upper.coor_x and 
                  encombr_face.minimum.coor_y <= upper.coor_y and 
                  encombr_face.maximum.coor_x >= lower.coor_x and
                  encombr_face.maximum.coor_y >= lower.coor_y and
                  face.Nombre_arc/=0 then
                  n_f:=n_f+1;
                  tab_faces(n_f):=i_f;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_f;
end;


-- ===========================================================================
Separate(Gen_io)
Procedure GR_CREER_FACE(graphe : in out graphe_type;
                        tab_arcs : liens_array_type;
                        n : positive;
                        att_graph : natural;
                        face : out positive) is 

-- Creation d une face a partir d une liste de n arcs constituant le contour de
-- la face; ces arcs sont classes dans l ordre de description de la face : 
-- contour exterieur continu puis contours interieurs (trous) continus

arc_debut : integer;
trou : boolean;
num_face : positive;
compteur : natural;
liste_arc_face : liens_array_type(1..n);
rx1,rx2,ry1,ry2,ir : integer;
encombr_face : boite_type;

begin
   -- initialisation facile
   num_face:=graphe.info.nb_faces+1;
   face:=num_face;
   -- mise a jour du fichier des domaines
   trou:=false;
   compteur:=0;
   arc_debut:=tab_arcs(1);
   for i in 1..n loop
       compteur:=compteur+1;
       liste_arc_face(compteur):=tab_arcs(i);
       if tab_arcs(i)>0 then
          graphe.tarc(tab_arcs(i)).face_d:=num_face;
          if graphe.tarc(tab_arcs(i)).arc_sui=arc_debut then
             -- on a 1 face : determinons les elements de la face (face ou trou)
             graphe.info.nb_faces:=graphe.info.nb_faces+1;
             graphe.tfac(graphe.info.nb_faces).premier_arc:=graphe.info.nb_domaines+1;
             graphe.tfac(graphe.info.nb_faces).nombre_arc:=compteur;
             graphe.tfac(graphe.info.nb_faces).att_graph:=att_graph;
             if att_graph>graphe.info.nb_graph_z then
                graphe.info.nb_graph_z:=att_graph;
             end if;

             if trou=false then
                graphe.tfac(graphe.info.nb_faces).zone_englob:=0;
             else
                graphe.tfac(graphe.info.nb_faces).zone_englob:=num_face;
             end if;

             -- on passe au trou suivant s il existe
             if i/=n then 
                graphe.tfac(graphe.info.nb_faces).trou_sui:=graphe.info.nb_faces
                                                            +1;
                trou:=true;
                arc_debut:=tab_arcs(i+1);
             else
                graphe.tfac(graphe.info.nb_faces).trou_sui:=0;
             end if;

             -- ecriture du domaine
             ECRIRE_DOMAINES_FIN(graphe,liste_arc_face,compteur);
             graphe.info.nb_domaines:=graphe.info.nb_domaines+compteur;
             if compteur>graphe.info.max_arc_fac then
                graphe.info.max_arc_fac:=compteur;
             end if;
             compteur:=0;   
          end if;          
       else
          graphe.tarc(-tab_arcs(i)).face_g:=num_face;
          if graphe.tarc(-tab_arcs(i)).arc_pre=arc_debut then
             -- on a 1 face : determinons les elements de la face (face ou trou)
             graphe.info.nb_faces:=graphe.info.nb_faces+1;
             graphe.tfac(graphe.info.nb_faces).premier_arc:=graphe.info.nb_domaines+1;
             graphe.tfac(graphe.info.nb_faces).nombre_arc:=compteur;
             graphe.tfac(graphe.info.nb_faces).att_graph:=att_graph;
             if att_graph>graphe.info.nb_graph_z then
                graphe.info.nb_graph_z:=att_graph;
             end if;

             if trou=false then
                graphe.tfac(graphe.info.nb_faces).zone_englob:=0;
             else
                graphe.tfac(graphe.info.nb_faces).zone_englob:=num_face;
             end if;

             -- on passe au trou suivant s il existe
             if i/=n then 
                graphe.tfac(graphe.info.nb_faces).trou_sui:=graphe.info.nb_faces
                                                            +1;
                trou:=true;
                arc_debut:=tab_arcs(i+1);
             else
                graphe.tfac(graphe.info.nb_faces).trou_sui:=0;
             end if;

             -- ecriture des domaines
             ECRIRE_DOMAINES_FIN(graphe,liste_arc_face,compteur);
             graphe.info.nb_domaines:=graphe.info.nb_domaines+compteur;
             if compteur>graphe.info.max_arc_fac then
                graphe.info.max_arc_fac:=compteur;
             end if;
             compteur:=0;   
          end if;          
       end if;
   end loop;

   -- Mise a jour eventuelle de la regionalisation
   if graphe.pasx/=0 then
      encombr_face.minimum.coor_x:=Graphe.info.delta_x;
      encombr_face.minimum.coor_y:=Graphe.info.delta_y;
      encombr_face.maximum.coor_x:=0;
      encombr_face.maximum.coor_y:=0;
      for i in 1..n loop
          encombr_face.minimum.coor_x:=MIN(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .minimum.coor_x,encombr_face.minimum.coor_x);
                                    
          encombr_face.minimum.coor_y:=MIN(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .minimum.coor_y,encombr_face.minimum.coor_y);
          encombr_face.maximum.coor_x:=MAX(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .maximum.coor_x,encombr_face.maximum.coor_x);
          encombr_face.maximum.coor_y:=MAX(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .maximum.coor_y,encombr_face.maximum.coor_y);
      end loop;

      rx1:=ceil(encombr_face.minimum.coor_x,graphe.pasx);
      rx2:=ceil(encombr_face.maximum.coor_x,graphe.pasx);
      ry1:=ceil(encombr_face.minimum.coor_y,graphe.pasy);
      ry2:=ceil(encombr_face.maximum.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for i in ry1..ry2 loop
          for j in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                            graphe.rfin_f,graphe.rvid_f,num_face,ir);
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_DETRUIRE_FACE(graphe : in out graphe_type;
                           face : positive) is 

my_face : face_type;
tab_arcs : liens_array_type(1..graphe.info.Max_arc_fac);
parcelle : natural;

begin
   parcelle:=face;
   while parcelle/=0 loop
      my_face:=GR_FACE(Graphe,parcelle);
      tab_arcs(1..my_face.nombre_arc):=GR_ARCS_DE_FACE(graphe,parcelle);
 
      -- elimination de la parcelle (face si premier passage dans la boucle)
      --                            (trou si passage dans la boucle > 1)
      graphe.tfac(parcelle).nombre_arc:=0;
      graphe.tfac(parcelle).trou_sui:=0;
      graphe.tfac(parcelle).zone_englob:=0;
 
      -- mise a jour des faces gauches ou droites des arcs bordant la parcelle
      for i in 1..my_face.nombre_arc loop
          if graphe.tarc(ABS(tab_arcs(i))).face_g=face then
             graphe.tarc(ABS(tab_arcs(i))).face_g:=0;
          else
             graphe.tarc(ABS(tab_arcs(i))).face_d:=0;
          end if;
      end loop;

      -- Passage au trou suivant s il existe
      parcelle:=my_face.trou_sui;
   end loop;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_MOD_ARCS_DE_FACE(graphe : In out Graphe_type;
                              face : positive;
                              tab_arcs : liens_array_type;
                              n : natural) is

-- Modification de la liste des arcs constituant le contour d une face ou d un
-- trou

begin
   if n<=graphe.tfac(face).nombre_arc then
      -- puisqu on diminue ou egale le nombre d arc de la face, on peut reecrire
      -- dans les memes positions du buffer d arcs
      ECRIRE_DOMAINES(graphe,tab_arcs,n,graphe.tfac(face).premier_arc);
   else
      -- puisqu on augmente le nombre d arc intermediaires, on ne peut pas
      -- reecrire dans les memes positions du buffer d arcs; on reecrira en fin
      -- de fichier du buffer d arcs
      ECRIRE_DOMAINES_FIN(graphe,tab_arcs,n);
      graphe.tfac(face).premier_arc:=graphe.info.nb_domaines+1;
      graphe.info.nb_domaines:=graphe.info.nb_domaines+n;
      if n>graphe.info.max_arc_fac then
         graphe.info.max_arc_fac:=n;
      end if;
   end if;
   graphe.tfac(face).nombre_arc:=n;

   -- mise a jour de la valeur de face_d ou face_g ?

   -- Mise a jour de la regionalisation ?   
end;

-- =====================================================================
Separate(Gen_io)
Procedure GR_MOD_ATT_FACE (graphe : In out graphe_type; face : positive;
                            att_graph : natural) is

begin
   graphe.tfac(face).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_z then
      graphe.info.nb_graph_z:=att_graph;
   end if;
end;


-- ======================================================================
Separate(Gen_io) Procedure GR_CONTOURS_DE(graphe : graphe_type; 
                                          F : Positive;
                                          S : in out Surf_type) is

Myfac : Gen_io.Face_type;
Myarc : Gen_io.Arc_type;
Taf   : Liens_array_type(1..graphe.all.Info.Nb_arcs);
tab_points : Point_liste_type(1..graphe.all.info.max_pts_arc);
nb_points : natural;
Ip,Ip0    : Integer;
II,JJ : integer;
So    : Point_type;
Nf    : Integer;
begin
   Ip:=0;   
   S.NE:=0;
   Nf:=F;
   -- ----- La face ,a priori le contour exterieur ----
   Myfac:=gr_face(graphe,F);      
   loop
      S.Ne:=S.Ne+1;
      S.NA(S.Ne):=0;
      Ip0:=Ip+1;
      Taf(1..Myfac.Nombre_arc):=Gr_arcs_de_face(graphe,nf);
      for j in 1..myfac.nombre_arc loop
         Myarc:=gr_arc(graphe,Abs(taf(j)));
         Gr_points_d_arc(graphe,abs(taf(j)),tab_points,nb_points);
         S.Pts(ip+1..ip+nb_points):=tab_points(1..nb_points);
         If Taf(j)<0 then
            ii:=nb_points/2;
            jj:=Ip+nb_points;
            for k in 1..ii loop
               So:=S.Pts(ip+k);
               S.pts(ip+k):=S.pts(jj);
               S.pts(jj):=So;
               jj:=jj-1;
            end loop;
         end if;
         Ip:=Ip+nb_points-1;
         S.Na(S.ne):=S.na(s.ne)+nb_points-1;
      end loop;
      -- On ajoute le dernier point = au premier point du contour
      S.na(s.ne):=S.na(s.ne)+1;
      Ip:=Ip+1;
      S.pts(Ip):=S.pts(Ip0);
      Exit when Myfac.Trou_sui=0;
      Nf:=Myfac.Trou_sui;
      Myfac:=Gr_face(graphe,nf);
   end loop;  
end;

-- ======================================================================
With geometrie; Use Geometrie;
Separate(gen_io) Function GR_QUELLE_FACE(graphe : graphe_type; P : Gen_io.Point_type) 
         return natural is

is_in,n,ninterm,Min,imin : integer;
twf,twfinterm   : liens_array_type(1..graphe.all.info.nb_faces);
S     : surf_type(graphe.all.info.nb_faces,graphe.all.info.nb_points);
encombr : boite_type;

begin   
   is_in:=0;
   
   -- Liste des faces concernees --
   Gr_faces_in_rectangle(graphe,P,p,twf,n);
    
   -- On teste si P est dans la face, en s'arretant quand on en a trouve.
   ninterm:=0;
   for i in 1..n loop
      Gr_contours_de(graphe,twf(i),s);
      if Est_il_dans_surface(P,S) then 
         ninterm:=ninterm+1;
         twfinterm(ninterm):=twf(i); 
      end if;
   end loop;

   Min:=0;
   imin:=0;
   for i in 1..ninterm loop
       encombr:=Gr_encombrement_face(graphe,twfinterm(i));
       if encombr.minimum.coor_x > Min then 
          Min:=encombr.minimum.coor_x;
          imin:=i;
       end if;
   end loop;
   if imin=0 then
      is_in:=0;
   else
      is_in:=twfinterm(imin);
   end if;
   return is_in;
end;

-- =========================================================================
With geometrie; Use Geometrie;
Separate(Gen_io) Function GR_DIST_FACE(graphe : graphe_type; P : Point_type;
     F : Positive) return float is

dmin,d : float;
myface : Face_type;
tarc   : liens_array_type(1..graphe.info.Max_arc_fac);
S     : surf_type(graphe.info.nb_faces,graphe.info.nb_points);

begin   
   dmin:=float'last;
   Gr_contours_de(graphe,F,s);
   if Est_il_dans_surface(P,s) then 
      dmin:=0.0;
      return dmin;
   end if;
   myface:=GR_FACE(graphe,F);
   tarc(1..myface.nombre_arc):=GR_ARCS_DE_FACE(graphe,F);
   for i in 1..myface.nombre_arc loop
       d:=GR_DIST_ARC(graphe,P,tarc(i));
       if d<dmin then
          dmin:=d;
       end if;
   end loop;
   return dmin;
end;


