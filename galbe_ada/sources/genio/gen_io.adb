--******************************************************************************
-- Modifie le 18/06/1999 pour portage sous WindowsNT
--******************************************************************************

with Filler_p; Use Filler_p;
With Math_int_basic; Use Math_int_basic;
With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
With Direct_io;
With Sequential_io;
With Unchecked_deallocation;
With lissage_Filtrage; use lissage_Filtrage;
With text_io;Use text_io ;
With Geometrie;use Geometrie;

Package body Gen_io is

Taille_mini : constant integer:=100;
Subtype Point_buffer_type is Point_liste_type(1..128);
Subtype Cubique_buffer_type is Cubique_liste_type(1..50);
Type Arc_array_type is array(natural range<>) of Arc_type;
Type Noeud_array_type is array(natural range<>) of Noeud_type;
Type Face_array_type is array(natural range<>) of Face_type;
Subtype Liens_buffer_type is Liens_array_type(1..128);
Subtype Affichage_buffer_type is Affichage_array_type(1..128);

type tab_reel is array(positive range <>) of float;

Type Entete_type is record
   Info  : Info_type;
   Reste : Filler_type(1..440);
end record;

Type Regbuf_type is access Liens_array_type;
Type One_reg_type is record
   First : Natural:=0;
   Count : Natural:=0;
end record;
Type Array_reg_type is array(natural range <>) of One_reg_type;
Type Regadr_type is access Array_reg_type;

Package Arc_io is new sequential_io(Arc_type);              Use Arc_io;
Package Noeud_io is new sequential_io(Noeud_type);          Use Noeud_io;
Package Point_io is new direct_io(Point_Buffer_type);       Use Point_io;
Package Cubique_io is new direct_io(Cubique_Buffer_type);   Use Cubique_io;
Package Face_io is new sequential_io(Face_type);            Use Face_io;
Package Liens_io is new direct_io(Liens_buffer_type);       Use Liens_io;
Package Entete_io is new sequential_io(Entete_type);        Use Entete_io;
Package Legende_io is new direct_io(Affichage_buffer_type); Use Legende_io;

Type Graphe_Struc_type( Na : natural; Nn : natural; Nf : natural) is record
   Info  : Info_type;
   Tarc  : Arc_array_type(1..Na);
   Tnod  : Noeud_array_type(1..Nn);
   Tfac  : Face_array_type(1..Nf);
   F_pts : Point_io.file_type;
   Lblp  : Natural :=0;
   Bpts  : Point_buffer_type;
   F_cub : Cubique_io.file_type;
   Lblc  : Natural :=0;
   Bcub  : Cubique_buffer_type;
   F_lnk : Liens_io.file_type;
   Lbll  : Natural :=0;
   Blnk  : Liens_buffer_type;
   F_dom : Liens_io.file_type;
   Lbld  : Natural :=0;
   Bdom  : Liens_buffer_type;
   Mode  : Graphe_mode_type;
   -- information de regionalisation
   Pasx  : natural:=0;
   Pasy  : natural:=0;
   Nbregx: natural:=0;
   Nbregy: natural:=0;

   Radr_a : Regadr_type;  -- addresse dans le tableau rblk_a du 1er element
                          -- et nombre d elements
   Rblk_a : Regbuf_type;  -- tableau des arcs par region
   Rfin_a : natural:=0;   -- numero du dernier element de rblk_a
   Rvid_a : natural:=0;   -- nombre de vide dans le tableau rblk_a

   Radr_n : Regadr_type;  -- addresse dans le tableau rblk_n du 1er element
                          -- et nombre d elements
   Rblk_n : Regbuf_type;  -- tableau des noeuds par region
   Rfin_n : natural:=0;   -- numero du dernier element de rblk_n
   Rvid_n : natural:=0;   -- nombre de vide dans le tableau rblk_n

   Radr_f : Regadr_type;  -- addresse dans le tableau rblk_f du 1er element
                          -- et nombre d elements
   Rblk_f : Regbuf_type;  -- tableau des faces par region
   Rfin_f : natural:=0;   -- numero du dernier element de rblk_f
   Rvid_f : natural:=0;   -- nombre de vide dans le tableau rblk_f
end record;
--Pragma Component_alignment(Storage_unit,Graphe_struc_type);

Type Legende_Struc_type( Nlegl:natural; Nlegp:natural; Nlegz:natural) is record
   Tleg_l  : Affichage_array_type(0..Nlegl);
   Tleg_p  : Affichage_array_type(0..Nlegp);
   Tleg_z  : Affichage_array_type(0..Nlegz);
end record;


Arc_size     : constant string:= integer'image(Arc_type'size /8);
Point_size   : constant string:= integer'image(Point_buffer_type'size /8);
Cubique_size : constant string:= integer'image(Cubique_buffer_type'size /8);
Noeud_size   : constant string:= integer'image(Noeud_type'size /8);
Liens_size   : constant string:= integer'image(Liens_buffer_type'size /8);
Face_size    : constant string:= integer'image(Face_type'size /8);
Info_size    : constant string:= integer'image(Entete_type'size /8);
Affichage_size : constant string:= integer'image(Affichage_buffer_type'size /8);

Increment    : constant integer:=512;
Procedure Free_graphe is new unchecked_deallocation(graphe_struc_type,
          graphe_type);
procedure free_pliste is new unchecked_deallocation(
			point_liste_type, point_access_type);
procedure free_cliste is new unchecked_deallocation(
			cubique_liste_type, cubique_access_type);

Package Polyligne is

  Function LIRE(graphe : graphe_type;
                arc : positive)
                return Point_liste_type;

  Procedure ECRIRE_FIN(graphe : in out graphe_type;
                       tab_points : point_liste_type;
                       n : positive);
  Procedure ECRIRE(graphe : in out graphe_type;
                   tab_points : point_liste_type;
                   n : positive;
                   premier : positive );
end;

package body Polyligne is separate;

Package Polycubique is

  Function LIRE(graphe 	: graphe_type;
                arc 	: positive) return Cubique_liste_type;

  Procedure ECRIRE_FIN(graphe 	    : in out graphe_type;
                       tab_cubiques : cubique_liste_type;
                       n 	    : positive);

  Procedure ECRIRE(graphe 	: in out graphe_type;
                   tab_cubiques : cubique_liste_type;
                   n 		: positive;
                   premier 	: positive );

  Procedure CHANG_CUB_PTS(tab_cubiques	: in cubique_liste_type;
                          n_cub 	: in positive;
			  pas		: in integer;
                          tab_points 	: out point_liste_reel;
                          n_pts 	: out positive);

  Procedure CHANG_PTS_CUB(arc_courant       : in point_liste_type;
                          ncourant          : in natural;
                          tab_cubiques      : out cubique_liste_type;
                          n_cub             : out positive;
			  sigma             : in float;
			  pas		    : in float;
			  tolerance         : in float;
			  demi_tolerance_max: in float;
 			  produit_vectoriel_min: in float;
			  part_angle        : in integer);

  Procedure Recherche_sommet( tab_points    : in point_liste_type;
                            n_pts           : in natural;
			    pred_i1, pred_i2: in integer;
			    pt_i1, pt_i2    : in point_type;
			    phiav1, phiar1  : in float;
			    Dphiav, Dphiar  : in float;
			    part_angle      : in integer;
			    a1a, a2a        : out float;
			    phi1a, phi2a    : out float;
			    x1a, x2a        : out integer;
			    Davmaxa, Darmaxa: out float;
			    Surface         : out float );


  Procedure Det_points_inflexion( Tprove_lisse	 : in reel_tableau_type;
			          np		 : in natural;
				  produit_vectoriel_min : in float;
			          Tpoints_pred_pi: out liens_array_type;
				  nb_pts_inflex	 : out natural);

  Procedure Det_angles_phi(Tab_points      : in point_liste_type;
			 n_pts             : in natural;
			 Tpoints_pred_pi   : in liens_array_type;
			 nb_pts_inflex     : in natural;
			 tolerance         : in float;
			 demi_tolerance_max: in float;
			 Tpoints_inflex    : out point_liste_type;
	      		 PTphi		   : in out reel_tableau_type;
			 Ttolphi	   : in out reel_tableau_type );

end;

package body Polycubique is separate;

-- ===========================================================================
-- Fonction globale sur le graphe : GENIOB_GRAPHE

Function GR_DISK_INFOS (nom : string) return Info_type is
  Entete : Entete_type;
  Fichier: Entete_io.file_type;
  NomFic:string(1..80):=(1..80=>' ');
  N : integer;
begin
   NomFic(1..nom'length):=Nom;
   NomFic(nom'length+1..nom'length+4 ):=".GEI";
   begin
     Open (fichier, In_File, NomFic);
   Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
          Put(" Programme termine ... Appuyez sur une touche...");
          get_line(Nomfic,n);
          return Entete.Info;
   End;
   Read(Fichier, Entete);
   Close(Fichier);
   Return Entete.Info;
end;

Function ALLOCATE ( arcs : natural; noeuds : natural; faces: natural)
            return Graphe_type is

My_graphe  : Graphe_type;
Na, Nn, Nf : natural;
Begin
   Na:=MAX(arcs,taille_mini);
   Nn:=Max(noeuds,taille_mini);
   Nf:=max(faces,taille_mini);
   My_graphe:=new Graphe_struc_type(na,nn,nf);
   Return my_graphe;
end;

Function GR_LOAD_FROM_DISK ( nom : string; extension : natural:=10;
            mode: graphe_mode_type:=In_graphe ) return graphe_type is


My_graphe : Graphe_type;
Entete : Info_type;
Na, Nn, Nf : Natural;
Coef : float;
F_arc : Arc_io.file_type;
F_nod : Noeud_io.file_type;
F_fac : Face_io.File_type;
F_pts_aux : Point_io.File_type;
F_cub_aux : Cubique_io.File_type;
F_lnk_aux : Liens_io.File_type;
F_dom_aux : Liens_io.File_type;
Nomfic:string(1..80):=(1..80=>' ');

begin
  -- Recuperation des Infos sur le disque --
   entete:=Gr_disk_infos(nom);
   nomfic(1..nom'length):=nom;

   -- Evaluation des nouvelles tailles --
   Coef:=1.0 + Float(extension)/100.0;
   Na := Integer(Float(entete.nb_arcs+10)*coef);
   Nn := Integer(Float(entete.nb_noeuds+10)*coef);
   Nf := Integer(Float(entete.nb_faces+10)*coef);

   -- Creation d'un nouveau graphe en memoire --
   My_graphe:=ALLOCATE(na,nn,nf);
   My_graphe.info:=entete;

   -- Chargement en memoire des elements fixes --
   Begin
      Nomfic(Nom'length+1..Nom'length+4):=".GEA";
      Open(f_arc,in_file,Nomfic);
               begin

      For i in 1..entete.nb_arcs loop
             read(f_arc,my_graphe.tarc(i));
      end loop;
         Exception when TEXT_IO.END_ERROR =>
                close(f_arc);
         end;

      Close(f_arc);
      Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
   end;

   -- Chargement en memoire des elements fixes --
   Begin
        Nomfic(Nom'length+1..Nom'length+4):=".GEN";
   		Open(f_nod,in_file,Nomfic);
        For i in 1..entete.nb_noeuds loop
            begin
      			read(f_nod,my_graphe.tnod(i));

                  Exception when TEXT_IO.END_ERROR =>
                    close(f_nod);
                    exit;
            End;
   		end loop;
   		Close(f_nod);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2);
   End;

   -- Chargement en memoire des elements fixes --
   Begin
   		Nomfic(Nom'length+1..Nom'length+4):=".GEF";
   		Open(f_fac,in_file,Nomfic);
   		For i in 1..entete.nb_faces loop
            Begin
      			read(f_fac,my_graphe.tfac(i));
                Exception when TEXT_IO.END_ERROR =>
                close(f_fac);
                exit;
            End;
   		end loop;
   		Close(f_fac);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2);
   End;

   My_graphe.mode:=mode;
   -- Si le Graphe est Inout il faut recopier les fichiers bufferises
   if mode=Inout_Graphe then

      -- Fichier des Points
   Nomfic(Nom'length+1..Nom'length+4):=".GEP";
   Begin
   	  	Open(f_pts_aux,in_file,Nomfic);
      	Nomfic(Nom'length+1..Nom'length+8):=".GEP_TMP";
      	Create(My_graphe.f_pts,inout_file,nomfic);
      	for i in 1..ceil(entete.nb_points,128) loop
          begin
          	read(F_pts_aux,my_graphe.bpts);
          	write(My_graphe.f_pts,my_graphe.bpts);
            Exception when TEXT_IO.END_ERROR =>
                close(f_pts_aux);
                exit;
          End;
      	end loop;
        Close (F_pts_aux);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2);
       End;

      -- Fichier des Cubiques
    Nomfic(Nom'length+1..Nom'length+4):=".GEC";
   begin
        Open(F_Cub_aux, In_file,Nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GEC_TMP";
        Create(My_graphe.f_cub,inout_file,Nomfic);
        for i in 1..ceil(entete.nb_cubiques,50) loop
            begin
              read(F_cub_aux,my_graphe.bcub);
              write(My_graphe.f_cub,my_graphe.bcub);
              Exception when TEXT_IO.END_ERROR =>
                close(f_cub_aux);
                exit;
            End;
        end loop;
        Close (F_cub_aux);
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2);
      End;

      -- Fichier des Liens
      Nomfic(Nom'length+1..Nom'length+4):=".GEL";
        begin
        Open(F_lnk_aux, In_file,Nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GEL_TMP";
        Create(My_graphe.f_lnk,inout_file,Nomfic);
        for i in 1..ceil(entete.nb_liens,128) loop
            Begin
              read(F_lnk_aux,my_graphe.blnk);
              write(My_graphe.f_lnk,my_graphe.blnk);
              Exception when TEXT_IO.END_ERROR =>
                close(f_lnk_aux);
                exit;
            End;
        end loop;
        Close (F_lnk_aux);
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2);
      End;

      -- Fichier des Domaines
      Nomfic(Nom'length+1..Nom'length+4):=".GED";
      Begin
        Open(F_dom_aux, In_file,nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GED_TMP";
        Create(My_graphe.f_dom,inout_file,Nomfic);
        for i in 1..ceil(entete.nb_domaines,128) loop
            begin
              read(F_dom_aux,my_graphe.bdom);
              write(My_graphe.f_dom,my_graphe.bdom);
              Exception when TEXT_IO.END_ERROR =>
                close(f_dom_aux);
                exit;
            End;
        end loop;
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2);
        Close (F_dom_aux);
      End;

    else
       Nomfic(Nom'length+1..Nom'length+4):=".GEP";
          Begin
         Open(My_graphe.F_pts,In_file,nomfic);
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GEC";
          Begin
          Open(My_graphe.F_cub,In_file,nomfic);
          Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GEL";
               begin
       Open(My_graphe.F_lnk,In_file,nomfic);
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GED";
              Begin
       Open(My_graphe.F_dom,In_file,nomfic);
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
          end if;
   return my_graphe;
end;


Function  GR_INFOS(graphe : graphe_type) return info_type is

begin
   return graphe.info;
end;


Procedure GR_SAVE_TO_DISK (graphe : graphe_type; nom : string) is

F_inf : Entete_io.file_type;
F_arc : Arc_io.file_type;
F_nod : Noeud_io.file_type;
F_fac : Face_io.File_type;
F_pts_aux : Point_io.File_type;
F_cub_aux : Cubique_io.File_type;
F_lnk_aux : Liens_io.File_type;
F_dom_aux : Liens_io.File_type;
entete : entete_type;
tab_pts : point_liste_type(1..graphe.info.Max_pts_arc);
nb_pts,ptstotal,indice_pts,nbuf,max_pts : integer;
bufpts : point_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');
begin

   -- ecriture des points et des cubiques en condensant les fichiers

   Nomfic(Nom'length+1..Nom'length+4):=".GEP";
   Create(f_pts_aux,inout_file,Nomfic);
   Reset(graphe.f_pts);

   Nomfic(Nom'length+1..Nom'length+4):=".GEC";
   Create(f_cub_aux,inout_file,Nomfic);
   Reset(graphe.f_cub);

   Indice_pts:=1;
   Nbuf:=0;
   ptstotal:=0;
   max_pts:=0;
   For i in 1..graphe.info.nb_arcs loop
       if graphe.tarc(i).Mode_met=Ligne then
          gr_points_d_arc(graphe,i,tab_pts,nb_pts);
          -- mise a jour du maximum de points par arc
          if nb_pts > max_pts then
             max_pts:=nb_pts;
          end if;
          graphe.tarc(i).premier_met:=Indice_pts;
          ptstotal:=ptstotal+nb_pts;
          -- Bufferisation des points
          for j in 1..nb_pts loop
              Nbuf:=nbuf+1;
              if Nbuf=129 then
                 write(f_pts_aux,bufpts);
                 bufpts(1..128):=(others=>(0,0));
                 Nbuf:=1;
                 bufpts(Nbuf):=tab_pts(j);
              else
                 bufpts(Nbuf):=tab_pts(j);
              end if;
          end loop;
          Indice_pts:=Indice_pts+nb_pts;
       else
         -- meme traitement pour la liste de cubiques
         -- il faudrait faire le meme traitement, mais on n'utilise
         -- plus les cubiques
         null;
       end if;
   end loop;
   write(f_pts_aux,bufpts);
   Close(f_pts_aux);
   Graphe.Lblp:=0;
   Graphe.info.Nb_points:=ptstotal;
   Graphe.Info.Max_pts_arc:=max_pts;

   -- Ecriture des cubiques --
   For i in 1..ceil(graphe.info.nb_cubiques,50) loop
      read(graphe.f_cub,graphe.bcub);
      write(f_cub_aux,graphe.bcub);
   end loop;
   Close(f_cub_aux);
   Graphe.Lblc:=0;

   -- Ecriture des arcs --

   Nomfic(Nom'length+1..Nom'length+4):=".GEA";
   Create(f_arc,out_file,Nomfic);
   For i in 1..graphe.info.nb_arcs loop
      write(f_arc,graphe.tarc(i));
   end loop;
   Close(f_arc);

   -- Ecriture des noeuds --
   Nomfic(Nom'length+1..Nom'length+4):=".GEN";
   Create(f_nod,out_file,Nomfic);
   For i in 1..graphe.info.nb_noeuds loop
      write(f_nod,graphe.tnod(i));
   end loop;
   Close(f_nod);

   -- Ecriture des liens --
   Nomfic(Nom'length+1..Nom'length+4):=".GEL";
   Create(f_lnk_aux,inout_file,Nomfic);
   Reset(graphe.f_lnk);
   For i in 1..ceil(graphe.info.nb_liens,128) loop
      read(graphe.f_lnk,graphe.blnk);
      write(f_lnk_aux,graphe.blnk);
   end loop;
   Close(f_lnk_aux);

   -- Ecriture des faces --
   Nomfic(Nom'length+1..Nom'length+4):=".GEF";
   Create(f_fac,out_file,Nomfic);
   For i in 1..graphe.info.nb_faces loop
      write(f_fac,graphe.tfac(i));
   end loop;
   Close(f_fac);

   -- Ecriture des domaines --
   Nomfic(Nom'length+1..Nom'length+4):=".GED";
   Create(f_dom_aux,out_file,Nomfic);
   Reset(graphe.f_dom);
   For i in 1..ceil(graphe.info.nb_domaines,128) loop
      read(graphe.f_dom,graphe.bdom);
      write(f_dom_aux,graphe.bdom);
   end loop;
   Close(f_dom_aux);

   -- Ecriture des Infos --
   entete.info:=graphe.info;
   Nomfic(Nom'length+1..Nom'length+4):=".GEI";
   Create(f_inf,out_file,Nomfic);
   Write(f_inf,entete);
   Close(f_inf);

end;


Function  GR_CREATE_NEW ( deltax : Positive;
                          deltay : positive;
                          Resolution : Integer;
                          Echelle : Integer :=1;
                          Narcs : Positive :=100;
                          Nnoeuds : positive :=100;
                          Nfaces : Positive :=100;
                          Nom_tmp : string :="Unnamed_graphe"
                           )return graphe_type is

My_graphe : Graphe_type;
N : Integer;
Nomfic:string(1..120):=(1..120=>' ');

begin
   my_graphe := ALLOCATE(Narcs, Nnoeuds, Nfaces);
   my_graphe.Info.delta_x:=deltax;
   my_graphe.Info.delta_y:=deltay;
   my_graphe.Info.resolution:=resolution;
   my_graphe.Info.echelle:=echelle;
   my_graphe.mode:=Inout_graphe;


   -- autre initialisation nulle :
   my_graphe.Info.nb_arcs:=0;
   my_graphe.Info.nb_noeuds:=0;
   my_graphe.Info.nb_faces:=0;
   my_graphe.Info.nb_points:=0;
   my_graphe.Info.nb_cubiques:=0;
   my_graphe.Info.nb_liens:=0;
   my_graphe.Info.nb_domaines:=0;
   my_graphe.Info.nb_graph_l:=0;
   my_graphe.Info.nb_graph_p:=0;
   my_graphe.Info.nb_graph_z:=0;
   my_graphe.Info.max_pts_arc:=0;
   my_graphe.Info.max_cub_arc:=0;
   my_graphe.Info.max_arc_neu:=0;
   my_graphe.Info.max_arc_fac:=0;

    nomfic(1..Nom_tmp'last):= Nom_tmp;
    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEP_TMP";
    Create(My_graphe.f_pts,inout_file,Nomfic);

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEC_TMP";
    Create(My_graphe.f_cub,inout_file,Nomfic);

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEL_TMP";
    Create(My_graphe.f_lnk,inout_file,Nomfic);

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GED_TMP";
    Create(My_graphe.f_dom,inout_file,Nomfic);

   return my_graphe;
end;


Procedure GR_DELETE ( graphe : in out Graphe_type) is
begin
   if graphe.mode=In_graphe then
      close(graphe.f_pts);
      close(graphe.f_cub);
      close(graphe.f_lnk);
      close(graphe.f_dom);
   else
      delete(graphe.f_pts);
      delete(graphe.f_cub);
      delete(graphe.f_lnk);
      delete(graphe.f_dom);
   end if;
   Gr_annule_region(Graphe);
   Free_graphe(Graphe);
end;


Function GR_IS_REGION(graphe : graphe_type) return boolean is
begin
   if graphe.pasx/=0 then
      return true;
   else
      return false;
   end if;
end;


Procedure GR_REGION(graphe : in out graphe_type;
                      Pasx : positive:=256;
                      Pasy : positive:=256) is
 package entier_io is new integer_io(integer); use entier_io;
-- Buffers intermediaires pour liste des regions par arcs --
Type Buffer_type;
Type Buffer_access_type is access Buffer_type;
Type Buffer_type is record
   Values : Liens_array_type(1..8192);
   Nextb  : Buffer_access_type;
end record;
PBuf_init,PBuf,old : Buffer_access_type;
Procedure Free_buf is new unchecked_deallocation(Buffer_type,
     Buffer_access_type);
Procedure Free_preg is new unchecked_deallocation(Liens_array_type,
     Regbuf_type);
--
-- Variables intermediaires ---
Nbreg,Nbregx,nbregy,somme,ir,ind,ind_1er_face_reg_i  : Natural;
face_prise : boolean;
Prega,Pregn,Pregf : Regadr_type;
Pregb,Prego,Pregg,Preggprov  : regbuf_type;
Rarcs : Liens_array_type(1..graphe.info.nb_arcs);
Rnods : Liens_array_type(1..graphe.info.nb_noeuds);
Rx1,rx2,ry1,ry2 : integer;
pas : positive ;

begin
   -- Modif FL du 30/12/97 : adaption du pas de regionalisation a la taille du
   -- fichier pour gerer les fichiers tres grands. Par defaut la valeur est
   -- 256*256
   -- On fixe apres experience qu'une taille superieure a 100*100 est interdite
   pas:=pasx;
   loop
     if max(1,graphe.info.delta_x/pas)*max(1,graphe.info.delta_y/pas)<10000 then
        exit;
     else
        pas:=pas*2;
     end if;
   end loop;

   --- Determination du nombre de regions ---
   Nbregx:=ceil(graphe.info.delta_x,pas);
   Nbregy:=ceil(graphe.info.delta_y,pas);

   Nbreg:=nbregx*nbregy;
   Prega:=new array_reg_type(1..Nbreg);
   Pregn:=new array_reg_type(1..Nbreg);
   Pregf:=new array_reg_type(1..Nbreg);

   --- Parcours des arcs et determination des regions auxquels ils
   --- appartiennent... en stockant l'info dans les tableaux values...

   Pbuf_init:= new Buffer_type;
   Pbuf:=pbuf_init;
   ind:=0;
   for i in 1..graphe.info.nb_arcs loop
      rx1:=ceil(graphe.tarc(i).encombr.minimum.coor_x,pas);
      rx2:=ceil(graphe.tarc(i).encombr.maximum.coor_x,pas);
      ry1:=ceil(graphe.tarc(i).encombr.minimum.coor_y,pas);
      ry2:=ceil(graphe.tarc(i).encombr.maximum.coor_y,pas);
      ir:=(ry1-1)*nbregx+rx1-1;
      for j in ry1..ry2 loop
         for k in rx1..rx2 loop
            ir:=ir+1;
            ind:=ind+1;
            if ind>pbuf.values'length then
               pbuf.nextb:=new buffer_type;
               pbuf:=pbuf.nextb;
               ind:=1;
            end if;
            pbuf.values(ind):=ir;
            prega.all(ir).count:=prega.all(ir).count+1;
         end loop;
         ir:=ir+nbregx-rx2+rx1-1;
      end loop;
      rarcs(i):=(rx2-rx1+1)*(ry2-ry1+1);
   end loop;

   --- Mise en place des valeurs First et preparation des compteurs ---
   Prega.all(1).first:=1;
   for i in 1..nbreg-1 loop
      Prega.all(i+1).first:=Prega.all(i).count+Prega.all(i).first;
      Prega.all(i).count:=0;
   end loop;
   Somme:=Prega.all(nbreg).First+prega.all(nbreg).count-1;
   Prega.all(nbreg).count:=0;

   --- Rangement des arcs et liberation de la memoire ---
   pregb:=new Liens_array_type(1..somme+increment);
   pbuf:=pbuf_init;
   ind:=0;
   for i in 1..graphe.info.nb_arcs loop
      for j in 1..rarcs(i) loop
         ind:=ind+1;
         if ind>pbuf.values'length then
            old:=pbuf;
            pbuf:=pbuf.nextb;
            free_buf(old);
            ind:=1;
         end if;
         ir:=pbuf.values(ind);
         pregb.all(prega.all(ir).first+ prega.all(ir).count):=i;
         prega.all(ir).count:=prega.all(ir).count+1;
      end loop;
   end loop;
   free_buf(pbuf);

   --- Regionalisation des Noeuds ---
   --- determination de la region de chaque noeud --
   for i in 1..graphe.info.nb_noeuds loop
      rx1:=ceil(graphe.tnod(i).coords.coor_x,pas);
      ry1:=ceil(graphe.tnod(i).coords.coor_y,pas);
      ir:=(ry1-1)*nbregx+rx1;
      Rnods(i):=ir;
      pregn.all(ir).count:=pregn.all(ir).count+1;
   end loop;

   --- Mise en place des valeurs fisrt et preparation des compteurs ---
   Pregn.all(1).first:=1;
   for i in 1..nbreg-1 loop
      Pregn.all(i+1).first:=Pregn.all(i).count+Pregn.all(i).first;
      Pregn.all(i).count:=0;
   end loop;
   Pregn.all(nbreg).count:=0;

   --- Rangement des noeuds ---
   prego:=new Liens_array_type(1..graphe.info.nb_noeuds+increment);
   for i in 1..graphe.info.nb_noeuds loop
      prego.all(pregn.all(rnods(i)).first+ pregn.all(rnods(i)).count):=i;
      pregn.all(rnods(i)).count:=pregn.all(rnods(i)).count+1;
   end loop;


   --- Regionalisation des faces  ----
   preggprov:=new Liens_array_type(1..2*somme);
   ind:=0;
   pregf.all(1).first:=1;
   for i in 1..nbreg loop
      pregf.all(i).count:=0;

      ind_1er_face_reg_i:=ind+1;
      if prega.all(i).count/=0 then
         -- on etudie tous les arcs de la region
         for j in 1..prega.all(i).count loop
            -- face gauche de l arc appartient-il deja a la liste des faces?
            face_prise:=false;
            for k in 1..pregf.all(i).count loop
               if graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_g=
                         preggprov.all(ind_1er_face_reg_i-1+k) then
                  face_prise:=true;
                  exit;
               end if;
            end loop;
            -- si la face gauche n existe pas dans la liste on ajoute cette face
            if face_prise=false and
               graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_g/=0 then
               ind:=ind+1;
               pregf.all(i).count:=pregf.all(i).count+1;
               preggprov.all(ind):=graphe.tarc(pregb.all(prega.all(i).first
                                                                  -1+j)).face_g;
            end if;
            -- face droite de l arc appartient-il deja a la liste des faces?
            face_prise:=false;
            for k in 1..pregf.all(i).count loop
               if graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_d=
                         preggprov.all(ind_1er_face_reg_i-1+k) then
                  face_prise:=true;
                  exit;
               end if;
            end loop;
            -- si la face droite n existe pas dans la liste on ajoute cette face
            if face_prise=false and
               graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_d/=0 then
               ind:=ind+1;
               pregf.all(i).count:=pregf.all(i).count+1;
               preggprov.all(ind):=graphe.tarc(pregb.all(prega.all(i).first
                                                                  -1+j)).face_d;
            end if;
         end loop;
      end if;
      -- Mise en place de la valeur first suivante
      if i/=nbreg then
         pregf.all(i+1).first:=pregf.all(i).first+pregf.all(i).count;
      end if;
   end loop;
   -- on peut allouer maintenant la bonne taille du tableau des faces
   pregg:=new Liens_array_type(1..ind+increment);
   pregg(1..ind):=preggprov(1..ind);
   free_preg(preggprov);


   --- Stockage des donnees dans la strucure de graphe ----
   graphe.pasx:=pas;
   graphe.pasy:=pas;
   graphe.nbregx:=nbregx;
   graphe.nbregy:=nbregy;
   graphe.radr_a:=prega;
   graphe.rblk_a:=pregb;
   graphe.rfin_a:=prega.all(Nbreg).first+prega.all(Nbreg).count-1;
   graphe.radr_n:=pregn;
   graphe.rblk_n:=prego;
   graphe.rfin_n:=pregn.all(Nbreg).first+pregn.all(Nbreg).count-1;
   graphe.radr_f:=pregf;
   graphe.rblk_f:=pregg;
   graphe.rfin_f:=pregf.all(Nbreg).first+pregf.all(Nbreg).count-1;
end;


Function GR_REGION_OF(Graphe : graphe_type;
                      Point : Point_type)
                      return natural is
rx,ry,ir : natural;
begin
   rx:=ceil(Point.coor_x,Graphe.pasx);
   ry:=ceil(Point.Coor_y,Graphe.pasy);
   ir:=(ry-1)*graphe.nbregx+rx;
   return ir;
end;


Procedure GR_ANNULE_REGION(graphe : in out graphe_type) is

Procedure Free_buf is new unchecked_deallocation(Liens_array_type,
     Regbuf_type);
Procedure Free_adr is new unchecked_deallocation(Array_reg_type,
     Regadr_type);

--
begin
   Graphe.pasx:=0;
   Graphe.pasy:=0;
   Graphe.Nbregx:=0;
   Graphe.Nbregy:=0;

   Graphe.rfin_a:=0;
   Graphe.rvid_a:=0;
   Graphe.rfin_n:=0;
   Graphe.rvid_n:=0;
   Graphe.rfin_f:=0;
   Graphe.rvid_f:=0;

   Free_buf(graphe.Rblk_a);
   Free_adr(graphe.Radr_a);
   Free_buf(graphe.Rblk_n);
   Free_adr(graphe.Radr_n);
   Free_buf(graphe.Rblk_f);
   Free_adr(graphe.Radr_f);
end;


Procedure AMENAGER_REGION(radr: in out regadr_type;
                          rblk: in out regbuf_type;
                          rfin : in out natural;
                          rvid : in out natural;
                          ajout : positive) is

-- Recuperation de place dans le buffer d'entier rblk trop petit, utile a la
-- regionalisation du graphe,
-- soit par agrandissement du tableau
-- soit par condensation du tableau s il y a de la place vide a recuperer

pregbprov : regbuf_type;
procedure Free_pregb is new unchecked_deallocation(Liens_array_type,regbuf_type);

pregaprov : regadr_type;
numreg : regbuf_type;
procedure Free_prega is new unchecked_deallocation(Array_reg_type,regadr_type);
n,imin,vmin1,vmin2,interm1,interm2,interm3,decalage : natural;

begin

   if rvid < ajout then        -- cas ou on agrandit le tableau
      pregbprov:= new Liens_array_type(1..(rblk.all'length + ajout));
      pregbprov.all(1..rblk.all'length):=rblk(1..rblk.all'length);
      free_pregb(rblk);
      rblk:=pregbprov;
   else                        -- cas ou on condense le tableau
      -- initialisation
      n:=radr.all'length;
      numreg:= new Liens_array_type(1..n);
      pregaprov:= new Array_reg_type(1..n);
      for i in 1..n loop
          numreg.all(i):=i;
      end loop;
      pregaprov.all(1..n):=radr.all(1..n);
      -- tri du tableau pregaprov dans l ordre croissant des valeurs  de
      -- pregaprov.first et en cas d egalite dans l ordre croissant de
      -- pregaprov.count (cas particulier ou pregaprov.count=0)
      for i in 1..n-1 loop
         imin:=i;
         vmin1:=pregaprov.all(i).first;
         vmin2:= pregaprov.all(i).count;
         for j in i+1..n loop
            if pregaprov.all(j).first<vmin1 then
               vmin1:=pregaprov.all(j).first;
               vmin2:=pregaprov.all(j).count;
               imin:=j;
            elsif pregaprov.all(j).first=vmin1 then
                  if pregaprov.all(j).count<vmin2 then
                     vmin1:=pregaprov.all(j).first;
                     vmin2:=pregaprov.all(j).count;
                     imin:=j;
                  end if;
            end if;
         end loop;
         interm1:=numreg(i);
         interm2:=pregaprov.all(i).first;
         interm3:=pregaprov.all(i).count;
         numreg(i):=numreg(imin);
         pregaprov.all(i).first:=pregaprov.all(imin).first;
         pregaprov.all(i).count:=pregaprov.all(imin).count;
         numreg(imin):=interm1;
         pregaprov.all(imin).first:=interm2;
         pregaprov.all(imin).count:=interm3;
      end loop;
      -- decalage des adresses
      decalage:=pregaprov.all(1).first-1;
      interm2:=pregaprov.all(1).first;
      pregaprov.all(1).first:=1;
      radr.all(numreg(1)):=pregaprov.all(1);
      rblk.all(1..pregaprov.all(1).count):=
                           rblk.all(interm2..interm2+pregaprov.all(1).count-1);
      for i in 2..n loop
         decalage:=decalage+pregaprov.all(i).first - interm2
                                                   - pregaprov.all(i-1).count;
         interm2:=pregaprov.all(i).first;
         pregaprov.all(i).first:=pregaprov.all(i).first - decalage;
         radr.all(numreg(i)):=pregaprov.all(i);
         rblk.all(pregaprov.all(i).first..
                           pregaprov.all(i).first+pregaprov.all(i).count-1)
                :=rblk.all(interm2..interm2+pregaprov.all(i).count-1);
      end loop;
      free_pregb(numreg);
      free_prega(pregaprov);
      rvid:=0;
      rfin:=rfin-decalage;
   end if;

end;


Procedure AJOUT_ELEMENT_IN_REGION(radr : in out regadr_type;
                                  rblk : in out regbuf_type;
                                  rfin : in out natural;
                                  rvid : in out natural;
                                  element : positive;
                                  ir : positive) is

-- Ajout eventuel d un element (face, arc, noeud) dans la region ir

deja : boolean;

begin
   deja:=false;
   -- verification de la presence eventuelle de l element dans la region
   for i in 1..radr.all(ir).count loop
       if rblk.all(radr.all(ir).first+i-1)=element then
          deja:=true;
          exit;
       end if;
   end loop;
   if deja=false then
      -- il faut ajouter l element dans la region ir et on ecrit en fin de
      -- tableau
      if rblk.all'length < rfin + radr.all(ir).count+1 then
         -- amenagement du tableau necessaire
         AMENAGER_REGION(radr,rblk,rfin,rvid,radr.all(ir).count+increment);
      end if;
      -- affectation de tableau de 1 a radr.all(ir).count :
      rblk.all(rfin+1..rfin+radr.all(ir).count):=rblk.all(radr.all(ir).first..
                                      radr.all(ir).first+radr.all(ir).count -1);
      rvid:=rvid+radr.all(ir).count;
      radr.all(ir).first:=rfin+1;
      radr.all(ir).count:=radr.all(ir).count+1;
      rfin:=rfin+radr.all(ir).count;
      rblk.all(rfin):=element;
   end if;

end;


-- ===========================================================================
-- Fonction sur les noeuds : GENIOB_NOEUD
Function  GR_NOEUD (graphe : Graphe_type; noeud : positive) return noeud_type is
begin
   return Graphe.Tnod(noeud);
end;


Function  GR_ARCS_DU_NOEUD (graphe : graphe_type; noeud : positive)
             return liens_array_type is
A_lire,P_lire,First,N,reste : integer;
My_liste : Liens_array_type(1..graphe.info.max_arc_neu);
begin
   A_lire:=ceil(graphe.tnod(noeud).premier_arc,128);
   if A_lire/=graphe.lbll then
      read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(graphe.tnod(noeud).premier_arc-1,128);
   Reste:=graphe.tnod(noeud).nombre_arc;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,128-p_lire);
      My_liste(First+1..First+N):=Graphe.Blnk(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_lnk,graphe.blnk);
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;
   return my_liste(1..Graphe.tnod(noeud).nombre_arc);
end;


Procedure ECRIRE_LIENS_FIN(graphe : in out graphe_type;
                       tab_arcs : liens_array_type;
                       n : positive) is

-- ecriture des liens (liste de numero d arcs) en fin de fichier des liens

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_liens+1,128);
   if A_lire/=graphe.lbll then
      if modulo(graphe.info.nb_liens,128)/=0 then
         read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      end if;
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_liens,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Blnk(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;
end;



Procedure ECRIRE_LIENS(graphe : in out graphe_type;
                       tab_arcs : liens_array_type;
                       n : positive;
                       premier : positive ) is

-- reecriture des liens (liste de numero d arcs) a la meme adresse du buffer de
-- lien (on ecrit donc une liste dont le nombre d element est inferieur ou egal
-- a la liste de depart)

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,128);
   if A_lire/=graphe.lbll then
      read(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      graphe.lbll:=a_lire;
   end if;
   P_lire:=modulo(premier-1,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Blnk(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_lnk,graphe.blnk,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_lnk,graphe.blnk);
         Graphe.lbll:=graphe.lbll+1;
         P_lire:=0;
      end if;
   end loop;
end;


Procedure GR_NOEUDS_IN_RECTANGLE(graphe : graphe_type;
                                 F_point : Point_type;
                                 S_point : Point_type;
                                 tab_noeuds : out liens_array_type;
                                 n : out natural) is

flags : array(1..graphe.info.nb_noeuds) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_n,i_n : integer;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_n:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_n.all(ir+j).count-1 loop
            i_n:=graphe.rblk_n.all(graphe.radr_n.all(ir+j).first+k);
            if not flags(i_n) then
               flags(i_n):=true;
               if graphe.tnod(i_n).coords.coor_x <= upper.coor_x and
                  graphe.tnod(i_n).coords.coor_y <= upper.coor_y and
                  graphe.tnod(i_n).coords.coor_x >= lower.coor_x and
                  graphe.tnod(i_n).coords.coor_y >= lower.coor_y and
                  (graphe.tnod(i_n).nombre_arc/=0 or
                   graphe.tnod(i_n).point_seul=true) then
                  n_n:=n_n+1;
                  tab_noeuds(n_n):=i_n;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_n;
end;


Procedure GR_MOD_NOEUD(graphe:in out graphe_type;
                       noeud:positive;
                       point:point_type) is
-- Deplacement des coordonn�es d un noeud (!! SANS DEPLACEMENT DES ARCS !! )


rx1,ry1,ir : integer;

begin

   graphe.tnod(noeud).coords:=point;
   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tnod(noeud).coords.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tnod(noeud).coords.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1;
      AJOUT_ELEMENT_IN_REGION(graphe.radr_n,graphe.rblk_n,
                                         graphe.rfin_n,graphe.rvid_n,noeud,ir);
   end if;

end;


Procedure GR_CREER_NOEUD_SUR_RIEN(graphe : in out graphe_type;
                                  point : point_type;
                                  point_seul : boolean;
                                  orientation : angle_type;
                                  att_graph : natural;
                                  noeud : out positive) is

-- Creation d un noeud sur rien

rx1,ry1,ir : integer;

begin
   -- etudier ulterieurement s il faut ajouter un test au cas on ne peut
   -- ajouter un noeud au graphe pour des problemes de taille de tableau
   graphe.info.nb_noeuds:=graphe.info.nb_noeuds + 1;
   graphe.tnod(graphe.info.nb_noeuds).coords:=point;
   graphe.tnod(graphe.info.nb_noeuds).premier_arc:=graphe.info.nb_liens+1;
   graphe.tnod(graphe.info.nb_noeuds).nombre_arc:=0;
   graphe.tnod(graphe.info.nb_noeuds).point_seul:=point_seul;
   graphe.tnod(graphe.info.nb_noeuds).orientation:=orientation;
   graphe.tnod(graphe.info.nb_noeuds).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_p then
      graphe.info.nb_graph_p:=att_graph;
   end if;

   noeud:=graphe.info.nb_noeuds;

   -- mise a jour eventuelle de la regionalisation
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tnod(graphe.info.nb_noeuds).coords.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tnod(graphe.info.nb_noeuds).coords.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1;
      AJOUT_ELEMENT_IN_REGION(graphe.radr_n,graphe.rblk_n,
                          graphe.rfin_n,graphe.rvid_n,graphe.info.nb_noeuds,ir);
   end if;

end;



Procedure GR_CREER_NOEUD_SUR_ARC(graphe : in out graphe_type;
                                 arc : in positive;
                                 point : in point_type;
                                 point_seul : boolean;
                                 orientation : angle_type;
                                 att_graph : natural;
                                 noeud : out positive;
                                 arc_debut : out positive;
                                 arc_fin : out positive) is

-- Creation d un noeud sur un arc et donc decoupe de l'arc initial en 2 arcs

my_arc : arc_type;
my_face_g,my_face_d : face_type;
--type point_access_type is access point_liste_type;
liste_points,tab1,tab2 : point_liste_type(1..graphe.info.max_pts_arc);
infx,infy,maxx,maxy,inter : integer;
n1,n2,nouveau_noeud : positive;
troncon_inter : natural :=0;
nombre_pt : natural;
dist,distance_min : float;
tarc_face_g,tarc_face_d : liens_array_type(1..graphe.info.max_arc_fac+1);
position_arc_face_g,position_arc_face_d : natural;
face : natural;
arc_d,arc_f : positive;
Point_hors_ligne : boolean :=false;
esc : boolean;

begin
   my_arc:=GR_ARC(graphe,arc);
   -- determination de la position du point sur l'arc
   GR_POINTS_D_ARC(graphe,arc,liste_points,nombre_pt);
   for i in 1..nombre_pt-1 loop
       dist:=Distance_a_ligne(point,liste_points(i),liste_points(i+1),
                              c_segment_type);
       if dist=0.0 then
          troncon_inter:=i;
          exit;
       end if;
   end loop;

   if troncon_inter=0 then
      -- il n y a aucun point rigoureusement sur un segment de droite de l'arc
      -- on modifie un peu la geometrie du troncon et on peut le signaler
      distance_min:=Distance_a_ligne(point,liste_points(1),liste_points(1+1),
                              c_segment_type);
      troncon_inter:=1;
      Point_hors_ligne:=true;
      for i in 2..nombre_pt-1 loop
          dist:=Distance_a_ligne(point,liste_points(i),liste_points(i+1),
                                 c_segment_type);
          if dist < distance_min then
             distance_min:=dist;
             troncon_inter:=i;
          end if;
      end loop;
   end if;


   if liste_points(troncon_inter+1)=point then
      -- le point intermediaire existe deja
      n1:=troncon_inter+1;
      tab1(1..n1):=liste_points(1..n1);
      n2:=nombre_pt-n1+1;
      tab2(1..n2):=liste_points(n1..nombre_pt);
   else
      -- le point intermediaire n existe pas : il faut le creer comme un noeud
      n1:=troncon_inter+1;
      tab1(1..n1-1):=liste_points(1..n1-1);
      tab1(n1):=point;
      n2:=nombre_pt-n1+2;
      tab2(1):=point;
      tab2(2..n2):=liste_points(n1..nombre_pt);
   end if;

   if my_arc.face_g/=0 then
-- Si l arc est limite de face_g, il faut stocker la liste des arcs constituant
-- cette face pour pouvoir modifier la face;
      face:=my_arc.face_g;
      position_arc_face_g:=0;
      loop
         my_face_g:=GR_FACE(graphe,face);
         tarc_face_g(1..my_face_g.nombre_arc):=GR_ARCS_DE_FACE(graphe,face);
         for i in 1..my_face_g.nombre_arc loop
             if tarc_face_g(i)=-arc then
                position_arc_face_g:=i;
                exit;
             end if;
         end loop;
         exit when(position_arc_face_g/=0);
         face:=my_face_g.trou_sui;
      end loop;
   end if;

   if my_arc.face_d/=0 then
-- Si l arc est limite de face_d, il faut stocker la liste des arcs constituant
-- cette face pour pouvoir modifier la face;
      face:=my_arc.face_d;
      position_arc_face_d:=0;
      loop
         my_face_d:=GR_FACE(graphe,face);
         tarc_face_d(1..my_face_d.nombre_arc):=GR_ARCS_DE_FACE(graphe,face);
         for i in 1..my_face_d.nombre_arc loop
             if tarc_face_d(i)=arc then
                position_arc_face_d:=i;
                exit;
             end if;
         end loop;
         exit when(position_arc_face_g/=0);
         face:=my_face_d.trou_sui;
      end loop;
   end if;

   GR_DETRUIRE_ARC(graphe,arc);
   GR_CREER_NOEUD_SUR_RIEN(graphe,point,point_seul,orientation,att_graph,
                           nouveau_noeud);
   noeud:=nouveau_noeud;
   GR_CREER_ARC_bis(graphe,my_arc.noeud_ini,nouveau_noeud,tab1,n1,Ligne,my_arc.att_graph,0,arc_d,esc);
   GR_CREER_ARC_bis(graphe,nouveau_noeud,my_arc.noeud_fin,tab2,n2,Ligne,my_arc.att_graph,0,arc_f,esc);
   arc_debut:=arc_d;
   arc_fin:=arc_f;

-- il faut alors ajouter la modification des faces eventuelles avec le remplace-
-- ment de l arc coupe par les deux nouveaux arcs
   if my_arc.face_g/=0 then
      graphe.tarc(arc_d).face_g:=my_arc.face_g;
      graphe.tarc(arc_f).face_g:=my_arc.face_g;
      for i in reverse position_arc_face_g+1..my_face_g.nombre_arc loop
          tarc_face_g(i+1):=tarc_face_g(i);
      end loop;
      tarc_face_g(position_arc_face_g):=-arc_f;
      tarc_face_g(position_arc_face_g+1):=-arc_d;
      GR_MOD_ARCS_DE_FACE(graphe,my_arc.face_g,tarc_face_g,my_face_g.nombre_arc
                                                           +1);
   end if;

   if my_arc.face_d/=0 then
      graphe.tarc(arc_d).face_d:=my_arc.face_d;
      graphe.tarc(arc_f).face_d:=my_arc.face_d;
      for i in reverse position_arc_face_d+1..my_face_d.nombre_arc loop
          tarc_face_d(i+1):=tarc_face_d(i);
      end loop;
      tarc_face_d(position_arc_face_d):=arc_d;
      tarc_face_d(position_arc_face_d+1):=arc_f;
      GR_MOD_ARCS_DE_FACE(graphe,my_arc.face_d,tarc_face_d,my_face_d.nombre_arc
                                                           +1);
   end if;


end;



Procedure GR_DETRUIRE_NOEUD(graphe : in out graphe_type;
                            noeud : positive) is

-- Destruction d'un noeud

begin
  graphe.tnod(noeud).point_seul:=false ;
end;



Procedure GR_MOD_ATT_NOEUD (graphe : In out graphe_type; noeud : positive;
                            att_graph : natural) is

begin
   graphe.tnod(noeud).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_p then
      graphe.info.nb_graph_p:=att_graph;
   end if;
end;



Procedure GR_FUSION_AU_NOEUD(graphe : In out Graphe_type;
                             noeud : positive;
                             arc : out natural) is

Tab_arcs : Liens_array_type(1..2);
liste_points1,liste_points2 : point_liste_type(1..graphe.info.max_pts_arc);
liste_points : point_liste_type(1..2*graphe.info.max_pts_arc);
arc_prov : integer;
arc1,arc2 : positive;
my_arc1,my_arc2 : arc_type;
nombre_pts1,nombre_pts2,nombre_pts : natural;
noeud_ini,noeud_fin : positive;
my_noeud_ini,my_noeud_fin : noeud_type;
tab_arcs_noeud_fin,tab_arcs_noeud_ini : liens_array_type(1..graphe.info.Max_arc_neu);
flag : boolean;
nb : integer;

begin

   if graphe.tnod(noeud).point_seul=false and
       graphe.tnod(noeud).nombre_arc=2  then

       Tab_arcs(1..2):=GR_ARCS_DU_NOEUD(graphe,noeud);

       -- on fait en sorte de conserver le numero d'arc le plus petit
       if abs(tab_arcs(2)) < abs(Tab_arcs(1)) then
          arc_prov:=tab_arcs(1);
          tab_arcs(1):=tab_arcs(2);
          tab_arcs(2):=arc_prov;
       end if;

       arc1:=abs(Tab_arcs(1));
       arc2:=abs(Tab_arcs(2));

       my_arc1:=GR_arc(graphe,arc1);
       my_arc2:=GR_arc(graphe,arc2);

       if my_arc1.att_graph=my_arc2.att_graph and arc1/=arc2 then

          -- Recuperation de la liste des points
          GR_POINTS_D_ARC(graphe,arc1,liste_points1,nombre_pts1);
          GR_POINTS_D_ARC(graphe,arc2,liste_points2,nombre_pts2);

          -- Destruction de l'arc2 et du noeud intermediaire
          graphe.tarc(arc2).nombre_met:=0;
          graphe.tnod(noeud).nombre_arc:=0;


          nombre_pts:=nombre_pts1 + nombre_pts2 -1;
          if Tab_arcs(1)<0 then
             -- arc1 arrive au noeud
             liste_points(1..nombre_pts1):=liste_points1(1..nombre_pts1);
             if Tab_arcs(2)>0 then   -- arc2 part du noeud
                -- mise a jour geometrique
                liste_points(nombre_pts1..nombre_pts):=liste_points2(1..nombre_pts2);

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_fin:=my_arc2.noeud_fin;
                if my_arc2.arc_sui=-arc2 then
                   graphe.tarc(arc1).arc_sui:=-arc1;
                else
                   graphe.tarc(arc1).arc_sui:=my_arc2.arc_sui;
                end if;

                -- mise a jour topologique au niveau du noeud_fin
                noeud_fin:=my_arc2.noeud_fin;
                my_noeud_fin:=GR_NOEUD(graphe,noeud_fin);
                tab_arcs_noeud_fin(1..my_noeud_fin.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_fin);
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)=-arc2 then
                       tab_arcs_noeud_fin(i):=-arc1;
                       exit;
                    end if;
                end loop;
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre=-arc2 then
                          graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre:=-arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui=-arc2 then
                          graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui:=-arc1;
                          exit;
                       end if;
                    end if;
                end loop;
             else  -- arc2 arrive au noeud
                -- mise a jour geometrique
                for i in 1..nombre_pts2-1 loop
                    liste_points(nombre_pts1+i):=liste_points2(nombre_pts2-i);
                end loop;

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_fin:=my_arc2.noeud_ini;
                if my_arc2.arc_pre=arc2 then
                   graphe.tarc(arc1).arc_sui:=-arc1;
                else
                   graphe.tarc(arc1).arc_sui:=my_arc2.arc_pre;
                end if;

                -- mise a jour topologique au niveau du noeud_fin
                noeud_fin:=my_arc2.noeud_ini;
                my_noeud_fin:=GR_NOEUD(graphe,noeud_fin);
                tab_arcs_noeud_fin(1..my_noeud_fin.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_fin);
                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)=arc2 then
                       tab_arcs_noeud_fin(i):=-arc1;
                       exit;
                    end if;
                end loop;

                for i in 1..my_noeud_fin.nombre_arc loop
                    if tab_arcs_noeud_fin(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre=arc2 then
                          graphe.tarc(tab_arcs_noeud_fin(i)).arc_pre:=-arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui=arc2 then
                          graphe.tarc(-tab_arcs_noeud_fin(i)).arc_sui:=-arc1;
                          exit;
                       end if;
                    end if;
                end loop;
             end if;
             -- reecriture des bons liens au noeud
             Ecrire_liens(graphe,tab_arcs_noeud_fin,my_noeud_fin.nombre_arc,
                               my_noeud_fin.premier_arc);

          else
             -- arc1 part du noeud; Pour avoir le nouvel arc dans le meme sens
             --  que'arc1, il faut inverser le sens du nouve arc

             liste_points(nombre_pts2..nombre_pts):=liste_points1(1..nombre_pts1);


             if Tab_arcs(2)<0 then   -- arc2 arrive au noeud (arc2, arc1 meme orientation)
                -- mise a jour geometrique
                liste_points(1..nombre_pts2):=liste_points2(1..nombre_pts2);

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_ini:=my_arc2.noeud_ini;
                if my_arc2.arc_pre=arc2 then
                   graphe.tarc(arc1).arc_pre:=arc1;
                else
                   graphe.tarc(arc1).arc_pre:=my_arc2.arc_pre;
                end if;

                -- mise a jour topologique au niveau du noeud_ini
                noeud_ini:=my_arc2.noeud_ini;
                my_noeud_ini:=GR_NOEUD(graphe,noeud_ini);
                tab_arcs_noeud_ini(1..my_noeud_ini.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_ini);
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)=arc2 then
                       tab_arcs_noeud_ini(i):=arc1;
                       exit;
                    end if;
                end loop;
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre=arc2 then
                          graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre:=arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui=arc2 then
                          graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui:=arc1;
                          exit;
                       end if;
                    end if;
                end loop;
             else  -- arc2 part du noeud (arc1 et arc2 en sens opposes)
                -- mise a jour geometrique
                for i in 1..nombre_pts2 loop
                    liste_points(i):=liste_points2(nombre_pts2+1-i);
                end loop;

                -- mise a jour topologique au niveau de l'arc conserve
                graphe.tarc(arc1).noeud_ini:=my_arc2.noeud_fin;
                if my_arc2.arc_sui=-arc2 then
                   graphe.tarc(arc1).arc_pre:=arc1;
                else
                   graphe.tarc(arc1).arc_pre:=my_arc2.arc_sui;
                end if;

                -- mise a jour topologique au niveau du noeud_ini
                noeud_ini:=my_arc2.noeud_fin;
                my_noeud_ini:=GR_NOEUD(graphe,noeud_ini);
                tab_arcs_noeud_ini(1..my_noeud_ini.nombre_arc):=
                                            GR_ARCS_DU_NOEUD(graphe,noeud_ini);
                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)=-arc2 then
                       tab_arcs_noeud_ini(i):=arc1;
                       exit;
                    end if;
                end loop;

                for i in 1..my_noeud_ini.nombre_arc loop
                    if tab_arcs_noeud_ini(i)>0 then
                       if graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre=-arc2 then
                          graphe.tarc(tab_arcs_noeud_ini(i)).arc_pre:=arc1;
                          exit;
                       end if;
                    else
                       if graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui=-arc2 then
                          graphe.tarc(-tab_arcs_noeud_ini(i)).arc_sui:=arc1;
                          exit;
                       end if;
                    end if;
                end loop;
             end if;
             -- reecriture des bons liens au noeud
             Ecrire_liens(graphe,tab_arcs_noeud_ini,my_noeud_ini.nombre_arc,
                               my_noeud_ini.premier_arc);

          end if;

          GR_MOD_POINTS_D_ARC(graphe,arc1,liste_points,nombre_pts);
          arc:=arc1;

       else
         arc:=0;
       end if;
    else
       arc:=0;
    end if;
end;



-- ===========================================================================
-- Fonction sur les arcs : GENIOB_ARC

Function  GR_ARC (graphe : Graphe_type; arc : positive ) return arc_type is
begin
   return Graphe.Tarc(arc);
end;


Procedure  GR_POINTS_D_ARC (graphe : graphe_type;
                            arc : positive;
                            tab_points : out point_liste_type;
                            n : out natural) is

My_prov : Cubique_liste_type(1..graphe.info.max_cub_arc);

begin
   if graphe.tarc(arc).mode_met=Ligne then
      n:=graphe.tarc(arc).nombre_met;
      tab_points(1..graphe.tarc(arc).nombre_met):=Polyligne.LIRE(graphe,arc);
  else
      n:=graphe.tarc(arc).nombre_met;
      my_prov(1..graphe.tarc(arc).nombre_met):= Polycubique.LIRE(graphe,arc);
      -- ??????? Tranferer les cubiques en liste de points
  end if;
end;


Procedure  GR_CUBIQUES_D_ARC (graphe : graphe_type;
                              arc : positive;
                              tab_cubiques : out cubique_liste_type;
                              n_cubiques : out natural) is

begin
  n_cubiques:= graphe.tarc(arc).nombre_met;
  tab_cubiques(1..graphe.tarc(arc).nombre_met):= Polycubique.LIRE(graphe,arc);
end;


Procedure GR_ARCS_IN_RECTANGLE(graphe : graphe_type;
                               F_point : Point_type;
                               S_point : Point_type;
                               tab_arcs : out liens_array_type;
                               n : out natural) is

flags : array(1..graphe.info.nb_arcs) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_a,i_a : integer;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_a:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_a.all(ir+j).count-1 loop
            i_a:=graphe.rblk_a.all(graphe.radr_a.all(ir+j).first+k);
            if not flags(i_a) then
               flags(i_a):=true;
               if graphe.tarc(i_a).encombr.minimum.coor_x <= upper.coor_x and
                  graphe.tarc(i_a).encombr.minimum.coor_y <= upper.coor_y and
                  graphe.tarc(i_a).encombr.maximum.coor_x >= lower.coor_x and
                  graphe.tarc(i_a).encombr.maximum.coor_y >= lower.coor_y and
                  graphe.tarc(i_a).nombre_met/=0 then
                  n_a:=n_a+1;
                  tab_arcs(n_a):=i_a;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_a;
end;



Procedure GR_MOD_POINTS_D_ARC(graphe:in out graphe_type;
                              arc:positive;
                              tab_points:point_liste_type;
                              n:natural) is

A_lire,P_lire,First,K,reste,Min_x,Min_y,Max_x,Max_y : integer;

rx1,rx2,ry1,ry2,ir,Nbreg : integer;
flag_mod : boolean;
tab_points_prov : point_liste_type(1..n);
n_prov : integer;

begin

   -- Elimination des points doubles eventuels
   tab_points_prov(1):=tab_points(1);
   n_prov:=1;
   for i in 2..n loop
      if tab_points(i)/=tab_points(i-1) then
         n_prov:=n_prov+1;
         tab_points_prov(n_prov):=tab_points(i);
      end if;
   end loop;


   if n_prov<=graphe.tarc(arc).nombre_met then
   --
   -- puisqu on diminue ou egale le nombre de points intermediaires, on peut
   -- reecrire dans les memes positions du buffer de point
   --
      Polyligne.ECRIRE(graphe,tab_points_prov,n_prov,graphe.tarc(arc).premier_met);

   else
   --
   -- puisqu on augmente le nombre de points intermediaires, on ne peut pas
   -- reecrire dans les memes positions du buffer de point; on reecrira en
   -- fin de fichier du buffer de point
   --
      Polyligne.ECRIRE_FIN(graphe,tab_points_prov,n_prov);
      graphe.tarc(arc).premier_met:=graphe.info.nb_points+1;
      graphe.info.nb_points:=graphe.info.nb_points+n;
      if n_prov>graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:=n_prov;
      end if;
   end if;
   graphe.tarc(arc).nombre_met:=n_prov;

   -- Mise a jour de l'encombrement de l arc.
   --
   min_x:=tab_points_prov(1).coor_x;
   max_x:=tab_points_prov(1).coor_x;
   min_y:=tab_points_prov(1).coor_y;
   max_y:=tab_points_prov(1).coor_y;
   for i in 2..n_prov loop
       Min_x:=Min(tab_points_prov(i).coor_x,Min_x);
       Max_x:=Max(tab_points_prov(i).coor_x,Max_x);
       Min_y:=Min(tab_points_prov(i).coor_y,Min_y);
       Max_y:=Max(tab_points_prov(i).coor_y,Max_y);
   end loop;
   if Min_x < graphe.tarc(arc).encombr.minimum.coor_x or
      Max_x > graphe.tarc(arc).encombr.maximum.coor_x or
      Min_y < graphe.tarc(arc).encombr.minimum.coor_y or
      Max_y > graphe.tarc(arc).encombr.maximum.coor_y then
      --  si graphe regionalise, il faudra peut-etre ajouter des arcs dans les
      --  regions avoisinantes
      flag_mod:=true;
   end if;
   graphe.tarc(arc).encombr.minimum.coor_x:=Min_x;
   graphe.tarc(arc).encombr.maximum.coor_x:=Max_x;
   graphe.tarc(arc).encombr.minimum.coor_y:=Min_y;
   graphe.tarc(arc).encombr.maximum.coor_y:=Max_y;

   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if flag_mod=true and graphe.pasx/=0 then
      rx1:=ceil(graphe.tarc(arc).encombr.minimum.coor_x,graphe.pasx);
      rx2:=ceil(graphe.tarc(arc).encombr.maximum.coor_x,graphe.pasx);
      ry1:=ceil(graphe.tarc(arc).encombr.minimum.coor_y,graphe.pasy);
      ry2:=ceil(graphe.tarc(arc).encombr.maximum.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for j in ry1..ry2 loop
          for k in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_a,graphe.rblk_a,
                                         graphe.rfin_a,graphe.rvid_a,arc,ir);
              if graphe.tarc(arc).face_g/=0 then
                 AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                       graphe.rfin_f,graphe.rvid_f,graphe.tarc(arc).face_g,ir);
              end if;
              if graphe.tarc(arc).face_d/=0 then
                 AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                       graphe.rfin_f,graphe.rvid_f,graphe.tarc(arc).face_d,ir);
              end if;
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;

Procedure GR_CREER_ARC(graphe : in out graphe_type;
                       noeud_ini : positive;
                       noeud_fin : positive;
                       tab_points : point_liste_type;
                       n : positive;
                       mode : metrique_type;
                       att_graph : natural;
                       arc : out positive) is
-- creation d un arc s appuyant sur 2 noeuds et dont on a la liste des
-- points intermediares
Type tablo_float is array(integer range<>) of float;
my_noeud : noeud_type;
my_arc : arc_type;
rx1,rx2,ry1,ry2,ir,min_x,min_y,max_x,max_y : integer;
tab_points_prov : point_liste_type(1..n);
n_prov : integer;
liste_arcs: liens_array_type(1..graphe.info.max_arc_neu+3);
liste_points: point_liste_type(1..max(graphe.info.max_pts_arc,n));
liste_angles : tablo_float(1..graphe.info.max_arc_neu+2);
tab_cubiques : cubique_liste_type(1..n-1);
n_cub : positive;
nombre_pt : natural;
minarc : integer;
minangle : float;
Function ANGLE(pini,pfin : point_type) return float is
 -- Calcul de l arctangente de 2 points
 ang,v1,v2 : float;
                      begin  if pini.coor_x/=pfin.coor_x then     v1:=float(pfin.coor_y-pini.coor_y);
     v2:=float(pfin.coor_x-pini.coor_x);
     ang:=Arctan(V1,V2);
  elsif pfin.coor_y>pini.coor_y then         ang:=3.14159/2.0;
     else        ang:=-3.14159/2.0;
  end if;
  return ang;
end ANGLE;
    begin   -- Elimination des points doubles eventuels
	tab_points_prov(1):=tab_points(1);
   n_prov:=1;
   for i in 2..n loop      if tab_points(i)/=tab_points(i-1) then          n_prov:=n_prov+1;
         tab_points_prov(n_prov):=tab_points(i);
      end if;
   end loop;
   -- initialisations faciles
   graphe.info.nb_arcs:=graphe.info.nb_arcs + 1;
   arc:=graphe.info.nb_arcs;
   graphe.tarc(graphe.info.nb_arcs).noeud_ini:=noeud_ini;
   graphe.tarc(graphe.info.nb_arcs).noeud_fin:=noeud_fin;
   graphe.tarc(graphe.info.nb_arcs).face_g:=0;
   graphe.tarc(graphe.info.nb_arcs).face_d:=0;
   graphe.tarc(graphe.info.nb_arcs).mode_met:=mode;
   graphe.tarc(graphe.info.nb_arcs).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_l then      graphe.info.nb_graph_l:=att_graph;
   end if;
   -- ecriture des points intermediaires en mode ligne ou cubique
   Polyligne.ECRIRE_FIN(graphe,tab_points_prov,n_prov);
   graphe.tarc(graphe.info.nb_arcs).mode_met:= ligne;
   graphe.tarc(graphe.info.nb_arcs).premier_met:=graphe.info.nb_points+1;
   graphe.tarc(graphe.info.nb_arcs).nombre_met:=n_prov;
   graphe.info.nb_points:=graphe.info.nb_points+n_prov;
   if n_prov>graphe.info.max_pts_arc then         graphe.info.max_pts_arc:=n_prov;
   end if;
   -- Mise a jour de l'encombrement de l arc.
   min_x:=tab_points_prov(1).coor_x;
   max_x:=tab_points_prov(1).coor_x;
   min_y:=tab_points_prov(1).coor_y;
   max_y:=tab_points_prov(1).coor_y;
   for i in 2..n_prov loop       Min_x:=Min(tab_points_prov(i).coor_x,Min_x);
       Max_x:=Max(tab_points_prov(i).coor_x,Max_x);
       Min_y:=Min(tab_points_prov(i).coor_y,Min_y);
       Max_y:=Max(tab_points_prov(i).coor_y,Max_y);
   end loop;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x:=Min_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x:=Max_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y:=Min_y;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y:=Max_y;
   -- Mise a jour du lien avec le noeud initial
   --   my_noeud:=GR_NOEUD(graphe,noeud_ini);
   if my_noeud.nombre_arc/=0 then      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_ini);
   end if;
   -- calcul de l angle de chaque arc issu du noeud initial
   for i in 1..my_noeud.nombre_arc loop       if liste_arcs(i)>0 then
			my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else          my_arc:=GR_ARC(graphe,-1*liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-1*liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),                                liste_points(nombre_pt-1));
       end if;
   end loop;
   -- ajout de l arc au noeud initial
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(1),tab_points(2));
   liste_arcs(my_noeud.nombre_arc+1):=graphe.info.nb_arcs;
   graphe.tnod(noeud_ini).nombre_arc:=graphe.tnod(noeud_ini).nombre_arc + 1;
   graphe.tnod(noeud_ini).premier_arc:=graphe.info.nb_liens+1;
   -- tri suivant l ordre croissant des angles
   for i in 1..graphe.tnod(noeud_ini).nombre_arc-1 loop       for j in  i+1..graphe.tnod(noeud_ini).nombre_arc loop
   if liste_angles(j)<liste_angles(i) then              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_ini).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_ini).nombre_arc loop       if liste_arcs(i)>0 then          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;
   -- ecriture dans le fichier des liens
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_ini).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_ini).nombre_arc;
   if graphe.tnod(noeud_ini).nombre_arc>graphe.info.max_arc_neu then      graphe.info.max_arc_neu:=graphe.tnod(noeud_ini).nombre_arc;
   end if;
      -- Mise a jour du lien avec le noeud final    --
	  my_noeud:=GR_NOEUD(graphe,noeud_fin);
   if my_noeud.nombre_arc/=0 then      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_fin);
   end if;
   -- calcul de l angle de chaque arc issu du noeud final
   for i in 1..my_noeud.nombre_arc loop       if liste_arcs(i)>0 then          my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else          my_arc:=GR_ARC(graphe,-liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),                                liste_points(nombre_pt-1));
       end if;
   end loop;
   -- ajout de l arc au noeud final
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(n_prov),                                              tab_points(n_prov-1));
   liste_arcs(my_noeud.nombre_arc+1):=-graphe.info.nb_arcs;
   graphe.tnod(noeud_fin).nombre_arc:=graphe.tnod(noeud_fin).nombre_arc + 1;
   graphe.tnod(noeud_fin).premier_arc:=graphe.info.nb_liens+1;
   -- tri suivant l ordre croissant des anlges
   for i in 1..graphe.tnod(noeud_fin).nombre_arc-1 loop       for j in  i+1..graphe.tnod(noeud_fin).nombre_arc loop           if liste_angles(j)<liste_angles(i) then              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_fin).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_fin).nombre_arc loop       if liste_arcs(i)>0 then          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;
   -- ecriture dans le fichier des liens
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_fin).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_fin).nombre_arc;
   if graphe.tnod(noeud_fin).nombre_arc>graphe.info.max_arc_neu then      graphe.info.max_arc_neu:=graphe.tnod(noeud_fin).nombre_arc;
   end if;
      --   
	  -- Mise a jour eventuelle de la regionalisation 
	  --
	  if graphe.pasx/=0 then      rx1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x,                graphe.pasx);
      rx2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x,                graphe.pasx);
      ry1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y,                graphe.pasy);
      ry2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y,                graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;
      for i in ry1..ry2 loop          for j in rx1..rx2 loop              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_a,graphe.rblk_a,                            graphe.rfin_a,graphe.rvid_a,graphe.info.nb_arcs,ir);
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;
end;

Procedure GR_CREER_ARC_bis(graphe : in out graphe_type;
                       noeud_ini : positive;
                       noeud_fin : positive;
                       tab_points : point_liste_type;
                       n : positive;
                       mode : metrique_type;
                       att_graph : natural;
 					   OV : in short_short_integer;
                       arc : out positive;
					   Exit_sans_creer : out boolean) is


-- creation d un arc s appuyant sur 2 noeuds et dont on a la liste des
-- points intermediares

Type tablo_float is array(integer range<>) of float;
my_noeud : noeud_type;
my_arc : arc_type;
rx1,rx2,ry1,ry2,ir,min_x,min_y,max_x,max_y : integer;
tab_points_prov : point_liste_type(1..n);
n_prov : integer;

liste_arcs: liens_array_type(1..graphe.info.max_arc_neu+3);
liste_points: point_liste_type(1..max(graphe.info.max_pts_arc,n));
liste_angles : tablo_float(1..graphe.info.max_arc_neu+2);
tab_cubiques : cubique_liste_type(1..n-1);
n_cub : positive;
nombre_pt : natural;
minarc : integer;
minangle : float;

Function ANGLE(pini,pfin : point_type) return float is
-- Calcul de l arctangente de 2 points
ang,v1,v2 : float;

begin
  if pini.coor_x/=pfin.coor_x then
     v1:=float(pfin.coor_y-pini.coor_y);
     v2:=float(pfin.coor_x-pini.coor_x);
     --ang:=ATAN2(V1,V2);
     ang:=ArcTAN(V1,V2);
  elsif pfin.coor_y>pini.coor_y then
        ang:=3.14159/2.0;
     else
        ang:=-3.14159/2.0;
  end if;
  return ang;
end ANGLE;



begin

   -- Elimination des points doubles eventuels
   tab_points_prov(1):=tab_points(1);
   n_prov:=1;
   for i in 2..n loop
      if tab_points(i)/=tab_points(i-1) then
         n_prov:=n_prov+1;
         tab_points_prov(n_prov):=tab_points(i);
      end if;
   end loop;

   Exit_sans_creer:=false;
   if n_prov=1 then
     -- tous les points de l'arc etaient confondus
	 Exit_sans_creer:=true;
	 return;
   end if;

   -- initialisations faciles
   graphe.info.nb_arcs:=graphe.info.nb_arcs + 1;
   arc:=graphe.info.nb_arcs;
   graphe.tarc(graphe.info.nb_arcs).noeud_ini:=noeud_ini;
   graphe.tarc(graphe.info.nb_arcs).noeud_fin:=noeud_fin;
   graphe.tarc(graphe.info.nb_arcs).face_g:=0;
   graphe.tarc(graphe.info.nb_arcs).face_d:=0;
   graphe.tarc(graphe.info.nb_arcs).mode_met:=mode;
   graphe.tarc(graphe.info.nb_arcs).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_l then
      graphe.info.nb_graph_l:=att_graph;
   end if;
   graphe.tarc(graphe.info.nb_arcs).Octet_vms:=oV;

   -- ecriture des points intermediaires en mode ligne ou cubique
   Polyligne.ECRIRE_FIN(graphe,tab_points_prov,n_prov);
   graphe.tarc(graphe.info.nb_arcs).mode_met:= ligne;
   graphe.tarc(graphe.info.nb_arcs).premier_met:=graphe.info.nb_points+1;
   graphe.tarc(graphe.info.nb_arcs).nombre_met:=n_prov;
   graphe.info.nb_points:=graphe.info.nb_points+n_prov;
   if n_prov>graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:=n_prov;
   end if;

   -- Mise a jour de l'encombrement de l arc.
   min_x:=tab_points_prov(1).coor_x;
   max_x:=tab_points_prov(1).coor_x;
   min_y:=tab_points_prov(1).coor_y;
   max_y:=tab_points_prov(1).coor_y;
   for i in 2..n_prov loop
       Min_x:=Min(tab_points_prov(i).coor_x,Min_x);
       Max_x:=Max(tab_points_prov(i).coor_x,Max_x);
       Min_y:=Min(tab_points_prov(i).coor_y,Min_y);
       Max_y:=Max(tab_points_prov(i).coor_y,Max_y);
   end loop;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x:=Min_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x:=Max_x;
   graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y:=Min_y;
   graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y:=Max_y;


   -- Mise a jour du lien avec le noeud initial
   --
   my_noeud:=GR_NOEUD(graphe,noeud_ini);
   if my_noeud.nombre_arc/=0 then
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_ini);
   end if;

   -- calcul de l angle de chaque arc issu du noeud initial
   for i in 1..my_noeud.nombre_arc loop
       if liste_arcs(i)>0 then
          my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else
          my_arc:=GR_ARC(graphe,-1*liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-1*liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),
                                liste_points(nombre_pt-1));
       end if;
   end loop;

   -- ajout de l arc au noeud initial
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(1),tab_points(2));
   liste_arcs(my_noeud.nombre_arc+1):=graphe.info.nb_arcs;
   graphe.tnod(noeud_ini).nombre_arc:=graphe.tnod(noeud_ini).nombre_arc + 1;
   graphe.tnod(noeud_ini).premier_arc:=graphe.info.nb_liens+1;

   -- tri suivant l ordre croissant des angles
   for i in 1..graphe.tnod(noeud_ini).nombre_arc-1 loop
       for j in  i+1..graphe.tnod(noeud_ini).nombre_arc loop
           if liste_angles(j)<liste_angles(i) then
              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_ini).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_ini).nombre_arc loop
       if liste_arcs(i)>0 then
          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else
          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;

   -- ecriture dans le fichier des liens
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_ini).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_ini).nombre_arc;
   if graphe.tnod(noeud_ini).nombre_arc>graphe.info.max_arc_neu then
      graphe.info.max_arc_neu:=graphe.tnod(noeud_ini).nombre_arc;
   end if;


   -- Mise a jour du lien avec le noeud final
   --
   my_noeud:=GR_NOEUD(graphe,noeud_fin);
   if my_noeud.nombre_arc/=0 then
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,noeud_fin);
   end if;

   -- calcul de l angle de chaque arc issu du noeud final
   for i in 1..my_noeud.nombre_arc loop
       if liste_arcs(i)>0 then
          my_arc:=GR_ARC(graphe,liste_arcs(i));
          GR_POINTS_D_ARC(graphe,liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(1),liste_points(2));
       else
          my_arc:=GR_ARC(graphe,-liste_arcs(i));
          GR_POINTS_D_ARC(graphe,-liste_arcs(i),liste_points,nombre_pt);
          liste_angles(i):=ANGLE(liste_points(nombre_pt),
                                liste_points(nombre_pt-1));
       end if;
   end loop;

   -- ajout de l arc au noeud final
   liste_angles(my_noeud.nombre_arc+1):=ANGLE(tab_points(n_prov),
                                              tab_points(n_prov-1));
   liste_arcs(my_noeud.nombre_arc+1):=-graphe.info.nb_arcs;
   graphe.tnod(noeud_fin).nombre_arc:=graphe.tnod(noeud_fin).nombre_arc + 1;
   graphe.tnod(noeud_fin).premier_arc:=graphe.info.nb_liens+1;

   -- tri suivant l ordre croissant des anlges
   for i in 1..graphe.tnod(noeud_fin).nombre_arc-1 loop
       for j in  i+1..graphe.tnod(noeud_fin).nombre_arc loop
           if liste_angles(j)<liste_angles(i) then
              minarc:=liste_arcs(j);
              minangle:=liste_angles(j);
              liste_arcs(j):=liste_arcs(i);
              liste_angles(j):=liste_angles(i);
              liste_arcs(i):=minarc;
              liste_angles(i):=minangle;
           end if;
       end loop;
   end loop;
   liste_arcs(graphe.tnod(noeud_fin).nombre_arc+1):=liste_arcs(1);
   for i in 1..graphe.tnod(noeud_fin).nombre_arc loop
       if liste_arcs(i)>0 then
          graphe.tarc(liste_arcs(i)).arc_pre:=liste_arcs(i+1);
       else
          graphe.tarc(-liste_arcs(i)).arc_sui:=liste_arcs(i+1);
       end if;
   end loop;

   -- ecriture dans le fichier des liens
   ECRIRE_LIENS_FIN(graphe,liste_arcs,graphe.tnod(noeud_fin).nombre_arc);
   graphe.info.nb_liens:=graphe.info.nb_liens+graphe.tnod(noeud_fin).nombre_arc;
   if graphe.tnod(noeud_fin).nombre_arc>graphe.info.max_arc_neu then
      graphe.info.max_arc_neu:=graphe.tnod(noeud_fin).nombre_arc;
   end if;


   --
   -- Mise a jour eventuelle de la regionalisation
   --
   if graphe.pasx/=0 then
      rx1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_x,
                graphe.pasx);
      rx2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_x,
                graphe.pasx);
      ry1:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.minimum.coor_y,
                graphe.pasy);
      ry2:=ceil(graphe.tarc(graphe.info.nb_arcs).encombr.maximum.coor_y,
                graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for i in ry1..ry2 loop
          for j in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_a,graphe.rblk_a,
                            graphe.rfin_a,graphe.rvid_a,graphe.info.nb_arcs,ir);
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;



Procedure GR_DETRUIRE_ARC(graphe : in out graphe_type;
                          arc : positive) is
my_noeud : noeud_type;
my_arc : arc_type;

liste_arcs: liens_array_type(1..graphe.info.max_arc_neu);

begin
   my_arc:=GR_ARC( graphe,arc);

   -- elimination de l arc
   graphe.tarc(arc).nombre_met:=0;

   -- mise a jour des arcs issus du noeud initial de l arc a eliminer
   my_noeud:=GR_NOEUD(graphe,my_arc.noeud_ini);
   if my_noeud.nombre_arc>1 then -- il y a d autres arcs que celui detruit
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,
                                                           my_arc.noeud_ini);
      -- mise a jour de l arc precedent
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)<0 then --(arc entrant)
             if graphe.tarc(-liste_arcs(i)).arc_sui=arc then
                graphe.tarc(-liste_arcs(i)).arc_sui:=graphe.tarc(arc).arc_pre;
                exit;
             end if;
          else --(arc sortant)
             if graphe.tarc(liste_arcs(i)).arc_pre=arc then
                graphe.tarc(liste_arcs(i)).arc_pre:=graphe.tarc(arc).arc_pre;
                exit;
             end if;
          end if;
      end loop;
      -- mise a jour du fichier des liens pour le noeud initial de l arc elimine
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)=arc then
             for j in i+1..my_noeud.nombre_arc loop
                 liste_arcs(j-1):=liste_arcs(j);
             end loop;
             exit;
          end if;
      end loop;
      ECRIRE_LIENS(graphe,liste_arcs,my_noeud.nombre_arc-1,my_noeud.premier_arc);
   end if;
   graphe.tnod(my_arc.noeud_ini).nombre_arc:=my_noeud.nombre_arc-1;

   -- mise a jour des arcs issus du noeud final de l arc a eliminer
   my_noeud:=GR_NOEUD(graphe,my_arc.noeud_fin);
   if my_noeud.nombre_arc>1 then -- il y a d autres arcs que celui detruit
      liste_arcs(1..my_noeud.nombre_arc):=GR_ARCS_DU_NOEUD(graphe,
                                                           my_arc.noeud_fin);
      -- mise a jour de l arc suivant
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)<0 then --(arc entrant)
             if graphe.tarc(-liste_arcs(i)).arc_sui=-arc then
                graphe.tarc(-liste_arcs(i)).arc_sui:=graphe.tarc(arc).arc_sui;
                exit;
             end if;
          else --(arc sortant)
             if graphe.tarc(liste_arcs(i)).arc_pre=-arc then
                graphe.tarc(liste_arcs(i)).arc_pre:=graphe.tarc(arc).arc_sui;
                exit;
             end if;
          end if;
      end loop;
      -- mise a jour du fichier des liens pour le noeud final de l arc elimine
      for i in 1..my_noeud.nombre_arc loop
          if liste_arcs(i)=-arc then
             for j in i+1..my_noeud.nombre_arc loop
                 liste_arcs(j-1):=liste_arcs(j);
             end loop;
             exit;
          end if;
      end loop;
      ECRIRE_LIENS(graphe,liste_arcs,my_noeud.nombre_arc-1,my_noeud.premier_arc);
   end if;
   graphe.tnod(my_arc.noeud_fin).nombre_arc:=my_noeud.nombre_arc-1;
end;



Procedure GR_MOD_ATT_ARC (graphe : In out graphe_type; arc : positive;
                          att_graph : natural) is

begin
   graphe.tarc(arc).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_l then
      graphe.info.nb_graph_l:=att_graph;
   end if;
end;



Procedure GR_LIGNE_CUBIQUES( graphe                : in out graphe_type;
			     arc                   : in positive;
			     sigma                 : in float;
			     pas 	           : in float;
			     tolerance             : in float;
			     demi_tolerance_max    : in float;
 			     produit_vectoriel_min : in float;
			     part_angle            : in integer ;
 			     tab_cubiques          : out cubique_liste_type;
 			     n_cub                 : out positive;
			     mode_stockage         : in metrique_type ) is


  ligne   : point_liste_type(1..graphe.info.max_pts_arc);
  npoints : positive;
  tab_cub : cubique_liste_type(1..graphe.info.max_cub_arc+300);
  ncub    : positive;

begin

  Gr_points_d_arc(graphe,arc,ligne,npoints);

  polycubique.CHANG_PTS_CUB(ligne,npoints,tab_cub,ncub,
      sigma,pas,demi_tolerance_max,tolerance,produit_vectoriel_min,part_angle);

  if mode_stockage = cubique then
      polycubique.ECRIRE_FIN(graphe, tab_cub, ncub);
      graphe.tarc(arc).mode_met:= cubique;
      graphe.tarc(arc).premier_met:=graphe.info.nb_cubiques+1;
      graphe.tarc(arc).nombre_met:=ncub;
      graphe.info.nb_cubiques:=graphe.info.nb_cubiques+ncub;
      if ncub>graphe.info.max_cub_arc then
         graphe.info.max_cub_arc:=ncub;
      end if;
  end if;

  tab_cubiques(1..ncub):= tab_cub(1..ncub);
  n_cub:= ncub;

end;
-- NB : les coord. des pts correspondant aux cubiques sont en FLOAT !!!


Procedure GR_CUBIQUES_LIGNE( graphe        : in out graphe_type;
			     arc           : in positive;
			     pas	   : in integer;
			     tab_points	   : out point_liste_reel;
			     n_points      : out positive;
			     mode_stockage : in metrique_type ) is

 tab_cubiques : cubique_liste_type(1..graphe.info.max_cub_arc+300);
 n_cub        : positive;
 tabpoints    : point_access_type;
 lgr          : float := 0.0;

begin

  Gr_cubiques_d_arc(graphe,arc,tab_cubiques,n_cub);

  for cub in 1..n_cub loop
    lgr := lgr + SQRT(float(tab_cubiques(cub).Xlocal_sommet**2
                  +(integer(tab_cubiques(cub).a)*(tab_cubiques(cub).Xlocal_sommet)**3)**2));
  end loop;

  declare
    tab_pts_temp : point_liste_reel(1..integer(lgr)+300);
    npoints      : positive;
    tab_pts_filt : point_access_type;
    npoints_filt : positive:=1;
  begin

   polycubique.CHANG_CUB_PTS(tab_cubiques,n_cub,pas,tab_pts_temp,npoints);

   tabpoints:= new point_liste_type(1..npoints);

   for i in 1..npoints loop
       tabpoints(i).coor_x:= integer(tab_pts_temp(i).coor_x);
       tabpoints(i).coor_y:= integer(tab_pts_temp(i).coor_y);
   end loop;

   -- Filtrage de la ligne des points correspondants aux cubiques :
   tab_pts_filt := new point_liste_type(1..npoints);
   Lissage_filtrage.Douglas_Peucker( tabpoints,npoints,
					tab_pts_filt,npoints_filt,
					-- seuil de filtrage en mm !!!
					1.0/float(graphe.info.resolution),
					graphe.info.resolution);

   if mode_stockage = ligne then
    polyligne.ECRIRE_FIN( graphe, tab_pts_filt(1..npoints_filt),npoints_filt);
    graphe.tarc(arc).mode_met:= ligne;
    graphe.tarc(arc).premier_met:= graphe.info.nb_points+1;
    graphe.tarc(arc).nombre_met:= npoints_filt;
    graphe.info.nb_points:= graphe.info.nb_points + npoints_filt;
    if npoints_filt > graphe.info.max_pts_arc then
         graphe.info.max_pts_arc:= npoints_filt;
    end if;
   end if;

   n_points:= npoints_filt;
   for i in 1..npoints_filt loop
    tab_points(i).coor_x:=float(tab_pts_filt(i).coor_x);
    tab_points(i).coor_y:=float(tab_pts_filt(i).coor_y);
   end loop;

   free_pliste(tabpoints);
   free_pliste(tab_pts_filt);

  end; -- Declare --
end;




-- ===========================================================================
-- Fonction sur les faces : GENIOB_FACE
Function  GR_FACE (graphe : Graphe_type; face : positive) return face_type is
begin
   return Graphe.Tfac(face);
end;


Function  GR_ARCS_DE_FACE (graphe : graphe_type; face : positive)
             return liens_array_type is
A_lire,P_lire,First,N,reste : integer;
My_liste : Liens_array_type(1..graphe.info.max_arc_fac);
ch : string(1..10);
nch : integer;
 package int_io is new text_io.integer_io(integer); use int_io;

begin

   A_lire:=ceil(graphe.tfac(face).premier_arc,128);

   begin
   if A_lire/=graphe.lbld then
      read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      graphe.lbld:=a_lire;
   end if;

exception when liens_io.end_error=>
   put_line("fin de fichier");
   end;

   P_lire:=modulo(graphe.tfac(face).premier_arc-1,128);
   Reste:=graphe.tfac(face).nombre_arc;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,128-p_lire);
      My_liste(First+1..First+N):=Graphe.Bdom(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_dom,graphe.bdom);
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop;
   return my_liste(1..Graphe.tfac(face).nombre_arc);
end;



Procedure ECRIRE_DOMAINES_FIN(graphe : in out graphe_type;
                              tab_arcs : liens_array_type;
                              n : positive ) is

-- ecriture des domaines (liste de numero d arcs) en fin de fichier des domaines

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_domaines+1,128);
   if A_lire/=graphe.lbld then
      if modulo(graphe.info.nb_domaines,128)/=0 then
         read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      end if;
      graphe.lbld:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_domaines,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bdom(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop;
end;


Procedure ECRIRE_DOMAINES(graphe : in out graphe_type;
                          tab_arcs : liens_array_type;
                          n : positive;
                          premier : positive ) is

-- reecriture des domaines (liste de numero d arcs) a la meme adresse du buffer
-- de lien (on ecrit donc une liste dont le nombre d element est inferieur ou
-- egal a la liste de depart)


A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,128);
   if A_lire/=graphe.lbld then
      read(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      graphe.lbld:=a_lire;
   end if;
   P_lire:=modulo(premier-1,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bdom(P_lire+1..P_lire+K):=tab_arcs(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_dom,graphe.bdom,Liens_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_dom,graphe.bdom);
         Graphe.lbld:=graphe.lbld+1;
         P_lire:=0;
      end if;
   end loop;
end;


Function GR_ENCOMBREMENT_FACE(Graphe : graphe_type;
                           face : positive)
                           return boite_type is

my_liste : Liens_array_type(1..graphe.info.max_arc_fac);
encombr_face : boite_type;
  package int_io is new text_io.integer_io(integer); use int_io;

begin
   my_liste(1..Graphe.tfac(face).nombre_arc):=GR_ARCS_DE_FACE(Graphe,face);
   encombr_face.minimum.coor_x:=Graphe.info.delta_x;
   encombr_face.minimum.coor_y:=Graphe.info.delta_y;
   encombr_face.maximum.coor_x:=0;
   encombr_face.maximum.coor_y:=0;
   for i in 1..Graphe.tfac(face).nombre_arc loop
      encombr_face.minimum.coor_x:=MIN(graphe.tarc(ABS(my_liste(i))).encombr.
                                   minimum.coor_x,encombr_face.minimum.coor_x);
      encombr_face.minimum.coor_y:=MIN(graphe.tarc(ABS(my_liste(i))).encombr.
                                   minimum.coor_y,encombr_face.minimum.coor_y);
      encombr_face.maximum.coor_x:=MAX(graphe.tarc(ABS(my_liste(i))).encombr.
                                   maximum.coor_x,encombr_face.maximum.coor_x);
      encombr_face.maximum.coor_y:=MAX(graphe.tarc(ABS(my_liste(i))).encombr.
                                   maximum.coor_y,encombr_face.maximum.coor_y);
   end loop;
   return encombr_face;
end;


Procedure GR_FACES_IN_RECTANGLE(graphe : graphe_type;
                                F_point : Point_type;
                                S_point : Point_type;
                                tab_faces : out liens_array_type;
                                 n : out natural) is

flags : array(1..graphe.info.nb_faces) of boolean :=(others=>false);
Lower,Upper : point_type;
i1,i2,ir,dx,dy,n_f,i_f : integer;
face : face_type;
encombr_face : boite_type;
begin
   Lower.coor_x:=MIN(f_point.coor_x,s_point.coor_x);
   Lower.coor_y:=MIN(f_point.coor_y,s_point.coor_y);
   Upper.coor_x:=Max(f_point.coor_x,s_point.coor_x);
   Upper.coor_y:=Max(f_point.coor_y,s_point.coor_y);

   Lower.coor_x:=Max(Lower.coor_x,1);
   Lower.coor_y:=Max(Lower.coor_y,1);
   Lower.coor_x:=Min(Lower.coor_x,graphe.info.delta_x);
   Lower.coor_y:=Min(Lower.coor_y,graphe.info.delta_y);

   Upper.coor_x:=Min(Upper.coor_x,graphe.info.delta_x);
   Upper.coor_y:=Min(Upper.coor_y,graphe.info.delta_y);
   Upper.coor_x:=Max(Upper.coor_x,1);
   Upper.coor_y:=Max(Upper.coor_y,1);

   i1:=GR_REGION_OF(graphe,lower);
   i2:=GR_REGION_OF(graphe,upper);
   dx:=MODULO(i2-i1,graphe.nbregx)+1;
   dy:=Ceil(i2-i1+1,graphe.nbregx);
   Ir:=i1-1;
   n_f:=0;
   for i in 1..dy loop
      for j in 1..dx loop
         for k in 0..graphe.radr_f.all(ir+j).count-1 loop
            i_f:=graphe.rblk_f.all(graphe.radr_f.all(ir+j).first+k);
            if not flags(i_f) then
               flags(i_f):=true;
               encombr_face:=GR_ENCOMBREMENT_FACE(graphe,i_f);
               face:=GR_FACE(graphe,i_f);
               if encombr_face.minimum.coor_x <= upper.coor_x and
                  encombr_face.minimum.coor_y <= upper.coor_y and
                  encombr_face.maximum.coor_x >= lower.coor_x and
                  encombr_face.maximum.coor_y >= lower.coor_y and
                  face.Nombre_arc/=0 then
                  n_f:=n_f+1;
                  tab_faces(n_f):=i_f;
               end if;
            end if;
         end loop;
      end loop;
      ir:=ir+graphe.nbregx;
   end loop;
   n:=n_f;
end;



Procedure GR_CREER_FACE(graphe : in out graphe_type;
                        tab_arcs : liens_array_type;
                        n : positive;
                        att_graph : natural;
                        face : out positive) is

-- Creation d une face a partir d une liste de n arcs constituant le contour de
-- la face; ces arcs sont classes dans l ordre de description de la face :
-- contour exterieur continu puis contours interieurs (trous) continus

arc_debut : integer;
trou : boolean;
num_face : positive;
compteur : natural;
liste_arc_face : liens_array_type(1..n);
rx1,rx2,ry1,ry2,ir : integer;
encombr_face : boite_type;

begin
   -- initialisation facile
   num_face:=graphe.info.nb_faces+1;
   face:=num_face;
   -- mise a jour du fichier des domaines
   trou:=false;
   compteur:=0;
   arc_debut:=tab_arcs(1);
   for i in 1..n loop
       compteur:=compteur+1;
       liste_arc_face(compteur):=tab_arcs(i);
       if tab_arcs(i)>0 then
          graphe.tarc(tab_arcs(i)).face_d:=num_face;
          if graphe.tarc(tab_arcs(i)).arc_sui=arc_debut then
             -- on a 1 face : determinons les elements de la face (face ou trou)
             graphe.info.nb_faces:=graphe.info.nb_faces+1;
             graphe.tfac(graphe.info.nb_faces).premier_arc:=graphe.info.nb_domaines+1;
             graphe.tfac(graphe.info.nb_faces).nombre_arc:=compteur;
             graphe.tfac(graphe.info.nb_faces).att_graph:=att_graph;
             if att_graph>graphe.info.nb_graph_z then
                graphe.info.nb_graph_z:=att_graph;
             end if;

             if trou=false then
                graphe.tfac(graphe.info.nb_faces).zone_englob:=0;
             else
                graphe.tfac(graphe.info.nb_faces).zone_englob:=num_face;
             end if;

             -- on passe au trou suivant s il existe
             if i/=n then
                graphe.tfac(graphe.info.nb_faces).trou_sui:=graphe.info.nb_faces
                                                            +1;
                trou:=true;
                arc_debut:=tab_arcs(i+1);
             else
                graphe.tfac(graphe.info.nb_faces).trou_sui:=0;
             end if;

             -- ecriture du domaine
             ECRIRE_DOMAINES_FIN(graphe,liste_arc_face,compteur);
             graphe.info.nb_domaines:=graphe.info.nb_domaines+compteur;
             if compteur>graphe.info.max_arc_fac then
                graphe.info.max_arc_fac:=compteur;
             end if;
             compteur:=0;
          end if;
       else
          graphe.tarc(-tab_arcs(i)).face_g:=num_face;
          if graphe.tarc(-tab_arcs(i)).arc_pre=arc_debut then
             -- on a 1 face : determinons les elements de la face (face ou trou)
             graphe.info.nb_faces:=graphe.info.nb_faces+1;
             graphe.tfac(graphe.info.nb_faces).premier_arc:=graphe.info.nb_domaines+1;
             graphe.tfac(graphe.info.nb_faces).nombre_arc:=compteur;
             graphe.tfac(graphe.info.nb_faces).att_graph:=att_graph;
             if att_graph>graphe.info.nb_graph_z then
                graphe.info.nb_graph_z:=att_graph;
             end if;

             if trou=false then
                graphe.tfac(graphe.info.nb_faces).zone_englob:=0;
             else
                graphe.tfac(graphe.info.nb_faces).zone_englob:=num_face;
             end if;

             -- on passe au trou suivant s il existe
             if i/=n then
                graphe.tfac(graphe.info.nb_faces).trou_sui:=graphe.info.nb_faces
                                                            +1;
                trou:=true;
                arc_debut:=tab_arcs(i+1);
             else
                graphe.tfac(graphe.info.nb_faces).trou_sui:=0;
             end if;

             -- ecriture des domaines
             ECRIRE_DOMAINES_FIN(graphe,liste_arc_face,compteur);
             graphe.info.nb_domaines:=graphe.info.nb_domaines+compteur;
             if compteur>graphe.info.max_arc_fac then
                graphe.info.max_arc_fac:=compteur;
             end if;
             compteur:=0;
          end if;
       end if;
   end loop;

   -- Mise a jour eventuelle de la regionalisation
   if graphe.pasx/=0 then
      encombr_face.minimum.coor_x:=Graphe.info.delta_x;
      encombr_face.minimum.coor_y:=Graphe.info.delta_y;
      encombr_face.maximum.coor_x:=0;
      encombr_face.maximum.coor_y:=0;
      for i in 1..n loop
          encombr_face.minimum.coor_x:=MIN(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .minimum.coor_x,encombr_face.minimum.coor_x);

          encombr_face.minimum.coor_y:=MIN(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .minimum.coor_y,encombr_face.minimum.coor_y);
          encombr_face.maximum.coor_x:=MAX(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .maximum.coor_x,encombr_face.maximum.coor_x);
          encombr_face.maximum.coor_y:=MAX(graphe.tarc(ABS(tab_arcs(i))).encombr
                                   .maximum.coor_y,encombr_face.maximum.coor_y);
      end loop;

      rx1:=ceil(encombr_face.minimum.coor_x,graphe.pasx);
      rx2:=ceil(encombr_face.maximum.coor_x,graphe.pasx);
      ry1:=ceil(encombr_face.minimum.coor_y,graphe.pasy);
      ry2:=ceil(encombr_face.maximum.coor_y,graphe.pasy);
      ir:=(ry1-1)*graphe.nbregx+rx1-1;

      for i in ry1..ry2 loop
          for j in rx1..rx2 loop
              ir:=ir+1;
              AJOUT_ELEMENT_IN_REGION(graphe.radr_f,graphe.rblk_f,
                            graphe.rfin_f,graphe.rvid_f,num_face,ir);
          end loop;
          ir:=ir+graphe.nbregx-rx2+rx1-1;
      end loop;
   end if;

end;


Procedure GR_DETRUIRE_FACE(graphe : in out graphe_type;
                           face : positive) is

my_face : face_type;
tab_arcs : liens_array_type(1..graphe.info.Max_arc_fac);
parcelle : natural;

begin
   parcelle:=face;
   while parcelle/=0 loop
      my_face:=GR_FACE(Graphe,parcelle);
      tab_arcs(1..my_face.nombre_arc):=GR_ARCS_DE_FACE(graphe,parcelle);

      -- elimination de la parcelle (face si premier passage dans la boucle)
      --                            (trou si passage dans la boucle > 1)
      graphe.tfac(parcelle).nombre_arc:=0;
      graphe.tfac(parcelle).trou_sui:=0;
      graphe.tfac(parcelle).zone_englob:=0;

      -- mise a jour des faces gauches ou droites des arcs bordant la parcelle
      for i in 1..my_face.nombre_arc loop
          if graphe.tarc(ABS(tab_arcs(i))).face_g=face then
             graphe.tarc(ABS(tab_arcs(i))).face_g:=0;
          else
             graphe.tarc(ABS(tab_arcs(i))).face_d:=0;
          end if;
      end loop;

      -- Passage au trou suivant s il existe
      parcelle:=my_face.trou_sui;
   end loop;
end;


Procedure GR_MOD_ARCS_DE_FACE(graphe : In out Graphe_type;
                              face : positive;
                              tab_arcs : liens_array_type;
                              n : natural) is

-- Modification de la liste des arcs constituant le contour d une face ou d un
-- trou

begin
   if n<=graphe.tfac(face).nombre_arc then
      -- puisqu on diminue ou egale le nombre d arc de la face, on peut reecrire
      -- dans les memes positions du buffer d arcs
      ECRIRE_DOMAINES(graphe,tab_arcs,n,graphe.tfac(face).premier_arc);
   else
      -- puisqu on augmente le nombre d arc intermediaires, on ne peut pas
      -- reecrire dans les memes positions du buffer d arcs; on reecrira en fin
      -- de fichier du buffer d arcs
      ECRIRE_DOMAINES_FIN(graphe,tab_arcs,n);
      graphe.tfac(face).premier_arc:=graphe.info.nb_domaines+1;
      graphe.info.nb_domaines:=graphe.info.nb_domaines+n;
      if n>graphe.info.max_arc_fac then
         graphe.info.max_arc_fac:=n;
      end if;
   end if;
   graphe.tfac(face).nombre_arc:=n;

   -- mise a jour de la valeur de face_d ou face_g ?

   -- Mise a jour de la regionalisation ?
end;



Procedure GR_MOD_ATT_FACE (graphe : In out graphe_type; face : positive;
                            att_graph : natural) is

begin
   graphe.tfac(face).att_graph:=att_graph;
   if att_graph>graphe.info.nb_graph_z then
      graphe.info.nb_graph_z:=att_graph;
   end if;
end;




-- ============================================================================
-- Fonction sur la legende : GENIOB_LEGENDE
Function GR_CREATE_NEW_LEGENDE (leg_l : natural;
                      	     	leg_p : natural;
                           	leg_z: natural)
                           		return Legende_type is

My_legende  : Legende_type;
Begin
   my_legende:=new Legende_struc_type(leg_l,leg_p,leg_z);
   Return my_legende;
end;


Function GR_LOAD_FROM_DISK_LEGENDE ( nom : string) return legende_type is

Entete : Info_type;
My_legende : Legende_type;
F_leg : legende_io.file_type;
Baff : Affichage_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');

begin
   -- Recuperation des Infos sur le disque --
   entete:=Gr_disk_infos(nom);

   -- Creation d'une nouvelle legende en memoire --
   My_legende:=GR_CREATE_NEW_LEGENDE(2*(entete.nb_graph_l+10),
                     2*(entete.nb_graph_p+10),2*(entete.nb_graph_z+10));

   -- Ouverture du fichier legende
     NomFic(1..nom'last):=Nom;
     nomfic(nom'last+1..nom'last+4 ):=".GEV";

     begin
        Open (F_leg, In_File, NomFic);
   		-- Chargement en memoire des informations de legende lineaire --
   		for i in 1..ceil(entete.nb_graph_l,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_l(128*(i-1)+1..Min(128*i,entete.nb_graph_l)):=
           	baff(1..Min(128,entete.nb_graph_l-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_l+1 en 0
   		if Modulo(entete.nb_graph_l,128)=0 then
      		read(f_leg,baff);
   		end if;
   		my_legende.tleg_l(0):=baff(Modulo(entete.nb_graph_l+1,128));

   -- Chargement en memoire des informations de legende ponctuelle --
   		for i in 1..ceil(entete.nb_graph_p,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_p(128*(i-1)+1..Min(128*i,entete.nb_graph_p)):=
           	baff(1..Min(128,entete.nb_graph_p-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_p+1 en 0
   		if Modulo(entete.nb_graph_p,128)=0 then
      		read(f_leg,baff);
   		end if;
   		my_legende.tleg_p(0):=baff(Modulo(entete.nb_graph_p+1,128));

   -- Chargement en memoire des informations de legende zonale --
   		for i in 1..ceil(entete.nb_graph_z,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_z(128*(i-1)+1..Min(128*i,entete.nb_graph_z)):=
           	baff(1..Min(128,entete.nb_graph_z-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_zz+1 en 0
   		if Modulo(entete.nb_graph_z,128)=0 then
     	 	read(f_leg,baff);
   		end if;
   		my_legende.tleg_z(0):=baff(Modulo(entete.nb_graph_z+1,128));
   		Close (F_leg);
        Exception when TEXT_IO.NAME_ERROR =>
        --put_line( " le fichier "& nomfic & " n'existe pas ");
        --new_line(2);
        raise TEXT_IO.NAME_ERROR;
    end;
   	return my_legende;

end;



Procedure GR_SAVE_TO_DISK_LEGENDE (Graphe : graphe_type; legende : Legende_type;
                                   nom : string) is

Entete : Info_type;
My_legende : Legende_type;
F_leg : legende_io.file_type;
Baff : Affichage_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');
begin

   -- Ouverture du nouveau fichier legende ou on va sauvegarder les donnees
     NomFic(1..nom'last):=Nom;
     NomFic(nom'last+1..nom'last+4 ):=".GEV";
     Open (F_leg, out_File, NomFic);
   -- Recuperation du nombre d'affichages lineaires, ponctuels, zonaux
   entete:=GR_infos(graphe);

   -- Sauvegarde des informations de legende lineaire --
   for i in 1..ceil(Entete.nb_graph_l,128) loop
       baff(1..Min(128,Entete.nb_graph_l-(i-1)*128)):=
          legende.tleg_l(128*(i-1)+1..Min(128*i,Entete.nb_graph_l));
       if Min(128,Entete.nb_graph_l-(i-1)*128)<128 then
          baff(Entete.nb_graph_l+1-(i-1)*128):=Legende.tleg_l(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_l+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_l,128)=0 then
      baff(1):=Legende.tleg_l(0);
      write(F_leg,baff);
   end if;

   -- Sauvegarde des informations de legende ponctuelle --
   for i in 1..ceil(Entete.nb_graph_p,128) loop
       baff(1..Min(128,Entete.nb_graph_p-(i-1)*128)):=
          legende.tleg_p(128*(i-1)+1..Min(128*i,Entete.nb_graph_p));
       if Min(128,Entete.nb_graph_p-(i-1)*128)<128 then
          baff(Entete.nb_graph_p+1-(i-1)*128):=Legende.tleg_p(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_p+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_p,128)=0 then
      baff(1):=Legende.tleg_p(0);
      write(F_leg,baff);
   end if;

   -- Sauvegarde des informations de legende zonale --
   for i in 1..ceil(Entete.nb_graph_z,128) loop
       baff(1..Min(128,Entete.nb_graph_z-(i-1)*128)):=
          legende.tleg_z(128*(i-1)+1..Min(128*i,Entete.nb_graph_z));
       if Min(128,Entete.nb_graph_z-(i-1)*128)<128 then
          baff(Entete.nb_graph_z+1-(i-1)*128):=Legende.tleg_z(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_z+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_z,128)=0 then
      baff(1):=Legende.tleg_z(0);
      write(F_leg,baff);
   end if;

   close(F_leg);
end;



Function  GR_LEGENDE_L (legende : Legende_type;
                        att_graph : natural)
                        return affichage_type is
begin
   return legende.tleg_l(att_graph);
end;



Function  GR_LEGENDE_P (legende : Legende_type;
                        att_graph : natural)
                         return affichage_type is
begin
   return legende.tleg_p(att_graph);
end;


Function  GR_LEGENDE_Z (legende : Legende_type;
                        att_graph : natural)
                        return affichage_type is
begin
   return legende.tleg_z(att_graph);
end;



Procedure GR_MOD_LEGENDE_L(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type) is
begin
   legende.tleg_l(att_graph):=affichage;
end;


Procedure GR_MOD_LEGENDE_P(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type)  is
begin
   legende.tleg_p(att_graph):=affichage;
end;


Procedure GR_MOD_LEGENDE_Z(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type) is
begin
   legende.tleg_z(att_graph):=affichage;
end;


Procedure GR_AJOUT_LEGENDE_L(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;


Procedure GR_AJOUT_LEGENDE_P(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;


Procedure GR_AJOUT_LEGENDE_Z(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;


-- --------- Unites separees --- Voir GENIO2BTEST --
Procedure GR_CONTOURS_DE(graphe : graphe_type;
                                          F : Positive;
                                          S : in out Surf_type) is

Myfac : Gen_io.Face_type;
Myarc : Gen_io.Arc_type;
Taf   : Liens_array_type(1..graphe.all.Info.Nb_arcs);
tab_points : Point_liste_type(1..graphe.all.info.max_pts_arc);
nb_points : natural;
Ip,Ip0    : Integer;
II,JJ : integer;
So    : Point_type;
Nf    : Integer;
begin
   Ip:=0;
   S.NE:=0;
   Nf:=F;
   -- ----- La face ,a priori le contour exterieur ----
   Myfac:=gr_face(graphe,F);
   loop
      S.Ne:=S.Ne+1;
      S.NA(S.Ne):=0;
      Ip0:=Ip+1;
      Taf(1..Myfac.Nombre_arc):=Gr_arcs_de_face(graphe,nf);
      for j in 1..myfac.nombre_arc loop
         Myarc:=gr_arc(graphe,Abs(taf(j)));
         Gr_points_d_arc(graphe,abs(taf(j)),tab_points,nb_points);
         S.Pts(ip+1..ip+nb_points):=tab_points(1..nb_points);
         If Taf(j)<0 then
            ii:=nb_points/2;
            jj:=Ip+nb_points;
            for k in 1..ii loop
               So:=S.Pts(ip+k);
               S.pts(ip+k):=S.pts(jj);
               S.pts(jj):=So;
               jj:=jj-1;
            end loop;
         end if;
         Ip:=Ip+nb_points-1;
         S.Na(S.ne):=S.na(s.ne)+nb_points-1;
      end loop;
      -- On ajoute le dernier point = au premier point du contour
      S.na(s.ne):=S.na(s.ne)+1;
      Ip:=Ip+1;
      S.pts(Ip):=S.pts(Ip0);
      Exit when Myfac.Trou_sui=0;
      Nf:=Myfac.Trou_sui;
      Myfac:=Gr_face(graphe,nf);
   end loop;
end;



Function GR_QUELLE_FACE(graphe : graphe_type; P : Gen_io.Point_type)
         return natural is

is_in,n,ninterm,Min,imin : integer;
twf,twfinterm   : liens_array_type(1..graphe.all.info.nb_faces);
S     : surf_type(graphe.all.info.nb_faces,graphe.all.info.nb_points);
encombr : boite_type;

begin
   is_in:=0;

   -- Liste des faces concernees --
   Gr_faces_in_rectangle(graphe,P,p,twf,n);

   -- On teste si P est dans la face, en s'arretant quand on en a trouve.
   ninterm:=0;
   for i in 1..n loop
      Gr_contours_de(graphe,twf(i),s);
      if Est_il_dans_surface(P,S) then
         ninterm:=ninterm+1;
         twfinterm(ninterm):=twf(i);
      end if;
   end loop;

   Min:=0;
   imin:=0;
   for i in 1..ninterm loop
       encombr:=Gr_encombrement_face(graphe,twfinterm(i));
       if encombr.minimum.coor_x > Min then
          Min:=encombr.minimum.coor_x;
          imin:=i;
       end if;
   end loop;
   if imin=0 then
      is_in:=0;
   else
      is_in:=twfinterm(imin);
   end if;
   return is_in;
end;



Procedure GR_ARC_PROCHE(Graphe : graphe_type; P : point_type;
       A : out natural; Distance : out float) is
d,dm : float;
di,sn,na : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_arcs);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   loop
      Gr_arcs_in_rectangle(graphe,Haut,Bas,Table,Na);
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Graphe.all.Pasx;
      Haut.Coor_y:=Haut.Coor_y + Graphe.all.Pasy;
      BAs.Coor_x:=BAs.Coor_X - Graphe.all.Pasx;
      Bas.Coor_y:=Bas.Coor_y - Graphe.all.Pasy;
   end loop;
   -- ----- 1 er calcul de distance ----
   Dm:=Float'last;
   for i in 1..na loop
      begin
         D:=GR_DIST_ARC(graphe,p,table(i));
      exception
         when others => D:=Float'last;
      end;
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   -- ----- Calcul de l'encombrement ----
   if dm>0.0 then
      DI:=integer(Sqrt(dm)+1.0);
      Haut.Coor_x:=P.coor_x + di;
      Haut.Coor_y:=P.coor_y + di;
      Bas.Coor_x:=P.coor_x - di;
      Bas.Coor_y:=P.coor_y - di;
      Gr_ARCs_in_rectangle(graphe,Bas,Haut,Table,Na);

      for i in 1..na loop
         begin
            d:=Gr_dist_arc(graphe,P,Table(i));
         exception
            when others => D:=Float'last;
         end;
         if d<dm then dm:=d; Sn:=Table(i); end if;
         exit when dm=0.0;
      end loop;
   end if;
   A:=Sn;
   Distance:=sqrt(dm);
end;



Function GR_DIST_ARC(graphe : graphe_type; P : Point_type;
     A : Positive) return float is

dmin,d : float;
myarc: Arc_type;
Mypts: Point_liste_type(1..Graphe.all.info.Max_pts_arc);
nb_points : natural;


begin
   dmin:=float'last;
   MyArc:=GR_ARC(graphe,A);
   Gr_points_d_arc(Graphe,A,Mypts,nb_points);
   for i in 2..nb_points loop
      d:=Distance_a_ligne(P,Mypts(i-1),Mypts(i),C_segment_type);
      if d<dmin then dmin:=d; end if;
      exit when dmin=0.0;
   end loop;
   return dmin;
end;


Procedure GR_NOEUD_PROCHE(Graphe : graphe_type; P : point_type;
       N : out natural; Distance : out float) is

d,dm : float;
di,sn,na,pas  : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_noeuds);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   if graphe.all.pasx > graphe.all.pasy then pas:=graphe.all.pasx;
   else pas:=graphe.all.pasy; end if;
   loop
      Pas:=Pas+1;
	  Gr_noeuds_in_rectangle(graphe,Haut,Bas,Table,Na);
      Pas:=Pas-1;
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Pas;
      Haut.Coor_y:=Haut.Coor_y + Pas;
      BAs.Coor_x:=BAs.Coor_X - Pas;
      Bas.Coor_y:=Bas.Coor_y - Pas;
   end loop;
   -- ----- calcul de distance ----
   Dm:=Float'last;
   for i in 1..na loop
      D:=DISTANCE_DE_POINTS(P,Graphe.all.Tnod(table(i)).Coords);
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   N:=Sn;
   Distance:=sqrt(dm);
end;


Procedure GR_NOEUD_PROCHE_LONG(Graphe : graphe_type; P : point_type;
       N : out natural; Distance : out long_float) is

Function Distance_de_points_long( M : in Point_type; N : in point_type) return long_float is
D2 : long_float;
begin
   D2:=long_float(m.coor_x-n.coor_x)*long_float(m.coor_x-n.coor_x)+long_float(m.coor_y-n.coor_y)*long_float(m.coor_y-n.coor_y);
   return long_float(D2);
end;

d,dm : long_float;
di,sn,na,pas  : integer;
Haut, Bas : Point_type;
TAble : Liens_array_type(1..Graphe.all.info.nb_noeuds);
begin
   Haut:=P;
   Bas:=P;
   Na:=0;
   if graphe.all.pasx > graphe.all.pasy then pas:=graphe.all.pasx;
   else pas:=graphe.all.pasy; end if;
   loop
	  Gr_noeuds_in_rectangle(graphe,Haut,Bas,Table,Na);
      exit when na/=0;
      Haut.Coor_x:=Haut.Coor_X + Pas;
      Haut.Coor_y:=Haut.Coor_y + Pas;
      BAs.Coor_x:=BAs.Coor_X - Pas;
      Bas.Coor_y:=Bas.Coor_y - Pas;
   end loop;
   -- ----- calcul de distance ----
   Dm:=long_Float'last;
   for i in 1..na loop
      D:=DISTANCE_DE_POINTS_lONG(P,Graphe.all.Tnod(table(i)).Coords);
      if d<dm then dm:=d; sn:=table(i); end if;
      exit when dm=0.0;
   end loop;

   N:=Sn;
   Distance:=dm;
end;

Function GR_DIST_FACE(graphe : graphe_type; P : Point_type; F : Positive)
   return float is

dmin,d : float;
myface : Face_type;
tarc   : liens_array_type(1..graphe.info.Max_arc_fac);
S     : surf_type(graphe.info.nb_faces,graphe.info.nb_points);

begin
   dmin:=float'last;
   Gr_contours_de(graphe,F,s);
   if Est_il_dans_surface(P,s) then
      dmin:=0.0;
      return dmin;
   end if;
   myface:=GR_FACE(graphe,F);
   tarc(1..myface.nombre_arc):=GR_ARCS_DE_FACE(graphe,F);
   for i in 1..myface.nombre_arc loop
       d:=GR_DIST_ARC(graphe,P,tarc(i));
       if d<dmin then
          dmin:=d;
       end if;
   end loop;
   return dmin;
end;


end;
