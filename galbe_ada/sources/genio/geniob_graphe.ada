--******************************************************************************
--                              GENIOB_GRAPHE
-- Modifie le 08/07/1999 pour portage sous WindowsNT
--******************************************************************************


Separate(Gen_io) 

Function GR_DISK_INFOS (nom : string) return Info_type is

Entete : Entete_type;
Fichier: Entete_io.file_type;
NomFic:string(1..80):=(1..80=>' ');
N : integer;
begin
   NomFic(1..nom'length):=Nom;
   NomFic(nom'length+1..nom'length+4 ):=".GEI";
   begin
     Open (fichier, In_File, NomFic);
   Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
          Put(" Programme termine ... Appuyez sur une touche...");
          get_line(Nomfic,n);
          return Entete.Info;     
   End; 
   Read(Fichier, Entete);
   Close(Fichier);
   Return Entete.Info;
end;

-- ===========================================================================
Separate(Gen_io) 
Function ALLOCATE ( arcs : natural; noeuds : natural; faces: natural)
            return Graphe_type is

My_graphe  : Graphe_type;
Na, Nn, Nf : natural;
Begin
   Na:=MAX(arcs,taille_mini);
   Nn:=Max(noeuds,taille_mini);
   Nf:=max(faces,taille_mini);
   My_graphe:=new Graphe_struc_type(na,nn,nf);
   Return my_graphe;
end;

-- ===========================================================================
Separate(Gen_io) 
Function GR_LOAD_FROM_DISK ( nom : string; extension : natural:=10;
            mode: graphe_mode_type:=In_graphe ) return graphe_type is


My_graphe : Graphe_type;
Entete : Info_type;
Na, Nn, Nf : Natural;
Coef : float;
F_arc : Arc_io.file_type;
F_nod : Noeud_io.file_type;
F_fac : Face_io.File_type;
F_pts_aux : Point_io.File_type; 
F_cub_aux : Cubique_io.File_type; 
F_lnk_aux : Liens_io.File_type;
F_dom_aux : Liens_io.File_type;
Nomfic:string(1..80):=(1..80=>' ');

begin
  -- Recuperation des Infos sur le disque --
   entete:=Gr_disk_infos(nom);
   nomfic(1..nom'length):=nom;

   -- Evaluation des nouvelles tailles --
   Coef:=1.0 + Float(extension)/100.0;
   Na := Integer(Float(entete.nb_arcs+10)*coef);
   Nn := Integer(Float(entete.nb_noeuds+10)*coef);
   Nf := Integer(Float(entete.nb_faces+10)*coef);

   -- Creation d'un nouveau graphe en memoire --
   My_graphe:=ALLOCATE(na,nn,nf);
   My_graphe.info:=entete;
   
   -- Chargement en memoire des elements fixes --
   Begin
      Nomfic(Nom'length+1..Nom'length+4):=".GEA";
      Open(f_arc,in_file,Nomfic);
               begin

      For i in 1..entete.nb_arcs loop 
             read(f_arc,my_graphe.tarc(i));
      end loop;
         Exception when TEXT_IO.END_ERROR => 
                close(f_arc);
         end;

      Close(f_arc);
      Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
   end;      
   
   -- Chargement en memoire des elements fixes --
   Begin
        Nomfic(Nom'length+1..Nom'length+4):=".GEN";
   		Open(f_nod,in_file,Nomfic);
        For i in 1..entete.nb_noeuds loop 
            begin
      			read(f_nod,my_graphe.tnod(i));
 
                  Exception when TEXT_IO.END_ERROR =>
                    close(f_nod);
                    exit; 
            End;
   		end loop;
   		Close(f_nod);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2);       
   End;
   
   -- Chargement en memoire des elements fixes --
   Begin 
   		Nomfic(Nom'length+1..Nom'length+4):=".GEF";
   		Open(f_fac,in_file,Nomfic);
   		For i in 1..entete.nb_faces loop 
            Begin   
      			read(f_fac,my_graphe.tfac(i));
                Exception when TEXT_IO.END_ERROR => 
                close(f_fac);
                exit;   
            End;
   		end loop;
   		Close(f_fac);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2);                  
   End;
   
   My_graphe.mode:=mode;
   -- Si le Graphe est Inout il faut recopier les fichiers bufferises 
   if mode=Inout_Graphe then
                              
      -- Fichier des Points
   Nomfic(Nom'length+1..Nom'length+4):=".GEP";
   Begin
   	  	Open(f_pts_aux,in_file,Nomfic);
      	Nomfic(Nom'length+1..Nom'length+8):=".GEP_TMP";
      	Create(My_graphe.f_pts,inout_file,nomfic);
      	for i in 1..ceil(entete.nb_points,128) loop
          begin               
          	read(F_pts_aux,my_graphe.bpts);
          	write(My_graphe.f_pts,my_graphe.bpts);
            Exception when TEXT_IO.END_ERROR => 
                close(f_pts_aux);
                exit;  
          End;
      	end loop;
        Close (F_pts_aux);
        Exception when TEXT_IO.NAME_ERROR =>
              put_line( " le fichier "& nomfic & " n'existe pas ");
              new_line(2); 
       End;
      
      -- Fichier des Cubiques
    Nomfic(Nom'length+1..Nom'length+4):=".GEC";
   begin
        Open(F_Cub_aux, In_file,Nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GEC_TMP";
        Create(My_graphe.f_cub,inout_file,Nomfic);       
        for i in 1..ceil(entete.nb_cubiques,50) loop
            begin
              read(F_cub_aux,my_graphe.bcub);
              write(My_graphe.f_cub,my_graphe.bcub);
              Exception when TEXT_IO.END_ERROR => 
                close(f_cub_aux);
                exit; 
            End;
        end loop;
        Close (F_cub_aux);
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2); 
      End;                   
      
      -- Fichier des Liens
      Nomfic(Nom'length+1..Nom'length+4):=".GEL";
        begin
        Open(F_lnk_aux, In_file,Nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GEL_TMP";
        Create(My_graphe.f_lnk,inout_file,Nomfic); 
        for i in 1..ceil(entete.nb_liens,128) loop
            Begin
              read(F_lnk_aux,my_graphe.blnk);
              write(My_graphe.f_lnk,my_graphe.blnk);
              Exception when TEXT_IO.END_ERROR => 
                close(f_lnk_aux);
                exit; 
            End;
        end loop;
        Close (F_lnk_aux);
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2); 
      End;
      
      -- Fichier des Domaines
      Nomfic(Nom'length+1..Nom'length+4):=".GED";
      Begin
        Open(F_dom_aux, In_file,nomfic);
        Nomfic(Nom'length+1..Nom'length+8):=".GED_TMP";
        Create(My_graphe.f_dom,inout_file,Nomfic); 
        for i in 1..ceil(entete.nb_domaines,128) loop
            begin
              read(F_dom_aux,my_graphe.bdom);
              write(My_graphe.f_dom,my_graphe.bdom);
              Exception when TEXT_IO.END_ERROR => 
                close(f_dom_aux);
                exit; 
            End;
        end loop;            
        Exception when TEXT_IO.NAME_ERROR =>
            put_line( " le fichier "& nomfic & " n'existe pas ");
            new_line(2); 
        Close (F_dom_aux);
      End;
      
    else              
       Nomfic(Nom'length+1..Nom'length+4):=".GEP";
          Begin
         Open(My_graphe.F_pts,In_file,nomfic);
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GEC";
          Begin
          Open(My_graphe.F_cub,In_file,nomfic); 
          Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GEL";
               begin
       Open(My_graphe.F_lnk,In_file,nomfic); 
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
       Nomfic(Nom'length+1..Nom'length+4):=".GED";
              Begin
       Open(My_graphe.F_dom,In_file,nomfic); 
       Exception when TEXT_IO.NAME_ERROR =>
          put_line( " le fichier "& nomfic & " n'existe pas ");
          new_line(2);
       end;
          end if;  
   return my_graphe;
end;

-- ===========================================================================
Separate(Gen_io) 
Function  GR_INFOS(graphe : graphe_type) return info_type is

begin
   return graphe.info;
end;

-- ===========================================================================
Separate(Gen_io) 
Procedure GR_SAVE_TO_DISK (graphe : graphe_type; nom : string) is

F_inf : Entete_io.file_type;
F_arc : Arc_io.file_type;
F_nod : Noeud_io.file_type;
F_fac : Face_io.File_type;
F_pts_aux : Point_io.File_type; 
F_cub_aux : Cubique_io.File_type; 
F_lnk_aux : Liens_io.File_type;
F_dom_aux : Liens_io.File_type;
entete : entete_type;
tab_pts : point_liste_type(1..graphe.info.Max_pts_arc);
nb_pts,ptstotal,indice_pts,nbuf,max_pts : integer;
bufpts : point_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');
begin
   
   -- ecriture des points et des cubiques en condensant les fichiers
    
   Nomfic(Nom'length+1..Nom'length+4):=".GEP";
   Create(f_pts_aux,inout_file,Nomfic); 
   Reset(graphe.f_pts);
   
   Nomfic(Nom'length+1..Nom'length+4):=".GEC";
   Create(f_cub_aux,inout_file,Nomfic); 
   Reset(graphe.f_cub);

   Indice_pts:=1;
   Nbuf:=0;
   ptstotal:=0;
   max_pts:=0;
   For i in 1..graphe.info.nb_arcs loop
       if graphe.tarc(i).Mode_met=Ligne then
          gr_points_d_arc(graphe,i,tab_pts,nb_pts);
          -- mise a jour du maximum de points par arc
          if nb_pts > max_pts then
             max_pts:=nb_pts;
          end if;
          graphe.tarc(i).premier_met:=Indice_pts;
          ptstotal:=ptstotal+nb_pts;
          -- Bufferisation des points
          for j in 1..nb_pts loop
              Nbuf:=nbuf+1;
              if Nbuf=129 then
                 write(f_pts_aux,bufpts);
                 bufpts(1..128):=(others=>(0,0));
                 Nbuf:=1;
                 bufpts(Nbuf):=tab_pts(j);
              else 
                 bufpts(Nbuf):=tab_pts(j);
              end if;
          end loop;
          Indice_pts:=Indice_pts+nb_pts;
       else
         -- meme traitement pour la liste de cubiques
         -- il faudrait faire le meme traitement, mais on n'utilise 
         -- plus les cubiques 
         null;
       end if;
   end loop;
   write(f_pts_aux,bufpts);
   Close(f_pts_aux);
   Graphe.Lblp:=0;
   Graphe.info.Nb_points:=ptstotal;
   Graphe.Info.Max_pts_arc:=max_pts;

   -- Ecriture des cubiques --
   For i in 1..ceil(graphe.info.nb_cubiques,50) loop
      read(graphe.f_cub,graphe.bcub);
      write(f_cub_aux,graphe.bcub);
   end loop;
   Close(f_cub_aux);
   Graphe.Lblc:=0;

   -- Ecriture des arcs --
   
   Nomfic(Nom'length+1..Nom'length+4):=".GEA";
   Create(f_arc,out_file,Nomfic); 
   For i in 1..graphe.info.nb_arcs loop
      write(f_arc,graphe.tarc(i));
   end loop;
   Close(f_arc);

   -- Ecriture des noeuds --
   Nomfic(Nom'length+1..Nom'length+4):=".GEN";
   Create(f_nod,out_file,Nomfic); 
   For i in 1..graphe.info.nb_noeuds loop
      write(f_nod,graphe.tnod(i));
   end loop;
   Close(f_nod);

   -- Ecriture des liens --
   Nomfic(Nom'length+1..Nom'length+4):=".GEL";
   Create(f_lnk_aux,inout_file,Nomfic); 
   Reset(graphe.f_lnk);
   For i in 1..ceil(graphe.info.nb_liens,128) loop
      read(graphe.f_lnk,graphe.blnk);
      write(f_lnk_aux,graphe.blnk);
   end loop;
   Close(f_lnk_aux);

   -- Ecriture des faces --
   Nomfic(Nom'length+1..Nom'length+4):=".GEF";
   Create(f_fac,out_file,Nomfic); 
   For i in 1..graphe.info.nb_faces loop
      write(f_fac,graphe.tfac(i));
   end loop;
   Close(f_fac);

   -- Ecriture des domaines --
   Nomfic(Nom'length+1..Nom'length+4):=".GED";
   Create(f_dom_aux,out_file,Nomfic); 
   Reset(graphe.f_dom);
   For i in 1..ceil(graphe.info.nb_domaines,128) loop
      read(graphe.f_dom,graphe.bdom);
      write(f_dom_aux,graphe.bdom);
   end loop;
   Close(f_dom_aux);

   -- Ecriture des Infos --
   entete.info:=graphe.info;
   Nomfic(Nom'length+1..Nom'length+4):=".GEI";
   Create(f_inf,out_file,Nomfic); 
   Write(f_inf,entete);
   Close(f_inf);

end;

-- ===========================================================================
Separate(Gen_io) 
Function  GR_CREATE_NEW ( deltax : Positive; 
                          deltay : positive;
                          Resolution : Integer;
                          Echelle : Integer :=1; 
                          Narcs : Positive :=100;
                          Nnoeuds : positive :=100;
                          Nfaces : Positive :=100;
                          Nom_tmp : string :="Unnamed_graphe"
                           )return graphe_type is

My_graphe : Graphe_type;
N : Integer;         
Nomfic:string(1..120):=(1..120=>' ');

begin
   my_graphe := ALLOCATE(Narcs, Nnoeuds, Nfaces);
   my_graphe.Info.delta_x:=deltax;
   my_graphe.Info.delta_y:=deltay;
   my_graphe.Info.resolution:=resolution;
   my_graphe.Info.echelle:=echelle;
   my_graphe.mode:=Inout_graphe;     

   
   -- autre initialisation nulle :
   my_graphe.Info.nb_arcs:=0;
   my_graphe.Info.nb_noeuds:=0;
   my_graphe.Info.nb_faces:=0;
   my_graphe.Info.nb_points:=0;
   my_graphe.Info.nb_cubiques:=0;
   my_graphe.Info.nb_liens:=0;
   my_graphe.Info.nb_domaines:=0;
   my_graphe.Info.nb_graph_l:=0;
   my_graphe.Info.nb_graph_p:=0;
   my_graphe.Info.nb_graph_z:=0;
   my_graphe.Info.max_pts_arc:=0;
   my_graphe.Info.max_cub_arc:=0;
   my_graphe.Info.max_arc_neu:=0;
   my_graphe.Info.max_arc_fac:=0;
   
    nomfic(1..Nom_tmp'last):= Nom_tmp;   
    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEP_TMP";
    Create(My_graphe.f_pts,inout_file,Nomfic); 

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEC_TMP";
    Create(My_graphe.f_cub,inout_file,Nomfic); 

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GEL_TMP";
    Create(My_graphe.f_lnk,inout_file,Nomfic); 

    Nomfic(Nom_tmp'last+1..Nom_tmp'last+8):=".GED_TMP";
    Create(My_graphe.f_dom,inout_file,Nomfic); 
  
   return my_graphe;
end;   

-- ===========================================================================
Separate(Gen_io) 
Procedure GR_DELETE ( graphe : in out Graphe_type) is
begin
   if graphe.mode=In_graphe then
      close(graphe.f_pts);
      close(graphe.f_cub);
      close(graphe.f_lnk);
      close(graphe.f_dom);
   else
      delete(graphe.f_pts);
      delete(graphe.f_cub);
      delete(graphe.f_lnk);
      delete(graphe.f_dom);
   end if;
   Gr_annule_region(Graphe);
   Free_graphe(Graphe);
end;


-- ===========================================================================
Separate(Gen_io) 
Function GR_IS_REGION(graphe : graphe_type) return boolean is
begin
   if graphe.pasx/=0 then 
      return true;
   else
      return false;
   end if;
end;

-- ===========================================================================
Separate(Gen_io) 
Procedure GR_REGION(graphe : in out graphe_type;
                      Pasx : positive:=256; 
                      Pasy : positive:=256) is
 package entier_io is new integer_io(integer); use entier_io;
-- Buffers intermediaires pour liste des regions par arcs --
Type Buffer_type;
Type Buffer_access_type is access Buffer_type;
Type Buffer_type is record
   Values : Liens_array_type(1..8192);
   Nextb  : Buffer_access_type;
end record;
PBuf_init,PBuf,old : Buffer_access_type;
Procedure Free_buf is new unchecked_deallocation(Buffer_type,
     Buffer_access_type);
Procedure Free_preg is new unchecked_deallocation(Liens_array_type,
     Regbuf_type);
--
-- Variables intermediaires ---
Nbreg,Nbregx,nbregy,somme,ir,ind,ind_1er_face_reg_i  : Natural;
face_prise : boolean;
Prega,Pregn,Pregf : Regadr_type;
Pregb,Prego,Pregg,Preggprov  : regbuf_type;
Rarcs : Liens_array_type(1..graphe.info.nb_arcs);
Rnods : Liens_array_type(1..graphe.info.nb_noeuds);
Rx1,rx2,ry1,ry2 : integer;
pas : positive ;

begin
   -- Modif FL du 30/12/97 : adaption du pas de regionalisation a la taille du
   -- fichier pour gerer les fichiers tres grands. Par defaut la valeur est
   -- 256*256
   -- On fixe apres experience qu'une taille superieure a 100*100 est interdite
   pas:=pasx;
   loop
     if max(1,graphe.info.delta_x/pas)*max(1,graphe.info.delta_y/pas)<10000 then
        exit;
     else
        pas:=pas*2;
     end if;
   end loop;
  
   --- Determination du nombre de regions ---
   Nbregx:=ceil(graphe.info.delta_x,pas);
   Nbregy:=ceil(graphe.info.delta_y,pas);
   
   Nbreg:=nbregx*nbregy;  
   Prega:=new array_reg_type(1..Nbreg);
   Pregn:=new array_reg_type(1..Nbreg);
   Pregf:=new array_reg_type(1..Nbreg);
   
   --- Parcours des arcs et determination des regions auxquels ils 
   --- appartiennent... en stockant l'info dans les tableaux values...

   Pbuf_init:= new Buffer_type;
   Pbuf:=pbuf_init;
   ind:=0;
   for i in 1..graphe.info.nb_arcs loop
      rx1:=ceil(graphe.tarc(i).encombr.minimum.coor_x,pas);
      rx2:=ceil(graphe.tarc(i).encombr.maximum.coor_x,pas);
      ry1:=ceil(graphe.tarc(i).encombr.minimum.coor_y,pas);
      ry2:=ceil(graphe.tarc(i).encombr.maximum.coor_y,pas);
      ir:=(ry1-1)*nbregx+rx1-1;
      for j in ry1..ry2 loop
         for k in rx1..rx2 loop
            ir:=ir+1;
            ind:=ind+1;
            if ind>pbuf.values'length then
               pbuf.nextb:=new buffer_type;
               pbuf:=pbuf.nextb;
               ind:=1;
            end if;
            pbuf.values(ind):=ir;
            prega.all(ir).count:=prega.all(ir).count+1;
         end loop;
         ir:=ir+nbregx-rx2+rx1-1;
      end loop;
      rarcs(i):=(rx2-rx1+1)*(ry2-ry1+1);
   end loop;

   --- Mise en place des valeurs First et preparation des compteurs ---
   Prega.all(1).first:=1;
   for i in 1..nbreg-1 loop
      Prega.all(i+1).first:=Prega.all(i).count+Prega.all(i).first;
      Prega.all(i).count:=0;
   end loop;      
   Somme:=Prega.all(nbreg).First+prega.all(nbreg).count-1;
   Prega.all(nbreg).count:=0;

   --- Rangement des arcs et liberation de la memoire ---
   pregb:=new Liens_array_type(1..somme+increment);
   pbuf:=pbuf_init;
   ind:=0;
   for i in 1..graphe.info.nb_arcs loop
      for j in 1..rarcs(i) loop
         ind:=ind+1;
         if ind>pbuf.values'length then
            old:=pbuf;
            pbuf:=pbuf.nextb;
            free_buf(old);
            ind:=1;
         end if;
         ir:=pbuf.values(ind);
         pregb.all(prega.all(ir).first+ prega.all(ir).count):=i;
         prega.all(ir).count:=prega.all(ir).count+1;
      end loop;
   end loop;
   free_buf(pbuf);

   --- Regionalisation des Noeuds ---
   --- determination de la region de chaque noeud --
   for i in 1..graphe.info.nb_noeuds loop
      rx1:=ceil(graphe.tnod(i).coords.coor_x,pas);
      ry1:=ceil(graphe.tnod(i).coords.coor_y,pas);
      ir:=(ry1-1)*nbregx+rx1;
      Rnods(i):=ir;
      pregn.all(ir).count:=pregn.all(ir).count+1;
   end loop;     

   --- Mise en place des valeurs fisrt et preparation des compteurs ---
   Pregn.all(1).first:=1;
   for i in 1..nbreg-1 loop
      Pregn.all(i+1).first:=Pregn.all(i).count+Pregn.all(i).first;
      Pregn.all(i).count:=0;
   end loop;      
   Pregn.all(nbreg).count:=0;

   --- Rangement des noeuds ---
   prego:=new Liens_array_type(1..graphe.info.nb_noeuds+increment);
   for i in 1..graphe.info.nb_noeuds loop
      prego.all(pregn.all(rnods(i)).first+ pregn.all(rnods(i)).count):=i;
      pregn.all(rnods(i)).count:=pregn.all(rnods(i)).count+1;
   end loop;


   --- Regionalisation des faces  ----
   preggprov:=new Liens_array_type(1..2*somme);
   ind:=0;
   pregf.all(1).first:=1;
   for i in 1..nbreg loop
      pregf.all(i).count:=0;

      ind_1er_face_reg_i:=ind+1;
      if prega.all(i).count/=0 then
         -- on etudie tous les arcs de la region
         for j in 1..prega.all(i).count loop
            -- face gauche de l arc appartient-il deja a la liste des faces?
            face_prise:=false;
            for k in 1..pregf.all(i).count loop
               if graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_g=
                         preggprov.all(ind_1er_face_reg_i-1+k) then
                  face_prise:=true;
                  exit;
               end if;
            end loop;
            -- si la face gauche n existe pas dans la liste on ajoute cette face
            if face_prise=false and 
               graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_g/=0 then
               ind:=ind+1;
               pregf.all(i).count:=pregf.all(i).count+1;
               preggprov.all(ind):=graphe.tarc(pregb.all(prega.all(i).first
                                                                  -1+j)).face_g;
            end if;
            -- face droite de l arc appartient-il deja a la liste des faces?
            face_prise:=false;
            for k in 1..pregf.all(i).count loop
               if graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_d=
                         preggprov.all(ind_1er_face_reg_i-1+k) then
                  face_prise:=true;
                  exit;
               end if;
            end loop;
            -- si la face droite n existe pas dans la liste on ajoute cette face
            if face_prise=false and 
               graphe.tarc(pregb.all(prega.all(i).first-1+j)).face_d/=0 then
               ind:=ind+1;
               pregf.all(i).count:=pregf.all(i).count+1;
               preggprov.all(ind):=graphe.tarc(pregb.all(prega.all(i).first
                                                                  -1+j)).face_d;
            end if;
         end loop;
      end if;
      -- Mise en place de la valeur first suivante
      if i/=nbreg then
         pregf.all(i+1).first:=pregf.all(i).first+pregf.all(i).count;
      end if;
   end loop;
   -- on peut allouer maintenant la bonne taille du tableau des faces
   pregg:=new Liens_array_type(1..ind+increment);
   pregg(1..ind):=preggprov(1..ind);
   free_preg(preggprov);


   --- Stockage des donnees dans la strucure de graphe ----
   graphe.pasx:=pas;
   graphe.pasy:=pas;
   graphe.nbregx:=nbregx;
   graphe.nbregy:=nbregy;
   graphe.radr_a:=prega;
   graphe.rblk_a:=pregb;
   graphe.rfin_a:=prega.all(Nbreg).first+prega.all(Nbreg).count-1;
   graphe.radr_n:=pregn;
   graphe.rblk_n:=prego;
   graphe.rfin_n:=pregn.all(Nbreg).first+pregn.all(Nbreg).count-1;
   graphe.radr_f:=pregf;
   graphe.rblk_f:=pregg;
   graphe.rfin_f:=pregf.all(Nbreg).first+pregf.all(Nbreg).count-1;
end;

-- ===========================================================================
Separate(Gen_io) 
Function GR_REGION_OF(Graphe : graphe_type;
                      Point : Point_type) 
                      return natural is
rx,ry,ir : natural;
begin
   rx:=ceil(Point.coor_x,Graphe.pasx);
   ry:=ceil(Point.Coor_y,Graphe.pasy);
   ir:=(ry-1)*graphe.nbregx+rx;
   return ir;
end;   


-- ===========================================================================
Separate(Gen_io) 
Procedure GR_ANNULE_REGION(graphe : in out graphe_type) is

Procedure Free_buf is new unchecked_deallocation(Liens_array_type,
     Regbuf_type);
Procedure Free_adr is new unchecked_deallocation(Array_reg_type,
     Regadr_type);

--
begin
   Graphe.pasx:=0;
   Graphe.pasy:=0;
   Graphe.Nbregx:=0;
   Graphe.Nbregy:=0;

   Graphe.rfin_a:=0;
   Graphe.rvid_a:=0;
   Graphe.rfin_n:=0;
   Graphe.rvid_n:=0;
   Graphe.rfin_f:=0;
   Graphe.rvid_f:=0;

   Free_buf(graphe.Rblk_a);
   Free_adr(graphe.Radr_a);
   Free_buf(graphe.Rblk_n);
   Free_adr(graphe.Radr_n);
   Free_buf(graphe.Rblk_f);
   Free_adr(graphe.Radr_f);
end;



-- ====================================================================
Separate(Gen_io) 
Procedure AMENAGER_REGION(radr: in out regadr_type;
                          rblk: in out regbuf_type; 
                          rfin : in out natural;
                          rvid : in out natural;
                          ajout : positive) is

-- Recuperation de place dans le buffer d'entier rblk trop petit, utile a la 
-- regionalisation du graphe,
-- soit par agrandissement du tableau
-- soit par condensation du tableau s il y a de la place vide a recuperer

pregbprov : regbuf_type;
procedure Free_pregb is new unchecked_deallocation(Liens_array_type,regbuf_type);

pregaprov : regadr_type;
numreg : regbuf_type;
procedure Free_prega is new unchecked_deallocation(Array_reg_type,regadr_type);
n,imin,vmin1,vmin2,interm1,interm2,interm3,decalage : natural;

begin

   if rvid < ajout then        -- cas ou on agrandit le tableau
      pregbprov:= new Liens_array_type(1..(rblk.all'length + ajout));
      pregbprov.all(1..rblk.all'length):=rblk(1..rblk.all'length);
      free_pregb(rblk);
      rblk:=pregbprov;
   else                        -- cas ou on condense le tableau
      -- initialisation
      n:=radr.all'length;
      numreg:= new Liens_array_type(1..n);
      pregaprov:= new Array_reg_type(1..n);
      for i in 1..n loop
          numreg.all(i):=i;
      end loop;
      pregaprov.all(1..n):=radr.all(1..n);
      -- tri du tableau pregaprov dans l ordre croissant des valeurs  de 
      -- pregaprov.first et en cas d egalite dans l ordre croissant de
      -- pregaprov.count (cas particulier ou pregaprov.count=0)
      for i in 1..n-1 loop
         imin:=i;
         vmin1:=pregaprov.all(i).first;
         vmin2:= pregaprov.all(i).count;
         for j in i+1..n loop
            if pregaprov.all(j).first<vmin1 then
               vmin1:=pregaprov.all(j).first;
               vmin2:=pregaprov.all(j).count;
               imin:=j;
            elsif pregaprov.all(j).first=vmin1 then
                  if pregaprov.all(j).count<vmin2 then
                     vmin1:=pregaprov.all(j).first;
                     vmin2:=pregaprov.all(j).count;
                     imin:=j;
                  end if;         
            end if;
         end loop;
         interm1:=numreg(i);
         interm2:=pregaprov.all(i).first;
         interm3:=pregaprov.all(i).count;
         numreg(i):=numreg(imin);
         pregaprov.all(i).first:=pregaprov.all(imin).first;
         pregaprov.all(i).count:=pregaprov.all(imin).count;
         numreg(imin):=interm1;
         pregaprov.all(imin).first:=interm2;
         pregaprov.all(imin).count:=interm3;
      end loop;
      -- decalage des adresses
      decalage:=pregaprov.all(1).first-1;
      interm2:=pregaprov.all(1).first;
      pregaprov.all(1).first:=1;
      radr.all(numreg(1)):=pregaprov.all(1);
      rblk.all(1..pregaprov.all(1).count):=
                           rblk.all(interm2..interm2+pregaprov.all(1).count-1);
      for i in 2..n loop
         decalage:=decalage+pregaprov.all(i).first - interm2 
                                                   - pregaprov.all(i-1).count;
         interm2:=pregaprov.all(i).first;
         pregaprov.all(i).first:=pregaprov.all(i).first - decalage;
         radr.all(numreg(i)):=pregaprov.all(i);
         rblk.all(pregaprov.all(i).first..
                           pregaprov.all(i).first+pregaprov.all(i).count-1)
                :=rblk.all(interm2..interm2+pregaprov.all(i).count-1);
      end loop;
      free_pregb(numreg);     
      free_prega(pregaprov);
      rvid:=0;
      rfin:=rfin-decalage;
   end if;

end;   


-- ======================================================================
Separate(Gen_io) 
Procedure AJOUT_ELEMENT_IN_REGION(radr : in out regadr_type;
                                  rblk : in out regbuf_type; 
                                  rfin : in out natural;
                                  rvid : in out natural;
                                  element : positive;
                                  ir : positive) is

-- Ajout eventuel d un element (face, arc, noeud) dans la region ir

deja : boolean;

begin
   deja:=false;
   -- verification de la presence eventuelle de l element dans la region
   for i in 1..radr.all(ir).count loop
       if rblk.all(radr.all(ir).first+i-1)=element then
          deja:=true;
          exit;
       end if;
   end loop;
   if deja=false then
      -- il faut ajouter l element dans la region ir et on ecrit en fin de
      -- tableau
      if rblk.all'length < rfin + radr.all(ir).count+1 then
         -- amenagement du tableau necessaire
         AMENAGER_REGION(radr,rblk,rfin,rvid,radr.all(ir).count+increment);
      end if;
      -- affectation de tableau de 1 a radr.all(ir).count :
      rblk.all(rfin+1..rfin+radr.all(ir).count):=rblk.all(radr.all(ir).first..
                                      radr.all(ir).first+radr.all(ir).count -1);
      rvid:=rvid+radr.all(ir).count;
      radr.all(ir).first:=rfin+1;
      radr.all(ir).count:=radr.all(ir).count+1;
      rfin:=rfin+radr.all(ir).count;
      rblk.all(rfin):=element;
   end if;

end;


