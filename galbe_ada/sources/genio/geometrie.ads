With Gen_io;use Gen_io;
With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
With math_int_basic; use math_int_basic; 

Package Geometrie is

 pi: constant float :=3.1415926536;

------------------------------------------------
-- Fonctions Relatives au calcul de distances --
------------------------------------------------

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites
--
-- Sortie : distance euclidienne (puissance 2) entre les deux points (en reel)
-----------------------------------------------------------------------------
Function Distance_de_points(M : in Point_type; 
			    N : in Point_type) return float;

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites
--
-- Sortie : distance euclidienne (puissance 2) entre les deux points (en reel)
-----------------------------------------------------------------------------
Function Distance_de_points_long(M : in Point_type; 
			    N : in Point_type) return long_float;

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites
--
-- Sortie : distance euclidienne (puissance 2) entre les deux points (en entier)                                
-----------------------------------------------------------------------------
Function Distance_de_points(M : in Point_type; 
			    N : in Point_type) return integer;

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites
--
-- Sortie : distance euclidienne (puissance 2) entre les deux points (en reel)
-----------------------------------------------------------------------------
Function Distance_de_points( M : in Point_type_reel; 
			     N : in point_type_reel) return float;

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites reels
--
-- Sortie : distance euclidienne (puissance 2) entre les deux points (en entier)                                
-----------------------------------------------------------------------------

Function Distance_de_points( M : in Point_type_reel; 
			     N : in point_type_reel) return integer;

-- --------------------------------------------------------------------------
-- Entree : M,N 	: Points extremites en entiers
--     ou : M,N 	: Points extremites en reels
--     ou : Mx,My,Nx,Ny : Coordonnees extremites en reels
-- Sortie : distance euclidienne entre les deux points (en entier) 
-----------------------------------------------------------------------------
Function norme_v2( M : in Point_type; N : in point_type) return float;

Function Norme(M : in Point_type; N : in Point_type) return float;

Function Norme(m,n : in point_type_reel) return float;

Function Norme( x1,y1,x2,y2 : float) return float;



-- --------------------------------------------------------------------------
-- Entree : M		: un point
--	    A,B		: Extremites du segment
--	    Mode	: Indique le mode de calcul : projection ou point plus proche 
--
-- Sortie : si Mode = projection alors distance du point M a la ligne de support [A,B]
--	    si Mode = point plus proche alors distance du point M au segment [A,B]
-----------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type; A : in Point_type; 
                    B : in Point_type; Mode : Distance_type) return float;

-- --------------------------------------------------------------------------
-- Entree : M		: un point
--	    A,B		: Extremites du segment
--	    Mode	: Indique le mode de calcul : projection ou point plus proche 
--
-- Sortie : si Mode = projection alors distance du point M a la ligne de support [A,B]
--	    si Mode = point plus proche alors distance du point M au segment [A,B]
-----------------------------------------------------------------------------
Function distance_a_ligne(M : in Point_type; A : in Point_type; 
                    B : in Point_type; Mode : Distance_type) return integer;

-- --------------------------------------------------------------------------
-- Entree : M		: un point (en reel)
--	    A,B		: Extremites du segment (en reels)
--	    Mode	: Indique le mode de calcul : projection ou point plus proche 
--
-- Sortie : si Mode = projection alors distance du point M a la ligne de support [A,B]
--	    si Mode = point plus proche alors distance du point M au segment [A,B]
-----------------------------------------------------------------------------

Function distance_a_ligne(M : in Point_type_reel; 
			  A : in Point_type_reel; 
                    	  B : in Point_type_reel; 
		       Mode : Distance_type) return float;

-- --------------------------------------------------------------------------
-- Entree : M		: un point(en reel)
--	    A,B		: Extremites du segment (emn reels)
--	    Mode	: Indique le mode de calcul : projection ou point plus proche 
--
-- Sortie : si Mode = projection alors distance du point M a la ligne de support [A,B]
--	    si Mode = point plus proche alors distance du point M au segment [A,B]
-----------------------------------------------------------------------------

Function distance_a_ligne(M : in Point_type_reel; 
			  A : in Point_type_reel; 
                    	  B : in Point_type_reel; 
		       Mode : Distance_type) return integer;

-- --------------------------------------------------------------------------
-- Entree : M		: un point
--	    PL		: La polyligne
-- 	    taille	: le nombre de points de la polyligne
--	    Mode        : Indique le mode de calcul : projection ou point plus proche
--
-- Sortie : Distance d'un point a une polyligne (reelle)
-----------------------------------------------------------------------------
Function Distance_a_polylignes( M  	: in Point_type; 
				PL 	: in Point_liste_type;
    	  			taille 	: in natural;
				Mode 	: in Distance_type) return float;


Function Distance_a_polylignes_v2( M : in Point_type; 
				                 PL : in Point_liste_type;
								 Indice_Ini : in natural;
    	  			             taille : in natural;
				                 Mode : in Distance_type) return float;


-- --------------------------------------------------------------------------
-- Entree : M		: un point
--	    PL		: La polyligne
-- 	    taille	: le nombre de points de la polyligne
--	    Mode        : Indique le mode de calcul : projection ou point plus proche
--
-- Sortie : Distance d'un point a une polyligne (entier)
-----------------------------------------------------------------------------
Function Distance_a_polylignes( M 	: in Point_type;
				PL 	: in Point_liste_type;
    	  			taille 	: in natural;
				Mode 	: in Distance_type) return integer;

-- --------------------------------------------------------------------------
-- Entree : M		: un point (reel)
--	    PL		: La polyligne (reelle)
-- 	    taille	: le nombre de points de la polyligne
--	    Mode        : Indique le mode de calcul : projection ou point plus proche
--
-- Sortie : Distance d'un point a une polyligne (reelle)
-----------------------------------------------------------------------------
Function Distance_a_polylignes( M 	: in Point_type_reel;
				PL 	: in Point_liste_reel;
    	  			taille 	: in natural;
				Mode 	: in Distance_type) return float;


------------------------------------------------
-- Fonctions relatives au calcul d'angles     --
------------------------------------------------

-- -----------------------------------------------------------------
-- Entree : A,B,C 	: Points formant l'angle (BA,BC) de sommet B 
--
-- Sortie : L'angle (BA,BC) en degres [-180:180]
--------------------------------------------------------------------
Function Angle_3points(	A : in Point_type; 
			B : in Point_type; 
			C : in Point_type) return angle_type;

-- -----------------------------------------------------------------------------------
-- Entree : A,B         : Extremites du segment
--
-- Sortie : L'angle (Ax,AB). Ax etant l'horizontal passant par A. 
--   En degres [-180:180]
--------------------------------------------------------------------------------------
Function Angle_Horizontal(A : in Point_type; 
			  B : in Point_type) return angle_type;

--*****************************************************************************
-- Angle_horizontal
--*****************************************************************************
-- Entree : A et B, point_type_reel
-- Sortie : l'angle du vecteur AB par rapport a l'horizontale,
-- en radians (entre -pi et pi)
Function Angle_horizontal(A,B	: in Point_type_reel) return Float;

-- -----------------------------------------------------------------
-- Entree : A,B,C 	: Points formant l'angle (BA,BC) de sommet B 
--
-- Sortie : L'angle (BA,BC) en radians [-pi:pi]
--------------------------------------------------------------------
Function Angle_3points( A : in Point_type;
			B : in Point_type;
			C : in Point_type) return float;

-- -----------------------------------------------------------------------------
-- Entree : A,B         : Extremites du segment
--
-- Sortie : L'angle (Ax,AB). Ax etant l'horizontal passant par A. En radians [-pi:pi]
--------------------------------------------------------------------------------
Function Angle_Horizontal(A : in Point_type; B : in Point_type) return float;


-- -----------------------------------------------------------------
-- Entree : A,B,C       : Points reels formant l'angle (BA,BC) de sommet B
--
-- Sortie : L'angle (BA,BC) en radians [-pi:pi]
--------------------------------------------------------------------
Function Angle_3points(A, B, C : Point_type_reel) return float ;



------------------------------------------------
-- Fonctions Produit vectoriel		      --
------------------------------------------------

-- --------------------------------------------------------------------------
-- Entree : M,O,N	: Les points formant les deux vecteurs
--
-- Sortie : Produit vectoriel entre les vecteurs MO et NO
-----------------------------------------------------------------------------
Function p_vectoriel(M,O,N : in Point_type) return float;

Function p_vectoriel(M,O,N : in Point_type_reel) return float;

-- --------------------------------------------------------------------------
-- Entree : ligne	: Liste des points de la ligne
--	    N		: Nombre de points de la ligne
--
-- Sortie : Liste des produits vectoriels calcules en chacun des points de la
--	    ligne en prenant a chaque fois les deux points voisins 
-----------------------------------------------------------------------------
Function p_vectoriel_polylignes(ligne 	: in Point_liste_type; 
				N 	: natural) return reel_tableau_type;


--------------------------------------------------
-- Fonctions relatives au calcul d'intersection --
--------------------------------------------------

-- ------------------------------------------------------------------------
-- Entree : A,B 	: Extremites du premier segment
--	    C,D		: Extremites du deuxieme segment
--	    Mode	: Indique le mode de calcul : segment ou ligne 
--
-- Sortie : Flags	: Mis a True s'il y a intersection
--	    M		: Coordonnees du point intersection (en entier)
-----------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D 	: Point_type;
				 Mode 		: Distance_type;
                                 Flags 		: out boolean; 
				 M 		: out Point_type);


--procedure Test_Intersection_de_segments(A,B,C,D : Point_type; Flags : out boolean);
                               
-- ------------------------------------------------------------------------
-- Entree : A,B 	: Extremites du premier segment
--	    C,D		: Extremites du deuxieme segment
--	    Mode	: Indique le mode de calcul : projection ou point plus proche 
--
-- Sortie : Flags	: Mis a True s'il y a intersection
--	    M		: Coordonnees du point intersection (en reel)
-----------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 Flags : out boolean; M : out Point_type_reel);

Procedure Intersection_de_lignes(A,B,C,D : Point_type_reel;
                                 Mode : Distance_type;
                                 Flags : out boolean;
                                 M : out Point_type_reel);

-- ------------------------------------------------------------------------
-- Supplement afin de gerer le cas de segments confondus
-- Entree : A,B 	: Extremites du premier segment
--	    C,D		: Extremites du deuxieme segment
--	    Mode	: Indique le mode de calcul : segment ou ligne 
--
-- Sortie : N 		: le nombre d'intersection (0,1 ou 2 (cas confondus))
--	    lres 	: Tableau des points intersection (coordonnees entieres)
-----------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 N : out natural; lres : in out Point_liste_type);

-- ------------------------------------------------------------------------
-- Supplement afin de gerer le cas de segments confondus
-- Entree : A,B 	: Extremites du premier segment
--	    C,D		: Extremites du deuxieme segment
--	    Mode	: Indique le mode de calcul : segment ou ligne 
--
-- Sortie : N 		: le nombre d'intersection (0,1 ou 2 (cas confondus))
--	    lres 	: Tableau des points intersection (coordonnees reelles) 
-----------------------------------------------------------------------------
Procedure Intersection_de_lignes(A,B,C,D : Point_type; Mode : Distance_type;
                                 N : out natural; lres : in out Point_liste_reel);

  Procedure Intersection_de_lignes(A,B,C,D : Point_type_reel;
                                   Mode : Distance_type;
                                   Ni : out natural;
                                   lres : in out Point_Liste_reel) ;


-- ------------------------------------------------------------------------
-- Entree : L1		: Premiere polyligne
--	    L2		: Deuxieme polyligne
--
-- Sortie : Lres	: Tableau des points d'intersection (en entier)
--	    N		: Nombre de points d'intersection
-----------------------------------------------------------------------------

Procedure Intersections_de_polylignes(	L1, L2 : in Point_liste_type; 
					N1,N2 : in natural; 				
					Lres : in out Point_Access_type; 
					N : out integer);

-- ------------------------------------------------------------------------
-- Entree : L1		: Premiere polyligne
--	    L2		: Deuxieme polyligne
--
-- Sortie : Lres	: Tableau des points d'intersection (en entier)
--	    N		: Nombre de points d'intersection
--      option  : passer TRUE uniquement pour le chargement du graphe des donnees lineaires
-----------------------------------------------------------------------------
Procedure Intersections_de_polylignes(	L1, L2 : in Point_liste_type; 
					N1,N2 : in natural; 
					Lres : in out Point_Access_type; 
					N : out integer; 
					Option : in boolean);

-- ------------------------------------------------------------------------
-- Entree : L1		: Premiere polyligne
--	    L2		: Deuxieme polyligne
--
-- Sortie : Lres	: Tableau des points d'intersection (en reel...prevoir un type)
--	    N		: Nombre de points d'intersection
-----------------------------------------------------------------------------
Procedure Intersections_de_polylignes(	L1, L2	: in Point_liste_type; 
					N1,N2 	: in natural;
					Lres	: in out Point_Access_reel;
					N 	: out integer);

-- ------------------------------------------------------------------------
-- Supplement afin de rendre compte de portions confondus
-- Entree : L1		: Premiere polyligne
--	    L2		: Deuxieme polyligne
--
-- Sortie : Lres	: Tableau des points d'intersection (en entier)
--	    N		: Nombre de points d'intersection
--	    Seg_Conf	: Liste des points correspondant aux portions communes
--	    NConf       : Nombres de points de cette liste
-----------------------------------------------------------------------------
Procedure Intersections_de_polylignes(L1, L2 	: in Point_liste_type; 
				      N1,N2  	: in natural; 
				      Lres   	: in out Point_Access_type;
				      N      	: out integer; 
				      Seg_Conf 	: in out Point_Access_type; 
				      NConf 	: in out natural);

-- ------------------------------------------------------------------------
-- Supplement afin de rendre compte de portions confondus
-- Entree : L1		: Premiere polyligne
--	    L2		: Deuxieme polyligne
--
-- Sortie : Lres	: Tableau des points d'intersection (en reel...prevoir un type)
--	    N		: Nombre de points d'intersection
--	    Seg_Conf	: Liste des points correspondant aux portions communes
--	    NConf       : Nombres de points de cette liste
-----------------------------------------------------------------------------
Procedure Intersections_de_polylignes(L1, L2	: in Point_liste_type;
				      N1,N2 	: in natural;
				      Lres 	: in out Point_Access_reel;
				      N 	: out integer;
				      Seg_Conf 	: in out Point_Access_type;
				      NConf 	: in out natural);

-- ------------------------------------------------------------------------
-- Entree : A,B,C,D	: Sommets du premier rectangle
--	    E,F,G,H	: Sommets du deuxieme rectangle
--
-- Sortie : Lres	: Tableau des points d'intersection (en entier)
--	    N 		: Nombre de points d'intersection
-----------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_type; N : out integer); 

-- ------------------------------------------------------------------------
-- Entree : A,B,C,D	: Sommets du premier rectangle
--	    E,F,G,H	: Sommets du deuxieme rectangle
--
-- Sortie : Lres	: Tableau des points d'intersection (en reel)
--	    N 		: Nombre de points d'intersection
-----------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_reel; N : out integer); 

-- ------------------------------------------------------------------------
-- Supplement, prise en compte des portions confondues
-- Entree : A,B,C,D	: Sommets du premier rectangle
--	    E,F,G,H	: Sommets du deuxieme rectangle
--
-- Sortie : Lres	: Tableau des points d'intersection (en entier)
--	    N 		: Nombre de points d'intersection
--	    Seg_Conf	: Liste des points correspondant aux portions communes
--	    NConf       : Nombres de points de cette liste
-----------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_type; N : out integer;Seg_Conf : in out Point_Access_type; NConf : in out natural); 

-- ------------------------------------------------------------------------
-- Supplement, prise en compte des portions confondues
-- Entree : A,B,C,D	: Sommets du premier rectangle
--	    E,F,G,H	: Sommets du deuxieme rectangle
--
-- Sortie : Lres	: Tableau des points d'intersection (en reel)
--	    N 		: Nombre de points d'intersection
--	    Seg_Conf	: Liste des points correspondant aux portions communes
--	    NConf       : Nombres de points de cette liste
-----------------------------------------------------------------------------
Procedure Intersection_rectangles(A,B,C,D : in Point_type; E,F,G,H : in Point_type; LRes : in out Point_Access_reel; N : out integer;Seg_Conf : in out Point_Access_type; NConf : in out natural); 

-- ------------------------------------------------------------------------
-- Entree : M		: un point
--	    S		: La surface
--
-- Sortie : Retourne True si le point M est contenu dans la surface S
-----------------------------------------------------------------------------
Function Est_il_dans_surface(M : Point_type; S : Surf_type) return boolean;
Function Est_il_dans_surface(M : Point_type_reel; S : Surf_type) return boolean;

-- ------------------------------------------------------------------------
-- Entree : M		: un point
--	    A,B,C,D	: Sommets du rectangle 
--
-- Sortie : Retourne True si le point M appartient au rectangle (ABCD)
-----------------------------------------------------------------------------
Function est_il_dans_rectangle(M : Point_type; A,B,C,D : in Point_type) return boolean;


---------------------------------------------------
-- Fonctions relatives aux resolutionx d'equations-
---------------------------------------------------
-- ------------------------------------------------------------------------
-- Entree : A,B,C 	: Coefficients de l'equation Ax2+Bx+C=0 (entier)
--
-- Sortie : N		: Nombre de solutions
--	    x1,x2	: La (ou les) solution(s) (entiere)
-----------------------------------------------------------------------------
--Procedure Equation2(A,B,C : in integer; N : out natural; x1,x2 : out integer);

-- ------------------------------------------------------------------------
-- Entree : A,B,C 	: Coefficients de l'equation Ax2+Bx+C=0 (reelle)
--
-- Sortie : N		: Nombre de solutions
--	    x1,x2	: La (ou les) solution(s) (reelle)
-----------------------------------------------------------------------------
Procedure Equation2(A,B,C : in float; N : out natural; x1,x2 : out float);

-- ------------------------------------------------------------------------
-- Entree : A,B 	: Coefficients de l'equation y=Ax+B (entier)
-- 	    X		: Valeur attribuee a l'abscisse X (entier)
--
-- Sortie : Y		: Valeur de l'ordonnee Y (entier)
-----------------------------------------------------------------------------
Procedure EquationY(A,B : in integer; X : in integer; Y : out integer);

-- ------------------------------------------------------------------------
-- Entree : A,B 	: Coefficients de l'equation y=Ax+B (reel)
-- 	    X		: Valeur attribuee a l'abscisse X (reel)
--
-- Sortie : Y		: Valeur de l'ordonnee Y (reel)
-----------------------------------------------------------------------------
Procedure EquationY(A,B : in float; X : in float; Y : out float);

-- ------------------------------------------------------------------------
-- Entree : A,B 	: Coefficients de l'equation y=Ax+B (entier)
-- 	    Y		: Valeur attribuee a l'ordonnee Y B (entier)
--
-- Sortie : flag 	: Mis a true si le calcul est possible (i-e A /= 0)
--	    X 		: Valeur de l'abscisse X si le calcul est possible B (entier)
-----------------------------------------------------------------------------
Procedure EquationX(A,B : in integer; Y : in integer; flag : in out boolean; X : out integer);

-- ------------------------------------------------------------------------
-- Entree : A,B 	: Coefficients de l'equation y=Ax+B (reel)
-- 	    Y		: Valeur attribuee a l'ordonnee Y B (reel)
--
-- Sortie : flag 	: Mis a true si le calcul est possible (i-e A /= 0)
--	    X 		: Valeur de l'abscisse X si le calcul est possible B (reel)
-----------------------------------------------------------------------------
Procedure EquationX(A,B : in float; Y : in float; flag : in out boolean; X : out float);


---------------------------------------------------
-- Fonction relative au changement de repere     --
---------------------------------------------------
-- ------------------------------------------------------------------------
-- Entree : M		: un point
--	    Vt		: Vecteur de la translation
--	    alpha	: Angle de la rotation	
--
-- Sortie : les coordonnes du point M dans le nouveau repere
-----------------------------------------------------------------------------
Function Transforme_repere(M 		: in Point_type;
			   Vt		: in Point_type:=(0,0);
			   alpha 	: in angle_type) return Point_type;

-- --------------------------------------------------------------------------
Function Transforme_repere(M 	 : in Point_type_reel;
			   Vt	 : in Point_type_reel:=(0.0,0.0);
			   alpha : in angle_type) return Point_type_reel;



-- ------------------------------------------------------------------------
-- Entree : M		: un point
--	    Vt		: Vecteur de la translation
--	    alpha	: Angle de la rotation en radians	
--
-- Sortie : les coordonnes du point M dans le nouveau repere
-----------------------------------------------------------------------------
Function Transforme_repere(M 	: in Point_type; 
			   Vt 	: in Point_type:=(0,0); 
			alpha   : in float:=0.0) return Point_type;

Function Transforme_repere(M 	: in Point_type_reel; 
			   Vt 	: in Point_type_reel:=(0.0,0.0); 
			alpha   : in float:=0.0) return Point_type_reel;

-- ------------------------------------------------------------------------
function ROTATION(p	: point_type;
		  an	: float) return point_type;

-- ------------------------------------------------------------------------
function ROTATION(p	: point_type_reel;
		  an	: float) return point_type_reel;

-- ------------------------------------------------------------------------

----------------------------------------------------------------------
-- Operateur sur des points (reels ou entiers)
---------------------------------------------------------------------
Function "+"(a,b : point_type) return point_type;
Function "+"(a,b : point_type_reel) return point_type_reel;
function "-" (a,b: point_type) return point_type;
function "-" (a,b: point_type_reel) return point_type_reel;
function "*" (A: point_type; k : integer) return point_type;
function "*" (A: point_type_reel; k : float) return point_type_reel;
function "*" (k:integer;A: point_type) return point_type;
function "*" (k : float;A: point_type_reel) return point_type_reel;
function "/" (A: point_type_reel; k : float) return point_type_reel;

function Scalaire(U,V : point_type) return integer;
function Scalaire(U,V : point_type_reel) return float;
function Vectoriel(U,V : point_type) return integer;
function Vectoriel(U,V : point_type_reel) return float;

-------------------------------------------------------------------------
-- Fonctions de passage de point(s) entier en point(s) reel et inversement 
-------------------------------------------------------------------------
function Reel(Arc : Point_liste_type) return Point_liste_reel;
function Reel(A : Point_type) return Point_type_reel;
function Entier(Arc : Point_liste_reel) return Point_liste_type;
function Entier(A : Point_type_reel) return Point_type;

---------------------------------------------------
-- Fonctions diverses...                         --
---------------------------------------------------
-- ----------------------------------------------------------------
-- Entree : P		: un point
--	    ligne	: La liste des points constituant la ligne
--                        
-- Sortie : Le rang du point de la ligne le plus proche du point P
-------------------------------------------------------------------
Function rang_point_plus_proche(P 	: in Point_type; 
				ligne	: in Point_liste_type;
				N 	: in natural) return natural;



-- **********************************************************************
--  POINT_MOYEN : Calcule le point moyen d'un ensemble de n_points defini 
-- 		  dans un tableau TAB_POINTS
-- **********************************************************************
Function POINT_MOYEN( tab_points : point_liste_type) return point_type;

Function POINT_MOYEN( tab_points : point_liste_reel) return point_type_reel;




-- ================================== --
-- Fonctions de CALCUL SUR LES LIGNES --
-- ================================== --

  Function taille_ligne(lg : in Point_liste_type; N : natural:=0) return float;

  Function taille_ligne(lg : in Point_liste_reel; N : natural:=0) return float;

  Function taille_ligne2(lg : in Point_liste_reel; N1 : natural:=0; N2 : natural:=0) return float;

  Function taille_ligne2(lg : in Point_liste_Type; N1 : natural:=0; N2 : natural:=0) return float;

  Function taille_arc(arc	: integer;
		      graphe	: graphe_type) return float;

  function surface_arc(Arc : point_liste_reel;
                       N : natural) return float ;
  function surface_arc(Arc : point_liste_type;
                       N : natural) return float ;

  Procedure Decompose_ligne(l_avant    	: in Point_liste_type; 
			    navant     	: in natural; 
			    l_apres    	: out Point_liste_type; 	
			    napres     	: out natural; 
			    pas        	: in float; 
			    nb_segments	: in out Liens_array_type);


  Procedure Decompose_ligne(l_avant	: in Point_liste_type; 
			    navant 	: in natural; 
			    l_apres	: out Point_liste_reel; 
			    napres 	: out natural; 
			    pas     	: in float;
	     		    nb_segments : in out Liens_array_type);

  Procedure Decompose_ligne(l_avant    : in Point_liste_type; 
			  navant       : in natural;
			  l_apres      : in out Point_Access_reel; 
			  napres       : out natural; 
			  pas          : in float; 
			  nb_segments  : in out Liens_array_type);


  Procedure Decompose_ligne(l_avant    : in Point_liste_reel;
                          navant     : in natural;
                          l_apres    : in out Point_Access_reel;
                          napres     : out natural;
                          pas        : in float;
                          nb_segments: in out Liens_array_type);


--*****************************************************************************
-- Procedure Decomposition_ligne 
--*****************************************************************************
-- Cf "geometrie" avec pointeur en sortie + gestion cas ou des segments 
-- de la ligne sont moins longs que le pas de decomposition
-- En entree une ligne a coord. entieres Ligne, en sortie :
-- * un pointeur sur une ligne decomposee a coord. reelles Ligne_dec
-- * un tableau des nb de segments sur Ligne_dec corresp a chaque segment
--   de Ligne
Procedure Decomposition_ligne(Ligne	: IN Point_liste_type;
			Pas		: IN Float;
			Ligne_dec	: OUT Point_access_reel;
			lgr_segments	: OUT Liens_array_type);

--*****************************************************************************
-- Procedure Decomposition_ligne - coord. reelles
--*****************************************************************************
-- Cf "geometrie" avec pointeur en sortie + gestion cas ou des segments 
-- de la ligne sont moins longs que le pas de decomposition
-- En entree une ligne a coord. reelles Ligne, en sortie :
-- * un pointeur sur une ligne decomposee a coord. reelles Ligne_dec
-- * un tableau des nb de segments sur Ligne_dec corresp a chaque segment
--   de Ligne
Procedure Decomposition_ligne(Ligne	: IN Point_liste_reel;
			Pas		: IN Float;
			Ligne_dec	: OUT Point_access_reel;
			lgr_segments	: OUT Liens_array_type);



-------------------------------------------------------------------------

  Procedure NOEUD_COMMUN(graphe : graphe_type;
                         a,b    : in integer;
                         ni,nf  : out integer);

--  function NOEUD_COMMUN(graphe	: graphe_type;
--			a	: integer;
--			b	: integer) return integer;

  function SURFACE_VECT(	p1,p2	: point_type) return float;

-- ====================================== --
-- Fonctions de PROJECTION SUR LES LIGNES --
-- ====================================== --

  Function PROJECTION_SEGMENT(p	: point_type;
			      a	: point_type;
			      b : point_type) return point_type;

  Function PROJECTION_SEGMENT(p	: point_type_reel;
			      a	: point_type_reel;
			      b : point_type_reel) return point_type_reel;



  Function  PROJECTION_POLYLIGNES(TAB	: point_liste_type;
				  I	: integer;
				  P	: point_type) return point_type;

  procedure  PROJECTION_POLYLIGNES_v2(TAB	: point_liste_type;
				  indice_ini : in integer;
				  I	: integer;
				  P	: point_type;
				  No_pred : out natural;
				  reste : out float);

  Function  PROJECTION_POLYLIGNES(TAB	: point_liste_type;
				  I  	: integer;
				  P	: point_type) return integer;

  Function  PROJECTION_POLYLIGNES(TAB	: point_liste_reel;
				  I	: integer;
				  P	: point_type_reel) return point_type_reel;

  Function  PROJECTION_POLYLIGNES(TAB	: point_liste_reel;
				  I  	: integer;
				  P	: point_type_reel) return integer;


-- ------------------------------------------------------------------------
-- Entree : P 		: Point a projeter sur la droite
-- 	    x0,(x,y)	: Droite engendree par x0,. 
--		   	  P1 optionnel. si absent, la droite passe par (0,0).
--
-- Sortie :P 		: Projection de P sur (P1,P2).
-----------------------------------------------------------------------------

  Function PROJECTION_DROITE(p,x0	: point_type;
			     x,y	: float) return point_type;


-- ------------------------------------------------------------------------
-- Entree : P 		: Point a projeter sur la droite
-- 	    P1,P2	: Droite engendree par P1,P2. 
--			  P1 optionnel. si absent, la droite passe par (0,0).
--
-- Sortie :P 		: Projection de P sur (P1,P2).
-----------------------------------------------------------------------------

  Function PROJECTION_DROITE(p	: point_type;
			     p1	: point_type:=(0,0);
			     p2	: point_type) return point_type;


  Function PROJECTION_DROITE(p,x0 : point_type_reel;
			     x,y  : float) return point_type_reel;

  Function PROJECTION_DROITE(p	: point_type_reel;
			     p1	: point_type_reel:=(0.0,0.0);
			     p2	: point_type_reel) return point_type_reel;


--**********************************************************
-- Geometrie, complements : CD
--**********************************************************

-------------------------------------------------------------------------
-- PROCEDURES PERMETTANT DE PASSER DES COORDONNEES D'UN POINT
-- DU PLAN A SA POSITION SUR UNE LIGNE OU INVERSEMENT ET PERMETTANT
-- DE FAIRE LE LIEN ENTRE LIGNE ORIGINALE ET LIGNE DECOMPOSEE
-------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Procedure Situe_sur_ini
------------------------------------------------------------------------
--  Pour une ligne decomposee decrite par le tableau de decomposition 
--  lgr_segment de cardinal npoints-1 (npoints nb de points de la ligne 
--  initiale),
--  et pour un point P de no pos_dec sur la ligne decomposee, renvoie le no 
--  no_pred du point de la ligne initiale precedent P et un reel compris entre
--  0 et 1 representant la position de P sur le segment no_pred, no_pred+1
-- CD
------------------------------------------------------------------------
Procedure Situe_sur_ini(lgr_segments	: in Liens_array_type;
			npoints		: in natural;
			pos_dec		: in natural;
			no_pred		: out natural;
			reste		: out float) ;

Procedure Situe_sur_ini_v2(lgr_segments	: in Liens_array_type;
			indice_ini : in integer;
            npoints		: in natural;
			pos_dec		: in natural;
			no_pred		: out natural;
			reste		: out float);

------------------------------------------------------------------------
-- Procedure Situe_sur_segm
------------------------------------------------------------------------
--  Pour une ligne decomposee decrite par le tableau de decomposition 
--  lgr_segment de cardinal npoints-1 (npoints nb de points de la ligne 
--  initiale),
--  et pour un point P, pas forcement un point intermediaire, de la ligne
--  initiale: en entree, P est decrit par le no no_pred du point de la ligne 
--  initiale qui le precede et un reel compris entre 0 et 1 representant la 
--  position de P sur le segment no_pred, no_pred+1.
--  En sortie, le no no_dec du point precedent P sur la ligne decomposee.

Procedure Situe_sur_segm(lgr_segments	: in Liens_array_type;
			npoints		: in natural;
			no_pred		: in natural;
			reste		: in float;
			pos_dec		: in out natural) ;


------------------------------------------------------------------------
-- Procedure Position_to_Abs_curv
------------------------------------------------------------------------
-- renvoie l'abscisse curviligne d'un point sur une ligne, pas forcement un
-- point intermediaire. Le point est decrit en entree par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Position_to_Abs_curv(ligne	: in Point_liste_type;
				npts	: in integer;
				no_pred	: in integer;
				reste	: in float;
				abscurv	: in out float);

Procedure Position_to_Abs_curv_v2(ligne	: in Point_liste_type;
				indice_ini : in integer;
                npts	: in integer;
				no_pred	: in integer;
				reste	: in float;
				abscurv	: in out float);

------------------------------------------------------------------------
-- Procedure Position_to_Coord
------------------------------------------------------------------------
-- renvoie les coordonnees d'un point sur une ligne, pas forcement un
-- point intermediaire. Le point est decrit en entree par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Position_to_Coord(ligne	: in Point_liste_type;
			npts	: in integer;
			no_pred	: in integer;
			reste	: in float;
			point	: in out Point_type);

------------------------------------------------------------------------
-- Procedure Abs_curv_to_Position
------------------------------------------------------------------------
-- en entree l'abscisse curviligne d'un point sur une ligne, pas forcement 
-- un point intermediaire. Le point est decrit en sortie par le i du point qui
-- le precede, et un reel reste compris entre 0 et 1 qui permet de le situer
-- entre Pi et Pi+1

Procedure Abs_curv_to_position(ligne	: in Point_liste_type;
				npts	: in integer;
				abscurv	: in float;
				no_pred	: in out integer;
				reste	: in out float);


--**********************************************************
-- Geometrie, complements : CD
--**********************************************************
Procedure Ajouter_un_vertex(Ligne		: IN Point_liste_type;
			Vertex		: IN Point_type;
			Position_pred	: IN positive;
			Ligne_out	: OUT Point_access_type);

Procedure Enlever_un_vertex(Ligne	: IN Point_liste_type;
			Position	: IN positive;
			Ligne_out	: OUT Point_access_type);

Function Filtrage(Tab : Point_liste_type) return Point_access_type;

Procedure Trier_tableau_croissant(Tab		: IN Liens_array_type;
				 Tab_trie	: OUT Liens_array_type);

Procedure Enlever_des_vertex(Ligne	: IN Point_liste_type;
			Positions	: IN Liens_array_type;
			Ligne_out	: OUT Point_access_type);

Procedure Ajouter_des_vertex(Ligne	: IN Point_liste_type;
			Vertex		: IN Point_liste_type;
			Positions	: IN Liens_array_type;
			Ligne_out	: OUT Point_access_type;
			Positions_apres	: OUT Liens_access_type);


--------------------------------------
-- GEOMETRIE_SURFACE
--------------------------------------
-- Calculs d'aires d'intersection et union de surface
-- NB: la validite de ces calculs est dependante de la la validite
-- de point_dans_surface
--------------------------------------
-- Sebastien Mustiere
-- 02/99
--------------------------------------

-- les surfaces en entree sont des point_liste_type fermes.

Function Surface_union(Surface1,
                       Surface2 : Point_liste_type)
                       return float;

Function Surface_inter(Surface1,
                       Surface2 : Point_liste_type)
                       return float;



END GEOMETRIE;
