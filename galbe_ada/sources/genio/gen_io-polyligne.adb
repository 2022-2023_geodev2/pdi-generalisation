with geometrie; use geometrie;
Separate(Gen_io)
Package body Polyligne is

-- Package de traitement des Polylignes, travaillant sur le fichier des points
--

Function  LIRE(graphe : graphe_type; 
               arc : positive) 
               return Point_liste_type is
Package entier_io is new integer_io(integer); use entier_io;

My_liste : Point_liste_type(1..graphe.info.max_pts_arc);
A_lire,P_lire,First,N,reste : integer;
begin
   A_lire:=ceil(graphe.tarc(arc).premier_met,128);
   if A_lire/=graphe.lblp then
      read(graphe.f_pts,graphe.bpts,Point_io.count(a_lire));
      graphe.lblp:=a_lire;
   end if;
   P_lire:=modulo(graphe.tarc(arc).premier_met-1,128);
   Reste:=graphe.tarc(arc).nombre_met;
   First:=0;
   while reste /=0 loop
      N:=MIN(reste,128-p_lire);
      My_liste(First+1..First+N):=Graphe.Bpts(P_lire+1..P_lire+N);
      Reste:=Reste-N;
      First:=first+N;
      if Reste>0 then
         Read(graphe.f_pts,graphe.bpts);
         Graphe.lblp:=graphe.lblp+1;
         P_lire:=0;
      end if;
   end loop;      
   return my_liste(1..graphe.tarc(arc).nombre_met);
end;


-- =====================================================================
Procedure ECRIRE_FIN(graphe : in out graphe_type;
                     tab_points : point_liste_type;
                     n : positive) is

-- Ecriture des points intermediaires en fin de fichier des points

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(graphe.info.nb_points+1,128);
   if A_lire/=graphe.lblp then
      if modulo(graphe.info.nb_points,128)/=0 then
         read(graphe.f_pts,graphe.bpts,Point_io.count(a_lire));
      end if;
      graphe.lblp:=a_lire;
   end if;
   P_lire:=modulo(graphe.info.nb_points,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bpts(P_lire+1..P_lire+K):=tab_points(First+1..First+K);
      Reste:=Reste-K;
      First:=First+K;
      write(graphe.f_pts,graphe.bpts,Point_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Graphe.lblp:=graphe.lblp+1;
         P_lire:=0;
      end if;
   end loop;  

end;
 

-- =====================================================================
Procedure ECRIRE(graphe : in out graphe_type;
                 tab_points : point_liste_type;
                 n : positive;
                 premier : positive ) is

-- Reecriture des points a la meme adresse du buffer de points (on ecrit donc
-- une liste dont le nombre d elements est inferieur ou egal a la liste de 
-- depart)

A_lire,P_lire,First,reste,k : integer;

begin
   A_lire:=ceil(premier,128);
   if A_lire/=graphe.lblp then
      read(graphe.f_pts,graphe.bpts,Point_io.count(a_lire));
      graphe.lblp:=a_lire;
   end if;
   P_lire:=modulo(premier-1,128);
   Reste:=n;
   First:=0;
   while reste /=0 loop
      K:=Min(reste,128-p_lire);
      Graphe.Bpts(P_lire+1..P_lire+K):=tab_points(First+1..First+K);
      Reste:=Reste-K;
      First:=first+K;
      write(graphe.f_pts,graphe.bpts,Point_io.count(a_lire));
      a_lire:=a_lire+1;
      if Reste>0 then
         Read(graphe.f_pts,graphe.bpts);
         Graphe.lblp:=graphe.lblp+1;
         P_lire:=0;
      end if;
    end loop;  

end;



end Polyligne;
