with a;
use a;
-- ============================================================================
Separate(Gen_io)
Function GR_CREATE_NEW_LEGENDE (leg_l : natural;
                      	     	leg_p : natural;
                           	leg_z: natural)
                           		return Legende_type is

My_legende  : Legende_type;
Begin
   my_legende:=new Legende_struc_type(leg_l,leg_p,leg_z);
   Return my_legende;
end;

-- ===========================================================================
Separate(Gen_io)
Function GR_LOAD_FROM_DISK_LEGENDE ( nom : string) return legende_type is

Entete : Info_type;
My_legende : Legende_type;
F_leg : legende_io.file_type;
Baff : Affichage_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');

begin
   -- Recuperation des Infos sur le disque --
   entete:=Gr_disk_infos(nom);

   -- Creation d'une nouvelle legende en memoire --
   My_legende:=GR_CREATE_NEW_LEGENDE(2*(entete.nb_graph_l+10),
                     2*(entete.nb_graph_p+10),2*(entete.nb_graph_z+10));

   -- Ouverture du fichier legende
     NomFic(1..nom'last):=Nom;
     nomfic(nom'last+1..nom'last+4 ):=".GEV";

     begin
        Open (F_leg, In_File, NomFic);
   		-- Chargement en memoire des informations de legende lineaire --
   		for i in 1..ceil(entete.nb_graph_l,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_l(128*(i-1)+1..Min(128*i,entete.nb_graph_l)):=
           	baff(1..Min(128,entete.nb_graph_l-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_l+1 en 0
   		if Modulo(entete.nb_graph_l,128)=0 then
      		read(f_leg,baff);
   		end if;
   		my_legende.tleg_l(0):=baff(Modulo(entete.nb_graph_l+1,128));

   -- Chargement en memoire des informations de legende ponctuelle --
   		for i in 1..ceil(entete.nb_graph_p,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_p(128*(i-1)+1..Min(128*i,entete.nb_graph_p)):=
           	baff(1..Min(128,entete.nb_graph_p-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_p+1 en 0
   		if Modulo(entete.nb_graph_p,128)=0 then
      		read(f_leg,baff);
   		end if;
   		my_legende.tleg_p(0):=baff(Modulo(entete.nb_graph_p+1,128));

   -- Chargement en memoire des informations de legende zonale --
   		for i in 1..ceil(entete.nb_graph_z,128) loop
       		read(F_leg,baff);
       		my_legende.tleg_z(128*(i-1)+1..Min(128*i,entete.nb_graph_z)):=
           	baff(1..Min(128,entete.nb_graph_z-(i-1)*128));
   		end loop;
   -- Chargement de la valeur nb_graph_zz+1 en 0
   		if Modulo(entete.nb_graph_z,128)=0 then
     	 	read(f_leg,baff);
   		end if;
   		my_legende.tleg_z(0):=baff(Modulo(entete.nb_graph_z+1,128));
   		Close (F_leg);
        Exception when TEXT_IO.NAME_ERROR =>
        --put_line( " le fichier "& nomfic & " n'existe pas ");
        --new_line(2);
        raise TEXT_IO.NAME_ERROR;
    end;
   	return my_legende;

end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_SAVE_TO_DISK_LEGENDE (Graphe : graphe_type; legende : Legende_type;
                                   nom : string) is

Entete : Info_type;
My_legende : Legende_type;
F_leg : legende_io.file_type;
Baff : Affichage_buffer_type;
Nomfic : string(1..80):=(1..80=>' ');
begin

   -- Ouverture du nouveau fichier legende ou on va sauvegarder les donnees
     NomFic(1..nom'last):=Nom;
     NomFic(nom'last+1..nom'last+4 ):=".GEV";
     Open (F_leg, out_File, NomFic);
   -- Recuperation du nombre d'affichages lineaires, ponctuels, zonaux
   entete:=GR_infos(graphe);

   -- Sauvegarde des informations de legende lineaire --
   for i in 1..ceil(Entete.nb_graph_l,128) loop
       baff(1..Min(128,Entete.nb_graph_l-(i-1)*128)):=
          legende.tleg_l(128*(i-1)+1..Min(128*i,Entete.nb_graph_l));
       if Min(128,Entete.nb_graph_l-(i-1)*128)<128 then
          baff(Entete.nb_graph_l+1-(i-1)*128):=Legende.tleg_l(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_l+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_l,128)=0 then
      baff(1):=Legende.tleg_l(0);
      write(F_leg,baff);
   end if;

   -- Sauvegarde des informations de legende ponctuelle --
   for i in 1..ceil(Entete.nb_graph_p,128) loop
       baff(1..Min(128,Entete.nb_graph_p-(i-1)*128)):=
          legende.tleg_p(128*(i-1)+1..Min(128*i,Entete.nb_graph_p));
       if Min(128,Entete.nb_graph_p-(i-1)*128)<128 then
          baff(Entete.nb_graph_p+1-(i-1)*128):=Legende.tleg_p(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_p+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_p,128)=0 then
      baff(1):=Legende.tleg_p(0);
      write(F_leg,baff);
   end if;

   -- Sauvegarde des informations de legende zonale --
   for i in 1..ceil(Entete.nb_graph_z,128) loop
       baff(1..Min(128,Entete.nb_graph_z-(i-1)*128)):=
          legende.tleg_z(128*(i-1)+1..Min(128*i,Entete.nb_graph_z));
       if Min(128,Entete.nb_graph_z-(i-1)*128)<128 then
          baff(Entete.nb_graph_z+1-(i-1)*128):=Legende.tleg_z(0);
       end if;
       write(F_leg,baff);
   end loop;
   -- Ecriture eventuelle de la valeur nb_graph_z+1 dans un nouvel enregistrement
   if Modulo(Entete.nb_graph_z,128)=0 then
      baff(1):=Legende.tleg_z(0);
      write(F_leg,baff);
   end if;

   close(F_leg);
end;

-- ===========================================================================
Separate(Gen_io)
Function  GR_LEGENDE_L (legende : Legende_type;
                        att_graph : natural)
                        return affichage_type is
begin
   return legende.tleg_l(att_graph);
end;

-- ===========================================================================
Separate(Gen_io)
Function  GR_LEGENDE_P (legende : Legende_type;
                        att_graph : natural)
                         return affichage_type is
begin
   return legende.tleg_p(att_graph);
end;

-- ===========================================================================
Separate(Gen_io)
Function  GR_LEGENDE_Z (legende : Legende_type;
                        att_graph : natural)
                        return affichage_type is
begin
   return legende.tleg_z(att_graph);
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_MOD_LEGENDE_L(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type) is
begin
   legende.tleg_l(att_graph):=affichage;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_MOD_LEGENDE_P(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type)  is
begin
   legende.tleg_p(att_graph):=affichage;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_MOD_LEGENDE_Z(legende : in out Legende_type;
                           att_graph : natural;
                           Affichage : affichage_type) is
begin
   legende.tleg_z(att_graph):=affichage;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_AJOUT_LEGENDE_L(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_AJOUT_LEGENDE_P(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;

-- ===========================================================================
Separate(Gen_io)
Procedure GR_AJOUT_LEGENDE_Z(legende : in out Legende_type;
                             att_graph : out natural ;
                             Affichage : affichage_type) is
begin
   att_graph:=0;
end;
