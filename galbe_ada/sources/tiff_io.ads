--========================== SPECIFICATION DU PAQUETAGE TIFF_IO =================================================

with unchecked_conversion;
with unchecked_deallocation;

package TIFF_IO is 
 
----------------------------------------------------------------------------------------------------------------------------------------
-- 1/ LES CONSTANTES
 
 SPP_MAX : constant integer := 4;-- nb maxi de composantes d'un pixel(Sample Per Pixel)  
 NBIFD_MAX : constant integer := 10;----- nb maxi d'IFDs dans un fichier Tiff
   
----------------------------------------------------------------------------------------------------------------------------------------
-- 2/ LES STRUCTURES
 
	 type string_2 is new string(1..2);
	 type string_4 is new string(1..4);
    
		-- En-tete(Image File Header)
		-----------------------------       
	 subtype T_arbitrary_number is string_2;
	 subtype T_offset_ifd1 is string_4;
	 subtype T_byte_order is string(1..2); -- si string_2 alors put_line inutilisable!??
	 
	 type T_IFH is record
		  byte_order : t_byte_order;
		  arbitrary_number : t_arbitrary_number;
		  offset_ifd1 : t_offset_ifd1;
	 end record;
	 
  -- Les Tags contenus dans les IFDs
  ----------------------------------
		subtype T_tagid is string_2;
		subtype T_typ is string_2;
		subtype T_nbvals is string_4;
	 subtype T_valoff is string_4;
	
	 type Typ_Tag is record
	   Tagid 	: T_Tagid; ---------- identifiant
	   Typ 			: T_Typ; ------------ type de la ou des valeurs du champ 'valoff' 
	   NbVals	: T_Nbvals; --------- nb de valeurs
	   valoff	: T_valoff; --- valeur(s) directe(s) ou adresse(s) o� trouver les valeurs
	 end record;
	
 	-- IFD(Image File Directory) 
		---------------------------- 	
	 subtype T_NDE is short_integer;
	 type Tablo_Tag is array (T_NDE range <>) of Typ_Tag; 
	 subtype t_offset_next_ifd is string_4;
	 
	 type IFD(NDE : T_NDE) is record----------------- NDE = nb de tags de l'IFD
		  tags : Tablo_Tag(1..NDE);--------------------- tableau contenant les tags
		  offset_next_ifd : t_offset_next_ifd;---------- adresse de l'IFD suivante       
	 end record;
	 
	 -- La liste des IFDs est un tableau de pointeurs sur les IFDs 
   -- (permettant ainsi de stocker dans une m�me structure,
 	 -- de mani�re indirecte, les IFDs qui peuvent �tre de tailles diff�rentes)
 	type Acces_IFD is access IFD;
		type T_Liste_IFDs is array (1..NBIFD_MAX) of Acces_IFD; 
  
  -- Tableau des adresses ou des longueurs de bande
  -------------------------------------------------
  type tabstrip is array (integer range <>) of integer;
  type acces_tabstrip is access tabstrip;
  
  -- Une Bande(strip) de l'image est un tableau d'octets
  --(octet represente par des caract�res)
  -------------------------------------------------------
   type strip is array (integer range <>) of character;

  -- La Zone de l'image � lire est un tableau de pixel
  ----------------------------------------------------
  subtype composante_pix is integer range 0..255;
  type pixel is array (1..SPP_MAX) of composante_pix;
  subtype lnpix is integer; ----------- d�finition des ss-types lnpix et colpix 
  subtype colpix is integer;----------- pour se garantir d'une inversion ligne/colonne 
  type typ_tabpixels is array (integer range <>, integer range <>) of pixel;           
  
--------------------------------------------------------------------------------------
-- 3/ FONCTIONS ET PROCEDURES
  
  -- fonctions de liberation de place m�moire
  -------------------------------------------
  procedure Free_tabstrip is  new unchecked_deallocation(tabstrip, acces_tabstrip);
  procedure Free_IFD is new unchecked_deallocation(IFD, acces_IFD);
	 
	-- fonctions permettant de r�interpr�ter un motif binaire contenu dans une chaine
	--------------------------------------------------------------------------------
	function convert2 is new unchecked_conversion(string_2, short_integer);
	function convert4 is new unchecked_conversion(string_4, long_integer);
  function convertfloat is new unchecked_conversion(string_4, float);
  function convert1 is new unchecked_conversion(character, integer);
                               
  --procedure de lecture des param�tres d'un fichier Tiff                                            
  ----------------------------------------------------------------------
  procedure Charge_param_image(lps, ncol, nlig: out integer; 
                               stripoffset: out acces_tabstrip);
 
--------------------------------------------------------------------------------
end TIFF_IO;





