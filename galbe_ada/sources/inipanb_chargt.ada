--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

-- Ce fichier contient les procedures separees du package Inipan. --------- 

--=========================================================
-- Chargement des fontes 
--=========================================================
Separate(INIPAN) procedure charge_fontes(info_font: out typtfont;
                                         nbfont: out integer ) is
                                     
AOC    : Win32.PSTR;
log_font : aliased Win32.wingdi.Logfont;
ficpolice_in :   text_io.file_type;
car: character;
ncar, icar : integer;
nom_police :string(1..maxcar); 
ch :   string(1..maxcar);

Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
MaxLLigne : integer:=200;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne);	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
last: positive ;
no_lig: text_io.count;

--=======================================================================================================
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
   function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
-- conversion de System.Address en Win32.LPCSTR 
      function UC is new Unchecked_Conversion
            (System.Address,Win32.LPCSTR);
  	  begin
        return UC(Str(Str'First)'Address);
   end Str_to_LPCSTR;
                  ----------------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR (pointeur sur un caractere constant)
   function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
-- conversion de System.Address en Win32.LPCSTR 
      function UC is new Unchecked_Conversion
            (System.Address,Win32.PCHAR);
  	  begin
        return UC(C_Str(C_Str'First)'Address);
   end CHAR_Array_to_PCHAR;
                  ----------------------------------------------------

use type win32.int;
use type win32.long;

begin
    
  -- Lecture des polices dans le fichier
  nbfont:=0;
  open(ficpolice_in,in_file,nomfichier_polices(1..ncar_pol));

  loop
     if end_of_file(ficpolice_in) then exit; end if;

     begin
        no_lig:=line(ficpolice_in);
        get_line(ficpolice_in,Ligne,LLigne);
     exception when text_io.END_ERROR =>
        close(ficpolice_in);
        Open(ficpolice_in,in_file,nomfichier_polices(1..ncar_pol));
        set_line(ficpolice_in, no_lig);
        LLigne:=0;
        while (not End_of_file(ficpolice_in)) and ( LLigne<MaxLLigne ) loop
          LLigne:=LLigne+1;
          get(ficpolice_in, Ligne(LLigne));
        end loop;
     end;
  
     nbfont:=nbfont+1;
     Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);
     Info_Font(nbfont).code(1..Ptr_indice(1)-1):=Ligne(1..Ptr_indice(1)-1);
     ncar:=Ptr_indice(2)-Ptr_indice(1)-1;
     nom_police(1..ncar):= Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);
     -- on rajoute le caractere nul en fin de chaine
     nom_police (1..ncar+1):=nom_police (1..ncar)&ascii.nul;
     AOC:= Win32.crt.strings.strcpy( CHAR_Array_to_PCHAR(log_font.lfFaceName),Str_to_LPCSTR(nom_police));
           
    -- lecture du corps en points
    flo_io.get(Ligne(Ptr_indice(2)+1..LLigne), Info_Font(nbfont).hauteur, last);
    -- lecture de l'inter-lettres en points
    flo_io.get(Ligne(Ptr_indice(3)+1..LLigne), Info_Font(nbfont).InterLettres, last);
    Gr_free_liens_array(Ptr_indice);
  
    Info_Font(nbfont).police.lffacename := log_font.lffacename;
    Info_Font(nbfont).police.lfHeight 		     := 0;
    Info_Font(nbfont).police.lfWidth           := 0;
    Info_Font(nbfont).police.lfEscapement      := 0;
    Info_Font(nbfont).police.lfOrientation     := 0;
    Info_Font(nbfont).police.lfWeight          := 0;
    Info_Font(nbfont).police.lfItalic          := 0;
    Info_Font(nbfont).police.lfUnderline       := 0;
    Info_Font(nbfont).police.lfStrikeOut       := 0;
    Info_Font(nbfont).police.lfCharSet         := 0;
    Info_Font(nbfont).police.lfOutPrecision    := 0;
    Info_Font(nbfont).police.lfClipPrecision   := 0;
    Info_Font(nbfont).police.lfQuality         := 0;
    Info_Font(nbfont).police.lfPitchAndFamily  := 0;

  end loop;
  close(ficpolice_in);

  exception when Event : others =>
  Msg_Erreur:=To_Unbounded_String(
            "Erreur de lecture:"&EOL
            &"Fichier: "&nomfichier_polices(1..ncar_pol)&eol
            &"Ligne n�"&Integer'Image(integer(no_lig)));
  close(ficpolice_in);
  -- raise;
end charge_fontes;                                     
                                     
----==============================================================================
---- Abbreviation de SAINT en ST ... --
----===================================
--Separate(INIPAN) Procedure Abrev(tbnomi:in out typtbnomi; no:in integer) is
--  begin
--    for icar in 1..tbnomi(no).ncar-5 loop
--      if    tbnomi(no).chaine(icar..icar+4)="SAINT"
--      or tbnomi(no).chaine(icar..icar+4)="saint"
--      or tbnomi(no).chaine(icar..icar+4)="Saint" then
--        for jcar in 5+icar-1..tbnomi(no).ncar loop
--          tbnomi(no).chaine(jcar-3):= tbnomi(no).chaine(jcar);
--        end loop;
--        tbnomi(no).chaine(tbnomi(no).ncar-2):= ' ';
--        tbnomi(no).chaine(tbnomi(no).ncar-1):= ' ';
--        tbnomi(no).chaine(tbnomi(no).ncar  ):= ' ';
--        tbnomi(no).ncar:= tbnomi(no).ncar-3;
--      end if;
--    end loop;
--  end abrev;

--=========================================================================
Separate(INIPAN) procedure LECTURE_CHARGT(tbnomi : in out typtbnomi;
                                          info_font : in typtfont;
                                          nbfont : in integer) is
  erreur_code : exception;
  trouve : boolean:=false;
  Nnom_pb : integer;
  nt : nom_type;
begin
  for nnom in 1..tbnomi'last-NbNorou loop
    nt:=tbnomi(nnom);
    trouve:=false;
    for i in 1..nbfont loop
      if info_font(i).code = tbnomi(nnom).topo.code then
        tbnomi(nnom).police:=i;
        tbnomi(nnom).hauteur:=info_font(i).hauteur;
        trouve:=true;
        exit;
      end if;
    end loop;    
    if trouve=false then
      Nnom_pb:=Nnom;
      raise erreur_code;
    end if;    
       --tbnomi(nnom).corps:=integer(tbnomi(nnom).hauteur*float(ptparmm));
	     --if flag_abrev=1 then
	     --  ABREV(tbnomi,nnom);
       --end if;
  end loop;
exception when others => 
  Msg_Erreur:=To_Unbounded_String("Code typo inconnu � la ligne"&integer'image(Nnom_pb)&" du fichier des �critures horizontales");
  raise;
end lecture_chargt;

--======================================================
-- Coupure des noms: - possible au dela de 9 caracteres, 
-- 		     - obligatoire au dela de 15,
-- S'il y a coupure: - pas apres un article s'il y en a,
--                   - tirets dupliques s'il y en a.
--======================================================

--Separate(INIPAN) procedure CESURE(tbnomi:in out typtbnomi; nb_nom: in integer) is
--  maxlchaine: integer:=100;
--  itab, NC: integer :=0;
--  mincar: integer:=maxlchaine;
--  --Coup_Option: integer:=9;
--  ncar: integer;
--  min: integer:=maxlchaine/2;
--  m, r: float;
--  TabSep : array (0..min) of integer;
--  TabCes : array (1..min) of integer;
--  Chaine : string(1..MaxLChaine):=(1..MaxLChaine => ' ');
--  nbSep, nbCes, Ces: integer:=0;
--  B: boolean:=False;
--  maxcar: integer:=5;
--  nb_art: integer:=69;
--  TabArt : array (1..nb_art) of string(1..maxcar):=(
--          "sous ", "lous ", "agli ", "beim ", "dels ",
--          "dets ", "les  ", "des  ", "sur  ", "une  ",
--          "aux  ", "als  ", "das  ", "dem  ", "den  ",
--          "der  ", "die  ", "ech  ", "�la  ", "els  ",
--          "era  ", "ero  ", "eul  ", "eun  ", "eur  ",
--          "gli  ", "het  ", "las  ", "los  ", "lou  ",
--          "dei  ", "del  ", "det  ", "vom  ", "zum  ",
--          "zur  ", "le   ", "la   ", "un   ", "au   ",
--          "de   ", "du   ", "et   ", "�s   ", "al   ",
--          "an   ", "ar   ", "el   ", "en   ", "er   ",
--          "l�   ", "lo   ", "ul   ", "ur   ", "ai   ",
--          "al   ", "am   ", "as   ", "em   ", "im   ",
--          "um   ", "st   ", "pont ", "mont ", "fond ",
--          "bois ", "morne", "croix", "st.  ");
--  
--begin
--    for n in 1..nb_nom loop
--      tbnomi(n).ncar1:=tbnomi(n).ncar(1);
--      tbnomi(n).ncar2:=0;
--      tbnomi(n).nl:=1;            
--      tbnomi(n).chaine1:=tbnomi(n).chaine(1);
--      ncar:=tbnomi(n).ncar(1);
--      if ncar>=Coup_Option then
--        Chaine:=(1..MaxLChaine => ' ');
--        Chaine:=tbnomi(n).chaine(1);
--        nbSep:=0;
--        nbCes:=0;
--        TabSep(0):=0;
--        B:=False;
--
--        for i in 1..ncar loop
--          if chaine(i)=' ' then B:=true; exit; end if;
--        end loop;
--        if B then
--          for i in 1..ncar loop
--            if chaine(i)=' ' then
--              nbSep:=nbSep+1;
--              TabSep(nbSep):=i;
--            end if;
--          end loop;
--        else
--          for i in 1..ncar loop
--            if chaine(i)='-' then
--              nbSep:=nbSep+1;
--              TabSep(nbSep):=i;
--            end if;
--          end loop;    
--        end if;
--
--        if nbSep=0 then goto label; end if;
--        for i in 1..nbSep loop
--          B:=True;
--          if TabSep(i)-tabSep(i-1)=2 then
--            B:=False;
--          else
--          for j in 1..nb_art loop
--            NC:=0;
--            for k in 1..maxcar loop
--              if TabArt(j)(k)/=' ' then
--                NC:=NC+1;
--              else
--                exit;
--              end if;
--            end loop;
--            if TabSep(i)-tabSep(i-1)=NC+1 then
--              if ( ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Lower(TabArt(j)(1..NC)) )
--              or   ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Upper(TabArt(j)(1))&To_Lower(TabArt(j)(2..NC)) )
--              or   ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Upper(TabArt(j)(1..NC)) ) )then
--                B:=False;
--                exit;
--              end if;
--            end if;
--          end loop;
--          end if;
--          if B then
--            nbCes:=nbCes+1;
--            TabCes(nbCes):=TabSep(i);
--          end if;
--        end loop;
--        if nbCes=0 then goto label; end if;
--        m:=float(ncar);
--        for i in 1..nbCes loop
--          r:=abs(float(TabCes(i))-float(ncar)/2.0);
--          if r< m then 
--            m:= r;
--            Ces:=TabCes(i);
--          end if;   
--        end loop;
--        if chaine(ces)=' ' then
--          tbnomi(n).ncar1:= ces-1;
--          tbnomi(n).ncar2:= ncar-ces;
--          tbnomi(n).chaine1(1..ces-1):=tbnomi(n).chaine(1)(1..ces-1);
--          tbnomi(n).chaine2(1..ncar-ces):=tbnomi(n).chaine(1)(ces+1..ncar);
--        else  --chaine(ces)='-'
--          tbnomi(n).ncar1:= ces;
--          tbnomi(n).ncar2:= ncar-ces+1;
--          tbnomi(n).chaine1(1..ces):=tbnomi(n).chaine(1)(1..ces);
--          tbnomi(n).chaine2(1..ncar-ces+1):=tbnomi(n).chaine(1)(ces..ncar);
--        end if;
--        if ncar>Coup_oblig then tbnomi(n).nl:= 2; end if;
--      end if;
--      <<label>> null;  
--    end loop;
--end CESURE;

-- debut modif 26.06.2006
Separate(INIPAN) procedure CESURE(tbnomi:in out typtbnomi; nb_nom: in integer) is
  maxlchaine: integer:=100;
  itab, NC: integer :=0;
  mincar: integer:=maxlchaine;
  --Coup_Option: integer:=9;
  ncar: integer;
  min: integer:=maxlchaine/2;
  m, r: float;
  TabSep : array (0..min) of integer;
  TabCes : array (1..min) of integer;
  Chaine : string(1..MaxLChaine):=(1..MaxLChaine => ' ');
  nbSep, nbCes, Ces: integer:=0;
  B: boolean:=False;
  maxcar: integer:=5;
  nb_art: integer:=69;
  TabArt : array (1..nb_art) of string(1..maxcar):=(
          "sous ", "lous ", "agli ", "beim ", "dels ",
          "dets ", "les  ", "des  ", "sur  ", "une  ",
          "aux  ", "als  ", "das  ", "dem  ", "den  ",
          "der  ", "die  ", "ech  ", "�la  ", "els  ",
          "era  ", "ero  ", "eul  ", "eun  ", "eur  ",
          "gli  ", "het  ", "las  ", "los  ", "lou  ",
          "dei  ", "del  ", "det  ", "vom  ", "zum  ",
          "zur  ", "le   ", "la   ", "un   ", "au   ",
          "de   ", "du   ", "et   ", "�s   ", "al   ",
          "an   ", "ar   ", "el   ", "en   ", "er   ",
          "l�   ", "lo   ", "ul   ", "ur   ", "ai   ",
          "al   ", "am   ", "as   ", "em   ", "im   ",
          "um   ", "st   ", "pont ", "mont ", "fond ",
          "bois ", "morne", "croix", "st.  ");
  Passe: integer:=1;

  
begin
    for n in 1..nb_nom loop
      tbnomi(n).ncar1:=tbnomi(n).ncar(1);
      tbnomi(n).ncar2:=0;
      tbnomi(n).nl:=1;            
      tbnomi(n).chaine1:=tbnomi(n).chaine(1);
      ncar:=tbnomi(n).ncar(1);
      if ncar>=Coup_Option then
        Chaine:=(1..MaxLChaine => ' ');
        Chaine:=tbnomi(n).chaine(1);

        for passe in 1..2 loop

        nbSep:=0;
        nbCes:=0;
        TabSep(0):=0;
        B:=False;

        for i in 1..ncar loop
          if (passe=1 and chaine(i)=' ') or (passe=2 and chaine(i)='-') then B:=true; exit; end if;
        end loop;
        if B then
          for i in 1..ncar loop
            if (passe=1 and chaine(i)=' ') or (passe=2 and chaine(i)='-') then
              nbSep:=nbSep+1;
              TabSep(nbSep):=i;
            end if;
          end loop;
        end if;

        if nbSep=0 then goto label; end if;
        for i in 1..nbSep loop
          B:=True;
          if TabSep(i)-tabSep(i-1)=2 then
            B:=False;
          else
          for j in 1..nb_art loop
            NC:=0;
            for k in 1..maxcar loop
              if TabArt(j)(k)/=' ' then
                NC:=NC+1;
              else
                exit;
              end if;
            end loop;
            if TabSep(i)-tabSep(i-1)=NC+1 then
              if ( ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Lower(TabArt(j)(1..NC)) )
              or   ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Upper(TabArt(j)(1))&To_Lower(TabArt(j)(2..NC)) )
              or   ( chaine(TabSep(i)-NC..TabSep(i)-1) = To_Upper(TabArt(j)(1..NC)) ) )then
                B:=False;
                exit;
              end if;
            end if;
          end loop;
          end if;
          if B then
            nbCes:=nbCes+1;
            TabCes(nbCes):=TabSep(i);
          end if;
        end loop;
        if nbCes=0 then goto label; end if;
        m:=float(ncar);
        for i in 1..nbCes loop
          r:=abs(float(TabCes(i))-float(ncar)/2.0);
          if r< m then 
            m:= r;
            Ces:=TabCes(i);
          end if;   
        end loop;
        if chaine(ces)=' ' then
          tbnomi(n).ncar1:= ces-1;
          tbnomi(n).ncar2:= ncar-ces;
          tbnomi(n).chaine1(1..ces-1):=tbnomi(n).chaine(1)(1..ces-1);
          tbnomi(n).chaine2(1..ncar-ces):=tbnomi(n).chaine(1)(ces+1..ncar);
        else  --chaine(ces)='-'
          tbnomi(n).ncar1:= ces;
          tbnomi(n).ncar2:= ncar-ces+1;
          tbnomi(n).chaine1(1..ces):=tbnomi(n).chaine(1)(1..ces);
          tbnomi(n).chaine2(1..ncar-ces+1):=tbnomi(n).chaine(1)(ces..ncar);
        end if;
        if ncar>Coup_oblig then tbnomi(n).nl:= 2; end if;

      <<label>> null;

      if nbCes>0 then
        exit;
      end if;

      end loop;

      end if;
    end loop;
end CESURE;
-- fin modif 26.06.2006


--Separate(INIPAN) procedure Cesure(tbnomi:in out typtbnomi;no:in integer) is
--  ncar: integer:=tbnomi(no).ncar;
--  chaine: string:=tbnomi(no).chaine;
--  min: integer:=ncar/2;
--  TabSep : array (0..min) of integer;
--  TabCes : array (1..min) of integer;
--  nbSep, nbCes, Ces: integer:=0;
--  B: boolean:=False;
--  jb: integer:=0;
--  nbTab4 : integer :=6;
--  Tab4 : array (1..nbTab4) of string(1..4):=("sous", "lous", "agli", "beim", "dels",
--           "dets");     
--  nbTab3 : integer :=30;
--  Tab3 : array (1..nbTab3) of string(1..3):=("les", "des", "sur", "une", "aux",
--          "als", "das", "dem", "den", "der", "die", "ech", "�la", "els", "era",
--          "ero", "eul", "eun", "eur", "gli", "het", "las", "los", "lou", "dei",
--          "del", "det", "vom", "zum", "zur");
--  nbTab2 : integer :=25;
--  Tab2 : array (1..nbTab2) of string(1..2):=("le", "la", "un", "au", "de", "du",
--                     "et", "�s", "al", "an", "ar", "el", "en", "er", "l�", "lo",
--                     "ul", "ur", "ai", "al", "am", "as", "em", "im", "um");
----  nbTab1 : integer :=2;
----  Tab1 : array (1..nbTab1) of character:=('�', 'a', 'd', 'e', 'i', 'u');
--                                             
--begin
----  tbnomi(no).ncar1:=ncar;
----  tbnomi(no).ncar2:=0;
----  tbnomi(no).nl:=1;            
----  tbnomi(no).chaine1:=tbnomi(no).chaine;
--  TabSep(0):=0;
--  B:=False;
--  for i in 1..ncar loop
--    if chaine(i)=' ' then B:=true; exit; end if;
--  end loop;
--  if B then
--    for i in 1..ncar loop
--      if chaine(i)=' ' then
--        nbSep:=nbSep+1;
--        TabSep(nbSep):=i;
--      end if;
--    end loop;
--  else
--    for i in 1..ncar loop
--      if chaine(i)='-' then
--        nbSep:=nbSep+1;
--        TabSep(nbSep):=i;
--      end if;
--    end loop;    
--  end if;
--  if nbSep=0 then return; end if;
--  for i in 1..nbSep loop
--    case TabSep(i)-tabSep(i-1) is
--      when 5 =>
--        for j in 1..nbTab4 loop
--          if ( ( chaine(TabSep(i)-4..TabSep(i)-1) = To_Lower(Tab4(j)) )
--          or   ( chaine(TabSep(i)-4..TabSep(i)-1) = To_Upper(Tab4(j)(1))&To_Lower(Tab4(j)(2..4)) )
--          or   ( chaine(TabSep(i)-4..TabSep(i)-1) = To_Upper(Tab4(j)) ) )then
--            B:=False;
--            exit;
--          end if;
--          B:=True;
--        end loop;
--      when 4 =>
--        for j in 1..nbTab3 loop
--          if ( ( chaine(TabSep(i)-3..TabSep(i)-1) = To_Lower(Tab3(j)) )
--          or   ( chaine(TabSep(i)-3..TabSep(i)-1) = To_Upper(Tab3(j)(1))&To_Lower(Tab3(j)(2..3)) )
--          or   ( chaine(TabSep(i)-3..TabSep(i)-1) = To_Upper(Tab3(j)) ) )then
--            B:=False;
--            exit;
--          end if;
--          B:=True;
--        end loop;
--      when 3 =>
--        for j in 1..nbTab2 loop
--          if ( ( chaine(TabSep(i)-2..TabSep(i)-1) = To_Lower(Tab2(j)) )
--          or   ( chaine(TabSep(i)-2..TabSep(i)-1) = To_Upper(Tab2(j)(1))&To_Lower(Tab2(j)(2)) )
--          or   ( chaine(TabSep(i)-2..TabSep(i)-1) = To_Upper(Tab2(j)) ) )then
--            B:=False;
--            exit;
--          end if;
--          B:=True;
--        end loop;
--      when 2 => B:=False;
----        for j in 1..nbTab1 loop
----          if ( ( chaine(TabSep(i)-1)=To_Lower(Tab1(j)) )
----          or   ( chaine(TabSep(i)-1)=To_Upper(Tab1(j)) ) )then
----            B:=False;
----            exit;
----          end if;
----          B:=True;
----        end loop;
--      when others => B:=True;
--    end case;
--    if B then
--      nbCes:=nbCes+1;
--      TabCes(nbCes):=TabSep(i);
--    end if;
--  end loop;
--  if nbCes=0 then return; end if;
--  for i in 1..nbCes loop
--    if abs(TabCes(i)-ncar/2)< Min then 
--      Min:= abs(TabCes(i)-ncar/2);
--      Ces:=TabCes(i);
--    end if;   
--  end loop;
--  if chaine(ces)=' ' then
--    tbnomi(no).ncar1:= ces-1;
--    tbnomi(no).ncar2:= ncar-ces;
--    tbnomi(no).chaine1(1..ces-1):=tbnomi(no).chaine(1..ces-1);
--    tbnomi(no).chaine2(1..ncar-ces):=tbnomi(no).chaine(ces+1..ncar);
--  else  --chaine(ces)='-'
--    tbnomi(no).ncar1:= ces;
--    tbnomi(no).ncar2:= ncar-ces+1;
--    tbnomi(no).chaine1(1..ces):=tbnomi(no).chaine(1..ces);
--    tbnomi(no).chaine2(1..ncar-ces+1):=tbnomi(no).chaine(ces..ncar);
--  end if;
--  if tbnomi(no).ncar>Coup_oblig then tbnomi(no).nl:= 2; end if;
--end CESURE;

--Separate(INIPAN) procedure Cesure(tbnomi:in out typtbnomi;no:in integer) is
--  chaine: string100:=tbnomi(no).chaine;
--  ncar: integer:=tbnomi(no).ncar;
--  min: integer:=ncar/2;
--  TabSep : array (0..min) of integer;
--  TabCes : array (1..min) of integer;
--  nbSep, nbCes, Ces: integer:=0;
--  B: boolean:=False;
--  jb: integer:=0;
--begin
--  tbnomi(no).ncar1:=tbnomi(no).ncar;
--  tbnomi(no).ncar2:=0;
--  tbnomi(no).nl:=1;            
--  tbnomi(no).chaine1:=tbnomi(no).chaine;
--  tbnomi(no).chaine2(1..tbnomi(no).ncar2):="";
--  if tbnomi(no).ncar>Coup_Option then
--    TabSep(0):=0;
--    for i in 1..ncar loop
--      if chaine(i)=' ' or chaine(i)='-' then
--        nbSep:=nbSep+1;
--        TabSep(nbSep):=i;
--      end if;
--    end loop;
--    if nbSep=0 then return; end if;
--    for i in 1..nbSep loop
--    case TabSep(i)-tabSep(i-1) is
--      when 4 =>
--        if ( chaine(TabSep(i)-3..TabSep(i)-1)="les"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="Les"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="LES"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="des"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="Des"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="DES"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="sur"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="Sur"
--          or chaine(TabSep(i)-3..TabSep(i)-1)="SUR" )then
--        B:=False; else B:=true; end if;
--      when 3 =>
--        if ( chaine(TabSep(i)-2..TabSep(i)-1)="le"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="Le"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="LE"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="la"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="La"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="LA"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="de"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="De"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="DE"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="du"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="Du"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="DU"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="un"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="Un"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="UN"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="une"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="Une"
--          or chaine(TabSep(i)-2..TabSep(i)-1)="UNE" )then
--        B:=False; else B:=true; end if;
--      when 2 =>
--        case chaine(TabSep(i)-1) is
--          when '�'|'a'|'A' =>
--            B:=False;
--          when others => B:=true;
--        end case;
--      when others => B:=True;
--    end case;
--    if B then
--      nbCes:=nbCes+1;
--      TabCes(nbCes):=TabSep(i);
--    end if;
--    end loop;
--    if nbCes=0 then return; end if;
--    for i in 1..nbCes loop
--      if abs(TabCes(i)-ncar/2)< Min then 
--        Min:= abs(TabCes(i)-ncar/2);
--        Ces:=TabCes(i);
--      end if;   
--    end loop;
--    if chaine(ces)=' ' then
--      tbnomi(no).ncar1:= ces-1;
--      tbnomi(no).ncar2:= ncar-ces;
--      tbnomi(no).chaine1(1..ces-1):=tbnomi(no).chaine(1..ces-1);
--      tbnomi(no).chaine2(1..ncar-ces):=tbnomi(no).chaine(ces+1..ncar);
--    else  --chaine(ces)='-'
--      tbnomi(no).ncar1:= ces;
--      tbnomi(no).ncar2:= ncar-ces+1;
--      tbnomi(no).chaine1(1..ces):=tbnomi(no).chaine(1..ces);
--      tbnomi(no).chaine2(1..ncar-ces+1):=tbnomi(no).chaine(ces..ncar);
--    end if;
--    if tbnomi(no).ncar>Coup_oblig then tbnomi(no).nl:= 2; end if;
--  end if;
--end Cesure;

--==============================================================================
-- Coupure des noms: - possible au dela de Coup_Option caracteres, 
-- 		     - obligatoire au dela de Coup_oblig,
-- S'il y a coupure: - pas apres un article s'il y en a,
--                   - tirets dupliques s'il y en a.
--======================================================
--Separate(INIPAN) procedure Cesure_million(tbnomi:in out typtbnomi;no:in integer) is
--  jb: integer:=0;
--  begin
--    tbnomi(no).ncar1:=tbnomi(no).ncar;
--    tbnomi(no).ncar2:=0;
--    tbnomi(no).nl:=1;            
--    tbnomi(no).chaine1:=tbnomi(no).chaine;
--    tbnomi(no).chaine2(1..tbnomi(no).ncar2):="";
--    -- si le toponyme comporte une altitude ...
--    for icar in 1..tbnomi(no).ncar1 loop
--        if tbnomi(no).chaine(icar)=' '
--          and tbnomi(no).chaine1(icar+1) in '0'..'9' then 
--            tbnomi(no).ncar1:=icar-1;
--            tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..icar-1);
--            tbnomi(no).ncar2:=tbnomi(no).ncar-icar;
--            tbnomi(no).nl:= 1;
--            tbnomi(no).chaine2(1..tbnomi(no).ncar2):=tbnomi(no).chaine(icar+1..tbnomi(no).ncar); 
--            return;     
--        end if;  
--    end loop;            
--    
--    if tbnomi(no).ncar>Coup_Option then
--      for icar in 8..tbnomi(no).ncar loop  --!! ne peut etre inferieur a 5!!
--        if tbnomi(no).chaine(icar)='-' Or tbnomi(no).chaine(icar)=' ' then
--          jb:=0;
--          if  (tbnomi(no).chaine(icar-2..icar-1)="le"
--            or tbnomi(no).chaine(icar-2..icar-1)="la"
--            or tbnomi(no).chaine(icar-2..icar-1)="LE"
--            or tbnomi(no).chaine(icar-2..icar-1)="LA"
--            or tbnomi(no).chaine(icar-2..icar-1)="de"
--            or tbnomi(no).chaine(icar-2..icar-1)="DE"
--            or tbnomi(no).chaine(icar-2..icar-1)="du"
--            or tbnomi(no).chaine(icar-2..icar-1)="DU")
--            and ((tbnomi(no).chaine(icar-3)=' ')
--            or (tbnomi(no).chaine(icar-3)='-')) then
--              jb:=icar-3;
--          elsif (tbnomi(no).chaine(icar-3..icar-1)="les"
--            or tbnomi(no).chaine(icar-3..icar-1)="LES"
--            or tbnomi(no).chaine(icar-3..icar-1)="des"
--            or tbnomi(no).chaine(icar-3..icar-1)="DES"
--            or tbnomi(no).chaine(icar-3..icar-1)="Sur"
--            or tbnomi(no).chaine(icar-3..icar-1)="sur"
--            or tbnomi(no).chaine(icar-3..icar-1)="SUR")
--            and ((tbnomi(no).chaine(icar-4)=' ')
--            or (tbnomi(no).chaine(icar-4)='-')) then
--              jb:=icar-4;
--          end if;
--          if jb/=0 then -- Article dans le nom (pas en debut de ligne)
--            if tbnomi(no).chaine(jb)=' '  then
--              tbnomi(no).ncar1:= jb-1;
--              tbnomi(no).ncar2:= tbnomi(no).ncar-jb;
--              tbnomi(no).nl:=1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb-1);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                         tbnomi(no).chaine(jb+1..tbnomi(no).ncar); 
--            elsif tbnomi(no).chaine(jb)='-' then
--              --on rajoute un tiret sur la deuxieme ligne
--              tbnomi(no).ncar1:=jb;
--              tbnomi(no).ncar2:=tbnomi(no).ncar-jb+1;
--              tbnomi(no).nl:=1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                            tbnomi(no).chaine(jb..tbnomi(no).ncar);
--            end if;
--            exit;
--          elsif jb=0 then -- Pas d'article dans le nom ou avant icar.
--            if tbnomi(no).chaine(icar)='-' then
--                tbnomi(no).ncar1:= icar;
--                tbnomi(no).ncar2:= tbnomi(no).ncar-icar+1;
--                tbnomi(no).nl:= 1;
--                tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..icar);
--                tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                           tbnomi(no).chaine(icar..tbnomi(no).ncar);
--                exit;
--            end if;
--            if tbnomi(no).chaine(icar)=' ' then
--                tbnomi(no).ncar1:= icar-1;
--                tbnomi(no).ncar2:= tbnomi(no).ncar-icar;
--                tbnomi(no).nl:= 1;
--                tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..icar-1);
--                tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                           tbnomi(no).chaine(icar+1..tbnomi(no).ncar);
--                exit;
--            end if;
--          end if;
--        end if;
--      end loop;
--    end if;
--
--    if tbnomi(no).ncar>Coup_oblig and tbnomi(no).ncar2/=0 then
--      tbnomi(no).nl:= 2;
--    end if;
--  end Cesure_million;
--  
------------------------------------------------------------------------------------  
--  
--Separate(INIPAN) procedure Cesure_100000(tbnomi:in out typtbnomi;no:in integer) is
--  jb: integer:=0;
--  begin
--    tbnomi(no).ncar1:=tbnomi(no).ncar;
--    tbnomi(no).ncar2:=0;
--    tbnomi(no).nl:=1;            
--    tbnomi(no).chaine1:=tbnomi(no).chaine;
--    tbnomi(no).chaine2(1..tbnomi(no).ncar2):="";
--    
--    if tbnomi(no).ncar>Coup_Option then
--      for icar in 8..tbnomi(no).ncar loop  --!! ne peut etre inferieur a 5!!
--        if tbnomi(no).chaine(icar)='-' or tbnomi(no).chaine(icar)=' ' then
--          jb:=0;
--          if (tbnomi(no).chaine(icar-2..icar-1)="le"
--          or tbnomi(no).chaine(icar-2..icar-1)="la"
--          or tbnomi(no).chaine(icar-2..icar-1)="LE"
--          or tbnomi(no).chaine(icar-2..icar-1)="LA"
--          or tbnomi(no).chaine(icar-2..icar-1)="de"
--          or tbnomi(no).chaine(icar-2..icar-1)="DE"
--          or tbnomi(no).chaine(icar-2..icar-1)="du"
--          or tbnomi(no).chaine(icar-2..icar-1)="DU")
--          and ((tbnomi(no).chaine(icar-3)=' ')
--          or (tbnomi(no).chaine(icar-3)='-')) then
--            jb:=icar-3;
--          elsif (tbnomi(no).chaine(icar-3..icar-1)="les"
--          or tbnomi(no).chaine(icar-3..icar-1)="LES"
--          or tbnomi(no).chaine(icar-3..icar-1)="des"
--          or tbnomi(no).chaine(icar-3..icar-1)="DES"
--          or tbnomi(no).chaine(icar-3..icar-1)="Sur"
--          or tbnomi(no).chaine(icar-3..icar-1)="sur"
--          or tbnomi(no).chaine(icar-3..icar-1)="SUR")
--          and ((tbnomi(no).chaine(icar-4)=' ')
--          or (tbnomi(no).chaine(icar-4)='-')) then
--            jb:=icar-4;
--          end if;
--          if jb/=0 then -- Article dans le nom (pas en debut de ligne)
--            if tbnomi(no).chaine(jb)=' '  then
--              tbnomi(no).ncar1:= jb-1;
--              tbnomi(no).ncar2:= tbnomi(no).ncar-jb;
--              tbnomi(no).nl:=1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb-1);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                         tbnomi(no).chaine(jb+1..tbnomi(no).ncar); 
--            elsif tbnomi(no).chaine(jb)='-' then
--              --on rajoute un tiret sur la deuxieme ligne
--              tbnomi(no).ncar1:=jb;
--              tbnomi(no).ncar2:=tbnomi(no).ncar-jb+1;
--              tbnomi(no).nl:=1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                         tbnomi(no).chaine(jb..tbnomi(no).ncar);
--            end if;
--            exit;
--          elsif jb=0 then -- Pas d'article dans le nom ou avant icar.
--            if tbnomi(no).chaine(icar)='-' then
--              tbnomi(no).ncar1:= icar;
--              tbnomi(no).ncar2:= tbnomi(no).ncar-icar+1;
--              tbnomi(no).nl:= 1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..icar);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                         tbnomi(no).chaine(icar..tbnomi(no).ncar);
--              exit;
--            end if;
--            if tbnomi(no).chaine(icar)=' ' then
--              tbnomi(no).ncar1:= icar-1;
--              tbnomi(no).ncar2:= tbnomi(no).ncar-icar;
--              tbnomi(no).nl:= 1;
--              tbnomi(no).chaine1(1..tbnomi(no).ncar1):= tbnomi(no).chaine(1..icar-1);
--              tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--                                          tbnomi(no).chaine(icar+1..tbnomi(no).ncar);
--              exit;
--            end if;
--          end if;
--        end if;
--      end loop;
--    end if;
--
--    if tbnomi(no).ncar>Coup_oblig and tbnomi(no).ncar2/=0 then
--      tbnomi(no).nl:= 2;
--    end if;
--  end Cesure_100000;
--                
----======================================================
---- Coupure des noms: - possible au dela de 9 caracteres, 
---- 		     - obligatoire au dela de 15,
---- S'il y a coupure: - pas apres un article s'il y en a,
----                   - tirets dupliques s'il y en a.
----======================================================
--Separate(INIPAN) procedure Cesure_25000(tbnomi:in out typtbnomi;no:in integer) is
--  jb: integer:=0;
--begin
--  tbnomi(no).ncar1:=tbnomi(no).ncar;
--  tbnomi(no).ncar2:=0;
--  tbnomi(no).nl:=1;            
--  tbnomi(no).chaine1:=tbnomi(no).chaine;
--  if tbnomi(no).ncar>Coup_Option then
--    for icar in 9..tbnomi(no).ncar loop
--      if tbnomi(no).chaine(icar)='-' or tbnomi(no).chaine(icar)=' ' then
--        jb:=0;
--        if (tbnomi(no).chaine(icar-2..icar-1)="le"
--        or tbnomi(no).chaine(icar-2..icar-1)="la"
--        or tbnomi(no).chaine(icar-2..icar-1)="LE"
--        or tbnomi(no).chaine(icar-2..icar-1)="LA"
--        or tbnomi(no).chaine(icar-2..icar-1)="de"
--        or tbnomi(no).chaine(icar-2..icar-1)="DE"
--        or tbnomi(no).chaine(icar-2..icar-1)="du"
--        or tbnomi(no).chaine(icar-2..icar-1)="DU")
--        and tbnomi(no).chaine(icar-3)=' ' then
--          if tbnomi(no).chaine(icar-6..icar-4)=" de"
--          or tbnomi(no).chaine(icar-6..icar-4)=" DE" then
--            goto next_icar;
--          else
--            jb:=icar-3;
--          end if;
--        elsif (tbnomi(no).chaine(icar-3..icar-1)="les"
--        or tbnomi(no).chaine(icar-3..icar-1)="LES"
--        or tbnomi(no).chaine(icar-3..icar-1)="des"
--        or tbnomi(no).chaine(icar-3..icar-1)="DES"
--        or tbnomi(no).chaine(icar-3..icar-1)="Sur"
--        or tbnomi(no).chaine(icar-3..icar-1)="sur"
--        or tbnomi(no).chaine(icar-3..icar-1)="SUR")
--        and tbnomi(no).chaine(icar-4)=' ' then
--          jb:=icar-4;
--        end if;
--        if jb/=0 then -- Article dans le nom (pas en debut de ligne)
--          if tbnomi(no).chaine(jb)=' ' then
--            tbnomi(no).ncar1:= jb-1;
--            tbnomi(no).ncar2:= tbnomi(no).ncar-jb;
--            tbnomi(no).nl:= 1;
--		    		tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb-1);
--            tbnomi(no).chaine2(1..tbnomi(no).ncar2):=tbnomi(no).chaine(jb+1..tbnomi(no).ncar);
--          elsif tbnomi(no).chaine(jb)='-' then
--            tbnomi(no).ncar1:=jb;
--            tbnomi(no).ncar2:=tbnomi(no).ncar-jb+2;
--            tbnomi(no).nl:=1;
--		    		tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..jb);
--            tbnomi(no).chaine2(1..tbnomi(no).ncar2):=tbnomi(no).chaine(jb-1..tbnomi(no).ncar);
--          end if;
--          exit;
--        elsif jb=0 then -- Pas d'article dans le nom.
--          tbnomi(no).ncar1:= icar-1;
--          tbnomi(no).ncar2:= tbnomi(no).ncar-icar;
--          tbnomi(no).nl:= 1;
--          tbnomi(no).chaine1(1..tbnomi(no).ncar1):=tbnomi(no).chaine(1..icar-1);
--          tbnomi(no).chaine2(1..tbnomi(no).ncar2):=tbnomi(no).chaine(icar+1..tbnomi(no).ncar);
--          exit;
--        end if;
--      end if;
--      <<next_icar>> null;
--    end loop;
--  end if;
--                
--  if tbnomi(no).ncar>Coup_oblig and tbnomi(no).ncar2/=0 then
--    tbnomi(no).nl:= 2;
--  end if;
--end Cesure_25000;


----==============================================================================
---- Coupure des noms: - possible au dela de 9 caracteres, 
---- 		     - obligatoire au dela de 15,
---- S'il y a coupure: - pas apres un article s'il y en a,
----                   - tirets dupliques s'il y en a.
----======================================================
--Separate(INIPAN) procedure Cesure_33million(tbnomi:in out typtbnomi;no:in integer) is
--  jb: integer:=0;
--  begin
--    tbnomi(no).ncar1:=tbnomi(no).ncar;
--    tbnomi(no).ncar2:=0;
--    tbnomi(no).nl:=1;            
--    tbnomi(no).chaine1:=tbnomi(no).chaine;
--    if tbnomi(no).ncar>Coup_Option then
--        for icar in 8..tbnomi(no).ncar loop 
--	   if tbnomi(no).chaine(icar)='-' Or tbnomi(no).chaine(icar)=' ' then
--		jb:=0;
--	   	if  ( tbnomi(no).chaine(icar-2..icar-1)="le"
--                   or tbnomi(no).chaine(icar-2..icar-1)="la"
--                   or tbnomi(no).chaine(icar-2..icar-1)="LE"
--                   or tbnomi(no).chaine(icar-2..icar-1)="LA"
--                   or tbnomi(no).chaine(icar-2..icar-1)="de"
--                   or tbnomi(no).chaine(icar-2..icar-1)="DE"
--                   or tbnomi(no).chaine(icar-2..icar-1)="du"
--                   or tbnomi(no).chaine(icar-2..icar-1)="DU")
--		   and ((tbnomi(no).chaine(icar-3)=' ')
--		     	or (tbnomi(no).chaine(icar-3)='-')) then
--			jb:=icar-3;
--		elsif (tbnomi(no).chaine(icar-3..icar-1)="les"
--                    or tbnomi(no).chaine(icar-3..icar-1)="LES"
--                    or tbnomi(no).chaine(icar-3..icar-1)="des"
--                    or tbnomi(no).chaine(icar-3..icar-1)="DES"   
--		    		or tbnomi(no).chaine(icar-3..icar-1)="Sur"
--		    		or tbnomi(no).chaine(icar-3..icar-1)="sur"
--                    or tbnomi(no).chaine(icar-3..icar-1)="SUR")
--		    and ((tbnomi(no).chaine(icar-4)=' ')
--			or (tbnomi(no).chaine(icar-4)='-')) then
--                	jb:=icar-4;
--		end if;
--		if jb/=0 then -- Article dans le nom (pas en debut de ligne)
--	   	  if tbnomi(no).chaine(jb)=' ' then
--                    tbnomi(no).ncar1:= jb-1;
--                    tbnomi(no).ncar2:= tbnomi(no).ncar-jb; 
--                    tbnomi(no).nl:= 1;
--		    tbnomi(no).chaine1(1..tbnomi(no).ncar1):=
--				tbnomi(no).chaine(1..jb-1); 
--                    tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--				tbnomi(no).chaine(jb+1..tbnomi(no).ncar); 
--                  elsif tbnomi(no).chaine(jb)='-' then
--                    tbnomi(no).ncar1:=jb;
--                    tbnomi(no).ncar2:=tbnomi(no).ncar-jb+1; 
--                    tbnomi(no).nl:=1;
--		    tbnomi(no).chaine1(1..tbnomi(no).ncar1):=
--				tbnomi(no).chaine(1..jb); 
--                    tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--				tbnomi(no).chaine(jb..tbnomi(no).ncar); 
--		  end if;
--		  exit;
--                elsif jb=0 then -- Pas d'article dans le nom ou avant icar.
--		  if tbnomi(no).chaine(icar)='-' then            
--		    tbnomi(no).ncar1:= icar;
--                    tbnomi(no).ncar2:= tbnomi(no).ncar-icar; 
--                    tbnomi(no).nl:= 1;
--		    tbnomi(no).chaine1(1..tbnomi(no).ncar1):=
--				tbnomi(no).chaine(1..icar); 
--                    tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--				tbnomi(no).chaine(icar+1..tbnomi(no).ncar); 
--		    exit;
-- 		  end if;
--		  if tbnomi(no).chaine(icar)=' ' then            
--		    tbnomi(no).ncar1:= icar-1;
--                    tbnomi(no).ncar2:= tbnomi(no).ncar-icar; 
--                    tbnomi(no).nl:= 1;
--		    tbnomi(no).chaine1(1..tbnomi(no).ncar1):=
--				tbnomi(no).chaine(1..icar-1); 
--                    tbnomi(no).chaine2(1..tbnomi(no).ncar2):=
--				tbnomi(no).chaine(icar+1..tbnomi(no).ncar); 
--		    exit;
--		  end if;
--		end if;
--           end if;
--        end loop;          
--    end if;
--    
--    if tbnomi(no).ncar>Coup_oblig and tbnomi(no).ncar2/=0 then
--	tbnomi(no).nl:= 2;                   
--    end if;
--end Cesure_33million;
        
--====================================================================
-- Procedure qui determine l'emprise des noms a placer en fonction des 
-- polices  utilisees
-- pour chaque toponyme, on charge la police associee  
--=====================================================================
Separate(INIPAN) Procedure emprise(tbnomi:in out typtbnomi;
                                   i: in integer;
                                   info_font: in typtfont;
                                   hdc: Win32.windef.hdc) is

use type win32.int;
use type win32.long;
                                   
taille: aliased win32.windef.size; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
taille_m: float;
ch: string(1..20);
n: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
--H_400: win32.long;
H_400: integer;
T: float;
AOC: Win32.PSTR; 

                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
  Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------
                              
begin

 -- creer le police logique
    Log_Font.lfFaceName:=info_font(tbnomi(i).police).police.lfFaceName;
    -- on charge systematiquement une police de corps 400
    Log_Font.lfHeight 		  :=win32.long(-400);
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
      
    Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police    
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
    if (fhandle = Null) then  put_line("erreur de chargement de police");
       get_line(ch, n); -- se produit quand erreur dans hdc
    end if;
    
    -- caracteristiques de la police
    res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
    
    -- Corps en points=lptm.tmheight-lptm.tmInternalleading
    -- Hauteur des majuscules=lptm.tmheight-lptm.tmdescent-lptm.tmInternalleading
    -- Hauteur des majuscules=Corps-lptm.tmdescent
    H_400:= Integer(400-lptm.tmdescent);
    T:=float(info_font(tbnomi(i).police).hauteur)
       *float(25.4/(72.0*400.0))*float(ptparmm);
       
    -- calculer l'extension
    res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1))),
                              win32.int(tbnomi(i).ncar(1)),taille'unchecked_Access);
    tbnomi(i).corps:=integer(T*float(H_400));
    tbnomi(i).ln(1) :=integer(T*float(taille.cx))
                     +integer(float((tbnomi(i).ncar(1)-1))*info_font(tbnomi(i).police).InterLettres*float(tbnomi(i).corps)/float(info_font(tbnomi(i).police).hauteur));

    res:=win32.wingdi.getTextExtentPoint32(Hdc,
                             String_to_PCCH(tbnomi(i).chaine1(1..tbnomi(i).ncar1)),
                             win32.int(tbnomi(i).ncar1), taille'unchecked_Access);
    tbnomi(i).ln1:=integer(T*float(taille.cx))
                  +integer(float((tbnomi(i).ncar1-1))*info_font(tbnomi(i).police).InterLettres*float(tbnomi(i).corps)/float(info_font(tbnomi(i).police).hauteur));
    
   if tbnomi(i).ncar2/=0 then
       res:=win32.wingdi.getTextExtentPoint32(Hdc,
                             String_to_PCCH(tbnomi(i).chaine2(1..tbnomi(i).ncar2)),
                             win32.int(tbnomi(i).ncar2),taille'unchecked_Access); 
       tbnomi(i).ln2:=integer(T*float(taille.cx))
                     +integer(float((tbnomi(i).ncar2-1))*info_font(tbnomi(i).police).InterLettres*float(tbnomi(i).corps)/float(info_font(tbnomi(i).police).hauteur));

   else
      tbnomi(i).ln2:=0;
   end if; 
     --decharger la police
     res:=win32.wingdi.deleteobject(Font_Handle);
   
 end EMPRISE;

 
Separate(INIPAN) procedure det_zi(tbnomi:in typtbnomi;
                                  i     :in integer;
                                  tzi   :in out typtbe;
                               	  lig   :in integer;
		                          vesp  :in out integer) is

  bno:boite_type; 
  affich:affichage_type;
  noeud: noeud_type;
  rayon: float; -- en mm !!!
  hno:integer:=tbnomi(i).corps;
  lno:integer:=tbnomi(i).ln(1);
  hd:integer:=integer(dilat*float(hno));
  binfx,binfy,bsupx,bsupy: integer;
  --*--
  larg1:integer:=tbnomi(i).empblastop(lig).maximum.coor_x - tbnomi(i).empblastop(lig).minimum.coor_x;
  larg2:integer:=tbnomi(i).empblasdown(lig).maximum.coor_x - tbnomi(i).empblasdown(lig).minimum.coor_x;
  haut1:integer:=tbnomi(i).empblastop(lig).maximum.coor_y - tbnomi(i).empblastop(lig).minimum.coor_y;
  haut2:integer:=tbnomi(i).empblasdown(lig).maximum.coor_y - tbnomi(i).empblasdown(lig).minimum.coor_y;
  largmax:integer;
  --**--
    
begin
  if tbnomi(i).topo.objet_type=zonal then
    bno:=GR_ENCOMBREMENT_FACE(gros,tbnomi(i).topo.num_objet);
  elsif tbnomi(i).topo.objet_type=ponctuel then
    noeud:=GR_NOEUD(gros,tbnomi(i).topo.num_objet);
    affich:=GR_LEGENDE_P(legos,noeud.att_graph);
    rayon:=(affich.largeur/2.0)*float(ptparmm);
  	bno:=( (tbnomi(i).coord_objet.coor_x-integer(rayon), 
	          tbnomi(i).coord_objet.coor_y-integer(rayon)), 
	         (tbnomi(i).coord_objet.coor_x+integer(rayon), 
		        tbnomi(i).coord_objet.coor_y+integer(rayon)) );
  end if;

  if tbnomi(i).npic>0 then 
	  if larg1>=larg2 then
		  largmax:=larg1;
	  else
		  largmax:=larg2;
	  end if;
	  if largmax>tbnomi(i).ln(1) then
		binfx:=max(1,bno.minimum.coor_x-lno-2*hd-hno*3/8-integer(2.0*decalno*float(hno))-largmax);
		bsupx:=min(infoos.delta_x,bno.maximum.coor_x+lno+2*hd+hno*3/8+integer(2.0*decalno*float(hno))+largmax);
	  else
		binfx:=max(1,bno.minimum.coor_x-lno-2*hd-hno*3/8-integer(2.0*decalno*float(hno)));
		bsupx:=min(infoos.delta_x,bno.maximum.coor_x+lno+2*hd+hno*3/8+integer(2.0*decalno*float(hno)));
	  end if;
	  binfy:=max(1,bno.minimum.coor_y-2*(hno+2*hd)-hno*3/8-integer(2.0*decalno*float(hno))	
		-integer(espace*float(hno))-2*(haut1+haut2)-2*vesp); -- anciennement 1*(Haut1+Haut2)
      bsupy:=min(infoos.delta_y,bno.maximum.coor_y+2*(hno+2*hd)+hno*3/8+integer(2.0*decalno*float(hno))
		+integer(espace*float(hno))+2*(haut1+haut2)+2*vesp); -- anciennement 1*(Haut1+Haut2)
    else

    binfx:=max(1,bno.minimum.coor_x-lno-2*hd-hno*3/8-integer(2.0*decalno*float(hno)));
    binfy:=max(1,bno.minimum.coor_y-2*(hno+2*hd)-hno*3/8 
              -integer(2.0*decalno*float(hno))-integer(espace*float(hno))); 
    bsupx:=min(infoos.delta_x,bno.maximum.coor_x+lno+2*hd+hno*3/8+integer(2.0*decalno*float(hno)));
    bsupy:=min(infoos.delta_y,bno.maximum.coor_y+2*(hno+2*hd)+hno*3/8
		          +integer(2.0*decalno*float(hno))+integer(espace*float(hno))); 

  end if;

  tzi(i):=((binfx,binfy),(bsupx,bsupy));
  tzi(i):=Manipboite.dilatee(tzi(i),hd,hd);
end det_zi;
