--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with win32;
with Win32.CommDlg;
with win32.crt.strings;
with win32.winuser; use win32.winuser;
with System;
with Interfaces.C; use Interfaces.C;
with gb; use gb;   
with dialog1, dialog2,  dialog3, dialog4,  window1;
use dialog1, dialog2, dialog3, dialog4,  window1;
with math_int_basic; use math_int_basic;
with manipboite; use manipboite;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Float_Text_IO; use Ada.Float_Text_IO;
with Ada.Characters.Handling; use Ada.Characters.Handling;
with pab; use pab;
with lecture; use Lecture;
with norou; use norou;


package BODY INIPAN is
  
use type system.address;
use type win32.long;  
use type win32.int;


-- *****************************************************
-- *** Procedures d�finies dans PUBLICWINPAT.ada *******
-- *****************************************************

-- Charger les param�tres � partir des fichiers
------------------------------------------------ 
procedure LoadCalcul(FileName : in String) is Separate; 
procedure LoadRecuit(FileName : in String) is Separate;  

-- Remplir les champs des BDs des param�tres � partir des valeurs des param�tres 
-------------------------------------------------------------------------------
procedure SetDonnees is Separate;
procedure SetCalcul is Separate;
procedure SetRecuit is Separate;

-- Ecrire les fichiers des param�tres
--------------------------------------
procedure WriteCalcul(Filename : in String) is Separate;
procedure WriteRecuit(Filename : in String) is Separate;  

-- Initialiser les param�tres � partir des champs des BDs
--------------------------------------------------------
procedure InitDonnees is Separate;
--procedure InitCalcul;
--procedure InitRecuit;  
       
-- V�rifier la validit� des champs de param�tres, d'emprise, de r�solution
------------------------------------------------------------- 
function DonneesIsValid return Boolean is Separate;
function DonneesIsValid_Mes return Boolean is Separate;
function CalculIsValid return Boolean is Separate;
function RecuitIsValid return Boolean is Separate;  
function EmpriseIsValid return Boolean is Separate;  
function ResolutionIsValid return Boolean  is Separate;
 
--  Remise � z�ro 
---------------------------
procedure Raz_calcul is Separate;
procedure Raz_recuit is Separate;
procedure Raz is Separate;

-- Affichage de la fenetre Parametres de Calcul selon les options
-----------------------------------------------------------------
procedure ShowHideCalcul is Separate;

-- Rafra�chit l'affichage de contr�le de *.PAT    
----------------------------------------------
procedure RefreshPat is Separate;             

-- R�cup�re nom complet d'un fichier  
------------------------------------
procedure Search_Box(Filename : out String; 
                     Title : in String; 
                     WType : in Integer;
                     Owner : Win32.Windef.HWND; 
                     Response : out Win32.Bool) is Separate;
                      

-- *******************************************************
-- *** Procedures d�finies dans INIPANB_CHARGT.ada *******
-- *******************************************************
                     
--==========================================================================
procedure charge_fontes(info_font:out typtfont;
                        nbfont: out integer) is separate;
                         
----=========================================================================
--Procedure Abrev(tbnomi:in out typtbnomi; no:in integer) is separate;

--=========================================================================
procedure LECTURE_CHARGT(tbnomi:in out typtbnomi;
                         info_font:in typtfont;
                         nbfont : in integer) is separate;
              
--===========================================================================
procedure Cesure(tbnomi:in out typtbnomi;nb_nom:in integer) is separate;
--procedure Cesure_Million(tbnomi:in out typtbnomi;no:in integer) is separate;
--procedure Cesure_100000(tbnomi:in out typtbnomi;no:in integer) is separate;
--procedure Cesure_25000  (tbnomi:in out typtbnomi;no:in integer) is separate;
--procedure Cesure_33Million(tbnomi:in out typtbnomi;no:in integer) is separate;

--===========================================================================
procedure emprise(tbnomi:in out typtbnomi;
                  i: in integer;
                  info_font: in typtfont;
                  hdc: Win32.windef.hdc) is separate;
                  
--===========================================================================
procedure det_zi(tbnomi:in typtbnomi;
                 i     :in integer;
                 tzi   :in out typtbe;
                 lig   :in integer;
		             vesp  :in out integer) is separate; 
                                  
                     
-- ***********************************
-- *** Fonctions et Procedures *******
-- ***********************************

-- parcourt un fichier texte jusqu'� trouver et renvoyer le type cherch�
Procedure Skipandget(fic:in text_io.file_type;itemf:out float) is
   begin
     loop
        begin
          flo_io.get(fic,itemf);
          exit;
          exception when text_io.data_error=>skip_line(fic);
        end;
     end loop;
   end Skipandget;

Procedure Skipandget(fic:in text_io.file_type;itemi:out integer) is
   begin
     loop
        begin
          int_io.get(fic,itemi);
          exit;
          exception when text_io.data_error=>skip_line(fic);
       end;
     end loop;
   end Skipandget;

procedure Skipandget(fic:in text_io.file_type; item:out fix) is
begin
  loop
     begin
       Fix_IO.get(fic,item,0);
       exit;
       exception when text_io.data_error=>skip_line(fic);
     end;
  end loop;
end Skipandget;
   
procedure Skipandget(fic:in text_io.file_type; item:out Coeff) is
begin
  loop
     begin
       Coeff_IO.get(fic,item,0);
       exit;
       exception when text_io.data_error=>skip_line(fic);
     end;
  end loop;
end Skipandget;

procedure Skipandget(fic:in text_io.file_type; item:out Poids) is
begin
  loop
     begin
       Poids_IO.get(fic,item,0);
       exit;
       exception when text_io.data_error=>skip_line(fic);
     end;
  end loop;
end Skipandget;  

procedure Skipandget(fic:in text_io.file_type; item:out fix2) is
begin
  loop
     begin
       Fix2_IO.get(fic,item,0);
       exit;
       exception when text_io.data_error=>skip_line(fic);
     end;
  end loop;
end Skipandget;

--------------------------------------------------------------------------
-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat 
procedure Existence_fic( ficn: in out text_io.file_type;
                         n_lig: in text_io.count;
                         nom_fic: in out string;
                         ncar: in out integer) is
fictest : text_io.file_type;
EMPILE_PALETTE_ERROR : exception;

begin
  set_line(ficn,n_lig);
  get_line(ficn,nom_fic,ncar);

  begin
    if ncar=0 and (N_lig=52 or N_lig=55) then
  	  if N_lig=52 then
	    Msg_Erreur:=To_Unbounded_String("L'image de mutilation:"&eol&nomfichier_image(1..ncar_fi)&eol&
		                                "n'existe pas."&eol&"Sa cr�ation n�cessite la donn�e d'un fichier d'empilement.");
      end if;
	  if N_lig=55 then
	    Msg_Erreur:=To_Unbounded_String("L'image de mutilation:"&eol&nomfichier_image(1..ncar_fi)&eol&
		                                "n'existe pas."&eol&"Sa cr�ation n�cessite la donn�e d'un fichier palette.");
      end if;
	  raise EMPILE_PALETTE_ERROR;
    end if;
  end;

  open(fictest,in_file,nom_fic(1..ncar));
  close(fictest);

exception when text_io.NAME_ERROR =>
  Msg_Erreur:=To_Unbounded_String("Impossible d'ouvrir le fichier: "&nom_fic(1..ncar));
  close (ficn);
  raise text_io.NAME_ERROR;
end Existence_fic;


--------------------------------------------------------------------------
-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat 
procedure Existence_fic2( ficn: in out text_io.file_type;
                         n_lig: in text_io.count;
                         nom_fic: in out string;
                         ncar: in out integer;
						 mutil: in out boolean;
						 cremu: in out boolean) is

fictest : text_io.file_type;

begin
  mutil:=false;
  cremu:=false;
  set_line(ficn,n_lig);
  ncar:=0;
  get_line(ficn,nom_fic,ncar);
  if ncar/=0 then begin
    mutil:=true;
    open(fictest,in_file,nom_fic(1..ncar));
    close(fictest);
    exception when text_io.NAME_ERROR =>
--      Msg_Erreur:=To_Unbounded_String("Impossible d'ouvrir le fichier: "&nom_fic(1..ncar));
--      opt:=False;
	  Cremu:=true;

--      close (ficn);
--      raise  text_io.NAME_ERROR;
    end;
  else
    mutil:=False;
	Cremu:=false;
  end if;
--  gb.MsgBox("mutil : "&boolean'image(mutil));
--  gb.MsgBox("cremutil : "&boolean'image(Cremutil));

end Existence_fic2;


--------------------------------------------------------------------------
-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat  
procedure Existence_ficopt( ficn: in out text_io.file_type;
                            n_lig: in text_io.count;
                            nom_fic: in out string;
                            ncar: in out integer;
                            opt: in out boolean) is
fictest : text_io.file_type;

begin
  set_line(ficn,n_lig);
  ncar:=0;
  get_line(ficn,nom_fic,ncar);
  if ncar/=0 then begin
    open(fictest,in_file,nom_fic(1..ncar));
    close(fictest);
    opt:=true;
    exception when text_io.NAME_ERROR =>
      Msg_Erreur:=To_Unbounded_String("Impossible d'ouvrir le fichier: "&nom_fic(1..ncar));
      opt:=False;
      close (ficn);
      raise  text_io.NAME_ERROR;
    end;
  else
    opt:=False;
  end if;

end Existence_ficopt;
    



--------------------------------------------------------------------------
-- mise � jour de la fenetre de d�roulement du PAT
procedure AFFICHAGE_DEROULEMENT(etape: in out gb.Label; top: in integer) is
begin
  gb.Move(etape, 20, gb.int(top), 300, 15);
  gb.Visible(etape, gb.true);
  gb.Move(dialog3.CancelButton, 250, gb.int(top+35), 70, 20);
  gb.Visible(dialog3.CancelButton, gb.true);
  gb.height(dialog3.form, gb.int(top+90));
end AFFICHAGE_DEROULEMENT;

-- =====================================================================
--- trouve les indices des separateurs dans une chaine de type Geoconcept
--- ====================================================================
Procedure INDICE_SEPARATEUR(Ligne : in string;		-- Chaine de caract�res
                           LLigne : in integer;		-- Longueur de la chaine
                           Separateur : in character ;	-- Separateur recherch�
                           Ptr_Indice : out liens_access_type 
                                        -- Pointeur des indices des separateurs
                           ) is

compteur : integer :=0;
Tab_indice : liens_array_type(1..Lligne);

begin
  for i in 1..LLigne loop
    if Ligne(i)=Separateur then
      compteur:=compteur+1;
      Tab_indice(compteur):=i;
    end if;
  end loop;
  -- le suivant du dernier caractere est compte comme separateur
  compteur:=compteur+1;
  Tab_indice(compteur):=LLigne+1;
  Ptr_indice :=new Liens_array_type'(Tab_indice(1..compteur));
end INDICE_SEPARATEUR;


-- =====================================================================
--- trouve les indices des separateurs dans une chaine de type Geoconcept
--- ====================================================================
Procedure INDICE_SEPARATEUR2(Ligne : in string;		-- Chaine de caract�res
                           LLigne : in integer;		-- Longueur de la chaine
                           Separateur : in character ;	-- Separateur recherch�
                           Ptr_Indice : out liens_access_type; -- Pointeur des indices des separateurs
						   compteur : out integer) is

Tab_indice : liens_array_type(0..Lligne);

begin
  compteur:=0;
  Tab_indice(0):=0;
  for i in 1..LLigne loop
    if Ligne(i)=Separateur then
      compteur:=compteur+1;
      Tab_indice(compteur):=i;
    end if;
  end loop;
  
  -- le suivant du dernier caractere est compte comme separateur
  
  -- debut modif 29.07.2005
  -- ... sauf si le dernier caract�re est lui-meme un separateur
  -- compteur:=compteur+1;
  -- Tab_indice(compteur):=LLigne+1;
  if (Ligne(LLigne)/=Separateur) then
    compteur:=compteur+1;
    Tab_indice(compteur):=LLigne+1;
  end if;
  -- fin modif 29.07.2005


  Ptr_indice :=new Liens_array_type'(Tab_indice(0..compteur));
end INDICE_SEPARATEUR2;


--====================================================================
-- teste l'existence de polices Windows correspondant aux polices du
-- fichier des codes typo  
--=====================================================================
Procedure TEST_TABPOLICE(hWnd: in Win32.winnt.Handle) is

use type win32.int;
use type win32.long;

hDC: Win32.windef.hdc ;
ps: Win32.winuser.LPPAINTSTRUCT;                   
taille: aliased win32.windef.size;
ln_defaut, ln: win32.long; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
ficpolice_in :   text_io.file_type;
car: character;
hauteur: float;
ncar, ncar_test, icar : integer;
ch: string(1..20);
nom_police :string(1..maxcar):=(1..maxcar => ' '); 
ch_test, font_defaut: string(1..maxcar);
code_typo : string30:=(1..30 => ' ');
nbfont: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
AOC: Win32.PSTR;
FONT_ERROR		: exception;

Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
MaxLLigne : integer:=200;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne):=(1..MaxLLigne => ' ');	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
last: positive ;
no_lig: text_io.count;  
InterLettres : float;

                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
  Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------

begin
  ps  := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd, ps);
  ch_test(1..49):="chaine assez longue pour tester la police charg�e";
  ncar_test:=49;
  font_defaut(1..11):="���������� ";
  AOC:= Win32.crt.strings.strcpy(CHAR_Array_to_PCHAR(log_font.lfFaceName),
                                Str_to_LPCSTR(font_defaut));
  Log_Font.lfHeight 		    := 0;
  Log_Font.lfWidth          := 0;
  Log_Font.lfEscapement     := 0;
  Log_Font.lfOrientation    := 0;
  Log_Font.lfWeight         := 0;
  Log_Font.lfItalic         := 0;
  Log_Font.lfUnderline      := 0;
  Log_Font.lfStrikeOut      := 0;
  Log_Font.lfCharSet        := 0;
  Log_Font.lfOutPrecision   := 0;
  Log_Font.lfClipPrecision  := 0;
  Log_Font.lfQuality        := 0;
  Log_Font.lfPitchAndFamily := 0;
  Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
     -- charger la police
  FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
  -- caracteristiques de la police
  res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
  -- calculer l'extension
  res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(ch_test(1..ncar_test)),
       win32.int(ncar_test),taille'unchecked_Access);
  ln_defaut:=taille.cx;
  --decharger la police
  res:=win32.wingdi.deleteobject(Font_Handle);
                                 
  nbfont:=0;
  open(ficpolice_in,in_file,nomfichier_polices(1..ncar_pol));
  
  loop
     if end_of_file(ficpolice_in) then exit; end if;

     begin
        no_lig:=line(ficpolice_in);
        Ligne:=(1..MaxLLigne => ' ');
        get_line(ficpolice_in,Ligne,LLigne);
     exception when text_io.END_ERROR =>
        close(ficpolice_in);
        Open(ficpolice_in,in_file,nomfichier_polices(1..ncar_pol));
        set_line(ficpolice_in, no_lig);
        LLigne:=0;
        while (not End_of_file(ficpolice_in)) and ( LLigne<MaxLLigne ) loop
          LLigne:=LLigne+1;
          get(ficpolice_in, Ligne(LLigne));
        end loop;
     end;
  
     nbfont:=nbfont+1;
     nom_police :=(1..maxcar => ' ');
     code_typo:=(1..30 => ' ');
     INDICE_SEPARATEUR(Ligne,LLigne,Tab,Ptr_Indice);
     code_typo(1..Ptr_indice(1)-1):=Ligne(1..Ptr_indice(1)-1);
     ncar:=Ptr_indice(2)-Ptr_indice(1)-1;
     nom_police(1..ncar):= Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);
  -- on rajoute le caractere nul en fin de chaine
     nom_police (1..ncar+1):=nom_police (1..ncar)&ascii.nul;
     flo_io.get(Ligne(Ptr_indice(2)+1..LLigne), hauteur, last);
     flo_io.get(Ligne(Ptr_indice(3)+1..LLigne), InterLettres, last);
     Gr_free_liens_array(Ptr_indice);
                         
    -- creer le police logique
    AOC:= Win32.crt.strings.strcpy(CHAR_Array_to_PCHAR(log_font.lfFaceName),
                                   Str_to_LPCSTR(nom_police));
    Log_Font.lfHeight 		  :=0;
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
    Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
    -- caracteristiques de la police
    res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
    -- calculer l'extension
    res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(ch_test(1..ncar_test)),
       win32.int(ncar_test),taille'unchecked_Access);
    ln :=taille.cx;
     --decharger la police
    res:=win32.wingdi.deleteobject(Font_Handle);
    if ln=ln_defaut then raise FONT_ERROR; end if;
  end loop;
  close(ficpolice_in);
exception
  when FONT_ERROR =>
    close(ficpolice_in);
    Msg_Erreur:=
        To_Unbounded_String("Erreur de chargement de la police: "& nom_police);
    raise FONT_ERROR;
  when others => 
    close(ficpolice_in);
    Msg_Erreur:=To_Unbounded_String(
            "Erreur de lecture:"&EOL&"fichier: "&nomfichier_polices(1..ncar_pol)
			                    &EOL&"ligne no"&integer'image(integer(no_lig)));
    raise;
end TEST_TABPOLICE;

--============================================================================
-- outil de conversion des tailles des codes typo
procedure Outil_CodesTypo(hWnd: in Win32.winnt.Handle;
                          nom_ficin: in string;
                          nom_ficout: in string;
                          option: in integer) is

use type win32.int;
use type win32.long;

ficin, ficout :   text_io.file_type;
H_400, Corps_pt, hauteur: float;
string_corps : string(1..7):=(1..7 => ' ');
i : integer:=0;
hDC: Win32.windef.hdc ;
ps: Win32.winuser.LPPAINTSTRUCT;                   
taille: aliased win32.windef.size;
ln_defaut, ln: win32.long; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
car: character;
ncar, ncar_test, icar : integer;
ch: string(1..20);
nom_police :string(1..maxcar):=(1..maxcar => ' '); 
ch_test, font_defaut: string(1..maxcar);
code_typo : string30:=(1..30 => ' ');
nbfont: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
AOC: Win32.PSTR;
FONT_ERROR		: exception;

Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
MaxLLigne : integer:=200;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne):=(1..MaxLLigne => ' ');	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
last: positive;
no_lig: text_io.count;  
InterLettres : float;

                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
  Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------
        
begin
  ps  := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd, ps);
  ch_test(1..49):="chaine assez longue pour tester la police charg�e";
  ncar_test:=49;
  font_defaut(1..11):="���������� ";
  AOC:= Win32.crt.strings.strcpy(CHAR_Array_to_PCHAR(log_font.lfFaceName),
                                Str_to_LPCSTR(font_defaut));
  -- on charge systematiquement une police de corps 400
  Log_Font.lfHeight 			  :=win32.long(-400);
  Log_Font.lfWidth          := 0;
  Log_Font.lfEscapement     := 0;
  Log_Font.lfOrientation    := 0;
  Log_Font.lfWeight         := 0;
  Log_Font.lfItalic         := 0;
  Log_Font.lfUnderline      := 0;
  Log_Font.lfStrikeOut      := 0;
  Log_Font.lfCharSet        := 0;
  Log_Font.lfOutPrecision   := 0;
  Log_Font.lfClipPrecision  := 0;
  Log_Font.lfQuality        := 0;
  Log_Font.lfPitchAndFamily := 0;
  Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
     -- charger la police
  FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
  -- caracteristiques de la police
  res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
  -- calculer l'extension
  res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(ch_test(1..ncar_test)),
       win32.int(ncar_test),taille'unchecked_Access);
  ln_defaut:=taille.cx;
  --decharger la police
  res:=win32.wingdi.deleteobject(Font_Handle);
                                 
  nbfont:=0;
  open(ficin,in_file,nom_ficin);
  create(ficout, out_file, nom_ficout);
  
  loop
     if end_of_file(ficin) then exit; end if;

     begin
        no_lig:=line(ficin);
        Ligne:=(1..MaxLLigne => ' ');
        get_line(ficin,Ligne,LLigne);
     exception when text_io.END_ERROR =>
        close(ficin);
        Open(ficin,in_file,nom_ficin);
        set_line(ficin, no_lig);
        LLigne:=0;
        while (not End_of_file(ficin)) and ( LLigne<MaxLLigne ) loop
          LLigne:=LLigne+1;
          get(ficin, Ligne(LLigne));
        end loop;
     end;
  
     nbfont:=nbfont+1;
     nom_police :=(1..maxcar => ' ');
     code_typo:=(1..30 => ' ');
     INDICE_SEPARATEUR(Ligne,LLigne,Tab,Ptr_Indice);
     code_typo(1..Ptr_indice(1)-1):=Ligne(1..Ptr_indice(1)-1);
     ncar:=Ptr_indice(2)-Ptr_indice(1)-1;
     nom_police(1..ncar):= Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);
  -- on rajoute le caractere nul en fin de chaine
     nom_police (1..ncar+1):=nom_police (1..ncar)&ascii.nul;
     flo_io.get(Ligne(Ptr_indice(2)+1..LLigne), hauteur, last);
     flo_io.get(Ligne(Ptr_indice(3)+1..LLigne), InterLettres, last);
                         
    -- creer le police logique
    AOC:= Win32.crt.strings.strcpy(CHAR_Array_to_PCHAR(log_font.lfFaceName),
                                   Str_to_LPCSTR(nom_police));
    -- on charge systematiquement une police de corps 400
    Log_Font.lfHeight 			  :=win32.long(-400);
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
    Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
    -- caracteristiques de la police
    res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
    -- calculer l'extension
    res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(ch_test(1..ncar_test)),
       win32.int(ncar_test),taille'unchecked_Access);
    ln :=taille.cx;
    begin
    if ln=ln_defaut then raise FONT_ERROR; end if;
    exception when FONT_ERROR =>
      close(ficin);
      Msg_Erreur:=
        To_Unbounded_String("Erreur de chargement de la police: "& nom_police);
      raise FONT_ERROR;
    end;
    -- Corps en points=lptm.tmheight-lptm.tmInternalleading
    -- Hauteur des majuscules=lptm.tmheight-lptm.tmdescent-lptm.tmInternalleading
    -- Hauteur des majuscules=Corps en points-lptm.tmdescent
    H_400:= Float(400-lptm.tmdescent);
    
    case option is
      when 1 => -- param�tre DataDraw: 1.4*hauteur des majuscules en mm
        --Corps_pt:=(400.0/H_400)*(72.0/25.4)*hauteur/1.4;
        Corps_pt:=(400.0/H_400)*(72.0/25.4)*hauteur/1.35;
      when 2 => -- hauteur des majuscules en points
        Corps_pt:=(400.0/H_400)*hauteur;
      when 3 => -- hauteur des majuscules en mm
        Corps_pt:=(400.0/H_400)*(72.0/25.4)*hauteur;
      when 4 => -- corps en mm
        Corps_pt:=(72.0/25.4)*hauteur;
      when others => null;
    end case;
    flo_io.put(string_corps, Corps_pt, 3, 0);
    i:=1;
    while string_corps(i)=' ' loop i:=i+1; end loop;
    put_line(ficout, Ligne(1..Ptr_indice(2))&string_corps(i..7));
    Gr_free_liens_array(Ptr_indice);
    --decharger la police
    res:=win32.wingdi.deleteobject(Font_Handle);
  end loop;
  close(ficin);
  close(ficout);
exception when others => raise;
end Outil_CodesTypo;

--============================================================================
-- chargement des parametres de calcul
Procedure CHARGE_PARAM is
  ficpar:text_io.file_type;
  ch : string120;
  n : integer;
  symb : Affichage_type;
  no_lig: text_io.count;
  paramHori,ParamNorou : integer range 0..1;
begin
    open(ficpar,in_file,nomfichier_param_calcul(1..ncar_fpc));
    skipandget(ficpar,maxpi);
	maxp:=positive(maxpi);
    skipandget(ficpar,maxnpi);
    skipandget(ficpar,dilx);
    skipandget(ficpar,dily);

    skipandget(ficpar,ParamHori);
	if Text(EcrituresText) /= "" or ParamHori=1 then
      skipandget(ficpar,pas_maillemm);
      skipandget(ficpar,petit_pas_maillemm);
      skipandget(ficpar,pas_contourmm);
      pas_contour:=positive(pas_contourmm*float(ptparmm));
      part_cercle:=integer(pas_contour);
      pas_maille:=positive(pas_maillemm*float(ptparmm)); 
      Petit_pas_maille:=positive(petit_pas_maillemm*float(ptparmm)); 
      skipandget(ficpar,espace);
      skipandget(ficpar,Coup_option);
      skipandget(ficpar,Coup_oblig);
      skipandget(ficpar,dilat);
      skipandget(ficpar,decalno);
      skipandget(ficpar,flag_abrev);
      skipandget(ficpar,cpoimu);
      skipandget(ficpar,cpoidir);
      skipandget(ficpar,cpoidnc);
      skipandget(ficpar,cpoip);
      skipandget(ficpar,cpoii);
      skipandget(ficpar,pardir.ne);
      skipandget(ficpar,pardir.e);
      skipandget(ficpar,pardir.se);
      skipandget(ficpar,pardir.s);
      skipandget(ficpar,pardir.so);
      skipandget(ficpar,pardir.o);
      skipandget(ficpar,pardir.no);
      skipandget(ficpar,pardir.n);
      skipandget(ficpar,pardir.int);
      skipandget(ficpar,pardir.zi);
      skipandget(ficpar,pardir.zm);
      skipandget(ficpar,pardir.ze);
    end if;
  
    if mutil then 
      delta_xpix:= (pse.coor_x-pso.coor_x)/pt_image ; -- +1 ??? 
      delta_ypix:= (pne.coor_y-pse.coor_y)/pt_image ; -- +1 ??? Attention aux marges !
    end if;

    skipandget(ficpar,nb_codes_route);
    if nb_codes_route/=0 then
      skip_line(ficpar);
      skip_line(ficpar);
      for i in 1..nb_codes_route loop
        get_line( ficpar, ch, n);
        tcodes_route(i)(1..30):=(others=>' ');
        tcodes_route(i)(1..n):=ch(1..n);
      end loop;
      skipandget(ficpar,cpoi_intersec_plus);
    end if;
    	
    close(ficpar);
  exception when others => 
    no_lig:=line(ficpar); 
    Msg_Erreur:=To_Unbounded_String(
            "Erreur de lecture:"&EOL
            &"Fichier: "&nomfichier_param_calcul(1..ncar_fpc)&eol
            &"Ligne n� "&Integer'Image(integer(no_lig)));
    close(ficpar); 
    raise; 
  end CHARGE_PARAM;                                     
 
--============================================================================
-- remplissage de tbnomi et calcul d'infos suppl�mentaires sur les �critures
procedure DET_NOMS(tbnomi : in out typtbnomi;
                   tzi : in out typtbe; -- Tableau des zones d'influence
                   maxbla : in out integer;
		           vesp : in out integer;
                   InvEchelle : in integer;
                   Resolution : in positive;
				   Minimum_terrain : in point_type_reel;
                   hWnd : in Win32.winnt.Handle) is  -- Handle de fen Windows
  
hDC : Win32.windef.hdc;
ps : Win32.winuser.LPPAINTSTRUCT;
info_font : typtfont(1..500);
nbfont : integer;
lig:integer:=0;

begin
  ps := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd, ps);
  CHARGE_FONTES(info_font,nbfont); -- chargement des polices du fichier des codes typo
  LECTURE_CHARGT(tbnomi,info_font,nbfont); -- remplissage de certains champs de tbnomi
  CESURE(tbnomi,tbnomi'length-nbnorou); -- C�sure des �critures

  for no in 1..tbnomi'last-nbnorou loop
    EMPRISE(tbnomi,no,info_font,hdc); -- determination de l'emprise en fction de la police utilisee
  end loop;
  if blasons and Text(EcrituresText)/="" then
    PABMAIN(tbnomi,InvEchelle,Resolution,vesp,hdc);
  end if;
    for no in 1..Tbnomi'last-NbNoRou loop
      if tbnomi(no).ncar2=0 then 
	    -- anciennement :
	    -- if (tbnomi(no).nl=1 and tbnomi(no).ncar2=0) then  ...d'ou certaines ZI pas assez hautes
        lig:=1;
      else
        lig:=2;
      end if;		
      det_zi(tbnomi,no,tzi,lig,vesp); --determination de la zone d'influence des �critures
    end loop;
end det_noms;

--======================================================================
-- determination des interactions entre �critures
procedure DET_CI (tbnomi:in typtbnomi; 
                  tzi:in typtbe; -- tableau des zones d'influence
                  tnci  :in out typtnci;
                  tici  :in out typtici) is

nno :integer:=tbnomi'LAST;

begin
	
  for a in 1..(nno-1) loop
  	
    if 300*(A-1)/(nno-1) /= 300*A/(nno-1) or A=(nno-1) then		
      gb.Move(dialog3.Panel2,0,0,gb.INT(300*A/(nno-1)),20);
    end if;

    for b in a+1..nno loop
      if chevauch(tzi(a),tzi(b)) then
        tnci(a):=tnci(a)+1;
        tnci(b):=tnci(b)+1;
        tici(a,tnci(a)):=b;
        tici(b,tnci(b)):=a;
      end if; --chevauch
    end loop; --b
  end loop; --a

end det_ci;

--==============================================================================
-- cr�ation du tableau des ambiguit�s entre codes typo
Procedure Charge_ambiguites(tambi:in out typtambi; nb_ambi : out natural) is

     fic_ambi 	: text_io.file_type;
     Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
     MaxLLigne : integer:=200;	-- Longueur maximale des lignes du fichier texte
     Ligne : string (1..MaxLLigne):=(1..MaxLLigne => ' ');	-- Ligne du fichier texte
     LLigne : integer;		-- Longueur de la ligne
     no_lig: text_io.count;
     TAB_ERROR		: exception;

begin
   nb_ambi:=0;
    
   -- on commence par mettre les ambiguites qui sont dans le fichier
   if ncar_amb>0 then
     open(fic_ambi,in_file,nomfichier_ambiguites(1..ncar_amb));
     loop
       if end_of_file(fic_ambi) then exit; end if;
       begin
         no_lig:=line(fic_ambi);
         Ligne:=(1..MaxLLigne => ' ');
         get_line(fic_ambi,Ligne,LLigne);
       exception when text_io.END_ERROR =>
         close(fic_ambi);
         open(fic_ambi,in_file,nomfichier_ambiguites(1..ncar_amb));
         set_line(fic_ambi, no_lig);
         LLigne:=0;
         while (not End_of_file(fic_ambi)) and ( LLigne<MaxLLigne ) loop
           LLigne:=LLigne+1;
           get(fic_ambi, Ligne(LLigne));
         end loop;
       end;
       nb_ambi:=nb_ambi+1;
       INDICE_SEPARATEUR(Ligne,LLigne,Tab,Ptr_Indice);
       -- test pour s'assurer qu'il n'y a qu'une tabulation par ligne
       if ((Ptr_indice(2))/=(LLigne+1)) then
         raise TAB_ERROR;
       end if;
       tambi(nb_ambi).code1(1..Ptr_indice(1)-1):=Ligne(1..Ptr_indice(1)-1);
       tambi(nb_ambi).code2(1..LLigne-Ptr_indice(1)):=Ligne(Ptr_indice(1)+1..LLigne);
       Gr_free_liens_array(Ptr_indice);
     end loop;
     close(fic_ambi);
   end if;

   -- on rajoute les ambiguites pour les code identiques
   for i in 1..ncode loop
          nb_ambi:=nb_ambi+1;
          tambi(nb_ambi).code1:= tcode(i);
          tambi(nb_ambi).code2:=tcode(i);
   end loop;
 exception when  Event : others =>
  Msg_Erreur:=
    To_Unbounded_String(
      "Erreur de lecture du fichier: "&nomfichier_ambiguites(1..ncar_amb)
      &eol&"Ligne n� "&Integer'Image(integer(no_lig)));
  close(fic_ambi);
  raise;          
 end Charge_ambiguites;

--============================================================================
-- calcule la longueur d'une �criture (dans Insere_nouvelles_positions) 
procedure Det_long(tbnomi: in out typtbnomi;
                   n: in integer;
                   hWnd: in Win32.winnt.Handle) is 
  
hDC: Win32.windef.hdc ;
ps: Win32.winuser.LPPAINTSTRUCT;
info_font: typtfont(1..500);
nbfont: integer ;

begin
  ps  := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd, ps);
  CHARGE_FONTES(info_font,nbfont);
  EMPRISE(tbnomi,n,info_font,hdc);
end Det_long;
                
--=====================================================================
-- calcule le nb de codes typo diff�rents utilis�s dans tbnomi
Function Nb_code(tbnomi:in typtbnomi) return positive is
  
  trouve : boolean;
begin
    ncode:=1;
    tcode(ncode):=tbnomi(1).topo.code;
    for no in 2..tbnomi'last loop
        trouve:=false;
        for c in 1..ncode loop
	     if tbnomi(no).topo.code=tcode(c) then
            trouve:=true;-- le code est deja repertorie 
         	exit;
         end if; 
        end loop;
           
	    if not trouve then 
            ncode:=ncode+1; 
            tcode(ncode):=tbnomi(no).topo.code;
        end if;
    end loop;
    return ncode; 
end Nb_code;
--=====================================================================

end INIPAN;
