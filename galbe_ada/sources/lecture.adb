with text_io ; use text_io;
with geometrie; use geometrie;
With ada.numerics.elementary_functions; use  ada.numerics.elementary_functions;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with gb;
with norou; use norou;
with dialog3;

with  Win32.WinUser;

Package body Lecture is

-- =====================================================================
Package flo_io is new float_io(float);use flo_io;
package int_io is new integer_io(integer);use int_io;

--========================================================================
Procedure Filtre_Ptsdoubles(tab_pts :  in out point_liste_type; -- Points lus en coordonnees Graphe
                            Nb_pts : in out integer		-- Nombre de points
							) is
Tab_pts_temp : point_liste_type(1..Nb_pts);
nb_temp: integer:=1;
begin
  for i in 2..nb_pts loop
    if tab_pts(i)/=tab_pts(i-1) then 
      nb_temp:=nb_temp+1;
      Tab_pts_temp(nb_temp):=tab_pts(i);
    end if;
  end loop;
  nb_pts:=nb_temp;
  tab_pts(2..nb_temp):=Tab_pts_temp(2..nb_temp);
end Filtre_Ptsdoubles;

--========================================================================
Procedure Intersection(A,B,C,D 	: Point_type;
                       Flag 		: out boolean; 
				 							 M 				: out Point_type) is
Mode : Distance_type:=c_segment_type;
begin
  Intersection_de_lignes(A,B,C,D,Mode,Flag,M);
end Intersection;

--========================================================================
Procedure Sens(I,S1,S2,S3,S4 	: Point_type;
               Flag 		: out boolean) is
Mode : Distance_type:=c_segment_type;
C1,C2,C3,C4,M: Point_type_reel;
begin
  C1.coor_x:=cos(Angle_horizontal(I,S1));
  C1.coor_y:=sin(Angle_horizontal(I,S1));
  C2.coor_x:=cos(Angle_horizontal(I,S2));
  C2.coor_y:=sin(Angle_horizontal(I,S2));
  C3.coor_x:=cos(Angle_horizontal(I,S3));
  C3.coor_y:=sin(Angle_horizontal(I,S3));
  C4.coor_x:=cos(Angle_horizontal(I,S4));
  C4.coor_y:=sin(Angle_horizontal(I,S4));
  Intersection_de_lignes(C1,C3,C2,C4,Mode,Flag,M);
end sens;

--========================================================================
Procedure Pt_inter(I,S1,S2,S3,S4 	: Point_type;
                   I1,I2 		: out Point_type) is
Flag1, Flag2, Flag3: boolean;
IntM, IntP, Temp : point_type;
begin
  IntM.coor_x:=I.coor_x-1;
  IntM.coor_y:=I.coor_y;
  IntP.coor_x:=I.coor_x+1;
  IntP.coor_y:=I.coor_y;
  if S2=S3 then
    if I.coor_y=S2.coor_y then
      IntM.coor_x:=I.coor_x;
      IntM.coor_y:=I.coor_y-1;
      IntP.coor_x:=I.coor_x;
      IntP.coor_y:=I.coor_y+1;
    end if;
    Intersection(S1,IntM,IntP,S4,Flag1,Temp);
    Intersection(S1,IntM,IntP,S3,Flag2,Temp);
    Intersection(S2,IntM,IntP,S4,Flag3,Temp);
    if (Flag1=true or Flag2=true or Flag3=true) then
      I1:=IntP; I2:=IntM;
    else
      I1:=IntM; I2:=IntP;
    end if;
    return;
  end if;
  Intersection(S1,IntM,IntP,S4,Flag1,Temp);
  Intersection(S2,IntM,IntP,S3,Flag2,Temp);
  if ( Flag1=True or Flag2=True ) then
    I1:=IntP; I2:=IntM;
  else
    I1:=IntM; I2:=IntP;
  end if;
end Pt_inter;
       
--========================================================================
-- Procedure Debouclage visant � supprimer les surfaces s'intersectant
-- (ex du 8)
--========================================================================
Procedure Debouclage(
				tab_pts :  in out point_liste_type; -- Points lus en coordonnees Graphe
        Nb_pts : in out integer		-- Nombre de points
                    ) is
Flag: boolean;
Int, Int1, Int2 : point_type;
Tab1, Tab2: point_liste_type(0..2*Nb_pts);
n1, n2, i, n_iter: integer:=0;
S1,S2,S3,S4,S_temp: integer;
Erreur_Iteration  : 		exception; 
begin
  -- Elimination des points doubles successifs
  Filtre_Ptsdoubles(tab_pts, Nb_pts);
  -- Tableaux temp
  n1:= nb_pts-1;
  Tab1(0):=tab_pts(n1);
  Tab1(1..nb_pts):=tab_pts(1..nb_pts);
  Tab1(n1+2):=tab_pts(2);
  --
  <<label>>
  n_iter:=n_iter+1;  
  -- debut modif 07.04.2005
  -- If n_iter>20 then raise Erreur_Iteration; end if;
  If n_iter>20 then return; end if;
  -- fin modif 07.04.2005

  i:=1;  
  for j in i+2..n1-1 loop
      Intersection(Tab1(i),Tab1(i+1),Tab1(j),Tab1(j+1),Flag,Int);
      if Flag=true then
        if ((Int.coor_x-Tab1(i).coor_x)**2+(Int.coor_y-Tab1(i).coor_y)**2)<=16 then
          S1:=i-1;
        else
          S1:=i;
        end if;
        if ((Int.coor_x-Tab1(j).coor_x)**2+(Int.coor_y-Tab1(j).coor_y)**2)<=16 then
          S2:=j-1;
        else
          S2:=j;
        end if;
        if ((Int.coor_x-Tab1(i+1).coor_x)**2+(Int.coor_y-Tab1(i+1).coor_y)**2)<=16 then
          S3:=i+2;
        else
          S3:=i+1;
        end if;
        if ((Int.coor_x-Tab1(j+1).coor_x)**2+(Int.coor_y-Tab1(j+1).coor_y)**2)<=16 then
          S4:=j+2;
        else
          S4:=j+1;
        end if;
        Sens(Int,Tab1(S1),Tab1(S2),Tab1(S3),Tab1(S4),Flag);
        If flag=False then
          S_temp:=S3; S3:=S2; S2:=S_temp;
        end if;
        if S1>=1 then
          n2:=S1; 
          Tab2(1..n2):=Tab1(1..S1);
        else -- S1=0
          n2:=1;
          Tab2(1):=Tab1(S1);
        end if;
        Pt_inter(Int,Tab1(S1),Tab1(S2),Tab1(S3),Tab1(S4),Int1,Int2);
        n2:=n2+1;
        Tab2(n2):=Int1;
		-- debut modif 07.04.2005
        -- if S3>S2 then
        if S3>S2 and (n2+S3-S2+1)<=Tab2'last then
		-- fin modif 07.04.2005
          Tab2(n2+1..n2+S3-S2+1):=Tab1(S2..S3);
          n2:=n2+S3-S2+1;
        else
          for k in reverse S3..S2 loop
            n2:=n2+1;
            Tab2(n2):=Tab1(k);
          end loop;
        end if;
        n2:=n2+1;
        Tab2(n2):=Int2;
		-- debut modif 07.04.2005
        -- if S4<=(n1+1) then
        if S4<=(n1+1) and (n2+n1-S4+3)<=Tab2'last then
		-- fin modif 07.04.2005
          Tab2(n2+1..n2+n1-S4+3):=Tab1(S4..n1+2);
          n2:=n2+n1-S4+1;
        else -- S4=n1+2
          Tab2(1):=Tab2(n2);
          n2:=n2-1;
        end if;
        if S1=0 then
          n2:=n2-1;
        end if;
        n1:=n2;
        Tab1(0):=Tab2(n2);
        Tab1(1..n2):=Tab2(1..n2);
        Tab1(n2+1):=Tab2(1);
        Tab1(n2+2):=Tab2(2);
        goto label;
      end if;
  end loop;
  for i in 2..n1-2 loop
    for j in i+2..n1 loop
	  Intersection(Tab1(i),Tab1(i+1),Tab1(j),Tab1(j+1),Flag,Int);
      if Flag=true then
        if ((Int.coor_x-Tab1(i).coor_x)**2+(Int.coor_y-Tab1(i).coor_y)**2)<=25 then
          S1:=i-1;
        else
          S1:=i;
        end if;
        if ((Int.coor_x-Tab1(j).coor_x)**2+(Int.coor_y-Tab1(j).coor_y)**2)<=25 then
          S2:=j-1;
        else
          S2:=j;
        end if;
        if ((Int.coor_x-Tab1(i+1).coor_x)**2+(Int.coor_y-Tab1(i+1).coor_y)**2)<=25 then
          S3:=i+2;
        else
          S3:=i+1;
        end if;
        if ((Int.coor_x-Tab1(j+1).coor_x)**2+(Int.coor_y-Tab1(j+1).coor_y)**2)<=25 then
          S4:=j+2;
        else
          S4:=j+1;
        end if;
        Sens(Int,Tab1(S1),Tab1(S2),Tab1(S3),Tab1(S4),Flag);
        If flag=False then
          S_temp:=S3; S3:=S2; S2:=S_temp;
        end if;
        if S1>=1 then
          n2:=S1; 
          Tab2(1..n2):=Tab1(1..S1);
        else -- S1=0
          n2:=1;
          Tab2(1):=Tab1(S1);
        end if;
        Pt_inter(Int,Tab1(S1),Tab1(S2),Tab1(S3),Tab1(S4),Int1,Int2);
        n2:=n2+1;
        Tab2(n2):=Int1;
        -- debut modif 07.04.2005
        -- if S3>S2 then
        if S3>S2 and (n2+S3-S2+1)<=Tab2'last then
        -- fin modif 07.04.2005
          Tab2(n2+1..n2+S3-S2+1):=Tab1(S2..S3);
          n2:=n2+S3-S2+1;
        else
          for k in reverse S3..S2 loop
            n2:=n2+1;
            Tab2(n2):=Tab1(k);
          end loop;
        end if;
        n2:=n2+1;
        Tab2(n2):=Int2;

        -- debut modif 07.04.2005
        -- if S4<=(n1+1) then
        if S4<=(n1+1) and (n2+n1-S4+3)<=Tab2'last then
        -- fin modif 07.04.2005
          Tab2(n2+1..n2+n1-S4+3):=Tab1(S4..n1+2);
          n2:=n2+n1-S4+1;
        else -- S4=n1+2
          Tab2(1):=Tab2(n2);
          n2:=n2-1;
        end if;
        if S1=0 then
          n2:=n2-1;
        end if;
        n1:=n2;
        Tab1(0):=Tab2(n2);
        Tab1(1..n2):=Tab2(1..n2);
        Tab1(n2+1):=Tab2(1);
        Tab1(n2+2):=Tab2(2);
        goto label;
      end if;
    end loop;  
  end loop;
  nb_pts:=n1+1;
  Tab_pts(1..nb_pts):=Tab1(1..nb_pts);
exception 
   when Erreur_Iteration => raise;
end Debouclage;

----========================================================================
---- Procedure Debouclage visant � supprimer les surfaces s'intersectant
---- (ex du 8)
----========================================================================
--Procedure Debouclage(
--				tab_pts :  in out point_liste_type; -- Points lus en coordonnees Graphe
--        Nb_pts : in out integer		-- Nombre de points
--                    ) is
--B1, B2: boolean;
--Int, IntM, IntP : point_type;
--Tab_pts_temp : point_liste_type(1..2*Nb_pts);                      
--begin
--  -- sommets doubles:
--  -- i=1
--  for j in 4..nb_pts-1 loop
--    if tab_pts(1)=tab_pts(j) then
--      IntM.coor_x:=tab_pts(1).coor_x-1;
--      IntM.coor_y:=tab_pts(1).coor_y;
--      IntP.coor_x:=tab_pts(1).coor_x+1;
--      IntP.coor_y:=tab_pts(1).coor_y;
--      Intersection(tab_pts(nb_pts-1),IntM,IntP,tab_pts(j+1), Int, B1);
--      Intersection(IntM,tab_pts(j-1),IntP,tab_pts(2), Int, B2);
--      if (B1=true or B2=true) then
--        Tab_pts_temp(1):=IntP;
--        Tab_pts_temp(j):=IntM; 
--      else  
--        Tab_pts_temp(1):=IntM;
--        Tab_pts_temp(j):=IntP;
--      end if;
--      Intersection(tab_pts(nb_pts-1),tab_pts(2),tab_pts(j-1),tab_pts(j+1), Int, B1);
--      if B1=true then
--        for k in 1+1..j-1 loop
--          Tab_pts_temp(k):=Tab_pts(1+j-k);
--        end loop;
--        Tab_pts(1..j):=Tab_pts_temp(1..j);
--      else  
--        Tab_pts(1):=Tab_pts_temp(1);
--        Tab_pts(j):=Tab_pts_temp(j);
--      end if;
--    end if;
--  end loop;       
--  -- i>1      
--  for i in 2..nb_pts-4 loop
--    for j in i+3..nb_pts-1 loop
--      if tab_pts(i)=tab_pts(j) then
--        IntM.coor_x:=tab_pts(i).coor_x-1;
--        IntM.coor_y:=tab_pts(i).coor_y;
--        IntP.coor_x:=tab_pts(i).coor_x+1;
--        IntP.coor_y:=tab_pts(i).coor_y;
--        Intersection(tab_pts(i-1),IntM,IntP,tab_pts(j+1), Int, B1);
--        Intersection(IntM,tab_pts(j-1),IntP,tab_pts(i+1), Int, B2);
--        if (B1=true or B2=true) then
--          Tab_pts_temp(i):=IntP;
--          Tab_pts_temp(j):=IntM; 
--        else  
--          Tab_pts_temp(i):=IntM;
--          Tab_pts_temp(j):=IntP;
--        end if;
--        Intersection(tab_pts(i-1),tab_pts(i+1),tab_pts(j-1),tab_pts(j+1), Int, B1);
--        if B1=true then
--          for k in i+1..j-1 loop
--            Tab_pts_temp(k):=Tab_pts(i+j-k);
--          end loop;
--          Tab_pts(i..j):=Tab_pts_temp(i..j);
--        else  
--          Tab_pts(i):=Tab_pts_temp(i);
--          Tab_pts(j):=Tab_pts_temp(j);
--        end if;
--      end if;
--    end loop;    
--  end loop;    
--  -- arcs qui s'intersectent:
--  -- i=1
--  for j in 3..nb_pts-2 loop
--    Intersection(tab_pts(1),tab_pts(2),tab_pts(j),tab_pts(j+1), Int, B1);
--      if B1=True then
--        IntM.coor_x:=Int.coor_x-1;
--        IntM.coor_y:=Int.coor_y;
--        IntP.coor_x:=Int.coor_x+1;
--        IntP.coor_y:=Int.coor_y;
--        Intersection(tab_pts(1),IntM,IntP,tab_pts(j+1), Int, B1);
--        Intersection(IntM,tab_pts(j),IntP,tab_pts(2), Int, B2);
--        if (B1=true or B2=true) then
--          Tab_pts_temp(2):=IntP;
--          Tab_pts_temp(j+2):=IntM; 
--        else  
--          Tab_pts_temp(2):=IntM;
--          Tab_pts_temp(j+2):=IntP;
--        end if;
--        for k in 3..j+1 loop
--          Tab_pts_temp(k):=Tab_pts(3+j-k);
--        end loop;
--        Tab_pts_temp(j+3..nb_pts+2):=Tab_pts(j+1..nb_pts);
--        Tab_pts(2..nb_pts+2):=Tab_pts_temp(2..nb_pts+2);
--        nb_pts:=nb_pts+2;
--      end if;
--  end loop;
--  -- i>1
--  for i in 2..nb_pts-3 loop
--    for j in i+2..nb_pts-1 loop
--      Intersection(tab_pts(i),tab_pts(i+1),tab_pts(j),tab_pts(j+1), Int, B1);
--      if B1=True then
--        IntM.coor_x:=Int.coor_x-1;
--        IntM.coor_y:=Int.coor_y;
--        IntP.coor_x:=Int.coor_x+1;
--        IntP.coor_y:=Int.coor_y;
--        Intersection(tab_pts(i),IntM,IntP,tab_pts(j+1), Int, B1);
--        Intersection(IntM,tab_pts(j),IntP,tab_pts(i+1), Int, B2);
--        if (B1=true or B2=true) then
--          Tab_pts_temp(i+1):=IntP;
--          Tab_pts_temp(j+2):=IntM; 
--        else  
--          Tab_pts_temp(i+1):=IntM;
--          Tab_pts_temp(j+2):=IntP;
--        end if;
--        for k in i+2..j+1 loop
--          Tab_pts_temp(k):=Tab_pts(i+j+2-k);
--        end loop;
--        Tab_pts_temp(j+3..nb_pts+2):=Tab_pts(j+1..nb_pts);
--        Tab_pts(i+1..nb_pts+2):=Tab_pts_temp(i+1..nb_pts+2);
--        nb_pts:=nb_pts+2;
--      end if;
--    end loop;    
--  end loop;
--end Debouclage;
--

-- ========================================================================
-- Procedure interne visant a stocker les coordonnees d'une chaine Geoconcept
-- (chaque coordonnee etant separee par un ;) 
-- ========================================================================
procedure LirePoints(
        Ligne : in out string;		-- Ligne texte de coordonnees Geoconcept
        LLigne : in integer;		-- Longueur de la ligne
        InvEchelle : in integer;	-- Inverse de l'echelle de redaction
        Resolution : in integer;	-- Resolution de travail (nbr de pts par mm)
        Minimum_terrain : in point_type_reel;-- Origine des coordonnes terrain
        tab_pts : out point_liste_type; -- Points lus en coordonnees Graphe
        Nb_pts : out integer;		-- Nombre de points
        vnoVse : in boolean; -- true si contribution au calcul de Vno et Vso
        Arrondi : in boolean) is -- true si les coordonnees terrain doivent etre arrondies

Erreur_Plusieurs_Arcs, Erreur_Nombre_De_Points,
Erreur_LirePoints 				: exception ;
NbPtsALire : positive ;
X,Y : float;
last,Debut : positive ;
Preel : Point_type_reel;
-- si Arrondi=true alors les coordonnees terrain sont arrondies � l'entier multiple
-- de Arrondiviseur le plus proche de facon a corriger d'eventuelles petites
-- discontinuites dans les objets lineaires
Arrondiviseur : integer:=2; -- ce qui fait 0.08 mm carte au 1:25.000
                            --             0.02 mm carte au 1:100.000

begin
  -- Chaine vide
  if LLigne=0 then Nb_pts:=0; return; end if;

	-- Nombre d'arcs de la chaine a traiter
	if Ligne(1..2) /= "1;" then
           -- anomalie de donnees d'entree : il y a plusieurs arcs
   	 raise Erreur_Plusieurs_Arcs ;
	end if ;

	-- Nombre de points � lire a partir du 3eme caractere (apres '1;')
	get(Ligne(3..LLigne),NbPtsALire,last);
  Debut:=last+2;

  -- Correction eventuelle de (,) en (.) pour lecture des reels
  for i in 1..LLigne loop
    if Ligne(i)=',' then Ligne(i):='.'; end if;
  end loop;

  -- Lecture des coordonnees des points
  for i in 1..NbPtsAlire loop
            -- X
            get(Ligne(Debut..LLigne),X,last);
            Debut:=last+2;
            -- Y
            get(Ligne(Debut..LLigne),Y,last);
            Debut:=last+2;
            -- contribution a Vno et Vse
            if VnoVse=true then
              if x<Vno.coor_x then 
                vno.coor_x:=x;
              end if;
              if x>vse.coor_x then
                vse.coor_x:=x;
              end if;
              if y>vno.coor_y then 
                vno.coor_y:=y; 
              end if;
              if y<vse.coor_y then 
                vse.coor_y:=y; 
              end if;
            end if;
	          -- Stockage de la coordonnee, en format PlaGe
			  if arrondi=true then
			    -- les coordonnees sont arrondies � l'entier multiple de Arrondiviseur le plus proche
                Preel.coor_x:=float(Arrondiviseur*integer(x/float(Arrondiviseur)));
                Preel.coor_y:=float(Arrondiviseur*integer(Y/float(Arrondiviseur)));
	          else
                Preel.coor_x:=float(integer(x));
                Preel.coor_y:=float(integer(Y));
              end if;

            tab_pts(i):=Convert_coord_terrain_graphe(Preel,InvEchelle,Resolution,Minimum_terrain);
  end loop;
  if not (Last=LLigne or Last=LLigne-1) then  
  -- Dernier point ?
    raise Erreur_Nombre_De_Points ;
  end if ;

  nb_pts:=NbPtsALire;

exception when others => raise Erreur_LirePoints;
end LirePoints ;

-- ====================================================================
-- Procedure visant a analyser les coordonnees d'un fichier Geoconcept
-- (chaque coordonnee etant separee par un ;) et a extraire :
--  * le min-max des coordonnees reelles
--  * le nombre d'enregistrements du fichier
--  * le nombre maximum de points par arc

Procedure AnalyseCoord (
	Nom_fic : in string;		-- Nom du fichier de type Geoconcept
	Position : in integer;		-- Position ou trouver les coordonnees
	Minimum : out point_type_reel;	-- Minimum des coordonnees
	Maximum : out point_type_reel;	-- Maximum des coordonnees
    NB_enregistrement : out integer;-- Nombre d'enregistrement du fichier
	Nb_pts_max : out integer;	-- Nombre maximum de points par arc
	Nb_Pts_Courbes : out integer;  -- Nombre total de points des arcs
	AnalyseRoute : boolean) is

Erreur_Ligne_TropLongue,Erreur_Nombre_De_Points,
Erreur_Plusieurs_Arcs,Erreur_analysecoord  : 		exception;
Erreur_codeTypo_Inconnu,Erreur_CodeTypo_TropLong : exception;

Fic : text_io.file_type; 	-- Fichier texte

MaxLLigne : integer:=1500000;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne);	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
Chaine_coord : string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord : integer;		-- Longueur de la chaine des coordonnees
Chaine_codeTypo : string30 := (1..30 => ' '); -- Chaine contenant le code typo
LChaine_codeTypo : integer;		-- Longueur de la chaine du code typo
Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
Compteur_arc : integer :=0;		-- Compteur d'enregistrements
Nb_pts : integer;			-- Nombre de points de l'arc
Nb_pts_max_prov : integer;		-- Variable intermediaire (nb de points)
Minimum_prov, Maximum_prov : point_type_reel;-- Variable intermediaire
X,Y : float;				-- Coordonnees des points
last,Debut : positive;			-- indice de chaine
-- last,Debut : integer;			-- indice de chaine
no_lig: text_io.count;
NomCote : string(1..NbMaxMots*NBRE_CAR_NOM):=(others=>' ');
NcarNomCote : integer;
info_font: typtfont(1..500);
nbfont : integer;
trouve : boolean;


begin
  -- Ouverture du fichier structure Geoconcept
  Open(Fic,in_file,Nom_fic);

  -- Initialisation diverse pour trouver la boite englobante
  Minimum_prov:=(float'last,float'last);
  Maximum_prov:=(-float'last,-float'last);          
  Nb_Pts_max_prov :=0;  
  Compteur_arc:=0;
  Nb_Pts_Courbes:=0;

  charge_fontes(info_font,nbfont);

  -- Boucle sur les enregistrements du fichier
  loop
     if end_of_file(Fic) then
        exit;
     end if;

     loop
        begin
          no_lig:=line(fic);
          -- Ligne(1..MaxLLigne):=(others=>' ');
          get_line(Fic,Ligne,LLigne);
        exception when END_ERROR =>
          close(fic);
          Open(Fic,in_file,Nom_fic);
          set_line(fic, no_lig);
          LLigne:=0;
          while (not End_of_file(fic)) and (LLigne<MaxLLigne) loop
            LLigne:=LLigne+1;
            get(fic, Ligne(LLigne));
          end loop;
        end;
        if LLigne=MaxLLigne then 
	          raise Erreur_Ligne_TropLongue ;
        end if ;
        if Ligne(1)/='\' then 
          compteur_arc:=compteur_arc+1;
		  exit;
		end if;  -- commentaire dans les fichiers
     end loop;
     
     -- Deplacement dans la chaine de string pour trouver toutes 
     -- les tabulations ou la fin de ligne;
     Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);

     -- la liste de points doit se trouver entre Position et Position+1 
     -- si position+1 n'est pas la derniere tab !!!!
     -- tabulation
     Lchaine_coord:=Ptr_indice(position+1)-Ptr_indice(position)-1;
     if Lchaine_coord=0 then
        -- Il n'y a aucun point
        Nb_pts:=0;
     else
        Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(position)+1..Ptr_indice(position+1)-1);

        -- Lecture de la structure de points (coordonnees separeees par des ;)
        -- pour determiner les maximum et minimum :
        -- * lecture du nombre d'arcs de l'objet
        if Chaine_coord(1..2) /= "1;" then
          Msg_Erreur:=To_Unbounded_String(
            "Erreur de lecture:"&EOL
            &"Fichier: "&Nom_fic(1..Nom_fic'last)&EOL
            &"Ligne n� "&Integer'Image(integer(no_lig)));
          Gr_free_liens_array(Ptr_indice);
          raise Erreur_Plusieurs_Arcs ;
        end if;

        -- * Nombre de points � lire a partir du 3eme caractere
        -----------------------------------------------------
        get(Chaine_coord(3..Lchaine_coord),Nb_pts,last);
        Debut:=last+2;

        -- * Correction eventuelle de la virgule en point pour lecture des reels
        for i in 1..Lchaine_coord loop
          if Chaine_coord(i)=',' then
            Chaine_coord(i):='.';
          end if;
        end loop;

        -- * Lecture des coordonnees des points
        ------------------------------------------
        for i in 1..Nb_pts loop
          -- X
          get(Chaine_coord(Debut..Lchaine_coord),X,last);
          Debut:=last+2;
          -- Y
          get(Chaine_coord(Debut..LChaine_coord),Y,last);
          Debut:=last+2;
          -- Determination de la boite englobante de l'arc
          if X < Minimum_prov.coor_x then Minimum_prov.coor_x := X ; end if ;
          if Y < Minimum_prov.coor_y then Minimum_prov.coor_y := Y ; end if ;
	      if Maximum_prov.coor_x < X then Maximum_prov.coor_x := X ; end if ;
	      if Maximum_prov.coor_y < Y then Maximum_prov.coor_y := Y ; end if ;
        end loop;
        if not (Last=Lchaine_coord or Last=Lchaine_coord-1) then
 	     -- Dernier point ?
          raise Erreur_Nombre_De_Points;
        end if ; 
     end if;

     -- Mise a jour du nombre maximum de points par arc
     if Nb_pts > Nb_pts_Max_prov then Nb_pts_Max_prov:= Nb_pts; end if;

     if analyseRoute then
       if Ptr_indice(3)/=LLigne+1 then -- troncon portant un toponyme � placer
	   	
	   	 -- v�rification de la validit� du code typo
         Lchaine_CodeTypo:=Ptr_indice(5)-Ptr_indice(4)-1;
	     if Lchaine_CodeTypo>Chaine_codeTypo'last then
           raise Erreur_CodeTypo_TropLong;
         end if;
	     Chaine_codeTypo := (1..30 => ' ');
         Chaine_codeTypo(1..Lchaine_codeTypo):=Ligne(Ptr_indice(4)+1..Ptr_indice(5)-1);
	     trouve:=false;
         for i in 1..nbfont loop
           if info_font(i).code = Chaine_codeTypo then
             trouve:=true;
             exit;
           end if;
         end loop;    
         if trouve=false then
           raise Erreur_codeTypo_Inconnu;
         end if;

         -- comptage des vertex de cotes de courbes
         ncarNomCote:=Ptr_indice(4)-Ptr_indice(3)-1;
         nomCote(1..ncarNomCote):=Ligne(Ptr_indice(3)+1..Ptr_indice(4)-1);
         for J in 1..ncarNomCote loop
           if not((NomCote(J) in '0'..'9') or (NomCote(J)=',') or (NomCote(J)='.')) then
             exit;
           end if;
           if j=ncarNomCote then
             Nb_Pts_Courbes:=Nb_Pts_Courbes+Nb_Pts;
           end if;
         end loop;
       end if;
     end if;

     Gr_free_liens_array(Ptr_indice);
  end loop;

  -- affectation des variables en sortie:
  Minimum:=Minimum_prov;
  Maximum:=Maximum_prov;
  Nb_enregistrement:=compteur_arc;
  Nb_pts_max:=Nb_pts_max_prov;

  -- Fermeture du fichier en entree
  close(Fic);

exception 
   when Erreur_Plusieurs_Arcs =>
   	 close(Fic);
     raise;                            
   when Erreur_codeTypo_Inconnu =>
	   Msg_Erreur:=to_unbounded_string("Le code typo """&Chaine_codeTypo(1..Lchaine_codeTypo)&""" est inconnu."&eol&
									   "Il est invoqu� � la ligne"&Integer'Image(integer(no_lig))&" du fichier des donn�es lin�aires"&eol&
									   "mais il est absent du fichier des codes typo."&eol&
									   "Il se peut qu'il y en ait d'autres dans ce cas.");
   	 raise;
   when Erreur_CodeTypo_TropLong =>
	   Msg_Erreur:=to_unbounded_string("Le nom du code typo pr�sent � la ligne"&Integer'Image(integer(no_lig))&" du fichier des donn�es lin�aires n'est pas valide."&eol&
									   "Le nombre de caract�res qu'il comporte est sup�rieur aux"&Integer'Image(integer(Chaine_codeTypo'last))&" autoris�s."&eol&
									   "Il se peut qu'il y en ait d'autres dans ce cas.");
   	 raise;   	
   when others => 
     close(Fic);
--     if Msg_Erreur=Null_Unbounded_string then
	    Msg_Erreur:=To_Unbounded_String("Erreur de lecture:"&EOL
                                       &"Fichier: "&Nom_fic(1..Nom_fic'last)&eol
                                       &"Ligne n� "&Integer'Image(integer(no_lig)));
--     end if;
     raise;
end AnalyseCoord;


-- ====================================================================
-- Procedure visant a analyser les coordonnees du fichier des ecritures
-- (chaque coordonnee etant separee par un ;) et a extraire :
--  * le min-max des coordonnees reelles des pts d'accroche et des arcs
--  * le nombre d'enregistrements du fichier
--  * le nombre maximum de points par arc
     
Procedure AnalyseCoordNoms(
	Nom_fic : in string;		-- Nom du fichier des ecritures
	Position : in integer;		-- Position ou trouver les coordonnees
	Minimum : out point_type_reel;	-- Minimum des coordonnees
	Maximum : out point_type_reel;	-- Maximum des coordonnees
    NB_enregistrement : out integer;-- Nombre d'enregistrement du fichier
	Nb_pts_max : out integer;	-- Nombre maximum de points par arc
	NbLignes : out integer) is

Erreur_Ligne_TropLongue,Erreur_Nombre_De_Points,
Erreur_Plusieurs_Arcs,Erreur_analysecoord : exception;
Erreur_codeTypo_Inconnu,Erreur_CodeTypo_TropLong : exception;
Fic : text_io.file_type;

MaxLLigne : integer:=1500000;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne);	-- Ligne du fichier texte
LLigne : integer:=0;		-- Longueur de la ligne
Chaine_coord : string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord : integer;		-- Longueur de la chaine des coordonnees
Chaine_codeTypo : string30 := (1..30 => ' '); -- Chaine contenant le code typo
LChaine_codeTypo : integer;		-- Longueur de la chaine du code typo
Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs

Compteur_arc : integer :=0;		-- Compteur d'enregistrements
Nb_pts : integer;			-- Nombre de points de l'arc
Nb_pts_max_prov : integer;		-- Variable intermediaire (nb de points)
Minimum_prov, Maximum_prov : point_type_reel;-- Variable intermediaire
X,Y : float;				-- Coordonnees des points
last,Debut : positive ;			-- indice de chaine
no_lig: text_io.count:=1;
info_font: typtfont(1..500);
nbfont : integer;
trouve : boolean;

begin
  -- Ouverture du fichier
  Open(Fic,in_file,Nom_fic);
  -- Initialisations diverses pour trouver la boite englobante
  Minimum_prov:=(float'last,float'last);
  Maximum_prov:=(-float'last,-float'last);          
  Nb_Pts_max_prov :=0;  
  Compteur_arc:=0;

  charge_fontes(info_font,nbfont);

  -- Boucle sur les enregistrements du fichier
  loop
     if end_of_file(Fic) then
        exit;
     end if;

     loop
        begin
          no_lig:=line(fic);
          -- Ligne(1..MaxLLigne):=(others=>' ');
          get_line(Fic,Ligne,LLigne);
        exception when END_ERROR =>
          close(fic);
          Open(Fic,in_file,Nom_fic);
          set_line(fic, no_lig);
          LLigne:=0;
          while (not End_of_file(fic)) and ( LLigne<MaxLLigne ) loop
            LLigne:=LLigne+1;
            get(fic, Ligne(LLigne));
          end loop;
        end;
        if LLigne=MaxLLigne then 
	          raise Erreur_Ligne_TropLongue ;
         end if ;
         if Ligne(1)/='\' then   -- pas un point relief (un point relief ne sert qu'au placement des cotes de courbes)
           compteur_arc:=compteur_arc+1;
		   exit;
		 end if;

         if end_of_file(Fic) then
           exit;
         end if;
     end loop;

     if end_of_file(Fic) and Ligne(1)='\' then
       exit;
     end if;
     
     -- Deplacement dans la chaine de string pour trouver toutes 
     -- les tabulations ou la fin de ligne;
     Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);

     -- traitement du point d'accroche
     -- Correction eventuelle de la virgule en point pour lecture des reels
     
     for i in Ptr_indice(2)+1..Ptr_indice(3)-1 loop
       if Ligne(i)=',' then Ligne(i):='.'; end if;
     end loop;
     -- X
     get(Ligne(Ptr_indice(2)+1..Ptr_indice(3)-1),X,last);
     -- Y
     get(Ligne(last+2..Ptr_indice(3)-1),Y,last);
     if X < Minimum_prov.coor_x then Minimum_prov.coor_x := X ; end if ;
     if Y < Minimum_prov.coor_y then Minimum_prov.coor_y := Y ; end if ;
	 if Maximum_prov.coor_x < X then Maximum_prov.coor_x := X ; end if ;
	 if Maximum_prov.coor_y < Y then Maximum_prov.coor_y := Y ; end if ; 
     
     -- la liste de points doit se trouver entre Position et Position+1 
     -- si position+1 n'est pas la derniere tab !!!!
     -- tabulation
     Lchaine_coord:=Ptr_indice(position+1)-Ptr_indice(position)-1;
     
     if Lchaine_coord=0 then
        -- Il n'y a aucun point
        Nb_pts:=0;
--        Gr_free_liens_array(Ptr_indice);
     else
        Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(position)+1..Ptr_indice(position+1)-1);
        -- Lecture de la structure de points (coordonnees separeees par des ;)
        -- pour determiner les maximum et minimum :
        -- * lecture du nombre d'arcs de l'objet
        if Chaine_coord(1..2) /= "1;" then
           -- anomalie de donnees d'entree : il y a plusieurs arcs
          Msg_Erreur:=To_Unbounded_String(
            "Erreur de lecture:"&EOL
            &"Fichier: "&Nom_fic(1..Nom_fic'last)&EOL
            &"Toponyme: "&Ligne(1..Ptr_indice(2)-1));
          Gr_free_liens_array(Ptr_indice);
          raise Erreur_Plusieurs_Arcs ;
--        else
--          Gr_free_liens_array(Ptr_indice);
        end if ;
        
        -- * Nombre de points � lire a partir du 3eme caractere
        -----------------------------------------------------
        get(Chaine_coord(3..Lchaine_coord),Nb_pts,last);
        Debut:=last+2;

        -- * Correction eventuelle de la virgule en point pour lecture des reels
        for i in 1..Lchaine_coord loop
            if Chaine_coord(i)=',' then
               Chaine_coord(i):='.';
            end if;
        end loop;

        -- * Lecture des coordonnees des points
        ------------------------------------------
        for i in 1..Nb_pts loop
            -- X
            get(Chaine_coord(Debut..Lchaine_coord),X,last);
            Debut:=last+2;
            -- Y
            get(Chaine_coord(Debut..LChaine_coord),Y,last);
            Debut:=last+2;
            -- Determination de la boite englobante de l'arc
            if X < Minimum_prov.coor_x then Minimum_prov.coor_x := X ; end if ;
            if Y < Minimum_prov.coor_y then Minimum_prov.coor_y := Y ; end if ;
	          if Maximum_prov.coor_x < X then Maximum_prov.coor_x := X ; end if ;
	          if Maximum_prov.coor_y < Y then Maximum_prov.coor_y := Y ; end if ;
        end loop;
        if not (Last=Lchaine_coord or Last=Lchaine_coord-1) then
 	   -- Dernier point ?
          raise Erreur_Nombre_De_Points;
        end if ; 
     end if;

     -- Mise a jour du nombre maximum de points par arc
     if Nb_pts > Nb_pts_Max_prov then Nb_pts_Max_prov:= Nb_pts; end if;

	 -- v�rification de la validit� du code typo
     Lchaine_CodeTypo:=Ptr_indice(4)-Ptr_indice(3)-1;
	 if Lchaine_CodeTypo>Chaine_codeTypo'last then
       raise Erreur_CodeTypo_TropLong;
     end if;
	 Chaine_codeTypo := (1..30 => ' ');
     Chaine_codeTypo(1..Lchaine_codeTypo):=Ligne(Ptr_indice(3)+1..Ptr_indice(4)-1);
	 trouve:=false;
     for i in 1..nbfont loop
       if info_font(i).code = Chaine_codeTypo then
         trouve:=true;
         exit;
       end if;
     end loop;    
     if trouve=false then
       raise Erreur_codeTypo_Inconnu;
     end if;
     Gr_free_liens_array(Ptr_indice);
  end loop;

  -- affectation des variables en sortie:
  Minimum:=Minimum_prov;
  Maximum:=Maximum_prov;
  Nb_enregistrement:=compteur_arc;
  Nb_pts_max:=Nb_pts_max_prov;

  -- Fermeture du fichier en entree
  close(Fic);
  NbLignes:=integer(No_Lig);

exception 
   when Erreur_Plusieurs_Arcs =>
   	 close(Fic);
     raise;
   when Erreur_codeTypo_Inconnu =>
     close(Fic);
	 Msg_Erreur:=to_unbounded_string("Le code typo """&Chaine_codeTypo(1..Lchaine_codeTypo)&""" est inconnu."&eol&
									   "Il est invoqu� � la ligne"&Integer'Image(integer(no_lig))&" du fichier des �critures horizontales"&eol&
									   "mais il est absent du fichier des codes typo."&eol&
									   "Il se peut qu'il y en ait d'autres dans ce cas.");
   	 raise;
   when Erreur_CodeTypo_TropLong =>
     close(Fic);
	 Msg_Erreur:=to_unbounded_string("Le nom du code typo pr�sent � la ligne"&Integer'Image(integer(no_lig))&" du fichier des �critures horizontales n'est pas valide."&eol&
									   "Le nombre de caract�res qu'il comporte est sup�rieur aux"&Integer'Image(integer(Chaine_codeTypo'last))&" autoris�s."&eol&
									   "Il se peut qu'il y en ait d'autres dans ce cas.");
   	 raise;   	
   when others => 
     close(Fic);
     -- if Msg_Erreur=null_unbounded_string then
	   Msg_Erreur:=To_Unbounded_String("Erreur de lecture:"&EOL
                                      &"Fichier: "&Nom_fic(1..Nom_fic'last)&eol
                                      &"Ligne n� "&Integer'Image(integer(no_lig)));
     -- end if;
     raise;
end AnalyseCoordNoms;

-- ====================================================================
-- Fonction de conversion d'un Point_type en coordonnees reelles terrain
-- (metres) en un point_type en coordonnees entieres Plage (points), en fonction
--   - des coord. min sur le terrain
--   - de l'inverse de l'echelle (par ex 25000)
--   - de la resolution (nb de points par millimetres a l'echelle finale)

Function Convert_coord_terrain_graphe(Point: Point_type_reel;
                                      InvEchelle: positive;
                                      Resolution: positive;
                                      Minimum_terrain: Point_type_reel
                                      ) return Point_type is
M : Point_type;
begin
  M.Coor_x := Integer(((Point.Coor_x - Minimum_terrain.Coor_x) 
                        * Float(Resolution)) / (Float(InvEchelle) / 1000.0));
  M.Coor_y := Integer(((Point.Coor_y - Minimum_terrain.Coor_y) 
                        * Float(Resolution)) / (Float(InvEchelle) / 1000.0));
  Return M;
End Convert_coord_terrain_graphe;


-- ====================================================================
-- Fonction de conversion d'un Point_type en coordonnees entieres Plage (points)
-- en un point_type en coordonnees reelles terrain (metres), en fonction
--   - des coord. min sur le terrain
--   - de l'inverse de l'echelle (par ex 25000)
--   - de la resolution (nb de points par millimetres a l'echelle finale)

Function Convert_coord_graphe_terrain( Point   	: Point_type;
                                       InvEchelle      : positive;
                                       Resolution      : positive;
                                       Minimum_terrain : Point_type_reel
                                      ) return Point_type_reel is
M : Point_type_reel:=(0.0,0.0);
begin
  M.Coor_x := Minimum_terrain.coor_x
                       + (Float(Point.coor_x)/Float(Resolution)) * (Float(InvEchelle)/float(1000));                     
  M.Coor_y := Minimum_terrain.coor_y
                       + (Float(Point.coor_y)/Float(Resolution)) * (Float(InvEchelle)/Float(1000));                     
	Return M;
End Convert_coord_graphe_terrain;


-- ==========================================================================
-- Procedure de lecture du fichier des routes pour creer le graphe des routes
Procedure LireRoutes(NOM_Routes : in string;
                     NOM_GRAPHE : in string;
                     InvEchelle : in positive;
                     pts_par_mm : in positive;
                     Minimum_terrain : in point_type_reel;
                     Maximum_terrain : in point_type_reel;
                     Nb_Arcs_a_lire : integer;
                     Nb_Arcs_charges : in out integer;
                     Nb_pts_routes : in integer;
                     TAN : in out TARC_NOM;
					 NbNom : in integer;
					 Nb_pts_courbes : in integer) is

Chaine_theme_TropLongue,Plusieurs_Noeuds_Confondus  : exception;

Fic_routes : text_io.file_type;		-- Fichier texte des routes
Tab_pts : point_liste_type(1..Nb_pts_routes);

MaxLLigne : integer:=1500000;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne);	-- Ligne du fichier texte des routes
LLigne : integer;		-- Longueur de la ligne
Ptr_indice : Liens_access_type	;	-- Pointeur des indices des separateurs

Ligne2 : string (1..MaxLLigne);	-- Ecriture
Ptr_indice2 : Liens_access_type	;	-- Pointeur des indices des separateurs
Ncar2 : integer;

-- debut modif 30.06.2005
-- MaxTheme : integer :=300;
MaxTheme : integer :=1000;
-- fin modif 30.06.2005

Longueur_champs_theme : integer:=30;
TabLegPres : array(1..Maxtheme) of string(1..Longueur_champs_theme); -- Tableau des legendes presentes

Tab_largeur : array(1..Maxtheme) of float:=(others => -1.0);

Chaine_coord : string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord : integer;		-- Longueur de la chaine des coordonnees
Chaine_theme : string(1..Longueur_champs_theme);
Lchaine_theme : integer;
Chaine_largeur : string(1..50);
Lchaine_largeur : integer;
last : integer;
no_lig: text_io.count:=0;
Nb_pts : integer;

indice_legende : integer;
indice_legende_store : integer;
affichage : affichage_type;
Laffichthem : integer := affichage.theme'length;

Emprise : point_type;

Flag_existence : boolean;

nombre_arcs_ecrits,Numarc : integer;
NumNoeudI, NumnoeudF : integer;
NNoeudsProches : integer;
TabNoeudsProches : liens_array_type(1..10);

--Metrique : metrique_type;
OV : short_short_integer;
-- Ov stocke la valeur a affecter a l'attribut octet_vms des arcs du graphe
-- Cet attribut sert est utile a la representation sous Viewer des objets lineaires a nommer
-- Ov=0 : arc non represent� car ne portant pas de nom � placer
-- Ov=1 : arc de route
-- Ov=2 : arc de courbe de niveau dont la cote n'est pas � placer mais utile pour l'orientation des cotes � placer (cas ou les arcs de courbe de niveau ne sont pas orient�s 'amont � droite')
-- Ov=3 : arc de courbe de niveau dont la cote est � placer 
-- Ov=4 : arc hydro

arc : arc_type;

NbLegInc : integer:=0;  -- Nombre de noms de legende distincts inconnus (non d�clar�s dans un groupe)
NbLegNumRouInc : integer:=0;  -- Nombre de noms de legende distincts inconnus (non d�clar�s dans un groupe) pour les arcs qui portent un numero de route
k : integer;

NomCote : string(1..NBRE_CAR_NOM):=(others=>' ');

cadre_image : Point_liste_type(1..5);
Lres : Point_Access_type;
n : integer;
dedans,dedans2 : boolean:=true;
TTab_pts : point_liste_type(1..Nb_pts_routes);
NNb_Pts : integer;
C_TTab_pts : integer;
NbNoeudsMax	: integer;


NumeroDeRoute : boolean;
NumNoeud : integer;
a,modA,Cote,cote_mod_modA,A_mod_modA : float; 
Mes : Unbounded_String:= Null_Unbounded_String;
LegNoRouCon : boolean:=false; -- existence d'au moins une l�gende d'arc de route d�clar�e dans un groupe

nbl : integer:=NbLegendes;
esc : boolean;

--Correction FL du 30/08/2012 : D�clarations ajout�es
Reprise : Point_type;
n2 : integer;
Lres2 : Point_Access_type;
Portion_ligne : point_liste_type(1..2);
Sortie : boolean :=False;
--Correction FL du 30/08/2012 : Fin D�clarations ajout�es


-- partie entiere
function E(x : in float) return float is
begin
  if float(integer(x))<=x then
    return(float(integer(x)));
  else
    return(float(integer(x)-1));
  end if;
end E;

Begin
  -- Lecture du fichier des routes pour creer le graphe
  ------------------------------------------------
  Open(Fic_routes,in_file,NOM_Routes);

  -- Creation du graphe
  Emprise:=Convert_coord_terrain_graphe(Maximum_terrain,InvEchelle,Pts_par_mm,Minimum_terrain);
  begin
    if GrrExiste then
	  gr_Delete(Grr);
	end if;
    grr:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,Invechelle,Nb_Arcs_charges,2*Nb_Arcs_charges,1,Nom_graphe);
    GrrExiste:=True;
  exception when  Event : others =>
    GR_DELETE(grr);
    grr:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,Invechelle,Nb_Arcs_charges,2*Nb_Arcs_charges,1,Nom_graphe);
  end;

  begin
    if GrAltiExiste then
	  gr_Delete(GrAlti);
    end if;
    NbNoeudsMax:=integer(1.1*float(Nb_pts_courbes))+2*NbNom+1;
	-- facteur 1.1 en prevision du clipage par l'image
	-- facteur 2 car dans le cas ou une image de mutilation est creee, les cotes ponctuelles passent 2 fois dans GrAlti
    grAlti:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,InvEchelle,1,NbNoeudsMax,1,Nom_Graphe&"-Alti");
    GrAltiExiste:=True;
  exception when  Event : others =>
    GR_DELETE(grAlti);
    grAlti:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,InvEchelle,1,NbNoeudsMax,1,Nom_Graphe&"-Alti");
  end;

  -- Lecture des arcs 
  if NbLegendes=0 then
    NbGroupes:=0;
  end if;

  if mutil then
    cadre_image:=(Pno,Pne,Pse,pso,Pno);
  end if;

  nombre_arcs_ecrits:=0;
  for i in 1..Nb_Arcs_A_Lire loop

    if 300*(I-1)/Nb_Arcs_A_Lire /= 300*i/Nb_Arcs_A_Lire or i=Nb_Arcs_A_Lire then		
      gb.Move(dialog3.Panel2,0,0,gb.INT(300*i/Nb_Arcs_A_Lire),20);
    end if;

      loop

        begin
          no_lig:=line(fic_routes);
          -- Ligne(1..MaxLLigne):=(others=>' ');
          get_line(Fic_routes,Ligne,LLigne);
        exception when END_ERROR =>
          close(fic_routes);
          Open(Fic_routes,in_file,NOM_Routes);
          set_line(fic_routes, no_lig);
          LLigne:=0;
          while (not End_of_file(fic_routes)) and (LLigne<MaxLLigne) loop
            LLigne:=LLigne+1;
            get(fic_routes, Ligne(LLigne));
          end loop;
        end;
        if ligne(1)/='\' then
		  exit;
		end if;
      end loop;

      -- Deplacement dans la chaine de string pour trouver toutes 
      -- les tabulations ou la fin de ligne;
      Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);

      -- lecture des informations attributaires (theme de l'arc 
      -- et largeur si necessaire)
      Lchaine_theme:=Ptr_indice(2)-Ptr_indice(1)-1;
      if Lchaine_theme>Longueur_champs_theme then
         Gr_free_liens_array(Ptr_indice);
         raise Chaine_theme_TropLongue;
      else
         Chaine_theme(1..Longueur_champs_theme):=(others=>' ');
      end if;
      Chaine_theme(1..Lchaine_theme):=Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);

      -- verification de l'existence de ce theme (gestion de la legende PlaGe)
      -- s'il existe deja, on recupere son numero
      -- s'il n'existe pas on l'ajoute dans le tableau Tab_theme

      k:=0;
      flag_existence:=false;
      for j in 1..nbLegendes loop
		if TabLegendes(j).ncar=Lchaine_theme then
          if TabLegendes(j).nom(1..TabLegendes(j).ncar)=Chaine_theme(1..Lchaine_theme) then
            indice_legende:=j;
            if Tab_largeur(J)=-1.0 then
              TabLegPres(J):=Chaine_theme;
              Lchaine_largeur:=Ptr_indice(1)-1;
              Chaine_largeur:=(others => ' ');
              Chaine_largeur(1..Lchaine_largeur):=Ligne(1..Ptr_indice(1)-1);
              get(Chaine_largeur,Tab_largeur(J),last);
            end if;
            exit;
          end if;
        end if;
		k:=j;
      end loop;
      if k=NbLegendes then
        for j in 1..NbLegInc loop
          if TabLegPres(NbLegendes+j)=Chaine_theme then
            indice_legende:=Nblegendes+j;
            flag_existence:=true;
            exit;
          end if;
        end loop;

        if flag_existence=false then
          NbLegInc:=NbLegInc+1;
          TabLegPres(NbLegendes+NbLegInc):=Chaine_theme;
          -- Stockage de la largeur pour ce theme
          Lchaine_largeur:=Ptr_indice(1)-1;
		  Chaine_largeur:=(others => ' ');
          Chaine_largeur(1..Lchaine_largeur):=Ligne(1..Ptr_indice(1)-1);
          get(Chaine_largeur,Tab_largeur(NbLegendes+NbLegInc),last);
          indice_legende:=NbLegendes+NbLegInc;
          TabLegendes(NbLegendes+NbLegInc).NumGroupe:=NbGroupes+NbLegInc;
        end if;
      end if;

      indice_legende_store:=indice_legende;

      -- la liste de points doit se trouver entre la 2eme et 3eme tabulation
      Lchaine_coord:=Ptr_indice(3)-Ptr_indice(2)-1;
      Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(2)+1..Ptr_indice(3)-1);

      -- Lecture de la structure de points (coordonnees separeees par des ;)
      LirePoints(Chaine_coord,Lchaine_coord,InvEchelle,Pts_par_mm,Minimum_terrain,TTab_pts,NNb_pts,true,true);

      if (NNb_pts < 2) or (NNb_pts=2 and TTab_pts(1)=TTab_pts(2)) then
        -- on ne fait rien pour cet arc incoherent
        null;
      else

	    -- debut clipage par l'image
        N:=0;
        if mutil then	
	      Intersections_de_polylignes(TTab_pts,cadre_image,NNb_pts,5,Lres,N,true);
		  if est_il_dans_rectangle(TTab_pts(1),Pno,Pne,Pse,Pso)=true then
		    dedans:=true;
	      else
		    dedans:=false;
          end if;

          if Tab_largeur(indice_legende)=0.0 then
		    if N=0 and dedans=false then  -- surface en-dehors de l'image
              n:=-1;
            end if;
            if N/=0 then -- surface a cheval sur le bord de l'image
              dedans:=true;
              n:=0;
            end if;
          end if;
        end if;

		c_TTab_pts:=1;
       
		--Correction FL du 30/08/2012 : Modification pour prise en compte correcte des arcs qui intersectent le cadre de l'image sur un segment
	    Sortie:=False;
        for p in 1..N+1 loop
		 if Sortie=False then
          Nb_Pts:=1;
		  if p=1 then
            Tab_pts(1):=TTab_pts(1);
            c_TTab_pts:=c_TTab_pts+1;
	      else
			Tab_Pts(1):=Reprise;

            if dedans=true then
              dedans2:=false;
            else
              dedans2:=true;
            end if;
		    dedans:=dedans2;
          end if;

		  
          while C_TTab_pts<NNb_Pts and (est_il_dans_rectangle(TTab_pts(c_TTab_pts),Pno,Pne,Pse,Pso)=dedans or Tab_largeur(indice_legende_store)=0.0) loop
            -- Boucle surtout les points de la ligne (sauf le dernier point) qui sont du m�me cot� de l'image   
			Nb_Pts:=Nb_Pts+1;
            Tab_pts(Nb_pts):=TTab_pts(c_TTab_pts);
            c_TTab_pts:=c_TTab_pts+1;
          end loop;
          
		  -- Ici on arrive :
		  --     * soit au dernier point de la ligne � �tudier, 
		  --     * soit � un point de v�ritable changementde cot� (dedans/dehors de la zone en sachant qu'un point sur la limite est consid�r� comme dedans) 

          -- on regarde en d�tail ce ce qu'il se passe avec ce point suivant TTab_Pts (nouveau C_TTab_Pts) du dernier point ajout� dans la boucle while
	  	  Portion_ligne(1):=Tab_pts(Nb_Pts);
		  Portion_ligne(2):= TTab_pts(c_TTab_pts);
	      N2:=0;
          Intersections_de_polylignes(Portion_Ligne,cadre_image,2,5,Lres2,N2,FALSE);

		  If N2=1 then 
             -- on est sorti de la boucle pr�c�dente soit par arriv�e au dernier point, soit par intersection provoqu�e par le segment consitut� du point et 
			 -- du point suivant		  
			 if portion_Ligne(2)=Lres2(1) then
			 	-- le point suivant est sur la bordure et il a provoqu� le changement de cot� (et donc le passage � l'int�rieur)
				Nb_Pts:=Nb_Pts+1;
                Tab_pts(Nb_pts):=TTab_pts(c_TTab_pts);
				if C_TTab_Pts=NNb_Pts then
                   -- on est arriv� en plus en fin de ligne et on sort dans tous les cas
				   Sortie:=True;
		        else 
			   	   -- on rentre dans la zone et donc on finit la ligne en cours par ce point et on reprendra la ligne suivante de ce m�me point
			   	   reprise:=Tab_pts(Nb_pts);
			    end if;
	         else
			 	if Portion_Ligne(1)= Lres2(1) then
			 	   -- le point est sur la bordure et le point suivant provoque le changement de cot� ou la fin de la ligne (et donc le passage � l'ext�rieur)
                  if est_il_dans_rectangle(TTab_pts(C_TTab_Pts),Pno,Pne,Pse,Pso)=true then
			      	-- le dernier point reste � l'int�rieur
			       	 Nb_Pts:=Nb_Pts+1;
                     Tab_pts(Nb_pts):=TTab_pts(c_TTab_pts);
					 Sortie:=true;
			      else 
			         -- le point de suivant part � l'ext�rieur et il servira de reprise �ventuelle
					 Reprise:=TTab_Pts(C_TTab_Pts);
					 if C_TTab_Pts=NNb_Pts then
                        -- on est arriv� en plus en fin de ligne et on sort dans tous les cas
				        Sortie:=True;
				     end if;
				  end if;
	            else 
		 	       -- le point d'intersection est un nouveau point qu'il faut ajouter comme point de fin de la ligne et de d�but de la suivante 
                   Nb_Pts:=Nb_Pts+1;
				   Tab_pts(Nb_pts):=Lres2(1);
				   reprise:=Tab_pts(Nb_pts);
			    end if;
			 end if;

           else  
 		  	-- si N2=2 le segment est sur la ligne (� l'int�rieur) : la seule raison pour provoquer la sortie de la boucle while c'est qu'on est arriv� au dernier point de la ligne
            -- si N2=0 il n'y a pas d'intersection : la seule raison pour provoquer la sortie de la boucle while c'est qu'on est arriv� au dernier point de la ligne
   				Nb_Pts:=Nb_Pts+1;
                Tab_pts(Nb_pts):=TTab_pts(c_TTab_pts);
				Sortie:=True;
           end if;
			 --Correctif principal FL 30/08/2012

       -- fin clipage par l'image


          if (Nb_pts=2 and Tab_pts(1)=Tab_pts(2)) then
            goto finboucleP;		  	
	      end if;

          if dedans=false then
		    Indice_legende:=0;
	      else	  	
  		  	indice_legende:=indice_legende_store;
            
			-- Correctif  FL 30/08/2012 
		  -- On ne va stocker dans le graphe que les portions d'arc dans l'image de mutilation pour ne pas fausser les statistiques
		  -- (les portions ext�rieures ne doivent pas contenir de toponyme sur la carte)
		  -- d�calage du end if;          
		  --end if; On d�place la fin du test pour ne stocker que les arcs dans l'image de mutilation

          -- Creation des noeuds de l'arc dans le graphe
          if nombre_arcs_ecrits=0 then
            Gr_Creer_Noeud_Sur_Rien(grr, Tab_pts(1), false,0.0,1, NumNoeudI);
            if Tab_pts(1) = Tab_pts(Nb_pts) then
              NumNoeudF := NumNoeudI ;
            else			
              Gr_Creer_Noeud_Sur_Rien(grr, Tab_pts(Nb_pts),false,0.0,1,NumNoeudF);
            end if;
          else
            Gr_Noeuds_In_Rectangle(grr,Tab_pts(1),Tab_pts(1),TabNoeudsProches,NNoeudsProches);
            if NNoeudsProches=0 then
              Gr_Creer_Noeud_Sur_Rien(grr,Tab_pts(1),false,0.0,1,NumNoeudI);
            else
              NumNoeudI := TabNoeudsProches(1) ;
            end if;

            if Tab_pts(1) = Tab_pts(Nb_pts) then
              NumNoeudF := NumNoeudI ;
            else
               Gr_Noeuds_In_Rectangle(grr,Tab_pts(Nb_pts),Tab_pts(nb_pts),TabNoeudsProches,NNoeudsProches) ;
               if NNoeudsProches=0 then
			   	 Gr_Creer_Noeud_Sur_Rien(grr,Tab_pts(Nb_pts),false,0.0,1,NumNoeudF) ;
               else
                 NumNoeudF := TabNoeudsProches(1);
               end if;
            end if;
          end if;

          -- �criture de l'arc
          --------------------
          NumArc := 0;

          -- Marquage des arcs portant un numero de route et decompte des legendes inconnues de routes a nommer
		  OV:=0;
          nombre_arcs_ecrits:=nombre_arcs_ecrits+1;

          if Ptr_indice(3)/=LLigne+1 then -- troncon de route � numero

            TAN(nombre_arcs_ecrits).Marque:=false;

	        -- Correctif FL 06/09/12 : initalisation de la variable � chaine de blanc
            TAN(nombre_arcs_ecrits).code(1..30):=(1..30 => ' ');
 	        -- Fin de correctif FL 06/09/12 : suite dans LIREROUTES avec la m�me correction

            TAN(nombre_arcs_ecrits).code(1..Ptr_indice(5)-Ptr_indice(4)-1):=Ligne(Ptr_indice(4)+1..Ptr_indice(5)-1);


            case Ligne(Ptr_indice(5)+2) is
              when 'E' => if Ligne(Ptr_indice(5)+3)='s' or Ligne(Ptr_indice(5)+3)='S' then
			                TAN(nombre_arcs_ecrits).MODE_P:=Desaxe;
                          else
			                TAN(nombre_arcs_ecrits).MODE_P:=Decale;
                          end if;
              when 'e' => if Ligne(Ptr_indice(5)+3)='s' or Ligne(Ptr_indice(5)+3)='S' then
			                TAN(nombre_arcs_ecrits).MODE_P:=Desaxe;
                          else
			                TAN(nombre_arcs_ecrits).MODE_P:=Decale;
                          end if;
			  when 'X' => TAN(nombre_arcs_ecrits).MODE_P:=Axe;
	          when 'x' => TAN(nombre_arcs_ecrits).MODE_P:=Axe;
              when 'R' => TAN(nombre_arcs_ecrits).MODE_P:=droit;
              when 'r' => TAN(nombre_arcs_ecrits).MODE_P:=droit;
              when 'O' => TAN(nombre_arcs_ecrits).MODE_P:=Courbe;
              when 'o' => TAN(nombre_arcs_ecrits).MODE_P:=Courbe;
              when 'A' => TAN(nombre_arcs_ecrits).MODE_P:=CAxe;
              when 'a' => TAN(nombre_arcs_ecrits).MODE_P:=CAxe;
              when 'D' => TAN(nombre_arcs_ecrits).MODE_P:=CDesaxe;
              when 'd' => TAN(nombre_arcs_ecrits).MODE_P:=CDesaxe;
              when others => null;  -- declencher une erreur
            end case;

            if TAN(nombre_arcs_ecrits).MODE_P=Desaxe or TAN(nombre_arcs_ecrits).MODE_P=axe or TAN(nombre_arcs_ecrits).MODE_P=Droit or TAN(nombre_arcs_ecrits).MODE_P=Decale then
			  OV:=1;
            end if;
            if TAN(nombre_arcs_ecrits).MODE_P=CDesaxe or TAN(nombre_arcs_ecrits).MODE_P=Caxe then
			  OV:=4;
            end if;
            Ncar2:=Ptr_indice(4)-Ptr_indice(3)-1;
			Ligne2(1..Ncar2):=Ligne(Ptr_indice(3)+1..Ptr_indice(4)-1);

            Indice_Separateur2(Ligne2(1..Ncar2),Ncar2,' ',Ptr_Indice2,TAN(nombre_arcs_ecrits).Nb_Mots);
            if TAN(nombre_arcs_ecrits).MODE_P/=CAXE and TAN(nombre_arcs_ecrits).MODE_P/=CDESAXE then
			  Ptr_Indice2(1):=Ptr_indice2(TAN(nombre_arcs_ecrits).Nb_Mots);
              TAN(nombre_arcs_ecrits).Nb_Mots:=1;
			end if;

			for mot in 1..TAN(nombre_arcs_ecrits).Nb_Mots loop
              TAN(nombre_arcs_ecrits).ncar(mot):=Ptr_indice2(Mot)-Ptr_indice2(Mot-1)-1;
              TAN(nombre_arcs_ecrits).nom(Mot)(1..TAN(nombre_arcs_ecrits).ncar(Mot)):=Ligne2(Ptr_indice2(mot-1)+1..Ptr_indice2(mot)-1);
            end loop;

            NumeroDeRoute:=false;
            for J in 1..tAN(nombre_arcs_ecrits).ncar(1) loop
              if (tAN(nombre_arcs_ecrits).nb_mots/=1)
			  	  or (not((tAN(nombre_arcs_ecrits).Nom(1)(J) in '0'..'9') or (TAN(nombre_arcs_ecrits).Nom(1)(J)=',') or (tAN(nombre_arcs_ecrits).Nom(1)(J)='.'))) then
                NumeroDeRoute:=true;
			    exit;
              end if;

              if j=TAN(nombre_arcs_ecrits).ncar(1) then
			  	
		        if NbCotes=0 then
                  OV:=0;
                  exit;			  	
                end if;

                for k in 1..tAN(nombre_arcs_ecrits).ncar(1) loop
			      if tAN(nombre_arcs_ecrits).Nom(1)(k)/=',' then
 			  	    NomCote(k):=tAN(nombre_arcs_ecrits).Nom(1)(k);
                  else
 			  	    NomCote(k):='.';
                  end if;
                end loop;

                Cote:=float'value(NomCote(1..tAN(nombre_arcs_ecrits).ncar(1)));
                OV:=2;

                for K in 1..NbCotes loop
				  A:=float'value(TabCotes(K).Nom1(1..TabCotes(K).Ncar1));
                  ModA:=float'value(TabCotes(K).Nom2(1..TabCotes(K).Ncar2));
				  if modA/=0.0 then
				  	cote_mod_modA:=Cote-E(Cote/ModA)*modA;
					A_mod_modA:=A-E(A/ModA)*modA;
				  else
                    cote_mod_modA:=Cote;
					A_mod_modA:=A;
			      end if;
                  if cote_mod_modA = A_mod_modA and Cote/=0.0 then
                      OV:=3;
					  exit;
                  end if;
                end loop;

		  	    for k in 1..Nb_pts loop
                  Gr_Creer_Noeud_Sur_Rien(grAlti,Tab_pts(k),true,angle_type'value(NomCote(1..tAN(nombre_arcs_ecrits).ncar(1))),0,NumNoeud);
                end loop;
              end if;
            end loop;

			if NumeroDeRoute=true and k=NbLegendes and flag_existence=false and Indice_legende/=0 then
              NbLegNumRouInc:=NbLegNumRouInc+1;
              Mes := Mes & To_Unbounded_String(TAB & "- " & Chaine_theme(1..Lchaine_theme) & EOL);
            end if;

			if NumeroDeRoute=true and flag_existence=true and Indice_legende/=0 then
              LegNoRouCon:=true;
            end if;

          end if;

          if nombre_arcs_ecrits=1 then
            Gr_Creer_Arc(grr,NumNoeudI,NumNoeudF,Tab_pts,Nb_pts,Gen_io.Ligne,Indice_legende,OV,NumArc,esc);
            Gr_Region(grr,pts_par_mm*100,pts_par_mm*100);
            --Gr_Region(grr,pts_par_mm,pts_par_mm);
          else
            Gr_Creer_Arc(grr,NumNoeudI,NumNoeudF,Tab_pts,Nb_pts,Gen_io.Ligne,Indice_legende,OV,NumArc,esc);
          end if;

		  -- correction du compteur dans le cas ou l'arc n'a pas ete charge dans le graphe
		  -- (ce qui se produit quand tous les vertex de l'arc sont confondus)
		  if esc=true then
            nombre_arcs_ecrits:=nombre_arcs_ecrits-1;
	      end if;

		  -- Correctif  FL 30/08/2012        
		  -- On d�place la fin du test pour ne stocker que les arcs dans l'image de mutilation ici;
          end if;

	  	  << finBoucleP >>
		  null;
	     end if;
        end loop; -- boucle en P
		GR_Free_point_liste(Lres);
		GR_Free_point_liste(Lres2);
        --   Fin de zone du Correctif  FL 30/08/2012

      end if;
      Gr_free_liens_array(Ptr_indice);
  end loop;

  Nb_Arcs_charges:=nombre_arcs_ecrits; 

  if NbGroupes=0 then 
    NbGroupes:=1;
  end if;

  if (option=enchaine or option=calcul) and (NbLegNumRouInc>=2 or (NbLegNumRouInc=1 and LegNoRouCon=true)) and (StrNumRou(1)='O' or StrNumRou(1)='o') then
    if NbLegNumRouInc=1 then
	  gb.msgBox("Le symbole lin�aire suivant"&eol&"ne figure dans aucun groupe :"&eol&eol
	            &to_string(mes)&eol&"Il sera consid�r� comme appartenant     "&eol&"� un nouveau groupe distinct",
				" Fichier des donn�es lin�aires",Win32.WinUser.MB_ICONEXCLAMATION);
    else
      gb.msgBox("Les"&integer'image(NbLegNumRouInc)&" symboles lin�aires suivants "&eol&"ne figurent dans aucun groupe :"&eol&eol
	            &to_string(mes)&eol&"Ils seront consid�r�s comme appartenant     "&eol&"� autant de nouveaux groupes distincts",
				" Fichier des donn�es lin�aires",Win32.WinUser.MB_ICONEXCLAMATION);
	end if;
  end if;

  -- Creation de la legende associee a ce graphe PlaGe
  legr:= GR_create_new_legende(nbLegendes+NbLegInc,10,10);

  -- legende lineaire
  for i in 1..nbLegendes+NbLegInc loop
    affichage.largeur:=Tab_largeur(i);
    affichage.rouge:=1.0 - float(i-1)/float(nbLegendes+NbLegInc);
    affichage.vert:= float(nbLegendes-i)/float(nbLegendes+NbLegInc);
    affichage.bleu:= float(i)/float(nbLegendes+NbLegInc);
    affichage.larg_int:=Tab_largeur(i)-0.2;
    affichage.theme(1..Laffichthem):=TabLegPres(i)(1..Laffichthem);
    affichage.priorite:=1;
    GR_MOD_LEGENDE_L(legr,i,affichage);
  end loop;

  -- legende ponctuel
  affichage.largeur:=1.0;
  affichage.rouge:=1.0;
  affichage.vert:= 1.0;
  affichage.bleu:= 1.0;
  affichage.larg_int:=0.0;
  affichage.priorite:=1;
  GR_MOD_LEGENDE_p(legr,1,affichage);
 
  close(fic_routes);

exception when  Event : others =>
  close(fic_routes);    
  Msg_Erreur:=
    To_Unbounded_String(
      "Erreur de cr�ation du graphe des donn�es lin�aires"&eol
      &"Fichier: "&NOM_Routes&EOL
      &"Ligne n� "&Integer'Image(integer(no_lig)));
--  close(fic_routes);    
  raise;
end LireRoutes;


-- ======================================================================
-- Procedure de lecture du fichier des ecritures pour creer le graphe associe
-- cas o� il existe un fichier de symboles d'appui (appui=true)

procedure LireEcritures(NOM_Ecritures : in string ;
                        NOM_GRAPHE : in string ;
        			    InvEchelle : in positive ;
        			    pts_par_mm : in positive;
        			    Minimum_terrain : in point_type_reel;
        				Maximum_terrain : in point_type_reel;
        				Nb_ecritures : in integer;
        			    Nb_pts_ecritures : in integer;
        				Tab_symboles : symbole_liste_type;
						Nb_symboles : integer ;
                        tbnomi : in out typtbnomi) is


Erreur_coherence,Chaine_theme_TropLongue, erreur_placement,
Table_symboles_incomplete,Pas_de_point_d_accroche  : exception;

Fic_ecritures : text_io.file_type;		-- Fichier texte des ecritures
Tab_pts : point_liste_type(1..2*Nb_pts_ecritures);-- Tableau de points 
						-- decrivant les contours

MaxLLigne: integer:=1500000;	-- Longueur maximale des lignes du fichier texte
Ligne 	 : string (1..MaxLLigne);-- Ligne du fichier texte des routes
LLigne   : integer;		-- Longueur de la ligne
Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
Itab 	   : integer;			-- Indice de lecture des tabulations

Longueur_champs_theme : integer:=30;

Chaine_coord 	: string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord 	: integer;		-- Longueur de la chaine des coordonnees
Chaine_theme 	: string(1..Longueur_champs_theme);
Lchaine_theme 	: integer;

Chaine 		: string(1..50);	-- Chaine de lecture intermediaire
Lchaine : integer;			-- Longueur de la chaine
last,last1 : integer;			-- Variable local

Orientation	: float;		-- Orientation du symbole
proportion	: float;		-- Proportion du symboles
Diametre 	: float;		-- Diametre du symbole circulaire

Nb_pts 		: integer;		--
Nb_diametre	: integer;		-- Nombre de diametre de symboles circulaires
indice_diametre : integer;		-- Indice de diametre trouve
Tab_diametre 	: array(1..Nb_symboles) of float;-- Tableau des diametres

Emprise 	: point_type;		-- Emprise du graphe

Pt_accroche 	: point_type_reel;	-- Coordonnees du pt d'accroche (en m)
Pt_accroche_graphe : point_type;	-- Coord. du pt d'accroche dans graphe

indice_symbole 	: integer;		-- Indice dans la table des symboles

affichage 	: affichage_type;	-- variables intermediaires diverses
x1,y1,x2,y2 	: float;
nb_pts_symb 	: integer;
NumNoeudI,Numarc,Numface : integer;
Tabarc 		: liens_array_type(1..1);
Flag_existence 	: boolean;
car : string(1..20);
ncar : integer;
no_lig: text_io.count;
symbole_manquant : boolean:=false;

NomCote : string(1..NBRE_CAR_NOM):=(others=>' ');
I : integer;
TbChaine : string100 := (1..100 => ' ');
TbNcar : integer;
TbCoord_objet : point_type; 
esc : boolean;

begin
  -- Lecture du fichier des ecritures pour creer le graphe
  Open(Fic_Ecritures,in_file,Nom_Ecritures);

  -- Creation du graphe apres calcul de l'emprise
  Emprise:=Convert_coord_terrain_graphe(Maximum_terrain,InvEchelle,Pts_par_mm,Minimum_terrain);
  begin                                      
  if GrosExiste then
    Gr_Delete(Gros);
    GrosExiste:=false;
  end if;
  if Nb_ecritures>0 then
    gros:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,Invechelle,Nb_ecritures,2*Nb_ecritures,Nb_ecritures,Nom_graphe);
    GrosExiste:=true;
  end if;
  exception when  Event : others =>
    GR_DELETE(gros);
    gros:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,Invechelle,Nb_ecritures,2*Nb_ecritures,Nb_ecritures,Nom_graphe);
  end;
  -- Lecture et traitement de chaque enregistrement
  nb_diametre:=0;

  I:=1;
  	
  loop
    if end_of_file(Fic_Ecritures) then
      exit;
    end if;

    begin
      no_lig:=line(fic_Ecritures);
      -- Ligne(1..MaxLLigne):=(others=>' ');
      get_line(Fic_Ecritures,Ligne,LLigne);
    exception when END_ERROR =>
      close(Fic_Ecritures);
      Open(Fic_Ecritures,in_file,Nom_Ecritures);
      set_line(Fic_Ecritures, no_lig);
      LLigne:=0;
      while (not End_of_file(Fic_Ecritures)) and ( LLigne<MaxLLigne ) loop
        LLigne:=LLigne+1;
        get(Fic_Ecritures, Ligne(LLigne));
      end loop;
    end;

      -- Deplacement dans la chaine de string pour trouver toutes 
      -- les tabulations ou la fin de ligne;
      Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);

      Itab:=1;
      -- le toponyme est entre la 1ere et la 2eme tabulation
      tbNcar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
      tbChaine(1..tbNcar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);

      -- Lecture du point d'accroche :
      -- Correction eventuelle de la virgule en point pour lecture des reels
      Itab:=2;
      Lchaine:=Ptr_indice(Itab+1) - Ptr_indice(Itab) -1;
      if Lchaine/=0 then
         Chaine(1..Lchaine):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
         for i in 1..Lchaine loop
             if Chaine(i)=',' then
                Chaine(i):='.';
             end if;
         end loop;
         get(Chaine(1..Lchaine),Pt_accroche.coor_x,last);         
         last1:=last+2;
         get(Chaine(last1..Lchaine),Pt_accroche.coor_y,last);
         -- contribution a Vno et Vse 
         if Pt_accroche.coor_x<Vno.coor_x then 
           Vno.coor_x:=Pt_accroche.coor_x;
         end if;
         if Pt_accroche.coor_x>Vse.coor_x then
           Vse.coor_x:=Pt_accroche.coor_x;
         end if;
         if Pt_accroche.coor_y>Vno.coor_y then 
           Vno.coor_y:=Pt_accroche.coor_y; 
         end if;
         if Pt_accroche.coor_y<Vse.coor_y then 
           Vse.coor_y:=Pt_accroche.coor_y; 
         end if;
         tbCoord_objet:=Convert_coord_terrain_graphe(Pt_accroche,InvEchelle,Pts_par_mm,Minimum_terrain);
         Pt_accroche_graphe:=tbCoord_objet;
         
      else
         Gr_free_liens_array(Ptr_indice);
         raise Pas_de_point_d_accroche;
      end if;
	  	
      if route then
        for J in 1..tbNcar loop
          if not((tbChaine(J) in '0'..'9') or (tbChaine(J)=',') or (tbChaine(J)='.')) then
            exit;
          end if;
          if j=tbNcar then
            for k in 1..tbNcar loop
		      if tbChaine(k)/=',' then
 			  	NomCote(k):=tbChaine(k);
              else 
                NomCote(k):='.';
              end if;
            end loop;
            Gr_Creer_Noeud_Sur_Rien(grAlti,Pt_accroche_graphe,true,angle_type'value(NomCote(1..tbNcar)),0,NumNoeudI);
          end if;
        end loop;
      end if;

      if Ligne(1)='\' then
		goto finboucle;
      end if;

      tbnomi(I).nb_mots:=1;
      tbnomi(I).Ncar(1):=TbNcar;
      tbnomi(I).Chaine(1)(1..tbnomi(I).Ncar(1)):=TbChaine(1..TbNcar);
      tbnomi(I).Coord_objet:=tbCoord_objet;

      Itab:=1;
      -- l'identifiant est entre le debut de la ligne et la 1ere tabulation
      get(ligne(1..Ptr_indice(Itab)-1),tbnomi(i).id,last);

      -- lecture du code typo entre 3 et 4
      Itab:=3;

      -- Correctif FL 06/09/12 : initalisation de la variable � chaine de blanc
      tbnomi(i).topo.code(1..30):=(1..30 => ' ');
	  -- Fin de correctif FL 06/09/12 : suite dans LIREROUTES avec la m�me correction

      tbnomi(i).topo.code(1..Ptr_indice(Itab+1)-Ptr_indice(Itab)-1):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
      
       -- lecture du type d'objet  entre 4 et 5
       Itab:=4;
       ncar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
       car(1..ncar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
       case car(1) is
			   when 'Z' => tbnomi(i).topo.objet_type:= zonal;
			   when 'P' => tbnomi(i).topo.objet_type:= ponctuel;
			   when 'L' => tbnomi(i).topo.objet_type:= lineaire;
			   when others => raise erreur_placement;
       end case;
      -- lecture du placement entre 5 et 6
       Itab:=5;
       ncar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
       car(1..ncar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
       case car(1) is
       	 when 'I' =>tbnomi(i).topo.mode_placement:= interieur;
	  	 when 'D' =>tbnomi(i).topo.mode_placement:= decentre;
	  	 when 'M' =>tbnomi(i).topo.mode_placement:= mordant;
	  	 when 'E' =>tbnomi(i).topo.mode_placement:= exterieur;
         when 'C' =>tbnomi(i).topo.mode_placement:= centre;
         when others => raise erreur_placement; 
       end case;
      -- la liste de points  doit se trouver entre la 6eme et 7eme tabulation
      
      Itab:=6;
      Lchaine_coord:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
      -- attention dans le cas  d'un ponctuel pas de liste de points
      -- verifier que cette condition est suffisante
      if Lchaine_coord/=0 then
      	Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
        -- Lecture de la structure de points (coordonnees separeees par des ;)
      	LirePoints (Chaine_coord,Lchaine_coord,InvEchelle,Pts_par_mm,Minimum_terrain,Tab_pts,Nb_pts,true,false);
        Filtre_Ptsdoubles(tab_pts,Nb_pts);                                   
        Debouclage(tab_pts,Nb_pts);                                   
      else -- cas d'un ponctuel
          Nb_pts:=0;      
      end if;
          
      -- Gestion des differents cas :
			-- * Ponctuel sans symbole : traiter en ponctuel
         		-- * Ponctuel circulaire : traiter en ponctuel
	  		-- * Ponctuel avec symbole : traiter en surfacique
			-- * Zonal : traiter en zonal
      if nb_pts = 0 then
         -- Pas de surface : On traite un symbole ponctuel;

         -- lecture des informations sur le symbole: nom du symbole
         -- et orientation et proportion
         Itab:=7;
         Lchaine_theme:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
         if Lchaine_theme>Longueur_champs_theme then
            Gr_free_liens_array(Ptr_indice);
            raise Chaine_theme_TropLongue;
         else
            Chaine_theme(1..Longueur_champs_theme):=(others=>' ');
         end if;
         Chaine_theme(1..Lchaine_theme):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
         tbnomi(i).IDsymbole:=Chaine_theme;

         -- Lecture de l'orientation pour ce theme
         Itab:=8;
         Lchaine:=Ptr_indice(Itab+1) - Ptr_indice(Itab) -1;
         if Lchaine/=0 then
            Chaine(1..Lchaine):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
            get(Chaine(1..Lchaine),orientation,last);
         else
            Orientation:=0.0;
         end if;

         -- Lecture de la proportion pour ce theme
         Itab:=9;
         Lchaine:=Ptr_indice(Itab+1) - Ptr_indice(Itab) -1;
         if Lchaine/=0 then
            Chaine(1..Lchaine):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
            get(Chaine(1..Lchaine),Proportion,last);
         else
            Proportion:=1.0;
         end if;
         tbnomi(i).prop:=Proportion;

         if Lchaine_theme=0 then
   	    -- * Ponctuel sans symbole : traiter en ponctuel sans legende
            Gr_Creer_Noeud_Sur_Rien(gros,Pt_accroche_graphe,true,0.0,0,NumNoeudI);
            tbnomi(i).topo.num_objet:=NumNoeudI;
         else
            -- verification de l'existence de ce theme (dans le fichier des symboles)
            -- s'il existe, on recupere son numero
            -- s'il n'existe pas on indique une erreur
            flag_existence:=false;
            for j in 1..nb_symboles loop 
                if Tab_symboles(j).Nom=Chaine_theme then
                   indice_symbole:=j;
                   tbnomi(i).indice_symbole:=j;
                   flag_existence:=true;
                   exit;
                end if;
            end loop;
            if flag_existence=false then
               Gr_free_liens_array(Ptr_indice);
			   symbole_manquant:=true;
               raise Table_symboles_incomplete;
            end if;

	    -- Determination du type de symbole :
            -- Cercle defini par diametre ou Contour defini par liste de points
            if Tab_symboles(indice_symbole).nb_pts=1 then
               -- On a affaire a un veritable cercle dont on lit le diametre
               Diametre:=float(tab_symboles(indice_symbole).tab_coord(1).coor_x)*proportion/float(pts_par_mm);
   
               -- verification de l'existence de ce diametre (gestion de la legende PlaGe)
               -- s'il existe deja, on recupere son numero
               -- s'il n'existe pas on l'ajoute dans le tableau Tab_diametre
               flag_existence:=false;
               for j in 1..nb_diametre loop
                   if Tab_diametre(j)=diametre then
                      indice_diametre:=j;
                      flag_existence:=true;
                      exit;
                   end if;
               end loop;
               if flag_existence=false then
                  nb_diametre:=nb_diametre+1;
                  Tab_diametre(nb_diametre):=diametre;
                  indice_diametre:=nb_diametre;
               end if;

               Gr_Creer_Noeud_Sur_Rien(gros,Pt_accroche_graphe,true,0.0,indice_diametre,NumNoeudI);
               tbnomi(i).topo.num_objet:=NumNoeudI;                                     
            else
               -- On a affaire a un contour defini par une liste de points
               -- qu'il faut orienter et proportionner
               -- On va creer une surface de ce symbole :
               nb_pts_symb:=tab_symboles(indice_symbole).nb_pts;
               declare
                  tab_surf : point_liste_type(1..nb_pts_symb);
               begin 
                  for j in 1..nb_pts_Symb loop
  	              -- 1er etape : remise a la  bonne proportion
                      x1:=proportion*float(tab_symboles(indice_symbole).
                                   tab_coord(j).coor_x);
                      y1:=proportion*float(tab_symboles(indice_symbole).
                                   tab_coord(j).coor_y);
   	              -- 2eme etape : rotation (angle dans le sens d'une montre
                      -- et preparation de l'arrondi
                      x2:= x1*cos(orientation, 360.0) + y1*sin(orientation, 360.0) + 0.5;
                      y2:= -x1*sin(orientation, 360.0) + y1*cos(orientation, 360.0) + 0.5;
 		      -- 3eme etape : translation au point d'accroche
                      tab_surf(j).coor_x:=Pt_accroche_graphe.coor_x+integer(x2);
                      tab_surf(j).coor_y:=Pt_accroche_graphe.coor_y+integer(y2);
                  end loop;

                  -- Creation du noeud, puis arc, puis face
                  Gr_Creer_Noeud_Sur_Rien(gros, Tab_surf(1), false,0.0,0,NumNoeudI) ;
                  Gr_Creer_Arc(gros,NumNoeudI,NumNoeudI,Tab_surf,Nb_pts_symb,gen_io.Ligne,1,0,NumArc,esc);
                  tabarc(1):=NumArc;
                  Gr_Creer_Face(gros,Tabarc,1,1,Numface);
                  
                   tbnomi(i).topo.num_objet:=Numface;
                   --!!! dans ce cas l'objet est traite comme surfacique
                   tbnomi(i).topo.objet_type:= zonal;
                   tbnomi(i).topo.mode_placement:=exterieur;
                   
               end;
            end if;
         end if;

      elsif nb_pts > 3  and tab_pts(1)=Tab_pts(nb_pts) then 
         -- on cree la face fermee :
         -- Creation du noeud + arc + face
         Gr_Creer_Noeud_Sur_Rien(gros, Tab_pts(1), false,0.0,0, NumNoeudI);
         Gr_Creer_Arc(gros,NumNoeudI,NumNoeudI,Tab_pts,Nb_pts,gen_io.Ligne,1,0,NumArc,esc);
         tabarc(1):=NumArc;
         Gr_Creer_Face(gros,Tabarc,1,2,Numface);
         tbnomi(i).topo.num_objet:=NumFace;
      else 
         -- cas non standard
         Gr_free_liens_array(Ptr_indice);
         raise Erreur_coherence;
      end if;  
      Gr_free_liens_array(Ptr_indice);

   	  I:=I+1;
	  << finboucle >>
	  null;
  end loop;

  if nb_ecritures>0 then
    -- Creation de la legende associee a ce graphe PlaGe
    legos:= GR_create_new_legende(1,nb_diametre,2);
    -- legende du lineaire : 1 seule couleur pour tous les arcs
    affichage.largeur:=1.0;
    affichage.rouge:=1.0;
    affichage.vert:= 0.0;
    affichage.bleu:= 1.0;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    gR_MOD_LEGENDE_L(legos,1,affichage);
    -- legende du zonal : 2 couleurs : vert pour les surfaces issues d'un symbole
    --  ponctuel, bleu pour les surfaces issues d'un contour
    affichage.largeur:=1.0;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    affichage.rouge:=0.0;
    affichage.vert:= 1.0;
    affichage.bleu:= 0.0;
    GR_MOD_LEGENDE_Z(legos,1,affichage);
    affichage.rouge:=0.0;
    affichage.vert:= 0.0;
    affichage.bleu:= 1.0;
    GR_MOD_LEGENDE_Z(legos,2,affichage);
    -- legende du ponctuel
    for i in 1..nb_diametre loop
      affichage.largeur:=tab_diametre(i);
      affichage.rouge:=1.0 - float(i-1)/float(nb_diametre);
      affichage.vert:= float(nb_diametre-i)/float(nb_diametre);
      affichage.bleu:= float(i)/float(nb_diametre);
      affichage.larg_int:=0.0;
      affichage.priorite:=1;
      GR_MOD_LEGENDE_P(legos,i,affichage);
    end loop;
    affichage.largeur:=0.0;
    affichage.rouge:=0.5;
    affichage.vert:=0.5;
    affichage.bleu:=0.5;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    GR_MOD_LEGENDE_P(legos,0,affichage);
  end if;

  close(Fic_ecritures);
  
exception when  Event : others =>
  
  close(Fic_ecritures);
  Msg_Erreur:=
    To_Unbounded_String("Erreur de cr�ation du graphe des �critures"&eol
                        &"Fichier: "&Nom_Ecritures&EOL
                        &"Ligne n� "&Integer'Image(integer(no_lig)));
  if symbole_manquant=true then
  	Msg_Erreur:=Msg_Erreur&To_Unbounded_String(eol&eol&"Le symbole ponctuel '"&Chaine_theme(1..Lchaine_theme)&"' est absent du fichier des symboles d'appui");
  end if;
--  close(fic_ecritures);    
  raise;
end LireEcritures;


-- ======================================================================
-- Procedure de lecture du fichier des ecritures pour creer le graphe associe
-- cas o� il n'y a pas de fichier de symboles d'appui (appui=false)

procedure LireEcritures(NOM_Ecritures : in string ;
                        NOM_GRAPHE : in string ;
        			    InvEchelle : in positive ;
        				pts_par_mm : in positive;
        				Minimum_terrain : in point_type_reel;
        				Maximum_terrain : in point_type_reel;
        				Nb_ecritures : in integer;
        				Nb_pts_ecritures : in integer;
                        tbnomi : in out typtbnomi) is

NomCote : string(1..NBRE_CAR_NOM):=(others=>' ');

Erreur_coherence,Chaine_theme_TropLongue, erreur_placement,
Pas_de_point_d_accroche  : exception;
Fic_ecritures : text_io.file_type;		-- Fichier texte des ecritures

Tab_pts : point_liste_type(1..2*Nb_pts_ecritures);-- Tableau de points 
						-- decrivant les contours

MaxLLigne: integer:=1500000;	-- Longueur maximale des lignes du fichier texte
Ligne 	 : string (1..MaxLLigne);-- Ligne du fichier texte des routes
LLigne   : integer;		-- Longueur de la ligne
Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
Itab 	   : integer;			-- Indice de lecture des tabulations

Longueur_champs_theme : integer:=30;

Chaine_coord 	: string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord 	: integer;		-- Longueur de la chaine des coordonnees
Chaine_theme 	: string(1..Longueur_champs_theme);
Lchaine_theme 	: integer;

Chaine 		: string(1..50);	-- Chaine de lecture intermediaire
Lchaine : integer;			-- Longueur de la chaine
last,last1 : integer;			-- Variable local

Orientation	: float;		-- Orientation du symbole
proportion	: float;		-- Proportion du symboles
Diametre 	: float;		-- Diametre du symbole circulaire

Nb_pts 		: integer;		--
Nb_diametre	: integer;		-- Nombre de diametre de symboles circulaires
indice_diametre : integer;		-- Indice de diametre trouve

--graphe		: graphe_type;
--Legende 	: legende_type;
Emprise 	: point_type;		-- Emprise du graphe

Pt_accroche 	: point_type_reel;	-- Coordonnees du pt d'accroche (en m)
Pt_accroche_graphe : point_type;	-- Coord. du pt d'accroche dans graphe

indice_symbole 	: integer;		-- Indice dans la table des symboles

affichage 	: affichage_type;	-- variables intermediaires diverses
x1,y1,x2,y2 	: float;
nb_pts_symb 	: integer;
NumNoeudI,Numarc,Numface : integer;
Tabarc 		: liens_array_type(1..1);
Flag_existence 	: boolean;
car : string(1..20);
ncar : integer;
no_lig: text_io.count;
ponctuel_decentre : boolean:=false;

I : integer;
TbChaine : string100 := (1..100 => ' ');
TbNcar : integer;
TbCoord_objet : point_type; 
esc : boolean;

begin
  -- Lecture du fichier des ecritures pour creer le graphe
  --------------------------------------------------------
  Open(Fic_Ecritures,in_file,Nom_Ecritures);

  -- Creation du graphe apres calcul de l'emprise
  Emprise:=Convert_coord_terrain_graphe(Maximum_terrain,InvEchelle,Pts_par_mm,Minimum_terrain);

  if GrosExiste=true then
    Gr_Delete(Gros);
  end if;
  gros:=Gr_create_new(emprise.coor_x,emprise.coor_y,Pts_par_mm,Invechelle,Nb_ecritures,2*Nb_ecritures,Nb_ecritures,Nom_graphe);
  grosExiste:=true;

  -- Lecture et traitement de chaque enregistrement
  nb_diametre:=0;
  i:=1;
  loop
    if end_of_file(Fic_Ecritures) then
      exit;
    end if;

    begin
      no_lig:=line(Fic_Ecritures);
      Ligne(1..MaxLLigne):=(others=>' ');
      get_line(Fic_Ecritures,Ligne,LLigne);
    exception when END_ERROR =>
      close(Fic_Ecritures);
      Open(Fic_Ecritures,in_file,Nom_Ecritures);
      set_line(Fic_Ecritures, no_lig);
      LLigne:=0;
      while (not End_of_file(Fic_Ecritures)) and ( LLigne<MaxLLigne ) loop
        LLigne:=LLigne+1;
        get(Fic_Ecritures, Ligne(LLigne));
      end loop;
    end;
      
      -- Deplacement dans la chaine de string pour trouver toutes 
      -- les tabulations ou la fin de ligne;
      Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);
      -- l'identifiant est entre le debut de la ligne et la 1ere tabulation
      Itab:=1;
      -- le toponyme est entre la 1ere et la 2eme tabulation
      TbNcar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
      tbChaine(1..TbNcar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
      
      -- Lecture du point d'accroche :
      -- Correction eventuelle de la virgule en point pour lecture des reels
      Itab:=2;
      Lchaine:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
      if Lchaine/=0 then
         Chaine(1..Lchaine):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
         for i in 1..Lchaine loop
             if Chaine(i)=',' then
                Chaine(i):='.';
             end if;
         end loop;
         get(Chaine(1..Lchaine),Pt_accroche.coor_x,last);
         last1:=last+2;
         get(Chaine(last1..Lchaine),Pt_accroche.coor_y,last);
         -- contribution a Vno et Vse
         if Pt_accroche.coor_x<vno.coor_x then 
           vno.coor_x:=Pt_accroche.coor_x;
         end if;
         if Pt_accroche.coor_x>vse.coor_x then
           vse.coor_x:=Pt_accroche.coor_x;
         end if;
         if Pt_accroche.coor_y>vno.coor_y then 
           vno.coor_y:=Pt_accroche.coor_y; 
         end if;
         if Pt_accroche.coor_y<vse.coor_y then 
           vse.coor_y:=Pt_accroche.coor_y; 
         end if;
         tbCoord_objet:=Convert_coord_terrain_graphe(Pt_accroche,InvEchelle,Pts_par_mm,Minimum_terrain);
         Pt_accroche_graphe:=tbCoord_objet;
      else
         Gr_free_liens_array(Ptr_indice);
         raise Pas_de_point_d_accroche;
      end if;

	  if route then
		for J in 1..tbNcar loop
          if not((tbChaine(J) in '0'..'9') or (tbChaine(J)=',') or (tbChaine(J)='.')) then
            exit;
          end if;
          if j=tbNcar then
            for k in 1..TbNcar loop
			  if tbChaine(k)/=',' then
 			  	NomCote(k):=tbChaine(k);
              else
                NomCote(k):='.';
              end if;
            end loop;
            Gr_Creer_Noeud_Sur_Rien(grAlti,Pt_accroche_graphe,true,angle_type'value(NomCote(1..tbNcar)),0,NumNoeudI);
          end if;
        end loop;
      end if;

      if Ligne(1)='\' then
		goto finboucle;
      end if;
 
      tbnomi(I).nb_mots:=1;
      tbnomi(I).Ncar(1):=TbNcar;
      tbnomi(I).Chaine(1)(1..tbnomi(I).Ncar(1)):=TbChaine(1..TbNcar);
      tbnomi(I).Coord_objet:=tbCoord_objet;

      -- l'identifiant est entre le debut de la ligne et la 1ere tabulation
      Itab:=1;
      get(ligne(1..Ptr_indice(Itab)-1),tbnomi(i).id,last);

      -- lecture du code typo entre 3 et 4
      Itab:=3;

	  -- Correctif FL 06/09/12 : initalisation de la variable � chaine de blanc
      tbnomi(i).topo.code(1..30):=(1..30 => ' ');
	  -- Fin de correctif FL 06/09/12 : suite dans LIREROUTES avec la m�me correction

      tbnomi(i).topo.code(1..Ptr_indice(Itab+1)-Ptr_indice(Itab)-1):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
       -- lecture du type d'objet  entre 4 et 5
       Itab:=4;
       ncar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
       car(1..ncar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
       case car(1) is
         when 'Z' => tbnomi(i).topo.objet_type:= zonal;
         when 'P' => tbnomi(i).topo.objet_type:= ponctuel;
         when 'L' => tbnomi(i).topo.objet_type:= lineaire;
         when others => raise erreur_placement;
       end case;
      -- lecture du placement entre 5 et 6
       Itab:=5;
       ncar:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
       car(1..ncar):=ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
       case car(1) is
         when 'I' =>tbnomi(i).topo.mode_placement:= interieur;
         when 'D' =>tbnomi(i).topo.mode_placement:= decentre;
         when 'M' =>tbnomi(i).topo.mode_placement:= mordant;
         when 'E' =>tbnomi(i).topo.mode_placement:= exterieur;
         when 'C' =>tbnomi(i).topo.mode_placement:= centre;
         when others => raise erreur_placement; 
       end case;

      -- la liste de points  doit se trouver entre la 6eme et 7eme tabulation      
      Itab:=6;
      Lchaine_coord:=Ptr_indice(Itab+1)-Ptr_indice(Itab)-1;
      -- attention dans le cas  d'uin ponctuel pas de liste de points
      -- verifier que cette condition est suffisante
      if Lchaine_coord/=0 then
      	Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(Itab)+1..Ptr_indice(Itab+1)-1);
      -- Lecture de la structure de points (coordonnees separeees par des ;)
      	LirePoints(Chaine_coord,Lchaine_coord,InvEchelle,Pts_par_mm,Minimum_terrain,Tab_pts,Nb_pts,true,false);
        Filtre_Ptsdoubles(tab_pts,Nb_pts);                                     
        Debouclage(tab_pts,Nb_pts);
      else -- cas d'un ponctuel
          Nb_pts:=0;      
      end if;

      -- Gestion des differents cas :
			-- * Ponctuel sans symbole : traiter en ponctuel
			-- * Zonal : traiter en zonal
      if nb_pts = 0 then
         -- Pas de surface : On traite un symbole ponctuel;
         Orientation:=0.0;
         Proportion:=1.0;
         Gr_Creer_Noeud_Sur_Rien(gros,Pt_accroche_graphe,true,0.0,0,NumNoeudI);
         tbnomi(i).topo.num_objet:=NumNoeudI;

		 if car(1)='D' and ponctuel_decentre=false then
		   gb.MsgBox("Le fichier des �critures horizontales contient des �critures ponctuelles en mode d�c�ntr�"&eol&"Leur placement n�cessite un fichier de symboles d'appui"
		    &eol&eol&"Des cercles de rayon nul vont servir de symboles d'appui par d�faut",
		             " Fichier des symboles d'appui absent",win32.winuser.MB_ICONEXCLAMATION);
		   ponctuel_decentre:=true;
         end if;

      elsif nb_pts > 3  and tab_pts(1)=Tab_pts(nb_pts) then 
         -- on cree la face fermee :
         -- Creation du noeud + arc + face
         Gr_Creer_Noeud_Sur_Rien(gros, Tab_pts(1),false,0.0,0,NumNoeudI);
         Gr_Creer_Arc(gros,NumNoeudI,NumNoeudI,Tab_pts,Nb_pts,gen_io.Ligne,1,0,NumArc,esc);
         tabarc(1):=NumArc;
         Gr_Creer_Face(gros,Tabarc,1,2,Numface);
          tbnomi(i).topo.num_objet:=NumFace;
      else 
         -- cas non standard
         Gr_free_liens_array(Ptr_indice);
         raise Erreur_coherence;
      end if;  
      Gr_free_liens_array(Ptr_indice);

    I:=I+1;
    << finboucle >>
	null;
  end loop;

  if nb_ecritures>0 then
    -- Creation de la legende associee a ce graphe PlaGe
    legos:= GR_create_new_legende(1,nb_diametre,2);
    -- legende du lineaire : 1 seule couleur pour tous les arcs
    affichage.largeur:=1.0;
    affichage.rouge:=1.0;
    affichage.vert:= 0.0;
    affichage.bleu:= 1.0;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    GR_MOD_LEGENDE_L(legos,1,affichage);
    -- legende du zonal : 2 couleurs : vert pour les surfaces issues d'un symbole
    --  ponctuel, bleu pour les surfaces issues d'un contour
    affichage.largeur:=1.0;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    affichage.rouge:=0.0;
    affichage.vert:= 1.0;
    affichage.bleu:= 0.0;
    GR_MOD_LEGENDE_Z(legos,1,affichage);
    affichage.rouge:=0.0;
    affichage.vert:= 0.0;
    affichage.bleu:= 1.0;
    GR_MOD_LEGENDE_Z(legos,2,affichage);
    -- legende du ponctuel
    affichage.largeur:=0.0;
    affichage.rouge:=0.5;
    affichage.vert:=0.5;
    affichage.bleu:=0.5;
    affichage.larg_int:=0.0;
    affichage.priorite:=1;
    GR_MOD_LEGENDE_P(legos,0,affichage);
  end if;

  close(Fic_ecritures);
  
exception when  Event : others =>
  close(fic_ecritures);    
  Msg_Erreur:=
    To_Unbounded_String("Erreur de cr�ation du graphe des �critures"&eol
                        &"Fichier: "&Nom_Ecritures&EOL
                        &"Ligne n� "&Integer'Image(integer(no_lig)));
--  close(fic_ecritures);    
  raise;
end LireEcritures;


-- ======================================================================
-- Procedure de lecture du fichier des symboles pour stocker les symboles
-- (nom + nombre de points du contour + coordonnees des points du contour)

procedure LireSymbolesPonct(NOM_SYMBOLES : in string;
                            InvEchelle : in positive; 
                            pts_par_mm : in positive;
                            Tab_symboles : in out Symbole_liste_type;
                            Nb_symboles  : in integer) is

Erreur_Chaine_nom_TropLongue, Erreur_LireSymbolesPonct,
Erreur_Contour_symbole_non_ferme  : exception;
Fic_symboles : text_io.file_type;	-- Fichier texte des symboles
MaxLLigne : integer:=32000;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne);-- Ligne du fichier texte des routes
LLigne : integer;		-- Longueur de la ligne
Ptr_indice : Liens_access_type;	-- Pointeur des indices des separateurs
no_lig : text_io.count;
Tab_pts : point_liste_type(1..Tab_symboles(1).Tab_coord'length); 
Chaine_coord : string(1..MaxLLigne);	-- Chaine contenant toutes les coordonnees
LChaine_coord : integer;		-- Longueur de la chaine des coordonnees
Longueur_champs_nom : integer:=30;
Chaine_nom : string(1..Longueur_champs_nom);-- Nom du symbole a lire
Lchaine_nom : integer;
Nb_pts : integer;
Origine_nulle : Point_type_reel:=(0.0,0.0); -- Point origine pour transferer les unite Graphe

begin
  -- Lecture du fichier des points d'appui
  Open(Fic_symboles,in_file,Nom_Symboles);

  -- Lecture successive des symboles
  for i in 1..Nb_symboles loop
    loop
      begin
        no_lig:=line(Fic_symboles);
        Ligne(1..MaxLLigne):=(others=>' ');
        get_line(Fic_symboles,Ligne,LLigne);
      exception when END_ERROR =>
        close(Fic_symboles);
        Open(Fic_symboles,in_file,Nom_Symboles);
        set_line(Fic_symboles, no_lig);
        LLigne:=0;
        while (not End_of_file(Fic_symboles)) and ( LLigne<MaxLLigne ) loop
          LLigne:=LLigne+1;
          get(Fic_symboles, Ligne(LLigne));
        end loop;
      end;
      if ligne(1)/='\' then
	    exit;
	  end if;
    end loop;
      -- Deplacement dans la chaine de string pour trouver toutes 
      -- les tabulations ou la fin de ligne;
      Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);

      -- lecture du nom du symbole (qui se trouve en debut d'enregistrement)
      Lchaine_nom:=Ptr_indice(1) - 1;
      if Lchaine_Nom > Longueur_champs_nom then
         Gr_free_liens_array(Ptr_indice);
         raise Erreur_Chaine_nom_TropLongue;
      else
         Chaine_nom(1..Longueur_champs_nom):=(others=>' ');
      end if;
      Chaine_nom(1..Lchaine_nom):=Ligne(1..Ptr_indice(1)-1);

      -- la liste de points se trouve entre la 1ere et 2eme tabulation
      Lchaine_coord:=Ptr_indice(2)-Ptr_indice(1)-1;
      Chaine_coord(1..Lchaine_coord):=Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);

      -- Lecture de la structure de points (coordonnees separeees par des ;)
      LirePoints (Chaine_coord,Lchaine_coord,InvEchelle,Pts_par_mm,Origine_nulle,Tab_pts,Nb_pts,false,false);

      if Tab_pts(1)/=Tab_pts(nb_pts) then
         Gr_free_liens_array(Ptr_indice);
         raise Erreur_Contour_symbole_non_ferme;
      end if;

      Tab_symboles(i).Tab_coord(1..nb_pts):=Tab_pts(1..nb_pts);
      Tab_symboles(i).nb_pts:=nb_pts;
      Tab_symboles(i).Nom:=Chaine_nom;

      Gr_free_liens_array(Ptr_indice);
  end loop; 
  close(Fic_symboles);

exception when  Event : others =>
  close(fic_symboles);
  Msg_Erreur:=
    To_Unbounded_String("Erreur de lecture du fichier: "&Nom_Symboles&eol
                        &"Ligne n� "&Integer'Image(integer(no_lig)));
--  close(fic_symboles);
  raise;
end LireSymbolesPonct;

-- ======================================================================
end Lecture ;

