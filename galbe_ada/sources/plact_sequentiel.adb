--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with norou; use norou;
with gb;
with DIALOG3;
with math_int_basic;

package BODY PLACT_SEQUENTIEL is

--=============================================================================
-- Tri a bulles des objets dans l'ordre des degres de contrainte decroissants:
-------------------------------------------------------------------------------
procedure ordonnerc (tbnomi :in typtbnomi;
                     tico,tr:in out typtico;
                     tdc,tdl:in float_array_type;
                     propa  :in boolean) is
  tmp:positive; 
begin
  for no in 1..tbnomi'last loop
    tico(no):=no;
  end loop;
  if not propa then
    for i in reverse 1..tbnomi'last loop
      for j in 1..i-1 loop
        if tdc(tico(j))<tdc(tico(j+1)) then
          tmp:=tico(j);
          tico(j):=tico(j+1);
          tico(j+1):=tmp;
        end if;
      end loop;
    end loop;
  else
    for i in reverse 1..tbnomi'last loop
      for j in 1..i-1 loop
        if ( tbnomi(tico(j)).id<0 and tbnomi(tico(j+1)).id>=0 ) or (tbnomi(tico(j)).NPic=0 and tbnomi(tico(j+1)).NPic>0)
		or ((tbnomi(tico(j)).id/math_int_basic.max(abs(tbnomi(tico(j)).id),1))*tbnomi(tico(j+1)).id>0 and tdl(tico(j))>tdl(tico(j+1))) then
          tmp:=tico(j);
          tico(j):=tico(j+1);
          tico(j+1):=tmp;
        end if;
      end loop;
    end loop;
  end if;
  for i in 1..tbnomi'last loop
    tr(tico(i)):=i;
  end loop;
end ordonnerc;

--=============================================================================
-- Retri d'un objet oi de rang compris entre tr(no) et no 
-- (propagation des contraintes)
-- dans l'ordre des degres de liberte croissants:
-------------------------------------------------------------------------------
procedure reordonnerc (no     :in positive;
                       noi    :in positive; 
                       tdl    :in float_array_type;
                       tico,tr:in out typtico;
                       propa  :in boolean) is
begin
  for r in tr(no)+1..tr(noi)-1 loop
    if tdl(noi)<tdl(tico(r)) then
      for r2 in reverse r..tr(noi)-1 loop
        tr(tico(r2)):=tr(tico(r2))+1;
        tico(r2+1):=tico(r2);
      end loop;
      tr(noi):=r;
      tico(r):=noi;
      exit;
    end if;
  end loop;
end reordonnerc;

--=============================================================================
-- Procedure calculant les degres de liberte des objets :
-----------------------------------------------------------
procedure DET_DL (tnp   :in typtnp;
                  tnpi  :in typtnpi;
                  tbnomi:in typtbnomi;
                  tdl   :in out float_array_type) is
begin
  for no in 1..tbnomi'last loop
    tdl(no):=0.0;
    if tnp(no)=0 then
      tdl(no):=0.0;
    elsif tnpi(no,1)=0 then
      tdl(no):=float'last;--+oo
    else
      for ip in 1..tnp(no) loop
        tdl(no):=tdl(no)+1.0-0.1/float(maxp)*float(ip);
      end loop;
    end if;
  end loop;
end det_dl;

--======================================================================
-- Procedure calculant les degres de contrainte des objets:
-------------------------------------------------------------
procedure DET_DC (tnp   :in typtnp;
                  tnpi  :in typtnpi;
                  tnci  :in typtnci;
                  tbnomi:in typtbnomi;
                  tici  :in typtici;
                  tdc   :in out float_array_type) is
  dc,numdc,dendc:float:=0.0;
begin
  for no in 1..tbnomi'last loop
    if tnp(no)=0 then
      tdc(no):=float'last;
    elsif tnpi(no,1)=0 then
      tdc(no):=0.0;
    else
      numdc:=0.0;
      dendc:=0.0;
      for ip in 1..tnp(no) loop
        numdc:=numdc+float(tnpi(no,ip))/float(ip);
        dendc:=dendc+1.0/float(ip);
      end loop;
    tdc(no):=numdc/dendc;
    end if;
  end loop;
end det_dc;

--=============================================================================
-- Procedure recalculant le dl d'un objet (propagation de contraintes) :
---------------------------------------=-----------------------------------
procedure REDET_DL (no  :in positive;
                    tnp :in typtnp;
                    tnpi:in typtnpi;
                    tp  :in out typtp;
                    tdl :in out float_array_type) is
begin
  tdl(no):=0.0;
  if tnp(no)=0 then
    tdl(no):=0.0;
  end if;
  if tnpi(no,1)=0 then
    tdl(no):=float'last;--+oo
  end if;
  for ip in 1..tnp(no) loop
    if tp(no,ip).poss=true then
      tdl(no):=tdl(no)+1.0-0.1/float(maxp)*float(ip);
    end if;
  end loop;
end redet_dl;

--============================================================================
-- Le poids d'interaction poii est grosso modo proportionnel au nombre de
-- positions pi incompatibles avec la position ip.
-- En fait, si pi est une position en interaction avec la position ip, et si
-- le poip de pi est eleve, alors pi n'augmente guere poii.
-- Au contraire, si le poip de pi est faible, alors pi augmente beaucoup poii.
-- En outre, si le nom de l'objet de pi a �t� plac� 
-- � une autre position que pi, alors pi ne modifie pas poii.
------------------------------------------------------------------------------
-- Procedure qui determine le poids total d'une position:
---------------------------------------------------------
procedure DETPOITOT (no    :in positive; 
                     p     :in natural;
                     poii  :out typpoi;
                     poitot:out typpoi;
                     tbnomi:in typtbnomi;
                     maxnpos:in integer;
                     tp    :in typtp;
                     tnpi  :in typtnpi;
                     tppi  :in typtppi;
                     lpi   :in typlpi) is
  pi:position_type;
  poiipipot:typpoi:=0.0;
  poiitmp:typpoi:=0.0;
begin
  if maxnpos=0 then
    poii:=0.0;
    poitot:=tp(no,p).poip;
    return;
  end if;
  for ipi in 1..tnpi(no,p) loop
    pi:=tp(lpi(tppi(no,p)+ipi-1).obj,lpi(tppi(no,p)+ipi-1).pos);
    -- Si le nom de no reste � placer, pi augmente poii:
    if tbnomi(lpi(tppi(no,p)+ipi-1).obj).p_choisie = 0 then
      poiipipot:=((10.0-pi.poip)**5/(10.0)**4)/float(maxnpos);
      --rajout 22/02/2000 pb d'arrondi
      if (poiitmp+poiipipot)>=10.0 then
        poiitmp:=10.0 ;
      else
        poiitmp:=poiitmp+poiipipot;
      end if;
      --fin rajout
      --poiitmp  :=poiitmp+poiipipot;
    end if;
  end loop;
  --------- Poids total de la position : ----------
  poitot:=Cpoip*tp(no,p).Poip+Cpoii*Poiitmp;
  poii:=poiitmp;
end detpoitot;

--==========================================================================
-- Procedure qui choisit la meilleure pos possible pour un objet donne:
-----------------------------------------------------------------------
procedure DET_MP (no     :in positive; 
                  tbnomi :in out typtbnomi;
                  tnp    :in typtnp;
                  tp     :in out typtp;
                  tnpi   :in typtnpi;
                  tppi   :in typtppi;
                  lpi    :in typlpi;
                  tnci   :in typtnci;
                  tici   :in typtici;
                  tico,tr:in out typtico;
                  tdl    :in out float_array_type;
                  blo    :in out boolean;
                  propa  :in boolean ) is
  poii,poitot,minpoitot,poiimp:typpoi:=0.0;
  imp:integer:=0; -- Indice de la meilleure position.
  maxnpos:integer:=0;
  nouvelle_pos:boolean;
begin
    ---------------------------------------------------------------------
    -- Examinons les positions possibles ip pour le nom d'un objet no:
    ---------------------------------------------------------------------
  if tnp(no)=0 then  -- pas de position
    return;
  end if;
  for ip in 1..tnp(no) loop
    if maxnpos<tnpi(no,ip) then
      maxnpos:=tnpi(no,ip);
    end if;
  end loop;
  minpoitot:= typpoi'last;
  for ip in 1..tnp(no) loop
    nouvelle_pos:= true;
    -- Si cette position est impossible, examinons la suivante:
    if tp(no,ip).poss = true then
      DETPOITOT(no,ip,poii,poitot,tbnomi,maxnpos,tp,tnpi,tppi,lpi);
      if poitot<minpoitot then
        imp:=ip;
        minpoitot:=poitot;
        poiimp:=poii;
      end if;
    end if;
  end loop;
    ---------------------------------------------------------------------
    -- Si nous avons trouve des positions possibles, adoptons la meilleure:
    ----------------------------------------------------------------------
  if imp>0 then
    blo:=false;
    tbnomi(no).p_choisie:=imp;
    -- Les pos en inter avec la pos choisie deviennent impossibles.
    -- Il faut mettre a jour l'indicateur de possibilite de ces pos:
    for ipi in 1..tnpi(no,imp) loop
      tp(lpi(tppi(no,imp)+ipi-1).obj,lpi(tppi(no,imp)+ipi-1).pos).poss:=false;
    end loop;
    if propa then  -- Propagation des contraintes:
      for noi in 1..tnci(no) loop
        if tr(tici(no,noi))>tr(no) then
          Redet_dl(tici(no,noi),tnp,tnpi,tp,tdl);
          Reordonnerc(no,tici(no,noi),tdl,tico,tr,propa);
        end if;
      end loop;
    end if;
    -------------------------
    -- Sinon, il y a blocage:
    -------------------------
  else
    blo:=true;
    tbnomi(no).p_choisie:=0;

    --put("Le nom suivant est bloque: ");
    --put(tbnomi(no).chaine(1..tbnomi(no).ncar));
    --new_line(1);
  end if;
end det_mp;

--======================================================================
-- Procedure de retour en arriere:
----------------------------------
procedure RA (no    :in positive;
              tnp   :in typtnp;
              tp    :in out typtp;
              tnpi  :in typtnpi;
              tppi  :in typtppi;
              lpi   :in typlpi;      
            	tnci  :in typtnci;      
              tici  :in typtici;     
              tr    :in out typtico;
              tbnomi:in out typtbnomi) is
                
 poii,poitot,minpoitot,poiimp:typpoi:=0.0;
 poii_ic,poitot_ic:typpoi:=0.0;
 poii_ica,poitot_ica:typpoi;
 imp   :integer;
 impa  :integer;  
 idpi  :obj_pos;
 pi    :position_type;
 ipca  :natural; 
 ica,rica:integer;
 maxnpos,maxnpica:integer:=0;
 comp:array(1..maxp,1..maxp) of boolean:=(others=>(others=>true));
 --Comp:tableau des combinaison de positions
 
begin 
  --------------------------------- 
  -- commencons par determiner ica, 
  ---------------------------------
  -- indice de celui des voisins de ic dont le nom a ete place en dernier, 
  -- i.e. le voisin dont le dc est > a celui de ic, et est le plus faible.
  rica:=1;  
  for noi in 1..tnci(no) loop
    if tr(tici(no,noi))<tr(no)
    and rica<tr(tici(no,noi))
    and tbnomi(tici(no,noi)).p_choisie/=0 then
      ica:=tici(no,noi);
      rica:=tr(ica);
    end if;
  end loop;     
  -----------------------------------------
  -- Supprimons le placement du nom de ica:
  -----------------------------------------
  ipca:=tbnomi(ica).p_choisie;
  tbnomi(ica).p_choisie:=0;

  -- Mettons a jour l'indicateur de possibilite des positions en 
  -- interaction avec la position de ica, qui vient d'etre supprimee:

  -- CP: attention tnpi(ica,ipca) peut etre nul --
  if tnpi(ica,ipca)/=0 then
    for ipi in 1..tnpi(ica,ipca) loop
      tp(lpi(tppi(ica,ipca)+ipi-1).obj,lpi(tppi(ica,ipca)+ipi-1).pos).poss:= true;
      for ipi2 in 1..tnpi(lpi(tppi(ica,ipca)+ipi-1).obj,
                          lpi(tppi(ica,ipca)+ipi-1).pos) loop
        idpi:=lpi(tppi(lpi(tppi(ica,ipca)+ipi-1).obj,
                       lpi(tppi(ica,ipca)+ipi-1).pos)+ipi2-1);
        if tbnomi(idpi.obj).p_choisie=idpi.pos then
          tp(lpi(tppi(ica,ipca)+ipi-1).obj,
             lpi(tppi(ica,ipca)+ipi-1).pos).poss:=false;
          exit;
        end if; 
      end loop;
    end loop;
  end if;
  ---------------------------------------------------------------------------
  -- Examinons une combin. d'une position pour ic et d'une position pour ica:
  ---------------------------------------------------------------------------
  -- Cherchons d'abord les combinaisons compatibles:
  for ip in 1..tnp(no) loop
    for ipi in 1..tnpi(no,ip) loop
       if lpi(tppi(no,ip)+ipi-1).obj=ica then
           comp(ip,lpi(tppi(no,ip)+ipi-1).pos):=false ;
       end if ;
    end loop ;
  end loop ;
  imp :=0;
  impa:=0;
  for ip in 1..tnp(no) loop
     if maxnpos<tnpi(no,ip) then
       maxnpos:=tnpi(no,ip);
     end if;
  end loop;
  for ipa in 1..tnp(ica) loop
     if maxnpica<tnpi(ica,ipa) then
       maxnpica:=tnpi(ica,ipa);
     end if;
  end loop;
  minpoitot:=typpoi'last;
  for ip in 1..tnp(no) loop
   -- Si cette position est impossible, examinons la suivante:
   if tp(no,ip).poss=true then
     for ipa in 1..tnp(ica) loop
       if tp(no,ipa).poss=true then 
         if comp(ip,ipa)=true then 
           detpoitot(no,ip,poii_ic,poitot_ic,tbnomi,maxnpos,tp,tnpi,tppi,lpi);
           detpoitot(ica,ipa,poii_ica,poitot_ica,tbnomi,maxnpica,tp,tnpi,tppi,lpi);
           poii:=(poii_ic+poii_ica)/2.0;
           poitot:=(poitot_ic+poitot_ica)/2.0;
           if poitot<minpoitot then 
                 imp:= ip;
                 impa:=ipa;
                 minpoitot:=poitot;
                 poiimp:=poii;
           end if;         
         end if;
       end if; 
     end loop;
   end if;
  end loop;
  ------------------------------------------------------------
  -- Adoptons la meilleure combinaison de positions possibles:
  ------------------------------------------------------------
  if imp>0 then
     tbnomi(ica).p_choisie:=impa;
     tbnomi(no).p_choisie:=imp;
     -- Les positions en interaction avec les 2 positions que nous venons de 
     -- choisir, deviennent impossibles. Il faut mettre a jour l'indicateur
     -- de possibilite de ces positions:
     for ipi in 1..tnpi(no,imp) loop
        tp(lpi(tppi(no,imp)+ipi-1).obj,lpi(tppi(no,imp)+ipi-1).pos).poss:=false;
     end loop;
     for ipi in 1..tnpi(ica,impa) loop
        tp(lpi(tppi(ica,impa)+ipi-1).obj,lpi(tppi(ica,impa)+ipi-1).pos).poss:=false;
     end loop;
  else 
     -- Redonnons au nom de l'objet arriere sa position initiale:
     tbnomi(ica).p_choisie:=ipca;

     -- Les pos. en interaction avec la pos ipca redeviennent impossibles. 
     -- Il faut maj l'indicateur de possibilite des pos. en inter. avec ipca:
     for ipi in 1..tnpi(ica,ipca) loop
          tp(lpi(tppi(ica,ipca)+ipi-1).obj,lpi(tppi(ica,ipca)+ipi-1).pos).poss:=false;
     end loop;
  end if;
end RA ;

--=======================================================================
-- Procedure de strategie de placement sequentiel  --
-- Methode de F. CHIRIE ( Stage au COGIT (1992) ): --
-----------------------------------------------------
procedure PLACEMENT_SEQUENTIEL(Tbnomi:in out typtbnomi;
                               Tnp   :in typtnp;
                               tp    :in out typtp;
                               Tnpi  :in typtnpi;
                               tppi  :in typtppi;
                               lpi   :in typlpi;
                               Tnci  :in typtnci;
                               Tici  :in typtici) is
                                 
  Tdl, Tdc : float_array_type(1..tbnomi'last); 
  Tico, Tr : typtico(1..tbnomi'last);
  Rang : positive;
  PROPA: boolean:=true;
  Rep: character;
begin
    --put("  Propagation de contraintes ? (o/n) ");
    --get(Rep); new_line(2);
    --if rep='o' then
    --  propa:=true;
    --elsif rep='n' then
    --  propa:=false;
    --end if;
    propa:=true; -- par experience
    if propa then
      DET_DL(tnp,tnpi,tbnomi,tdl);
    else
      DET_DC(tnp,tnpi,tnci,tbnomi,tici,tdc);
    end if;

    declare
      blo:boolean:=false;
    begin
      ORDONNERC(tbnomi,tico,tr,tdc,tdl,propa);
      for rang in 1..tbnomi'last loop
	      DET_MP(Tico(rang),tbnomi,tnp,tp,tnpi,tppi,lpi,
			           tnci,tici,tico,tr,tdl,blo,propa);
	     -- Retour en arriere en cas de blocage, sans propagation !!!
	      if (Blo) and (not propa) then
		      RA(Tico(rang),tnp,tp,tnpi,tppi,lpi,tnci,tici,tr,tbnomi);
	      end if;
      end loop;
    end; --declare
end PLACEMENT_SEQUENTIEL;

--======================================================================
end PLACT_SEQUENTIEL;
