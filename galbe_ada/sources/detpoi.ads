--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with gen_io; use gen_io;
with inipan; use inipan;

package detpoi is
    
--==================================================================
-- Fonction determinant le poids de direction d'une �criture
function POIDIR(dir:in typdir; poi:in typpoidir) return typpoi;

--=================================================================
-- Procedure qui determine le poids de mutilation de la boite 
-- et renvoie un flag dans le cas ou 1 pixel interdit est ecras�:
Procedure POIMUB(tpoi   :in typtipoi; -- interpretation de l'image de mut sur la zone d'influence
                 origine:in point_type; -- origine de la boite dans tpoi
                 npx,npy:in positive;  -- nb de col et lig de tpoi occup�es par la boite 
                 ecrase :out boolean; -- flag pixel interdit
                 poimux :out typpoi);  -- poids de mutilation

--=============================================================================
-- Fonction determinant le poids de distance d'une �criture
function POIDNC(dncmax: in natural; dnc: in natural) return typpoi;

end detpoi;
