--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

---------------------------------------------------------------------------
---------------------------Source : INIPANS--------------------------------
---------------------------------------------------------------------------
-- Ce package concerne la definition et l'initialisation des structures ---
-- de donnees qui seront utilisees pour le positionnement automatique -----
-- des noms, ainsi que les declarations des procedures de chargement de ---
-- ces donnees: - Chargement des objets (nom, coordonnees, type) -----------
-- 	       - Determination des abreviations et des cesures des noms ---
--	       - Determination de l'emprise des noms ----------------------
-- 	       - Determination des zones d'influence des objets -----------
-- 	       - Determination des interactions entre objets --------------
---------------------------------------------------------------------------

with WIN32.WINDEF; -- definitions de types Windows
with WIN32.WINGDI; -- applications graphiques
with WIN32.WINNT;
with GEN_IO; use GEN_IO; 
with TEXT_IO;use TEXT_IO;
with ADA.DIRECT_IO;
with ADA.STREAMS.STREAM_IO;
with ADA.STRINGS.UNBOUNDED; use ADA.STRINGS.UNBOUNDED;
with ADA.CHARACTERS.LATIN_1;
with GB;
with sequential_io;

package INIPAN is
  
 -- types utilis�s dans l'interface 
 type Fix is delta 0.001 range -0.001..1_000_000.0;
 type Fix1 is delta 0.1 range 0.0..10_000_000.0;
 type Fix2 is delta 0.01 range -0.01..10_000_000.0;
 type Coeff is delta 0.01 range 0.00..1.01;-- l'intervalle de base n'inclut pas +1.0 sur les machines � compl�ment � deux
 type Poids is delta 0.1 range 0.0..10.0;
 
 -- types options de traitement
 type typoption is (enchaine,calcul,placement);
 type typsortie is (texte,mercator,rien);
 type typcoord is (centre,bas_gauche);
 
 package FIX_IO is new TEXT_IO.FIXED_IO(Fix);
 package FIX1_IO is new TEXT_IO.FIXED_IO(Fix1);
 package FIX2_IO is new TEXT_IO.FIXED_IO(Fix2);
 package COEFF_IO is new TEXT_IO.FIXED_IO(Coeff);
 package POIDS_IO is new TEXT_IO.FIXED_IO(Poids);
 package INT_IO is new TEXT_IO.INTEGER_IO(integer);
 package FLO_IO is new TEXT_IO.FLOAT_IO(float);
 Package integ_io is new sequential_io(integer); use integ_io;

 use FIX_IO, FIX1_IO, FIX2_IO, COEFF_IO, POIDS_IO, INT_IO, FLO_IO;

-- d�finition de l'octet utilis� pour la lecture de l'image Tiff 
type byte is range 0..255;
for byte'size use 8;
Package BYTE_IO is new ADA.DIRECT_IO(byte);use BYTE_IO;

 
------------------------------------------
-- CONSTANTES ET TYPES POUR LES BLASONS --
------------------------------------------

maxcar: constant := 100;
subtype string100 is string(1..maxcar);
subtype string120 is string(1..120);
subtype string30 is string(1..30);

maxblatopo : constant :=20;		    -- nb. max. de pict. par toponyme
maxsertopo : constant :=10;	        -- nb. max. de series par toponyme

Type Blason is record
	nom : string100 := (1..maxcar=>' ');
	lnom : positive range 1..maxcar;
	style : positive;
	dx : natural:=0;
  dy : natural:=0;
end record;
		
Type Blasonf is record
	nom : string100 := (1..maxcar=>' ');
	lnom : positive range 1..maxcar;
	style : positive;   -- indice absolu du blason
	dx : fix2:=0.00;
    dy : fix2:=0.00;
end record;

Type ABlason is array(positive range <>) of Blason;
Type ABlasonf is array(positive range <>) of Blasonf;

Type Serie is record
	nom : string100 := (1..maxcar=>' ');
	lnom : positive range 1..maxcar;
	pbla, nbla : positive;  -- pbla : indice du premier blason de la serie  -- nbla : nombre de blasons de la serie
	decvert:integer:=-1;    -- decalage vertical additionnel
end record;

Type Serief is record
	nom : string100 := (1..maxcar=>' ');
	lnom : positive range 1..maxcar;
	pbla, nbla : positive;
	decvert:fix2:=-0.01;
end record;

Type ASerie is array(positive range <>) of Serie;
Type ASerief is array(positive range <>) of Serief;

Type ACent is array(1..maxblatopo,1..2) of positive;


Type Pict is record
	x,y : integer:=0;
	lineref : positive range 1..2;
    dx,dy : natural;          
    ser,style : integer :=0;
	str : string100;
	lstr : integer range 0..maxcar;
	end record; 

Type APict is array(1..maxblatopo) of Pict;

Type AAPict is array(1..2) of APict;

Type Pser is record
	iser : integer:=0; 
	jhori,jvert,jalig : character := ' ';
	decvert:integer:=0;
end record;

Type APser is array(1..maxsertopo) of Pser;	

Type RLin is record
	empx,empy : integer:=0;
end record;

Type ALin is array (1..2) of RLin;	

Type AAlin is array (1..2) of ALin;

Type Aboite_type is array (1..2) of boite_type;

Type typhpix is array (positive range <>) of integer;

Type APlace is array(1..maxblatopo,1..2,1..2) of integer;

----------------------------------------------------
-- CONSTANTES ET TYPES POUR LES NUMEROS DE ROUTES --
----------------------------------------------------

type TypeLegende is record
  nom : string30 := (1..30=>' ');
  Ncar : positive range 1..30;
  NumGroupe : integer:=0;      -- NumGroupe=0 sera le groupe des arcs hors image apres clipage
end record;

-- debut modif 30.06.2005
-- NbMaxLegendes : constant :=300;
NbMaxLegendes : constant :=1000;
-- fin modif 30.06.2005

NbMaxMots : integer:=10; -- Nombre maximum de mots separables d'une ecriture a disposition
MaxNGeom : integer:=2000; -- Nombre max de points decrivant la geometrie d'une ligne de support
PAS_TOPO : float:=48.0; -- (48 = 0.3 mm carte en resolution 160)    -- 10.0;

---------------------------------------------------
-- CONSTANTES ET TYPES POUR LES COTES DE COURBES --
---------------------------------------------------

type TypeCote is record
  nom1 : string(1..6) := (1..6=>' ');
  Ncar1 : positive range 1..6;
  nom2 : string(1..6) := (1..6=>' ');
  Ncar2 : integer range 0..6;
end record;

NbMaxCotes : constant :=100;


--==================================================
--=========== Noms a positionner ===================
--================================================== 
 
-- Constantes 
eol : constant String := Ada.Characters.Latin_1.CR & Ada.Characters.Latin_1.LF;-- Fin de Ligne
tab : constant character:=Ada.Characters.Latin_1.HT;  -- tabulation

Resolution_Mercator: constant :=10078.74; -- pt/mm dans Mercator  

type typcateg is (interieur,mordant,exterieur,decentre,centre,desaxe,axe,Droit,Decale,Courbe,CAxe,CDesaxe);
type typobjet is (zonal,ponctuel,lineaire);
type TypCause is (mutilation, bord_image, Hors_image, MetaCourt, Ambigu, MNT_Plat, Direction_G, Sommet_Plat, Agencement);

type LIEN_TOPO_TYPE is record
  objet_type	:typobjet; -- type de l'objet
  num_objet 	:integer;  -- num�ro de l'objet dans le graphe des �critures
  mode_placement:typcateg; -- type de placement
  code : string30 := (1..30 => ' '); -- code typo de l'objet
end record;
pragma Pack(LIEN_TOPO_TYPE);

type TypTabChaine is array (positive range <>) of string100;
type TypTabNcar is array (positive range <>) of integer;
type TypTabLn is array (positive range <>) of Positive;


type NOM_TYPE is record
  id : integer;			-- identifiant de l'ecriture  
  police : integer;     -- Indice dans le tableau des polices 
  corps  : integer; 		-- Hauteur des majuscules (coord graphe)
  Hauteur	: float;  -- Corps en points
  coord_objet : point_type; -- Coord de l'�criture
  ln  : TypTabLn(1..NbMaxMots); 		-- Long du nom en coord graphe
  nl  : positive range 1..2; 	-- Nb de lignes du nom 
  ln1 : integer;	 	-- Long de la 1ere ligne du nom
  ln2 : integer;	 	-- Long de la 2eme ligne du nom
  chaine  : TypTabChaine(1..NbMaxMots):=(others =>(1..100 => ' ')); -- Ecriture
  Nb_Mots : integer:=1;   -- Nombre de mots separables (ecritures a disposition sur courbe)
  Ncar : TypTabNcar(1..NbMaxMots):=(others => 0); -- Nombre de caracteres des mots separables
  chaine1 : string100 := (1..100 => ' '); -- Ligne 1 de l'�criture
  chaine2 : string100 := (1..100 => ' '); -- ligne 2 de l'�criture
  ncar1	: integer range 0..maxcar; -- Nb de car de la 1ere ligne du nom.
  ncar2	:integer range 0..maxcar; -- Nb de car de la 2eme ligne du nom.
  TOPO : lien_topo_type;	   -- Infos sur le num et le type d'objet topo.
  CAPict : AAPict; --Tableau a 2 elements contenant les donnees 
				   --sur les blasons positionnes autour du nom.
				   --Si ce dernier est sur 1 ligne, seul le 1er element est  
				   --rempli, si sur 2 seul le 2eme. Si indifferemment sur 1 ou 2,
				   --le 1er contient les donnes sur la configuration 1 ligne
				   --le 2eme sur la configuration 2 lignes  	
  CAPSer : APSer;		   --tableau contenant des informations sur les series de blasons	
  CALin : AALin;		   --tableau contenant les emprises du nom, suivant qu'il s'ecrit sur 1,2,1 ou 2 lignes.
  nser,npic : natural :=0;   
  empblastop,empblasdown : Aboite_type := (((0,0),(0,0)),((0,0),(0,0))); --enveloppes des blasons superieurs et inferieurs

  p_choisie : natural :=0; 	   -- Indice de la position choisie.
  IDsymbole : string(1..30) := (1..30 => ' ');
  indice_symbole : integer:=0; -- indice du symbole dans Tab_symboles
  prop : float:=0.0; -- proportion du symbole pour cette ecriture
  Cause : typcause; -- :=MetaTordu;
end record ;

pragma Pack(NOM_TYPE);

type font_type is record
    code : string30 := (1..30 => ' '); -- Nom du code typo
    hauteur : float; -- Corps en points
	corps_plage : integer:=0; -- corps en unit�s PlaGe
    police :aliased Win32.wingdi.Logfont;  -- police windows charg�e
	InterLettres : float; -- Inter-lettres en points
end record;

-- ambiguit� entre codes typo
type typambi is record
  code1, code2 : string30 := (1..30 => ' ');
end record;

-- tableau des �critures 
type typTBNOMI is array (positive range <>) of nom_type;
-- type de tableau utilis� dans le placement s�quentiel
type typTICO is array (positive range <>) of positive;
-- type du tableau des zones d'influence des �critures
type typTBE is array (positive range <>) of boite_type;
type Acces_TypTBE is access TypTBE;
-- tableau des codes typo
type typtfont is array (positive range <>) of font_type;
-- tableau des ambiguites entre codes typo    
type typtambi is array(positive range <>) of typambi;
--tableau des noms des codes typo
type tabcode is array  (positive range <>) of string30;

-- Interactions entre objets
type typTNCI is array (positive range <>) of integer;
type typTICI is array (positive range <>,positive range <>) of integer;

-- MNT
type typMNT is array (integer range <>,integer range <>) of Angle_type;

--=============Variables issues de PublicWinPat============================

-- couleur des champs non obligatoires
 LLGray : constant gb.COLORREF := Win32.WinGDI.RGB(205, 205, 205);
 LLLGray : constant gb.COLORREF := Win32.WinGDI.RGB(235, 235, 235);

-- couleur de "�chec" du test de coh�rence
 MediumRed : constant gb.COLORREF := Win32.WinGDI.RGB(192, 0, 0);

-- Indicateurs de modifications de(s) : 
 FieldDataChanged : Boolean := False;-- champs de la Boite de Dialogue "Donn�es" 
 FieldCalculChanged : Boolean := False; -- champs de BD "Param�tres de Calcul"
 FieldRecuitChanged : Boolean := False; -- champs de BD "Param�tres de Placement" 
 PatChanged : Boolean := False;--du fichier *.PAT
 ParamCharge : boolean; -- indique si les parametres de Calcul et de placement ont ete charges

 -- Indicateurs des options choisies
 option: typoption := enchaine; -- traitement choisi
 sortie: typsortie := texte;    -- format des fichiers de sortie
 sep : character:=tab;          -- separateur pour le format de sortie texte
 coord: typcoord := centre;     -- point d'accroche en sortie des �critures
 boite: Boolean := True;        -- sauvegarde ou non des boites entourant les �critures
 option_mutil: Boolean := False; -- option image de mutilation
 option_route: Boolean := False; -- option graphe de routes
 option_appui: Boolean := False; -- option fichier de symboles d'appui
 option_blasons: Boolean := False; -- option blasons
 option_Horiz: Boolean := False; -- option ecritures horizontales
 option_creation: Boolean := False; -- option creation de l'image de mutilation
 SansP : Boolean:=True; -- presence d'ecritures sans position

 -- Donn�es du PAT (BD "Donn�es")
 InvScaleInt : Natural :=0;
 Ecritures : Unbounded_String := Null_Unbounded_String;
 StrNumRou : string(1..3):="Non";
 CheckStore : gb.bool:=gb.False;  -- stocke l'etat de NumRouCheckBox avant grisage
 StrCotesCourbes : string(1..3):="Non";
 CheckStore2 : gb.bool:=gb.False;  -- stocke l'etat de CotesCourbesCheckBox avant grisage
 StrEcriDispo : string(1..3):="Non";
 CheckStore3 : gb.bool:=gb.False;  -- stocke l'etat de EcriDispoCheckBox avant grisage
 DistanceCourbesTextStore, PenaliteSensTextStore : Unbounded_String := Null_Unbounded_String; -- stocke le texte de DistanceCourbesText et PenaliteSensText avant grisage
 Pat_Chain_SM_mort,Pat_calcul_SM_mort : boolean:=false;
 Pat_plcmt_SM_mort : boolean:=true;
 CodeTypo : Unbounded_String := Null_Unbounded_String;
 Ambiguite : Unbounded_String := Null_Unbounded_String;
 RoadG : Unbounded_String := Null_Unbounded_String;
 Symb : Unbounded_String := Null_Unbounded_String;
 Mut : Unbounded_String := Null_Unbounded_String;
 Resol_Mut : Natural :=0;
 Resolution_image: positive;	-- Resolution de l'image en pixels/mm
 SubsTab : Unbounded_String := Null_Unbounded_String;
 UBlasons : Unbounded_String := Null_Unbounded_String;
-- Definition : Unbounded_String := Null_Unbounded_String;
 Empilement : Unbounded_String := Null_Unbounded_String;
 Palette : Unbounded_String := Null_Unbounded_String;
 Estompage : Unbounded_String := Null_Unbounded_String;

 Xmin_image : Fix2 := 0.00;
 Ymin_image : Fix2 := 0.00;
 Xmax_image : Fix2 := 0.00;
 Ymax_image : Fix2 := 0.00;
 
 -- Param�tres de Calcul
 CurrentCalcul : Unbounded_String := Null_Unbounded_String;
 TempCalcul : Unbounded_String := Null_Unbounded_String;
 Pas_MaillemmInt : Fix := 0.0;
 Petit_Pas_MaillemmInt : Fix := 0.0;
 Pas_ContourmmInt : Fix := 0.0;
 EspaceInt : Fix := 0.0;
 Coup_optionInt : Natural := 0;
 Coup_obligInt : Natural := 0;
 dilxInt : Fix := 0.0;
 dilyInt : Fix := 0.0;
 dilatInt : Fix := 0.0;
 decalnoInt : Fix := 0.0;
 maxpInt : Natural := 0;
 maxnpiInt : Natural := 0;  
 Flag_AbrevInt : Natural := 0;
 Nb_Codes_RouteInt : Natural := 0;
 Tab_Codes_Route : array (1..100) of Unbounded_String := (others => Null_Unbounded_String);
 cpoimuInt : Coeff := 0.0;
 cpoidirInt : Coeff := 0.0;
 cpoidncInt : Coeff := 0.0;
 cpoipInt: Coeff := 0.0;
 cpoiiInt: Coeff := 0.0;
 poine : Poids := 0.0;
 poie : Poids := 0.0;
 poise : Poids := 0.0;
 pois : Poids := 0.0;
 poiso : Poids := 0.0;
 poio : Poids := 0.0;
 poino : Poids := 0.0;
 poin : Poids := 0.0;
 poiint : Poids := 0.0;
 poizi : Poids := 0.0;
 poizm : Poids := 0.0;
 poize : Poids := 0.0;
 cpoi_intersec_plusInt : Poids := 0.0;
 TabCotes : array(1..NbMaxCotes) of TypeCote;
 NbCotes : integer:=0;
 NS : integer:=0; -- nombre de s�ries de blasons
 NbBlasons : integer:=0; -- nombre de blasons
 DefinSeries : ASerief(1..20);
 DefinBlasons : ABlasonf(1..200);
 hesp_terrain : Fix2:=-0.01;
 vesp_terrain : Fix2:=-0.01;
 dectxt : natural range 0..1:=1;
 etalparam : integer range -1..100:=-1;
 TabLegendes : array(0..NbMaxLegendes) of TypeLegende;  -- 0 est l'indice de legende attribuee aux arcs hors image lors du clipage
 NbNoRou : integer:=0; -- Nombre de portions de routes a nommer (meta-troncons)
 NbLegendes : integer:=0;
 NbGroupes : Integer:=1;
 ParamHori,ParamNoRou,ParamBlas, ParamCotesCourbes, ParamHydro : integer range 0..1; -- presence des parametres utiles au placement des ecritures horizontales et des numeros de route resp.

     
-- Param�tres de placement
 CurrentRecuit : Unbounded_String := Null_Unbounded_String;
 TempRecuit : Unbounded_String := Null_Unbounded_String;
 Init_NiterInt : Natural :=0;
 Delta_NiterInt : Natural :=0;
 Init_SeuilInt : Fix1 :=0.0;
 Fac_SeuilInt : Natural :=0;
 Mini_SeuilInt : Fix2 :=0.0;
 OA_FactorInt : Fix1 :=0.0;

--=============Fin des variables issues de PublicWinPat============================

-- Graphes plage 
gros, grr, GrAlti : graphe_type; 
legos, legr   : legende_type;
infoos, infor, InfoAlti : info_type;

--======================================================================
--================== Parametres ========================================
--======================================================================
-- Indicateurs des options choisies
mutil: boolean:=false;
route: boolean:=false;
appui: boolean:=false;
blasons: boolean:=false;

-- Message d'erreur de d�roulement du PAT
Msg_Erreur: Unbounded_String := Null_Unbounded_String;
--Message_Erreur_CodeTypo : Unbounded_String := Null_Unbounded_String;

nomdirexe : string120; ncar_nde : integer;-- chemin courant au lancement du PAT (celui du .exe)
type typetabnomdir is array (1..30) of string120;
type typetabncrdir is array (1..30,1..3) of integer;
tabdirvec : typetabnomdir:=(others=>(others=>gb.NullChar));   -- chemin et nom des donn�es vecteur (ecr.horiz ou, a defaut, donnees lineaires) successivemnent ouverts  
tabdirpat : typetabnomdir:=(others=>(others=>gb.NullChar));   -- chemin et nom des fichiers pat successivement ouverts
tabncrdir : typetabncrdir:=(others=>(others=>0));
nbdir : integer:=0;
nomgraphe_routes_plage: string120; -- Nom du graphe des arcs � �viter de franchir
nomgraphe_plage: string120; -- Nom du graphe des �critures
GrosExiste : boolean:=false; -- Existence du graphe des objets surfaciques
GrrExiste : boolean:=false; -- Existence du graphe des donn�es lin�aires
GrAltiExiste : boolean :=false; -- Existence du graphe de l'altim�trie
Risque_Chevauchement : boolean; -- Indique si le nombre maximum de positions en interaction a ete atteint
TnpiMax : integer; -- Stocke le nombre maximum des interactions entre positions atteint
                   -- lorsque le nombre maximum de positions en interaction pris en compte (param�tre utilisateur) est trop faible

-- nomfichier_fichiers : string120; ncar_ff : integer; -- nom du fic *.pat
Nomfichier_PAT : string120; ncar_ff : integer; -- nom du fic *.pat
nomfichier_objet : string120; ncar_fo : integer; -- nom du fic des �critures horizontales, ou, � d�faut, nom du fic des donn�es lin�aires
nomfichier_image : string120; ncar_fi : integer; -- nom de l'image de mutilation
nomgraphe : string120; ncar_gr : integer;     -- nom du graphe des �critures
nomgraphe_routes : string120; ncar_grr : integer; -- nom du graphe des routes
nomfichier_param_calcul : string120; ncar_fpc : integer; -- nom du fic des param de calcul
nomfichier_param_recuit: string120; ncar_fpr : integer; -- nom du fic des param de placement
nomfichier_ambiguites: string120; ncar_amb : integer; -- nom du fic des ambiguit�s
nomfichier_polices : string120; ncar_pol : integer; -- nom du fic des codes typo
nomfichier_tds : string120; ncar_tds : integer; -- nom du fic de la table de substitution
nomfichier_symboles : string120; ncar_fs : integer; -- nom du fichier des symboles d'appui
ficblasons : string120; ncar_ficblasons : integer; -- nom du fichier d'attribution des blasons
nomfichier_Empilement : string120; ncar_fe : integer; -- nom du fichier d'empilement
nomfichier_Palette : string120; ncar_fp : integer; -- nom du fichier palette
nomfichier_Estompage : string120; ncar_Es : integer; -- nom du fichier de l'estompage
nomfichier_ImageToponymes : string120; ncar_It : integer; -- nom du fichier de l'image des �critures

file_tiff :  ada.streams.stream_io.file_type; -- fic image servant pour la lecture de l'en-tete
file_image: byte_io.file_type; -- fic image servant pour la lecture des pixels

Ptparmm: positive;
ptparmm_image: float;  pt_image   : positive:=1;
Pas_maillemm : float;  pas_maille : positive;
Petit_Pas_maillemm: float;  Petit_pas_maille: positive;
pas_contourmm: float;  pas_contour: positive; 
delta_xpix: natural;  
delta_ypix: natural;  
maxpi: integer;
maxnpi: integer;
espace: float;
Coup_option:integer;
Coup_oblig :integer;
dilx   : float;
dily   : float;
dilat  : float;
decalno: float;
maxP: positive; -- nombre max de positions candidates retenues pour �criture 
-- emprise coordonnees plage de l'image de mutilation
pno : point_type;
pne : point_type;
pso : point_type;
pse : point_type;
-- emprise coordonnees terrain de l'image de mutilation
tno : point_type_reel:=(float'last,float'first);
tne : point_type_reel;
tso : point_type_reel;
tse : point_type_reel:=(float'first,float'last);
-- 
-- emprise coordonnees terrain des donn�es vecteur
Vno : point_type_reel:=(float'last,float'first);
Vne : point_type_reel;
Vso : point_type_reel;
Vse : point_type_reel:=(float'first,float'last);

flag_abrev: integer; -- obsol�te
 
part_cercle:integer; -- initialis� dans Charge_Param = pas_contour en coord plage
Nb_codes_route:integer; -- nb de styles de routes
Tcodes_route: Tabcode(1..100);     -- tableau des styles de routes
ncode : integer;   -- nb de codes typo
Tcode : Tabcode(1..500);     -- tableau des codes typo

--===========================================================================
-- types utilis�s pour le calcul des poids
Subtype typpoi is float range 0.0..10.0;
Subtype typipoi is short_integer range 0..10;
type typtipoi is array(positive range<>, positive range<>) of typipoi;
type typdir is (ne,se,no,so,n,s,e,o,int,zi,zm,ze);
type typpoidir is record 
   ne,se,no,so,n,s,e,o,int,zi,zm,ze:typpoi; 
end record;

--===========================================
-- Declarations des parametres de mutilation:
--===========================================
-------------------------
-- Poids de la direction:
-------------------------
pardir  : typpoidir;
------------------------------------------------------------------
-- Coefficients pour la combinaison des sous-poids du poids propre:
------------------------------------------------------------------
cpoimu 	: typpoi;  -- coeff du poids de mutilation                             
cpoidir	: typpoi;  -- coeff du poids de direction
cpoidnc	: typpoi;  -- coeff du poids de distance
-----------------------------------------------------------------
-- Coefficients pour la combinaison des sous-poids du poids total:
-----------------------------------------------------------------
cpoip	: typpoi; -- coeff de poids propre
cpoii	: typpoi; -- coeff de poids d'interaction
------------------------------------------------------------------------
-- Param. d'augmentation du poidnc si p est de l'autre cote d'une route:
------------------------------------------------------------------------
cpoi_intersec_plus:typpoi;

--========================================================================
-- Fonctions et Proc�dures
--========================================================================

------------------------------------
-- procedures issues de PublicWinPat
------------------------------------
     
-- Charger les param�tres � partir des fichiers
------------------------------------------------ 
procedure LOADCALCUL(FileName : in String); 
procedure LOADRECUIT(FileName : in String);  


-- Remplir les champs des BDs des param�tres � partir des valeurs des param�tres 
-------------------------------------------------------------------------------
procedure SETDONNEES;
procedure SETCALCUL;
procedure SETRECUIT;

-- Ecrire les fichiers des param�tres
--------------------------------------
procedure WRITECALCUL(Filename : in String);
procedure WRITERECUIT(Filename : in String);  

-- Initialiser les param�tres � partir des champs des BDs
--------------------------------------------------------
procedure INITDONNEES;
--procedure INITCALCUL;
--procedure INITRECUIT;  
       
-- V�rifier la validit� des champs de param�tres, d'emprise, de r�solution
------------------------------------------------------------- 
function DONNEESISVALID return Boolean;
function DONNEESISVALID_MES return Boolean;
function CALCULISVALID return Boolean;
function RECUITISVALID return Boolean;  
function EMPRISEISVALID return Boolean;  
function RESOLUTIONISVALID return Boolean ;
 
-- Remise � z�ro 
---------------------------
procedure RAZ_CALCUL;
procedure RAZ_RECUIT;
procedure RAZ;

-- Affichage de la fenetre Parametres de Calcul selon les options
-----------------------------------------------------------------
procedure SHOWHIDECALCUL;

-- Rafra�chit l'affichage de contr�le de *.PAT    
----------------------------------------------
procedure REFRESHPAT;             
                         
-- R�cup�re nom complet d'un fichier  
------------------------------------
procedure SEARCH_BOX(Filename : out String; 
                     Title : in String; 
                     WType : in Integer;
                     Owner : Win32.Windef.HWND; 
                     Response : out Win32.Bool);
                     
                     
-----------------------------------------------------------------
-- Procedures d�finies dans Inipanb
-----------------------------------------------------------------

-- parcourt un fichier texte jusqu'� trouver et renvoyer le type cherch�                                           
Procedure SKIPANDGET(fic:in text_io.file_type;itemf:out float);
Procedure SKIPANDGET(fic:in text_io.file_type;itemi:out integer);
procedure SKIPANDGET(Fic : in text_io.file_type; item :out fix); 
procedure SKIPANDGET(Fic : in text_io.file_type; item :out coeff);
procedure SKIPANDGET(Fic : in text_io.file_type; item :out poids);
procedure SKIPANDGET(Fic : in text_io.file_type; item :out fix2);

-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat
procedure EXISTENCE_FIC( ficn: in out text_io.file_type;
                         n_lig: in text_io.count;
                         nom_fic: in out string;
                         ncar: in out integer);

-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat
procedure EXISTENCE_FIC2( ficn: in out text_io.file_type;
                         n_lig: in text_io.count;
                         nom_fic: in out string;
                         ncar: in out integer;						 
						 mutil: in out boolean;
						 cremu: in out boolean);

-- v�rifie l'existence du fichier dont le nom est stock� � la ligne n_lig du fic .pat                                                  
procedure EXISTENCE_FICOPT( ficn: in out text_io.file_type;
                            n_lig: in text_io.count;
                            nom_fic: in out string;
                            ncar: in out integer;
                            opt: in out boolean);

-- mise � jour de la fenetre de d�roulement du PAT                                                                                 
procedure AFFICHAGE_DEROULEMENT(etape: in out gb.Label; top: in integer);

-- trouve les indices des separateurs dans une chaine de type Geoconcept
Procedure INDICE_SEPARATEUR(Ligne : in string; -- Chaine de caract�res
                            LLigne : in integer; -- Longueur de la chaine
                            Separateur : in character ;	-- Separateur recherch�
                            Ptr_Indice : out liens_access_type); -- Pointeur des indices des separateurs

Procedure INDICE_SEPARATEUR2(Ligne : in string;		-- Chaine de caract�res
                           LLigne : in integer;		-- Longueur de la chaine
                           Separateur : in character ;	-- Separateur recherch�
                           Ptr_Indice : out liens_access_type; -- Pointeur des indices des separateurs
						   compteur : out integer);

-- teste l'existence de polices Windows correspondant aux polices du
-- fichier des codes typo                            
procedure TEST_TABPOLICE(hWnd: in Win32.winnt.Handle);

-- outil de conversion des tailles des codes typo
procedure OUTIL_CODESTYPO(hWnd: in Win32.winnt.Handle;
                          nom_ficin: in string;
                          nom_ficout: in string;
                          option: in integer);
                          
-- chargement des parametres de calcul
procedure CHARGE_PARAM;

-- remplissage de tbnomi et calcul d'infos suppl�mentaires sur les �critures                                                            
procedure DET_NOMS( tbnomi:in out typtbnomi;
                    -- nb_ecritures : in integer;
                    tzi   :in out typtbe; -- Tableau des zones d'influence
                    maxbla : in out integer;
		                vesp : in out integer;
                    InvEchelle: in integer;
                    Resolution : in positive;
					Minimum_terrain : in point_type_reel;
                    hWnd      : in Win32.winnt.Handle); -- Handle de fen Windows

-- determination des interactions entre �critures                    
procedure DET_CI(tbnomi:in typtbnomi;
                 tzi   :in typtbe; -- tableau des zones d'influence
                 tnci  :in out typtnci;
                 tici  :in out typtici);

-- cr�ation du tableau des ambiguit�s entre codes typo
Procedure CHARGE_AMBIGUITES(tambi:in out typtambi; nb_ambi : out natural);

-- calcule la longueur d'une �criture (dans Insere_nouvelles_positions)   
procedure DET_LONG(tbnomi: in out typtbnomi;
                   n: in integer;
                   hWnd: in Win32.winnt.Handle);

-- calcule le nb de codes typo diff�rents utilis�s dans tbnomi                                   
Function NB_CODE(tbnomi:in typtbnomi) return positive;
 
procedure charge_fontes(info_font: out typtfont; nbfont: out integer);


end INIPAN;
