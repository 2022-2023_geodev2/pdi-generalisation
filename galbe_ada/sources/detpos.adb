--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with geometrie; use geometrie;
with manipboite; use manipboite;
with detpoi; use detpoi;
with ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
with math_int_basic; use math_int_basic;
with gb;
with dialog3;
with window1;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with verifmut; use verifmut;
with win32, win32.winbase;
with Interfaces.C;
with norou; use norou;


package BODY DETPOS is
 
--------------------------------------------------------------------------------
procedure comp(	listp:	in out point_liste_type;
                np:	in out natural;
                pas:	in positive ) is
	
  nptmp: natural:= 0;

  -- decompose la liste des arc (SI - SF ) en liste de points regulierement repartis
  ----------------------------------------------------------------------------------    
  procedure decomp(point : in out point_liste_type;  -- tableau des points
          		   np : in out natural;    -- nb de points du tableau
                   pas : in positive) is 

  A,DX, Dy :float;
  rang : integer;
  NPT : natural:=np; -- nb de points du tableau
  IX1, Iy1, IX0, Iy0 :integer; -- SI - SF de l'arc
  I,J : integer;
  NPP: integer;  -- nb de points rajouter
  PO :Point_liste_type(1..100000);    -- tableau des points rajoutes 
  ncar : integer;
  ch : string(1..20);

  begin
   IX1:=Point(1).Coor_X;
   IY1:=Point(1).Coor_Y;
   I:=1;
   For ind in 1..np-1 loop
       I:=I+1;
       IX0:=IX1;
       IY0:=IY1;
       IX1:=Point(I).Coor_X;
       IY1:=Point(I).Coor_Y;
       DX:=float(IX1-IX0);
       DY:=float(IY1-IY0);
       if abs(DX)>abs(DY) then
           A:=DY/DX;
           if DX > 0.0 then
               NPP:=(integer(DX)-1)/pas;  --nb de points a rajouter
               For II in 1..NPP loop
                   PO(II).Coor_X:=IX0+II*pas;
                   PO(II).Coor_Y:=IY0+integer(float(II)*A)*pas;
               End loop; 
               goto cent;
           else
               NPP:=(integer(-DX)-1)/pas; 
               For II in 1..NPP loop
                   PO(II).Coor_X:=IX0-II*pas;
                   PO(II).Coor_Y:=IY0-integer(float(II)*A)*pas;
               End loop; 
               goto cent; 
           end if;    
       end if;
       if DY = 0.0 then     -- point double (DX=0; DY=0)
           For k in I..NPT loop
               POINT(k-1):=POINT(k);-- on "ecrase" le point double
           end loop;    
           I:=I-1;  -- on revient un rang en arriere
           NPT:=NPT-1;
           goto cinquante; -- on traite le point suivant
       end if;
       A:=DX/DY;
       if DY>0.0 then
           Npp:=(integer(DY)-1)/pas;
           for II in 1..NPP loop
               PO(II).Coor_X:=IX0+integer(float(II)*A)*pas;
               PO(II).Coor_y:=IY0+II*pas;
           end loop;
       else
           NPP:=(integer(-DY)-1)/pas;
           for II in 1..NPP loop
               PO(II).Coor_X:=IX0-integer(float(II)*A)*pas;
               PO(II).Coor_y:=IY0-II*pas;
           end loop;
       end if;
       <<cent>> if NPP=0 then goto cinquante; end if;
       J:=I+NPP; -- rang du premier point suivant
       rang:=NPT;
       NPT:=NPT+NPP;
       -- on decale les sommets pour intercaler les npp points crees
       For k in reverse I+NPP..NPT loop
           Point (k):=point(rang);
           rang:=rang-1;
       End loop;  
       -- on insere les nouveaux points
       For k in 1..NPP loop
           Point (I-1+k):=PO(k);
       End loop;
       I:=J; 
       <<cinquante>> I:=I;
   End loop;       
   np:=NPT;  
  end decomp;
-----------------------------------------------------------------------------                             
begin
  decomp(listp,np,2);
  nptmp:= 1;
  listp(nptmp):= listp(1);
  if np/=2 then
	for ip in 2..np-1 loop
      if ip mod pas = 0 then 
		nptmp:= nptmp+1;
	    listp(nptmp):= listp(ip); -- N.B.: nptmp <= ip.
	  end if;	
	end loop; 
  end if;
  nptmp:= nptmp+1;
  listp(nptmp):= listp(np);
  np:= nptmp;
end comp;
--------------------------------------------------------------------------------
  
  
         
--=========================================================================
-- Prodedures qui determinent, soit � partir d'une maille, soit � partir --
-- d'un contour, une liste de points � partir desquels seront determinees - 
-- des positions. 
--=========================================================================
  
procedure POINTS_DE_CERCLE (no: in integer;
		   	                    tbnomi: in typtbnomi;
		  	                    n: in out integer;
		  	                    contpt: in out point_liste_type;
		   	                    iest,inord,iouest,isud: out natural;
								viewer : in boolean) is
                               
affich: affichage_type;
noeud: noeud_type;
rayon: float; -- en mm.
pi: constant float:=3.141593;
x,y: integer:=0;
p : integer;

begin
  noeud:=Gr_Noeud(gros,tbnomi(no).topo.num_objet);
  affich:=Gr_Legende_P(legos,noeud.att_graph);
  rayon:=affich.largeur/2.0;
  rayon:=rayon*float(ptparmm); -- Conversion des mm en points.
  if viewer then
  	p:=20*integer(pi*rayon/float(ptparmm)); -- un point tous les 0.1 mm pour les sorties graphiques viewer 
  else
    p:=min(integer(2.0*pi*rayon/float(part_cercle))+1,maxP);
  end if;
  for i in 1..p loop -- d�coupage du cercle en "part_cercle" secteurs
    x:=tbnomi(no).coord_objet.coor_x+integer(rayon*cos(pi*2.0*float(i)/float(p)));
    y:=tbnomi(no).coord_objet.coor_y+integer(rayon*sin(pi*2.0*float(i)/float(p)));
    contpt(p-i+1):=(x,y);
  end loop; -- i
  n:=p;
  isud:=p/4+1;
  iest:=1;
  inord:=3*P/4+1;
  iouest:=p/2+1;
end POINTS_DE_CERCLE;

--====================================================================
procedure LISTE_SUR_MAILLE(no: in integer;
                           tbnomi: in typtbnomi;
		  	                   npts: in out integer;
		  	                   listpts: in out point_liste_type) is
                             
maxi,maxj:integer;
pasm: positive;
b: boite_type;--Cadre du maillage
ntemp:integer:=0;
listemp:point_liste_type(1..listpts'last);

begin
  if tbnomi(no).topo.mode_placement=centre then
    ntemp:=1;
    listemp(1):=(tbnomi(no).coord_objet.coor_x,tbnomi(no).coord_objet.coor_y);
  else
    -- Determination de la boite b correspondant au cadre du maillage:
    ntemp:=0;
    if tbnomi(no).topo.objet_type=ponctuel 
    and tbnomi(no).topo.mode_placement=interieur then
      b.minimum.coor_x:=tbnomi(no).coord_objet.coor_x
                        -tbnomi(no).ln(1)-Petit_pas_maille;
      b.minimum.coor_y:=tbnomi(no).coord_objet.coor_y
                        -tbnomi(no).corps-Petit_pas_maille;
      b.maximum.coor_x:=tbnomi(no).coord_objet.coor_x
                        +Petit_pas_maille;
      b.maximum.coor_y:=tbnomi(no).coord_objet.coor_y
                        +Petit_pas_maille;
      pasm:=Petit_pas_maille; -- =Petit_Pas_maillemm*ptparmm
    elsif tbnomi(no).topo.objet_type=zonal then
      b:=Gr_ENCOMBREMENT_FACE(gros,tbnomi(no).topo.num_objet);
      if tbnomi(no).ln(1)<(b.maximum.coor_x-b.minimum.coor_x) then
        b.maximum.coor_x:=b.maximum.coor_x-tbnomi(no).ln1;
        b.maximum.coor_y:=b.maximum.coor_y-tbnomi(no).corps;
        pasm:=pas_maille;
      else
        b.minimum.coor_x:=tbnomi(no).coord_objet.coor_x
                          -tbnomi(no).ln(1)/2-tbnomi(no).corps;
        b.minimum.coor_y:=tbnomi(no).coord_objet.coor_y
                          -tbnomi(no).corps/2-tbnomi(no).corps;
        b.maximum.coor_x:=tbnomi(no).coord_objet.coor_x
                          -tbnomi(no).ln(1)/2+tbnomi(no).corps;
        b.maximum.coor_y:=tbnomi(no).coord_objet.coor_y
                          -tbnomi(no).corps/2+tbnomi(no).corps;
        pasm:=Petit_pas_maille;
      end if;
    end if;
     
    -- Determination de la liste de points a partir du maillage:
    maxi:=(b.maximum.coor_x-b.minimum.coor_x)/pasm;
    maxj:=(b.maximum.coor_y-b.minimum.coor_y)/pasm;

    for i in 0..maxi loop
      for j in 0..maxj loop
        ntemp:=ntemp+1;
        listemp(ntemp).coor_x:=b.minimum.coor_x+i*pasm;
        listemp(ntemp).coor_y:=b.minimum.coor_y+j*pasm;
      end loop;
    end loop;
  end if;

  npts:=ntemp;
  listpts(1..ntemp):=listemp(1..ntemp);
  
end liste_sur_maille;


--=========================================================================

procedure LISTE_SUR_CONTOUR(no: in integer;
                            tbnomi: in typtbnomi;
		                    npts: out integer;
		                    listpts: out point_liste_type;
		                    iest,inord,iouest,isud: out natural;
		                    iest_plus,inord_plus,iouest_plus,isud_plus: out natural) is

b : boite_type;
n,nc,np,nba,jj,npoints : integer;
ptemp : point_type;
listp,contpt,contptp : point_liste_type(1..listpts'last);
TARCS : liens_array_type(1..infoos.max_arc_fac); 
est,nord,ouest,sud : natural;
est_plus,nord_plus,ouest_plus,sud_plus : natural;
ch : string(1..20);
ncar : integer;
cos1,cos2,d1,d2 : float:=0.0;
p1,p2 : integer;

begin
  if tbnomi(no).topo.objet_type=zonal then
    n:=0;
    nba:= Gr_face(gros,tbnomi(no).topo.num_objet).Nombre_arc;
    tarcs(1..nba):= Gr_arcs_de_Face(gros,tbnomi(no).topo.num_objet);
    for i in 1..nba loop
      if tarcs(i)<0 then
        Gr_Points_d_arc(gros,-tarcs(i),contptp,nc);
        jj:=nc;
        for k in 1..nc/2 loop
          ptemp:=contptp(k);
          contptp(k) :=contptp(jj);
          contptp(jj):=ptemp;
          jj:=jj-1;
        end loop; -- k
      else
        Gr_Points_d_arc(gros,tarcs(i),contptp,nc);
      end if;
      for j in 1..nc loop contpt(n+j):= contptp(j); end loop;
      n:= n+nc;
    end loop; -- i
    b := Gr_ENCOMBREMENT_FACE(gros,tbnomi(no).topo.num_objet);
    for ipt in 1..n loop
      if contpt(ipt).coor_y=b.maximum.coor_y then NORD:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_x=b.maximum.coor_x then EST:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_y=b.minimum.coor_y then SUD:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_x=b.minimum.coor_x then OUEST:=ipt; exit; end if;
    end loop;
    
    -- determinons le sens de parcours de la surface.
    --Je considere le point le plus au sud et je prend le point p1=sud-1 et p2=sud+1  
    for ipt in 1..n-1 loop
      if contpt(ipt).coor_y=b.minimum.coor_y then SUD:=ipt; exit; end if;
    end loop;
    if sud=1 then p1:=n-1; p2:=2;
    else  p1:=sud-1; p2:=sud+1;
    end if;
    d1:=sqrt(float((contpt(p1).coor_x-contpt(sud).coor_x)**2 
        + (contpt(p1).coor_y-contpt(sud).coor_y)**2));
    d2:=sqrt(float((contpt(p2).coor_x-contpt(sud).coor_x)**2 
        + (contpt(p2).coor_y-contpt(sud).coor_y)**2));
    cos2:= float((contpt(p2).coor_x-contpt(sud).coor_x))/d2;
    cos1:= float((contpt(p1).coor_x-contpt(sud).coor_x))/d1;
    if cos2>cos1 then --sens trigo => on inverse
      for i in 1..integer(n/2) loop
        ptemp:=contpt(i);
        contpt(i):=contpt(n-i+1);
        contpt(n-i+1):=ptemp;
      end loop;
    end if;
    for ipt in 1..n loop
      if contpt(ipt).coor_y=b.maximum.coor_y then NORD:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_x=b.maximum.coor_x then EST:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_y=b.minimum.coor_y then SUD:=ipt; exit; end if;
    end loop;
    for ipt in 1..n loop
      if contpt(ipt).coor_x=b.minimum.coor_x then OUEST:=ipt; exit; end if;
    end loop;
  elsif tbnomi(no).topo.objet_type=ponctuel then
    POINTS_DE_CERCLE(no,tbnomi,n,contpt,est,nord,ouest,sud,false);
  end if;

  -- Mettons le nord en debut et fin de liste des pts du contour:
  if nord>=2 then
	  for ipt in 2..nord loop contpt(ipt+n-1):=contpt(ipt); end loop;
    for ipt in 1..n loop contpt(ipt):=contpt(ipt+nord-1); end loop;
    if est<nord then est:=est+n-nord; 
    else est:=est-nord+1; end if;
    if sud<=nord then sud:=sud+n-nord; 
    else sud:=sud-nord+1; end if;
    if ouest<=nord then ouest:=ouest+n-nord; 
    else ouest:=ouest-nord+1; end if;
    nord:=1;
  else
    if ouest=nord then ouest:=n; end if;
  end if;
  
  -- Determinons une liste de pts regulierement choisis sur le contour:
  npoints:=0;
  for ipt in 1..est loop
    listp(ipt):=contpt(ipt);
  end loop;
  np:=est;
  if tbnomi(no).topo.objet_type=zonal then
    -- COMP(listp,np,pas_contour); 
    COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  end if;

  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);
  end loop;
  npoints:=npoints+np;
  iest:=npoints;

  listp(1):=(contpt(est).coor_x,contpt(est).coor_y);
  listp(2):=(contpt(est).coor_x,contpt(est).coor_y-tbnomi(no).corps);
  np:=2;
  -- COMP(listp,np,pas_contour); 
  COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);
  end loop;
  npoints:=npoints+np;
  iest_plus:=npoints;

  for ipt in est..sud loop 
    listp(ipt-est+1):=contpt(ipt);
  end loop;

  np:=sud-est+1;
  if tbnomi(no).topo.objet_type=zonal then
    -- COMP(listp,np,pas_contour);
    COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  end if;
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);
  end loop;
  npoints:=npoints+np;
  isud:=npoints;

  listp(1):=(contpt(sud).coor_x,contpt(sud).coor_y);
  listp(2):=(contpt(sud).coor_x-tbnomi(no).ln(1),contpt(sud).coor_y);
  np:=2;
  -- COMP(listp,np,pas_contour);
  COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);
  end loop;
  npoints:=npoints+np;
  isud_plus:=npoints;

  for ipt in sud..ouest loop listp(ipt-sud+1):=contpt(ipt); end loop;
  np:=ouest-sud+1;
  if tbnomi(no).topo.objet_type=zonal then
    -- COMP(listp,np,pas_contour);
    COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  end if;
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);	
  end loop; 
  npoints:=npoints+np;
  iouest:=npoints;	 

  listp(1):=(contpt(ouest).coor_x,contpt(ouest).coor_y-tbnomi(no).corps);
  listp(2):=(contpt(ouest).coor_x,contpt(ouest).coor_y);
  np:=2;
  -- COMP(listp,np,pas_contour);
  COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);	
  end loop; 
  npoints:=npoints+np;
  iouest_plus:=npoints;

  for ipt in ouest..n loop listp(ipt-ouest+1):=contpt(ipt); end loop;
  np:=n-ouest+1; 
  if tbnomi(no).topo.objet_type=zonal then
    -- COMP(listp,np,pas_contour);
    COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  end if;
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);	
  end loop; 
  npoints:=npoints+np;
  inord:=npoints;

  listp(1):=(contpt(nord).coor_x-tbnomi(no).ln(1),contpt(nord).coor_y);
  listp(2):=(contpt(nord).coor_x,contpt(nord).coor_y);

  np:= 2;
  -- COMP(listp,np,pas_contour);
  COMP(listp,np,max(integer(taille_ligne(listp,np)/float(MaxP)),pas_contour)); -- voir la procedure  de BIBCONT --
  for ipt in 1..np loop
    listpts(npoints+ipt):=listp(ipt);	
  end loop; 
  npoints:=npoints+np;
  inord_plus:=npoints;

  npts:=npoints;
    
end Liste_Sur_Contour;


--==================================================================
-- Procedures qui determinent la geometrie des positions a partir --
-- d'un point d'une liste. 
--==================================================================

-- determination du coin inf gauche de la 2eme lig � partir de celui de la 1ere lig
procedure det_coin2 ( no: in positive; 
		     		          tbnomi: in typtbnomi;  
		     		          p: in out position_type) is
l1,l2:natural;
begin
  p.coin2.coor_y:=p.coin1(1).coor_y-tbnomi(no).corps
                  -integer(espace*float(tbnomi(no).corps));
  l1:=tbnomi(no).ln1;
  l2:=tbnomi(no).ln2;			
  p.coin2.coor_x:=p.coin1(1).coor_x+(l1-l2)/2;
  -- decalage � gauche de 0.45*corps pour compenser l'ajout du caractere '-'
  if tbnomi(no).chaine2(1)='-' then
    p.coin2.coor_x:=p.coin2.coor_x-integer(0.45*float(tbnomi(no).corps));
  end if;
end det_coin2;


-- determination du coin inf gauche de la 1ere lig � partir de celui de la 2eme lig
procedure det_coin1 (no: in positive; 
		                 tbnomi: in typtbnomi; 
		                 p: in out position_type) is
l1,l2:natural;
begin
  p.coin1(1).coor_y:=p.coin2.coor_y+tbnomi(no).corps 
			            +integer(espace*float(tbnomi(no).corps));
  l1:=tbnomi(no).ln1;
  l2:=tbnomi(no).ln2;			
  p.coin1(1).coor_x:=p.coin2.coor_x-l1/2+l2/2;
end det_coin1;


-- modification de la geometrie des positions determinees par DET_P par prise en compte des blasons
procedure MODIF_POS(nobj : in integer; tbnomi : in typtbnomi;	tpp : in out typtpp; npp : in out typnpp) is

haut1:float:=0.0;
haut2:float:=0.0;
c1x,c1y,c2x,c2y,d1x,d1y,d2x,d2y:float:=0.0;
deltaxg,deltaxd,deltayh,deltayb,deltaxg1,deltaxg2,deltaxd1,deltaxd2:float:=0.0;
minix,maxix:float:=0.0;
ligne:integer:=0;

begin
  if tbnomi(nobj).npic>0 then 
    for compt in 1..npp loop
		  if tpp(compt).nl=1 then
        ligne:=1;
        if (tbnomi(nobj).topo.mode_placement/=interieur) and (tbnomi(nobj).topo.mode_placement/=centre) then
			    if tbnomi(nobj).empblastop(ligne).minimum.coor_x<tbnomi(nobj).empblasdown(ligne).minimum.coor_x then
				    minix:=float(tbnomi(nobj).empblastop(ligne).minimum.coor_x);
    			else 
				    minix:=float(tbnomi(nobj).empblasdown(ligne).minimum.coor_x);
			    end if;
			    if minix<0.0 then 
				    deltaxg:=-minix;
			    end if;
			    if tbnomi(nobj).empblastop(ligne).maximum.coor_x > tbnomi(nobj).empblasdown(ligne).maximum.coor_x then
				    maxix:=float(tbnomi(nobj).empblastop(ligne).maximum.coor_x);
			    else 
				    maxix:=float(tbnomi(nobj).empblasdown(ligne).maximum.coor_x);
			    end if;
			    if maxix>float(tbnomi(nobj).ln(1)) then 
				    deltaxd:=(maxix-float(tbnomi(nobj).ln(1)));
			    end if;
    			haut1:=float(tbnomi(nobj).empblastop(ligne).maximum.coor_y - tbnomi(nobj).empblastop(ligne).minimum.coor_y);
			    haut2:=float(tbnomi(nobj).empblasdown(ligne).maximum.coor_y - tbnomi(nobj).empblasdown(ligne).minimum.coor_y);
			    deltayh:=haut1+3.0;
			    deltayb:=haut2+3.0;
          if tpp(compt).dir=n then 
			      c1x:=0.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
			    end if;
			    if tpp(compt).dir=o then 
			      c1x:=0.0;c1y:=0.0;c2x:=1.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=0.0;	
		    	end if;
		    	if tpp(compt).dir=s then 
		      	c1x:=0.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=1.0;d2x:=0.0;d2y:=0.0;	
	    		end if;
	    		if tpp(compt).dir=e then 
	      		c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=0.0;	
	    		end if;
	    		if tpp(compt).dir=ne then 
		       	c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
		    	end if;
	    		if tpp(compt).dir=no then 
	      		c1x:=0.0;c1y:=0.0;c2x:=-1.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
	    		end if;
	    		if tpp(compt).dir=se then 
	      		c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=-1.0;d2x:=0.0;d2y:=0.0;	
	    		end if;
	    		if tpp(compt).dir=so then 
	      		c1x:=0.0;c1y:=0.0;c2x:=-1.0;c2y:=0.0;d1x:=0.0;d1y:=-1.0;d2x:=0.0;d2y:=0.0;	
	    		end if;
	    		tpp(compt).coin1(1).coor_x:=integer(float(tpp(compt).coin1(1).coor_x) + c1x*deltaxg + d1x*deltayh + c2x*deltaxd + d2x*deltayb);
	    		tpp(compt).coin1(1).coor_y:=integer(float(tpp(compt).coin1(1).coor_y) + c1y*deltaxg + d1y*deltayh + c2y*deltaxd + d2y*deltayb); 
    		end if;
      else
        ligne:=2;
		    if (tbnomi(nobj).topo.mode_placement/=interieur) and (tbnomi(nobj).topo.mode_placement/=centre) then
			    if tbnomi(nobj).empblastop(ligne).minimum.coor_x<0 then
				    deltaxg1:=-float(tbnomi(nobj).empblastop(ligne).minimum.coor_x);
			    end if;
			    if tbnomi(nobj).empblasdown(ligne).minimum.coor_x < 0 then
				    deltaxg2:=-float(tbnomi(nobj).empblasdown(ligne).minimum.coor_x);
			    end if;
			    if tbnomi(nobj).empblastop(ligne).maximum.coor_x > tbnomi(nobj).ln1 then
				    deltaxd1:=-float(tbnomi(nobj).empblastop(ligne).maximum.coor_x);
			    end if;
			    if tbnomi(nobj).empblasdown(ligne).maximum.coor_x > tbnomi(nobj).ln2 then
				    deltaxd2:=-float(tbnomi(nobj).empblasdown(ligne).minimum.coor_x);
			    end if;
			    haut1:=float(tbnomi(nobj).empblastop(ligne).maximum.coor_y - tbnomi(nobj).empblastop(ligne).minimum.coor_y);
			    haut2:=float(tbnomi(nobj).empblasdown(ligne).maximum.coor_y - tbnomi(nobj).empblasdown(ligne).minimum.coor_y);
			    if tpp(compt).dir=s then 
			      c1x:=0.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=-1.0;d2x:=0.0;d2y:=0.0;	
			    end if;
			    if tpp(compt).dir=e then 
			      c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=0.0;	
			    end if;
			    if tpp(compt).dir=se then 
      			c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=-1.0;d2x:=0.0;d2y:=0.0;	
			    end if;
			    if tpp(compt).dir=so then 
			      c1x:=0.0;c1y:=0.0;c2x:=-1.0;c2y:=0.0;d1x:=0.0;d1y:=-1.0;d2x:=0.0;d2y:=0.0;	
			    end if;
			    if tpp(compt).dir=o then 
			      c1x:=0.0;c1y:=0.0;c2x:=-1.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=0.0;	
			    end if;
			    if tpp(compt).dir=n then 
			      c1x:=0.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
			    end if;
			    if tpp(compt).dir=ne then 
			      c1x:=1.0;c1y:=0.0;c2x:=0.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
			    end if;
			    if tpp(compt).dir=no then 
			      c1x:=0.0;c1y:=0.0;c2x:=-1.0;c2y:=0.0;d1x:=0.0;d1y:=0.0;d2x:=0.0;d2y:=1.0;	
			    end if;
			    if (tpp(compt).dir=s) or (tpp(compt).dir=e) or (tpp(compt).dir=so) or (tpp(compt).dir=o) or (tpp(compt).dir=se) then
				    tpp(compt).coin1(1).coor_x:=integer(float(tpp(compt).coin1(1).coor_x) + c1x*deltaxg1 + d1x*deltayh + c2x*deltaxd1); 
				    tpp(compt).coin1(1).coor_y:=integer(float(tpp(compt).coin1(1).coor_y) + c1y*deltaxg1 + d1y*deltayh + c2y*deltaxd1);
				    det_coin2(nobj,tbnomi,tpp(compt));
			    else
				    tpp(compt).coin2.coor_x:=integer(float(tpp(compt).coin1(1).coor_x) + c1x*deltaxg2 + d2x*deltayb + c2x*deltaxd2); 
				    tpp(compt).coin2.coor_y:=integer(float(tpp(compt).coin1(1).coor_y) + c1y*deltaxg2 + d2y*deltayb + c2y*deltaxd2);
				    det_coin1(nobj,tbnomi,tpp(compt)); 
			    end if;
		    end if;
		  end if;	
	  end loop;
  end if;
end MODIF_POS;

--========================================================================
-- calcul des positions possibles de tbnomi(nobj)
procedure DET_P(nobj: in integer;
		 		        tbnomi: in typtbnomi;
		 		        ipt: in integer;
		 		        listpts: in point_liste_type;
		 		        iest,inord,iouest,isud: in natural;
         		        iest_plus,inord_plus,iouest_plus,isud_plus: in natural;
		 		        tpp: in out typtpp;
		 		        npp :in out typnpp) is
             
ax,ay,bx,by,b1,b2: float:=0.0;
hno:float:=float(tbnomi(nobj).corps);
lno:float:=float(tbnomi(nobj).ln(1));
lno1:float:=float(tbnomi(nobj).ln1);
lno2:float:=float(tbnomi(nobj).ln2);
contour : surf_type(1,gr_infos(gros).Max_pts_arc);
point_interm : point_type;
flag1,flag2,flag3,flag4,flag5,flag6,flag7,flag8,flag9,flag10 : boolean;
  
begin
  npp:=0;
  if tbnomi(nobj).nl=1 then -- Positions a 1 ligne --
	   npp:=npp+1;
     tpp(npp).nl:=1;
	   if tbnomi(nobj).topo.mode_placement=interieur then
	      tpp(npp).coin1(1):=(listpts(ipt).coor_x,listpts(ipt).coor_y);
        if tbnomi(nobj).topo.objet_type=ponctuel then
           tpp(npp).dir:=int;
           -- (nom sur 1 ligne)
        else
           -- Examen de la position des 4 coins et du centre de l'ecriture
           -- par rapport a la surface a nommer :
           -- zi : les 4 coins sont dans la surface
           -- zm : Entre 1 et 3 coins sont hors de la surface
           -- ze : les 4 coins sont hors de la surface
           Gr_contours_de(gros,tbnomi(nobj).topo.num_objet,contour);
           flag1:=Est_il_dans_surface(tpp(npp).coin1(1),contour);
           point_interm:=tpp(npp).coin1(1)+(0,tbnomi(nobj).corps);
           flag2:=Est_il_dans_surface(point_interm,contour);
           point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln(1),tbnomi(nobj).corps);
           flag3:=Est_il_dans_surface(point_interm,contour);
           point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln(1),0);
           flag4:=Est_il_dans_surface(point_interm,contour);
           point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln(1)/2,tbnomi(nobj).corps/2);
           flag5:=Est_il_dans_surface(point_interm,contour);
           if flag1 and flag2 and flag3 and flag4 and flag5 then
              tpp(npp).dir:=zi;
           else
             if flag1=false and flag2=false and flag3=false
               and flag4=false and flag5=false then
                tpp(npp).dir:=ze;
             else
                tpp(npp).dir:=zm;   
             end if;
           end if;  
 	      end if;
     else
        if tbnomi(nobj).topo.mode_placement=centre then
	         tpp(npp).coin1(1):=(listpts(ipt).coor_x-(tbnomi(nobj).ln(1)/2),
			     listpts(ipt).coor_y-(tbnomi(nobj).corps/2));
	         tpp(npp).dir:=int;
	      else  -- placement d�cal�
	         if ipt<=iest then -- *NORD-EST* 
		          tpp(npp).dir:=ne; 
		          ax:=1.0;ay:=1.0;bx:=0.0;by:=0.0;
	         end if;
	         if ipt>=iest+1 and ipt<=iest_plus then -- *EST* 
		          tpp(npp).dir:=e; 
		          ax:=sqrt(2.0);ay:=0.0;bx:=0.0;by:=0.0;
	         end if;
	         if ipt>=iest_plus+1 and ipt<=isud then -- *SUD-EST* 
		          tpp(npp).dir:=se; 
		          ax:=1.0;ay:=-1.0;bx:=0.0;by:=-1.0;
	         end if;
	         if ipt>=isud+1 and ipt<=isud_plus then -- *SUD* 
		          tpp(npp).dir:=s; 
		          ax:= 0.0;ay:=-(sqrt(2.0));bx:=0.0;by:=-1.0;
	         end if;
	         if ipt>=isud_plus+1 and ipt<=iouest then -- *SUD-OUEST* 
		          tpp(npp).dir:=so; 
		          ax:=-1.0;ay:=-1.0;bx:=-1.0;by:=-1.0;
	         end if;
	         if ipt>=iouest+1 and ipt<=iouest_plus then -- *OUEST* 
		          tpp(npp).dir:=o; 
		          ax:=-sqrt(2.0);ay:=0.0;bx:=-1.0;by:=0.0;
	         end if;
	         if ipt>=iouest_plus+1 and ipt<=inord then -- *NORD-OUEST* 
		           tpp(npp).dir:=no; 
		           ax:=-1.0;ay:=1.0;bx:=-1.0;by:=0.0;
	         end if;
	         if ipt>=inord+1 and ipt<=inord_plus then -- *NORD* 
		          tpp(npp).dir:=n; 
		          ax:=0.0;ay:=sqrt(2.0);bx:=0.0;by:=0.0;
	         end if;
	         tpp(npp).coin1(1).coor_x:=integer(float(listpts(ipt).coor_x)
			                             + ax*decalno*hno + bx*lno);
	         tpp(npp).coin1(1).coor_y:=integer(float(listpts(ipt).coor_y)
			                           + ay*decalno*hno + by*hno);
           if tbnomi(nobj).topo.mode_placement=mordant then
 		          npp:=npp+1;
        	    tpp(npp).nl:=1;
		          ax:=-ax; ay:=-ay;
	            tpp(npp).coin1(1).coor_x:=integer(float(listpts(ipt).coor_x)
			                               + ax*decalno*hno + bx*lno);
	            tpp(npp).coin1(1).coor_y:=integer(float(listpts(ipt).coor_y)
			                               + ay*decalno*hno + by*hno);
	         end if;
       end if;
    end if; -- mode placement   
	  if tbnomi(nobj).ln2=0 then 
      RETURN; 
    end if;
 end if; -- nl=1
        
            -- Positions a 2 lignes -------
 if tbnomi(nobj).topo.mode_placement=interieur then
	  npp:=npp+1;
	  tpp(npp).coin1(1).coor_x:=listpts(ipt).coor_x
				+(tbnomi(nobj).ln(1)-tbnomi(nobj).ln1)/2;
	  tpp(npp).coin1(1).coor_y:=listpts(ipt).coor_y+tbnomi(nobj).corps/2
			      +integer(espace*float(tbnomi(nobj).corps)/2.0);
	  Det_coin2(nobj,tbnomi,tpp(npp));
	  tpp(npp).nl:=2;
    if tbnomi(nobj).topo.objet_type=ponctuel then
        tpp(npp).dir:=int;
        -- Correction de Jenny faite en DTL introduite dans le code ADA 
        -- (nom sur 2 lignes)
    else
        -- Examen de la position des 8 coins de l'ecriture par rapport
        --  a la surface a nommer :
        -- zi : les 8 coins sont dans la surface
        -- zm : Entre 1 et 7 coins sont hors de la surface
        -- ze : les 8 coins sont hors de la surface
        Gr_contours_de(gros,tbnomi(nobj).topo.num_objet,contour);
        flag1:=Est_il_dans_surface(tpp(npp).coin1(1),contour);
        point_interm:=tpp(npp).coin1(1)+(0,tbnomi(nobj).corps);
        flag2:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln1,tbnomi(nobj).corps);
        flag3:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln1,0);
        flag4:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln1/2,tbnomi(nobj).corps/2);
        flag5:=Est_il_dans_surface(point_interm,contour);
        flag6:=Est_il_dans_surface(tpp(npp).coin2,contour);
        point_interm:=tpp(npp).coin2+(0,tbnomi(nobj).corps);
        flag7:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin2+(tbnomi(nobj).ln2,tbnomi(nobj).corps);
        flag8:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin2+(tbnomi(nobj).ln2,0);
        flag9:=Est_il_dans_surface(point_interm,contour);
        point_interm:=tpp(npp).coin1(1)+(tbnomi(nobj).ln2/2,tbnomi(nobj).corps/2);
        flag10:=Est_il_dans_surface(point_interm,contour);
        if flag1 and flag2 and flag3 and flag4 and flag5
          and flag6 and flag7 and flag8 and flag9 and flag10 then
          tpp(npp).dir:=zi;
        else
          if flag1=false and flag2=false and 
                  flag3=false and flag4=false and
                  flag5=false and flag6=false and 
                  flag7=false and flag8=false and
                  flag9=false and flag10=false then
            tpp(npp).dir:=ze;
          else
            tpp(npp).dir:=zm;   
          end if;
        end if;
    end if;
 else
    if tbnomi(nobj).topo.mode_placement=centre then
	      npp:= npp+1;
	      tpp(npp).coin1(1).coor_x:=listpts(ipt).coor_x-tbnomi(nobj).ln1/2;
	      tpp(npp).coin1(1).coor_y:=listpts(ipt).coor_y
			      +integer(espace*float(tbnomi(nobj).corps)/2.0);
	      Det_coin2(nobj,tbnomi,tpp(npp));
	      tpp(npp).nl:=2;
	      tpp(npp).dir:=int;
    else
        if ipt>=iest+1 and ipt<=iouest_plus then
	         if ipt>=iest+1 and ipt<=iest_plus then -- *EST* 
	            npp:=npp+1;
	            tpp(npp).dir:=e; 
	            ax:=sqrt(2.0);ay:=0.0;bx:=0.0;b1:=0.0;b2:=0.0;by:=0.0;
	         end if;
	         if ipt>=iest_plus+1 and ipt<=isud then -- *SUD-EST* 
	            npp:=npp+1;
	            tpp(npp).dir:=se; 
	            ax:=1.0;ay:=-1.0;bx:=0.0;b1:=0.0;b2:=0.0;by:=-1.0;
	         end if;
	         if ipt>=isud+1 and ipt<=isud_plus then -- *SUD* 
	            if listpts(isud).coor_x>=listpts(ipt).coor_x+tbnomi(nobj).ln1 then
		             return; 
    	        end if;
	            npp:=npp+1;
	            tpp(npp).dir:=s; 
	            ax:= 0.0;ay:=-(sqrt(2.0));bx:=0.0;b1:=0.0;b2:=0.0;by:=-1.0;
	         end if;
	         if ipt>=isud_plus+1 and ipt<=iouest then -- *SUD-OUEST* 
	            npp:=npp+1;
	            tpp(npp).dir:=so; 
	            ax:=-1.0;ay:=-1.0;bx:=0.0;b1:=-1.0;b2:=0.0;by:=-1.0;
	         end if;
	         if ipt>=iouest+1 and ipt<=iouest_plus then -- *OUEST*
	            npp:=npp+1;
	            tpp(npp).dir:=o; 
	            ax:=-sqrt(2.0);ay:=0.0;bx:=0.0;b1:=-1.0;b2:=0.0;by:=0.0;
	         end if;
	         tpp(npp).nl:=2;
           tpp(npp).coin1(1).coor_x:=integer(float(listpts(ipt).coor_x) 
			                            + ax*decalno*hno + bx*lno + b1*lno1 + b2*lno2);
	         tpp(npp).coin1(1).coor_y:=integer(float(listpts(ipt).coor_y) 
			                            + ay*decalno*hno + by*hno);
	         Det_coin2(nobj,tbnomi,tpp(npp));
	         if tbnomi(nobj).topo.mode_placement=mordant then
	             npp:=npp+1;
	             ax:=-ax; ay:=-ay;
	             tpp(npp).nl:=2;
               tpp(npp).coin1(1).coor_x:=integer(float(listpts(ipt).coor_x) 
			                            + ax*decalno*hno + bx*lno + b1*lno1 + b2*lno2);
	             tpp(npp).coin1(1).coor_y:=integer(float(listpts(ipt).coor_y) 
			                            + ay*decalno*hno + by*hno);
	             Det_coin2(nobj,tbnomi,tpp(npp));
	         end if;                                                
        else
           if (ipt>=iouest+1 and ipt<=inord_plus) or (ipt<=iest_plus) then
	             if ipt<=iest then  -- *NORD-EST*
	                npp:=npp+1;
	                tpp(npp).dir:=ne; 
	                ax:=1.0;ay:=1.0;bx:=0.0;b1:=0.0;b2:=0.0;by:=0.0;
	             end if;
	             if ipt>=iest+1 and ipt<=iest_plus then -- *EST*
	                npp:=npp+1;
	                tpp(npp).dir:=e; 
	                ax:=sqrt(2.0);ay:=0.0;bx:=0.0;b1:=0.0;b2:=0.0;by:=0.0;
	             end if;
	             if ipt>=iouest+1 and ipt<=iouest_plus then  -- *OUEST*
	                npp:=npp+1;
	                tpp(npp).dir:=o; 
	                ax:=-sqrt(2.0);ay:=0.0;bx:=0.0;b1:=0.0;b2:=-1.0;by:=0.0;
	             end if;
	             if ipt>=iouest_plus+1 and ipt<=inord then  -- *NORD-OUEST*
	                npp:=npp+1;
	                tpp(npp).dir:=no; 
	                ax:=-1.0;ay:=1.0;bx:=0.0;b1:=0.0;b2:=-1.0;by:=0.0;
	             end if;
	             if ipt>=inord+1 and ipt<=inord_plus then  -- *NORD*
	                if listpts(inord).coor_x>=listpts(ipt).coor_x+tbnomi(nobj).ln2 then
	                   return; 
    	            end if;
	                npp:=npp+1;
	                tpp(npp).dir:=n; 
	                ax:=0.0;ay:=sqrt(2.0);bx:=0.0;b1:=0.0;b2:=0.0;by:=0.0;
	             end if;
	             tpp(npp).nl:=2;
               tpp(npp).coin2.coor_x:=integer(float(listpts(ipt).coor_x)
			                             + ax*decalno*hno + bx*lno + b1*lno1 + b2*lno2);
	             tpp(npp).coin2.coor_y:=integer(float(listpts(ipt).coor_y)        
			                             + ay*decalno*hno + by*hno);
	             Det_coin1(nobj,tbnomi,tpp(npp));
	             if tbnomi(nobj).topo.mode_placement=mordant then
	                 npp:=npp+1;
	                 ax:=-ax; ay:=-ay;
	                 tpp(npp).nl:=2;
                   tpp(npp).coin2.coor_x:=integer(float(listpts(ipt).coor_x)
			                              + ax*decalno*hno + bx*lno + b1*lno1 + b2*lno2);
	                 tpp(npp).coin2.coor_y:=integer(float(listpts(ipt).coor_y)        
			                              + ay*decalno*hno + by*hno);
	                 Det_coin1(nobj,tbnomi,tpp(npp));
	             end if;
           end if;
        end if;         
    end if;  
 end if;
end det_p;

--======================================================================
-- Renvoie les contours des objets en interaction avec un objet no: 
-------------------------------------------------------------------
procedure DET_CONTOURS (gros    :in graphe_type;
                        no      :in integer;
                        tbnomi  :in typtbnomi;
                        ncontour:out integer;
                        contour :out point_liste_type) is
                          
  TARCS: liens_array_type(1..contour'last); 
  iest,inord,iouest,isud: natural;
  contptp: point_liste_type(1..contour'last);
  n,nba,ncontemp: integer:=0;
  
begin
  -- Determinons le contour de l'objet no:
  if tbnomi(no).topo.objet_type=zonal then
    nba:=Gr_face(gros,tbnomi(no).topo.num_objet).Nombre_arc;
    tarcs(1..nba):=Gr_arcs_de_face(gros,tbnomi(no).topo.num_objet);
    for i in 1..nba loop
      if tarcs(i)<0 then
        Gr_Points_d_Arc(gros,-tarcs(i),contptp,n);
      else
        Gr_Points_d_Arc(gros,tarcs(i),contptp,n);
      end if;
      for j in 1..n loop
        contour(ncontemp+j):=contptp(j);
      end loop;
      ncontemp:= ncontemp+n;
    end loop;
    ncontour:=ncontemp;
  end if;
  if tbnomi(no).topo.objet_type=ponctuel then
    Points_De_Cercle(no,tbnomi,ncontour,contour,iest,inord,iouest,isud,false);
  end if;
end DET_CONTOURS;

--=========================================================================
-- Cette procedure rejette la position si :
-- 	* elle est trop proche ou recouvre le pt centre de l'objet
-- 	* elle est plus proche d'un autre OBJET
--	* elle est a cheval sur le contour de l'image
--================================================================
procedure FILTRE (tpp : in out typtpp;
		          npp : in typnpp;
		          no : in integer;
		          tbnomi : in typtbnomi;
		          tnci : in typtnci;	
		          tici : in typtici;
		          tambi : in typtambi;
                  nb_ambi : in natural;
 		          flag : out boolean) is
                     
b : constant boite_type:= ( tpp(npp).coin1(1),
  (tpp(npp).coin1(1).coor_x+tbnomi(no).ln(1),tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
b1l : constant boite_type:=( tpp(npp).coin1(1),
  (tpp(npp).coin1(1).coor_x+tbnomi(no).ln1,tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
b2l : constant boite_type:=( tpp(npp).coin2,
  (tpp(npp).coin2.coor_x+tbnomi(no).ln2,tpp(npp).coin2.coor_y+tbnomi(no).corps));
dncont : natural; -- Dist du nom au contour de l'objet
dncontoi : natural; -- Dist du nom au contour d'un objet en interaction
dncontoi_min : natural:=tbnomi(no).corps; -- Dist min du nom au contour d'un objet en interaction
--*--
dncontoiint,dncontoiint2,dncontoibb,dncontoibh : integer:=0;
--*--
be : boite_type; 
affich : affichage_type;
noeud : noeud_type;
oi : integer;
nb_pts_max : integer:=pas_maille*(infoos.max_arc_fac+2)*(infoos.max_pts_arc+2);
ambi : boolean;
--*--
bblastopd:boite_type :=tbnomi(no).empblastop(tpp(npp).nl);
bblasdownd:boite_type :=tbnomi(no).empblasdown(tpp(npp).nl);
bbtopxmin,bbtopxmax,bbdownxmin,bbdownxmax,bbtopymin,bbtopymax,bbdownymin,bbdownymax:integer:=0;
absmin,absmax,ordmin,ordmax:integer:=0;
absmintemp,absmaxtemp,ordmintemp,ordmaxtemp:integer:=0;
absmintemp2,absmaxtemp2,ordmintemp2,ordmaxtemp2:integer:=0;
b1xmin,b1xmax,b1ymin,b1ymax:integer:=0;
b2xmin,b2xmax,b2ymin,b2ymax:integer:=0;
bxmin,bxmax,bymin,bymax:integer:=0; 
btop:boite_type:=((0,0),(0,0));
bdown:boite_type:=((0,0),(0,0));

nt : nom_type;


begin  

  if tbnomi(no).npic /=0 then   
    if tpp(npp).nl=1 then
	    if bblastopd /= ((0,0),(0,0)) then
        btop.minimum.coor_x:=bblastopd.minimum.coor_x + tpp(npp).coin1(1).coor_x;
        btop.maximum.coor_x:=bblastopd.maximum.coor_x + tpp(npp).coin1(1).coor_x;	
        btop.minimum.coor_y:=bblastopd.minimum.coor_y + tpp(npp).coin1(1).coor_y;
        btop.maximum.coor_y:=bblastopd.maximum.coor_y + tpp(npp).coin1(1).coor_y;	
      end if;
  	  if bblasdownd /= ((0,0),(0,0)) then
        bdown.minimum.coor_x:=bblasdownd.minimum.coor_x + tpp(npp).coin1(1).coor_x;
        bdown.maximum.coor_x:=bblasdownd.maximum.coor_x + tpp(npp).coin1(1).coor_x; 
        bdown.minimum.coor_y:=bblasdownd.minimum.coor_y + tpp(npp).coin1(1).coor_y;
        bdown.maximum.coor_y:=bblasdownd.maximum.coor_y + tpp(npp).coin1(1).coor_y;
 	  end if;
    else
  	  if bblastopd /= ((0,0),(0,0)) then
        btop.minimum.coor_x:=bblastopd.minimum.coor_x + tpp(npp).coin1(1).coor_x;
        btop.maximum.coor_x:=bblastopd.maximum.coor_x +tpp(npp).coin1(1).coor_x;	
        btop.minimum.coor_y:=bblastopd.minimum.coor_y + tpp(npp).coin1(1).coor_y;
        btop.maximum.coor_y:=bblastopd.maximum.coor_y +tpp(npp).coin1(1).coor_y;	
	  end if;
  	  if bblasdownd /= ((0,0),(0,0)) then
        bdown.minimum.coor_x:=bblasdownd.minimum.coor_x + tpp(npp).coin2.coor_x;
        bdown.maximum.coor_x:=bblasdownd.maximum.coor_x +tpp(npp).coin2.coor_x;
        bdown.minimum.coor_y:=bblasdownd.minimum.coor_y +tpp(npp).coin2.coor_y;
        bdown.maximum.coor_y:=bblasdownd.maximum.coor_y +tpp(npp).coin2.coor_y;
	    end if;
    end if;
  end if;

  --- La position est rejetee si elle deborde de l'image de mutilation
  if mutil then
    if tpp(npp).nl=1 then
      if b.minimum.coor_x<=pso.coor_x or b.maximum.coor_x>=pse.coor_x
      or b.minimum.coor_y<=pso.coor_y or b.maximum.coor_y>=pne.coor_y then
        flag:=false;
        return;
      end if;
    else
      if b1l.minimum.coor_x<=pso.coor_x or b1l.maximum.coor_x>=pse.coor_x 
      or b1l.minimum.coor_y<=pso.coor_y or b1l.maximum.coor_y>=pne.coor_y
      or b2l.minimum.coor_x<=pso.coor_x or b2l.maximum.coor_x>=pse.coor_x
      or b2l.minimum.coor_y<=pso.coor_y or b2l.maximum.coor_y>=pne.coor_y then
        flag:=false;
        return;
      end if;
    end if;
  end if;
        
  flag:=true;
  declare
    ncontour : integer;
    contour : point_liste_type(1..nb_pts_max);
  begin
    DET_CONTOURS(gros,no,tbnomi,ncontour,contour);
    if tpp(npp).nl=1 then
      --*--
      bxmin:=b.minimum.coor_x;
      bxmax:=b.maximum.coor_x;
      bymin:=b.minimum.coor_y;
      bymax:=b.maximum.coor_y;	

      dncont:=Manipboite.DBCONT(b,ncontour,contour);

      if btop.minimum.coor_x/=0 then
        absmintemp:=min(bxmin,btop.minimum.coor_x);
      else 
        absmintemp:=bxmin;
      end if;
	    if bdown.minimum.coor_x/=0 then 
        absmin:=min(absmintemp,bdown.minimum.coor_x);
      else
        absmin:=absmintemp;
      end if;
	    if btop.maximum.coor_x/=0 then
        absmaxtemp:=max(bxmax,btop.maximum.coor_x);
      else 
        absmaxtemp:=bxmax;
      end if; 
    	if bdown.maximum.coor_x/=0 then 
        absmax:=max(absmaxtemp,bdown.maximum.coor_x);
      else 
        absmax:=absmaxtemp;
      end if;
	    if btop.minimum.coor_y/=0 then 
        ordmintemp:=min(bymin,btop.minimum.coor_y);
      else
        ordmintemp:=bymin;
      end if;
	    if bdown.minimum.coor_y/=0 then 
        ordmin:=min(ordmintemp,bdown.minimum.coor_y);
      else
        ordmin:=ordmintemp;
      end if;
	    if btop.maximum.coor_y/=0 then
        ordmaxtemp:=max(bymax,btop.maximum.coor_y);
      else
        ordmaxtemp:=bymax;
      end if;
	    if bdown.maximum.coor_y/=0 then 
        ordmax:=max(ordmaxtemp,bdown.maximum.coor_y);
      else 
        ordmax:=ordmaxtemp;
      end if;

 	else	
 
      b1xmin:=b1l.minimum.coor_x;
      b1xmax:=b1l.maximum.coor_x;
      b1ymin:=b1l.minimum.coor_y;
      b1ymax:=b1l.maximum.coor_y;
	  b2xmin:=b2l.minimum.coor_x;
      b2xmax:=b2l.maximum.coor_x;
      b2ymin:=b2l.minimum.coor_y;
      b2ymax:=b2l.maximum.coor_y;					

      dncont:=MIN(Manipboite.DBCONT(b1l,ncontour,contour),Manipboite.DBCONT(b2l,ncontour,contour));

 	  absmintemp:=min(b1xmin,b2xmin);
	  if (btop.minimum.coor_x /=0) and (bdown.minimum.coor_x/=0) then
        absmintemp2:=min(btop.minimum.coor_x,bdown.minimum.coor_x);
      end if;
	    if ( btop.minimum.coor_x/=0) and (bdown.minimum.coor_x=0) then
        absmintemp2:=btop.minimum.coor_x;
      end if;
	    if (btop.minimum.coor_x =0) and (bdown.minimum.coor_x/=0) then
        absmintemp2:=bdown.minimum.coor_x;
      end if;
	    if absmintemp2/=0 then
        absmin:=min(absmintemp,absmintemp2);
      else
        absmin:=absmintemp;
      end if;
      absmaxtemp:=max(b1xmax,b2xmax);
     	if (btop.maximum.coor_x /=0) and (bdown.maximum.coor_x /=0) then
        absmaxtemp2:=max(btop.maximum.coor_x,bdown.maximum.coor_x);
      end if;
	    if (btop.maximum.coor_x /=0) and (bdown.maximum.coor_x=0) then
        absmaxtemp2:=btop.maximum.coor_x;
      end if;
	    if (btop.maximum.coor_x =0) and (bdown.maximum.coor_x/=0) then
        absmaxtemp2:=bdown.maximum.coor_x;
      end if;
	    if absmaxtemp2/=0 then
        absmax:=max(absmaxtemp,absmaxtemp2);
      else
        absmax:=absmaxtemp;
      end if;
	    ordmintemp:=min(b1ymin,b2ymin);
	    if (btop.minimum.coor_y /=0) and (bdown.minimum.coor_y /=0) then
        ordmintemp2:=min(btop.minimum.coor_y,bdown.minimum.coor_y);
      end if;
	    if (btop.minimum.coor_y /=0) and (bdown.minimum.coor_y=0) then
        ordmintemp2:=btop.minimum.coor_y;
      end if;
	    if (btop.minimum.coor_y =0) and (bdown.minimum.coor_y/=0) then
        ordmintemp2:=bdown.minimum.coor_y;
      end if;
	    if ordmintemp2/=0 then ordmin:=min(ordmintemp,ordmintemp2);else ordmin:=ordmintemp;end if;
	    ordmaxtemp:=max(b1ymax,b2ymax);
	    if (btop.maximum.coor_y /=0) and (bdown.maximum.coor_y /=0) then
        ordmaxtemp2:=max(btop.maximum.coor_y,bdown.maximum.coor_y);
      end if;
	    if (btop.maximum.coor_y /=0) and (bdown.maximum.coor_y=0) then
        ordmaxtemp2:=btop.maximum.coor_y;
      end if;
	    if (btop.maximum.coor_y =0) and (bdown.maximum.coor_y/=0) then
        ordmaxtemp2:=bdown.maximum.coor_y;
      end if;
	    if ordmaxtemp2/=0 then
        ordmax:=max(ordmaxtemp,ordmaxtemp2);
      else
        ordmax:=ordmaxtemp;
      end if;

    end if;
  end;--declare;

  -- La position est rejetee si elle est ambigue avec un autre objet,
  -- seulement dans le cas ou les ambiguites entre obj. sont compatibles.
  for i in 1..tnci(no) loop
    oi:=tici(no,i);

    nt:=tbnomi(oi);

    ambi:=false;
    for k in 1..nb_ambi loop
      if ((tambi(k).code1=tbnomi(no).topo.code) and (tambi(k).code2=tbnomi(oi).topo.code))
      or ((tambi(k).code2=tbnomi(no).topo.code) and (tambi(k).code1=tbnomi(oi).topo.code))
      or (tbnomi(no).topo.code=tbnomi(oi).topo.code) then
	    -- les Ponctuels Imprecis ne sont jamais ambigus
        if tbnomi(no).topo.objet_type/=ponctuel or tbnomi(no).topo.mode_placement/=interieur then
		  -- Jamais d'ambiguites avec des �critures a disposition	
          if tbnomi(oi).topo.objet_type/=Lineaire then
		    ambi:=true;
            exit;
          end if;
        end if;
      end if;
    end loop;
    if ambi then
      declare
        ncontour: integer;
        contour : point_liste_type(1..nb_pts_max);
      begin
        DET_CONTOURS(gros,tici(no,i),tbnomi,ncontour,contour);
        if tbnomi(oi).topo.objet_type=zonal then
    	    be:=GR_ENCOMBREMENT_FACE(gros,tbnomi(oi).topo.num_objet);
	    end if;
        if tbnomi(oi).topo.objet_type=ponctuel then
          noeud:=GR_NOEUD(gros,tbnomi(oi).topo.num_objet);
          affich:=GR_LEGENDE_P(legos,noeud.att_graph);
          be:=((tbnomi(oi).coord_objet.coor_x-integer(affich.largeur/2.0), 
	            tbnomi(oi).coord_objet.coor_y-integer(affich.largeur/2.0)), 
	           (tbnomi(oi).coord_objet.coor_x+integer(affich.largeur/2.0), 
	            tbnomi(oi).coord_objet.coor_y+integer(affich.largeur/2.0)));
        end if;          
        if tpp(npp).nl=1 then
          dncontoiint:= Manipboite.DBB(b,be);
        else
          dncontoiint:= min(Manipboite.DBB(b1l,be),Manipboite.DBB(b2l,be));
        end if;

        if btop/=((0,0),(0,0)) then
          dncontoibh:=Manipboite.DBB(btop,be);
          dncontoiint2:=min(dncontoiint,dncontoibh);
        end if;
      	if bdown/=((0,0),(0,0)) then
          dncontoibb:=Manipboite.DBB(bdown,be);
          if dncontoiint2/=0 then
            dncontoi:=min(dncontoiint2,dncontoibb);
          else
            dncontoi:=min(dncontoiint,dncontoibb);
          end if;
        end if;
	      if (btop/=((0,0),(0,0))) and (bdown=((0,0),(0,0))) then
          dncontoi:=dncontoiint2;
        end if;
	      if (btop=((0,0),(0,0))) and (bdown=((0,0),(0,0))) then
          dncontoi:=dncontoiint;
        end if;

	      -- Si la distance a la boite de l'objet en interaction est inferieure,
	      -- il faut calculer la distance au contour:
        if (dncontoi<dncont or dncontoi<dncontoi_min) then
          DET_CONTOURS(gros,tici(no,i),tbnomi,ncontour,contour);
          if tpp(npp).nl=1 then
            dncontoiint:=Manipboite.DBCONT(b,ncontour,contour);
          else
            dncontoiint:=min(Manipboite.DBCONT(b1l,ncontour,contour),Manipboite.DBCONT(b2l,ncontour,contour));
          end if;

          if btop /= ((0,0),(0,0)) then
            dncontoibh:=Manipboite.DBCONT(btop,ncontour,contour);
            dncontoiint2:=min(dncontoiint,dncontoibh);
          end if;
	        if bdown /= ((0,0),(0,0)) then
            dncontoibb:=Manipboite.DBCONT(bdown,ncontour,contour);
            if dncontoiint2 /=0 then
              dncontoi:=min(dncontoiint2,dncontoibb);
            else
              dncontoi:=min(dncontoiint,dncontoibb);
            end if;
          end if;
	        if (btop/=((0,0),(0,0))) and (bdown=((0,0),(0,0))) then
            dncontoi:=dncontoiint2;
          end if;
	        if (btop=((0,0),(0,0))) and (bdown=((0,0),(0,0))) then
            dncontoi:=dncontoiint;
          end if;

          if (dncontoi<dncont or dncontoi<dncontoi_min) then
            flag:=false;
            return; -- trop pres d'un contour d'un autre objet !
          end if;
        end if;
      end;
    end if; --ambi
  end loop;
  
-- La position est rejetee si elle est a cheval sur le cadre ------

  if mutil then
  	if tpp(npp).nl=2 then
	  if Manipboite.DEBORDE(b1l,pne,pno,pso,pse) 
	     or Manipboite.DEBORDE(b2l,pne,pno,pso,pse)
	     or ((btop/=((0,0),(0,0))) and (Manipboite.DEBORDE(btop,pne,pno,pso,pse)))
	     or ((bdown/=((0,0),(0,0))) and (Manipboite.DEBORDE(bdown,pne,pno,pso,pse))) then 
  	     flag:=false;
	     return; -- la pos est rejettee !!! -- A cheval sur le contour de l'image -- 
	  end if;
    else
	  if Manipboite.DEBORDE(b,pne,pno,pso,pse) 
         or ((btop/=((0,0),(0,0))) and (Manipboite.DEBORDE(btop,pne,pno,pso,pse)))
	     or ((bdown/=((0,0),(0,0))) and (Manipboite.DEBORDE(bdown,pne,pno,pso,pse))) then 
  	     flag:=false;
	     return; -- la pos est rejettee!!! A cheval sur le contour de l'image -- 
	  end if;
    end if;
  end if;
  
end FILTRE;


--================================================================================
-- Procedure qui determine le poids propre d'une position sans image de mutilation
----------------------------------------------------------------------------------
procedure DET_POIDS_PROPRE(tpp: in out typtpp;
                           npp: in typnpp;
                           no: in integer;
                           tbnomi: in typtbnomi;
                           tzi: in typtbe;
                           dncmax: in natural;
                           interdit: out boolean) is
                    
c,cen_p : point_type:= tbnomi(no).coord_objet;
p : position_type:= tpp(npp);
br : boite_type;
b : constant boite_type:=(tpp(npp).coin1(1),(tpp(npp).coin1(1).coor_x+tbnomi(no).ln(1),tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
hd : integer:=integer(dilat*float(tbnomi(no).corps));
na,n : integer;
arc : arc_type;
poids : float:=0.0;
ch : string(1..20);
nc : integer;
pos_centre,pos_centre1,pos_centre2 : point_type;
boiteng : boite_type;
demiboite,demiboite1,demiboite2 : point_type;
d,d1,d2 : array(0..3) of float;
dist,dist1,dist2,dmax,dmax1,dmax2 : float;
bblastopd : boite_type:=Manipboite.dilatee(tbnomi(no).empblastop(p.nl),hd,hd);
bblasdownd : boite_type:=Manipboite.dilatee(tbnomi(no).empblasdown(p.nl),hd,hd);
  
begin
  Interdit:=false;
  if p.nl=1 then
    declare          
      bd : boite_type :=Manipboite.dilatee(b,hd,hd);
      bxmin : positive:=bd.minimum.coor_x;
      bxmax : positive:=bd.maximum.coor_x;
      bymin : positive:=bd.minimum.coor_y;
      bymax : positive:=bd.maximum.coor_y;
      ecrase : boolean;
      bbtopxmin : integer:=bblastopd.minimum.coor_x+p.coin1(1).coor_x;
      bbtopxmax : integer:=bblastopd.maximum.coor_x+p.coin1(1).coor_x;	
      bbdownxmin : integer:=bblasdownd.minimum.coor_x+p.coin1(1).coor_x;
      bbdownxmax : integer:=bblasdownd.maximum.coor_x+p.coin1(1).coor_x;
      bbtopymin : integer:=bblastopd.minimum.coor_y+p.coin1(1).coor_y;
      bbtopymax : integer:=bblastopd.maximum.coor_y+p.coin1(1).coor_y;	
      bbdownymin : integer:=bblasdownd.minimum.coor_y+p.coin1(1).coor_y;
      bbdownymax : integer:=bblasdownd.maximum.coor_y+p.coin1(1).coor_y; 
      tempdnc,tempdnc2 : natural:=0;
    
    begin
      if tbnomi(no).topo.mode_placement=interieur then
         demiboite:=(tbnomi(no).ln(1)/2,tbnomi(no).corps/2);
         pos_centre:= b.minimum + demiboite;
         dist:=norme_v2(pos_centre,cen_p);
         if tbnomi(no).topo.objet_type=ponctuel then
            dmax:=sqrt(float((tbnomi(no).ln(1)/2))**2+(float(tbnomi(no).corps/2))**2);
         else
            boiteng:=Gr_encombrement_face(gros,tbnomi(no).topo.num_objet);
            if tbnomi(no).ln(1)<boiteng.maximum.coor_x-boiteng.minimum.coor_x then
               boiteng.maximum:=boiteng.maximum - 2*demiboite; 
            else
               boiteng.minimum.coor_x:=cen_p.coor_x - tbnomi(no).ln(1)/2
                                                     - tbnomi(no).corps;
               boiteng.minimum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     - tbnomi(no).corps;
               boiteng.maximum.coor_x:=cen_p.coor_x - tbnomi(no).ln(1)/2
                                                     + tbnomi(no).corps;
               boiteng.maximum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     +tbnomi(no).corps;
            end if;
            d(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 + (float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            dmax:=d(0);
            for k in 1..3 loop
                dmax:=max(dmax,d(k));
            end loop;
         end if;
         -- p.dnc:=integer(dist)*dncmax/integer(dmax)/4;
         p.dnc:=natural(dist*float(dncmax)/dmax/4.0);
      else
         p.dnc :=Manipboite.Dbpt(b,tbnomi(no).coord_objet);
      end if;

	  -- debut  modif 26.06.2006
      p.poid(1):=Poidnc(dncmax,p.dnc); 

      -- p.poid(1):=0.0;
	  -- -- le poids de distance ne joue que pour les "ponctuel imprecis" et les "zonal interieur"
      -- if (tbnomi(no).topo.objet_type=ponctuel and tbnomi(no).topo.mode_placement=interieur)
	  -- or (tbnomi(no).topo.objet_type=Zonal and tbnomi(no).topo.mode_placement=Interieur) then
      --   p.poid(1):=poidnc(dncmax,p.dnc);
      -- end if;
	  -- fin modif 26.06.2006

      p.poip:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
    end;
  else
    declare
      b1l : constant boite_type:=(tpp(npp).coin1(1),(tpp(npp).coin1(1).coor_x+tbnomi(no).ln1,tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
      b2l : constant boite_type:=(tpp(npp).coin2,(tpp(npp).coin2.coor_x+tbnomi(no).ln2,tpp(npp).coin2.coor_y+tbnomi(no).corps));
      b1ld : boite_type:=Manipboite.dilatee(b1l,hd,hd);
      b2ld : boite_type:=Manipboite.dilatee(b2l,hd,hd);
      bxmin1 : positive:=b1ld.minimum.coor_x;bxmax1:positive:=b1ld.maximum.coor_x;
      bymin1 : positive:=b1ld.minimum.coor_y;bymax1:positive:=b1ld.maximum.coor_y;
      bxmin2 : positive:=b2ld.minimum.coor_x;bxmax2:positive:=b2ld.maximum.coor_x;
      bymin2 : positive:=b2ld.minimum.coor_y;bymax2:positive:=b2ld.maximum.coor_y;
      bbtopxmin:integer:=bblastopd.minimum.coor_x + p.coin1(1).coor_x;
      bbtopxmax:integer:=bblastopd.maximum.coor_x + p.coin1(1).coor_x;	
      bbdownxmin:integer:=bblasdownd.minimum.coor_x + p.coin2.coor_x;
      bbdownxmax:integer:=bblasdownd.maximum.coor_x + p.coin2.coor_x;
      bbtopymin:integer:=bblastopd.minimum.coor_y + p.coin1(1).coor_y;
      bbtopymax:integer:=bblastopd.maximum.coor_y + p.coin1(1).coor_y;	
      bbdownymin:integer:=bblasdownd.minimum.coor_y + p.coin2.coor_y;
      bbdownymax:integer:=bblasdownd.maximum.coor_y + p.coin2.coor_y; 
      ecrasebtop,ecrasebdown:boolean:=false;
      tempdnc,tempdnc2:natural:=0;

    begin 
      if tbnomi(no).topo.mode_placement=interieur then
         demiboite1:=(tbnomi(no).ln1/2,tbnomi(no).corps/2);
         demiboite2:=(tbnomi(no).ln2/2,tbnomi(no).corps/2);
         pos_centre1:= b1l.minimum + demiboite1;
         pos_centre2:= b2l.minimum + demiboite2;
         dist1:=norme_v2(pos_centre1,cen_p);
         dist2:=norme_v2(pos_centre2,cen_p);

         if tbnomi(no).topo.objet_type=ponctuel then
            dmax1:=sqrt(float(tbnomi(no).ln1/2)**2+float(tbnomi(no).corps/2)**2);
            dmax2:=sqrt(float(tbnomi(no).ln2/2)**2+float(tbnomi(no).corps/2)**2);
         else
            boiteng:=Gr_encombrement_face(gros,tbnomi(no).topo.num_objet);
            if tbnomi(no).ln(1)<boiteng.maximum.coor_x-boiteng.minimum.coor_x then
               boiteng.maximum:=boiteng.maximum - 2*demiboite1; 
            else
               boiteng.minimum.coor_x:=cen_p.coor_x-tbnomi(no).ln1/2-tbnomi(no).corps;
               boiteng.minimum.coor_y:=cen_p.coor_y-tbnomi(no).corps/2-tbnomi(no).corps;
               boiteng.maximum.coor_x:=cen_p.coor_x-tbnomi(no).ln1/2+tbnomi(no).corps;
               boiteng.maximum.coor_y:=cen_p.coor_y-tbnomi(no).corps/2+tbnomi(no).corps;
            end if;
            d1(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);

            d2(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);

            dmax1:=d1(0);
            dmax2:=d2(0);
            for k in 1..3 loop
                dmax1:=max(dmax1,d1(k));
                dmax2:=max(dmax2,d2(k));
            end loop;
         end if;
        -- p.dnc:=min(integer(dist1)*dncmax/integer(dmax1)/4,integer(dist2)*dncmax/integer(dmax2)/4);
        p.dnc:=min(natural(dist1*float(dncmax)/dmax1/4.0),natural(dist2*float(dncmax)/dmax2/4.0));

	  else  	
         p.dnc:=min(Manipboite.dbpt(b1l,tbnomi(no).coord_objet),
                    Manipboite.dbpt(b2l,tbnomi(no).coord_objet));
      end if;

	  -- debut modif 26.06.2006
      p.poid(1):=poidnc(dncmax,p.dnc);

--      p.poid(1):=0.0;
--	  -- le poids de distance ne joue que pour les "ponctuel imprecis" et les "zonal interieur"
--      if (tbnomi(no).topo.objet_type=ponctuel and tbnomi(no).topo.mode_placement=interieur)
--	  or (tbnomi(no).topo.objet_type=Zonal and tbnomi(no).topo.mode_placement=Interieur) then
--        p.poid(1):=poidnc(dncmax,p.dnc);
--      end if;
	  -- fin modif 26.06.2006

      p.poip:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
    end;
  end if;
  
  if route then
  declare
    tarcs:Liens_array_type(1..gr_infos(grr).nb_arcs);
    lista:Point_liste_type(1..gr_infos(grr).max_pts_arc);
  begin
    -------------------------------------------------------------------
    -- le poids de dist de p est augment� si un arc � �viter de franchir
    -- s�pare p de l'objet d�sign�
    -------------------------------------------------------------------
  if p.nl=1 then
    cen_p.coor_x:=p.coin1(1).coor_x+tbnomi(no).ln(1)/2;
    cen_p.coor_y:=p.coin1(1).coor_y+tbnomi(no).corps/2;
  else
    cen_p.coor_x:=p.coin1(1).coor_x+tbnomi(no).ln1/2;
    cen_p.coor_y:=p.coin1(1).coor_y-integer(float(tbnomi(no).corps)*espace/2.0);
  end if;
  br.minimum.coor_x:=min(tbnomi(no).coord_objet.coor_x,cen_p.coor_x);
  br.maximum.coor_x:=max(tbnomi(no).coord_objet.coor_x,cen_p.coor_x);
  br.minimum.coor_y:=min(tbnomi(no).coord_objet.coor_y,cen_p.coor_y);
  br.maximum.coor_y:=max(tbnomi(no).coord_objet.coor_y,cen_p.coor_y);
  Gr_arcs_in_rectangle(grr,br.minimum,br.maximum,tarcs,na);
  if na>0 then
    for ia in 1..na loop
      arc:=Gr_arc(grr,tarcs(ia));
      for l in 1..nb_codes_route loop
        if gr_legende_l(legr, arc.att_graph).theme=tcodes_route(l) then
          Gr_points_d_arc(grr,tarcs(ia),lista,n);
          for ip in 1..n-1 loop
            if Manipboite.intersec(tbnomi(no).coord_objet,cen_p,lista(ip),lista(ip+1)) then
              poids:=p.poid(1)+cpoi_intersec_plus;
              if poids>10.0 then
                p.poid(1):=10.0;
              else
                p.poid(1):=poids;
              end if;
              poids:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
                if poids>10.0 then
                  p.poip:=10.0;
                else
                  p.poip:=poids;
                end if;
              goto label1;
            end if;
          end loop;--ip
        end if;
      end loop;--l 
    end loop;--ia
  end if;
  end;
  end if;
  
  <<label1>> if tbnomi(no).topo.mode_placement=interieur then
      if tbnomi(no).topo.objet_type=zonal then
        poids:=p.poip+poidir(p.dir,pardir);
        if poids>10.0 then
          p.poip:=10.0;
        else
          p.poip:=poids;
        end if;
      end if;
  end if;
  
  tpp(npp):=p;
end det_poids_propre;


          
--===========================================================================
-- Procedure qui determine le poids propre d'une position: avec image de mutilation
-- pamametre tpoi en plus
----------------------------------------------------------
procedure DET_POIDS_PROPRE(tpp: in out typtpp;
                           npp: in typnpp;
                           no: in integer;
                           tbnomi: in typtbnomi;
                           tpoi: in typtipoi;
                           tzi: in typtbe;
                           dncmax: in natural;
                           interdit: out boolean) is
                    
c, cen_p: point_type:= tbnomi(no).coord_objet;
p       : position_type:= tpp(npp);
br      : boite_type;
b:constant boite_type:=(tpp(npp).coin1(1),(tpp(npp).coin1(1).coor_x
	                    +tbnomi(no).ln(1),tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
hd:integer:=integer(dilat*float(tbnomi(no).corps));
na,n:integer;
arc:arc_type;
poids:float:=0.0;
ch : string(1..20);
nc : integer;
pos_centre,pos_centre1,pos_centre2 : point_type;
boiteng : boite_type;
demiboite,demiboite1,demiboite2 : point_type;
d,d1,d2 : array(0..3) of float;
dist,dist1,dist2,dmax,dmax1,dmax2 : float;

bblastopd : boite_type;
bblasdownd : boite_type;
  
begin
  Interdit:=false;
  if tbnomi(no).empblastop(p.nl)/=((0,0),(0,0)) then
    bblastopd:=Manipboite.dilatee(tbnomi(no).empblastop(p.nl),hd,hd);
  else
    bblastopd:=((0,0),(0,0));
  end if;
  if tbnomi(no).empblasdown(p.nl)/=((0,0),(0,0)) then
    bblasdownd:=Manipboite.dilatee(tbnomi(no).empblasdown(p.nl),hd,hd);
  else
    bblasdownd:=((0,0),(0,0));
  end if;
  if p.nl=1 then
    declare
      bd:boite_type :=Manipboite.dilatee(b,hd,hd);
      bxmin:positive:=bd.minimum.coor_x;
      bxmax:positive:=bd.maximum.coor_x;
      bymin:positive:=bd.minimum.coor_y;
      bymax:positive:=bd.maximum.coor_y;
      npx:positive:=(bxmax-bxmin)/pt_image;
      npy:positive:=(bymax-bymin)/pt_image;
      ecrase:boolean;
      origine:point_type:=((bxmin-Max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
	                       (bymin-Max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
    bbtopxmin:integer:=bblastopd.minimum.coor_x + p.coin1(1).coor_x;
    bbtopxmax:integer:=bblastopd.maximum.coor_x + p.coin1(1).coor_x;	
    bbdownxmin:integer:=bblasdownd.minimum.coor_x + p.coin1(1).coor_x;
    bbdownxmax:integer:=bblasdownd.maximum.coor_x + p.coin1(1).coor_x;
    bbtopymin:integer:=bblastopd.minimum.coor_y + p.coin1(1).coor_y;
    bbtopymax:integer:=bblastopd.maximum.coor_y + p.coin1(1).coor_y;	
    bbdownymin:integer:=bblasdownd.minimum.coor_y + p.coin1(1).coor_y;
    bbdownymax:integer:=bblasdownd.maximum.coor_y + p.coin1(1).coor_y; 
    nbpxtop:integer:=(bbtopxmax-bbtopxmin)/pt_image;
    nbpytop:integer:=(bbtopymax-bbtopymin)/pt_image;
    nbpxdown:integer:=(bbdownxmax-bbdownxmin)/pt_image;
    nbpydown:integer:=(bbdownymax-bbdownymin)/pt_image;
    ecrasebtop,ecrasebdown:boolean:=false;
    originebtop:point_type:=((bbtopxmin-max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			   (bbtopymin-max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
    originebdown:point_type:=((bbdownxmin-max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			   (bbdownymin-max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
    poimbtop,poimbdown:typpoi:=0.0;
    tempdnc,tempdnc2:natural:=0;

    begin
      if origine.coor_x<1 or origine.coor_y<1
      or origine.coor_x+npx>tpoi'last(1) or origine.coor_y+npy>tpoi'last(2) then
        interdit:=true;
        return;
      end if;
      
      if originebtop.coor_x<1 or originebtop.coor_y<1
      or originebtop.coor_x+nbpxtop>tpoi'last(1) or originebtop.coor_y+nbpytop>tpoi'last(2) then
        interdit:=true;
        return;
      end if;

      if originebdown.coor_x<1 or originebdown.coor_y<1
      or originebdown.coor_x+nbpxdown>tpoi'last(1) or originebdown.coor_y+nbpydown>tpoi'last(2) then
        interdit:=true;
        return;
      end if;

      poimub(tpoi,origine,npx,npy,ecrase,p.poim(1));
      if (nbpxtop*nbpytop)/=0 then
        poimub(tpoi,originebtop,nbpxtop,nbpytop,ecrasebtop,poimbtop);
      else
        poimbtop:=0.0;
      end if;
      if (nbpxdown*nbpydown)/=0 then
        poimub(tpoi,originebdown,nbpxdown,nbpydown,ecrasebdown,poimbdown);
      else
        poimbdown:=0.0;
      end if;
      
      if ecrase=true or ecrasebtop=true or ecrasebdown=true then -- la position ecrase un pixel interdit !
        interdit:=true;
        return;
      end if;

      -- p.poim:=(p.poim+poimbtop+poimbdown)/3.0;
      p.poim(1):=(p.poim(1)*float(npx*npy)+poimbtop*float(nbpxtop*nbpytop)+poimbdown*float(nbpxdown*nbpydown))/float((npx*npy)+(nbpxtop*nbpytop)+(nbpxdown*nbpydown));
      
      if p.poim(1)>10.0 then
        p.poim(1):=10.0;
      end if;
      
      -- (nom sur 1 ligne)
      if tbnomi(no).topo.mode_placement=interieur then
         demiboite:=(tbnomi(no).ln(1)/2,tbnomi(no).corps/2);
         pos_centre:= b.minimum + demiboite;
         dist:=norme_v2(pos_centre,cen_p);
         if tbnomi(no).topo.objet_type=ponctuel then
            dmax:=sqrt(float((tbnomi(no).ln(1)/2))**2+(float(tbnomi(no).corps/2))**2);
         else
            boiteng:=Gr_encombrement_face(gros,tbnomi(no).topo.num_objet);
            if tbnomi(no).ln(1)<boiteng.maximum.coor_x-boiteng.minimum.coor_x then
               boiteng.maximum:=boiteng.maximum - 2*demiboite; 
            else
               boiteng.minimum.coor_x:=cen_p.coor_x - tbnomi(no).ln(1)/2
                                                     - tbnomi(no).corps;
               boiteng.minimum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     - tbnomi(no).corps;
               boiteng.maximum.coor_x:=cen_p.coor_x - tbnomi(no).ln(1)/2
                                                     + tbnomi(no).corps;
               boiteng.maximum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     +tbnomi(no).corps;
            end if;
            d(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 + (float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln(1)/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            dmax:=d(0);
            for k in 1..3 loop
                dmax:=max(dmax,d(k));
            end loop;
         end if;
         -- p.dnc:=integer(dist)*dncmax/integer(dmax)/4;
         p.dnc:=natural(dist*float(dncmax)/dmax/4.0);
      else
         p.dnc :=Manipboite.Dbpt(b,tbnomi(no).coord_objet);
      end if;

   	  -- debut modif 26.06.2006
      p.poid(1):=Poidnc(dncmax,p.dnc);

--      p.poid(1):=0.0;
--	  -- le poids de distance ne joue que pour les "ponctuel imprecis" et les "zonal interieur"
--      if (tbnomi(no).topo.objet_type=ponctuel and tbnomi(no).topo.mode_placement=interieur)
--	  or (tbnomi(no).topo.objet_type=Zonal and tbnomi(no).topo.mode_placement=Interieur) then
--        p.poid(1):=poidnc(dncmax,p.dnc);
--      end if;
	  -- fin modif 26.06.2006

      p.poip:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
    end;
  else
    declare
      b1l:constant boite_type:=(tpp(npp).coin1(1),
	     (tpp(npp).coin1(1).coor_x+tbnomi(no).ln1,tpp(npp).coin1(1).coor_y+tbnomi(no).corps));
      b2l:constant boite_type:=(tpp(npp).coin2,
	     (tpp(npp).coin2.coor_x+tbnomi(no).ln2,tpp(npp).coin2.coor_y+tbnomi(no).corps));
      b1ld:boite_type:=Manipboite.dilatee(b1l,hd,hd);
      b2ld:boite_type:=Manipboite.dilatee(b2l,hd,hd);
      bxmin1:positive:=b1ld.minimum.coor_x;bxmax1:positive:=b1ld.maximum.coor_x;
      bymin1:positive:=b1ld.minimum.coor_y;bymax1:positive:=b1ld.maximum.coor_y;
      bxmin2:positive:=b2ld.minimum.coor_x;bxmax2:positive:=b2ld.maximum.coor_x;
      bymin2:positive:=b2ld.minimum.coor_y;bymax2:positive:=b2ld.maximum.coor_y;
      ecrase1,ecrase2:boolean;
      npx1:positive:=(bxmax1-bxmin1)/pt_image;
      npy1:positive:=(bymax1-bymin1)/pt_image;
      npx2:positive:=(bxmax2-bxmin2)/pt_image;
      npy2:positive:=(bymax2-bymin2)/pt_image;
      origine1:point_type:=((bxmin1-Max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			                      (bymin1-Max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
      origine2:point_type:=((bxmin2-Max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			                      (bymin2-Max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
      poim1,poim2:typpoi:=0.0;
      --*--
      bbtopxmin:integer:=bblastopd.minimum.coor_x + p.coin1(1).coor_x;
      bbtopxmax:integer:=bblastopd.maximum.coor_x + p.coin1(1).coor_x;	
      bbdownxmin:integer:=bblasdownd.minimum.coor_x + p.coin2.coor_x;
      bbdownxmax:integer:=bblasdownd.maximum.coor_x + p.coin2.coor_x;
      bbtopymin:integer:=bblastopd.minimum.coor_y + p.coin1(1).coor_y;
      bbtopymax:integer:=bblastopd.maximum.coor_y + p.coin1(1).coor_y;	
      bbdownymin:integer:=bblasdownd.minimum.coor_y + p.coin2.coor_y;
      bbdownymax:integer:=bblasdownd.maximum.coor_y + p.coin2.coor_y; 
      nbpxtop:integer:=(bbtopxmax-bbtopxmin)/pt_image;
      nbpytop:integer:=(bbtopymax-bbtopymin)/pt_image;
      nbpxdown:integer:=(bbdownxmax-bbdownxmin)/pt_image;
      nbpydown:integer:=(bbdownymax-bbdownymin)/pt_image;
      ecrasebtop,ecrasebdown:boolean:=false;
      originebtop:point_type:=((bbtopxmin-max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			   (bbtopymin-max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
      originebdown:point_type:=((bbdownxmin-max(tzi(no).minimum.coor_x,pso.coor_x))/pt_image,
			   (bbdownymin-max(tzi(no).minimum.coor_y,pso.coor_y))/pt_image);
      poimbtop,poimbdown:typpoi:=0.0;
      tempdnc,tempdnc2:natural:=0;

    begin 
      if origine1.coor_x<1 or origine1.coor_y<1
      or origine1.coor_x+npx1>tpoi'last(1) or origine1.coor_y+npy1>tpoi'last(2)
      or origine2.coor_x<1 or origine2.coor_y<1
      or origine2.coor_x+npx2>tpoi'last(1) or origine2.coor_y+npy2>tpoi'last(2) then
        interdit:=true;
        return;
      end if;
      poimub(tpoi,origine1,npx1,npy1,ecrase1,poim1);
      poimub(tpoi,origine2,npx2,npy2,ecrase2,poim2);
      
      if (nbpxtop*nbpytop)/=0 then
        if originebtop.coor_x<1 or originebtop.coor_y<1
        or originebtop.coor_x+nbpxtop>tpoi'last(1) or originebtop.coor_y+nbpytop>tpoi'last(2) then
          interdit:=true;
          return;
        end if;
        poimub(tpoi,originebtop,nbpxtop,nbpytop,ecrasebtop,poimbtop);
      else
        poimbtop:=0.0;
      end if;
      if (nbpxdown*nbpydown)/=0 then
        if originebdown.coor_x<1 or originebdown.coor_y<1
        or originebdown.coor_x+nbpxdown>tpoi'last(1) or originebdown.coor_y+nbpydown>tpoi'last(2) then
          interdit:=true;
          return;
        end if;
        poimub(tpoi,originebdown,nbpxdown,nbpydown,ecrasebdown,poimbdown);
      else
        poimbdown:=0.0;
      end if;

      if ecrase1=true or ecrase2=true or ecrasebtop=true or ecrasebdown=true then -- p ecrase un pixel interdit !
        interdit:=true;
        return; 
      end if;
      -- poids:=(poim1+poim2)/2.0;
      -- poids:=(poim1+poim2+poimbtop+poimbdown)/4.0;
      poids:=(poim1*float(npx1*npy1)+poim2*float(npx2*npy2)+poimbtop*float(nbpxtop*nbpytop)+poimbdown*float(nbpxdown*nbpydown))/float((npx1*npy1)+(npx2*npy2)+(nbpxtop*nbpytop)+(nbpxdown*nbpydown));

      if poids>10.0 then
        p.poim(1):=10.0;
      else
      	p.poim(1):=poids;
      end if;

      -- (nom sur 2 lignes)
      if tbnomi(no).topo.mode_placement=interieur then
         demiboite1:=(tbnomi(no).ln1/2,tbnomi(no).corps/2);
         demiboite2:=(tbnomi(no).ln2/2,tbnomi(no).corps/2);
         pos_centre1:= b1l.minimum + demiboite1;
         pos_centre2:= b2l.minimum + demiboite2;
         dist1:=norme_v2(pos_centre1,cen_p);
         dist2:=norme_v2(pos_centre2,cen_p);

         if tbnomi(no).topo.objet_type=ponctuel then
            dmax1:=sqrt(float(tbnomi(no).ln1/2)**2+float(tbnomi(no).corps/2)**2);
            dmax2:=sqrt(float(tbnomi(no).ln2/2)**2+float(tbnomi(no).corps/2)**2);
         else
            boiteng:=Gr_encombrement_face(gros,tbnomi(no).topo.num_objet);
            if tbnomi(no).ln(1)<boiteng.maximum.coor_x-boiteng.minimum.coor_x then
               boiteng.maximum:=boiteng.maximum - 2*demiboite1; 
            else
               boiteng.minimum.coor_x:=cen_p.coor_x - tbnomi(no).ln1/2
                                                     - tbnomi(no).corps;
               boiteng.minimum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     - tbnomi(no).corps;
               boiteng.maximum.coor_x:=cen_p.coor_x - tbnomi(no).ln1/2
                                                     + tbnomi(no).corps;
               boiteng.maximum.coor_y:=cen_p.coor_y - tbnomi(no).corps/2
                                                     +tbnomi(no).corps;
            end if;
            d1(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d1(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln1/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);

            d2(0):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(1):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.minimum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(2):=sqrt((float(boiteng.minimum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);
            d2(3):=sqrt((float(boiteng.maximum.coor_x+tbnomi(no).ln2/2-cen_p.coor_x))**2
                 +(float(boiteng.maximum.coor_y+tbnomi(no).corps/2-cen_p.coor_y))**2);

            dmax1:=d1(0);
            dmax2:=d2(0);
            for k in 1..3 loop
                dmax1:=max(dmax1,d1(k));
                dmax2:=max(dmax2,d2(k));
            end loop;
         end if;
         -- p.dnc:=min(integer(dist1)*dncmax/integer(dmax1)/4,integer(dist2)*dncmax/integer(dmax2)/4);
         p.dnc:=min(natural(dist1*float(dncmax)/dmax1/4.0),natural(dist2*float(dncmax)/dmax2/4.0));
      else
         p.dnc:=min(Manipboite.dbpt(b1l,tbnomi(no).coord_objet),
                    Manipboite.dbpt(b2l,tbnomi(no).coord_objet));
      end if;

	  -- debut modif 26.06.2006
      p.poid(1):=poidnc(dncmax,p.dnc);

--      p.poid(1):=0.0;
--	  -- le poids de distance ne joue que pour les "ponctuel imprecis" et les "zonal interieur"
--      if (tbnomi(no).topo.objet_type=ponctuel and tbnomi(no).topo.mode_placement=interieur)
--	  or (tbnomi(no).topo.objet_type=Zonal and tbnomi(no).topo.mode_placement=Interieur) then
--        p.poid(1):=poidnc(dncmax,p.dnc);
--      end if;
	  -- fin modif 26.06.2006

      p.poip:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
    end;
  end if;
  
  if route then
  declare
    tarcs:Liens_array_type(1..gr_infos(grr).nb_arcs);
    lista:Point_liste_type(1..gr_infos(grr).max_pts_arc);
  begin
    -------------------------------------------------------------------
    -- le poids de dist de p est augment� si un arc � �viter de franchir
    -- s�pare p de l'objet d�sign�
    -------------------------------------------------------------------
  if p.nl=1 then
    cen_p.coor_x:=p.coin1(1).coor_x+tbnomi(no).ln(1)/2;
    cen_p.coor_y:=p.coin1(1).coor_y+tbnomi(no).corps/2;
  else
    cen_p.coor_x:=p.coin1(1).coor_x+tbnomi(no).ln1/2;
    cen_p.coor_y:=p.coin1(1).coor_y-integer(float(tbnomi(no).corps)*espace/2.0);
  end if;
  br.minimum.coor_x:=min(tbnomi(no).coord_objet.coor_x,cen_p.coor_x);
  br.maximum.coor_x:=max(tbnomi(no).coord_objet.coor_x,cen_p.coor_x);
  br.minimum.coor_y:=min(tbnomi(no).coord_objet.coor_y,cen_p.coor_y);
  br.maximum.coor_y:=max(tbnomi(no).coord_objet.coor_y,cen_p.coor_y);
  Gr_arcs_in_rectangle(grr,br.minimum,br.maximum,tarcs,na);
  if na>0 then
    for ia in 1..na loop
      arc:=Gr_arc(grr,tarcs(ia));
      for l in 1..nb_codes_route loop
        if gr_legende_l(legr, arc.att_graph).theme=tcodes_route(l) then
          Gr_points_d_arc(grr,tarcs(ia),lista,n);
          for ip in 1..n-1 loop
            if Manipboite.intersec(tbnomi(no).coord_objet,cen_p,lista(ip),lista(ip+1)) then
              poids:=p.poid(1)+cpoi_intersec_plus;
              if poids>10.0 then
                p.poid(1):=10.0;
              else
                p.poid(1):=poids;
              end if;
              poids:=cpoimu*p.poim(1)+cpoidnc*p.poid(1)+cpoidir*poidir(p.dir,pardir);
                if poids>10.0 then
                  p.poip:=10.0;
                else
                  p.poip:=poids;
                end if;
              goto label1;
            end if;
          end loop;--ip
        end if;
      end loop;--l 
    end loop;--ia
  end if;
  end;
  end if;
  
  <<label1>> if tbnomi(no).topo.mode_placement=interieur then
      if tbnomi(no).topo.objet_type=zonal then
        poids:=p.poip+poidir(p.dir,pardir);
        if poids>10.0 then
          p.poip:=10.0;
        else
          p.poip:=poids;
        end if;
      end if;
  end if;
  
  tpp(npp):=p;
end det_poids_propre;


procedure det_poids_propre_noroute(a : in out integer) is
begin
  a:=0;
end det_poids_propre_noroute;


--======================================================================
-- Les positions sont classees par poids croissant.
-- Si la position a un poids plus eleve que toutes les autres positions,
-- on la met en dernier (si on a pas encore maxp positions).
-- Sinon, elle est selectionnee a condition qu'elle fasse partie des
-- maxp meilleures positions de l'objet(:
--======================================================================
procedure TRI_POSITIONS(tpp: in typtpp;
                        npp: in typnpp;
                        no: in integer;
                        tbnomi: in typtbnomi;
                        tnp: in out typtnp;
                        tp: in out typtp ) is
                               
p: position_type:=tpp(npp);

begin
  if tnp(no)=0 then
    tnp(no):=1;
    tp(no,1):=p;
  elsif tnp(no)<maxp then
    for ip in 1..tnp(no) loop
      if p.poip<tp(no,ip).poip then
        for ip2 in reverse ip..tnp(no) loop
          tp(no,ip2+1):=tp(no,ip2);
        end loop; -- ip2
        tnp(no):=tnp(no)+1;
        tp(no,ip):=p;
        goto p_est_mise;
      end if;
    end loop; -- ip
    tnp(no):=tnp(no)+1;
    tp(no,tnp(no)):=p;
    <<p_est_mise>> null;
  elsif p.poip<tp(no,tnp(no)).poip then
    for ip in 1..tnp(no) loop
      if p.poip<tp(no,ip).poip then
        for ip2 in reverse ip..maxp-1 loop
          tp(no,ip2+1):=tp(no,ip2);
        end loop;
        tp(no,ip):=p;
        return;
      end if;
    end loop; -- ip
  end if;
end TRI_POSITIONS;
               

procedure TRI_POSITIONS_noroutes(a : in out integer) is
begin
  a:=0;
end TRI_POSITIONS_noroutes;
     

--===========================================================================
-- Cette procedure est appelee dans le programme principal.
-- Pour chaque objet a nommer, elle determine une liste de points.
-- Pour chaque point, elle determine 1 a 3 positions.
-- Pour chaque position, elle verifie sa validite,
-- 			      elle calcule son poids propre,
--			      elle la selectionne si l'objet a - de maxp pos. 
--===========================================================================
procedure DET_POSITIONS (tbnomi: in out typtbnomi;
    	                 tnci: in typtnci;	
			             tici: in typtici;
			             tzi: in typtbe;
			             tnp: in out typtnp;
                         tp: in out typtp;
                         mutil: in boolean;
                         Nb_symboles : in integer;
                         InvEchelle : in integer;
                         Resolution : in integer;
                         Minimum_terrain : in point_type_reel;
                         Maximum_terrain : in point_type_reel;
                         top : in out integer;
						 NbNom : in integer) is
                  
use type Interfaces.C.Char;
use type Win32.Bool;

use type Interfaces.C.Char_Array;
subtype C_String is Interfaces.C.Char_Array; 

function To_C_String (Str : String) return C_String is      
Result : C_String (1 .. 1024);
begin
  for I in Str'Range loop         
    Result (Interfaces.C.Size_t(I)) := Win32.Char'Val (Character'Pos (Str(I)));
  end loop;
  return Result (Interfaces.C.Size_t(Str'First) .. Interfaces.C.Size_t(Str'Last));
end To_C_String;

-- partie entiere
function E(x : in float) return integer is
begin
  if float(integer(x))<=x then
    return(integer(x));
  else
    return(integer(x)-1);
  end if;
end E;

tpp:typtpp;
npp:typnpp;
verifp:boolean:=true;
ncode:positive:=Nb_code(tbnomi);
tambi:typtambi(1..200);
nb_ambi : natural;
-- nb_pts_max:integer:=pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
-- *2 suite a plantage constat� dans LISTE_SUR_CONTOUR
nb_pts_max:integer:=2*pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
listpts:point_liste_type(1..nb_pts_max);
nb_pts:integer:=0;  -- nb de points du contour
iest,inord,iouest,isud:natural:=0;
iest_plus,inord_plus,iouest_plus,isud_plus:natural:=0;
dncmax,dxmax,dymax: natural;
hd,lno,hno:integer;
b:boite_type;
ch : string(1..20);
ncar : integer;
interdit:boolean;
lps, ncol: integer;
nlig : integer;
stripoffset: tiff_io.acces_tabstrip;
nb_interp: integer;
tinterp: substitution_io.acces_tabinterp;
tsubst: substitution_io.acces_tabsub;
tabstat : typtabstat(1..Nb_symboles,inter..fond) := (others=>(others=>0));
verdict : typverdict;
mispo : boolean:=false; -- presence de symboles ponctuels
miobs : boolean:=false; -- presence d'objets surfaciques
miecf : boolean:=false; -- presence d'ecritures floues
Delta_Terrain : float; -- extension des coordonn�es terrain quand il n'y a pas d'image de mutilation
Resol_Mut_reel : float:=0.0;
affich : affichage_type;
noeud : noeud_type;
rayon : float; -- en mm !
incoherenceX,incoherenceY : float:=0.0;
EMPRISE_ERROR : exception;
causeAmbi : boolean;
Ncol_calc, Nlig_calc : integer;

begin
	
  Charge_ambiguites(tambi, nb_ambi);
  if mutil then
    tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);

	incoherenceX:=Abs(Float(Ncol)-float(Resolution)*1000.0*Float(Xmax_Image-Xmin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
	incoherenceY:=Abs(Float(Nlig)-float(Resolution)*1000.0*float(Ymax_Image-Ymin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
    If incoherenceX>=1.0 or incoherenceY>=1.0 then
      raise EMPRISE_ERROR;
	end if;

    substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
    -- ouverture du fichier TIFF comme fichier binaire
    begin
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
    exception when  Event : others =>
      byte_io.close(file_image);
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
    end;   
  end if;

   if (mutil=true) then
       top:=top+20;
       -- AFFICHAGE_DEROULEMENT(dialog3.Etape5_5, top); NE PAS DECOMMENTER
       gb.Move(dialog3.Etape5_5, 20, gb.int(top), 185, 15);
       gb.Visible(dialog3.Etape5_5, gb.true);
       gb.Move(dialog3.CancelButton, 250, gb.int(top+35), 70, 20);
       gb.Visible(dialog3.CancelButton, gb.true);
       gb.height(dialog3.form, gb.int(top+90));
    end if;
      vno.coor_x:=vno.coor_x-0.02*float(InvEchelle);
      vno.coor_y:=vno.coor_y+0.02*float(InvEchelle);
      vse.coor_x:=vse.coor_x+0.02*float(InvEchelle);
      vse.coor_y:=vse.coor_y-0.02*float(InvEchelle);
      Delta_Terrain:=max(Vse.coor_x-Vno.coor_x,Vno.coor_y-Vse.coor_y);  
      Resol_Mut_reel:=float(InvEchelle)*8.0/Delta_Terrain; -- 8 comme 8000*8000 : dimensions de l'image de fond

  if mutil=true then
    verifmutil(tbnomi,tsubst,nb_interp,InvEchelle,Resolution,Minimum_terrain,file_image,lps,ncol,nlig,stripoffset,Nb_symboles,tabstat,verdict);
  end if;
  
  gb.Enabled(Window1.VisuEntree_SM,1);
  if mutil=true then
    if verdict=passe then
      gb.Move(dialog3.Etape5_6, 205, gb.int(top), 300, 15);
      gb.Visible(dialog3.Etape5_6, gb.true);       
    end if;
    if verdict=echoue then
      gb.Move(dialog3.Etape5_7, 205, gb.int(top), 300, 15);
      gb.Visible(dialog3.Etape5_7, gb.true);       
    end if;       
    if verdict/=passe and verdict/=echoue then
      gb.Move(dialog3.Etape5_8, 205, gb.int(top), 300, 15);
      gb.Visible(dialog3.Etape5_8, gb.true);       
    end if;
  end if;     

  ficview(tbnomi,InvEchelle,Resolution,Minimum_terrain,Nb_symboles,Resol_Mut_reel,mispo,miobs,miecf);
  
  if NbNom>0 then -- presence d'ecritures horizontales
    top:=top+20;
    gb.Move(dialog3.Etape6, 20, gb.int(top), 300, 15);
    gb.Visible(dialog3.Etape6, gb.true);  -- Calcul des positions possibles des �critures horizontales
    gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
    gb.Move(dialog3.Panel2, 0, 0, 0, 20);
    gb.Visible(dialog3.Panel1, gb.true);
    gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
    gb.height(dialog3.form, gb.int(top+115));

    for no in 1..tbnomi'last loop
    begin
	
    -- if 40*(no-1)/tbnomi'last /= 40*no/tbnomi'last or no=tbnomi'last then
    if 300*(no-1)/tbnomi'last /= 300*no/tbnomi'last or no=tbnomi'last then
      gb.Move(dialog3.Panel2,0,0,gb.INT(300*no/tbnomi'last),20);
    end if;

	if tbnomi(no).topo.Objet_type/=lineaire then
      if tbnomi(no).topo.mode_placement=interieur or tbnomi(no).topo.mode_placement=centre then
        LISTE_SUR_MAILLE(no,tbnomi,nb_pts,listpts);
      else --exterieur
        LISTE_SUR_CONTOUR(no,tbnomi,nb_pts,listpts,iest,inord,iouest,isud,iest_plus,inord_plus,iouest_plus,isud_plus);
      end if;
      -------------------------------------------------------------------
      -- Determination de la distance max du nom au centre de l'objet: --
      -------------------------------------------------------------------
      lno:=tbnomi(no).ln(1);
      hno:=tbnomi(no).corps;
      hd:=integer(dilat*float(hno));

      if tbnomi(no).topo.objet_type=zonal then
      	b:=GR_ENCOMBREMENT_FACE(gros,tbnomi(no).topo.num_objet);
      elsif tbnomi(no).topo.objet_type=ponctuel then
	      noeud:=GR_NOEUD(gros,tbnomi(no).topo.num_objet);
	      affich:=GR_LEGENDE_P(legos,noeud.att_graph);
	      rayon:=(affich.largeur/2.0)*float(ptparmm);
  	      b:=((tbnomi(no).coord_objet.coor_x-integer(rayon), 
	           tbnomi(no).coord_objet.coor_y-integer(rayon)), 
	          (tbnomi(no).coord_objet.coor_x+integer(rayon), 
		       tbnomi(no).coord_objet.coor_y+integer(rayon)));
      end if;
           
      dxmax:=max(b.maximum.coor_x-tbnomi(no).coord_objet.coor_x
			     +2*integer(sqrt(2.0)*decalno*float(hno)),
		         tbnomi(no).coord_objet.coor_x-b.minimum.coor_x
			     +2*integer(sqrt(2.0)*decalno*float(hno)));
                
      dymax:=max(b.maximum.coor_y-tbnomi(no).coord_objet.coor_y
			     +2*integer(sqrt(2.0)*decalno*float(hno)),
		         tbnomi(no).coord_objet.coor_y-b.minimum.coor_y
			     +2*integer(sqrt(2.0)*decalno*float(hno)));
                
      Dncmax:=natural(sqrt(float(dxmax)**2+float(dymax)**2))+2;
      -- Dncmax:=natural(sqrt(float(dxmax**2+dymax**2)))+2;
     
    --------------------------------------------
    -- Chargement de l'image des mutilations: --
    --------------------------------------------
    if mutil then
      Declare
      -- En coordonnes Image: --
      xpixmin:integer:=max(1,(tzi(no).minimum.coor_x-pso.coor_x)/pt_image+1);
      xpixmax:integer:=min(delta_xpix,(tzi(no).maximum.coor_x-pso.coor_x)/pt_image+1);
      ypixmin:integer:=min(delta_ypix,delta_ypix-((tzi(no).minimum.coor_y-pso.coor_y)/pt_image)); 
      ypixmax:integer:=max(1,delta_ypix-((tzi(no).maximum.coor_y-pso.coor_y)/pt_image));
       
      begin

	    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
          tbnomi(no).cause:=hors_image;
        else
          declare
            npx:positive:=xpixmax-xpixmin+1;
            npy:positive:=ypixmin-ypixmax+1;  --NB:coordonnees en y inversees!!!
            tpoi:typtipoi(1..npx,1..npy);
          begin
            begin  
              substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,
                                   tbnomi(no).topo.code, lps, ncol, stripoffset,
                                   tinterp, tsubst, nb_interp, tpoi); 
--              exception when Event : others =>
--              Msg_Erreur:=To_Unbounded_String("Erreur de lecture de l'image: "
--			              &eol&nomfichier_image(1..ncar_fi)
--						  &eol&eol&"au cours du calcul des positions possibles"
--						  &eol&"de l'�criture d'identifiant: "&integer'image(tbnomi(No).id));
--              raise;
            end;                                       

            causeAmbi:=true;
            ------------------------------------------------------------------------
            -- Determination des positions, filtrage, calcul du poids propre et tri:
            ------------------------------------------------------------------------
	        for ipt in 1..nb_pts loop
              npp:=0;
	          DET_P(no,tbnomi,ipt,listpts,iest,inord,iouest,isud,
		              iest_plus,inord_plus,iouest_plus,isud_plus,tpp,npp);
              if npp/=0 then
                for np in 1..npp loop
                  FILTRE(tpp,np,no,tbnomi,tnci,tici,tambi,nb_ambi,verifp);
                  if verifp=true then -- p n'est pas ambigue !
				  	causeAmbi:=false;
                    DET_POIDS_PROPRE(tpp,np,no,tbnomi,tpoi,tzi,dncmax,interdit);
                    if interdit=false then -- p n'ecrase pas de pixel interdit !
                      TRI_POSITIONS(tpp,np,no,tbnomi,tnp,tp);
                    elsif interdit=true and tbnomi(no).topo.mode_placement=centre then
                      null;
		              -- gb.MsgBox("Le nom n'a qu'1 pos. possible, mais qui mutile l'image !");
                    end if;
                  elsif verifp=false and tbnomi(no).topo.mode_placement=centre then
                    -- gb.MsgBox("Le nom suivant n'a qu'1 pos. possible, mais ambigu� ou debordant du cadre de l'image!");
					null;
                  end if;
                end loop; --np
 	          end if; --npp
            end loop; --ipt

			if causeAmbi=True then
			  tbnomi(no).cause:=Ambigu;
            end if;

          end; --declare Tpoi
	    end if; -- xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
	  end; --declare Xpixmin, etc
    else -- mutil=false
      ------------------------------------------------------------------------
      -- Determination des positions, filtrage, calcul du poids propre et tri:
      ------------------------------------------------------------------------
      causeAmbi:=true;
      for ipt in 1..nb_pts loop
	    npp:=0;
	    DET_P(no,tbnomi,ipt,listpts,iest,inord,iouest,isud,iest_plus,inord_plus,iouest_plus,isud_plus,tpp,npp);
        if npp/=0 then
    	  for np in 1..npp loop
		    if tbnomi(No).Topo.Objet_type=Lineaire then
		      Verifp:=true;
            else
              FILTRE(tpp,np,no,tbnomi,tnci,tici,tambi,nb_ambi,verifp);
            end if;
            if verifp=true then -- p n'est pas ambigue !
              causeAmbi:=false;
	          DET_POIDS_PROPRE(tpp,np,no,tbnomi,tzi,dncmax,interdit);
		      TRI_POSITIONS(tpp,np,no,tbnomi,tnp,tp);
            end if; --verifp
      	  end loop;
 	    end if; --npp
      end loop; --ipt

	  if causeAmbi=true then
        tbnomi(no).cause:=Ambigu;
      end if;
    end if;  --mutil
  end if;  -- lineaire

  exception when Event : others =>
    if Msg_Erreur=To_Unbounded_String("Erreur d'ex�cution") then
       Msg_Erreur:=To_Unbounded_String("Erreur de d�termination des positions"&eol
                                       &"Ligne n�"&integer'image(no)&" du fichier des �critures horizontales");
    end if;
    raise;
  end;
  end loop;  --no
  end if; -- NbNom/=0
  if mutil then
    byte_io.close(file_image);
    tiff_io.Free_tabstrip(stripoffset);
    substitution_io.Free_tabinterp(tinterp);
    substitution_io.Free_tabsub(tsubst);
  end if;

exception
  when EMPRISE_ERROR =>
	Ncol_calc:=E(float(Resolution)*1000.0*Float(Xmax_Image-Xmin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
	Nlig_calc:=E(float(Resolution)*1000.0*Float(Ymax_Image-Ymin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
    Msg_Erreur:=To_Unbounded_String("L'emprise terrain de l'image, sa r�solution et l'�chelle des donn�es"
                     	            &eol&"sont incompatibles avec les dimensions physiques de l'image"
                     	        &eol&eol&"dimensions de l'image :"&tab&integer'image(ncol)&"  � "&integer'image(nlig)
                     	            &eol&"dimensions calcul�es :"&tab&integer'image(ncol_calc)&"  � "&integer'image(nlig_calc));
	raise;
end det_positions;


--=======================================================================
-- Procedure qui calcule les interactions entre positions:
----------------------------------------------------------
procedure DET_PI(tbnomi: in out typtbnomi; -- tableau des toponymes
                 tnci: in typtnci;
                 tici: in typtici;
                 tnp: in typtnp;
                 tp: in typtp;
                 tnpi: in out typtnpi;
                 tppi: in out typtppi;
                 lpi: in out typlpi) is
               
hi,dilli,dilhi,hj,dillj,dilhj : integer;
li,l1i,l2i,lj,l1j,l2j,j : integer:=0; 
bi,b1i,b2i,bj,b1j,b2j : bloc_type; -- Boites des ecritures
bdi,bd1i,bd2i,bdj,bd1j,bd2j : bloc_type; -- Boites dilatees
bdblastopi,bdblasdowni,bdblastopj,bdblasdownj : bloc_type;--boites dilatees des blasons
d_ip_j,d_jp_i : natural;
ncode : positive:=Nb_code(tbnomi);
tambi : typtambi(1..200);
nb_ambi : natural;
nlpi:integer:=0; -- Compteur de la liste lpi
ambi : boolean:=false;
nlI, NlJ : positive range 1..2;
TnpiIIp : integer:=0;
UneBoite : boite_type;
UnBloc : bloc_type;

begin
	
  Charge_ambiguites(tambi,nb_ambi);

  -- Boucle sur chaque objet i a nommer:
  for i in 1..tbnomi'last loop
  	
    -- if 40*(I-1)/tbnomi'last /= 40*i/tbnomi'last or i=tbnomi'last then		
    if 300*(I-1)/tbnomi'last /= 300*i/tbnomi'last or i=tbnomi'last then		
      gb.Move(dialog3.Panel2,0,0,gb.INT(300*i/tbnomi'last),20);
    end if;

	hi:=tbnomi(i).corps; 
    dilli:=integer(float(tbnomi(i).corps)*dilx);
    dilhi:=integer(float(tbnomi(i).corps)*dily);
    l1i:=tbnomi(i).ln1; 
    l2i:=tbnomi(i).ln2; 

    if tnci(i)>0 then
      -- Boucle sur les positions possibles ip de i: 
     for ip in 1..tnp(i) loop
	  TnpiIIp:=0;
       -- Boucle sur les mots de i: 
	  for MotI in 1..tp(i,Ip).Nb_Mots loop
	  	li:=tbnomi(i).ln(motI);
	    if tp(i,ip).nl=1 or tbnomi(i).topo.objet_type=lineaire then
		  NlI:=1;
        else
          NlI:=2;
        end if;

		UneBoite:=Manipboite.dilatee(tbnomi(i).empblastop(nlI),dilli,dilhi);
        bdblastopi:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
        UneBoite:=Manipboite.dilatee(tbnomi(i).empblasdown(nlI),dilli,dilhi);
        bdblasdowni:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));

        if nlI=1 then
--          bi.minimum:=tp(i,ip).coin1(motI);
--          -- bi.maximum:=(tp(i,ip).coin1.coor_x+li,tp(i,ip).coin1.coor_y+hi);
--          bi.maximum:=(tp(i,ip).coin1(motI).coor_x+integer(float(li)*cos(Tp(I,Ip).angle(motI))+float(Hi)*abs(sin(Tp(I,Ip).angle(motI))))
--		              ,tp(i,ip).coin1(motI).coor_y+integer(float(hi)*cos(Tp(I,Ip).angle(motI))+float(Li)*abs(sin(Tp(I,Ip).angle(motI)))));

          QUATRE_COINS(tp(i,ip).coin1(motI),Tp(I,Ip).angle(motI),Li,Hi,Bi);

--       	  bdblastopi.minimum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblastopi.minimum.coor_x;
--	      bdblastopi.minimum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblastopi.minimum.coor_y;
--	      bdblastopi.maximum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblastopi.maximum.coor_x;
--	      bdblastopi.maximum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblastopi.maximum.coor_y;

       	  bdblastopi.P1:=tp(i,ip).coin1(motI)+bdblastopi.P1;
	      bdblastopi.P2:=tp(i,ip).coin1(motI)+bdblastopi.P2;
	      bdblastopi.P3:=tp(i,ip).coin1(motI)+bdblastopi.P3;
	      bdblastopi.P4:=tp(i,ip).coin1(motI)+bdblastopi.P4;

--	      bdblasdowni.minimum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblasdowni.minimum.coor_x;
--	      bdblasdowni.minimum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblasdowni.minimum.coor_y;
--	      bdblasdowni.maximum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblasdowni.maximum.coor_x;
--	      bdblasdowni.maximum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblasdowni.maximum.coor_y;

          bdblasdowni.P1:=tp(i,ip).coin1(motI)+bdblasdowni.P1;
          bdblasdowni.P2:=tp(i,ip).coin1(motI)+bdblasdowni.P2;
          bdblasdowni.P3:=tp(i,ip).coin1(motI)+bdblasdowni.P3;
          bdblasdowni.P4:=tp(i,ip).coin1(motI)+bdblasdowni.P4;
        else
--          b1i.minimum:= tp(i,ip).coin1(motI);
--          b1i.maximum:=(tp(i,ip).coin1(motI).coor_x+l1i,tp(i,ip).coin1(motI).coor_y+hi);
--          b2i.minimum:= tp(i,ip).coin2;
--          b2i.maximum:=(tp(i,ip).coin2.coor_x+l2i,tp(i,ip).coin2.coor_y+hi);

          UneBoite.minimum:= tp(i,ip).coin1(motI);
          UneBoite.maximum:=(tp(i,ip).coin1(motI).coor_x+l1i,tp(i,ip).coin1(motI).coor_y+hi);
          B1I:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
          UneBoite.minimum:= tp(i,ip).coin2;
          UneBoite.maximum:=(tp(i,ip).coin2.coor_x+l2i,tp(i,ip).coin2.coor_y+hi);
          B2I:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));

--     	  bdblastopi.minimum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblastopi.minimum.coor_x;
--	      bdblastopi.minimum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblastopi.minimum.coor_y;
--	      bdblastopi.maximum.coor_x:=tp(i,ip).coin1(motI).coor_x + bdblastopi.maximum.coor_x;
--	      bdblastopi.maximum.coor_y:=tp(i,ip).coin1(motI).coor_y + bdblastopi.maximum.coor_y;

     	  bdblastopi.P1:=tp(i,ip).coin1(motI)+bdblastopi.P1;
     	  bdblastopi.P2:=tp(i,ip).coin1(motI)+bdblastopi.P2;
     	  bdblastopi.P3:=tp(i,ip).coin1(motI)+bdblastopi.P3;
     	  bdblastopi.P4:=tp(i,ip).coin1(motI)+bdblastopi.P4;          

--          bdblasdowni.minimum.coor_x:=tp(i,ip).coin2.coor_x + bdblasdowni.minimum.coor_x;
--	      bdblasdowni.minimum.coor_y:=tp(i,ip).coin2.coor_y + bdblasdowni.minimum.coor_y;
--	      bdblasdowni.maximum.coor_x:=tp(i,ip).coin2.coor_x + bdblasdowni.maximum.coor_x;
--	      bdblasdowni.maximum.coor_y:=tp(i,ip).coin2.coor_y + bdblasdowni.maximum.coor_y;

		  bdblasdowni.P1:=tp(i,ip).coin2+bdblasdowni.P1;
		  bdblasdowni.P2:=tp(i,ip).coin2+bdblasdowni.P2;
		  bdblasdowni.P3:=tp(i,ip).coin2+bdblasdowni.P3;
		  bdblasdowni.P4:=tp(i,ip).coin2+bdblasdowni.P4;

        end if;
        -- dilatons les boites de ip:
        if nlI=1 then
          bdi :=Manipboite.dilatee_oblique(bi,Tp(I,Ip).angle(motI),dilli,dilhi);
        else
		  UneBoite:=Manipboite.dilatee((b1i.P1,b1i.P3),dilli,dilhi);
		  Bd1I:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
          UneBoite:=Manipboite.dilatee((b2i.P1,B2I.P3),dilli,dilhi);
          bd2i:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
        end if;

        -- Boucle sur les objets j en interaction avec l'objet i : 
        for jc in 1..tnci(i) loop
		  j:=tici(i,jc);
          hj:=tbnomi(j).corps; 
          dillj:=integer(float(tbnomi(j).corps)*dilx);
          dilhj:=integer(float(tbnomi(j).corps)*dily);
          -- lj:=tbnomi(j).ln(1); 
          l1j:=tbnomi(j).ln1; 
          l2j:=tbnomi(j).ln2;
          
          -- Boucle sur les positions possibles jp de jc: 
          for jp in 1..tnp(j) loop
		   if MotI>=2 then
		     for IndiceLpi in tppi(I,Ip)..tppi(I,Ip)+tnpi(I,Ip)-1 loop
			   if lpi(IndiceLpi)=(J,Jp) then
                 goto FinBoucleJp;
               end if;
			 end loop;	
           end if;
           -- Boucle sur les mots de jp:
           for MotJ in 1..tp(J,Jp).Nb_Mots loop
            lj:=tbnomi(j).ln(motJ); 
            if tp(J,Jp).nl=1 or tbnomi(J).topo.objet_type=lineaire then
              NlJ:=1;
			else
              NlJ:=2;
            end if;
            -- bdblastopj:=Manipboite.dilatee(tbnomi(j).empblastop(nlJ),dillj,dilhj);
			UneBoite:=Manipboite.dilatee(tbnomi(j).empblastop(nlJ),dillj,dilhj);
            bdblastopj:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));

	        -- bdblasdownj:=Manipboite.dilatee(tbnomi(j).empblasdown(nlJ),dillj,dilhj); 
			UneBoite:=Manipboite.dilatee(tbnomi(j).empblasdown(nlJ),dillj,dilhj);
            bdblasdownj:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));

            if nlJ=1 then
--              bj.minimum:=tp(j,jp).coin1(MotJ);
--              -- bj.maximum:=(tp(j,jp).coin1.coor_x+lj,tp(j,jp).coin1.coor_y+hj);
--              bj.maximum:=(tp(j,jp).coin1(motJ).coor_x+integer(float(lj)*cos(Tp(j,jp).angle(motJ))+float(Hj)*abs(sin(Tp(j,jp).angle(motJ))))
--			              ,tp(j,jp).coin1(motJ).coor_y+integer(float(Hj)*cos(Tp(j,jp).angle(motJ))+float(Lj)*abs(sin(Tp(j,jp).angle(motJ)))));

                QUATRE_COINS(tp(J,Jp).coin1(motJ),Tp(J,Jp).angle(motJ),Lj,Hj,Bj);

--                bdblastopj.minimum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblastopj.minimum.coor_x;
--	            bdblastopj.minimum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblastopj.minimum.coor_y;
--	            bdblastopj.maximum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblastopj.maximum.coor_x;
--	            bdblastopj.maximum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblastopj.maximum.coor_y;

                bdblastopj.P1:=tp(j,jp).coin1(motJ)+bdblastopj.P1;
                bdblastopj.P2:=tp(j,jp).coin1(motJ)+bdblastopj.P2;
                bdblastopj.P3:=tp(j,jp).coin1(motJ)+bdblastopj.P3;
                bdblastopj.P4:=tp(j,jp).coin1(motJ)+bdblastopj.P4;

--	            bdblasdownj.minimum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblasdownj.minimum.coor_x;
--	            bdblasdownj.minimum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblasdownj.minimum.coor_y;
--	            bdblasdownj.maximum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblasdownj.maximum.coor_x;
--	            bdblasdownj.maximum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblasdownj.maximum.coor_y;

			    bdblasdownj.P1:=tp(j,jp).coin1(motJ)+bdblasdownj.P1;
			    bdblasdownj.P2:=tp(j,jp).coin1(motJ)+bdblasdownj.P2;
			    bdblasdownj.P3:=tp(j,jp).coin1(motJ)+bdblasdownj.P3;
			    bdblasdownj.P4:=tp(j,jp).coin1(motJ)+bdblasdownj.P4;

            else			
--              b1j.minimum:=tp(j,jp).coin1(motJ);
--              b1j.maximum:=(tp(j,jp).coin1(motJ).coor_x+l1j,tp(j,jp).coin1(1).coor_y+hj);
--              b2j.minimum:= tp(j,jp).coin2;
--              b2j.maximum:=(tp(j,jp).coin2.coor_x+l2j,tp(j,jp).coin2.coor_y+hj); 

                UneBoite.Minimum:=tp(j,jp).coin1(motJ);
                UneBoite.maximum:=(tp(j,jp).coin1(motJ).coor_x+l1j,tp(j,jp).coin1(1).coor_y+hj);
                B1J:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
                UneBoite.minimum:=tp(j,jp).coin2;
                UneBoite.maximum:=(tp(j,jp).coin2.coor_x+l2j,tp(j,jp).coin2.coor_y+hj);
                B2j:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));

--     	      bdblastopj.minimum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblastopj.minimum.coor_x;
--	          bdblastopj.minimum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblastopj.minimum.coor_y;
--	          bdblastopj.maximum.coor_x:=tp(j,jp).coin1(motJ).coor_x + bdblastopj.maximum.coor_x;
--	          bdblastopj.maximum.coor_y:=tp(j,jp).coin1(motJ).coor_y + bdblastopj.maximum.coor_y;

     	      bdblastopj.P1:=tp(j,jp).coin1(motJ)+bdblastopj.P1;
     	      bdblastopj.P2:=tp(j,jp).coin1(motJ)+bdblastopj.P2;
     	      bdblastopj.P3:=tp(j,jp).coin1(motJ)+bdblastopj.P3;
     	      bdblastopj.P4:=tp(j,jp).coin1(motJ)+bdblastopj.P4;

--	          bdblasdownj.minimum.coor_x:=tp(j,jp).coin2.coor_x + bdblasdownj.minimum.coor_x;
--	          bdblasdownj.minimum.coor_y:=tp(j,jp).coin2.coor_y + bdblasdownj.minimum.coor_y;
--	          bdblasdownj.maximum.coor_x:=tp(j,jp).coin2.coor_x + bdblasdownj.maximum.coor_x;
--	          bdblasdownj.maximum.coor_y:=tp(j,jp).coin2.coor_y + bdblasdownj.maximum.coor_y;

	          bdblasdownj.P1:=tp(j,jp).coin2+bdblasdownj.P1;
	          bdblasdownj.P2:=tp(j,jp).coin2+bdblasdownj.P2;
	          bdblasdownj.P3:=tp(j,jp).coin2+bdblasdownj.P3;
	          bdblasdownj.P4:=tp(j,jp).coin2+bdblasdownj.P4;

            end if;
            -- dilatons les boites de jp: 
            if nlJ=1 then
              bdj :=Manipboite.dilatee_oblique(bj,Tp(J,Jp).angle(motJ),dillj,dilhj);
            else
              UneBoite:=Manipboite.dilatee((b1j.P1,B1J.P3),dillj,dilhj);
              bd1j:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
              UneBoite:=Manipboite.dilatee((b2j.P1,b2j.P3),dillj,dilhj);
			  Bd2j:=(UneBoite.Minimum,(UneBoite.Minimum.Coor_X,UneBoite.Maximum.Coor_Y),UneBoite.Maximum,(UneBoite.Maximum.Coor_X,UneBoite.Minimum.Coor_Y));
            end if;
            
            -- Si AMBIGUITES: Comp. des d_i_bj � d_i_bi et d_j_bi � d_j_bj:
            ambi:=false;
            if tbnomi(i).topo.Objet_type/=lineaire and tbnomi(J).topo.Objet_type/=lineaire then   -- seules les ecritures horiz. peuvent etre ambigues
			  for k in 1..nb_ambi loop
                if ((tambi(k).code1= tbnomi(i).topo.code)
                   and (tambi(k).code2=tbnomi(j).topo.code))
                   or  ((tambi(k).code2= tbnomi(i).topo.code)
                   and (tambi(k).code1=tbnomi(j).topo.code))
                   or  (tbnomi(i).topo.code=tbnomi(j).topo.code) then  
                   ambi:=true;
                   exit;
                 end if;
               end loop; --k
             end if;
             if ambi then
              -- Determinons d_jp_i=dist. de jp au cen de ic:
              if nlJ=1 then
                -- d_jp_i:=Manipboite.dbpt(bj,tbnomi(i).coord_objet);
				d_jp_i:=Manipboite.dbpt((bj.P1,Bj.P3),tbnomi(i).coord_objet);
              
              else
                -- d_jp_i:=min(Manipboite.dbpt(b1j,tbnomi(i).coord_objet),Manipboite.dbpt(b2j,tbnomi(i).coord_objet));
                d_jp_i:=min(Manipboite.dbpt((b1j.P1,b1j.P3),tbnomi(i).coord_objet),Manipboite.dbpt((b2j.P1,b2j.P3),tbnomi(i).coord_objet));
              end if;
              -- jp est-elle plus proche que ip du cen de ic ?
              if d_jp_i>=tp(i,ip).dnc then
                -- Determinons d_ip_j:
                if nlI=1 then
                  -- d_ip_j:=Manipboite.dbpt(bi,tbnomi(j).coord_objet);
                  d_ip_j:=Manipboite.dbpt((bi.P1,Bi.P3),tbnomi(j).coord_objet);
                else
                  -- d_ip_j:=min(Manipboite.dbpt(b1i,tbnomi(j).coord_objet),
                  --            Manipboite.dbpt(b2i,tbnomi(j).coord_objet));
                  d_ip_j:=min(Manipboite.dbpt((b1i.P1,b1i.P3),tbnomi(j).coord_objet),
                              Manipboite.dbpt((b2i.P1,b2i.P3),tbnomi(j).coord_objet));
                end if;
                -- ip est-elle plus proche que jp du cen de jc ?
                if d_ip_j>=tp(j,jp).dnc then
                  --boite dilatee de ip chevauche b. dilatee de jp ?
                  case nlI is
                    when 1=>case nlJ is 
                      when 1=>if Manipboite.chevauch((bdi.P1,bdi.P3),(bdj.P1,bdj.P3))
                        	  or Manipboite.chevauch((bdi.P1,bdi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdi.P1,bdi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdj.P1,bdj.P3),(bdblastopi.P1,bdblastopi.P3)) 
				              or Manipboite.chevauch((bdj.P1,bdj.P3),(bdblasdowni.P1,bdblasdowni.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
 				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
                                if tnpi(i,ip)<maxnpi then
								  nlpi:=nlpi+1;
                                  tnpi(i,ip):=tnpi(i,ip)+1;
                                  if tnpi(i,ip)<=1 then
                                    tppi(i,ip):=nlpi;
                                  end if;
                                  lpi(nlpi):=(j,jp);
                                  if tnpi(i,ip)=maxnpi then
				  	                -- Risque_Chevauchement:=true;
								    TnpiIIp:=Maxnpi;
								    -- goto finBoucleIp;
								  end if;
						          goto finBoucleJp;
                                else
                                  TnpiIIp:=TnpiIIp+1;
                                end if;
                              end if;
                      when 2=>if Manipboite.chevauch((bdi.P1,bdi.P3),(bd1j.P1,bd1j.P3))
                              or Manipboite.chevauch((bdi.P1,bdi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdi.P1,bdi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bd1j.P1,bd1j.P3),(bdblastopi.P1,bdblastopi.P3)) 
				              or Manipboite.chevauch((bd1j.P1,bd1j.P3),(bdblasdowni.P1,bdblasdowni.P3))
                              or Manipboite.chevauch((bdi.P1,bdi.P3),(bd2j.P1,bd2j.P3))
                              or Manipboite.chevauch((bd2j.P1,bd2j.P3),(bdblastopi.P1,bdblastopi.P3))
				              or Manipboite.chevauch((bd2j.P1,bd2j.P3),(bdblasdowni.P1,bdblasdowni.P3)) 
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
                                if tnpi(i,ip)<maxnpi then
                                  nlpi:=nlpi+1;
                                  tnpi(i,ip):=tnpi(i,ip)+1;
	                              if tnpi(i,ip)<=1 then
                                    tppi(i,ip):=nlpi;
                                  end if;
								  lpi(nlpi):=(j,jp);
                                  if tnpi(i,ip)=maxnpi then
				  	                -- Risque_Chevauchement:=true;
								    TnpiIIp:=Maxnpi;
								    -- goto finBoucleIp;
								  end if;
						          goto finBoucleJp;
							    else
                                  TnpiIIp:=TnpiIIp+1;
                                end if;
                              end if;
                    end case;
                  
                    when 2=>case nlJ is
                      when 1=>if Manipboite.chevauch((bd1i.P1,bd1i.P3),(bdj.P1,bdj.P3))
                          	  or Manipboite.chevauch((bd1i.P1,bd1i.P3),(bdblastopj.P1,bdblastopj.P3))
                              or Manipboite.chevauch((bd1i.P1,bd1i.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdj.P1,bdj.P3),(bdblastopi.P1,bdblastopi.P3))
				              or Manipboite.chevauch((bdj.P1,bdj.P3),(bdblasdowni.P1,bdblasdowni.P3)) 
                              or Manipboite.chevauch((bd2i.P1,bd2i.P3),(bdj.P1,bdj.P3))
                         	  or Manipboite.chevauch((bd2i.P1,bd2i.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bd2i.P1,bd2i.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
                                if tnpi(i,ip)<maxnpi then
								  nlpi:=nlpi+1;
                                  tnpi(i,ip):=tnpi(i,ip)+1;
                                  if tnpi(i,ip)<=1 then
                                    tppi(i,ip):=nlpi; 
                                  end if;
								  lpi(nlpi):=(j,jp);
                                  if tnpi(i,ip)=maxnpi then
				  	                -- Risque_Chevauchement:=true;
									TnpiIIp:=maxnpi;
								    -- goto finBoucleIp;
								  end if;
						          goto finBoucleJp;
							    else
								  TnpiIIp:=TnpiIIp+1;
                                end if;
                              end if;
                      when 2=>if Manipboite.chevauch((bd1i.p1,bd1i.p3),(bd1j.p1,bd1j.p3))
                              or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bdblastopj.p1,bdblastopj.p3))
				              or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bdblasdownj.p1,bdblasdownj.p3))
				              or Manipboite.chevauch((bd1j.p1,bd1j.p3),(bdblastopi.p1,bdblastopi.p3))
				              or Manipboite.chevauch((bd1j.p1,bd1j.p3),(bdblasdowni.p1,bdblasdowni.p3))
                              or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bd2j.p1,bd2j.p3)) 
 				              or Manipboite.chevauch((bd2j.p1,bd2j.p3),(bdblastopi.p1,bdblastopi.p3)) 
				              or Manipboite.chevauch((bd2j.p1,bd2j.p3),(bdblasdowni.p1,bdblasdowni.p3)) 
                              or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bd1j.p1,bd1j.p3))
 				              or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bdblastopj.p1,bdblastopj.p3))
				              or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bdblasdownj.p1,bdblasdownj.p3)) 
                              or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bd2j.p1,bd2j.p3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3))
				              or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
                                if tnpi(i,ip)<maxnpi then
								  nlpi:=nlpi+1;
                                  tnpi(i,ip):=tnpi(i,ip)+1;
                                  if tnpi(i,ip)<=1 then
                                    tppi(i,ip):=nlpi;
                                  end if;
                                  lpi(nlpi):=(j,jp);
                                  if tnpi(i,ip)=maxnpi then
				  	                -- Risque_Chevauchement:=true;
									TnpiIIp:=maxnpi;
								    -- goto finBoucleIp;
								  end if;
						          goto finBoucleJp;
						        else
								  TnpiIIp:=TnpiIIp+1;
						        end if;
                              end if;
                    end case;
                  end case;

			    else
			      if tnpi(i,ip)<maxnpi then
		   		    nlpi:=nlpi+1;
                    tnpi(i,ip):=tnpi(i,ip)+1;
	                if tnpi(i,ip)<=1 then
                      tppi(i,ip):=nlpi;
                    end if;
		   		    lpi(nlpi):=(j,jp);
				    if tnpi(i,ip)=maxnpi then
				  	  -- Risque_Chevauchement:=true;
				      TnpiIIp:=maxnpi;
                      -- goto finBoucleIp;
			        end if;
			        goto finBoucleJp;
			      else
			  	    TnpiIIp:=TnpiIIp+1;
                  end if;
                end if;

              else
				if tnpi(i,ip)<maxnpi then
				  nlpi:=nlpi+1;
                  tnpi(i,ip):=tnpi(i,ip)+1;
	        	  if tnpi(i,ip)<=1 then
                    tppi(i,ip):=nlpi;
                  end if;
				  lpi(nlpi):=(j,jp);
                  if tnpi(i,ip)=maxnpi then
				    -- Risque_Chevauchement:=true;
				    TnpiIIp:=maxnpi;
				    -- goto finBoucleIp;
				  end if;
			      goto finBoucleJp;
			    else
			  	  TnpiIIp:=TnpiIIp+1;
                end if;
              end if;
                
	          else -- ambi Calcul des chevauchements des boites:
              case nlI is
                when 1=>case nlJ is 
                  when 1=>if Manipboite.chevauch_oblique(bdi,bdj)
						  or Manipboite.chevauch_oblique(bdi,bdblastopj)
						  or Manipboite.chevauch_oblique(bdi,bdblasdownj)
						  or Manipboite.chevauch_oblique(bdj,bdblastopi) 
						  or Manipboite.chevauch_oblique(bdj,bdblasdowni)
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
						    if tnpi(i,ip)<maxnpi then
                              nlpi:=nlpi+1;
                              tnpi(i,ip):=tnpi(i,ip)+1;
                              if tnpi(i,ip)<=1 then
                                tppi(i,ip):=nlpi;
                              end if;
                              lpi(nlpi):=(j,jp);
                              if tnpi(i,ip)=maxnpi then
				  	            -- Risque_Chevauchement:=true;
								TnpiIIp:=Maxnpi;
							    -- goto finBoucleIp;
							  end if;
						      goto finBoucleJp;
                            else
                              TnpiIIp:=TnpiIIp+1;
                            end if;
                          end if;
                  when 2=>if Manipboite.chevauch_oblique(bdi,bd1j)
                          or Manipboite.chevauch_oblique(bdi,bdblastopj)
                          or Manipboite.chevauch_oblique(bdi,bdblasdownj)
				          or Manipboite.chevauch_oblique(bd1j,bdblastopi) 
				          or Manipboite.chevauch_oblique(bd1j,bdblasdowni)	 
                          or Manipboite.chevauch_oblique(bdi,bd2j)
                          or Manipboite.chevauch_oblique(bd2j,bdblastopi)
				          or Manipboite.chevauch_oblique(bd2j,bdblasdowni) 
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
						    if tnpi(i,ip)<maxnpi then
                              nlpi:=nlpi+1;
                              tnpi(i,ip):=tnpi(i,ip)+1;
                              if tnpi(i,ip)<=1 then
                                tppi(i,ip):=nlpi;
                              end if;
                              lpi(nlpi):=(j,jp);
							  if tnpi(i,ip)=maxnpi then
				  	            -- Risque_Chevauchement:=true;
								TnpiIIp:=Maxnpi;
							    -- goto finBoucleIp;
							  end if;
		                      goto finBoucleJp;
						    else
                              TnpiIIp:=TnpiIIp+1;
                            end if;
                          end if;
                  end case;
                when 2=>case nlJ is
                  when 1=>if Manipboite.chevauch_oblique(bd1i,bdj)
                          or Manipboite.chevauch_oblique(bd1i,bdblastopj)
                          or Manipboite.chevauch_oblique(bd1i,bdblasdownj)
                          or Manipboite.chevauch_oblique(bdj,bdblastopi) 
                          or Manipboite.chevauch_oblique(bdj,bdblasdowni) 
                          or Manipboite.chevauch_oblique(bd2i,bdj)
                          or Manipboite.chevauch_oblique(bd2i,bdblastopj)
				          or Manipboite.chevauch_oblique(bd2i,bdblasdownj) 
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
						    if tnpi(i,ip)<maxnpi then
                              nlpi:=nlpi+1;
                              tnpi(i,ip):=tnpi(i,ip)+1;
                              if tnpi(i,ip)<=1 then
                                tppi(i,ip):=nlpi;
                              end if;
                              lpi(nlpi):=(j,jp);
                              if tnpi(i,ip)=maxnpi then
				  	            -- Risque_Chevauchement:=true;
								TnpiIIp:=Maxnpi;
							    -- goto finBoucleIp;
							  end if;
			                  goto finBoucleJp;
							else
                              TnpiIIp:=TnpiIIp+1;
                            end if;
                          end if;
                  when 2=>if Manipboite.chevauch((bd1i.p1,bd1i.p3),(bd1j.p1,bd1j.p3))
                          or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bdblastopj.p1,bdblastopj.p3))
				          or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bdblasdownj.p1,bdblasdownj.p3))
				          or Manipboite.chevauch((bd1j.p1,bd1j.p3),(bdblastopi.p1,bdblastopi.p3))
				          or Manipboite.chevauch((bd1j.p1,bd1j.p3),(bdblasdowni.p1,bdblasdowni.p3))
                          or Manipboite.chevauch((bd1i.p1,bd1i.p3),(bd2j.p1,bd2j.p3)) 
 				          or Manipboite.chevauch((bd2j.p1,bd2j.p3),(bdblastopi.p1,bdblastopi.p3)) 
				          or Manipboite.chevauch((bd2j.p1,bd2j.p3),(bdblasdowni.p1,bdblasdowni.p3)) 
                          or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bd1j.p1,bd1j.p3))
 				          or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bdblastopj.p1,bdblastopj.p3))
				          or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bdblasdownj.p1,bdblasdownj.p3)) 
                          or Manipboite.chevauch((bd2i.p1,bd2i.p3),(bd2j.p1,bd2j.p3))
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblastopj.P1,bdblastopj.P3))
				          or Manipboite.chevauch((bdblastopi.P1,bdblastopi.P3),(bdblasdownj.P1,bdblasdownj.P3))
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblastopj.P1,bdblastopj.P3)) 
				          or Manipboite.chevauch((bdblasdowni.P1,bdblasdowni.P3),(bdblasdownj.P1,bdblasdownj.P3)) then
						    if tnpi(i,ip)<maxnpi then
                              nlpi:=nlpi+1;
                              tnpi(i,ip):=tnpi(i,ip)+1;
	                          if tnpi(i,ip)<=1 then
                                tppi(i,ip):=nlpi;
                              end if;
                              lpi(nlpi):=(j,jp);
                              if tnpi(i,ip)=maxnpi then
				  	            -- Risque_Chevauchement:=true;
								TnpiIIp:=Maxnpi;
							    -- goto finBoucleIp;
							  end if;
						      goto finBoucleJp;
                            else
                              TnpiIIp:=TnpiIIp+1;
							end if;
                          end if;
                  end case;
              end case;
            end if;-- ambi
           end loop; -- sur les mots de j
           << finBoucleJp >>
           null;
          end loop; -- sur jp
        end loop; -- sur jc
		if TnpiIIp>TnpiMax then
		  TnpiMax:=TnpiIIp;
        end if;
	    << finBoucleIp >>
        null;
       end loop; -- sur les mots de i
      end loop; -- sur ip
    end if; -- tnci(i)>0
  end loop; -- sur i
end DET_PI;

--==========================================================================  

end DETPOS;

