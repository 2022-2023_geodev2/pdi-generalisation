With GEOMETRIE; Use GEOMETRIE;
With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
with math_int_basic; use math_int_basic; 
with gb;
with manipboite; use Manipboite;
with dialog1;

package body NoRou is

------------------------------------------------------------------------
--RCH_ROUTE_D-----------------------------------------------------------
--Recherche sur le noeud droite de l'arc si la route continue-----------
--Appele par:-----------------------------------------------------------
--		RECHERCHE_ROUTE-----------------------------------------
--		RCH_ROUTE_D    -----------------------------------------
------------------------------------------------------------------------
procedure RCH_ROUTE_D(gr : graphe_type;
                      nd : in out integer;
		   		      n_arc : integer;
                      att_gr : natural;
				      T_AN : in out TARC_NOM; 
				      T_ARCS : in out TARCS;
				      ttroncon : integer;
				      crst : in out integer) is

i : integer:=1;
prolong : boolean:=false;
crs : integer:=gr_noeud(gr,nd).nombre_arc;
t_arc : liens_array_type(1..crs);
muet : string(1..1);	
signe : integer;
type_arc : arc_type;

begin
  t_Arc:=gr_arcs_du_noeud(gr,nd);
  i:=1;
  while (i<=crs) and not(prolong) loop        
    if t_arc(i)>=0 then
	  signe:=1;
    else 
	  signe:=-1;
    end if;
   	t_arc(i):=abs(t_arc(i));

   	if (T_AN(t_arc(i)).nom=T_AN(n_arc).nom) and (T_AN(t_arc(i)).code=T_AN(n_arc).code) and (n_arc/=t_arc(i)) then
	  type_arc:=gr_arc(gr,t_arc(i));

      if (Att_gr*type_arc.Att_graph/=0) or (Att_gr+type_arc.Att_graph=0) then
      
        if type_arc.noeud_ini=nd then
          nd:=type_arc.noeud_fin;
        else
          nd:=type_arc.noeud_ini;
        end if;
        if T_AN(t_arc(i)).marque then
			null;
      else -- si il existe deja on arrete la prolong
     	  T_ARCS(crst).n_arc:=signe*t_arc(i);
          T_ARCS(crst).N_TRONCON:=ttroncon;
          T_AN(t_arc(i)).marque:=true;
          crst:=crst+1;
     	  RCH_ROUTE_D(gr,nd,t_arc(i),att_gr,T_AN,T_ARCS,ttroncon,crst); 
     		--prolong permet de ne trouver qu'un seul arc qui prolonge
    	  prolong:=true;   
        end if;
      end if;
   	end if;
   	i:=i+1;
  end loop;
end RCH_ROUTE_D;

------------------------------------------------------------------------
--RCH_ROUTE_G-----------------------------------------------------------
--Recherche sur le noeud gauche de l'arc si la route continue-----------
--Appele par:-----------------------------------------------------------
--		RECHERCHE_ROUTE-----------------------------------------
--		RCH_ROUTE_G  -----------------------------------------
------------------------------------------------------------------------
procedure RCH_ROUTE_G(gr : graphe_type;
				      nd : in out integer;
				      n_arc : integer;
					  att_gr : natural;
				      T_AN : in out TARC_NOM;
				      T_ARCS : in out TARCS;
				      ttroncon : integer;
				      crst : in out integer) is

i : integer:=1;
prolong : boolean:=false;
crs : integer:=gr_noeud(gr,nd).nombre_arc;
t_arc : liens_array_type(1..crs);
muet : STRING(1..1); 
signe : integer;
type_arc : arc_type;

begin

  t_arc:=gr_arcs_du_noeud(gr,nd);
  i:=1;

  while (i<=crs) and not(prolong) loop

 	if t_arc(i)>=0 then
      signe:=1;
    else
      signe:=-1;
    end if;

    t_arc(i):=abs(t_arc(i));

    if (T_AN(t_ARC(i)).nom=T_AN(N_arc).nom) and (T_AN(t_ARC(i)).code=T_AN(N_arc).code) and (n_arc/=t_arc(i)) then
      type_arc:=gr_arc(gr,t_arc(i));

      if (Att_gr*type_arc.Att_graph/=0) or (Att_gr+type_arc.Att_graph=0) then

        if type_Arc.noeud_ini=nd then
          nd:=type_arc.noeud_fin;
        else
          nd:=type_arc.noeud_ini;
        end if;

        if T_AN(t_arc(i)).marque then

	null;
   else-- si il existe deja on arrete la prolong

          T_AN(t_arc(i)).marque:=true;
          RCH_ROUTE_G(gr,nd,t_arc(i),att_gr,T_AN,T_ARCS,ttroncon,crst); 
          T_ARCS(crst).n_arc:=(-1)*signe*t_arc(i);
          T_ARCS(crst).n_troncon:=ttroncon;
          crst:=crst+1;

------------------------------------------------------------------------
--prolong permet de ne trouver qu'un seul arc qui prolonge--------------
          prolong:=true;                   
        end if;
      end if;
    end if;
    i:=i+1;
  end loop;
end RCH_ROUTE_G;

------------------------------------------------------------------------
--RECHERCHE_ROUTE-------------------------------------------------------
--recherche la route qui passe par l'arc e.arc--------------------------
--de nom e.nom----------------------------------------------------------
------------------------------------------------------------------------
procedure RECHERCHE_ROUTE(n_arc : integer;
				          gr : graphe_type;        
				          T_AN : in out TARC_NOM; 
				          T_ARCS : in out TARCS;
				          ttroncon : integer;
				          crst : in out integer ) is

n_ini,n_fin : integer;
type_arc : Arc_type;

begin

  type_Arc:=gr_arc(gr,n_arc);
  n_ini:=Type_Arc.noeud_ini;
  n_fin:=Type_Arc.noeud_fin;
  T_AN(n_arc).marque:=true;      
  RCH_ROUTE_G(gr,n_ini,n_Arc,type_arc.att_graph,T_AN,T_ARCS,ttroncon,crst);
  T_ARCS(crst).n_arc:=n_arc;
  T_ARCS(crst).n_troncon:=ttroncon;
  crst:=crst+1;
  RCH_ROUTE_D(gr,n_fin,n_arc,type_arc.att_graph,T_AN,T_ARCS,ttroncon,crst);
end RECHERCHE_ROUTE; 


------------------------------------------------------------------------
--R_R
--Cette procedure detecte dans NOM_ROUTE.DAT les routes
--et range dans N_ROUTE_R.DAT par nom de route 
--classe par ordre d'apparition dans ex901.dat
--separe chaque route par un arc -1
------------------------------------------------------------------------
procedure R_R(graphe : graphe_type;
		      T_AN : in out TARC_NOM;
			  Nb_Arcs : in integer;
		      T_RTES : out TROUTES;
		      T_ARCS : in out TARCS;
			  Crs_tt : out integer;
			  Coheroute : out boolean) is

use type gb.bool;
	
inout_graph	: graphe_mode_type;
c_tab_route	: integer :=1;
K : integer:=0;
i : string(1..1);
index : integer;
crs_a : integer:=1;
A_placer : boolean:=true;
NomCote : string(1..NBRE_CAR_NOM):=(others=>' ');
Cote,A,ModA,cote_mod_modA,A_mod_modA : float;
L : integer;
LeMode_P : typcateg;

-- partie entiere
function E(x : in float) return float is
begin
  if float(integer(x))<=x then
    return(float(integer(x)));
  else
    return(float(integer(x)-1));
  end if;
end E;

begin
	
  Coheroute:=true;
  crs_tt:=0;
  for i in 1..Nb_Arcs loop

    if ((t_AN(I).mode_p=Axe or t_AN(I).mode_p=Desaxe or t_AN(I).mode_p=Droit or t_AN(I).mode_p=Decale) and gb.checked(dialog1.NumRouCheckBox)=gb.true)
	or (t_AN(I).mode_p=courbe and gb.checked(dialog1.CotesCourbesCheckBox)=gb.true)
    or ((t_AN(I).mode_p=CAxe or t_AN(I).mode_p=CDesaxe) and gb.checked(dialog1.EcriDispoCheckBox)=gb.true) then
      a_placer:=True;
    else
	  A_Placer:=false;
    end if;

	if A_Placer=true then
      for J in 1..t_AN(I).ncar(1) loop
		
        if (t_AN(I).Nb_Mots/=1) or (not((t_AN(I).Nom(1)(J) in '0'..'9') or (T_AN(I).Nom(1)(J)=',') or (t_AN(I).Nom(1)(J)='.'))) then
          exit;
        end if;
        if j=T_AN(I).ncar(1) then
	  	
	      for k in 1..t_AN(I).ncar(1) loop
	        if t_AN(I).Nom(1)(k)/=',' then
 		  	  NomCote(k):=t_AN(I).Nom(1)(k);
            else
 			  NomCote(k):='.';
            end if;
          end loop;

          Cote:=float'value(NomCote(1..t_AN(I).ncar(1)));
          a_placer:=false;

          for K in 1..NbCotes loop
	        A:=float'value(TabCotes(K).Nom1(1..TabCotes(K).Ncar1));
            ModA:=float'value(TabCotes(K).Nom2(1..TabCotes(K).Ncar2));
	        if modA/=0.0 then
		      cote_mod_modA:=Cote-E(Cote/ModA)*modA;
			  A_mod_modA:=A-E(A/ModA)*modA;
		    else
              cote_mod_modA:=Cote;
			  A_mod_modA:=A;
	        end if;
            if cote_mod_modA = A_mod_modA and Cote/=0.0 then
              a_placer:=true;
			  exit;
            end if;
          end loop;

        end if;
      end loop;
    end if;

    if not(T_AN(i).marque) and not(T_AN(I).Nom(1)(1)=' ') and a_placer=true then
      crs_tt:=crs_tt+1;
      for j in 1..T_AN(i).Nb_Mots loop
        T_RTES(crs_tt).NCAR(J):=T_AN(i).nCAR(J);
        T_RTES(crs_tt).NOM(J):=T_AN(i).nOM(J);

		-- debut gestion du caract�re '%' pour kilometrages
		l:=T_RTES(crs_tt).NCAR(J);
		for K in 2..T_RTES(crs_tt).NCAR(J)-1 loop
          if T_RTES(crs_tt).NOM(J)(K)='%' then
		  	l:=k-1;
		    exit;
          end if;
        end loop;
        if L/=T_RTES(crs_tt).NCAR(J) then
		  T_RTES(crs_tt).NCAR(J):=L;
		  T_RTES(crs_tt).kilo:=true;
        end if;
		-- fin gestion du caract�re '%' pour kilometrages

      end loop;
      T_RTES(crs_tt).Nb_Mots:=T_AN(i).Nb_Mots;
      T_RTES(crs_tt).Code:=T_AN(i).Code;
      T_RTES(crs_tt).mode_p:=T_AN(i).mode_p;
	  T_RTES(crs_tt).PT_DEB:=crs_a;
      RECHERCHE_ROUTE(i,graphe,T_AN,T_ARCS,crs_tt,crs_a);
	  T_RTES(crs_tt).PT_FIN:=crs_a-1;

      if Coheroute=true then
        if T_rtes(crs_tt).PT_DEB/=0 and T_rtes(Crs_tt).PT_FIN/=T_rtes(crs_tt).PT_DEB then
          LeMODE_P:=T_AN(abs(T_ARCS(T_rtes(crs_tt).PT_DEB).N_ARC)).MODE_P;
	      for j in T_rtes(Crs_tt).PT_DEB+1..T_rtes(Crs_tt).PT_FIN loop
          -- if T_AN(abs(T_ARCS(J).N_ARC)).CODE/=LeCODE or
            if T_AN(abs(T_ARCS(J).N_ARC)).MODE_P/=LeMODE_P then
 	          Coheroute:=false;
              exit;
            end if;
          end loop;
        end if;
      end if;

    end if;

  end loop;
end R_R;          

--***********************************************************************
--NVEL_ARC.NORM_ANGLE
--Renvoie n'importe quel angle def. dans R, en un angle defini entre O et PI.
--**************************************************************************
function NORM_ANGLE(ang : float ) return float is
a: float;		
begin
	a:=ang;
        --met l'angle entre -PI et PI----------------------
	while a<-PI loop
	  a:=a+2.0*PI;
	end loop;
	while a>PI loop
	  a:=a-2.0*PI;
	end loop;
        --renvoie la valeur entre 0 et PI-------------------
	return abs(a);
end NORM_ANGLE;

FUNCTION TAILLE(graphe : graphe_Type; arc : integer) return float is 
Tab : point_liste_type (1..2*NBRE_PTS_D_ARC);
Crs : integer;

begin
  gr_points_d_arc(graphe,abs(arc),Tab,crs);
  return Taille_ligne(tab,crs); 
end TAILLE;

FUNCTION DENSITE_P(graphe : graphe_Type;
                   arc1 : integer;
                   arc2 : integer) return integer is 

nini,nfin,ninterm : integer;

begin

 noeud_commun(graphe,arc1,arc2,nini,nfin);
 if nini/=0 then
    ninterm:=nini;
 else
    ninterm:=nfin;
 end if;

 if gr_noeud(graphe, ninterm).Nombre_arc>2 then
   return 1;
 else
   return 0;
 end if;

end DENSITE_P;

FUNCTION SEGMENT1_LIN(graphe : graphe_type; arc : integer; resolution : in positive) return point_type is

TAB_ARC		: point_liste_type (1..NBRE_PTS_D_ARC);
TAB_ARC2	: point_liste_type (1..NBRE_PTS_D_ARC);
c_arc		: integer;
TAB_LIN		: point_liste_type(1..NBRE_PTS_D_ARC);
c_lin		: positive;
resul		: point_type;

begin
  gr_points_d_arc(graphe,abs(arc),tab_arc2,c_arc);
  if arc<0 then 
    for i in 1..c_arc loop
	  tab_arc(i):=tab_arc2(c_arc-i+1);
    end loop;
  else
    tab_arc:=tab_arc2;
  end if;
  SEG_MC(tab_arc,c_arc,TAB_LIN,c_lin,seuil_s*float(resolution));
  resul.coor_x:=tab_lin(2).coor_x-tab_lin(1).coor_x;
  resul.coor_y:=tab_lin(2).coor_y-tab_lin(1).coor_y;
  return resul;
end;

procedure POINTS(T_MT : MT;
                 T_TRS : TTRONCON;
                 T_ARCS : TARCS;
                 graphe : graphe_type;
                 Tab : out point_liste_type;
                 crs : in out integer) is

  Tab_arc	: point_liste_type(1..nbre_pts_d_arc);
  c_arc		: integer;
begin
    crs:=0;
      for m in T_MT.T_Deb..T_MT.T_fin loop
        for n in t_TRS(m).a_deb..t_Trs(m).a_fin loop
	  gr_points_d_arc(graphe,abs(T_ARCS(n).N_ARC),Tab_arc,c_arc);
          if T_ARCS(n).N_ARC>0 then
            for k in 1..c_arc loop
              crs:=crs+1;
	      Tab(crs):=Tab_arc(k);
            end loop;
          else
            for k in reverse 1..c_arc loop
              crs:=crs+1;
	      Tab(crs):=Tab_arc(k);
            end loop;
          end if;
        end loop;
      end loop;
end POINTS;

--*****************************************************************************
--C_ANGLE
--calcule l'angle avec l'horizontal d'un vecteur (2 points)
--*****************************************************************************
function C_ANGLE(p1, p2 : point_type) return float is

ang	:float;
signeY:integer;

begin

  signey:=p2.coor_y-p1.coor_y;
  if p1/=p2 then
  --renvoie dans [0;PI]
    ang:=arcCos(float(p2.coor_x-p1.coor_x)
	/norme(float(p1.coor_x),float(p1.coor_y),
 	 float(p2.coor_x),float(p2.coor_y)));
  --renvoie dans [-PI;PI]
    if signey<0 then
      return ang;
    else
      return -ang;
    end if;
  else 
    return 0.0;
  end if;
end C_ANGLE;

function TAILLE_STRING(ST : string) return integer is 

  taille	 :integer:=0;

begin

   for i in 1..ST'length  loop
   if  (st(i)>='A') and (st(i)<='Z') then
	taille:=taille+1;
   end if;
   if  (st(i)>='0') and (st(i)<='9') then
	taille:=taille+1;
   end if;
   end loop;
--le +1 est pour l'espace entre les lettres et les chiffres.
   return taille+1;

end TAILLE_STRING;  

function C_REPERE(p1,p2 : point_type; an : float) return point_type is
begin
  return Translation(Rotation(p1,an),p2);
end;

--|X| =|p2.coor_X| + |cos(an)  sin(an)||p1.coor_x|
--|Y| =|p2.coor_Y|   |-sin(an) cos(an)||p1.coor_y|
function Translation(P1,P2 : point_type) return point_type is
begin 
  return (p1.coor_x+p2.coor_x,p1.coor_y+p2.coor_y);
end;

function ROTATION(p : point_type; an : float) return point_type is
begin    
  return (integer(float(p.coor_x)*cos(an)+float(p.coor_y)*sin(an)),
          integer(float(p.coor_y)*cos(an)-float(p.coor_x)*sin(an)));
end ROTATION;

-- Renvoie le point max en x et y d'un tableau de points:
function MAX(tableau : point_liste_type; crs : integer:=0) return point_type is

res	:point_type:=tableau(tableau'first);
c	: positive:=crs;
begin

    if crs=0 then 
      c:=tableau'length;
    else
      c:=crs;
    end if;

    for i in tableau'first..tableau'first+c-1 loop
	if tableau(i).coor_x>res.coor_x then
		res.coor_x:=tableau(i).coor_x;
	end if;	
	if tableau(i).coor_y>res.coor_y then
		res.coor_y:=tableau(i).coor_y;
	end if;	
    end loop;
    return (res.coor_x,res.coor_y);
end MAX;

-- Renvoie le point min en x et y d'un tableau de points:
function MIN(tableau : point_liste_type; crs : integer:=0) return point_type IS

  res	: point_type:=tableau(tableau'first);
  c	: Positive:=crs;
Begin
  if crs=0 then 
    c:=tableau'length;
  end if;
  for i in tableau'first..tableau'first+c-1 loop
    if tableau(i).coor_x<res.coor_x then
      res.coor_x:=tableau(i).coor_x;
    end if;	
    if tableau(i).coor_y<res.coor_y then
      res.coor_y:=tableau(i).coor_y;
    end if;	
  end loop;
  return (res.coor_x,res.coor_y);
end MIN;

--*****************************************************************************
--PROJ
--projette un point sur une droite (1 point 1 vecteur)
--*****************************************************************************         
function PROJ(p,x0 : point_type; x,y : float) return point_type is

pt : point_type;
nr : float;
a,b : float;
cosa,sina : float;

begin

  nr:=norme(x,y,0.0,0.0);
  if nr/=0.0 then 
    cosa:=x/nr;
    sina:=y/nr;
    a:=float(p.coor_x-x0.coor_x)*cosa+float(p.coor_y-x0.coor_y)*sina;
    b:=0.0;
    sina:=0.0-sina;          
    pt.coor_x:=integer(a*cosa+b*sina)+x0.coor_x;
    pt.coor_y:=integer(b*cosa-a*sina)+x0.coor_y;
  else
    pt:=p;
  end if;
  return pt;  

end PROJ;

--*****************************************************************************
--DIST_PD
--calcul de la distance d'un point a une droite (1 pt, 1 vect)
--*****************************************************************************
function DIST_PD(p,x0 : point_type; x,y : float) return float is
resul : float;
point : point_type;

begin
  point:=proj(p,x0,x,y);
  resul:=norme(float(point.coor_x),float(point.coor_y),
		float(p.coor_x),float(p.coor_y));
  return resul;
end DIST_PD;


FUNCTION NOEUD_NEGLIGEABLE(graphe : graphe_type;
                           noeud : integer;
                           arc1 : integer;
                           arc2 : integer;
                           resolution : in positive) return boolean is

leg1 : integer:=0;
leg2 : integer:=0;
T_A_N : liens_array_type(1..NBRE_MAX_N_A);
i1 : integer;
i2 : integer;
S1 : point_type;
S2 : point_type;
Si : point_type;
PLAT : boolean:=FALSE;
type_arc : Arc_type;
Nbre_arc : integer;

begin

  leg1:=gr_arc(graphe,abs(arc1)).att_graph; 
  leg2:=gr_arc(graphe,abs(arc2)).att_graph;

  -- noeud non negligeable si les symboles des 2 arcs appartiennent a des groupes distincts
  if TabLegendes(leg1).NumGroupe/=TabLegendes(leg2).NumGroupe then
    return false;
  else
	
	-- renvoie le premier vecteur de la linearisation qui part du noeud commun.
    S1:=SEGMENT1_LIN(graphe,arc1,resolution);
    S2:=SEGMENT1_LIN(graphe,arc2,resolution);

    if (abs(PI-norm_angle(C_angle(s1,s2)))<SEUIL_PLAT) then
	  PLAT:=true;
	end if;

    -- repere les 2 arcs de notre route dans le tableau des arcs du noeud
    -- et detecte si les autres arcs du noeud ont leur legende commune
    -- a la legende de l'un des 2 arcs.
	Nbre_arc:=GR_NOEUD(graphe,noeud).nombre_arc;
    t_a_n(1..Nbre_arc):=gr_arcs_du_noeud(graphe,noeud);
    for i in 1..Nbre_arc loop
      type_arc:=gr_arc(graphe,abs(t_a_n(i)));
	  if ((leg1=type_Arc.att_graph) OR (leg2=type_arc.att_graph))
	  AND (abs(t_a_n(i))/=abs(arc1) and  abs(t_a_n(i))/=abs(arc2)) THEN
        Si:=SEGMENT1_LIN(graphe,T_a_n(i),resolution);
        if not(PLAT and (abs(PI/2.0-norm_angle(C_angle(S1,Si)))<SEUIL_ORT)) then
          return false;
          exit;
	    end if;
      end if;
    end loop;
    return TRUE;
  end if;
end NOEUD_NEGLIGEABLE;


PROCEDURE DECOUPE_TRONCON(graphe : Graphe_type;
                          T_RTES : in out TROUTES;
                          T_ARCS : in out TARCS;
                          T_TRS : in out TTRONCON;
						  resolution : in positive) is
						  
Taille_s : float:=0.0;
Taille_tmp : float:=0.0;
Densite : integer:=0;
Densite_tmp : integer:=0;
C_T : integer:=1;
PT_tmp : integer;
noeud : integer;
nini,nfin,ninterm : integer;

begin

  -- boucle sur les routes
  for c_r in T_RTES'FIRST..T_RTES'LAST loop

    if T_RTES(c_r).pt_deb=0 then
  	  exit;
    end if;
    
    pt_tmp:=c_t;

    Taille_s:=Taille(graphe,T_ARCS(T_RTES(c_r).pt_deb).N_ARC);
    T_TRS(c_t).a_deb:=T_RTES(c_r).pt_deb;

    -- boucle sur les arcs de la route
    for C_A in T_RTES(c_r).pt_deb+1..T_RTES(c_r).pt_fin loop
  	
      Taille_tmp:=Taille(graphe,T_ARCS(c_a).N_ARC);
      Densite_tmp:=Densite_p(graphe,T_ARCS(c_a-1).N_ARC, T_ARCS(c_A).N_ARC);

      if ((T_RTES(c_r).Mode_p=AXE or T_RTES(c_r).Mode_p=DESAXE or T_RTES(c_r).Mode_p=DROIT or T_RTES(c_r).Mode_p=DECALE) and Taille_s+Taille_tmp>SEUIL_TAILLE*float(resolution))
      or ((T_RTES(c_r).Mode_p=AXE or T_RTES(c_r).Mode_p=DESAXE or T_RTES(c_r).Mode_p=DROIT or T_RTES(c_r).Mode_p=DECALE) and (Densite+Densite_tmp>SEUIL_DENSITE))
	  or (T_RTES(c_r).Mode_p=Courbe and Taille_s+Taille_tmp>SEUIL_TAILLE_COURBE*float(resolution))
      or ((T_RTES(c_r).Mode_p=CAXE or T_RTES(c_r).Mode_p=CDESAXE) and Taille_s+Taille_tmp>SEUIL_TAILLE_HYDRO*float(resolution)) then
      -- Cas ou un des 2 seuils est depass� : on arr�te le tron�on courant et on passe au suivant
 	    Densite:=Densite_tmp;
	    Taille_s:=Taille_tmp;
	    T_TRS(c_t).a_fin:=c_a-1;
	    c_t:=c_t+1;
        T_TRS(c_t).a_deb:=c_a;
      else
        noeud_commun(graphe,T_ARCS(c_a-1).N_ARC,T_ARCS(C_A).N_ARC,nini,nfin);
        if nini/=0 then
          ninterm:=nini;
        else
          ninterm:=nfin;
        end if;

        if (T_RTES(c_r).Mode_p=AXE or T_RTES(c_r).Mode_p=DESAXE or T_RTES(c_r).Mode_p=DROIT or T_RTES(c_r).Mode_p=DECALE) then
	      if NOEUD_NEGLIGEABLE(graphe,ninterm,T_ARCS(c_a-1).N_arc,T_ARCS(c_a).N_arc,resolution) then
            Densite:=Densite+Densite_tmp;
            Taille_s:=Taille_s+Taille_tmp;
          else
	        Densite:=Densite_tmp;
	        Taille_s:=Taille_tmp;
	        T_TRS(c_t).a_fin:=c_a-1;
	        c_t:=c_t+1;
            T_TRS(c_t).a_deb:=c_a;
          end if;
        else
          Densite:=Densite+Densite_tmp;
          Taille_s:=Taille_s+Taille_tmp;
        end if;

      end if;
    end loop;

    T_TRS(c_t).a_fin:=T_RTES(c_r).pt_fin;
    T_RTES(c_r).pt_Deb:=pt_tmp;
    T_RTES(c_r).pt_fin:=c_t;

    c_t:=c_t+1;
  end loop;

end DECOUPE_TRONCON;


--******************************************************************************
--MC
--cacule la pente de la droite des MC pour les points de tab
--passant par le premier point de tab
--******************************************************************************
procedure MC(tab : point_liste_type; curs : integer; a,b : out float) is

g,h,sumx,sumy,sumxy : float;
k : integer;

begin

  sumx:=0.0;
  sumy:=0.0;
  sumxy:=0.0;
  for k in 1..curs loop
    sumx:=sumx+"**"(float(tab(k).coor_x-tab(1).coor_x),2); 
    sumy:=sumy+"**"(float(tab(k).coor_y-tab(1).coor_y),2); 
    sumxy:=sumxy+float(tab(k).coor_x-tab(1).coor_x)*
			float(tab(k).coor_y-tab(1).coor_y);
  end loop;
  g:=sumx-sumy;
  h:=sqrt(g*g+4.0*sumxy*sumxy);
  h:=g-h;
  if (sumx=0.0) then
    a:=1.0;
    b:=0.0;
  else if (sumy=0.0) or (sumxy=0.0) then
      a:=0.0;
      b:=1.0;
    else
      h:=h/(2.0*sumxy);
      a:=h/sqrt(h*h+1.0);
      b:=1.0/sqrt(h*h+1.0);
    end if;
  end if;

end MC;


procedure MC2(tab : point_liste_type; curs1 : integer; curs2 : integer; a,b : out float) is

g,h,sumx,sumy,sumxy : float;
k : integer;

begin

  sumx:=0.0;
  sumy:=0.0;
  sumxy:=0.0;
  for k in Curs1..curs2 loop
    sumx:=sumx+"**"(float(tab(k).coor_x-tab(Curs1).coor_x),2); 
    sumy:=sumy+"**"(float(tab(k).coor_y-tab(Curs1).coor_y),2); 
    sumxy:=sumxy+float(tab(k).coor_x-tab(Curs1).coor_x)*
			float(tab(k).coor_y-tab(Curs1).coor_y);
  end loop;
  g:=sumx-sumy;
  h:=sqrt(g*g+4.0*sumxy*sumxy);
  h:=g-h;
  if (sumx=0.0) then
    a:=1.0;
    b:=0.0;
  else if (sumy=0.0) or (sumxy=0.0) then
      a:=0.0;
      b:=1.0;
    else
      h:=h/(2.0*sumxy);
      a:=h/sqrt(h*h+1.0);
      b:=1.0/sqrt(h*h+1.0);
    end if;
  end if;

end MC2;



--******************************************************************************
--SEG_MC
--renvoie un tableau qui contient les points qui linearise
--un arc par les MC contraints
--******************************************************************************
procedure SEG_MC(arc_t : point_liste_type;
                 curseur : integer;
                 seg_t : in out point_liste_type;
                 curseur_f : in out integer;
                 seuil : float) is
         
seuil_nd : boolean:=TRUE;
a,b,a2,b2 : float:=0.0;
i : integer:=1;
ap : integer:=1;
fp : integer:=2;
tt_p : point_type;
tt : point_liste_type(1..curseur);
c_tt : integer:=2;
distance : float:=0.0;

begin

  ap:=1;
  tt(1):=arc_t(ap);
  tt(2):=arc_t(2);
  curseur_f:=1;
  seg_t(1).coor_x:=arc_t(1).coor_x;  
  seg_t(1).coor_y:=arc_t(1).coor_y;

  while (fp/=curseur+1) loop
    tt(c_tt).coor_x:=arc_t(fp).coor_x;
    tt(c_tt).coor_y:=arc_t(fp).coor_y;
    a2:=a;
    b2:=b;
    MC(tt,c_tt,a,b);
    i:=1;

    while seuil_nd and i/=c_tt+1 loop
      distance:=dist_pd(tt(i),tt(1),b,-a);

      if distance>seuil then
        seuil_nd:=false;
      else
        i:=i+1;
      end if;
    end loop;

    if seuil_nd then
      fp:=fp+1;
      c_tt:=c_tt+1;
    else
      c_tt:=2;
      curseur_f:=curseur_f+1;
      tt(2).coor_x:=arc_t(fp-1).coor_x;
      tt(2).coor_y:=arc_t(fp-1).coor_y;
      tt_p:=Projection_droite(tt(2),tt(1),b2,-a2);
      seg_t(curseur_f).coor_x:=tt_p.coor_x;
      seg_t(curseur_f).coor_y:=tt_p.coor_y;
      ap:=curseur_f;
      tt(1).coor_x:=seg_t(curseur_f).coor_x;
      tt(1).coor_y:=seg_t(curseur_f).coor_y;
      seuil_nd:=true;
    end if;             
  end loop;
  curseur_f:=curseur_f+1;

  if c_tt=3 then 
    seg_t(curseur_f).coor_x:=arc_t(fp-1).coor_x;
    seg_t(curseur_f) .coor_y:=arc_t(fp-1).coor_y;
  else
    tt_p:=Projection_droite(arc_t(fp-1),tt(1),b,-a);
    seg_t(curseur_f).coor_x:=tt_p.coor_x;
    seg_t(curseur_f).coor_y:=tt_p.coor_y;
  end if;  

end SEG_MC;

--******************************************************************************
--MCL
--cacule la pente de la droite des MC pour les points de tab
--passant par le premier point de tab
--******************************************************************************
procedure MCL(tab : point_liste_type; curs : integer; a,b : out float) is

  g,h		: float;
  sumx,
  sumy,
  sumxy 	: float;
  k		: integer;
  moyen		: point_type;

begin

  sumx:=0.0;
  sumy:=0.0;
  sumxy:=0.0;

  moyen:=POINT_MOYEN(tab,curs);

  for k in 1..curs loop
    sumx:=sumx+"**"(float(tab(k).coor_x-moyen.coor_x),2); 
    sumy:=sumy+"**"(float(tab(k).coor_y-moyen.coor_y),2); 
    sumxy:=sumxy+float(tab(k).coor_x-moyen.coor_x)*
			float(tab(k).coor_y-moyen.coor_y);
  end loop;
  sumxy:=sumxy;

  if (sumx=0.0)  then
    a:=1.0;
    b:=0.0;
  else if sumy=0.0 then                             
         a:=0.0;
         b:=1.0;
       else
         a:=sumxy/NORME(sumx,sumxy,0.0,0.0);
         b:=-sumx/NORME(sumx,sumxy,0.0,0.0); 
       end if;
  end if;

end MCL;

procedure MCL2(tab : point_liste_type; curs1,curs2 : integer; a,b : out float) is

  g,h		: float;
  sumx,
  sumy,
  sumxy 	: float;
  k		: integer;
  moyen		: point_type;

begin

  sumx:=0.0;
  sumy:=0.0;
  sumxy:=0.0;

  moyen:=POINT_MOYEN2(tab,curs1,Curs2);

  for k in Curs1..curs2 loop
    sumx:=sumx+"**"(float(tab(k).coor_x-moyen.coor_x),2); 
    sumy:=sumy+"**"(float(tab(k).coor_y-moyen.coor_y),2); 
    sumxy:=sumxy+float(tab(k).coor_x-moyen.coor_x)*
			float(tab(k).coor_y-moyen.coor_y);
  end loop;
  sumxy:=sumxy;

  if (sumx=0.0)  then
    a:=1.0;
    b:=0.0;
  else if sumy=0.0 then                             
         a:=0.0;
         b:=1.0;
       else
         a:=sumxy/NORME(sumx,sumxy,0.0,0.0);
         b:=-sumx/NORME(sumx,sumxy,0.0,0.0); 
       end if;
  end if;

end MCL2;


--******************************************************************************
--SEG_MCL
--renvoie un tableau qui contient les points qui linearise
--un arc par les MC 
--******************************************************************************                                            
procedure SEG_MCL(arc_t : point_liste_type;
                  curseur : integer;
				  seg_t : in out plt_opt;
                  curseur_f : in out integer;
                  seuil : float) is

  seuil_nd	:boolean:=TRUE;
  a,
  b,
  a2,
  b2		:float:=0.0;
  i		:integer:=1;
  ap		:integer:=1;
  fp		:integer:=2;
  tt_p		:point_type;
  tt		:point_liste_type(1..2*curseur);
  c_tt		:integer:=2;
  distance	:float:=0.0;

begin

  ap:=1;
  tt(1):=arc_t(ap);
  tt(2):=arc_t(2);
  curseur_f:=0;
  while (fp/=curseur+1) loop
    tt(c_tt).coor_x:=arc_t(fp).coor_x;
    tt(c_tt).coor_y:=arc_t(fp).coor_y;
    a2:=a;
    b2:=b;
    MCL(tt,c_tt,a,b);
    i:=1;
    while seuil_nd and i/=c_tt+1 loop
      distance:=DIST_PD(tt(i),POINT_MOYEN(tt,c_tt),b,-a);
      if distance>seuil then
        seuil_nd:=false;
      else
        i:=i+1;       
      end if;
    end loop;
    if seuil_nd then
      c_tt:=c_tt+1;
      fp:=fp+1;
    else

--calcul du premier point du segment-----------------------
      curseur_f:=curseur_f+1;
      tt_p:=Projection_droite(tt(1),POINT_MOYEN(tt,(c_tt-1)),b2,-a2);
      seg_t(curseur_f).coor_x:=tt_p.coor_x;
      seg_t(curseur_f).coor_y:=tt_p.coor_y;
      seg_t(curseur_f).c_pt:=ap;  

--calcul du deuxieme point du segment----------------------
      curseur_f:=curseur_f+1;
      tt_p:=Projection_droite(tt(c_tt-1),POINT_MOYEN(tt,(c_tt-1)),b2,-a2);
      seg_t(curseur_f).coor_x:=tt_p.coor_x;
      seg_t(curseur_f).coor_y:=tt_p.coor_y;
      seg_t(curseur_f).c_pt:=fp-1;  

--reinitialisation-----------------------------------------
      ap:=fp-1;

      tt(1).coor_x:=arc_t(ap).coor_x;
      tt(1).coor_y:=arc_t(ap).coor_y;
      seuil_nd:=true;
      c_tt:=2;

    end if;      
  end loop;
--gestion du dernier point---------------------------------
  if c_tt=3 then 
    curseur_f:=curseur_f+1;
    seg_t(curseur_f).coor_x:=tt(1).coor_x;
    seg_t(curseur_f).coor_y:=tt(1).coor_y;
    seg_t(curseur_f).c_pt:=ap;  

    curseur_f:=curseur_f+1;
    seg_t(curseur_f).coor_x:=tt(2).coor_x;
    seg_t(curseur_f).coor_y:=tt(2).coor_y;
    seg_t(curseur_f).c_pt:=ap+1;  
  else
      curseur_f:=curseur_f+1;
      tt_p:=Projection_droite(tt(1),POINT_MOYEN(tt,(c_tt-1)),b,-a);
      seg_t(curseur_f).coor_x:=tt_p.coor_x;
      seg_t(curseur_f).coor_y:=tt_p.coor_y;
      seg_t(curseur_f).c_pt:=ap;  

      curseur_f:=curseur_f+1;
      tt_p:=Projection_droite(tt(c_tt-1),POINT_MOYEN(tt,(c_tt-1)),b,-a);
      seg_t(curseur_f).coor_x:=tt_p.coor_x;
      seg_t(curseur_f).coor_y:=tt_p.coor_y;
      seg_t(curseur_f).c_pt:=fp-1;  
  end if;  

end SEG_MCL;

--******************************************************************************
--POINT_MOYEN
--Calcule le point moyen d'un ensemble de CRS points defini 
--dans un tableau TB
--******************************************************************************
function POINT_MOYEN(tb : point_liste_type; crs : integer) return point_type is
  myn	: point_type;
  k	: integer;
  mynR	: point_type_reel;

begin

  -- debut modif 14.04.2005
  -- myn.coor_x:=0;
  -- myn.coor_y:=0;
  -- for k in 1..crs loop
    -- myn.coor_x:=myn.coor_x+tb(k).coor_x;
    -- myn.coor_y:=myn.coor_y+tb(k).coor_y;
  -- end loop;
  -- myn.coor_x:=myn.coor_x/crs;
  -- myn.coor_y:=myn.coor_y/crs;

  mynR.coor_x:=0.0;
  mynR.coor_y:=0.0;
  for k in 1..crs loop
    mynR.coor_x:=mynR.coor_x+float(tb(k).coor_x)/float(Crs);
    mynR.coor_y:=mynR.coor_y+float(tb(k).coor_y)/float(Crs);
  end loop;
  Myn.Coor_X:=integer(MynR.coor_X);
  Myn.Coor_Y:=integer(MynR.coor_Y);
  -- fin modif 14.04.2005

  return myn;

end  POINT_MOYEN;

function POINT_MOYEN2(tb : point_liste_type; crs1,Crs2 : integer) return point_type is
  myn	: point_type;
  k	: integer;
  DCrs : integer:=(crs2-Crs1+1);
  MynR  : Point_type_reel;

begin

  -- debut modif 14.04.2005
  -- myn.coor_x:=0;
  -- myn.coor_y:=0;
  -- for k in Crs1..crs2 loop
    -- myn.coor_x:=myn.coor_x+tb(k).coor_x;
    -- myn.coor_y:=myn.coor_y+tb(k).coor_y;
  -- end loop;
  -- myn.coor_x:=myn.coor_x/(crs2-Crs1+1);
  -- myn.coor_y:=myn.coor_y/(crs2-Crs1+1);

  mynR.coor_x:=0.0;
  mynR.coor_y:=0.0;
  for k in Crs1..crs2 loop
    mynR.coor_x:=mynR.coor_x+float(tb(k).coor_x)/float(DCrs);
    mynR.coor_y:=mynR.coor_y+float(tb(k).coor_y)/float(DCrs);
  end loop;
  Myn.Coor_X:=integer(MynR.coor_X);
  Myn.Coor_Y:=integer(MynR.coor_Y);
  -- fin modif 14.04.2005

  return myn;

end POINT_MOYEN2;


end Norou;
