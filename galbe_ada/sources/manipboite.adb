With ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
with math_int_basic; use math_int_basic;
with geometrie; use geometrie;

package body manipboite is


--==========================================================================
-- Procedure qui renvoie les 4 coins correspondants d'une boite a partir 
-- du coin1, de l'angle, de l'emprise, de le hauteur:
-- --------------------------------------------------
procedure QUATRE_COINS(coin1 : in point_type;
		               angle : in float;
		               emprise, hauteur : in integer;
			           boite : out bloc_type) is

x1,x2,x3,x4,y1,y2,y3,y4:integer;

begin
    x1:=coin1.coor_x;
    y1:=coin1.coor_y;
    x2:=x1-integer(float(hauteur)*sin(angle));
    y2:=y1+integer(float(hauteur)*cos(angle));
    x3:=x2+integer(float(emprise)*cos(angle));
    y3:=y2+integer(float(emprise)*sin(angle));
    x4:=x1+integer(float(emprise)*cos(angle));
    y4:=y1+integer(float(emprise)*sin(angle));

    Boite:=((x1,y1),(x2,y2),(x3,y3),(x4,y4));
end;


-- =============================================================
-- Ajout FL 07/02/97
-- Procedure qui renvoie les 4 coins correspondants d'une boite a partir
-- du coin1 et coin2 (axe central de la boite) , et de la hauteur:
-- --------------------------------------------------
procedure QUATRE_COINS( coin1          :in point_type;
                        coin2          :in point_type;
                        angle 	       :in float;
                        hauteur        :in integer;
                        boite          :out bloc_type) is

  x1,x2,x3,x4,y1,y2,y3,y4 : integer;

begin
    x1:=coin1.coor_x + integer(float(hauteur)/2.0*sin(angle));
    y1:=coin1.coor_y - integer(float(hauteur)/2.0*cos(angle));
    x2:=coin1.coor_x - integer(float(hauteur)/2.0*sin(angle));
    y2:=coin1.coor_y + integer(float(hauteur)/2.0*cos(angle));
    x4:=coin2.coor_x + integer(float(hauteur)/2.0*sin(angle));
    y4:=coin2.coor_y - integer(float(hauteur)/2.0*cos(angle));
    x3:=coin2.coor_x - integer(float(hauteur)/2.0*sin(angle));
    y3:=coin2.coor_y + integer(float(hauteur)/2.0*cos(angle));

    Boite:=((x1,y1),(x2,y2),(x3,y3),(x4,y4));
end;

--=============================================================================
-- Function determinant si 2 boites se chevauchent:
---------------------------------------------------
function Chevauch(b1,b2:in boite_type) return boolean is
begin
  if b1.minimum.coor_x < b2.maximum.coor_x
  and b2.minimum.coor_x < b1.maximum.coor_x
  and b1.minimum.coor_y < b2.maximum.coor_y
  and b2.minimum.coor_y < b1.maximum.coor_y then
	  return true ;
  else 
	  return false; 
  end if;
end chevauch;


--=============================================================================
-- Function determinant si 2 boites obliques se chevauchent:
------------------------------------------------------------
function Chevauch_oblique(b1,b2:in Bloc_type) return boolean is
inters : boolean:=false;
Surface : Surf_type(1,4);
Boite1,Boite2 : boite_type;
Centre1,Centre2 : point_type;
rayon1,Rayon2 : float;
-- Hauteur1,Hauteur2,Longueur1,Longueur2 : float;

begin

  Boite1.minimum.coor_x:=min(min(B1.P1.Coor_x,B1.P2.Coor_x),min(B1.P3.Coor_x,B1.P4.Coor_x));
  Boite1.minimum.coor_Y:=min(min(B1.P1.Coor_Y,B1.P2.Coor_Y),min(B1.P3.Coor_Y,B1.P4.Coor_Y));
  Boite1.maximum.coor_x:=max(max(B1.P1.Coor_x,B1.P2.Coor_x),max(B1.P3.Coor_x,B1.P4.Coor_x));
  Boite1.maximum.coor_Y:=max(max(B1.P1.Coor_Y,B1.P2.Coor_Y),max(B1.P3.Coor_Y,B1.P4.Coor_Y));
  Boite2.minimum.coor_x:=min(min(B2.P1.Coor_x,B2.P2.Coor_x),min(B2.P3.Coor_x,B2.P4.Coor_x));
  Boite2.minimum.coor_Y:=min(min(B2.P1.Coor_Y,B2.P2.Coor_Y),min(B2.P3.Coor_Y,B2.P4.Coor_Y));
  Boite2.maximum.coor_x:=max(max(B2.P1.Coor_x,B2.P2.Coor_x),max(B2.P3.Coor_x,B2.P4.Coor_x));
  Boite2.maximum.coor_Y:=max(max(B2.P1.Coor_Y,B2.P2.Coor_Y),max(B2.P3.Coor_Y,B2.P4.Coor_Y));

  if chevauch(boite1,boite2)=true then

    Centre1:=(integer(float(Boite1.minimum.coor_x+Boite1.maximum.coor_x)/2.0),integer(float(Boite1.minimum.coor_Y+Boite1.maximum.coor_Y)/2.0));
    Centre2:=(integer(float(Boite2.minimum.coor_x+Boite2.maximum.coor_x)/2.0),integer(float(Boite2.minimum.coor_Y+Boite2.maximum.coor_Y)/2.0));
    Rayon1:=sqrt(distance_de_points(Centre1,B1.P1));
    Rayon2:=sqrt(distance_de_points(Centre2,B2.P1));

    if sqrt(distance_de_points(Centre1,centre2))<(rayon1+Rayon2) then
      if intersec(B1.P1,B1.P2,B2.P1,B2.P2) then return(true); end if;
      if intersec(B1.P1,B1.P2,B2.P2,B2.P3) then return(true); end if;
      if intersec(B1.P1,B1.P2,B2.P3,B2.P4) then return(true); end if;
      if intersec(B1.P1,B1.P2,B2.P4,B2.P1) then return(true); end if;
      if intersec(B1.P2,B1.P3,B2.P1,B2.P2) then return(true); end if;
      if intersec(B1.P2,B1.P3,B2.P2,B2.P3) then return(true); end if;
      if intersec(B1.P2,B1.P3,B2.P3,B2.P4) then return(true); end if;
      if intersec(B1.P2,B1.P3,B2.P4,B2.P1) then return(true); end if;
      if intersec(B1.P3,B1.P4,B2.P1,B2.P2) then return(true); end if;
      if intersec(B1.P3,B1.P4,B2.P2,B2.P3) then return(true); end if;
      if intersec(B1.P3,B1.P4,B2.P3,B2.P4) then return(true); end if;
      if intersec(B1.P3,B1.P4,B2.P4,B2.P1) then return(true); end if;
      if intersec(B1.P4,B1.P1,B2.P1,B2.P2) then return(true); end if;
      if intersec(B1.P4,B1.P1,B2.P2,B2.P3) then return(true); end if;
      if intersec(B1.P4,B1.P1,B2.P3,B2.P4) then return(true); end if;

--    test_Intersection_de_segments(B1.P1,B1.P2,B2.P1,B2.P2,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P1,B1.P2,B2.P2,B2.P3,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P1,B1.P2,B2.P3,B2.P4,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P1,B1.P2,B2.P4,B2.P1,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P2,B1.P3,B2.P1,B2.P2,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P2,B1.P3,B2.P2,B2.P3,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P2,B1.P3,B2.P3,B2.P4,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P2,B1.P3,B2.P4,B2.P1,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P3,B1.P4,B2.P1,B2.P2,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P3,B1.P4,B2.P2,B2.P3,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P3,B1.P4,B2.P3,B2.P4,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P3,B1.P4,B2.P4,B2.P1,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P4,B1.P1,B2.P1,B2.P2,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P4,B1.P1,B2.P2,B2.P3,Inters);
--    if inters=true then return(true); end if;
--    test_Intersection_de_segments(B1.P4,B1.P1,B2.P3,B2.P4,Inters);
--    if inters=true then return(true); end if;
    --  Intersection_de_lignes(B1.P4,B1.P1,B2.P4,B2.P1,C_segment_type,Inters);
    --  if inters=true then return(true); end if;


--      Hauteur1:=distance_de_points(B1.P1,B1.P2);
--      longueur1:=distance_de_points(B1.P2,B1.P3);
--      Hauteur2:=distance_de_points(B2.P1,B2.P2);
--      Longueur2:=distance_de_points(B2.P2,B2.P3);
--
--	  if Hauteur1>Hauteur2 and Longueur1>Longueur2 then
        if est_il_dans_rectangle(B2.p1,B1.P1,B1.P2,B1.P3,B1.P4)=true then
	      return(true);
	    end if;
--      end if;

--	  if Hauteur1<Hauteur2 and Longueur1<Longueur2 then
        if est_il_dans_rectangle(B1.p1,B2.P1,B2.P2,B2.P3,B2.P4)=true then
	      return(true);
	    end if;
--      end if;

    end if;

--    surface.PTS:=(B1.P1,B1.P2,B1.P3,B1.P4);
--    surface.NE:=1;
--    surface.NA(1):=4;      
--    if Est_il_dans_surface(B2.P1,Surface)=true then return(true); end if;
--
--    surface.PTS:=(B2.P1,B2.P2,B2.P3,B2.P4);
--    surface.NE:=1;
--    surface.NA(1):=4;      
--    if Est_il_dans_surface(B1.P1,Surface)=true then return(true); end if;

  end if;

  return(false);

end chevauch_oblique;


--=============================================================================
-- Fonction determinant les distances entre une boite et un point:
-----------------------------------------------------------------------
function dbpt (b: in boite_type; pt: in point_type ) return natural is
  d2 : long_float;
begin
  if pt.coor_x < b.minimum.coor_x then
    if pt.coor_y < b.minimum.coor_y then
      d2:= (long_float(b.minimum.coor_x)-long_float(pt.coor_x))**2+(long_float(b.minimum.coor_y)-long_float(pt.coor_y))**2;
      return natural(sqrt(float(d2)));
    end if;
    if pt.coor_y <= b.maximum.coor_y then
      return natural(b.minimum.coor_x-pt.coor_x);
    end if;
    d2:= (long_float(b.minimum.coor_x)-long_float(pt.coor_x))**2+(long_float(b.maximum.coor_y)-long_float(pt.coor_y))**2;
    return natural(sqrt(float(d2)));
  end if;
    
  if pt.coor_x <= b.maximum.coor_x then
    if pt.coor_y < b.minimum.coor_y then
      return natural(b.minimum.coor_y-pt.coor_y);
    end if;
    if pt.coor_y <= b.maximum.coor_y then return 0;
    end if;
    return natural(pt.coor_y-b.maximum.coor_y);
  end if;
    
  if pt.coor_y < b.minimum.coor_y then
    d2:= (long_float(b.maximum.coor_x)-long_float(pt.coor_x))**2+(long_float(b.minimum.coor_y)-long_float(pt.coor_y))**2;
    return natural(sqrt(float(d2)));
  end if;
  if pt.coor_y <= b.maximum.coor_y then
    return natural(pt.coor_x-b.maximum.coor_x);
  end if;
  d2:= (long_float(b.maximum.coor_x)-long_float(pt.coor_x))**2+(long_float(b.maximum.coor_y)-long_float(pt.coor_y))**2;
  return natural(sqrt(float(d2)));  
end dbpt;


--=============================================================================
-- Fonction determinant la distance entre un point et un contour de points:
----------------------------------------------------------------------------
function dptcont	(p: in point_type;
                 ncont: in positive;
                 cont: in point_liste_type ) return natural is
d2min: integer:= integer'last;
d2: integer;
q: point_type;
begin
  for ip in 1..ncont loop
    q:=cont(ip);
    d2:= (p.coor_x-q.coor_x)**2 + (p.coor_y-q.coor_y)**2;
    if d2 < d2min then d2min:= d2; end if;
  end loop;
  return natural(sqrt(float(d2min)));
end dptcont;

--=============================================================================
-- Fonction determinant la distance entre une boite et un contour de points:
----------------------------------------------------------------------------
function dbcont	(b: in boite_type;
                 ncont: in positive;
                 cont: in point_liste_type ) return natural is
dmin: natural:= natural'last;
d: natural;
begin
  for ip in 1..ncont loop
    d:= dbpt(b,cont(ip));
    if d < dmin then dmin:= d; end if;
  end loop;
  return dmin;
end dbcont;

--=============================================================================
-- Fonction determinant la distance entre une boite et une autre boite:
-----------------------------------------------------------------------
function dbb (b1: in boite_type;  b2: in boite_type ) return natural is
  dx, dy, da, db : integer;
  Das,Dbs : float;
begin
  da:=b2.minimum.coor_x-b1.maximum.coor_x;
  db:=b2.maximum.coor_x-b1.minimum.coor_x;
  if Da/=0 then
    das:=float(da)/float(abs(da));
  else	
	Das:=0.0;
  end if;
  if Db/=0 then
    dbs:=float(db)/float(abs(db));
  else
    Dbs:=0.0;
  end if;
  if Das*Dbs<=0.0 then
    dx:=0;
  else
    dx:=min(abs(da), abs(db));
  end if;
  da:=b2.minimum.coor_y-b1.maximum.coor_y;
  db:=b2.maximum.coor_y-b1.minimum.coor_y;
  if Da/=0 then
    das:=float(da)/float(abs(da));
  else	
	Das:=0.0;
  end if;
  if Db/=0 then
    dbs:=float(db)/float(abs(db));
  else
    Dbs:=0.0;
  end if;
  if Das*Dbs<=0.0 then
    dy:=0;
    return natural(dx);
  else
    dy:=min(abs(da), abs(db));
    return natural(sqrt(float(dx**2+dy**2)));
  end if;
end dbb;


--=============================================================================
-- Fonction determinant si une boite d�borde d'un cadre pench�:
---------------------------------------------------------------
function deborde(b: in boite_type; pne,pno,pso,pse: point_type) return boolean is
  function ext(p: in point_type) return boolean is
  begin -- N.b.: Le cadre ne doit pas etre horizontal ni vertical.
    --Gestion des cas abh�rents qui feraient planter
    if (pne.coor_y=pse.coor_y) or (pne.coor_x=pno.coor_x) or
       (pno.coor_y=pso.coor_y) or (pse.coor_x=pso.coor_x)  then
        return false;
    end if;
	  if (pne.coor_x-pse.coor_x)*(p.coor_y-pse.coor_y)/(pne.coor_y-pse.coor_y)+pse.coor_x < p.coor_x then
		return true;
	  end if;		
	  if (pne.coor_y-pno.coor_y)*(p.coor_x-pno.coor_x)/(pne.coor_x-pno.coor_x)+pno.coor_y < p.coor_y then
		return true;
	  end if;		
	  if p.coor_x < (pno.coor_x-pso.coor_x)*(p.coor_y-pso.coor_y)/(pno.coor_y-pso.coor_y)+pso.coor_x then
		return true;
	  end if;		
	  if p.coor_y < (pse.coor_y-pso.coor_y)*(p.coor_x-pso.coor_x)/(pse.coor_x-pso.coor_x)+pso.coor_y then
		return true;
	  end if;		
	  return false;
	end ext;

  begin
    if ext(b.maximum)
    or ext((b.minimum.coor_x,b.maximum.coor_y))
    or ext(b.minimum)
    or ext((b.maximum.coor_x,b.minimum.coor_y)) then 
      return true ;
    else 
      return false;
    end if;
  end deborde;

--=============================================================================
-- Fonction dilatant une boite:
-------------------------------
function dilatee (b: in boite_type;
                  dila_x, dila_y: in natural) return boite_type is
begin
  return ( (b.minimum.coor_x-dila_x, b.minimum.coor_y-dila_y),	
		       (b.maximum.coor_x+dila_x, b.maximum.coor_y+dila_y) );	
end dilatee; 

--=============================================================================
-- Fonction dilatant une boite oblique:
---------------------------------------
function dilatee_oblique (b: in bloc_type; Angle : in float; dila_x, dila_y: in natural) return bloc_type is
BlocDilat : bloc_type;
V1,V2 : point_type;
begin
  V1:=(integer(-float(Dila_X)*cos(angle)+float(Dila_Y)*Sin(angle)),integer(-float(Dila_X)*sin(angle)-float(Dila_Y)*cos(angle)));
  V2:=(integer(-float(Dila_X)*cos(angle)-float(Dila_Y)*Sin(angle)),integer(-float(Dila_X)*sin(angle)+float(Dila_Y)*cos(angle)));
  BlocDilat.p1:=B.P1+V1;
  BlocDilat.p2:=B.P2+V2;
  BlocDilat.p3:=B.P3-V1;
  BlocDilat.p4:=B.P4-V2;
  return(BlocDilat);
end dilatee_oblique; 


--=========================================================================
-- Fonction d�tectant si deux ouverts ]a,b[ et ]c,d[ s'intersectent:
--------------------------------------------------------------------
Function Intersec(a,b,c,d:in point_type) return boolean is
  t1n,t2n,td: integer;
  begin
	t1n:=(c.coor_x-a.coor_x)*(c.coor_y-d.coor_y)
		-(c.coor_x-d.coor_x)*(c.coor_y-a.coor_y);
	td:= (b.coor_x-a.coor_x)*(c.coor_y-d.coor_y)
		-(c.coor_x-d.coor_x)*(b.coor_y-a.coor_y);
	if td=0 then return false; end if;
	if td<0 and 0<t1n then return false; end if;
	if 0<td and t1n<0 then return false; end if;
	if abs(td)<abs(t1n) then return false; end if;
	t2n:=(b.coor_x-a.coor_x)*(c.coor_y-a.coor_y)
		-(c.coor_x-a.coor_x)*(b.coor_y-a.coor_y);
	if td<0 and 0<t2n then return false; end if;
	if 0<td and t2n<0 then return false; end if;
	if abs(td)<abs(t2n) then return false; end if;
	return true;
end Intersec; 

--=============================================================================
end manipboite;
