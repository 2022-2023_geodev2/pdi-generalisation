--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with inipan;  use inipan;
with detpos;  use detpos;


Package PLACT_SEQUENTIEL is

Type float_array_type is array(positive range <>) of float;

--
----==============================================================================
---- Tri a bulles des communes dans l'ordre des degres de contrainte decroissants:
----------------------------------------------------------------------------------
--procedure ordonnerc ( tbnomi  : in typtbnomi; 
--		      tico,tr : in out typtico;
--		      tdc,tdl : in float_array_type;
--		      propa   : in boolean );
--
----=============================================================================
---- Retri a bulles des communes entre ic et nc (propagation des contraintes)
---- dans l'ordre des degres de liberte croissants:
---------------------------------------------------
--procedure reordonnerc ( no      : in positive;
--			noi     : in positive; 
--			tdl     : in float_array_type;
--			tico,tr : in out typtico;
--			propa   : in boolean );
--
----=============================================================================
---- Procedure calculant les degres de contrainte des communes:
---------------------------------------------------------------
--procedure DET_DC ( tnp   : in typtnp;
--		   tnpi  : in typtnpi;
--		   tnci  : in typtnci;
--		   tbnomi: in typtbnomi;
--		   tici  : in typtici;
--		   tdc   : in out float_array_type );
--
----=============================================================================
---- Procedure calculant les degres de liberte des communes:
------------------------------------------------------------
--procedure DET_DL ( tnp   : in typtnp;
--		   tnpi  : in typtnpi;
--		   tbnomi: in typtbnomi;
--		   tdl   : in out float_array_type );
--
----=============================================================================
---- Procedure recalculant le degre de liberte d'une commune 
---- (propagation de contraintes):
------------------------------------------------------------
--procedure REDET_DL ( no    : in positive;
--		     tnp   : in typtnp;
--		     tnpi  : in typtnpi;
--		     tp    : in out typtp;
--		     tdl   : in out float_array_type );
--
----=============================================================================
---- Procedure determinant le poids total d'une position:
---------------------------------------------------------
--procedure DETPOITOT (   no    :	in positive; 
--			p     : in natural; 
--			poii  : out typpoi;
--			poitot: out typpoi;
--			tbnomi: in typtbnomi;
--			maxnpos: in integer;
--			tp    :	in typtp;
--			tnpi  :	in typtnpi;
--			tppi  :	in typtppi;
--			lpi   : in typlpi );
--
----==========================================================================
---- Procedure qui choisit la meilleure pos possible pour une commune donnee :
------------------------------------------------------------------------------
--procedure DET_MP ( no      : in positive; 
--		   tbnomi  : in out typtbnomi;
--		   tnp     : in typtnp;
--		   tp      : in out typtp;
--		   tnpi    : in typtnpi;
--		   tppi    : in typtppi;
--		   lpi	   : in typlpi;
--		   tnci    : in typtnci;
--		   tici    : in typtici;
--		   tico,tr : in out typtico;
--		   tdl     : in out float_array_type;
--		   blo     : in out boolean;
--		   propa   : in boolean );
--
----=================================
---- Procedure de retour en arriere :
----=================================
--procedure RA (  no    : in positive;
--		tnp   : in typtnp;
--		tp    : in out typtp;
--		tnpi  :	in typtnpi;
--		tppi  :	in typtppi;
--		lpi   : in typlpi;
--		tnci  : in typtnci;
--		tici  : in typtici;
--		tr    : in out typtico;
--		tbnomi: in out typtbnomi);
--

--==================================================================
procedure PLACEMENT_SEQUENTIEL (Tbnomi   : in out typtbnomi;
                                Tnp      : in typtnp;
                                tp       : in out typtp;
                                Tnpi     : in typtnpi;
                                tppi     : in typtppi;
                                lpi 	 : in typlpi;
                                Tnci     : in typtnci;
                                Tici     : in typtici);

--======================================================
end PLACT_SEQUENTIEL;
