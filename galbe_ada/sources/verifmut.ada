with INIPAN; use INIPAN;
with tiff_io; use tiff_io;
with SUBSTITUTION_IO; use SUBSTITUTION_IO;
with detpos; use detpos;
with text_io; use text_io;
with lecture; use lecture;
with gen_io; use gen_io;
with geometrie; use geometrie;
with ada.numerics.elementary_functions; use ada.numerics.elementary_functions;
with Math_int_basic; use Math_int_basic;
with win32;
with Win32.CommDlg;
with win32.crt.strings;
with win32.winuser; use win32.winuser;
with system;
with Ada.Characters.Handling; use Ada.Characters.Handling;
with gb;
with Interfaces.C;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with norou; use norou;
with dialog1;
with dialog2;

package VERIFMUT is

package int_io is new text_io.integer_io(integer); use int_io;
package flo_io is new text_io.float_io(float); use flo_io; 
  
-- poids de pixel correspondant a de l'interdit (=10)
poidspixinterdit : constant short_integer := typipoi'last;

-- tableau de valeurs d'index ayant le meme poids pour tous les codes typo
type typtabindextous is array (integer range <>) of integer;
type acces_typtabindextous is access typtabindextous;

-- tableau des identifiants de symboles representes dans la base
type typtabidsymboles is array (integer range <>) of string(1..30);
type acces_typtabidsymboles is access typtabidsymboles;

-- tableau d'info sur les symboles
type typinfosymbole is (feracheval, rayon);
type typtabinfosymboles is array(integer range <>, typinfosymbole range <>) of integer;

-- tableau d'info sur la proportion minimale des symboles
type typtabpropmin is array(integer range <>) of float;                                                                                              

-- partie entiere
function E(x : in float) return integer;
                     
-- cree un fichier du vecteur au format Viewer
procedure exportvecviewer(tbnomi : in typtbnomi; ficviewer : File_type; ficecrier : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; Resol_Mut_reel : in float;
                     ps, pe, cs, ce, ncs, nce, sse, rs : in boolean; mispo, miobs, miecf : in out boolean);

-- cree un fichier du vecteur au format Viewer pour creation de l'image de mutilation
procedure export_creMutil(tbnomi : in typtbnomi; Ficvecteur : in out File_type; Ji : in integer;
                          InvEchelle : in integer; Resolution : in integer; Minimum_terrain : in point_type_reel;
                          Nb_symboles : in integer; ppmm : in integer;
						  Estomp_CoinInfG : out point_type_reel; Estomp_Index : in out integer;
						  ImaEcr_CoinInfG : out point_type_reel; ImaEcr_Index : in out integer);
					 
-- cree un fichier du vecteur au format Viewer pour le module de saisie des positions des ecritures sans position
procedure exportvecviewerm_horiz(tbnomi : in typtbnomi; ficviewer : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; tnp : in typtnp;
                     ps, pe, cs, ce, ncs, nce, sse, rs : in boolean);

-- cree un fichier du vecteur au format Viewer pour le module de saisie des positions
-- des ecritures sans position
procedure exportvecviewerm_NoRou(tbnomi : in typtbnomi; ficviewer : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; tnp : in typtnp; T_MT : in out TMT;
                     T_TRS : in out TTRONCON; T_ARCS : in TARCS;
                     ps, pe, cs, ce, ncs, nce, sse, rs : in boolean);

-- renvoie true si dans la bordure interdite
function dansbordinter(xpix,ypix : in integer; pastestinterb : in integer; ko : in integer;
                       file_image	: in byte_io.file_type; lps,ncol,nlig : in integer;
                       stripoffset : in acces_tabstrip; nbindexintertous : in integer;
                       tabindexintertous : in acces_typtabindextous) return boolean;

-- renvoie le nombre de valeurs d'index interpretees comme interdites par tous les codes typo
function calcnbindextous(tsubst : in acces_tabsub; nb_interp : in integer; valindextous : in integer) return integer;

-- calcul des valeurs d'index interpretees comme interdites par tous les codes typo
procedure calcindextous(tsubst : in acces_tabsub; nb_interp : in integer; valindextous : in integer;
                        nbindextous : in integer;
                        tabindextous : out acces_typtabindextous);

function valindex(xpix,ypix : in integer; file_image	: in byte_io.file_type;
                  lps,ncol : in integer; stripoffset : in acces_tabstrip) return byte;

procedure ficview(tbnomi: in typtbnomi; InvEchelle : in integer; Resolution : in integer; Minimum_terrain : in point_type_reel;
                  Nb_symboles : in integer; Resol_Mut_reel : in float;
                  mispo : in out boolean; miobs : in out boolean; miecf : in out boolean);
  
-- procedure principale du package
procedure verifmutil(tbnomi: in typtbnomi; tsubst : in acces_tabsub; nb_interp : in integer;
                     InvEchelle : in integer; Resolution : in integer; Minimum_terrain : in point_type_reel;
                     file_image	: in out byte_io.file_type;
                     lps,ncol,nlig: in integer; stripoffset: in tiff_io.acces_tabstrip;
                     Nb_symboles : in integer; tabstat : in out typtabstat;
                     verdict : out typverdict);

end VERIFMUT;

package body VERIFMUT is

-- partie entiere
function E(x : in float) return integer is
begin
  if float(integer(x))<=x then
    return(integer(x));
  else
    return(integer(x)-1);
  end if;
end E;

function address_to_integer is new system.FETCH_FROM_ADDRESS(integer);

-- cree un fichier du vecteur au format Viewer
procedure exportvecviewer(tbnomi : in typtbnomi; ficviewer : File_type; ficecrier : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; Resol_Mut_reel : in float;
                     ps, pe, cs, ce, ncs, nce, sse, rs : in boolean; mispo, miobs, miecf : in out boolean) is
-- polygone symbole, polygone ecriture, circulaire symbole, circulaire ecriture
-- non circulaire symbole, non circulaire ecriture, sans symbole ecriture, routes symboles

use type gb.bool;

code : integer; -- 1=symboles 2=surfaces 3=routes 4=ecr.symboles 5=ecr.surfaces 6=topoflou
symboleviewer : integer range 0..7; -- 7=pas de symbole
couleur : integer;
gis : integer range 0..14 :=0;
corpsfois10 : integer :=150;
M,Plusud : Point_type_reel;
S : Point_type;
epaisseur : integer;
styligne : integer range 0..4;  -- 0=continue 1=tiret�e
n,nc,nba,jj : integer;
ptemp : point_type;
nb_pts_max:integer:=pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
contpt,contptp : point_liste_type(1..nb_pts_max);
tarcs : liens_array_type(1..infoos.max_arc_fac); 
c : character;
cptnbanul : integer :=0;
iest,inord,iouest,isud : natural;
j : integer;
info_font: typtfont(1..500);
nbfont : integer;
aa,bb : integer;
policename : string(1..70); -- string(1..32);
k,jo : integer;
M1g,M2g : Point_type;
M1t,M2t : Point_type_reel;
Plageno, Plagese : Point_type;


begin

  charge_fontes(info_font,nbfont);

  put_line(ficviewer,"FORMAT VECTEUR VIEWER 1.0");
  put_line(ficviewer,"COORDONNEES TERRAIN");

  put_line(ficecrier,"FORMAT VECTEUR VIEWER 1.0");
  put_line(ficecrier,"COORDONNEES TERRAIN");

  -- routes et courbes de niveau
  if route=true and rs=true then
    declare
      rtarcs : Liens_array_type(1..gr_infos(grr).Nb_arcs);
      lista : Point_liste_type(1..gr_infos(grr).max_pts_arc);    
      ia,ip,na : integer;
      arc : arc_type;
      br : boite_type;
    begin
  	  Plageno:=convert_coord_terrain_graphe(Vno,InvEchelle,Resolution,Minimum_terrain);
      Plagese:=convert_coord_terrain_graphe(Vse,InvEchelle,Resolution,Minimum_terrain);
 	  br.minimum:=(plageno.coor_x,plagese.coor_y);
      br.maximum:=(plagese.coor_x,plageno.coor_y);
      Gr_arcs_in_rectangle(grr,br.minimum,br.maximum,rtarcs,na);
      code:=3;
      if na>0 then
        for ia in 1..na loop
          arc:=Gr_arc(grr,rtarcs(ia));
		  	
          if (arc.octet_vms=1 and (strNumRou(1)='O' or strNumRou(1)='o'))
          or (arc.octet_vms=4 and (StrEcriDispo(1)='O' or StrEcriDispo(1)='o'))
		  or (((arc.octet_vms=2 and Gb.checked(dialog2.AmontADroiteCheckBox)=gb.false) or arc.octet_vms=3) and (strCotesCourbes(1)='O' or strCotesCourbes(1)='o')) then
		  	-- C'est un arc qui porte un nom a placer et l'option de placement correspondante a �t� s�lectionn�e
            Gr_points_d_arc(grr,rtarcs(ia),lista,n);
            put_line(ficviewer,integer'image(code));        
            put_line(ficviewer,"LIGNE");
            styligne:=0;
            if arc.octet_vms=1 or arc.octet_vms=3 or arc.octet_vms=4 then   -- route, courbe de niveau ou arc hydro avec �criture � placer
              Epaisseur:=2;
			  if arc.att_graph/=0 then
		  	    -- dans l'emprise de l'image
			    if arc.octet_vms=1 then
				  -- arc de route impliqu� dans un placement
				  couleur:=65535; -- jaune
                end if;
				if arc.octet_vms=3 then
                  -- arc de courbe de niveau impliqu� dans un placement
				  couleur:=377594; -- orange
                end if;
				if arc.octet_vms=4 then
                  -- arc hydro impliqu� dans un placement
			      couleur:=16777088; -- bleu ciel
                end if;
		      else
		  	    -- hors image
                couleur:=0; -- noir			
	  	      end if;
	        else   -- arc.octet_vms=2 : courbe de niveau sans �criture � placer mais contribuant au MNT 
              Epaisseur:=1;
			  if arc.att_graph/=0 then
		  	    -- dans l'emprise de l'image
				Couleur:=12632256; -- gris
	          else
		  	    -- hors image
                couleur:=0; -- noir			
	  	      end if; 			
            end if;
            put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
            put_line(ficviewer,integer'image(n));
            for ip in 1..n loop
              M:=Convert_coord_graphe_terrain(lista(ip),InvEchelle,Resolution,Minimum_terrain);
              put(ficviewer,M.coor_x,0,2,0);
              put(ficviewer,tab);
              put(ficviewer,M.coor_y,0,2,0);
              new_line(ficviewer);
            end loop;
          end if;

          for l in 1..nb_codes_route loop
            if nbNoRou/=tbnomi'last and gr_legende_l(legr,arc.att_graph).theme=tcodes_route(l) then
              -- arc de route � �viter de franchir
              Gr_points_d_arc(grr,rtarcs(ia),lista,n);
              put_line(ficviewer,integer'image(code));        
              put_line(ficviewer,"LIGNE");
              couleur:=4194432; -- marron
			  styligne:=1;
			  Epaisseur:=1;
              put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
              put_line(ficviewer,integer'image(n));
              for ip in 1..n loop
                M:=Convert_coord_graphe_terrain(lista(ip),InvEchelle,Resolution,Minimum_terrain);
                put(ficviewer,M.coor_x,0,2,0);
                put(ficviewer,tab);
                put(ficviewer,M.coor_y,0,2,0);
                new_line(ficviewer);
              end loop;
            end if;
  
          end loop;
        end loop;
      end if;
    end;
  end if;
  
  for i in 1..tbnomi'last-NbNoRou loop
    
    -- symboles circulaires
    if tbnomi(i).TOPO.objet_type/=zonal and tbnomi(i).indice_symbole>=1 then
      if cs=true then
        Points_De_Cercle(i,tbnomi,nc,contpt,iest,inord,iouest,isud,true);
        mispo:=true;
        code:=1;
        put_line(ficviewer,integer'image(code));        
        put_line(ficviewer,"LIGNE");
        couleur:=6532376; -- vert
        -- couleur:=16744576; -- bleu profond
        styligne:=0;
		Epaisseur:=1;
        put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(ficviewer,integer'image(E((float(nc)-1.0)/3.0+1.0)+1));
        j:=1;
        while j<=nc loop
          M:=Convert_coord_graphe_terrain(contpt(j),InvEchelle,Resolution,Minimum_terrain);          
          put(ficviewer,M.coor_x,0,2,0);
          put(ficviewer,tab);
          put(ficviewer,M.coor_y,0,2,0);
          new_line(ficviewer);
          j:=j+3;
        end loop;  
        M:=Convert_coord_graphe_terrain(contpt(1),InvEchelle,Resolution,Minimum_terrain);          
        put(ficviewer,M.coor_x,0,2,0);
        put(ficviewer,tab);
        put(ficviewer,M.coor_y,0,2,0);
        new_line(ficviewer);
      end if;
      if ce=true then
        code:=4;
        put_line(ficviewer,integer'image(code));
        put_line(ficviewer,"POINT");
        if mutil then
		  put_line(ficecrier,integer'image(code));
          put_line(ficecrier,"POINT");
        end if;
		if cs=true then
          symboleviewer:=7;
        else
          symboleviewer:=tbnomi(i).indice_symbole mod 7;        
        end if;
        couleur:=0; -- noir
        put(ficviewer,"1");
        put(ficviewer,tab);
        put(ficviewer,integer'image(symboleviewer));
        put(ficviewer,tab);
        put(ficviewer,integer'image(couleur));
        put(ficviewer,tab);
        put(ficviewer,integer'image(gis));
        put(ficviewer,tab);
        if mutil then
          put(ficecrier,"1");
          put(ficecrier,tab);
          put(ficecrier,integer'image(symboleviewer));
          put(ficecrier,tab);
          put(ficecrier,integer'image(couleur));
          put(ficecrier,tab);
          put(ficecrier,integer'image(gis));
          put(ficecrier,tab);
	    end if;
		M1g:=(0,0);
        M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
        M2g:=(0,tbnomi(i).corps);      
        M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
        if mutil then
          put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0,0,2,0);
        else
          put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);         
        end if;
        new_line(ficviewer);
        if mutil then
          put(ficecrier,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);         
          new_line(ficecrier);
        end if;
        for j in 0..31 loop -- Interfaces.C.Size_t(info_font(tbnomi(i).police).police.lfFaceName) loop --31
          c:=character(info_font(tbnomi(i).police).police.lfFaceName(j));
            policename(j+1):=c;
        end loop;
        jo:=33;
        for j in 1..32 loop
          if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
            jo:=j;
            exit;
          end if;
        end loop;
        put_line(ficviewer,policename(1..jo-1));
        put_line(ficviewer,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
        put_line(ficviewer,"1");
        if mutil then
		  put_line(ficecrier,policename(1..jo-1));
          put_line(ficecrier,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
          put_line(ficecrier,"1");
        end if;
		if cs=true then
          M:=Convert_coord_graphe_terrain(contpt(isud),InvEchelle,Resolution,Minimum_terrain);
        else
          M:=Convert_coord_graphe_terrain(tbnomi(i).coord_objet,InvEchelle,Resolution,Minimum_terrain);
        end if;
        put(ficviewer,M.coor_x,0,2,0);
        put(ficviewer,tab);
        put(ficviewer,M.coor_y,0,2,0);
        new_line(ficviewer);
        if mutil then
		  put(ficecrier,M.coor_x,0,2,0);
          put(ficecrier,tab);
          put(ficecrier,M.coor_y,0,2,0);
          new_line(ficecrier);
        end if;
      end if;
    end if;
    
    -- polygones et symboles non circulaires
    if tbnomi(i).TOPO.objet_type=zonal then
      if (tbnomi(i).indice_symbole=0 and ps=true) or (tbnomi(i).indice_symbole>=1 and ncs=true) then
        Plusud:=(0.0,float'last);
        n:=0;
        nba:= Gr_face(gros,tbnomi(i).topo.num_objet).Nombre_arc;
        tarcs(1..nba):= Gr_arcs_de_Face(gros,tbnomi(i).topo.num_objet);
        for a in 1..nba loop
          if tarcs(a)<0 then
            Gr_Points_d_arc(gros,-tarcs(a),contptp,nc);
            jj:=nc;
            for k in 1..nc/2 loop
              ptemp:=contptp(k);
              contptp(k) :=contptp(jj);
              contptp(jj):=ptemp;
              jj:=jj-1;
            end loop;
          else
            Gr_Points_d_arc(gros,tarcs(a),contptp,nc);
          end if;
          for j in 1..nc loop contpt(n+j):= contptp(j); end loop;
          n:= n+nc;
          if tbnomi(i).indice_symbole=0 then
            miobs:=true;
            code:=2;
          else
            mispo:=true;
            code:=1;
          end if;          
          put_line(ficviewer,integer'image(code));        
          put_line(ficviewer,"LIGNE");
          couleur:=6532376; -- vert
          -- couleur:=16744576; -- bleu profond
          styligne:=0;
		  Epaisseur:=1;
          put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(ficviewer,integer'image(nc));
          for j in 1..nc loop
            M:=Convert_coord_graphe_terrain(contptp(j),InvEchelle,Resolution,Minimum_terrain);          
            if M.coor_y<Plusud.coor_y then
              Plusud:=M;
            end if;
            put(ficviewer,M.coor_x,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M.coor_y,0,2,0);
            new_line(ficviewer);
          end loop;  
        end loop;
        if (tbnomi(i).indice_symbole=0 and pe=true) or (tbnomi(i).indice_symbole>=1 and nce=true) then
          if tbnomi(i).indice_symbole=0 then
            code:=5;
          else
            code:=4;
          end if;
          put_line(ficviewer,integer'image(code));
          put_line(ficviewer,"POINT");
          if mutil then
		    put_line(ficecrier,integer'image(code));
            put_line(ficecrier,"POINT");
          end if;
		  if tbnomi(i).indice_symbole=0 then
            symboleviewer:=0;
          else
            if ncs=true then
              symboleviewer:=7;
            else
              symboleviewer:=tbnomi(i).indice_symbole mod 7;
            end if;
          end if;
          couleur:=0; -- noir
          put(ficviewer,"1");
          put(ficviewer,tab);
          put(ficviewer,integer'image(symboleviewer));
          put(ficviewer,tab);
          put(ficviewer,integer'image(couleur));
          put(ficviewer,tab);
          put(ficviewer,integer'image(gis));
          put(ficviewer,tab);
          if mutil then
		    put(ficecrier,"1");
            put(ficecrier,tab);
            put(ficecrier,integer'image(symboleviewer));
            put(ficecrier,tab);
            put(ficecrier,integer'image(couleur));
            put(ficecrier,tab);
            put(ficecrier,integer'image(gis));
            put(ficecrier,tab);
          end if;
		  M1g:=(0,0);
          M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
          M2g:=(0,tbnomi(i).corps);      
          M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
          if mutil then
            put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0,0,2,0);
          else
            put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);
          end if;            
          new_line(ficviewer);
          if mutil then
            put(ficecrier,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);
            new_line(ficecrier);
          end if;            
          for j in 0..31 loop
            policename(j+1):=character(info_font(tbnomi(i).police).police.lfFaceName(j));
          end loop;
          jo:=33;
          for j in 1..32 loop
            if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
              jo:=j;
              exit;
            end if;
          end loop;
          put_line(ficviewer,policename(1..jo-1));
          put_line(ficviewer,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
          put_line(ficviewer,"1");
          if mutil then
		    put_line(ficecrier,policename(1..jo-1));
            put_line(ficecrier,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
            put_line(ficecrier,"1");
          end if;
		  if tbnomi(i).indice_symbole=0 then
            M:=Convert_coord_graphe_terrain(tbnomi(i).coord_objet,InvEchelle,Resolution,Minimum_terrain);
            put(ficviewer,M.coor_x,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M.coor_y,0,2,0);
            new_line(ficviewer);
			if mutil then
              put(ficecrier,M.coor_x,0,2,0);
              put(ficecrier,tab);
              put(ficecrier,M.coor_y,0,2,0);
              new_line(ficecrier);
            end if;
          else
            if ncs=true then
              put_line(ficviewer,integer'image(integer(Plusud.coor_x))&tab&integer'image(integer(Plusud.coor_y)));
              if mutil then
			    put_line(ficecrier,integer'image(integer(Plusud.coor_x))&tab&integer'image(integer(Plusud.coor_y)));
              end if;
            else
              M:=Convert_coord_graphe_terrain(tbnomi(i).coord_objet,InvEchelle,Resolution,Minimum_terrain);            
              put(ficviewer,M.coor_x,0,2,0);
              put(ficviewer,tab);
              put(ficviewer,M.coor_y,0,2,0);
              new_line(ficviewer);
              if mutil then
			    put(ficecrier,M.coor_x,0,2,0);
                put(ficecrier,tab);
                put(ficecrier,M.coor_y,0,2,0);
                new_line(ficecrier);
              end if;
            end if;
          end if;
        end if;
      end if;
    end if;

    --  Topoflou (�critures horizontales sans symbole ni surface d'appui)
    if tbnomi(i).TOPO.objet_type/=zonal and tbnomi(i).indice_symbole=0 and sse=true then
      miecf:=true;
      code:=6;
      put_line(ficviewer,integer'image(code));
      put_line(ficviewer,"POINT");
      symboleviewer:=0;
      couleur:=0; -- noir      
      put(ficviewer,"1");
      put(ficviewer,tab);
      put(ficviewer,integer'image(symboleviewer));
      put(ficviewer,tab);
      put(ficviewer,integer'image(couleur));
      put(ficviewer,tab);
      put(ficviewer,integer'image(gis));
      put(ficviewer,tab);
      if mutil then 
	    put_line(ficecrier,integer'image(code));
        put_line(ficecrier,"POINT");
        put(ficecrier,"1");
        put(ficecrier,tab);
        put(ficecrier,integer'image(symboleviewer));
        put(ficecrier,tab);
        put(ficecrier,integer'image(couleur));
        put(ficecrier,tab);
        put(ficecrier,integer'image(gis));
        put(ficecrier,tab);
      end if;
	  M1g:=(0,0);
      M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
      M2g:=(0,tbnomi(i).corps);      
      M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
      if mutil then
        put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0,0,2,0);
      else
        put(ficviewer,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);      
      end if;
      new_line(ficviewer);
      if mutil then
        put(ficecrier,(M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0,0,2,0);      
        new_line(ficecrier);
      end if;
      for j in 0..31 loop
        policename(j+1):=character(info_font(tbnomi(i).police).police.lfFaceName(j));
      end loop;
      jo:=33;
      for j in 1..32 loop
        if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
          jo:=j;
          exit;
        end if;
      end loop;
      put_line(ficviewer,policename(1..jo-1));
      put_line(ficviewer,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
      put_line(ficviewer,"1");
      M:=Convert_coord_graphe_terrain(tbnomi(i).coord_objet,InvEchelle,Resolution,Minimum_terrain);
      put(ficviewer,M.coor_x,0,2,0);
      put(ficviewer,tab);
      put(ficviewer,M.coor_y,0,2,0);
      new_line(ficviewer);
      if mutil then
	    put_line(ficecrier,policename(1..jo-1));
        put_line(ficecrier,tbnomi(i).chaine(1)(1..tbnomi(i).ncar(1)));
        put_line(ficecrier,"1");
        put(ficecrier,m.coor_x,0,2,0);
        put(ficecrier,tab);
        put(ficecrier,M.coor_y,0,2,0);
        new_line(ficecrier);
      end if;
    end if;
  end loop;

  -- ecriture du contour de l'image pour l'entr�e sans image
  if mutil then
    code:=0; 
	put_line(ficviewer,integer'image(code));
    put_line(ficviewer,"LIGNE");
    couleur:=0; -- noir
    styligne:=0;
    Epaisseur:=3;
    put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
    put_line(ficviewer,"5");
    put(ficviewer,Tso.coor_x,0,2,0);
    put(ficviewer,tab);
    put(ficviewer,tso.coor_Y,0,2,0);
    new_line(ficviewer);
    put(ficviewer,tso.coor_x,0,2,0);
    put(ficviewer,tab);
    put(ficviewer,tne.coor_Y,0,2,0);
    new_line(ficviewer);
    put(ficviewer,tne.coor_x,0,2,0);
    put(ficviewer,tab);
    put(ficviewer,tne.coor_Y,0,2,0);
    new_line(ficviewer);
    put(ficviewer,tne.coor_x,0,2,0);
    put(ficviewer,tab);
    put(ficviewer,tso.coor_Y,0,2,0);
    new_line(ficviewer);
    put(ficviewer,tso.coor_x,0,2,0);
    put(ficviewer,tab);
    put(ficviewer,tso.coor_Y,0,2,0);
    new_line(ficviewer);
  end if;

end exportvecviewer;


-- cree un fichier du vecteur au format Viewer pour creation de l'image de mutilation
procedure export_creMutil(tbnomi : in typtbnomi; Ficvecteur : in out File_type; Ji : in integer; InvEchelle : in integer;
                          Resolution : in integer; Minimum_terrain : in point_type_reel;
                          Nb_symboles : in integer; ppmm : in integer;
						  Estomp_CoinInfG : out point_type_reel; Estomp_Index : in out integer;
						  ImaEcr_CoinInfG : out point_type_reel; ImaEcr_Index : in out integer) is

type Color is record 
  nom : Unbounded_string:=Null_unbounded_string;
  R : integer;
  G : integer;
  B : integer;
end record;

use type gb.bool;

code : integer:=1;
couleur : integer;
epaisseur : integer;
M : point_type_reel;
styligne : integer range 0..6;
n,nc,nba,jj : integer;
ptemp : point_type;
nb_pts_max:integer:=pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
contpt,contptp : point_liste_type(1..nb_pts_max);
tarcs : liens_array_type(1..infoos.max_arc_fac); 
c : character;
cptnbanul : integer :=0;
iest,inord,iouest,isud : natural;
j : integer;
aa,bb : integer;
policename : string(1..70); -- string(1..32);
k,jo : integer;
M1g,M2g : Point_type;
M1t,M2t : Point_type_reel;
Plageno, Plagese : Point_type;
stylePlein : integer:=6;
x : float;
fEmpile,Frgb : File_type;        -- Fichier d'empilement des legendes
Legende : string30;
Jtab, index, Last : integer;
Ligne : string(1..32000);	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
ColorMap : array(0..255,1..3) of integer := (others => (others => -1));
i : integer:=-1;
Ptr_indice : Liens_access_type;	-- Pointeur des indices des separateurs
Compteur : integer; -- Nombre de tabulations sur une ligne du fichier empile.txt + retour charriot
j0 : integer;

ok : boolean;
NColor : integer:=138;
Palette : array(1..NColor) of Color:=
		((to_unbounded_string("AliceBlue"),240,248,255),
		 (to_unbounded_string("AntiqueWhite"),250,235,215),
		 (to_unbounded_string("Aquamarine"),127,255,212),
		 (to_unbounded_string("Azure"),240,255,255),
		 (to_unbounded_string("Beige"),245,245,220),
		 (to_unbounded_string("Bisque"),255,228,196),
		 (to_unbounded_string("Black"),0,0,0),
		 (to_unbounded_string("BlanchedAlmond"),255,235,205),
		 (to_unbounded_string("Blue"),0,0,255),
		 (to_unbounded_string("BlueViolet"),138,43,226),
		 (to_unbounded_string("Brown"),165,42,42),
		 (to_unbounded_string("BurlyWood"),222,184,135),
		 (to_unbounded_string("CadetBlue"),95,158,160),
		 (to_unbounded_string("Chartreuse"),127,255,0),
		 (to_unbounded_string("Chocolate"),210,105,30),
		 (to_unbounded_string("Coral"),255,127,80),
		 (to_unbounded_string("CornflowerBlue"),100,149,237),
		 (to_unbounded_string("Cornsilk"),255,248,220),
		 (to_unbounded_string("Crimson"),220,20,60),
		 (to_unbounded_string("Cyan"),0,255,255),
		 (to_unbounded_string("DarkBlue"),0,0,139),
		 (to_unbounded_string("DarkCyan"),0,139,139),
		 (to_unbounded_string("DarkGoldenrod"),184,134,11),
		 (to_unbounded_string("DarkGray"),169,169,169),
		 (to_unbounded_string("DarkGreen"),0,100,0),
		 (to_unbounded_string("DarkKhaki"),189,183,107),
		 (to_unbounded_string("DarkMagenta"),139,0,139),
		 (to_unbounded_string("DarkOliveGreen"),85,107,47),
		 (to_unbounded_string("DarkOrange"),255,140,0),
		 (to_unbounded_string("DarkOrchid"),153,50,204),
		 (to_unbounded_string("DarkRed"),139,0,0),
		 (to_unbounded_string("DarkSalmon"),233,150,122),
		 (to_unbounded_string("DarkSeaGreen"),143,188,143),
		 (to_unbounded_string("DarkSlateBlue"),72,61,139),
		 (to_unbounded_string("DarkSlateGray"),47,79,79),
		 (to_unbounded_string("DarkTurquoise"),0,206,209),
		 (to_unbounded_string("DarkViolet"),148,0,211),
		 (to_unbounded_string("DeepPink"),255,20,147),
		 (to_unbounded_string("DeepSkyBlue"),0,191,255),
		 (to_unbounded_string("DimGray"),105,105,105),
		 (to_unbounded_string("DodgerBlue"),30,144,255),
		 (to_unbounded_string("FireBrick"),178,34,34),
		 (to_unbounded_string("FloralWhite"),255,250,240),
		 (to_unbounded_string("ForestGreen"),34,139,34),
		 (to_unbounded_string("Fuchsia"),255,0,255),
		 (to_unbounded_string("Gainsboro"),220,220,220),
		 (to_unbounded_string("GhostWhite"),248,248,255),
		 (to_unbounded_string("Gold"),255,215,0),
		 (to_unbounded_string("Goldenrod"),218,165,32),
		 (to_unbounded_string("Gray"),128,128,128),
		 (to_unbounded_string("Green"),0,128,0),
		 (to_unbounded_string("GreenYellow"),173,255,47),
		 (to_unbounded_string("Honeydew"),240,255,240),
		 (to_unbounded_string("HotPink"),255,105,180),
		 (to_unbounded_string("IndianRed"),205,92,92),
		 (to_unbounded_string("Indigo"),75,0,130),
		 (to_unbounded_string("Ivory"),255,255,240),
		 (to_unbounded_string("Khaki"),240,230,140),
		 (to_unbounded_string("Lavender"),230,230,250),
		 (to_unbounded_string("LavenderBlush"),255,240,245),
		 (to_unbounded_string("LawnGreen"),124,252,0),
		 (to_unbounded_string("LemonChiffon"),255,250,205),
		 (to_unbounded_string("LightBlue"),173,216,230),
		 (to_unbounded_string("LightCoral"),240,128,128),
		 (to_unbounded_string("LightCyan"),224,255,255),
		 (to_unbounded_string("LightGoldenrodYellow"),250,250,210),
		 (to_unbounded_string("LightGreen"),144,238,144),
		 (to_unbounded_string("LightGrey"),211,211,211),
		 (to_unbounded_string("LightPink"),255,182,193),
		 (to_unbounded_string("LightSalmon"),255,160,122),
		 (to_unbounded_string("LightSeaGreen"),32,178,170),
		 (to_unbounded_string("LightSkyBlue"),135,206,250),
		 (to_unbounded_string("LightSlateGray"),119,136,153),
		 (to_unbounded_string("LightSteelBlue"),176,196,222),
		 (to_unbounded_string("LightYellow"),255,255,224),
		 (to_unbounded_string("Lime"),0,255,0),
		 (to_unbounded_string("LimeGreen"),50,205,50),
		 (to_unbounded_string("Linen"),250,240,230),
		 (to_unbounded_string("Maroon"),128,0,0),
		 (to_unbounded_string("MediumAquamarine"),102,205,170),
		 (to_unbounded_string("MediumBlue"),0,0,205),
		 (to_unbounded_string("MediumOrchid"),186,85,211),
		 (to_unbounded_string("MediumPurple"),147,112,219),
		 (to_unbounded_string("MediumSeaGreen"),60,179,113),
		 (to_unbounded_string("MediumSlateBlue"),123,104,238),
		 (to_unbounded_string("MediumSpringGreen"),0,250,154),
		 (to_unbounded_string("MediumTurquoise"),72,209,204),
		 (to_unbounded_string("MediumVioletRed"),199,21,133),
		 (to_unbounded_string("MidnightBlue"),25,25,112),
		 (to_unbounded_string("MintCream"),245,255,250),
		 (to_unbounded_string("MistyRose"),255,228,225),
		 (to_unbounded_string("Moccasin"),255,228,181),
		 (to_unbounded_string("NavajoWhite"),255,222,173),
		 (to_unbounded_string("Navy"),0,0,128),
		 (to_unbounded_string("OldLace"),253,245,230),
		 (to_unbounded_string("Olive"),128,128,0),
		 (to_unbounded_string("OliveDrab"),107,142,35),
		 (to_unbounded_string("Orange"),255,165,0),
		 (to_unbounded_string("OrangeRed"),255,69,0),
		 (to_unbounded_string("Orchid"),218,112,214),
		 (to_unbounded_string("PaleGoldenrod"),238,232,170),
		 (to_unbounded_string("PaleGreen"),152,251,152),
		 (to_unbounded_string("PaleTurquoise"),175,238,238),
		 (to_unbounded_string("PaleVioletRed"),219,112,147),
		 (to_unbounded_string("PapayaWhip"),255,239,213),
		 (to_unbounded_string("PeachPuff"),255,218,185),
		 (to_unbounded_string("Peru"),205,133,63),
		 (to_unbounded_string("Pink"),255,192,203),
		 (to_unbounded_string("Plum"),221,160,221),
		 (to_unbounded_string("PowderBlue"),176,224,230),
		 (to_unbounded_string("Purple"),128,0,128),
		 (to_unbounded_string("Red"),255,0,0),
		 (to_unbounded_string("RosyBrown"),188,143,143),
		 (to_unbounded_string("RoyalBlue"),65,105,225),
		 (to_unbounded_string("SaddleBrown"),139,69,19),
		 (to_unbounded_string("Salmon"),250,128,114),
		 (to_unbounded_string("SandyBrown"),244,164,96),
		 (to_unbounded_string("SeaGreen"),46,139,87),
		 (to_unbounded_string("Seashell"),255,245,238),
		 (to_unbounded_string("Sienna"),160,82,45),
		 (to_unbounded_string("Silver"),192,192,192),
		 (to_unbounded_string("SkyBlue"),135,206,235),
		 (to_unbounded_string("SlateBlue"),106,90,205),
		 (to_unbounded_string("SlateGray"),112,128,144),
		 (to_unbounded_string("Snow"),255,250,250),
		 (to_unbounded_string("SpringGreen"),0,255,127),
		 (to_unbounded_string("SteelBlue"),70,130,180),
		 (to_unbounded_string("Tan"),210,180,140),
		 (to_unbounded_string("Teal"),0,128,128),
		 (to_unbounded_string("Thistle"),216,191,216),
		 (to_unbounded_string("Tomato"),255,99,71),
		 (to_unbounded_string("Turquoise"),64,224,208),
		 (to_unbounded_string("Violet"),238,130,238),
		 (to_unbounded_string("Wheat"),245,222,179),
		 (to_unbounded_string("White"),255,255,255),
		 (to_unbounded_string("WhiteSmoke"),245,245,245),
		 (to_unbounded_string("Yellow"),255,255,0),
		 (to_unbounded_string("YellowGreen"),154,205,50));

message : Unbounded_string;
affiche_message : boolean:=false;
p : positive;

begin

  begin
    jo:=ncar_fi;
    loop
      if nomfichier_image(jo)='\' then
        exit;
      end if;
      jo:=jo-1;
    end loop;

	message:=to_Unbounded_string("Certains indexes du fichier palette"&eol&nomfichier_palette(1..ncar_fp)
                                             &eol&"sont associ�s � la m�me couleur"&eol&eol);
    open(Frgb,in_file,nomfichier_Palette(1..Ncar_Fp));
    i:=0;
    while not(end_of_file(Frgb)) loop
  	  get_line(Frgb,ligne,lLigne);
	  if lLigne=0 then
        goto finboucle;
      end if;
	  ok:=false;
      for j in 1..NColor loop
        if ligne(1..LLigne)=to_string(Palette(j).Nom) then
          ColorMap(I,1):=Palette(j).R;
          ColorMap(I,2):=Palette(j).G;
          ColorMap(I,3):=Palette(j).B;
		  ok:=true;
          exit;
        end if;
      end loop;
      if ok=false then
	    Indice_Separateur2(Ligne,LLigne,Tab,Ptr_Indice,Compteur);
        get(Ligne(Ptr_Indice(0)+1..Ptr_Indice(1)-1),ColorMap(I,1),last);
        get(Ligne(Ptr_Indice(1)+1..Ptr_Indice(2)-1),ColorMap(I,2),last);
        get(Ligne(Ptr_Indice(2)+1..LLigne),ColorMap(I,3),last);
      end if;

      for j in 0..i-1 loop
        if ((ColorMap(i,1)=ColorMap(J,1)) and (ColorMap(i,2)=ColorMap(J,2)) and (ColorMap(i,3)=ColorMap(J,3))) then	
          Message:=Message&to_Unbounded_string("l'indexe"&integer'image(I)&" va fusionner avec l'index"&integer'image(j)&eol);
          affiche_message:=true;
		  exit;
        end if;
      end loop;
	  << finboucle >>
      i:=i+1;
    end loop;
    close(Frgb);
  exception when others =>
    if i=-1 then
      Msg_erreur:=to_unbounded_string("Impossible d'ouvrir le fichier palette"&eol&nomfichier_Palette(1..ncar_fp));
    else
      Msg_Erreur:=to_unbounded_string("Erreur de lecture de la couleur de l'indexe"&integer'image(i)&eol
	                                  &"� la ligne"&integer'image(i+1)&" du fichier palette"
	                                   &eol&nomfichier_Palette(1..ncar_fp));
    end if;
	raise;
  end;
  
  if affiche_message=true then
  	gb.MsgBox(to_string(Message)," Cr�ation de l'image de mutilation",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;

  put_line(ficvecteur,"FORMAT VECTEUR VIEWER 1.0");
  put_line(ficvecteur,"COORDONNEES TERRAIN");

  declare
    k : integer:=0;
  begin
    -- open(FEmpile,in_file,nomfichier_image(1..jo)&"empile.txt");
    open(FEmpile,in_file,nomfichier_empilement(1..Ncar_fe));
    k:=1;
    while not(end_of_file(FEmpile)) loop

      Ligne:=(others => ' ');
      get_line(FEmpile,Ligne,LLigne);

      Indice_Separateur2(Ligne,LLigne,Tab,Ptr_Indice,Compteur);
      get(Ligne(Ptr_Indice(Compteur-1)+1..LLigne),index,last);

      for a in 1..Compteur-1 loop
        Legende:=(others => ' ');
        Legende(1..Ptr_Indice(A)-Ptr_Indice(A-1)-1):=Ligne(Ptr_Indice(A-1)+1..Ptr_Indice(A)-1);

        if Ptr_Indice(A)-Ptr_Indice(A-1)-1=9 then
		  if Legende(1..9)="ESTOMPAGE" then
            close(ficvecteur);

			if ncar_Es=0 then
		      Msg_Erreur:=To_Unbounded_String("Le fichier d'empilement fait r�f�rence � une image de l'estompage."&eol&" Vous devez en indiquer le chemin dans les Donn�es optionnelles");
              k:=-1;
		      p:=0;
		    end if;

--            open(ficn,in_file,ficPat);
--            EXISTENCE_FIC(ficn,61,nomfichier_Estompage,ncar_Es);
--            close(ficn);

            create(ficvecteur,Out_File,nomfichier_image(1..ji)&"vecteur2.txt","");
            Put_line(ficvecteur,"FORMAT VECTEUR VIEWER 1.0");
            put_line(ficvecteur,"COORDONNEES TERRAIN");
            Estomp_Index:=Index;
			if compteur=4 then  -- presence des coordonnees du coin inf.gauche
              Get(Ligne(Ptr_Indice(A)+1..Ptr_Indice(A+1)-1),Estomp_CoinInfG.Coor_X,Last);
              Get(Ligne(Ptr_Indice(A+1)+1..Ptr_Indice(A+2)-1),Estomp_CoinInfG.Coor_Y,Last);
			  Compteur:=Compteur-2;
            else
			  Estomp_CoinInfG.Coor_X:=tso.coor_X;
			  Estomp_CoinInfG.Coor_Y:=tso.coor_y;
            end if;
          end if;
        end if;

--        if Ptr_Indice(A)-Ptr_Indice(A-1)-1=15 then
--		  if Legende(1..15)="IMAGE_ECRITURES" then
--            ImaEcr_Index:=Index;
--            Get(Ligne(Ptr_Indice(A)+1..Ptr_Indice(A+1)-1),ImaEcr_CoinInfG.Coor_X,Last);
--            Get(Ligne(Ptr_Indice(A+1)+1..Ptr_Indice(A+2)-1),ImaEcr_CoinInfG.Coor_Y,Last);
--			Compteur:=Compteur-2;
--          end if;
--        end if;

        if Ptr_Indice(A)-Ptr_Indice(A-1)-1=15 then
		  if Legende(1..15)="IMAGE_ECRITURES" then
            ImaEcr_Index:=Index;
			if compteur=4 then  -- presence des coordonnees du coin inf.gauche
              Get(Ligne(Ptr_Indice(A)+1..Ptr_Indice(A+1)-1),ImaEcr_CoinInfG.Coor_X,Last);
              Get(Ligne(Ptr_Indice(A+1)+1..Ptr_Indice(A+2)-1),ImaEcr_CoinInfG.Coor_Y,Last);
			  Compteur:=Compteur-2;
            else -- pas de coordonnees presentes : par defaut, on prend celles de l'image a creer
			  ImaEcr_CoinInfG.Coor_X:=tso.coor_X;
			  ImaEcr_CoinInfG.Coor_Y:=tso.coor_y;
            end if;
          end if;
        end if;

        couleur:=ColorMap(index,1)+256*ColorMap(index,2)+256*256*ColorMap(index,3);
		if couleur<0 then
          msg_erreur:=to_unbounded_string("La couleur de l'indexe"&integer'image(index)&" invoqu� � la ligne"&integer'image(K)&" du fichier"
 		              &eol&nomfichier_empilement(1..Ncar_fe)&eol
		              &"n'est pas d�finie dans le fichier palette"&eol&nomfichier_palette(1..ncar_fp));
		  k:=-1;
		  Ligne(1..1):="plante!";
        end if;


	    if appui then
          declare
            Tab_symboles : Symbole_liste_type(1..Nb_symboles);
          begin  
            LireSymbolesPonct(nomfichier_symboles(1..ncar_fs), InvEchelle, Resolution, Tab_symboles, Nb_symboles);

            for i in 1..tbnomi'last-NbNoRou loop
    
              -- if tbnomi(i).TOPO.code=Legende then

              if tbnomi(i).indice_symbole>=1 then
                if Tab_symboles(tbnomi(i).indice_symbole).Nom=Legende then
                  -- symboles circulaires
                  if tbnomi(i).TOPO.objet_type/=zonal then
                    Points_De_Cercle(i,tbnomi,nc,contpt,iest,inord,iouest,isud,true);
                    -- code:=1;--- 
                    put_line(ficvecteur,integer'image(code));        
                    put_line(ficvecteur,"POLYGONE");
		            Epaisseur:=1;
                    put_line(ficvecteur,integer'image(epaisseur)&tab&integer'image(stylePlein)&tab&integer'image(couleur));
                    put_line(ficvecteur,integer'image(E((float(nc)-1.0)/3.0+1.0)));
                    j:=1;
                    while j<=nc loop
                      M:=Convert_coord_graphe_terrain(contpt(j),InvEchelle,Resolution,Minimum_terrain);
                      put(ficvecteur,M.coor_x,0,2,0);
                      put(ficvecteur,tab);
                      put(ficvecteur,M.coor_y,0,2,0);
                      new_line(ficvecteur);
                      j:=j+3;
                    end loop;
                  end if;

                  -- polygones et symboles non circulaires
                  -- if tbnomi(i).TOPO.objet_type=zonal then

                  -- symboles non circulaires
                  if tbnomi(i).TOPO.objet_type=zonal then
                    n:=0;
                    nba:= Gr_face(gros,tbnomi(i).topo.num_objet).Nombre_arc;
                    tarcs(1..nba):= Gr_arcs_de_Face(gros,tbnomi(i).topo.num_objet);
                    for a in 1..nba loop
                      if tarcs(a)<0 then
                        Gr_Points_d_arc(gros,-tarcs(a),contptp,nc);
                        jj:=nc;
                        for k in 1..nc/2 loop
                          ptemp:=contptp(k);
                          contptp(k) :=contptp(jj);
                          contptp(jj):=ptemp;
                          jj:=jj-1;
                        end loop;
                      else
                        Gr_Points_d_arc(gros,tarcs(a),contptp,nc);
                      end if;
                      for j in 1..nc loop
				        contpt(n+j):= contptp(j);
				      end loop;
                      n:=n+nc;
                      -- if tbnomi(i).indice_symbole=0 then
                      --   code:=2;
                      -- else
                      --   code:=1;
                      -- end if;          
                      put_line(ficvecteur,integer'image(code));        
                      put_line(ficvecteur,"POLYGONE");
                      Epaisseur:=1;
                      put_line(ficvecteur,integer'image(epaisseur)&tab&integer'image(stylePlein)&tab&integer'image(couleur));
                      put_line(ficvecteur,integer'image(nc));
                      for j in 1..nc loop
                        M:=Convert_coord_graphe_terrain(contptp(j),InvEchelle,Resolution,Minimum_terrain);          
                        put(ficvecteur,M.coor_x,0,2,0);
                        put(ficvecteur,tab);
                        put(ficvecteur,M.coor_y,0,2,0);
                        new_line(ficvecteur);
                      end loop;  
                    end loop;
                  end if;
                end if;
			  end if;
            end loop;
	      end;
        end if;

        -- routes, courbes de niveau, ocs, hydro, etc, symboles ponctuels sans toponyme, ...
        if route=true then
          declare
            rtarcs : Liens_array_type(1..gr_infos(grr).Nb_arcs);
            lista : Point_liste_type(1..gr_infos(grr).max_pts_arc);    
            ia,ip,na : integer;
            arc : arc_type;
            br : boite_type;
          begin
  	        Plageno:=convert_coord_terrain_graphe(Vno,InvEchelle,Resolution,Minimum_terrain);
            Plagese:=convert_coord_terrain_graphe(Vse,InvEchelle,Resolution,Minimum_terrain);
 	        br.minimum:=(plageno.coor_x,plagese.coor_y);
            br.maximum:=(plagese.coor_x,plageno.coor_y);
            Gr_arcs_in_rectangle(grr,br.minimum,br.maximum,rtarcs,na);
            -- code:=3;
            if na>0 then
              for ia in 1..na loop
                arc:=Gr_arc(grr,rtarcs(ia));
                if gr_legende_l(legr,arc.att_graph).Theme=legende then
                  Gr_points_d_arc(grr,rtarcs(ia),lista,n);
                  -- arc ouvert de largeur nulle (ce cas n'est pas sens� se pr�senter)
			  	  if gr_legende_l(legr,arc.att_graph).Largeur=0.0 and lista(1)/=lista(N) then
                    -- on passe � l'arc suivant
					goto finBoucleIa;
                  end if;
                  put_line(ficvecteur,integer'image(code));
				  if gr_legende_l(legr,arc.att_graph).Largeur=0.0 then -- and lista(1)=lista(N) then
                    put_line(ficvecteur,"POLYGONE");
			        styligne:=6;
	              else
                    put_line(ficvecteur,"LIGNE");
                    styligne:=0;
                  end if;
  			      x:=gr_legende_l(legr,arc.att_graph).Largeur*float(ppmm);
			      if x-float(ent(x))<float(Ent(x+1.0))-X then 
                    Epaisseur:=max(ent(X),1);
	              else
                    Epaisseur:=max(ent(X+1.0),1);
                  end if;
                  put_line(ficvecteur,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
                  put_line(ficvecteur,integer'image(n));
                  for ip in 1..n loop
                    M:=Convert_coord_graphe_terrain(lista(ip),InvEchelle,Resolution,Minimum_terrain);
                    put(ficvecteur,M.coor_x,0,2,0);
                    put(ficvecteur,tab);
                    put(ficvecteur,M.coor_y,0,2,0);
                    new_line(ficvecteur);
                  end loop;
                end if;
				<< finBoucleIa >>
				null;
              end loop;  -- boucle en Ia
            end if;
          end;
        end if;

      end loop; 
	  k:=k+1;
    end loop;

    Close(FEmpile);

  exception when others => 
    if k=0 then
      Msg_erreur:=to_unbounded_string("Impossible d'ouvrir le fichier"&eol&nomfichier_empilement(1..ncar_fe));
    else
	  if k>0 then
        Msg_Erreur:=to_unbounded_string("Erreur de lecture � la ligne"&integer'image(k)&" du fichier"&eol&nomfichier_empilement(1..ncar_fe));
      end if;
    end if;
	raise;
  end;
end export_creMutil;


-- cree un fichier du vecteur au format Viewer pour le module de saisie des positions des ecritures horizontales sans position
procedure exportvecviewerm_Horiz(tbnomi : in typtbnomi; ficviewer : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; tnp : in typtnp; ps, pe, cs, ce, ncs, nce, sse, rs : in boolean) is
-- polygone symbole, polygone ecriture, circulaire symbole, circulaire ecriture
-- non circulaire symbole, non circulaire ecriture, sans symbole ecriture, routes symboles

code : integer;
symboleviewer : integer range 0..7; -- 7=pas de symbole
couleur : integer;
M,Plusud : Point_type_reel;
S : Point_type;
epaisseur : integer:=2;
styligne : integer range 0..4;  -- 0=continue 1=tiret�e
n,nc,nba,jj : integer;
ptemp : point_type;
nb_pts_max:integer:=pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
contpt,contptp : point_liste_type(1..nb_pts_max);
tarcs : liens_array_type(1..infoos.max_arc_fac); 
c : character;
iest,inord,iouest,isud : natural;
j : integer;
info_font: typtfont(1..500);
nbfont : integer;
k :integer;
signe : integer range -1..1;
IIdeb : integer;

begin

  charge_fontes(info_font,nbfont);
        
  for i in 1..tbnomi'last
  	loop
    if tnp(I)/=0 or tbnomi(I).coord_objet.coor_x<Pso.coor_x or tbnomi(I).coord_objet.coor_X>Pne.coor_x
			     or tbnomi(I).coord_objet.coor_Y<Pso.coor_y or tbnomi(I).coord_objet.coor_y>Pne.coor_y
				 then
	  goto finboucle;
    end if;
    Code:=tbnomi(I).id;

    -- symboles circulaires
    if tbnomi(i).TOPO.objet_type/=zonal and tbnomi(i).indice_symbole>=1 then
      if cs=true then
        Points_De_Cercle(i,tbnomi,nc,contpt,iest,inord,iouest,isud,true);
        put_line(ficviewer,integer'image(code));        
        put_line(ficviewer,"LIGNE");
        couleur:=6532376; -- vert
        -- couleur:=16744576; -- bleu profond
        styligne:=0;
        put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(ficviewer,integer'image(E((float(nc)-1.0)/3.0+1.0)+1));
        j:=1;
        while j<=nc loop
          M:=Convert_coord_graphe_terrain(contpt(j),InvEchelle,Resolution,Minimum_terrain);          
          put(ficviewer,M.coor_x,0,2,0);
          put(ficviewer,tab);
          put(ficviewer,M.coor_y,0,2,0);
          new_line(ficviewer);
          j:=j+3;
        end loop;  
        M:=Convert_coord_graphe_terrain(contpt(1),InvEchelle,Resolution,Minimum_terrain);          
        put(ficviewer,M.coor_x,0,2,0);
        put(ficviewer,tab);
        put(ficviewer,M.coor_y,0,2,0);
        new_line(ficviewer);
      end if;
    end if;
    
    -- polygones et symboles non circulaires
    if tbnomi(i).TOPO.objet_type=zonal then
      if (tbnomi(i).indice_symbole=0 and ps=true) or (tbnomi(i).indice_symbole>=1 and ncs=true) then
        Plusud:=(0.0,float'last);
        n:=0;
        nba:= Gr_face(gros,tbnomi(i).topo.num_objet).Nombre_arc;
        tarcs(1..nba):= Gr_arcs_de_Face(gros,tbnomi(i).topo.num_objet);
        for a in 1..nba loop
          if tarcs(a)<0 then
            Gr_Points_d_arc(gros,-tarcs(a),contptp,nc);
            jj:=nc;
            for k in 1..nc/2 loop
              ptemp:=contptp(k);
              contptp(k) :=contptp(jj);
              contptp(jj):=ptemp;
              jj:=jj-1;
            end loop;
          else
            Gr_Points_d_arc(gros,tarcs(a),contptp,nc);
          end if;
          for j in 1..nc loop contpt(n+j):= contptp(j); end loop;
          n:= n+nc;
          put_line(ficviewer,integer'image(code));        
          put_line(ficviewer,"LIGNE");
          couleur:=6532376; -- vert
          -- couleur:=16744576; -- bleu profond
          styligne:=0;
          put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(ficviewer,integer'image(nc));
          for j in 1..nc loop
            M:=Convert_coord_graphe_terrain(contptp(j),InvEchelle,Resolution,Minimum_terrain);          
            if M.coor_y<Plusud.coor_y then
              Plusud:=M;
            end if;
            put(ficviewer,M.coor_x,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M.coor_y,0,2,0);
            new_line(ficviewer);
          end loop;  
        end loop;
      end if;
    end if;

    << finboucle >>
    null;
  end loop;
end exportvecviewerm_horiz;


-- cree un fichier du vecteur au format Viewer pour le module de saisie des positions des num�ros de routes sans position
procedure exportvecviewerm_NoRou(tbnomi : in typtbnomi; ficviewer : File_type; InvEchelle : in integer;
                     Resolution : in integer; Minimum_terrain : in point_type_reel;
                     Nb_symboles : in integer; tnp : in typtnp; T_MT : in out TMT;
                     T_TRS : in out TTRONCON; T_ARCS : in TARCS;
                     ps, pe, cs, ce, ncs, nce, sse, rs : in boolean) is
-- polygone symbole, polygone ecriture, circulaire symbole, circulaire ecriture
-- non circulaire symbole, non circulaire ecriture, sans symbole ecriture, routes symboles

code : integer;
symboleviewer : integer range 0..7; -- 7=pas de symbole
couleur : integer;
M,Plusud : Point_type_reel;
S : Point_type;
epaisseur : integer:=2;
styligne : integer range 0..4;  -- 0=continue 1=tiret�e
n,nc,nba,jj : integer;
ptemp : point_type;
nb_pts_max:integer:=pas_maille*(infoos.max_arc_fac+5)*(infoos.max_pts_arc+5);
contpt,contptp : point_liste_type(1..nb_pts_max);
tarcs : liens_array_type(1..infoos.max_arc_fac); 
c : character;
iest,inord,iouest,isud : natural;
j : integer;
info_font: typtfont(1..500);
nbfont : integer;
k :integer;
pts_arc : point_liste_type(1..gr_infos(grr).max_pts_arc);
pts_meta : point_liste_type(1..10*gr_infos(grr).max_pts_arc);
signe : integer range -1..1;
IIdeb : integer;
M1,M2,M3 : Point_type_reel;
M1M2 : float;
l : integer;

begin

  charge_fontes(info_font,nbfont);
        
  for i in 1..tbnomi'last
  	loop

    if tnp(I)/=0 or tbnomi(I).coord_objet.coor_x<Pso.coor_x or tbnomi(I).coord_objet.coor_X>Pne.coor_x
			     or tbnomi(I).coord_objet.coor_Y<Pso.coor_y or tbnomi(I).coord_objet.coor_y>Pne.coor_y
			     or tbnomi(I).Topo.Mode_placement=CDesaxe or tbnomi(I).Topo.Mode_placement=CAxe then
	  goto finboucle;
    end if;
  
    Code:=tbnomi(I).id;

    -- debut gestion du caract�re '%' pour kilometrages
    l:=tbnomi(I).ncar(1);
    for K in 2..tbnomi(I).ncar(1)-1 loop
      if tbnomi(I).chaine(1)(K)='%' then
		l:=k-1;
		exit;
      end if;
    end loop;
	-- fin gestion du caract�re '%' pour kilometrages

    -- meta-troncons de routes
    if tbnomi(i).TOPO.objet_type=lineaire and (Tbnomi(I).cause/=MetaCourt or l/=tbnomi(I).ncar(1)) then
    -- if tbnomi(i).TOPO.objet_type=lineaire then

      IIdeb:=T_TRS(T_MT(-code).t_DEB).A_DEB;
      for iI in T_TRS(T_MT(-code).t_DEB).A_DEB..T_TRS(T_MT(-code).t_FIN).A_FIN loop	
	    Gr_Points_d_arc(grr,abs(t_ARCS(II).N_ARC),Pts_arc,nc);
        int_io.put(ficviewer,code,0);
		put(ficviewer,".");
        int_io.put(ficviewer,integer(II-IIdeb+1),0);
        new_line(ficviewer);
        put_line(ficviewer,"LIGNE");
        couleur:=16744576; -- bleu profond
        styligne:=0;
        put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(ficviewer,integer'image(nc));
        for j in 1..nc loop
          M:=Convert_coord_graphe_terrain(Pts_Arc(j),InvEchelle,Resolution,Minimum_terrain);          
          put(ficviewer,M.coor_x,0,2,0);
          put(ficviewer,tab);
          put(ficviewer,M.coor_y,0,2,0);
          new_line(ficviewer);
        end loop;

        -- ajout d'une fl�che jaune de 0.5 cm indiquant l'amont pour les cotes sur courbes de niveau orientees amont � droite
        if tbnomi(i).TOPO.mode_placement=COURBE and AMONT_A_DROITE=true then
		  if (2*iI=T_TRS(T_MT(-code).t_DEB).A_DEB+T_TRS(T_MT(-code).t_FIN).A_FIN
		   or 2*iI+1=T_TRS(T_MT(-code).t_DEB).A_DEB+T_TRS(T_MT(-code).t_FIN).A_FIN) then
            int_io.put(ficviewer,code,0);
	        Put(ficviewer,".");
            int_io.put(ficviewer,integer(T_TRS(T_MT(-code).t_FIN).A_FIN-IIdeb+2),0);
            new_line(ficviewer);
            put_line(ficviewer,"LIGNE");
            couleur:=65535; -- jaune
            styligne:=0;
            put_line(ficviewer,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
            put_line(ficviewer,integer'image(6));
            M1:=Convert_coord_graphe_terrain(Pts_Arc(Ent(float(Nc)/2.0)),InvEchelle,Resolution,Minimum_terrain);          
            M2:=Convert_coord_graphe_terrain(Pts_Arc(Ent(float(Nc)/2.0)+1),InvEchelle,Resolution,Minimum_terrain);          
            put(ficviewer,M1.coor_X,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M1.coor_y,0,2,0);
            new_line(ficviewer);
			M1M2:=norme(M1.coor_x,M1.coor_Y,M2.coor_x,M2.coor_Y);
			M3.coor_X:=M1.coor_X+(M2.coor_y-M1.coor_y)*(float(InvEchelle)*0.005)/M1M2;
            M3.coor_Y:=M1.coor_y-(M2.coor_X-M1.coor_X)*(float(InvEchelle)*0.005)/M1M2;
            put(ficviewer,M3.coor_X,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M3.coor_Y,0,2,0);
            new_line(ficviewer);
            put(ficviewer,M3.coor_X+(M2.coor_X-M1.coor_X)*(float(InvEchelle)*0.0005)/M1M2,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M3.coor_y+(M2.coor_Y-M1.coor_Y)*(float(InvEchelle)*0.0005)/M1M2,0,2,0);
            new_line(ficviewer);
            put(ficviewer,M1.coor_X+(M2.coor_y-M1.coor_y)*(float(InvEchelle)*0.006)/M1M2,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M1.coor_y-(M2.coor_X-M1.coor_X)*(float(InvEchelle)*0.006)/M1M2,0,2,0);
            new_line(ficviewer);
            put(ficviewer,M3.coor_X-(M2.coor_X-M1.coor_X)*(float(InvEchelle)*0.0005)/M1M2,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M3.coor_y-(M2.coor_Y-M1.coor_Y)*(float(InvEchelle)*0.0005)/M1M2,0,2,0);
            new_line(ficviewer);
            put(ficviewer,M3.coor_X,0,2,0);
            put(ficviewer,tab);
            put(ficviewer,M3.coor_Y,0,2,0);
            new_line(ficviewer);
          end if;
        end if;
      end loop;


    end if;

    << finboucle >>
    null;
  end loop;
end exportvecviewerm_NoRou;

  
-- renvoie true si dans la bordure interdite
function dansbordinter(xpix,ypix : in integer; pastestinterb : in integer; ko : in integer;
                       file_image	: in byte_io.file_type; lps,ncol,nlig : in integer;
                       stripoffset : in acces_tabstrip; nbindexintertous : in integer;
                       tabindexintertous : in acces_typtabindextous) return boolean is

k : integer;
int : boolean;
xpixc, ypixc : integer; -- coordonnes du pixel courant
nbdirpkint : integer:=0; -- nbre de directions dans lesquelles il n'y a pas que de l'interdit
c : character;
Val : integer;
begin
  
  nbdirpkint:=0;
  k:=1;
  while (xpix+k*pastestinterb)<=ncol and k<=ko loop
    int:=false;
    xpixc:=xpix+k*pastestinterb;
	Val:=integer(valindex(xpixc,ypix,file_image,lps,ncol,stripoffset));
    for i in 1..nbindexintertous loop
      if tabindexintertous(i)=Val then
        int:=true;
        exit;
      end if;
    end loop;
    if int=false then
      nbdirpkint:=nbdirpkint+1;
      exit;
    end if;
    k:=k+1;
  end loop;
  
  k:=-1;
  while (xpix+k*pastestinterb)>=1 and k>=-ko loop
    int:=false;
    xpixc:=xpix+k*pastestinterb;
	val:=integer(valindex(xpixc,ypix,file_image,lps,ncol,stripoffset));
    for i in 1..nbindexintertous loop
      if tabindexintertous(i)=Val then
        int:=true;
        exit;
      end if;
    end loop;
    if int=false then
      nbdirpkint:=nbdirpkint+1;
      if nbdirpkint=2 then
        return(false);
      end if;
      exit;
    end if;
    k:=k-1;
  end loop;
  
  k:=1;
  while (ypix+k*pastestinterb)<=nlig and k<=ko loop
    int:=false;
    ypixc:=ypix+k*pastestinterb;
	val:=integer(valindex(xpix,ypixc,file_image,lps,ncol,stripoffset));
    for i in 1..nbindexintertous loop
      if tabindexintertous(i)=val then
        int:=true;
        exit;
      end if;
    end loop;
    if int=false then
      nbdirpkint:=nbdirpkint+1;
      if nbdirpkint=2 then
        return(false);
      end if;
      exit;  
    end if;  
    k:=k+1;
  end loop;
  
  k:=-1;
  while (ypix+k*pastestinterb)>=1 and k>=-ko loop
    int:=false;
    ypixc:=ypix+k*pastestinterb;
	val:=integer(valindex(xpix,ypixc,file_image,lps,ncol,stripoffset));
    for i in 1..nbindexintertous loop
      if tabindexintertous(i)=val then
        int:=true;
        exit;
      end if;
    end loop;
    if int=false then
      nbdirpkint:=nbdirpkint+1;
      if nbdirpkint=2 then
        return(false);
      end if;
      exit;
    end if;  
    k:=k-1;
  end loop;  
  return(true);
end dansbordinter;

-- calcul du nombre de valeurs d'index ayant un poids egal a valindextous pour tous les codes typo
function calcnbindextous(tsubst : in acces_tabsub; nb_interp : in integer; valindextous : in integer) return integer is
nb : integer:=0;
sortie : boolean;
begin
  for i in 0..255 loop
  sortie:=false;
    for j in 1..nb_interp loop
      if tsubst.all(i,j)/=valindextous then
        sortie:=true;
        exit;
      end if;
    end loop;
    if sortie=false then
      nb:=nb+1;
    end if;
  end loop;
  return(nb);
end calcnbindextous;

-- calcul des valeurs d'index ayant un poids egal a valindextous pour tous les codes typo
procedure calcindextous(tsubst : in acces_tabsub; nb_interp : in integer; valindextous : in integer; nbindextous : in integer; tabindextous : out acces_typtabindextous) is
nb : integer :=0;
sortie : boolean;
begin
  tabindextous := new typtabindextous (1..nbindextous);
  tabindextous.all := (others => 0);
  for i in 0..255 loop
    sortie:=false;
    for j in 1..nb_interp loop
      if tsubst.all(i,j)/=valindextous then
        sortie:=true;
        exit;
      end if;
    end loop;
    if sortie=false then
      nb:=nb+1;
      tabindextous(nb):=i;
    end if;
    if nb=nbindextous then
      exit;
    end if;
  end loop;
end calcindextous;

-- lit une valeur de pixel dans l'image  
function valindex(xpix,ypix : in integer; file_image	: in byte_io.file_type;
                  lps,ncol : in integer; stripoffset : in acces_tabstrip) return byte is

val : byte;
numstrip : integer;
pos: byte_io.positive_count;

begin
  numstrip:=((ypix-1)/lps)+1;
  
  begin
    pos:=byte_io.positive_count(stripoffset.all(numstrip)+(((ypix-1) mod lps)*ncol)+xpix);
    pos:=byte_io.positive_count(stripoffset.all(numstrip)+(((ypix-1) mod lps)*ncol)+xpix);
    exception when Event : others =>
      Msg_Erreur:=To_Unbounded_String("Erreur de lecture de l'image:"&eol&nomfichier_image(1..ncar_fi));
    raise;
  end;
                                             
  byte_io.set_index(file_image, pos);
  byte_io.read (file_image, val);
  return(val);
end valindex;

-- ecriture des fichiers paramviewe et _viewe
procedure ficview(tbnomi: in typtbnomi; InvEchelle : in integer; Resolution : in integer; Minimum_terrain : in point_type_reel;
                  Nb_symboles : in integer; Resol_Mut_reel : in float;
                  mispo : in out boolean; miobs : in out boolean; miecf : in out boolean) is

use type gb.bool;

ficviewe : File_type; -- fichier contenant toutes les donn�es d'entree
nomficviewe : string120; -- nom du fichier contenant toutes les donn�es d'entree
ficecrie : File_type; -- fichier contenant toutes les ponctuels d'entree a afficher sur fond blanc
nomficecrie : string120; -- nom du fichier contenant les ponctuels d'entree a afficher sur fond blanc
ficparamviewe : File_type; -- fichier des parametres de visualisation des donn�es d'entree
nomficparamviewe : string(1..tabncrdir(nbdir,2)+10):=nomfichier_PAT(1..tabncrdir(nbdir,2))&"paramviewe";
Tab_symboles : Symbole_liste_type(1..Nb_symboles);
  
begin

  nomficviewe:=(others => gb.NullChar);
  nomficviewe(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomficviewe(ncar_fo-3..ncar_fo+2):="_viewe";
  create(ficviewe,Out_File,nomficviewe,"");

  nomficecrie:=(others => gb.NullChar);
  nomficecrie(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomficecrie(ncar_fo-3..ncar_fo+2):="_ecrie";
  create(ficecrie,Out_File,nomficecrie,"");
  
  exportvecviewer(tbnomi,ficviewe,Ficecrie,InvEchelle,Resolution,Minimum_terrain,Nb_symboles,Resol_Mut_reel,true,true,true,true,true,true,true,true,mispo,miobs,miecf);

  close(ficviewe);
  close(ficecrie);

  -- ecriture du fichier de parametres pour VisuEntree
  create(ficparamviewe,Out_File,nomficparamviewe,"");
  if mutil then
    put(ficparamviewe,nomfichier_image(1..ncar_fi));
    new_line(ficparamviewe);
  end if;
  if nomdirexe(1)='"' then
    put(ficparamviewe,nomdirexe(2..ncar_nde)&"\fond.jpg");
  else
    put(ficparamviewe,nomdirexe(1..ncar_nde)&"\fond.jpg");
  end if;
  new_line(ficparamviewe);
  put(ficparamviewe,nomficviewe(1..ncar_fo+2));
  new_line(ficparamviewe);
  if mutil then
    put(ficparamviewe,nomficecrie(1..ncar_fo+2));
    new_line(ficparamviewe);
    put(ficparamviewe,tno.coor_x,0,2,0);
    new_line(ficparamviewe);
    put(ficparamviewe,tno.coor_y,0,2,0);
    new_line(ficparamviewe);
    put(ficparamviewe,float(InvEchelle)/(float(Resol_Mut)*1000.0),0,5,0);
  else
  	put(ficparamviewe,"0.0");
    new_line(ficparamviewe);
  	put(ficparamviewe,"0.0");
    new_line(ficparamviewe);
  	put(ficparamviewe,"0.0");
  end if;
  new_line(ficparamviewe);
  put(ficparamviewe,Vno.coor_x,0,2,0);
  new_line(ficparamviewe);
  put(ficparamviewe,Vno.coor_y,0,2,0);
  new_line(ficparamviewe);  
  put(ficparamviewe,float(InvEchelle)/(Resol_Mut_reel*1000.0),0,5,0);    
  new_line(ficparamviewe);
  put(ficparamviewe,boolean'image(mispo));
  new_line(ficparamviewe);
  put(ficparamviewe,boolean'image(miobs));
  new_line(ficparamviewe);
  put(ficparamviewe,boolean'image((nb_codes_route>=1) or (gb.Checked(dialog1.NumRouCheckBox)=gb.true) or (gb.Checked(dialog1.CotesCourbesCheckBox)=gb.true)));
  new_line(ficparamviewe);
  put(ficparamviewe,boolean'image(miecf));
  close(ficparamviewe);
end;

-- procedure principale du package
procedure verifmutil(tbnomi: in typtbnomi; tsubst : in acces_tabsub; nb_interp : in integer;
                     InvEchelle : in integer; Resolution : in integer; Minimum_terrain : in point_type_reel;
                     file_image	: in out byte_io.file_type;
                     lps,ncol,nlig: in integer; stripoffset: in tiff_io.acces_tabstrip;
                     Nb_symboles : in integer; tabstat : in out typtabstat;
                     verdict : out typverdict) is

a : integer;
nbindexintertous : integer := calcnbindextous(tsubst,nb_interp,10);
tabindexintertous : acces_typtabindextous;
nbindexpoinultous : integer := calcnbindextous(tsubst,nb_interp,0);
tabindexpoinultous : acces_typtabindextous;
val : byte;
xpix,ypix : integer;
c : character;
estint,estfond : boolean;
pasmm : integer :=1; -- pas en mm carte pour le test de bordure interdite
paspix : integer; -- pas en pixels pour le test de bordure interdite
ko : integer :=15;
totalinter : integer:=0;
totalbinter : integer:=0;
totalpinter : integer:=0;
totalfond : integer:=0;
pourcent,pourcentI,pourcentQ: integer;
Tab_symboles : Symbole_liste_type(1..Nb_symboles);
Tabinfosymboles : typtabinfosymboles(1..Nb_symboles,feracheval..rayon) := (others=>(others=>-1));
Tabpropmin : typtabpropmin(1..Nb_symboles) := (others=>0.0);
ficstat : File_type;
nomficstat : string120;
symbole : surf_type(1,300);
ptori : point_type:=(0,0);
ficviewe : File_type; -- fichier contenant toutes les donn�es d'entree
nomficviewe : string120; -- nom du fichier contenant toutes les donn�es d'entree
ficparamviewe : File_type; -- fichier des parametres de visualisation des donn�es d'entree
-- nomficparamviewe : string(1..ncar_nde+11):=nomdirexe(1..ncar_nde)&"\paramviewe"; -- nom du fichier des parametres de visualisation des donn�es d'entree
nomficparamviewe : string(1..tabncrdir(nbdir,2)+10):=nomfichier_PAT(1..tabncrdir(nbdir,2))&"paramviewe";
M : Point_type_reel;
veriffaisable : boolean:=false; -- passe a true s'il y a au moins une categorie de symbole testable
doute : boolean:=true; -- passe a false des qu'une categorie de symbole aura pourcentI>80
mispo : boolean:=false; -- presence de symboles ponctuels
miobs : boolean:=false; -- presence d'objets surfaciques
miecf : boolean:=false; -- presence d'ecritures floues
jo : integer;

begin
    
  verdict:=passe;
  
  if Nb_symboles=0 then
    verdict:=pasfait1;
    --- goto ficview;
    return;
  end if;

  if nbindexintertous=0 then
    verdict:=pasfait2;
    --- goto ficview;
    return;
  end if;
   
  calcindextous(tsubst,nb_interp,10,nbindexintertous,tabindexintertous);
  calcindextous(tsubst,nb_interp,0,nbindexpoinultous,tabindexpoinultous);
  LireSymbolesPonct(nomfichier_symboles(1..ncar_fs),InvEchelle,Resolution,Tab_symboles,Nb_symboles);

  for i in 1..Nb_symboles loop
    if Tab_symboles(i).Nb_pts>=2 then
      symbole.PTS:=Tab_symboles(i).Tab_coord; -- (point_liste_type(1..300)
      symbole.NE:=1;
      symbole.NA(1):=Tab_symboles(i).Nb_pts;      
      if Est_il_dans_surface(ptori,symbole)=false then
        Tabinfosymboles(i,feracheval):=1;
      else
        Tabinfosymboles(i,feracheval):=0;
      end if;
    end if;    
  end loop;  

  for i in 1..tbnomi'last-NbNoRou loop
    if tbnomi(i).indice_symbole=0 then
      goto finfor;       
    end if;
    if tbnomi(i).coord_objet.coor_x<=pso.coor_x or tbnomi(i).coord_objet.coor_x>=pne.coor_x
      or tbnomi(i).coord_objet.coor_y<=pso.coor_y or tbnomi(i).coord_objet.coor_Y>=pne.coor_Y then
      goto finfor;
    end if;
    
    if Tabpropmin(tbnomi(i).indice_symbole)=0.0 then
      Tabpropmin(tbnomi(i).indice_symbole):=tbnomi(i).prop;
    else
      if tbnomi(i).prop<Tabpropmin(tbnomi(i).indice_symbole) then
        Tabpropmin(tbnomi(i).indice_symbole):=tbnomi(i).prop;
      end if;
    end if;
    
    xpix:=(tbnomi(i).coord_objet.coor_x-pso.coor_x)/pt_image+1;
    ypix:=delta_ypix-((tbnomi(i).coord_objet.coor_y-pso.coor_y)/pt_image);
	if Xpix<1 or Xpix>Ncol or Ypix<1 or Ypix>Nlig then 
      goto finfor;
    end if;

    val:=valindex(xpix,ypix,file_image,lps,ncol,stripoffset);
    
    estint:=false;
    for j in 1..nbindexintertous loop
      if integer(val)=tabindexintertous(j) then
        estint:=true;
        exit;
      end if;
    end loop;

    estfond:=false;
    for j in 1..nbindexpoinultous loop
      if integer(val)=tabindexpoinultous(j) then
        estfond:=true;
        tabstat(tbnomi(i).indice_symbole,fond):=tabstat(tbnomi(i).indice_symbole,fond)+1;
		-- gb.msgbox(Tab_symboles(tbnomi(i).indice_symbole).nom&"Fond"&integer'image(xpix)&integer'image(Ypix));
        exit;
        end if;
    end loop;
    
    if estint=false and estfond=false then
      tabstat(tbnomi(i).indice_symbole,pinter):=tabstat(tbnomi(i).indice_symbole,pinter)+1;
      -- gb.msgbox(Tab_symboles(tbnomi(i).indice_symbole).nom&"Quelconque"&integer'image(xpix)&integer'image(Ypix));
    end if;
      
    if estint=true then 
      paspix:=max((pasmm*Resol_Mut),1);   -- Resolution/pt_image),1);
      if dansbordinter(xpix,ypix,paspix,ko,file_image,lps,ncol,nlig,stripoffset,
                       nbindexintertous,tabindexintertous)=true then
        tabstat(tbnomi(i).indice_symbole,binter):=tabstat(tbnomi(i).indice_symbole,binter)+1;
      else
        tabstat(tbnomi(i).indice_symbole,inter):=tabstat(tbnomi(i).indice_symbole,inter)+1;
      end if;
    end if;
    << finfor >>
    null;
  end loop;
  
  -- calcul des rayons en d�cimillim�tres
  for i in 1..Nb_symboles loop
    if Tab_symboles(i).Nb_pts>=1 then
      -- Tabinfosymboles(i,rayon):=integer(Distance_a_polylignes(ptori,Tab_symboles(i).Tab_coord,Tab_symboles(i).Nb_pts,C_segment_type)*Tabpropmin(i)/float(InvEchelle)*10000.0);
      Tabinfosymboles(i,rayon):=integer(Distance_a_polylignes(ptori,Tab_symboles(i).Tab_coord,Tab_symboles(i).Nb_pts,C_segment_type)*Tabpropmin(i)*10.0/float(resolution));
      if Tab_symboles(i).Nb_pts=1 then   -- symbole circulaire
        Tabinfosymboles(i,rayon):=integer(float(Tabinfosymboles(i,rayon))/sqrt(2.0));
      end if;
    end if;
  end loop;

------------------------  
  
  nomficstat:=(others => gb.NullChar);
  nomficstat(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomficstat(ncar_fo-3..ncar_fo+1):="_stat";
  create(ficstat,Out_File,nomficstat,"");  
  
  if nbindexpoinultous>=1 then
    put_line(ficstat,"image avec fond");
  else
    put_line(ficstat,"image sans fond");
  end if;
  new_line(ficstat);
  if (nbindexpoinultous>=1) then
    put_line(ficstat,"SYMBOLE"&tab&tab&tab&tab&tab&" I"
           &tab&" B"&tab
           &" F"&tab&" Q"&tab&tab&" I/I+F+Q"
           &tab&tab&" I+F+Q");
  else
    put_line(ficstat,"SYMBOLE"&tab&tab&tab&tab&tab&" I"
           &tab&" B"&tab
           &" Q"&tab&tab&" I/I+Q"
           &tab&tab&" I+Q");
  end if;
  new_line(ficstat,2);
  for i in 1..Nb_symboles loop
    totalinter:=totalinter+tabstat(i,inter);
    totalbinter:=totalbinter+tabstat(i,binter);
    totalpinter:=totalpinter+tabstat(i,pinter);
    totalfond:=totalfond+tabstat(i,fond);
    
    if (tabstat(i,inter)+tabstat(i,binter)+tabstat(i,pinter)+tabstat(i,fond))=0 then
      goto finfor2;
    end if;
    
    if (nbindexpoinultous>=1) then
      if (tabstat(i,inter)+tabstat(i,pinter)+tabstat(i,fond))/=0 then
        pourcentI:=integer(100*tabstat(i,inter)/(tabstat(i,inter)+tabstat(i,pinter)+tabstat(i,fond)));
        pourcentQ:=integer(100*tabstat(i,pinter)/(tabstat(i,inter)+tabstat(i,pinter)+tabstat(i,fond)));
      else
        pourcentI:=0;
        pourcentQ:=0;
      end if;
    else
      if (tabstat(i,inter)+tabstat(i,pinter))/=0 then
        pourcentI:=integer(100*tabstat(i,inter)/(tabstat(i,inter)+tabstat(i,pinter)));
      else
        pourcentI:=0;
      end if;          
    end if;

    if (nbindexpoinultous>=1) then
      put_line(ficstat,Tab_symboles(i).nom
             &tab&integer'image(tabstat(i,inter))
             &tab&integer'image(tabstat(i,binter))
             &tab&integer'image(tabstat(i,fond))
             &tab&integer'image(tabstat(i,pinter))
             &tab&tab&integer'image(pourcentI)&"%"
             &tab&tab&integer'image(tabstat(i,inter)+tabstat(i,pinter)+tabstat(i,fond)));
      if (tabstat(i,inter)+tabstat(i,fond)+tabstat(i,pinter))>=10 and Tabinfosymboles(i,rayon)>=1 and Tabinfosymboles(i,feracheval)/=1 then 
        veriffaisable:=true;
        if (pourcentI<80 and pourcentI>20) or (pourcentQ>=1 and tabstat(i,pinter)>=4) then
          verdict:=echoue;
          put_line(ficstat,"�chec");
        end if;
        if pourcentI>=80 then
          doute:=false;
        end if;
      end if;
    else
      put_line(ficstat,Tab_symboles(i).nom
             &tab&integer'image(tabstat(i,inter))
             &tab&integer'image(tabstat(i,binter))
             &tab&integer'image(tabstat(i,pinter))
             &tab&tab&integer'image(pourcentI)&"%"
             &tab&tab&integer'image(tabstat(i,inter)+tabstat(i,pinter)+tabstat(i,fond)));
      if (tabstat(i,inter)+tabstat(i,pinter))>=10 and Tabinfosymboles(i,rayon)>=1 and Tabinfosymboles(i,feracheval)/=1 then 
        veriffaisable:=true;
        if pourcentI<80 then
          verdict:=echoue;
          put_line(ficstat,"�chec");
        else
          doute:=false;
        end if;
      end if;
    end if;
    
    if Tab_symboles(i).Nb_pts=1 then
       put_line(ficstat,"circulaire");
    else
       put_line(ficstat,"non circulaire");
    end if;
    if Tabinfosymboles(i,feracheval)=1 then
       put_line(ficstat,"fer a cheval");
    end if;
    if Tabinfosymboles(i,feracheval)=0 then
       put_line(ficstat,"non fer a cheval");
    end if;         
    if Tabinfosymboles(i,rayon)/=-1 then
       put_line(ficstat,"diametre"&integer'image(2*Tabinfosymboles(i,rayon))&" dmm");
    end if;

    new_line(ficstat);
    << finfor2 >>
    null;
  end loop;
    
  new_line(ficstat,2);
  if (totalinter+totalpinter)/=0 then
    pourcent:=integer(100*totalinter/(totalinter+totalpinter));
  else
    pourcent:=0;    
  end if;
  if (nbindexpoinultous>=1) then
    put_line(ficstat,"TOTAL"&tab&tab&tab&tab&tab&integer'image(totalinter)&tab&integer'image(totalbinter)
               &tab&integer'image(totalfond)&tab&integer'image(totalpinter)&tab&tab&integer'image(pourcent)&"%"&tab&tab&integer'image(totalinter+totalpinter));       
  else
    put_line(ficstat,"TOTAL"&tab&tab&tab&tab&tab&integer'image(totalinter)&tab&integer'image(totalbinter)
               &tab&integer'image(totalpinter)&tab&tab&integer'image(pourcent)&"%"&tab&tab&integer'image(totalinter+totalpinter));       
  end if;
  new_line(ficstat,3);
  put_line(ficstat,"I = interdit");
  put_line(ficstat,"B = bandeau interdit");
  if (nbindexpoinultous>=1) then
    put_line(ficstat,"F = fond");
  end if;
  put_line(ficstat,"Q = quelconque");
  close(ficstat);

  if veriffaisable=false then
    verdict:=pasfait3;
  end if;

  if verdict=passe and doute=true then
    verdict:=pasfait4;
  end if;
  
end verifmutil;

end VERIFMUT;
