with geometrie; use geometrie;
with math_int_basic; use math_int_basic;
with ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;

Package body morpholigne is

----------------------------------------------------------------------------
----            TYPES ET CONSTANTES INTERNES AU PACKAGE                   --
----------------------------------------------------------------------------
--
---- Les types associes aux segments
------------------------------------
---- Dans ce Package, on appelle segment un arc represente soit par
---- un segment de droite, Soit par un arc de cercle et un Poly_segment
---- un ensemble de segments
--
--type Interpolation_type is (Cercle,Ligne);
--
--type Segment is record
--  Deb, Fin, Centre : Point_type_reel;
--  Rayon, Angle : float;
--  Interpolation : interpolation_type;
--end record;
---- les donnees sont redondantes mais utiles pour minimiser les erreurs de
---- calcul
---- Deb [resp. fin]: le premier [resp.dernier] point du segment, le segment
----                  est oriente de Deb vers Fin
---- Centre : si le segment est un arc de cercle, le centre de ce cercle
---- Rayon : si le segment est un arc de cercle, le rayon de ce cercle
---- Angle : si le segment est un arc de cercle, l'angle (Deb, Centre, Fin)
----         cet angle est dans [-2Pi,2Pi], positif si le segment est parcouru
----         dans le sens trigonometrique, negatif sinon
---- Interpolation : pour savoir si c'est un arc de cercle ou un segment de droite
--
--
--type Poly_segment is array (natural range <>) of Segment;
--
--type Poly_segment_access is access Poly_segment;
--
--Type Intersection is record
--  Seg : Segment;
--  Cardinal : Natural;
--end record;
---- Cardinal = 0 => intersection vide
---- Cardinal = 1 => intersection reduite a un point
---- Cardinal = 2 => intersection reduite a deux points
---- Cardinal = 3 => intersection egale a un arc de cercle ou un segment de
----                 droite
--
--
---- Un type pour regrouper un arc, la demi-largeur du symbole, LES arcs
---- representant les bords a droite et a gauche et leur parente
--type Arc_bords is record
--  Arc       : Point_access_reel;
--  Largeur   : float;
--  Bordg     : Point_access_reel;
--  Parentg   : Liens_access_type;
--  Bordd     : Point_access_reel;
--  Parentd   : Liens_access_type;
--end record;
--
--
--Pt_infini : constant Point_type_reel := (float'last,float'last);
--Seg_vide : constant Segment := (Pt_infini,Pt_infini,Pt_infini,
--                                float'last,float'last,Ligne);
--

--------------------------------------------------------------------------
--      OPERATEURS, FONCTIONS ET PROCEDURES INTERNES AU PACKAGE         --
--------------------------------------------------------------------------


------------------------------------------------
-- FREE_POLY_SEGMENT et FREE_FLAG
------------------------------------------------
-- FONCTION INTERNE
------------------------------------------------
-- La deallocation du pointeur sur un Poly_segment 
------------------------------------------------
procedure Free_Poly_segment is new Unchecked_deallocation(
                                        Poly_segment, Poly_segment_access);

-----------------------------------------
-- NORME2
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Renvoie la norme au carre d'un vecteur
-----------------------------------------
Function Norme2( M : in Point_type_reel) return float is

begin 
  return M.coor_X*M.coor_X + M.coor_Y*M.coor_Y;
end Norme2;


-----------------------------------------
-- Renvoie la norme d'un vecteur
-----------------------------------------
Function Norme( M : in Point_type_reel) return float is

begin
  return sqrt(M.coor_X*M.coor_X + M.coor_Y*M.coor_Y);
end Norme;


----------------------------
-- LONG_CURVI
----------------------------
-- Renvoie la longueur curviligne d'un arc
----------------------------
Function Long_curvi(Arc : Point_liste_reel) return float is

D : float := 0.0;

begin
  for i in Arc'first..Arc'last-1 loop
    D := D + Norme(Arc(i),Arc(i+1));
  end loop;
  return D;
end Long_curvi;

----------------------------
-- SURFACE
----------------------------
-- Renvoie la surface algebrique delimitee par un arc
----------------------------
Function Surface(Arc : Point_liste_reel) return float is

S : float := 0.0;

begin
  for i in Arc'first..Arc'last-1 loop
    S:=S+(Arc(i).coor_X-Arc(i+1).coor_X)*(Arc(i).coor_Y+Arc(i+1).coor_Y)*0.5;
  end loop;
  return S;
end Surface;

-------------------------------------------
-- TROP_LOIN
-------------------------------------------
function Trop_loin(Arc   : Point_liste_reel;
                   Pt    : Point_type_reel;
                   Seuil : float) return boolean is

begin
  for i in Arc'range loop
    if Norme2(Arc(i)-Pt)>seuil then
      return true;
    end if;
  end loop;
  return false;
end;


-----------------------------------------
-- ANGLE_ORIENTE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Retourne un angle dans [0,2Pi[
-- angle BA,BC dans le sens trigo
-----------------------------------------
Function Angle_oriente(BA, BC : in Point_type_reel) return float is 

cosinus, sinus : float;

begin -- Angle_oriente
 
  if BA = (0.0,0.0) or else BC = (0.0,0.0) then  
    return 2.0*Pi;
  end if;

  -- Cosinus de l'angle
  Cosinus := Scalaire(BA,BC)/Norme(BA)/Norme(BC);

  -- NB : la fonction ArcCOS plante pour les valeurs 1.0 et -1.0 !!!
  if (Cosinus >= 1.0) or else BA = BC then
    return 0.0;
  elsif (Cosinus <= -1.0) or else BA = (-1.0)*BC then
    return Pi;
  end if;

  Sinus := Vectoriel(BA,BC);  
  if (Sinus < 0.0) then  
    return (2.0*Pi-ArcCOS(Cosinus));
  else  
    return ArcCOS(Cosinus);
  end if;

end Angle_oriente;

-----------------------------------------------
-- ROTATION 
-----------------------------------------------
-- FONCTION INTERNE
-----------------------------------------------
-- Rotation de A autour de B d'un angle alpha
-- dans le sens trigonometrique
-----------------------------------------------
function Rotation(A, B :  point_type_reel; 
		  Alpha : float) 
                  return point_type_reel is

C : Point_type_reel;     
    
begin
  C.coor_x := B.coor_x+(A.coor_x-B.coor_x)*cos(alpha)-(A.coor_y-B.coor_y)*sin(alpha);
  C.coor_y := B.coor_y+(A.coor_x-B.coor_x)*sin(alpha)+(A.coor_y-B.coor_y)*cos(alpha);
  return C;
end Rotation;

-----------------------------------------
-- POINT_DANS_REGION	
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------  
-- Pour une droite retourne True si le projete orthogonal 
-- du point sur la droite est dans le segment False sinon
--
-- Pour un cercle retourne True si le point est dans la
-- portion de plan delimitee par les deux demi droites
-- [Centre Deb) et [Centre Fin) et contenant l'arc de cercle
-- False sinon
-----------------------------------------
Function Point_dans_region(Pt:point_type_reel;Seg:Segment) return boolean is

A, Ps: float;

begin
  if Seg.interpolation = Cercle then
    A := Angle_oriente(Seg.Deb-Seg.centre,Pt-Seg.centre);
    if (Seg.angle >= 0.0 and then A <= Seg.angle) or else 
       (Seg.angle <= 0.0 and then (A = 0.0 or else A >= 2.0*Pi+Seg.angle)) then
      return True;
    else
      return False;
    end if;
  else
    Ps := Scalaire(Pt-Seg.deb,Seg.fin-Seg.deb); 
    if Ps >= 0.0 and then Ps <= Norme2(Seg.deb-Seg.fin) then 
      return True;
    else 
      return False;
    end if;
  end if;
    
end Point_dans_region;

-----------------------------------------
-- INTERSECTION_LIGNE_LIGNE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Intersection entre deux segemnts de droite
-----------------------------------------
Function intersection_ligne_ligne(Seg1, Seg2 : Segment) return Intersection is

N,N1,N2,Sdeb,Sfin,V,S : float;
                   
begin
  V := Vectoriel(Seg1.fin-Seg1.deb,Seg2.fin-Seg2.deb);
  if V = 0.0 then 
    -- droite paralleles
    if Vectoriel(Seg1.deb-Seg2.deb,Seg2.fin-Seg2.deb) /= 0.0 then
      -- droites disjointes
      return (Seg_vide,0);
    end if;
      -- droites confondues, reste a determiner l'intersection des segments
      -- les points Seg1.deb, Seg1,fin, Seg2.deb, Seg2.fin sont classees
      -- sur la droite orientee (Seg1.deb,Seg1.fin) selon le classement  
      -- de resp. 0, N, Sdeb, Sfin
    N := Norme2(Seg1.fin-Seg1.deb);
    Sdeb := Scalaire(Seg2.deb-Seg1.deb,Seg1.fin-Seg1.deb);
    Sfin := Scalaire(Seg2.fin-Seg1.deb,Seg1.fin-Seg1.deb);
    if Sdeb < 0.0 then
      if Sfin < 0.0 then
        return (Seg_vide,0);
      elsif Sfin = 0.0 then
        return ((Seg1.deb,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
      elsif Sfin <= N then
        return ((Seg1.deb,Seg2.fin,Pt_infini,0.0,0.0,Ligne),3);
      else
        return (Seg1,3);
      end if;
    elsif Sdeb = 0.0 then
      if Sfin <= 0.0 then
        return ((Seg1.deb,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
      elsif Sfin <= N then
        return (Seg2,3);
      else
        return (Seg1,3);
      end if;
    elsif Sdeb < N then
      if Sfin <= 0.0 then
        return ((Seg1.deb,Seg2.deb,Pt_infini,0.0,0.0,Ligne),3);
      elsif Sfin < Sdeb then
        return ((Seg2.fin,Seg2.deb,Pt_infini,0.0,0.0,Ligne),3);
      elsif Sfin <= N then
        return (Seg2,3);
      else
        return ((Seg2.deb,Seg1.fin,Pt_infini,0.0,0.0,Ligne),3);
      end if;
    elsif Sdeb = N then
      if Sfin <= 0.0 then
        return (Seg1,3);
      elsif Sfin < N then
        return ((Seg2.fin,Seg2.deb,Pt_infini,0.0,0.0,Ligne),3);
      else 
        return ((Seg1.fin,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
      end if;
    else
      if Sfin <= 0.0 then
        return (Seg1,3);
      elsif Sfin < N then
        return ((Seg2.fin,Seg1.fin,Pt_infini,0.0,0.0,Ligne),3);
      elsif Sfin = N then 
        return ((Seg2.fin,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
      else 
        return (Seg_vide,0);
      end if;
    end if;
  end if;

  if V > 0.0 then 
    S := 1.0;
  else 
    S := -1.0;
  end if;
  N1 := Vectoriel(Seg2.deb-Seg1.deb,Seg2.fin-Seg2.deb);
  if S*N1 < 0.0 or else abs(N1)>abs(V) then
    return (Seg_vide,0);
  end if;
  N2 := Vectoriel(Seg1.deb-Seg2.deb,Seg1.fin-Seg1.deb);
  if S*N2 > 0.0 or else abs(N2) > abs(V) then 
    return (Seg_vide,0);
  end if;
  return(((Seg1.fin-Seg1.deb)*N1/V+Seg1.deb,Pt_infini,Pt_infini,0.0,0.0,Ligne),1); 

end Intersection_ligne_ligne;

-----------------------------------------
-- INTERSECTION_LIGNE_CERCLE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Intersection entre un arc de cercle et un segment de droite
-----------------------------------------
Function Intersection_ligne_cercle(Seg1, Seg2: Segment) return Intersection is

D, D2 : float;
U,P,P1,P2 : Point_type_reel;

begin
  -- P = projete de Seg2.centre sur la droite definie par Seg1
  U := (Seg1.fin-Seg1.deb)/Norme(Seg1.fin-Seg1.deb);
  P := Seg1.deb + U*Scalaire(Seg2.centre-Seg1.deb,U);

  D2 := Seg2.rayon**2-Norme2(P-Seg2.centre);
  if D2 < 0.0  then
    -- la droite et le cercle ne s'intersectent pas
    return (Seg_vide,0);
  elsif D2 = 0.0 then
    -- la droite et le cercle sont tangeants en P
    if Point_dans_region(P,Seg1) and then Point_dans_region(P,Seg2) then
      return ((P,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    else
      return (Seg_vide,0);
    end if;
  else
    -- la droite et le cercle s'intersectent en deux points: P1 et P2
    -- reste a savoir si ils appartiennent a l'arc de cercle et au segment de
    -- droite
    D := sqrt(D2);
    P1 := P + D*U;
    P2 := P - D*U;
    if Point_dans_region(P1,Seg1) and then Point_dans_region(P1,Seg2) then
      if Point_dans_region(P2,Seg1) and then Point_dans_region(P2,Seg2) then
        return ((P1,P2,Pt_infini,0.0,0.0,Ligne),2);
      else
        return ((P1,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
      end if;
    elsif Point_dans_region(P2,Seg1) and then Point_dans_region(P2,Seg2) then
      return ((P2,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    else 
      return (Seg_vide,0);
    end if;
  end if;

end Intersection_ligne_cercle;

-----------------------------------------
-- INTERSECTION_CERCLE_CERCLE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Intersection entre deux arcs de cercle 
-----------------------------------------
-- ATTENTION
-- Cas des cercles confondus mal gere dans le cas general
-----------------------------------------
Function Intersection_cercle_cercle(Seg1, Seg2: Segment) return Intersection is

Alpha, D, cosinus : float;
CC, P, P1, P2 : Point_type_reel;

begin

  -- deux cercles confondus
  -- ATTENTION
  -- seul le cas des deux arcs arcs de cercle confondus ou avec une 
  -- intersection reduite a un point est gere la car c'est le seul cas 
  -- rencontree dans les erosion-dilatation  
  -- le resultat theorique peut etre 2 arcs de cercle disjoints
  if Seg1 = Seg2 then
    return (Seg1,3);
  end if;
  if Seg1.centre = Seg2.centre and Seg1.rayon = Seg2.rayon then
    if Seg1.deb = Seg2.deb or else Seg1.deb = Seg2.fin then
      return ((Seg1.deb,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    elsif Seg1.fin = Seg2.deb or else Seg1.fin = Seg2.fin then
      return ((Seg1.fin,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    else
      return (Seg_vide,0);
    end if;
  end if;

  -- cas des cercles disjoints
  D := Norme(Seg1.centre-Seg2.centre);
  if D > Seg1.rayon + Seg2.rayon or D < Abs(Seg1.rayon - Seg2.rayon) then
    return (Seg_vide,0);
  end if;

  -- cas des cercles tangeants
  CC := Seg2.centre-Seg1.centre;
  if D = Seg1.rayon + Seg2.rayon then
    -- cercles tangeants par l'exterieur en P1
    P1 := Seg1.centre+CC*Seg1.rayon/D;
    if Point_dans_region(P1,Seg1) and then Point_dans_region(P1,Seg2) then
      return ((P1,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    end if;
  elsif D = Seg2.rayon-Seg1.rayon then
    -- cercles tangeants par l'exterieur de Seg1 et l'interieur de Seg2 en P1
    P1 := Seg1.centre-CC*Seg1.rayon/D;
    if Point_dans_region(P1,Seg1) and then Point_dans_region(P1,Seg2) then
      return ((P1,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    end if;
  elsif D = Seg1.rayon-Seg2.rayon then
    -- cercles tangeants par l'exterieur de Seg2 et l'interieur de Seg1 en P1
    P1 := Seg2.centre+CC*Seg2.rayon/D;
    if Point_dans_region(P1,Seg1) and then Point_dans_region(P1,Seg2) then
      return ((P1,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    end if;
  end if;

  -- cas des cercles ayant deux intersections P1 et P2
  Cosinus := (Seg1.rayon**2+D**2-Seg2.rayon**2)/(2.0*Seg1.rayon*D);
  if cosinus <=-1.0 or cosinus >= 1.0 then
    -- se produit a cause des erreurs de calcul quelques fois pour des rayons
    -- tres grands
    return (seg_vide,0);
  end if;
  Alpha := Arccos(cosinus);
  P := Seg1.centre+CC*Seg1.rayon/D;
  P1 := Rotation(P,Seg1.centre,Alpha);
  P2 := Rotation(P,Seg1.centre,-Alpha);
  if Point_dans_region(P1,Seg1) and then Point_dans_region(P1,Seg2) then
    if Point_dans_region(P2,Seg1) and then Point_dans_region(P2,Seg2) then
      return ((P1,P2,Pt_infini,0.0,0.0,Ligne),2);
    else
      return ((P1,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
    end if;
  elsif Point_dans_region(P2,Seg1) and then Point_dans_region(P2,Seg2) then
    return ((P2,Pt_infini,Pt_infini,0.0,0.0,Ligne),1);
  else 
    return (Seg_vide,0);
  end if;

end Intersection_cercle_cercle;

-----------------------------------------
-- DISTANCE_POINT_SEGMENT
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Distance d'un point a un segment 
-- (ie. segment de droite ou arc de cercle)
-----------------------------------------
Function Distance_point_segment(Pt : Point_type_reel; 
		                Seg : Segment) return float is

D, Dp  : float;
U : Point_type_reel;

begin 
  if Seg.interpolation = Ligne then
    -- distance d'un point a un segment de droite
    if Seg.deb = Seg.fin then
      -- segment reduit a un point 
      return Norme(Seg.deb-Pt);
    else
      -- calcul du projete  de Pt sur (Seg.deb,Seg.fin)
      D := Norme(Seg.deb-Seg.fin);
      U := (Seg.fin-Seg.deb)/D;
      Dp := Scalaire(Pt-Seg.deb,U);

      if Dp <= 0.0 then
        -- Le projete est avant Seg.deb, on renvoie la distance Seg.deb-Pt
        return Norme(Seg.deb-Pt);
      elsif Dp >= D then
        -- Le projete est apres Seg.fin, on renvoie la distance Seg.fin-Pt
        return Norme(Seg.fin-Pt);
      else
        -- Le projete est sur le segment,
        -- on renvoie la distance au point projete sur la droite 
        return Norme(Seg.deb+Dp*U-Pt);
      end if;
    end if;
  else -- cercle
    if Point_dans_region(Pt,Seg) then
      -- distance realisee sur un point de l'arc de cercle
      return Abs(Norme(Seg.centre-Pt)-Seg.rayon); 
    else 
      -- distance realisee sur une extremite du segment
      return Min(Norme(Pt-Seg.deb),Norme(Pt-Seg.fin));
    end if;
  end if;
end Distance_point_segment;

-----------------------------------------
-- DISTANCE_LIGNE_LIGNE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Distance entre deux segments de droite
-----------------------------------------
Function Distance_ligne_ligne(Seg1, Seg2 :Segment) return float is

begin
  if Seg1.deb = Seg1.fin then
    -- le segment Seg1 est reduit a un point
    return Distance_point_segment(Seg1.deb,Seg2);
  elsif Seg2.deb = Seg2.fin then
    -- le segment Seg2 est reduit a un point
    return Distance_point_segment(Seg2.deb,Seg1);
  end if;

  if Intersection_ligne_ligne(Seg1,Seg2).cardinal /= 0 then
    -- les segments se coupent
    return 0.0;
  end if;

  -- les segments sont disoints, alors la distance est realise
  -- sur une des quatre extremites
  return Min(Min(Distance_point_segment(Seg1.deb,Seg2),
                 Distance_point_segment(Seg1.fin,Seg2)),
             Min(Distance_point_segment(Seg2.deb,Seg1),
                 Distance_point_segment(Seg2.fin,Seg1)));

end Distance_ligne_ligne;

-----------------------------------------
-- DISTANCE_LIGNE_CERCLE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Distance entre un segment de droite et un arc de cercle
-----------------------------------------
Function Distance_ligne_cercle(Seg1, Seg2 :Segment) return float is

D,Dp : float;
U,P : Point_type_reel;

begin
  if Seg1.deb = Seg1.fin then
    -- segment de droite reduit a un point
    return Distance_point_segment(Seg1.deb,Seg2);
  elsif Seg2.deb = Seg2.fin then
    -- arc de cercle reduit a un point
    return Distance_point_segment(Seg2.deb,Seg1);
  end if;

  if Intersection_ligne_cercle(Seg1,Seg2).cardinal /= 0 then
    -- les segments se coupent
    return 0.0;
  end if;

  -- les segments sont disjoints, la distance est realisee soit sur
  -- une des 4 extremites des segment soit sur le projete P du centre du cercle
  -- sur le segment
  D := Min(Min(Distance_point_segment(Seg1.deb,Seg2),
               Distance_point_segment(Seg1.fin,Seg2)),
           Min(Distance_point_segment(Seg2.deb,Seg1),
               Distance_point_segment(Seg2.fin,Seg1)));

  U := (Seg1.fin-Seg1.deb)/Norme(Seg1.fin-Seg1.deb);
  Dp := Scalaire(Seg2.centre-Seg1.deb,U);
  P := Seg1.deb+Dp*U;

  if Point_dans_region(P,Seg1) and then Point_dans_region(P,Seg2) then
    return Min(D,Distance_point_segment(P,Seg2));
  else
    return D;
  end if;

end Distance_ligne_cercle;

-----------------------------------------
-- DISTANCE_CERCLE_CERCLE
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Distance entre deux arcs de cercle
-----------------------------------------
Function Distance_cercle_cercle(Seg1, Seg2 :Segment) return float is

D : float;
U, P : Point_type_reel;

begin
  if Intersection_cercle_cercle(Seg1,Seg2).cardinal /= 0 then
    -- les arcs de cercle s'intersectent
    return 0.0;
  end if;

  -- Les arcs de cercle sont disjoints, la distance se realise soit sur 
  -- une des extremites des arcs de cercles, soit sur des points des arcs de
  -- cercle situes sur la droite (centre1 centre2)
  D := Min(Min(Distance_point_segment(Seg1.deb,Seg2),
               Distance_point_segment(Seg1.fin,Seg2)),
           Min(Distance_point_segment(Seg2.deb,Seg1),
               Distance_point_segment(Seg2.fin,Seg1)));

  if Seg1.centre = Seg2.centre then 
    return D;
  end if;

  U := (Seg2.centre-Seg1.centre)/Norme(Seg2.centre-Seg1.centre); 
  P := Seg1.centre + Seg1.rayon*U;
  if Point_dans_region(P,Seg1) and then Point_dans_region(P,Seg2) then
    D := Min(D,Distance_point_segment(P,Seg2));
  end if;
  P := Seg1.centre - Seg1.rayon*U;
  if Point_dans_region(P,Seg1) and then Point_dans_region(P,Seg2) then
    D := Min(D,Distance_point_segment(P,Seg2));
  end if;
  return D;  

end Distance_cercle_cercle;

-----------------------------------------
-- DISTANCE_SEGMENTS
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Distance entre deux segments quelconques
-----------------------------------------
Function Distance_segments(Seg1, Seg2 :Segment) return float is

begin
  if Seg1.interpolation = ligne and Seg2.interpolation = ligne then
    return Distance_ligne_ligne(Seg1,Seg2);
  elsif Seg1.interpolation = ligne and Seg2.interpolation = cercle then
    return Distance_ligne_cercle(Seg1,Seg2);
  elsif Seg1.interpolation = cercle and Seg2.interpolation = ligne then
    return Distance_ligne_cercle(Seg2,Seg1);
  else
    return Distance_cercle_cercle(seg1,Seg2);
  end if;
end Distance_segments;


-----------------------------------------
-- INTERSECTION_SEGMENTS
-----------------------------------------
-- FONCTION INTERNE
-----------------------------------------
-- Intersection entre deux segments quelconques
-----------------------------------------
Function Intersection_segments(Seg1, Seg2 : Segment) return Intersection is

begin
  if Seg1.interpolation = ligne and Seg2.interpolation = ligne then
    return Intersection_ligne_ligne(Seg1,Seg2);
  elsif Seg1.interpolation = ligne and Seg2.interpolation = cercle then
    return Intersection_ligne_cercle(Seg1,Seg2);
  elsif Seg1.interpolation = cercle and Seg2.interpolation = ligne then
    return Intersection_ligne_cercle(Seg2,Seg1);
  else
    return Intersection_cercle_cercle(Seg1,Seg2);
  end if;
end Intersection_segments;

-----------------------------------------
-- DECOUPAGE_POLY_SEGMENT
-----------------------------------------
-- PROCEDURE INTERNE
-----------------------------------------
-- Renvoie la ligne PS egale a la ligne
-- PS1 decoupee a chaque intersection
-- avec PS2, en conservant la parente
-----------------------------------------
procedure Decoupage_poly_segment(PS1, PS2 : in Poly_segment;
                                 Parent1  : in Liens_array_type;
                                 PP1      : in Liens_array_type;
                                 PS       : out Poly_segment_access;
                                 Parent   : out Liens_access_type;
                                 PP       : out Liens_access_type) is

PL : Point_liste_reel(1..PS2'length*2);
Ni, Ni2, N : natural;
Inter : Intersection;
Ordre : Reel_tableau_type(1..PS2'length*2);
P : Point_type_reel;
O, Angle : float;

-- PS3 est le Poly_segment PS1 decoupe a chaque intersection avec PS2
-- On est oblige de supposer que PS2 ne s'intersectera pas plus de 40 fois
-- en moyenne avec chaque segment de PS1. Bourin mais bon...
PS3 : Poly_segment(1..PS1'length*40);
Parent3 : Liens_array_type(1..PS1'length*40);
PP3 : Liens_array_type(1..PS1'length*40);

begin
  N := 0; -- le nombre de segments du Poly_segment decoupe
  for i in PS1'range loop
    -- calcul des intersections de PS1(i) avec PS2
    Ni := 0; -- Nb d'intersections entre PS1(i) et PS2
    for j in PS2'range loop
      -- calcul des intersections de PS1(i) avec PS2(j)

      -- on realise le test suivant pour etre sur que lorsqu'on 
      -- decoupe un Poly-Segment par lui-meme on ait bien:
      --  (Seg1 intersection Seg2) = (Seg2 intersection Seg1)
      -- ce qui n'est pas assure sinon a cause des erreurs d'arrondi
      -- dans les calculs (car mathematiquement c'est la meme chose!)
      if i<j then
        Inter := Intersection_segments(PS1(i),PS2(j));
      else
        Inter := Intersection_segments(PS2(j),PS1(i));
      end if;

      -- si il y a au moins une intersection entre PS1(i) et PS2(j)
      if Inter.cardinal >= 1 then
        Ni := Ni+1;  
        PL(Ni) := Inter.Seg.deb;
        -- PL est la liste des points d'intersection de PS1(i) avec PS2
        -- et Ni le nombre de ces intersections
      end if;

      -- si il y a au moins deux intersection entre PS1(i) et PS2(j)
      -- ie. 2 intersections ou une infinite
      if Inter.cardinal > 1 then 
        Ni := Ni+1;  
        PL(Ni) := Inter.Seg.fin;
      end if;
    end loop;  

    if Ni /= 0 then
      -- Si il y a des intersections entre PS1(i) et PS2

      -- Les intersections rencontrees doivent etre classees dans
      -- le sens de parcours de PS1(i). Le classement se fait avec Ordre(1..Ni)
      if PS1(i).interpolation = Ligne then
        -- Pour une ligne, l'ordre est la distance de l'intersection
        -- au premier point de PS(i)
        for j in 1..Ni loop
          Ordre(j) := Norme2(PL(j)-PS1(i).deb);
        end loop;
      elsif PS1(i).angle >= 0.0 then
        -- Pour un cercle dans le sens trigo, l'ordre est l'angle entre 
        -- le premier point de PS(i), le centre de PS1(i) et l'intersection
        for j in 1..Ni loop
          Ordre(j):=Angle_oriente(PS1(i).deb-PS1(i).centre,PL(j)-PS1(i).centre);
        end loop;
      else
        -- Pour un cercle dans le sens inverse trigo, l'ordre est l'angle entre 
        -- l'intersection, le centre de PS1(i) et le premier point de PS(i)
        for j in 1..Ni loop
          Ordre(j):=Angle_oriente(PL(j)-PS1(i).centre,PS1(i).deb-PS1(i).centre);
        end loop;
      end if;
  
      -- Classement des intersection dans l'ordre de parcours de PS1 
      -- classement croissant de PL(1..Ni) sur le critere Ordre
      for j in 1..Ni loop
        for k in j+1..Ni loop
          if Ordre(j) > Ordre(k) then
            O := Ordre(j);
            P := PL(j);
            Ordre(j) := Ordre(k);
            PL(j) := PL(k);
            Ordre(k) :=O;
            PL(k) := P;
          end if;
        end loop;
      end loop;
  
      -- Filtrage des intersections pour eviter les points doubles
      -- Ni2 est le Nb d'intersections sans doublets
      Ni2 := 1;
      for j in 2..Ni loop
        if Ordre(j) /= Ordre(Ni2) then
          Ni2 := Ni2+1;
          PL(Ni2) := PL(j);
          Ordre(Ni2) := Ordre(j);
        end if;
      end loop;

      -- remplissage du Poly_segment decoupe

      -- ajout dans PS3 du segment qui relie le premier point de PS1(i)
      -- a la premiere intersection rencontree
      if PS1(i).deb /= PL(1) then 
        N := N+1;
        -- Angle est l'angle du nouveau segment cree (si c'est un cercle)
        if PS1(i).interpolation = Cercle and PS1(i).angle > 0.0 then
          Angle:=Angle_oriente(PS1(i).deb-PS1(i).centre,PL(1)-PS1(i).centre); 
        elsif PS1(i).interpolation = Cercle and PS1(i).angle < 0.0 then
          Angle:=Angle_oriente(PS1(i).deb-PS1(i).centre,PL(1)-PS1(i).centre)-2.0*Pi; 
        else
          Angle := 0.0;
        end if;
        PS3(N):=(PS1(i).deb,PL(1),PS1(i).centre,
                 PS1(i).rayon,Angle,PS1(i).interpolation);
        Parent3(N):=Parent1(i);
        PP3(N):=PP1(i);
      end if;

      -- ajout dans PS3 des segments qui relient les intersections rencontrees
      -- entre elles
      for j in 1..Ni2-1 loop
        N := N+1;
        -- Angle est l'angle du nouveau segment cree (si c'est un cercle)
        if PS1(i).interpolation = Cercle and PS1(i).angle > 0.0 then
          Angle:=Angle_oriente(PL(j)-PS1(i).centre,PL(j+1)-PS1(i).centre); 
        elsif PS1(i).interpolation = Cercle and PS1(i).angle < 0.0 then
          Angle:=Angle_oriente(PL(j)-PS1(i).centre,PL(j+1)-PS1(i).centre)-2.0*Pi; 
        else
          angle := 0.0;
        end if;
        PS3(N):=(PL(j),PL(j+1),PS1(i).centre,
                 PS1(i).rayon,Angle,PS1(i).interpolation);
        Parent3(N):=Parent1(i);
        PP3(N):=PP1(i);
      end loop;

      -- ajout dans PS3 du segment qui relie la derniere intersection
      -- rencontree et le dernier point de PS1(i)
      if PL(Ni2) /= PS1(i).fin then 
        N := N+1;
        -- Angle est l'angle du nouveau segment cree (si c'est un cercle)
        if PS1(i).interpolation = Cercle and PS1(i).angle > 0.0 then
          Angle:=Angle_oriente(PL(Ni2)-PS1(i).centre,PS1(i).fin-PS1(i).centre); 
        elsif PS1(i).interpolation = Cercle and PS1(i).angle < 0.0 then
          Angle:=Angle_oriente(PL(Ni2)-PS1(i).centre,PS1(i).fin-PS1(i).centre)-2.0*Pi; 
        else
          Angle := 0.0;
        end if;
        PS3(N):=(PL(Ni2),PS1(i).fin,PS1(i).centre,
                 PS1(i).rayon,Angle,PS1(i).interpolation);
        Parent3(N):=Parent1(i);
        PP3(N):=PP1(i);
      end if;
    else
      -- s'il n'y a pas d'intersections entre PS1(i) et PS2
      N := N+1;
      PS3(N):=PS1(i);
      Parent3(N):=Parent1(i);
      PP3(N):=PP1(i);
    end if;
  end loop;

  PS := new Poly_segment'(PS3(1..N));
  Parent := new Liens_array_type'(Parent3(1..N));
  PP := new Liens_array_type'(PP3(1..N));

end Decoupage_poly_segment;

-----------------------------------------
-- CERCLE_TO_LIGNE
-----------------------------------------
-- PROCEDURE INTERNE
-----------------------------------------
-- Pour transformer un Polysegment en une Polyligne 
-----------------------------------------
Procedure Cercle_to_ligne(PS        : in Poly_segment;
                          ParentS   : in Liens_array_type;
                          PL        : out Point_access_reel;
                          ParentL   : out Liens_access_type;
                          Precision : in float := Precision_defaut) is

PL2 		: Point_access_reel;
ParentL2 	: Liens_access_type;
Rayon_maxi 	: float := 0.0;
Angle_maxi,
Angle_rot 	: float;
N, 
Nb_pts_maxi, 
Nb_pts_virage 	: natural;
Ptmp		: Point_type_reel;
-- MOdif 15/03/99 Problem d'arrondi dans un trou minuscule
-- Modif 23/06/03 : la resolution est de 160 (au lieu de 16) avec WINPAT,
-- adaptation experimentale d'erreur_fl a 4.0 au lieu de 1.0 
erreur_fl       : float :=4.0;

begin
  -- determination du rayon maximum des cercles du Poly-Segment
  for i in PS'range loop
    if PS(i).interpolation = Cercle and then PS(i).rayon > Rayon_maxi then
      Rayon_maxi := PS(i).rayon;
    end if;
  end loop;
  if Precision > 2.0*Rayon_maxi then
    Nb_pts_maxi := PS'length+1;
  else
    Angle_maxi := 2.0*arccos(1.0-Precision/Rayon_maxi);
    Nb_pts_maxi := Natural(2.0*Pi/Angle_maxi+1.0)*PS'length;
  end if;

  -- b�quille
  Nb_pts_maxi:=Nb_pts_maxi*2;

  PL2 := new Point_liste_reel(1..Nb_pts_maxi);
  ParentL2 := new Liens_array_type(1..Nb_pts_maxi);

  N := 0;
  for i in PS'range loop
    -- Ajout dans la polyligne du premier point du segment
    -- Dans le cas general ce point est egal au dernier point du segment
    -- precedent, si ce n'est pas le cas alors le segment cree sert a rejoindre
    -- deux polylignes non connexes delimitant le symbole (ie. la limite
    -- exterieure du symbole et un trou, ou deux trous), dans ce cas
    -- le segment cree aura la valeur 0 comme parent
    if i = PS'first then
      N:=N+1;
      PL2(N) := PS(i).deb;
      ParentL2(N) := ParentS(i);
    elsif PS(i).deb /= PL2(N) and then Norme(PS(i).deb-PL2(N)) > Erreur_fl then
      N:=N+1;
      PL2(N) := PS(i).deb;
      ParentL2(N):=ParentS(i);
      ParentL2(N-1) := 0;
    else
      ParentL2(N) := ParentS(i);
    end if;

    -- dans le cas d'un arc de cercle, ajout de points intermediaires
    if PS(i).interpolation = Cercle and then Precision < 2.0*PS(i).rayon then
      Angle_maxi := 2.0*arccos(1.0-(Precision-Erreur)/PS(i).rayon);
      Nb_pts_virage := Integer(Abs(PS(i).angle)/Angle_maxi-0.4999);
      Angle_rot := PS(i).angle/float(Nb_pts_virage+1);
      for j in 1..Nb_pts_virage loop
	PTmp:=Rotation(PS(i).deb,PS(i).centre,Angle_rot*float(j));
	If Norme(Ptmp-PL2(N)) > Erreur then
          N:=N+1;
          PL2(N) := Ptmp;
          ParentL2(N) := ParentS(i);
	End If;
      end loop;
    end if;

    -- fin du segment
    If Norme(PS(i).Fin-PL2(N)) > Erreur then
      N:=N+1;
      PL2(N) := PS(i).fin;
      ParentL2(N) := ParentS(i);
    End If;
  end loop;
                                           
  PL := new Point_liste_reel'(PL2(1..N));
  ParentL := new Liens_array_type'(ParentL2(1..N-1));

  GR_free_point_liste_reel(PL2);
  GR_free_liens_array(ParentL2);

end Cercle_to_ligne; 

-----------------------------------------
-- LIGNE_TO_CERCLE
-----------------------------------------
-- PROCEDURE EXPORTEE
-----------------------------------------
-- Pour transformer une Polyligne en un Polysegment  
-----------------------------------------
Procedure Ligne_to_cercle(PL        : in Point_liste_reel;
                          ParentL   : in Liens_array_type;
                          PS        : out Poly_segment_access;
                          ParentS   : out Liens_access_type) is

N : natural;
PS2 : Poly_segment_access;
ParentS2 : Liens_access_type;

begin
  PS2 := new Poly_segment(1..PL'length-1);
  ParentS2 := new Liens_array_type(1..PL'length-1);
  N := 0;
  for i in PL'first..PL'last-1 loop
    if PL(i) /= PL(i+1) then
      N := N+1;
      PS2(N) := (PL(i),PL(i+1),Pt_infini,0.0,0.0,Ligne);
      ParentS2(N) := ParentL(i);
    end if;
  end loop;          
  PS := new Poly_segment'(PS2(1..N));
  ParentS := new Liens_array_type'(ParentS2(1..N));
  Free_poly_segment(PS2);
  GR_free_liens_array(ParentS2);

end Ligne_to_cercle;

------------------------------------------------
-- DECALAGE_SEGMENT
------------------------------------------------
-- FONCTION INTERNE
------------------------------------------------
-- Decalage a gauche d'un segment 
-- retourne un arc de cercle ou un segment de droite
------------------------------------------------
function Decalage_segment(Seg : in Segment;
		          D   : in float) 
		          return Segment is
    

N : float;
U,P1,P2 : Point_type_reel;

begin
  if Seg.interpolation = ligne then 
    -- un segment de droite decale est un segment de droite
    N := Norme(Seg.deb-Seg.fin);
    U := (Seg.deb.coor_y-Seg.fin.coor_y,Seg.fin.coor_x-Seg.deb.coor_x)/N;
    return(Seg.deb+U*D,Seg.fin+U*D,Pt_infini,0.0,0.0,Ligne);
  else
    if Seg.angle > 0.0 then
      -- decalage vers l'interieur du cercle
      if D >= Seg.rayon then
        -- Correctio 2002 : dilqtqtion - erosion avec la meme valeur D
        return (Seg.centre,Seg.centre,Pt_infini,0.0,0.0,Ligne);
--	  ancienne version
--        return Seg_vide;
      else
        -- le decale est un arc de cercle de meme centre que le segment initial
        P1 := Seg.centre+(Seg.deb-Seg.centre)*(1.0-D/Seg.Rayon);
        P2 := Seg.centre+(Seg.fin-Seg.centre)*(1.0-D/Seg.Rayon);
        return(P1,P2,Seg.centre,Seg.rayon-D,Seg.angle,Cercle);
      end if;
    else
      -- le decale est un arc de cercle de meme centre que le segment initial
      P1 := Seg.centre+(Seg.deb-Seg.centre)*(1.0+D/Seg.Rayon);
      P2 := Seg.centre+(Seg.fin-Seg.centre)*(1.0+D/Seg.Rayon);
      return(P1,P2,Seg.centre,Seg.rayon+D,Seg.angle,Cercle);
    end if;  
  end if;

end Decalage_segment;

		      
------------------------------------------------
-- DECALAGE_JONCTION
------------------------------------------------
-- FONCTION INTERNE
------------------------------------------------
-- en cas d'arc ferme, virage au premier point
------------------------------------------------
Procedure Decalage_jonction(Seg1, Seg2 : in Segment;
		            D    : in float;
                            Segg1,Segg2 : out Segment) is

Virage, Angle, N1, N2 : float;
P1,P2,P3, U1, U2 : Point_type_reel;

begin
  if Seg1.interpolation = Ligne and Seg2.interpolation = Ligne then
    N1:=Norme(Seg1.deb-Seg1.fin);
    U1:=(Seg1.deb.coor_y-Seg1.fin.coor_y,Seg1.fin.coor_x-Seg1.deb.coor_x)/N1;
    N2:=Norme(Seg2.deb-Seg2.fin);
    U2:=(Seg2.deb.coor_y-Seg2.fin.coor_y,Seg2.fin.coor_x-Seg2.deb.coor_x)/N2;
  elsif Seg1.interpolation = Ligne and Seg2.interpolation = Cercle then
    N1:=Norme(Seg1.deb-Seg1.fin);
    U1:=(Seg1.deb.coor_y-Seg1.fin.coor_y,Seg1.fin.coor_x-Seg1.deb.coor_x)/N1;
    U2:=(Seg2.centre-Seg2.deb)/Seg2.rayon;
  elsif Seg1.interpolation = Cercle and Seg2.interpolation = Ligne then
    U1:=(Seg1.centre-Seg1.fin)/Seg1.rayon;
    N2:=Norme(Seg2.deb-Seg2.fin);
    U2:=(Seg2.deb.coor_y-Seg2.fin.coor_y,Seg2.fin.coor_x-Seg2.deb.coor_x)/N2;
  elsif Seg1.interpolation = Cercle and Seg2.interpolation = Cercle then
    U1:=(Seg1.centre-Seg1.fin)/Seg1.rayon;
    U2:=(Seg2.centre-Seg2.deb)/Seg2.rayon;
  end if;
  Angle:=Angle_oriente(U1,U2);
  if Angle >= Pi then
    Angle := Angle/2.0-Pi;
    P1 := Seg1.fin+D*U1;
    P2 := rotation(P1,Seg1.fin,Angle);
    P3 := Seg2.deb+D*U2; 
    Segg1 := (P1,P2,Seg1.fin,D,Angle,Cercle);
    Segg2 := (P2,P3,Seg1.fin,D,Angle,Cercle);
  else
    Segg1 := Seg_vide;
    Segg2 := Seg_vide;
  end if;

end Decalage_jonction;

---------------------------------
-- DILATATION_GAUCHE
---------------------------------
-- PROCEDURE INTERNE
---------------------------------
-- dilatation d'un Poly-Segment sur la gauche
-- toutes les procedures suivantes font appel
-- a cette procedure
---------------------------------
Procedure Dilatation_gauche( Arc     : in Poly_segment;
                             Parent  : in Liens_array_type;
                             D       : in float;
                             Arrondi : in boolean;
                             Arcg    : out Poly_segment_access;
                             Parentg : out Liens_access_type) is

N, N1, N3 : natural;
Arc1, Arc2, ARC3 : Poly_segment_access;
Virage, Dec1, Dec2, S1, S2, Prem, Dern : Segment;
PP1,PP2,Parent1, Parent2, Parent3 : Liens_access_type;
Pt1, U, V, P1, P2 : Point_type_reel;
Loin : boolean;
Inter : Intersection; 
Angle, Angle2 : float;

begin
 
  -- PREMIERE PASSE
  -- creation de l'arc constitues des segments decales de l'arc original
  -- creation de l'arc Arc1, Nb de segments: N1

  Arc1 := new Poly_segment(1..Arc'length*3);
  Parent1 := new Liens_array_type(1..Arc'length*3);
  PP1 := new Liens_array_type(1..Arc'length*3);
  -- NB: Les tableaux PP1 et PP2 n'ont une utilite que pour l'acceleration
  --     de l'algo lors du calcul de la distance d'un point du bord decale
  --     a la ligne initiale (troisieme passe)

  -- traitement des extremites
  -- determination du premier et dernier segment
  if Arrondi and Arc(Arc'first).deb = Arc(Arc'last).fin then
    -- arc ferme
    Decalage_jonction(Arc(Arc'last),Arc(Arc'first),D,Dern,Prem);
  elsif Arrondi then
    -- au debut
    -- P1 est le point dans le prolongement du premier segment a une distance D
    -- P2 est le point decale a gauche du debut du premier segment 
    -- a une distance D
    if Arc(Arc'first).interpolation = Ligne then
      U := Arc(Arc'first).deb-Arc(Arc'first).fin;
      V := (Arc(Arc'first).deb.coor_y-Arc(Arc'first).fin.coor_y,
            Arc(Arc'first).fin.coor_x-Arc(Arc'first).deb.coor_x);
    else
      U := Rotation(Arc(Arc'first).centre,Arc(Arc'first).deb,Pi/2.0);
      V := Arc(Arc'first).centre-Arc(Arc'first).deb;
    end if;
    P1 := Arc(Arc'first).deb+U/Norme(U)*D;
    P2 := Arc(Arc'first).deb+V/Norme(V)*D;
    Prem:=(P1,P2,Arc(Arc'first).deb,D,-Pi/2.0,Cercle);

    -- a la fin
    -- P1 est le point decale a gauche de la fin du dernier segment 
    -- a une distance D
    -- P2 est le point dans le prolongement du dernier segment a une distance D
    if Arc(Arc'first).interpolation = Ligne then
      U := (Arc(Arc'last).deb.coor_y-Arc(Arc'last).fin.coor_y,
          Arc(Arc'last).fin.coor_x-Arc(Arc'last).deb.coor_x);
      V := (Arc(Arc'last).fin-Arc(Arc'last).deb);
    else
      U := Arc(Arc'last).centre-Arc(Arc'last).fin;
      V := Rotation(Arc(Arc'last).centre,Arc(Arc'last).fin,Pi/2.0);
    end if;
    P1 := Arc(Arc'last).fin+U/Norme(U)*D;
    P2 := Arc(Arc'last).fin+V/Norme(V)*D;
    Dern:=(P1,P2,Arc(Arc'last).fin,D,-Pi/2.0,Cercle);
  else
    Prem := Seg_vide;
    Dern := Seg_vide;
  end if;

  -- traitement du debut de l'arc decale
  Dec1 := Decalage_segment(Arc(Arc'first),D);  
  Pt1 := Dec1.deb;
  if Prem /= Seg_vide then
    N1 := 1;
    Arc1(1) := Prem;
    Parent1(1) := Parent(Arc'first);
    PP1(1) := Arc'first;
  else N1 := 0;
  end if;

  -- traitement de tous les virages
  for i in Arc'first+1..Arc'last loop
    Dec2 := Decalage_segment(Arc(i),D); -- decale du segment apres le virage
    Angle := Angle_oriente(Dec1.fin-Arc(i).deb,Dec2.deb-Arc(i).deb);
    if arc(i-1).fin=arc(i).Deb and arc(i-1).deb=arc(i).fin then
       -- on est dans le cas ou l'arc revient sur lui meme, et donc
       -- l'angle = pi rigoureusement, et on evite les erreurs d'arrondi
       Angle:=Pi;
    end if;
    if Angle < Pi then
      -- cas d'un virage a gauche
      Inter := Intersection_segments(Dec1,Dec2);
      if Inter.cardinal = 1 then
        -- cas ou les deux decales se croisent
        if Pt1 /= Inter.Seg.deb then
          -- ajout du decale "coupe" du segment precedent
          N1 := N1+1;
          if Dec1.Interpolation = Cercle then
            Angle2 := Angle_oriente(Pt1-Dec1.centre,Inter.Seg.deb-Dec1.centre);
            if Angle2 > Pi then
              Angle2 := Angle2-2.0*Pi;
            end if;
          else
            Angle2 := 0.0;
          end if;
          Arc1(N1) :=
           (Pt1,Inter.Seg.deb,Dec1.centre,Dec1.rayon,Angle2,Dec1.interpolation);
          Parent1(N1) := Parent(i-1);
          PP1(N1) := i-1;
        end if;
        Pt1 := Inter.Seg.deb;
      else
        -- cas ou les deux decales ne se croisent pas
        if Pt1 /= Dec1.fin then
          N1 := N1+1;
          if Dec1.Interpolation = Cercle then
            Angle2 := Angle_oriente(Pt1-Dec1.centre,Dec1.fin-Dec1.centre);
            if Angle2 > Pi then
              Angle2 := Angle2-2.0*Pi;
            end if;
          else
            Angle2 := 0.0;
          end if;
          Arc1(N1) :=
            (Pt1,Dec1.fin,Dec1.centre,Dec1.rayon,Angle2,Dec1.interpolation);
          Parent1(N1) := Parent(i-1);
          PP1(N1) := i-1;
        end if;
        Pt1 := Dec2.deb;
      end if;
    else
      -- cas d'un virage a droite
      if Pt1 /= Dec1.fin then
        N1 := N1+1;
        if Dec1.Interpolation = Cercle then
          Angle2 := Angle_oriente(Pt1-Dec1.centre,Dec1.fin-Dec1.centre);
          if Angle2 > Pi then
            Angle2 := Angle2-2.0*Pi;
          end if;
        else
          Angle2 := 0.0;
        end if;
        Arc1(N1) :=
          (Pt1,Dec1.fin,Dec1.centre,Dec1.rayon,Angle2,Dec1.interpolation);
        Parent1(N1) := Parent(i-1);
        PP1(N1) := i-1;
      end if;
      if Angle /= Pi then
        N1 := N1+1;
        Arc1(N1) := (Dec1.fin,Dec2.deb,Arc(i).deb,D,Angle-2.0*Pi,Cercle);
        Parent1(N1) := Parent(i-1);
        PP1(N1) := i-1;
      else
        -- pour une bonne gestion du passage de l'arc droit a l'arc gauche
        P1 := Arc(i).deb+D*(Arc(i).deb-Arc(i).fin)/Norme(Arc(i).deb-Arc(i).fin);
        N1 := N1+1;
        Arc1(N1) := (Dec1.fin,P1,Arc(i).deb,D,-Pi/2.0,Cercle);
        Parent1(N1) := Parent(i-1);
        PP1(N1) := i-1;
        N1 := N1+1;
        Arc1(N1) := (P1,Dec2.deb,Arc(i).deb,D,-Pi/2.0,Cercle);
        Parent1(N1) := Parent(i);
        PP1(N1) := i;
      end if;
      Pt1 := Dec2.deb;
    end if;
    Dec1 := Dec2;
  end loop;

  -- traitement du dernier segment
  if Pt1 /= Dec1.fin then
    N1 := N1+1;
    if Dec1.Interpolation = Cercle then
      Angle2 := Angle_oriente(Pt1-Dec1.centre,Dec1.fin-Dec1.centre);
      if Angle2 > Pi then
        Angle2 := Angle2-2.0*Pi;
      end if;
    else
      Angle2 := 0.0;
    end if;
    Arc1(N1) :=
     (Pt1,Dec1.fin,Dec1.centre,Dec1.rayon,Angle2,Dec1.interpolation);
    Parent1(N1) := Parent(Arc'last);
    PP1(N1) := Arc'last;
  end if;

  if Dern /= Seg_vide then
    N1 := N1+1;
    Arc1(N1) := Dern;
    Parent1(N1) := Parent(Arc'last);
    PP1(N1) := Arc'last;
  end if;

  -- DEUXIEME PASSE
  -- Ajout de toutes les intersections de l'arc avec lui meme

  Decoupage_poly_segment(Arc1(1..N1),Arc1(1..N1),
                         Parent1(1..N1),PP1(1..N1),Arc2,Parent2,PP2);

  Free_poly_segment(Arc1);
  GR_free_liens_array(Parent1);
  GR_free_liens_array(PP1);

  -- TROISIEME PASSE
  -- Suppression de tous les segments a une distance a l'arc original
  -- inferieure a D

  -- C'est ici que les erreurs d'arrondi des calculs se revelent tres penibles
  -- pour resoudre ce probleme on peut travailer en Long_float (ou alors ne
  -- pas montrer l'algo avec des valeurs de decalage enorme pendant les demos)
  -- Si on met le seuil D1 de conservation des arcs trop proche de D (genre
  -- D-0.0001) des arcs qui ne devraient pas etre supprime le sont.
  -- Si on met le seuil D1 de conservation des arcs trop eloigne de D (genre
  -- D-0.1) on risque de conserver des arcs (tres petits) qui devraient etre
  -- supprimes.
  Arc3 := new Poly_segment(1..Arc2'length);
  Parent3 := new Liens_array_type(1..Arc2'length);
  N3 := 0;
  for i in Arc2'range loop
    Loin := true;
    N := Max(PP2(i)-Arc'first,Arc'last-PP2(i));

    for j in 1..N loop
      if PP2(i)-j >= Arc'first and then 
            Distance_segments(Arc2(i),Arc(PP2(i)-j)) < D - Erreur*4.0 then
         Loin := False;
         exit;
      end if;
      if PP2(i)+j <= Arc'last and then 
            Distance_segments(Arc2(i),Arc(PP2(i)+j)) < D - Erreur*4.0 then
         Loin := False;
         exit;
      end if;
    end loop;

    if Loin and (N3 = 0 or else Norme(Arc2(i).deb-Arc2(i).fin)>Erreur) then
      N3:=N3+1;
      Arc3(N3):=Arc2(i);
      Parent3(N3):=Parent2(i);
    end if;
  end loop;

  Free_poly_segment(Arc2);
  GR_free_liens_array(Parent2);
  GR_free_liens_array(PP2);

  if N3 = 0 then
    Arcg := Null;
    Parentg := Null;
    Free_poly_segment(Arc3);
    GR_free_liens_array(Parent3);
    return;
  end if;
  Arcg := new Poly_segment'(Arc3(1..N3));
  Parentg := new Liens_array_type'(Parent3(1..N3));
  Free_poly_segment(Arc3);
  GR_free_liens_array(Parent3);
 
end Dilatation_gauche;

---------------------------------
-- DILATATION_DROITE
---------------------------------
-- PROCEDURE INTERNE
---------------------------------
-- dilatation d'un Poly-Segment vers la droite
-- Cette procedure inverse l'arc, appelle
-- Dilatation gauche, et reinverse le resultat
---------------------------------
Procedure Dilatation_droite( Arc     : in Poly_segment;
                             Parent  : in Liens_array_type;
                             D       : in float;
                             Arrondi : in boolean;
                             Arcd    : out Poly_segment_access;
                             Parentd : out Liens_access_type) is

Arc_inv, Arcd_inv, Arcd2 : Poly_segment_access ;
Parent_inv, Parentd_inv, Parent2 : Liens_access_type;
S : Segment;
 
begin
  -- le sens de l'arc en entree est inverse
  Arc_inv := new Poly_segment(Arc'range);
  Parent_inv := new Liens_array_type(Arc'range);

  for i in Arc'range loop
    S := Arc(Arc'last-i+1);
    Arc_inv(i):=(S.fin,S.deb,S.centre,S.rayon,-S.angle,S.interpolation);
    Parent_inv(i) := Parent(Arc'last-i+1);
  end loop;

  -- calcul du bord gauche de l'arc inverse
  Dilatation_gauche(Arc_inv.all,Parent_inv.all,D,Arrondi,Arcd_inv,Parentd_inv);
  if Arcd_inv = Null then
    Arcd:= Null;
    Parentd := Null;
    Free_poly_segment(Arc_inv);
    Free_poly_segment(Arcd_inv);
    Gr_free_liens_array(Parent_inv);
    Gr_free_liens_array(Parentd_inv);
    return;
  end if;

  -- le sens des arc et liens en sortie est inverse
  Parent2 := new Liens_array_type(Parentd_inv'range);
  Arcd2 := new Poly_segment(Arcd_inv'range);

  for i in Arcd_inv'range loop
    Parent2(i) := Parentd_inv(Parentd_inv'last-i+1);
    S := Arcd_inv(Arcd_inv'last-i+1);
    Arcd2(i):=(S.fin,S.deb,S.centre,S.rayon,-S.angle,S.interpolation);
  end loop;

  Arcd := new Poly_segment'(Arcd2.all);
  Parentd := new Liens_array_type'(Parent2.all);

  Free_poly_segment(Arc_inv);
  Free_poly_segment(Arcd_inv);
  Free_poly_segment(Arcd2);
  Gr_free_liens_array(Parent_inv);
  Gr_free_liens_array(Parentd_inv);
  Gr_free_liens_array(Parent2);

end Dilatation_droite;

--------------------------------------------------------------------------
--                   PROCEDURES EXPORTEES                               --
--------------------------------------------------------------------------


---------------------------------
-- DILATATION_GAUCHE
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- dilatation d'une Poly-Ligne
-- Transforme la polyligne en Poly-segment
-- et appelle Dilatation_gauche d'un Polysegment
---------------------------------
Procedure Dilatation_gauche( Arc       : in  Point_liste_reel;
                             D         : in  float;
                             Arcg      : out Point_access_reel;
                             Parent    : out Liens_access_type;
                             Arrondi   : in  boolean := true;
                             Precision : in  float := Precision_defaut) is

PC, PCg: Poly_segment_access;
ParentC, ParentCg, ParentL: Liens_access_type; 

begin

  if Arc'length < 2 then
    Arcg:= Null;
    Parent := Null;
    return;
  end if;

  ParentL := new Liens_array_type(Arc'range);
  for i in Arc'range loop
    ParentL(i) := i;
  end loop;

  if D = 0.0 then
    Arcg := new Point_liste_reel'(Arc);
    Parent := ParentL;
    return;
  end if;

  Ligne_to_cercle(Arc,ParentL.all,PC,ParentC);
  Dilatation_gauche(PC.all,ParentC.all,D,Arrondi,PCg,ParentCg);
  if PCg/=Null then
     Cercle_to_ligne(PCg.all,ParentCg.all,Arcg,Parent,Precision);
     Free_poly_segment(PCg);
     GR_free_liens_array(ParentCg);
  end if;

  GR_free_liens_array(ParentL);
  GR_free_liens_array(ParentC);
  Free_poly_segment(PC);

end Dilatation_gauche;


---------------------------------
-- FERMETURE_GAUCHE
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- fermeture d'une Polyligne
-- Transforme la Polyligne en Polysegment
-- Appelle Dilatation_gauche d'un Polysegment
-- Appelle Dilatation_droite d'un Polysegment
---------------------------------
Procedure Fermeture_gauche( Arc       : in  Point_liste_reel;
                            D1        : in  float;
                            D2        : in  float;
                            Arcg      : out Point_access_reel;
                            Parent    : out Liens_access_type;
                            Arrondi   : in  boolean := true;
                            Precision : in  float := Precision_defaut) is

PC, PCg, PCd: Poly_segment_access;
ParentC, ParentCg, ParentCd, ParentL: Liens_access_type; 

begin

  if Arc'length < 2 then
    Arcg:= Null;
    Parent := Null;
    return;
  end if;

  ParentL := new Liens_array_type(Arc'range);
  for i in Arc'range loop
    ParentL(i) := i;
  end loop;

  if D1 = 0.0 and D2 = 0.0 then
    Arcg := new Point_liste_reel'(Arc);
    Parent := ParentL;
    return;
  end if;

  Ligne_to_cercle(Arc,ParentL.all,PC,ParentC);
  Dilatation_gauche(PC.all,ParentC.all,D1,Arrondi,PCg,ParentCg);
  if Pcg = Null then
    Arcg := Null;
    Parent := null;
    GR_free_liens_array(ParentL);
    GR_free_liens_array(ParentC);
    GR_free_liens_array(ParentCg);
    Free_poly_segment(PC);
    Free_poly_segment(PCg);
    return;
  end if;
  Dilatation_droite(PCg.all,ParentCg.all,D2,False,PCd,ParentCd);
  if PCd /=null then
    Cercle_to_ligne(PCd.all,ParentCd.all,Arcg,Parent,Precision);
    Free_poly_segment(PCd);
    GR_free_liens_array(ParentCd);
  end if;
  GR_free_liens_array(ParentL);
  GR_free_liens_array(ParentCg);
  GR_free_liens_array(ParentC);
  Free_poly_segment(PC);
  Free_poly_segment(PCg);

end Fermeture_gauche;

---------------------------------
-- DILATATION_DROITE
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- dilatation d'une Poly-Ligne
-- Transforme la polyligne en Poly-segment
-- et appelle Dilatation_droite d'un Polysegment
---------------------------------
Procedure Dilatation_droite( Arc       : in  Point_liste_reel;
                             D         : in  float;
                             Arcd      : out Point_access_reel;
                             Parent    : out Liens_access_type;
                             Arrondi   : in  boolean := true;
                             Precision : in  float := Precision_defaut) is

PC, PCd: Poly_segment_access;
ParentC, ParentCd, ParentL: Liens_access_type; 

begin

  if Arc'length < 2 then
    Arcd:= Null;
    Parent := Null;
    return;
  end if;

  ParentL := new Liens_array_type(Arc'range);
  for i in Arc'range loop
    ParentL(i) := i;
  end loop;

  if D = 0.0 then
    Arcd := new Point_liste_reel'(Arc);
    Parent := ParentL;
    return;
  end if;

  Ligne_to_cercle(Arc,ParentL.all,PC,ParentC);
  Dilatation_droite(PC.all,ParentC.all,D,Arrondi,PCd,ParentCd);
  if PCd = Null then
    Arcd:= Null;
    Parent := Null;
    Free_poly_segment(PC);
    Free_poly_segment(PCd);
    GR_free_liens_array(ParentL);
    GR_free_liens_array(ParentCd);
    GR_free_liens_array(ParentC);
    return;
  end if;
  Cercle_to_ligne(PCd.all,ParentCd.all,Arcd,Parent,Precision);

  GR_free_liens_array(ParentL);
  GR_free_liens_array(ParentCd);
  GR_free_liens_array(ParentC);
  Free_poly_segment(PC);
  Free_poly_segment(PCd);

end Dilatation_droite;

---------------------------------
-- FERMETURE_DROITE
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- fermeture d'une Poly-Ligne
-- Transforme la Polyligne en Polysegment
-- Appelle Dilatation_gauche d'un Polysegment
-- Appelle Dilatation_droite d'un Polysegment
---------------------------------
Procedure Fermeture_droite( Arc       : in  Point_liste_reel;
                            D1        : in  float;
                            D2        : in  float;
                            Arcd      : out Point_access_reel;
                            Parent    : out Liens_access_type;
                            Arrondi   : in  boolean := true;
                            Precision : in  float := Precision_defaut) is

PC, PCd, PCg: Poly_segment_access;
ParentC, ParentCd, ParentCg, ParentL: Liens_access_type; 

begin
  if Arc'length < 2 then
    Arcd:= Null;
    Parent := Null;
    return;
  end if;

  ParentL := new Liens_array_type(Arc'range);
  for i in Arc'range loop
    ParentL(i) := i;
  end loop;

  if D1 = 0.0 and D2 = 0.0 then
    Arcd := new Point_liste_reel'(Arc);
    Parent := ParentL;
    return;
  end if;

  Ligne_to_cercle(Arc,ParentL.all,PC,ParentC);
  Dilatation_droite(PC.all,ParentC.all,D1,Arrondi,PCd,ParentCd);
  if Pcd = Null then
    Arcd := Null;
    Parent := Null;
    GR_free_liens_array(ParentL);
    GR_free_liens_array(ParentCd);
    GR_free_liens_array(ParentC);
    Free_poly_segment(PCg);
    Free_poly_segment(PCd);
    return;
  end if;
  Dilatation_gauche(PCd.all,ParentCd.all,D2,False,PCg,ParentCg);
  if PCg /= null then
    Cercle_to_ligne(PCg.all,ParentCg.all,Arcd,Parent,Precision);
    Free_poly_segment(PCg);
    GR_free_liens_array(ParentCg);
  end if;
  GR_free_liens_array(ParentL);
  GR_free_liens_array(ParentCd);
  GR_free_liens_array(ParentC);
  Free_poly_segment(PC);
  Free_poly_segment(PCd);

end Fermeture_droite;

---------------------------------
-- DILATATION_ARC
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- Dilate un arc, ie. calcul le lieu des points a une distance D de l'arc
-- Cree l'arc compose de l'aller et retour de l'arc original
-- Appelle Dilatation_gauche d'une polyligne
---------------------------------
Procedure Dilatation_arc( Arc       : in  Point_liste_reel;
                          D         : in  float;
                          Arcd      : out Point_access_reel;
                          Parent    : out Liens_access_type;
                          Precision : in  float := Precision_defaut) is  

Arc_AR : Point_liste_reel(Arc'first..Arc'last+Arc'length-1);

begin
  if Arc'length < 2 then
    Arcd:= Null;
    Parent := Null;
    return;
  end if;

  -- creation de l'arc constitue de l'arc original Aller et Retour
  for i in Arc'range loop
    Arc_AR(i) := Arc(i);
  end loop;
  for i in Arc'last+1..Arc'last+Arc'length-1 loop
    Arc_AR(i) := Arc(2*Arc'last-i);
  end loop;

  -- decalage de l'arc cree
  Dilatation_gauche(Arc_AR(Arc_AR'range),D,Arcd,Parent,true,Precision);

return;

end Dilatation_arc;

---------------------------------
-- FERMETURE_ARC
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- Dilate un arc, ie. calcul le lieu des points a une distance D de l'arc
-- puis erosion de la surface ainsi determinee
-- Cree l'arc compose de l'aller et retour de l'arc original
-- Appelle Dilatation_gauche d'une polyligne
---------------------------------
-- Attention, cette procedure est fausse pour des arc ou la premiere
-- dilatation aurait cree une surface a trous
---------------------------------
Procedure Fermeture_arc( Arc       : in  Point_liste_reel;
                         D1        : in  float;
                         D2        : in  float;
                         Arcf      : out Point_access_reel;
                         Parent    : out Liens_access_type;
                         Precision : in  float := Precision_defaut) is  

Arc_AR : Point_liste_reel(Arc'first..Arc'last+Arc'length-1);

begin
  if Arc'length < 2 then
    Arcf:= Null;
    Parent := Null;
    return;
  end if;

  -- creation de l'arc constitue de l'arc original Aller et Retour
  for i in Arc'range loop
    Arc_AR(i) := Arc(i);
  end loop;
  for i in Arc'last+1..Arc'last+Arc'length-1 loop
    Arc_AR(i) := Arc(2*Arc'last-i);
  end loop;

  -- decalage de l'arc cree                                          
  Fermeture_gauche(Arc_AR(Arc_AR'range),D1,D2,Arcf,Parent,true,Precision);

return;

end fermeture_arc;

---------------------------------
-- DILATATION_ARC
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- Appelle l'autre procedure dilatation_arc qui ne renvoit qu'un arc
-- Puis decoupe l'arc resultant en deux
---------------------------------
Procedure Dilatation_arc( Arc       : in Point_liste_reel;
                          D         : in float;
                          Arcg      : out Point_access_reel;
                          Parentg   : out Liens_access_type;
                          Arcd      : out Point_access_reel;
                          Parentd   : out Liens_access_type;
                          Precision : in  float := Precision_defaut) is 

Demi_tour : Natural;
Arcdg,Arcd1,Arcd2 : Point_access_reel;
Parentdg,Parentd1,Parentd2 : Liens_access_type;

begin
  if Arc(Arc'first) = Arc(Arc'last) then
  -- Cas d'un arc ferme
    Dilatation_gauche(Arc,D,Arcg,Parentg,true,Precision);
    Dilatation_droite(Arc,D,Arcd,Parentd,true,Precision);
    return;
  end if; 

  -- cas d'un arc non ferme

  Dilatation_arc(Arc,D,Arcdg,Parentdg,Precision);
  -- l'arc en retour est decoupee en deux: ses parties a gauche et a droite
  Demi_tour := Arcdg'last;
  for i in Parentdg'range loop
    if Parentdg(i) >= Arc'last then
      Demi_tour := i;
      exit;
    end if;
  end loop;
  if Demi_tour = Arcdg'first then
    Arcg := Null;
    Parentg := Null;
  else
    Arcg := new Point_liste_reel'(Arcdg(Arcdg'first..Demi_tour));
    Parentg := new Liens_array_type'(Parentdg(Parentdg'first..Demi_tour-1));
  end if;
  if Demi_tour = Arcdg'last then
    Arcd := Null;
    Parentd := Null;
    GR_free_Point_liste_reel(Arcdg);
    GR_free_liens_array(Parentdg);
    return;
  else
    Arcd1 := new Point_liste_reel'(Arcdg(Demi_tour..Arcdg'last));
    Parentd1:= new Liens_array_type'(Parentdg(Demi_tour..Parentdg'last));
  end if;                           

  -- l'arc a droite est inverse pour suivre le sens de l'arc original
  Arcd2 := new Point_liste_reel(1..Arcd1'length);
  Parentd2 := new Liens_array_type(1..Parentd1'length);
  for i in Arcd2'range loop
    Arcd2(i):=Arcd1(Arcd1'last+1-i);
  end loop;
  for i in Parentd2'range loop
    if Parentd1(Arcd1'last-i) /= 0 then
      Parentd2(i):=2*Arc'last-Parentd1(Arcd1'last-i)-1;
    else
      Parentd2(i):=0;
    end if;
  end loop;

  Arcd := new Point_liste_reel'(Arcd2.all);
  Parentd := new Liens_array_type'(Parentd2.all);

  GR_free_Point_liste_reel(Arcdg);
  GR_free_liens_array(Parentdg);
  GR_free_Point_liste_reel(Arcd1);
  GR_free_liens_array(Parentd1);
  GR_free_Point_liste_reel(Arcd2);
  GR_free_liens_array(Parentd2);

end Dilatation_arc;

---------------------------------
-- FERMETURE_ARC
---------------------------------
-- PROCEDURE EXPORTEE
---------------------------------
-- Appelle l'autre procedure fermeture_arc qui ne renvoit qu'un arc
-- Puis decoupe l'arc resultant en deux
---------------------------------
-- Attention, cette procedure est fausse pour des arc ou la premiere
-- dilatation aurait cree une surface a trous
---------------------------------
Procedure Fermeture_arc( Arc       : in  Point_liste_reel;
                         D1        : in  float;
                         D2        : in  float;
                         Arcg      : out Point_access_reel;
                         Parentg   : out Liens_access_type;
                         Arcd      : out Point_access_reel;
                         Parentd   : out Liens_access_type;
                         Precision : in  float := Precision_defaut) is  
Demi_tour : Natural;
Arcdg,Arcd1,Arcd2 : Point_access_reel;
Parentdg,Parentd1,Parentd2 : Liens_access_type;

begin
  if Arc(Arc'first) = Arc(Arc'last) then
  -- Cas d'un arc ferme
    Fermeture_gauche(Arc,D1,D2,Arcg,Parentg,true,Precision);
    Fermeture_droite(Arc,D1,D2,Arcd,Parentd,true,Precision);
    return;
  end if; 

  -- cas d'un arc non ferme
  Fermeture_arc(Arc,D1,D2,Arcdg,Parentdg,Precision);

  -- l'arc en retour est decoupe en deux: ses parties a gauche et a droite
  Demi_tour := Arcdg'last;

  for i in Arcdg'range loop
    if Parentdg(i) >= Arc'last then
      Demi_tour := i;
      exit;
    end if;
  end loop;
  if Demi_tour = Arcdg'first then
    Arcg := Null;
    Parentg := Null;
  else
    Arcg := new Point_liste_reel'(Arcdg(Arcdg'first..Demi_tour));
    Parentg := new Liens_array_type'(Parentdg(Parentdg'first..Demi_tour));
  end if;
  if Demi_tour = Arcdg'last then
    Arcd := Null;
    Parentd := Null;
    GR_free_Point_liste_reel(Arcdg);
    GR_free_liens_array(Parentdg);
    return;
  else
    Arcd1 := new Point_liste_reel'(Arcdg(Demi_tour..Arcdg'last));
    Parentd1:= new Liens_array_type'(Parentdg(Demi_tour..Parentdg'first));
  end if;                           

  -- l'arc a droite est inverse pour suivre le sens de l'arc original

  Arcd2 := new Point_liste_reel(1..Arcd1'length);
  Parentd2 := new Liens_array_type(1..Arcd1'length);
  for i in Arcd2'range loop
    Arcd2(i):=Arcd1(Arcd1'last+1-i);
  end loop;

  for i in Parentd2'range loop
    if Parentd1(Arcd1'last-i) /= 0 then
      Parentd2(i):=2*Arc'last-Parentd1(Arcd1'last-i)-1;
    else
      Parentd2(i):=0;
    end if;
  end loop;


  Arcd := new Point_liste_reel'(Arcd2.all);
  Parentd := new Liens_array_type'(Parentd2.all);

  GR_free_Point_liste_reel(Arcdg);
  GR_free_liens_array(Parentdg);
  GR_free_Point_liste_reel(Arcd1);
  GR_free_liens_array(Parentd1);
  GR_free_Point_liste_reel(Arcd2);
  GR_free_liens_array(Parentd2);
end Fermeture_arc;



---------------------       	
-- CONFLIT_SYMBO
---------------------
-- PARTIE SUR LA RECHERCHE ET LA QUALIFICATION DES CONFLITS
-- CARTOGRAPHIQUES DUS A LA SYMBOLISATION D'UNE LIGNE
---------------------
-- Sebastien Mustiere
-- Decembre 97
---------------------

--------------------------------------------
-- RECHERCHE_CONFLITS
--------------------------------------------
-- Detecte les lieux des empatements et des superpositions a gauche ou a droite
-- Renvoie un arc constitue des points de la ligne original et les nouveaux
-- points de decoupage
-- Pour un point de l'arc original Position(i)=No du point dans l'arc original,
-- pour un nouveau point Position(i)=0
-- Conflit(i)=true ou false suivant que le segment [ArcDec(i),ArcDec(i+1)] 
-- est en conflit ou non
--------------------------------------------
procedure Recherche_conflits(AB        : in  Arc_bords; 
                             Cote      : in  Type_cote;  
                             Tolerance : in  float;
                             ArcDec    : out Point_access_reel;
                             Position  : out Liens_access_type;
                             Conflit   : out Access_flag) is

Arc,Bord, ArcD : Point_access_reel;
Parent,Pos     : Liens_access_type;
Conf           : Access_flag;
Pt1,Pt2        : Point_type_reel;
N              : natural := 0;
Seuil          : float;

begin
  if Cote = Gauche then
    Bord := AB.Bordg;
    Parent := AB.Parentg;
  else
    Bord := AB.Bordd;
    Parent := AB.Parentd;
  end if;

  if Bord = null then
    -- il n'y a pas de bord, l'arc est donc considere completement en conflit
    Pos := new Liens_array_type(AB.arc'range);
    for i in Pos'range loop
      Pos(i) := i;
    end loop;
    Conf := new Liste_flag(AB.arc'first..AB.arc'last-1);
    Conf.all := (others => true);

    ArcDec := new Point_liste_reel'(AB.arc.all);
    Position := Pos;
    Conflit := Conf;

    return;
  end if;

  Seuil := AB.largeur*AB.largeur*Tolerance*Tolerance;
  Arc := AB.arc;

  -- Dans le pire des cas on va creer un arc de longueur < 3*Arc'length
  ArcD := new Point_liste_reel(1..Arc'length*3);
  Pos := new Liens_array_type(1..Arc'length*3);
  Conf := new Liste_flag(1..Arc'length*3);
  N := 0;
 
  -- ajout des points de Arc entre le debut et le premier arc parent
  for i in Arc'first..Parent(Bord'first) loop
    N := N+1;
    ArcD(N) := Arc(i);
    Pos(N) := i;
  end loop;
  if Trop_loin(Arc(Arc'first..Parent(Bord'first)),Bord(Bord'first),Seuil) then
    -- le debut de l'arc est en conflit 
    Conf(1..N) := (others => true);
    -- Pt2 = projete de Bord'first sur Parent(Bord'first)
    Pt2:=Projection_segment(Bord(Bord'first), Arc(Parent(Bord'first)),
                                              Arc(Parent(Bord'first)+1));
    if Pt2 = ArcD(N) then
      -- Pt2 a deja ete mis dans la liste de points de decoupage
      Conf(N) := false;
    else
      -- Pt2 n'a pas deja ete mis dans la liste de points de decoupage
      N := N+1;
      ArcD(N) := Pt2;
      Conf(N) := false;
      Pos(N) := 0;
    end if;
  else
    -- ca commence en douceur sans conflit
    Conf(1..N) := (others => false);
  end if;

  for i in Bord'first+1..Bord'last-1 loop
    if Parent(i) /= Parent(i-1) and Parent(i-1) /= 0 then
      -- on passe a la partie du bord issue d'un nouveau decalage
      if Parent(i) = 0 then
        -- symptome caracteristique de la SUPERPOSITION INTERNE chronique
        -- Ajout de Pt1, projete de Bord(i) sur Parent(i-1)
        Pt1:= projection_segment(Bord(i),Arc(Parent(i-1)),Arc(Parent(i-1)+1));
        if Pt1 = ArcD(N) then
          -- Pt1 a deja ete mis dans la liste de points de decoupage
          Conf(N) := true;
        else
          -- Pt1 n'a pas deja ete mis dans la liste de points de decoupage
          N := N+1;
          ArcD(N) := Pt1;
          Conf(N) := true;
          Pos(N) := 0;
        end if;
        -- ajout des points de Arc entre Pt1 et Pt2
        if i=Bord'last-1 then
           -- CAs tres  particulier de conflits dua un trou en fin de ligne
           for j in Parent(i-1)+1..Arc'last loop
               if Arc(j) = ArcD(N) then
                  Pos(N) := j;
               else
                  N := N+1;
                  ArcD(N) := Arc(j);
                  Conf(N) := true;
                  Pos(N) := j;
               end if;
           end loop;
           -- dimensionnement des tableaux en sortie
           ArcDec := new Point_liste_reel'(ArcD(1..N));
           Position := new Liens_array_type'(Pos(1..N));
           Conflit := new Liste_flag'(Conf(1..N-1));

           -- Deallocations
           GR_free_point_liste_reel(ArcD);
           GR_free_liens_array(Pos);
           Free_flag(Conf);
           -- Sortie car das ce cas on a atteint le dernier point   
           return;
        end if;   
        for j in Parent(i-1)+1..Parent(i+1) loop
          if Arc(j) = ArcD(N) then
            Pos(N) := j;
          else
            N := N+1;
            ArcD(N) := Arc(j);
            Conf(N) := true;
            Pos(N) := j;
          end if;
        end loop;
        -- ajout de Pt2, projete de Bord(i+1) sur Parent(i+1)
        Pt2:= projection_segment(Bord(i+1),Arc(Parent(i+1)),Arc(Parent(i+1)+1));
        if Pt2 = ArcD(N) then
          -- Pt2 a deja ete mis dans la liste de points de decoupage
          Conf(N) := false;
        else
          -- Pt2 n'a pas deja ete mis dans la liste de points de decoupage
          N := N+1;
          ArcD(N) := Pt2;
          Conf(N) := false;
          Pos(N) := 0;
        end if;

      elsif Trop_loin(Arc(Parent(i-1)+1..Parent(i)),Bord(i),Seuil) then
        -- symptome caracteristique de l'EMPATEMENT du sous-arc {Pt1..Pt2}
        -- Ajout de Pt1, projete de Bord(i) sur Parent(i-1)
  -- A faire ###################################
  -- Traitement du cas vicieux ou le conflit de superposition ne se realise
  -- qu'au niveau d'un point de l'arc
        Pt1 := projection_segment(Bord(i),Arc(Parent(i-1)),Arc(Parent(i-1)+1));
        if Pt1 = ArcD(N) then
          -- Pt1 a deja ete mis dans la liste de points de decoupage
          Conf(N) := true;
        else
          -- Pt1 n'a pas deja ete mis dans la liste de points de decoupage
          N := N+1;
          ArcD(N) := Pt1;
          Conf(N) := true;
          Pos(N) := 0;
        end if;
        -- ajout des points de Arc entre Pt1 et Pt2
        for j in Parent(i-1)+1..Parent(i) loop
          if Arc(j) = ArcD(N) then
            Pos(N) := j;
          else
            N := N+1;
            ArcD(N) := Arc(j);
            Conf(N) := true;
            Pos(N) := j;
          end if;
        end loop;
        -- Ajout de Pt2, projete de Bord(i) sur Parent(i)
        Pt2 := projection_segment(Bord(i),Arc(Parent(i)),Arc(Parent(i)+1));
        if Pt2 = ArcD(N) then
          -- Pt2 a deja ete mis dans la liste de points de decoupage
          Conf(N) := false;
        else
          -- Pt2 n'a pas deja ete mis dans la liste de points de decoupage
          N := N+1;
          ArcD(N) := Pt2;
          Conf(N) := false;
          Pos(N) := 0;
        end if;
      else
        -- le sous arc Parent(i-1)+1..Parent(i) n'est PAS EMPATE
        -- ajout des points de Arc entre Parent(i-1)+1 et Parent(i)
        for j in Parent(i-1)+1..Parent(i) loop
          if Arc(j) = ArcD(N) then
            Pos(N) := j;
            Conf(N) := false;
          else
            N := N+1;
            ArcD(N) := Arc(j);
            Conf(N) := false;
            Pos(N) := j;
          end if;
        end loop;
      end if;
    end if;
  end loop;

  if Trop_loin(Arc(Parent(Bord'last-1)+1..Arc'last),Bord(Bord'last),Seuil) then
    -- la fin de l'arc est en conflit
    Pt1:=Projection_segment(Bord(Bord'last), Arc(Parent(Bord'last-1)),
                                             Arc(Parent(Bord'last-1)+1));
    if Pt1 = ArcD(N) then
      -- Pt1 a deja ete mis dans la liste de points de decoupage
      Conf(N) := true;
    else
      -- Pt1 n'a pas deja ete mis dans la liste de points de decoupage
      N := N+1;
      ArcD(N) := Pt1;
      Conf(N) := true;
      Pos(N) := 0;
    end if;
    for j in Parent(Bord'last-1)+1..Arc'last loop
      if Arc(j) = ArcD(N) then
        Pos(N) := j;
      else
        N := N+1;
        ArcD(N) := Arc(j);
        Conf(N) := true;
        Pos(N) := j;
      end if;
    end loop;
  else
    -- ca finit en douceur sans conflits
    for j in Parent(Bord'last-1)+1..Arc'last loop
      if Arc(j) = ArcD(N) then
        Pos(N) := j;
        Conf(N) := false;
      else
        N := N+1;
        ArcD(N) := Arc(j);
        Conf(N) := false;
        Pos(N) := j;
      end if;
    end loop;
  end if;

  -- dimensionnement des tableaux en sortie
  ArcDec := new Point_liste_reel'(ArcD(1..N));
  Position := new Liens_array_type'(Pos(1..N));
  Conflit := new Liste_flag'(Conf(1..N-1));

  -- Deallocations
  GR_free_point_liste_reel(ArcD);
  GR_free_liens_array(Pos);
  Free_flag(Conf);

end;


--------------------------------------------
-- FONCTIONS EXPORTEES
--------------------------------------------


--------------------------------------------
-- INDICATEUR_CONFLITS 
--------------------------------------------
-- CETTE FONCTION RENVOIE UN INDICATEUR DETAILLE DE L'IMPORTANCE DES
-- CONFLITS DANS L'ENSEMBLE D'UNE POLYLIGNE
--------------------------------------------

Function Indicateur_conflits(Arc          : Point_liste_type;
                             Largeur      : float;
                             Tolerance    : float:=1.7) 
		             return Force_conflit is
                 
Cd, Cg   : float := 0.0;
Decalage : float;
Arcf     : Point_liste_reel(Arc'range);
AB       : Arc_bords;
Conf     : Access_flag;
ArcD     : Point_access_reel;
Pos      : Liens_access_type;

begin
  Arcf := Reel(Arc);
  AB.arc := new Point_liste_reel'(Arcf);
  Decalage := Largeur;
  AB.Largeur := Decalage;
  Dilatation_arc(Arcf,Decalage,AB.Bordg,AB.Parentg,AB.Bordd,AB.Parentd);

  -- A gauche
  Recherche_conflits(AB,Gauche,Tolerance,ArcD,Pos,Conf);
  for i in Conf'range loop
    if Conf(i) then 
      Cg := Cg+Norme(ArcD(i),ArcD(i+1));
    end if;
  end loop;
  GR_free_point_liste_reel(ArcD);
  Free_flag(Conf);
  GR_free_liens_array(Pos);

  -- A droite
  Recherche_conflits(AB,Droite,Tolerance,ArcD,Pos,Conf);
  for i in Conf'range loop
    if Conf(i) then 
      Cd := Cd+Norme(ArcD(i),ArcD(i+1));
    end if;
  end loop;
  GR_free_point_liste_reel(ArcD);
  Free_flag(Conf);
  GR_free_liens_array(Pos); 

  -- Deallocations
  GR_free_point_liste_reel(AB.arc);
  GR_free_point_liste_reel(AB.Bordg);
  GR_free_point_liste_reel(AB.Bordd);
  GR_free_liens_array(AB.Parentg);
  GR_free_liens_array(AB.Parentd);

  return (Cg,Cd);

end Indicateur_conflits;

-------------------------------------------------------------
-- CETTE FONCTION RENVOIE LES POINTS DE DECOUPAGE DE LA POLYLIGNE
-- EN SOUS-ARCS HOMOGENES VIS A VIS DE LA PRESENCE DE CONFLITS
-- NB: Si on decoupe l'arc en N sous-arc, alors decoupage est un tableau 
-- de N+1 elements, les numeros des points de decoupage

Function Decoupage_conflits(Arc          : Point_liste_type;
                            Largeur      : float;
                            Separabilite : float := 1.7;
                            Tolerance    : float := 1.7) 
			    return Point_access_type is

Arcf                    : Point_Liste_reel(Arc'range);
N1,N2,N,id,ig,Sd,Sg     : natural;
Lmin, Decalage, Seuil   : float;
Conf_courant            : boolean;
Confg,Confd, Conf,Conf1 : access_flag;
Pt                      : Point_type;
Posg,Posd,Dec1,Dec2     : Liens_access_type;
ArcDg, ArcDd, ArcD      : Point_access_reel;
Dec, Decoupage          : Point_access_type;
AB                      : Arc_bords;

begin
  -- extraction de l'arc, de sa largeur, de ses bords, des parents des bords 
  Arcf := Reel(Arc);
  AB.arc := new Point_liste_reel'(Arcf);
  Decalage := Largeur;
  AB.Largeur := Decalage;
  Dilatation_arc(Arcf,Decalage,AB.Bordg,AB.Parentg,AB.Bordd,AB.Parentd);

  -- Detection des conflits internes pour chaque segment
  Recherche_conflits(AB,Gauche,Tolerance,ArcDg,Posg,Confg);
  Recherche_conflits(AB,Droite,Tolerance,ArcDd,Posd,Confd);

  -- Creation de l'arc constitue des points de ArcDg et ArcDd
  ArcD := new Point_liste_reel(1..ArcDg'length+ArcDd'length);
  Conf := new Liste_flag(1..ArcDg'length+ArcDd'length);
  ig := ArcDg'first;
  id := ArcDd'first;
  N := 0;

  loop
    if ig = ArcDg'last and id = ArcDd'last then 
      N := N+1;
      ArcD(N) := ArcDg(ig);
      exit;
    end if;
    if Posg(ig) /= 0 and Posg(ig) = Posd(id) then
      -- Le prochain point est ArcDd(id)=ArcDg(ig)
      N := N+1;
      ArcD(N) := ArcDg(ig);
      Conf(N) := Confd(id) or Confg(ig);
      Sg := ig;
      Sd := id;
      id := id+1;
      ig := ig+1;
    else
      if ArcDg(ig) = ArcDd(id) then
        -- Le prochain point est ArcDd(id)=ArcDg(ig)
        N := N+1;
        ArcD(N) := ArcDg(ig);
        Conf(N) := Confd(id) or Confg(ig);
        id := id+1;
        ig := ig+1;
      elsif Norme2(ArcDg(ig)-ArcDg(Sg)) > Norme2(ArcDd(id)-ArcDd(Sd)) then
        -- Le prochain point est ArcDd(id)
        N := N+1;
        ArcD(N) := ArcDd(id);
        Conf(N) := Confd(id) or Confg(ig-1);
        id := id+1;
      else 
        -- Le prochain point est ArcDg(ig)
        N := N+1;
        ArcD(N) := ArcDg(ig);
        Conf(N) := Confd(id-1) or Confg(ig);
        ig := ig+1;
      end if;
    end if;
  end loop;

  -- Decoupage de la ligne en lignes homogenes vis a vis de
  -- presence/absence d'un conflit (tous types de conflits confondus)
  Dec1 := new Liens_array_type(1..N);
  Conf1 := new Liste_flag(1..N-1);

  Conf_courant := Conf(1);
  Dec1(1) := 1;
  Conf1(1) := Conf_courant;
  N1 := 1;

  for i in 2..N-1 loop
    if Conf(i) /= Conf_courant then
      Conf_courant := Conf(i);
      N1 := N1+1;
      Dec1(N1) := i;
      Conf1(N1) := Conf_courant;
    end if;
  end loop;
  N1 := N1+1;
  Dec1(N1) := N;

  -- agregation des sous-arcs crees afin de ne pas creer d'arcs ou il n'y
  -- a pas de conflit ET de longueur non significative
  Lmin := AB.largeur*Separabilite;
  Dec2 := new Liens_array_type(1..N1);
  Dec2(1) := ArcDg'first;
  N2 := 1;
  for i in 1..N1-1 loop
    if not Conf1(i) and Long_curvi(ArcD(Dec1(i)..Dec1(i+1))) < Lmin then
      -- arc de longueur non significative suppression du dernier point
      if N2 /= 1 then
        N2 := N2-1;
      end if;
    else
      -- arc de longueur significative ou avec conflit, ajout du point suivant
      N2 := N2+1;
      Dec2(N2) := Dec1(i+1); 
    end if;
  end loop;
  -- si le dernier arc etait trop petit le dernier point n'a pas ete ajoute
  if Dec2(N2) /= N then
    N2 := N2+1;
    Dec2(N2) := N; 
  end if;

  Dec := new Point_liste_type(1..N2);
  N1 := 1;
  Dec(1):=(integer(ArcD(Dec2(1)).coor_X),integer(ArcD(Dec2(1)).coor_Y));
  for i in 2..N2 loop
    Pt := (integer(ArcD(Dec2(i)).coor_X),integer(ArcD(Dec2(i)).coor_Y));
    if Pt /= Dec(N1) then
      N1 := N1+1;
      Dec(N1):=Pt;
    end if;
  end loop;


  if N1 = 1 then
    -- peut arriver pour un arc ferme
    Decoupage := new Point_liste_type'(Dec(1),Dec(1));
  else
    Decoupage := new Point_liste_type'(Dec(1..N1));
  end if;

  -- Deallocations
  Free_flag(Confg);
  Free_flag(Confd);
  Free_flag(Conf1);
  Free_flag(Conf);
  GR_free_liens_array(Posg);
  GR_free_liens_array(Posd);
  GR_free_liens_array(Dec1);
  GR_free_liens_array(Dec2);
  GR_free_point_liste(Dec);
  GR_free_point_liste_reel(ArcDg);
  GR_free_point_liste_reel(ArcDd);
  GR_free_point_liste_reel(ArcD);
  GR_free_point_liste_reel(AB.arc);
  GR_free_point_liste_reel(AB.Bordg);
  GR_free_point_liste_reel(AB.Bordd);
  GR_free_liens_array(AB.Parentg);
  GR_free_liens_array(AB.Parentd);

  return Decoupage;

end Decoupage_conflits;
	----------------------------------------------------
-- procedure identique a decoupaqge_conflits mais rajoute en plus
-- en sortie l'information sur conflit ou non
-- NB: Si on decoupe l'arc en N sous-arc, alors decoupage est un tableau 
-- de N+1 elements, les numeros des points de decoupage

Procedure DECOUPAGE_CONFLITS_TROU(Arc : Point_liste_type;
                            Largeur      : in float;
                            Separabilite : in float := 1.7;
                            Tolerance    : in float := 1.7; 
			    Pts_dec : out  Point_access_type;
                            Conf3 : out access_flag ) is

Arcf                    : Point_Liste_reel(Arc'range);
N1,N2,N,id,ig,Sd,Sg     : natural;
Lmin, Decalage, Seuil   : float;
Conf_courant,Conf_pt            : boolean;
Confg,Confd, Conf,Conf1,Conf2,Conf_dec : access_flag;
Pt                      : Point_type;
Posg,Posd,Dec1,Dec2     : Liens_access_type;
ArcDg, ArcDd, ArcD      : Point_access_reel;
Dec, Decoupage          : Point_access_type;
AB                      : Arc_bords;

begin
  -- extraction de l'arc, de sa largeur, de ses bords, des parents des bords 
  Arcf := Reel(Arc);
  AB.arc := new Point_liste_reel'(Arcf);
  Decalage := Largeur;
  AB.Largeur := Decalage;
  Dilatation_arc(Arcf,Decalage,AB.Bordg,AB.Parentg,AB.Bordd,AB.Parentd);

  -- Detection des conflits internes pour chaque segment
  Recherche_conflits(AB,Gauche,Tolerance,ArcDg,Posg,Confg);
  Recherche_conflits(AB,Droite,Tolerance,ArcDd,Posd,Confd);

  -- Creation de l'arc constitue des points de ArcDg et ArcDd
  ArcD := new Point_liste_reel(1..ArcDg'length+ArcDd'length);
  Conf := new Liste_flag(1..ArcDg'length+ArcDd'length);
  ig := ArcDg'first;
  id := ArcDd'first;
  N := 0;

  loop
    if ig = ArcDg'last and id = ArcDd'last then 
      N := N+1;
      ArcD(N) := ArcDg(ig);
      exit;
    end if;
    if Posg(ig) /= 0 and Posg(ig) = Posd(id) then
      -- Le prochain point est ArcDd(id)=ArcDg(ig)
      N := N+1;
      ArcD(N) := ArcDg(ig);
      Conf(N) := Confd(id) or Confg(ig);
      Sg := ig;
      Sd := id;
      id := id+1;
      ig := ig+1;
    else
      if ArcDg(ig) = ArcDd(id) then
        -- Le prochain point est ArcDd(id)=ArcDg(ig)
        N := N+1;
        ArcD(N) := ArcDg(ig);
        Conf(N) := Confd(id) or Confg(ig);
        id := id+1;
        ig := ig+1;
      elsif Norme2(ArcDg(ig)-ArcDg(Sg)) > Norme2(ArcDd(id)-ArcDd(Sd)) then
        -- Le prochain point est ArcDd(id)
        N := N+1;
        ArcD(N) := ArcDd(id);
        Conf(N) := Confd(id) or Confg(ig-1);
        id := id+1;
      else 
        -- Le prochain point est ArcDg(ig)
        N := N+1;
        ArcD(N) := ArcDg(ig);
        Conf(N) := Confd(id-1) or Confg(ig);
        ig := ig+1;
      end if;
    end if;
  end loop;

  -- Decoupage de la ligne en lignes homogenes vis a vis de
  -- presence/absence d'un conflit (tous types de conflits confondus)
  Dec1 := new Liens_array_type(1..N);
  Conf1 := new Liste_flag(1..N-1);

  Conf_courant := Conf(1);
  Dec1(1) := 1;
  Conf1(1) := Conf_courant;
  N1 := 1;

  for i in 2..N-1 loop
    if Conf(i) /= Conf_courant then
      Conf_courant := Conf(i);
      N1 := N1+1;
      Dec1(N1) := i;
      Conf1(N1) := Conf_courant;
    end if;
  end loop;
  N1 := N1+1;
  Dec1(N1) := N;

  -- agregation des sous-arcs crees afin de ne pas creer d'arcs ou il n'y
  -- a pas de conflit ET de longueur non significative
  Lmin := AB.largeur*Separabilite;
  Dec2 := new Liens_array_type(1..N1);
  Conf2:=new Liste_flag(1..N1-1);

  Dec2(1) := ArcDg'first;
  N2 := 1;
  Conf2(1):=Conf1(1);
  for i in 1..N1-1 loop
    if not Conf1(i) and Long_curvi(ArcD(Dec1(i)..Dec1(i+1))) < Lmin then
      -- arc de longueur non significative suppression du dernier point
      if N2 /= 1 then
        N2 := N2-1;
      end if;
    else
      -- arc de longueur significative ou avec conflit, ajout du point suivant
      N2 := N2+1;
      Dec2(N2) := Dec1(i+1); 
         if i/= N1-1 then
            Conf2(N2):= Conf1(i+1);
         end if;
    end if;
  end loop;
  -- si le dernier arc etait trop petit le dernier point n'a pas ete ajoute
  if Dec2(N2) /= N then
    N2 := N2+1;
    Dec2(N2) := N; 
  end if;

  Dec := new Point_liste_type(1..N2);
  Conf_dec := new Liste_flag(1..N2-1);
  N1 := 1;
  Dec(1):=(integer(ArcD(Dec2(1)).coor_X),integer(ArcD(Dec2(1)).coor_Y));
  Conf_dec(1):=Conf2(1);
  for i in 2..N2 loop
    Pt := (integer(ArcD(Dec2(i)).coor_X),integer(ArcD(Dec2(i)).coor_Y));
    if i/=N2 then 
       Conf_pt:=Conf2(i);
    end if;   
    if Pt /= Dec(N1) then
      N1 := N1+1;
      Dec(N1):=Pt;
      if i/=N2 then
         Conf_dec(N1):=Conf_pt;
      end if;
    end if;
  end loop;


  if N1 = 1 then
    -- peut arriver pour un arc ferme
    Pts_dec := new Point_liste_type'(Dec(1),Dec(1));
    Conf3:= new Liste_flag'(Conf_dec(1),Conf_dec(1));
  else
    Pts_dec := new Point_liste_type'(Dec(1..N1));
    Conf3:= new Liste_flag'(Conf_dec(1..N1-1));
  end if;

  -- Deallocations
  Free_flag(Confg);
  Free_flag(Confd);
  Free_flag(Conf1);
  FREE_FLAG(Conf_dec);
  Free_flag(Conf2);
  Free_flag(Conf);
  GR_free_liens_array(Posg);
  GR_free_liens_array(Posd);
  GR_free_liens_array(Dec1);
  GR_free_liens_array(Dec2);
  GR_free_point_liste(Dec);
  GR_free_point_liste_reel(ArcDg);
  GR_free_point_liste_reel(ArcDd);
  GR_free_point_liste_reel(ArcD);
  GR_free_point_liste_reel(AB.arc);
  GR_free_point_liste_reel(AB.Bordg);
  GR_free_point_liste_reel(AB.Bordd);
  GR_free_liens_array(AB.Parentg);
  GR_free_liens_array(AB.Parentd);


end DECOUPAGE_CONFLITS_TROU;


-------------------------------------------------------------------------------
--DECOUPAGE_CONFLITS_TROU_GAUCHE
--procedure sortant une liste des points de depart de conflits sur un seul cote
-------------------------------------------------------------------------------

Procedure DECOUPAGE_CONFLITS_TROU_GD(GD : in type_cote;
                            Arc : Point_liste_type;
                            Largeur     : in float ;
                            Separabilite : in float := 1.7;
                            Tolerance    : in float := 1.7;
                            Pts_dec2 : out  Point_access_type;
                            Conf4 : out access_flag ) is

Arcf                    : Point_Liste_reel(Arc'range);
id,ig,Sd,Sg     : natural;
L,Lmin, Decalage, Seuil,Tolerance2   : float;
Conf_courant,Conf_pt            : boolean;
Confg,Confd, Conf,Conf1,Conf2,Conf_dec,
Confg2,Confg3,conf10,conf20,Conf21 : access_flag;
Pts_dec1 : Point_access_reel;
Pt                      : Point_type;
Posg,Posd,Dec1,Dec4,Dec10,Dec20     : Liens_access_type;
ArcDg, ArcDd, ArcD      : Point_access_reel;
Dec, Decoupage,Pts_dec3          : Point_access_type;
AB                      : Arc_bords;				
N5,N6,N7,N20 : integer;


begin
  -- extraction de l'arc, de sa largeur, de ses bords, des parents des bords
  Arcf := Reel(Arc);
  AB.arc := new Point_liste_reel'(Arcf);
  Decalage := Largeur;
  AB.Largeur := Decalage;
  DILATATION_ARC(Arcf,Decalage,AB.Bordg,AB.Parentg,AB.Bordd,AB.Parentd);

  -- Detection des conflits internes a gauche ou a droite
  Tolerance2 := 2.5 ;
  if GD=Gauche then 
     RECHERCHE_CONFLITS(AB,Gauche,Tolerance2,ArcDg,Posg,Confg);
     N5:=ArcDg'length;
     Pts_dec1:=new Point_liste_reel'(ArcDg(1..N5));
     Confg2:=new Liste_flag'(Confg(1..N5-1));
  else 
     RECHERCHE_CONFLITS(AB,Droite,Tolerance2,ArcDd,Posd,Confd);
     N5:=ArcDd'length;
     Pts_dec1:=new Point_liste_reel'(ArcDd(1..N5));
     Confg2:=new Liste_flag'(Confd(1..N5-1));
  end if;


--decoupage de la ligne en lignes homogenes vis a vis des conflits
  Dec10 := new Liens_array_type(1..N5);
  Conf10 := new Liste_flag(1..N5-1);

  Conf_courant := Confg2(1);
  Dec10(1) := 1;
  Conf10(1) := Conf_courant;
  N6 := 1;

  for i in 2..N5-1 loop
    if Confg2(i) /= Conf_courant then
      Conf_courant := Confg2(i);
      N6 := N6+1;
      Dec10(N6) := i;
      Conf10(N6) := Conf_courant;
    end if;
  end loop;
  N6:=N6+1;
  Dec10(N6):= N5;
--------------------------------------------------------------------------------

  -- agregation des sous-arcs crees afin de ne pas creer d'arcs ou il n'y
  -- a pas de conflit ET de longueur non significative
  Lmin := AB.largeur*Separabilite;
  Dec20 := new Liens_array_type(1..N6);
  Conf21:=new Liste_flag(1..N6-1);

  Dec20(1) := Dec10'first;
  N20 := 1;
  Conf21(1):=Conf10(1);
  for i in 1..N6-1 loop
    if GD=Gauche then
       L:=Long_curvi(ArcDg(Dec10(i)..Dec10(i+1)));
    else
       L:=Long_curvi(ArcDd(Dec10(i)..Dec10(i+1)));
    end if;
    if not Conf10(i) and L < Lmin then
      -- arc de longueur non significative suppression du dernier point
      if N20 /= 1 then
        N20 := N20-1;
      end if;
    else
      -- arc de longueur significative ou avec conflit, ajout du point suivant
      N20 := N20+1;
      Dec20(N20) := Dec10(i+1);
         if i/= N6-1 then
            Conf21(N20):= Conf10(i+1);
         end if;
    end if;
  end loop;
  -- si le dernier arc etait trop petit le dernier point n'a pas ete ajoute
  if Dec20(N20) /= N5 then
    N20 := N20+1;
    Dec20(N20) := N5;
  end if;
-------------------------------------------------------------------------------


--passage en Point_liste_type
  
  Pts_dec3 := new Point_liste_type(1..N20);
  Conf20 := new Liste_flag(1..N20-1);
  N7 := 1;
  Pts_dec3(1):=(integer(Pts_dec1(Dec20(1)).coor_X),integer(Pts_dec1(Dec20(1)).coor_Y));
  Conf20(1):=Conf21(1);
  for i in 2..N20 loop
    Pt := (integer(Pts_dec1(Dec20(i)).coor_X),
          integer(Pts_dec1(Dec20(i)).coor_Y));
        if i/=N20 then 
           Conf_pt:=conf21(i);
        end if;
    if Pt /= Pts_dec3(N7) then
      N7 := N7+1;
      Pts_dec3(N7):=Pt;
      if i/=N20 then
         Conf20(N7):=Conf_pt;
      end if;
    end if;
  end loop;

--cas de l'arc ferme
  if N7=1 then
     Pts_dec2:=new Point_liste_type'(Pts_dec3(1),Pts_dec3(1));
     Conf4:=new Liste_flag'(Conf20(1),Conf20(1));
  else
     Pts_dec2:=new Point_liste_type'(Pts_dec3(1..N7));
     Conf4:=new Liste_flag'(Conf20(1..N7-1));
  end if;
                   
--Deallocations
  FREE_FLAG(Confg);
  FREE_FLAG(Confg2);
  FREE_FLAG(Conf10);
  FREE_FLAG(Conf20);
  FREE_FLAG(Conf21);
  GR_FREE_LIENS_ARRAY(Posg);
  GR_FREE_LIENS_ARRAY(Dec10);
  GR_FREE_LIENS_ARRAY(Dec20);
  GR_FREE_POINT_LISTE_REEL(ArcDg);
  GR_FREE_POINT_LISTE_REEL(Pts_dec1);
  GR_FREE_POINT_LISTE(Pts_dec3);
  GR_FREE_POINT_LISTE_REEL(AB.arc);
  GR_FREE_POINT_LISTE_REEL(AB.Bordg);
  GR_FREE_POINT_LISTE_REEL(AB.Bordd);
  GR_FREE_LIENS_ARRAY(AB.Parentg);
  GR_FREE_LIENS_ARRAY(AB.Parentd);

  end DECOUPAGE_CONFLITS_TROU_GD;	   
------------------------------------------------------------------------------
--Fonction ARC_PROCHE_TROU qui renvoie un booleen precisant si les sous-arcs
--concernes ont des longueurs voisines
--sert a appareiller entre eux les sous-arcs createur de trous dans le symbole
-----------------------------------------------------------------------------

Function ARC_PROCHE_TROU( TP1 : in  Point_liste_type;
                          TP2 : in Point_liste_type;
                          ecart : in float:= 0.05)
                          return boolean is

N1,N2,calcul : float:=0.0;

begin

for i in TP1'first..TP1'last-1 loop
    N1:=N1+NORME(TP1(i),TP1(i+1));
end loop;

for i in TP2'first..TP2'last-1 loop
    N2:=N2+NORME(TP2(i),TP2(i+1));
end loop;

if N1>=N2 then                      
   calcul:=(N2-N1)/N1;
else
   calcul:=(N1-N2)/N2;
end if;

if calcul<ecart then
   return true;
else
   return false;
end if;

end ARC_PROCHE_TROU;


end Morpholigne;


