--============================ CORPS DU PAQUETAGE TIFF_IO ===============================

-- ce fichier est compilable sous ObjectAda 7.2 
-- les 'read' d'ObjectAda 7.2 lisent les octets a l'envers de ceux d'ObjectAda 7.1     
-- les differences sont signalees en remarque

with text_io; use text_io;
with ada.integer_text_io;
with ada.streams.stream_io; use ada.streams.stream_io;
with ada.strings.unbounded; use ada.strings.unbounded;
with inipan; use inipan;

package body TIFF_IO is
 
---------------------------------------------------------------------------
-- NOM : Inverse_2
-- TRAITEMENT : 1.1.1 ; 1.2.1 ; 1.3.1
-- DESCRIPTION : procedures d'inversion des elements d'une chaine de 2 car.
---------------------------------------------------------------------------
procedure inverse2(s : in out string_2) is
  m : string_2 := s;
begin
 	m(1) := s(2);
 	m(2) := s(1);
  s := m;
end inverse2; 

--------------------------------------------------------------------------
-- NOM : Inverse_4
-- TRAITEMENT : 1.1.2 ; 1.2.2 ; 1.3.2
-- DESCRIPTION : procedures d'inversion des elements d'une chaine de 4 car.
--------------------------------------------------------------------------
procedure inverse4(s : in out string_4) is
  m : string_4 := s;
begin
  m(1) := s(4);
  m(2) := s(3);
  m(3) := s(2);
  m(4) := s(1);
  s := m;
end inverse4;
 
-------------------------------------------------------------                                          
-- NOM         : Read_IFH
-- TRAITEMENT  : 1.1
-- DESCRIPTION : procedure de lecture de l'en-tete : HIPO 1.1
-------------------------------------------------------------
procedure Read_IFH(fichier_tiff	: ada.streams.stream_io.file_type;
                   IFH 			: out T_IFH;
                   bigendian 		: out boolean ) is
	  
	s 		:  ada.streams.stream_io.stream_access;
	pos : ada.streams.stream_io.positive_count;
begin
  s := ada.streams.stream_io.stream(fichier_tiff);
  t_ifh'read(S,ifh);       
  if ifh.byte_order = "MM" then
   	bigendian := true;
  else
   	bigendian := false;
    inverse2(ifh.arbitrary_number);  -- dans la clause if et non else
    inverse4(ifh.offset_ifd1);       -- sous ObjectAda 7.1 !
  end if;
end Read_IFH;


----------------------------------------------------------------------                                          
-- NOM         : Read_IFDs
-- TRAITEMENT  : 1.2
-- DESCRIPTION : procedure de lecture des IFDS ; recuperation des Tags
----------------------------------------------------------------------
procedure Read_IFDs(	fichier_tiff 	: in ada.streams.stream_io.file_type;
                      IFH 		 	: in T_IFH;
                      bigendian 		: in boolean;
                      Liste_IFDs 		: out T_Liste_IFDs;
                      nbifd 			: out natural ) is

  s 			:  ada.streams.stream_io.stream_access;
  IFD_Courant	: Acces_IFD;
  adresse 		:  ada.streams.stream_io.count;
  NDE_string 	: string_2;
begin
  -- Positionnement a l'adresse de l'ifd1 
  ---------------------------------------
  -- attention : le positionnement se fait en nb d'enregistrements(1 enreg. = 1 octet) 
    --et le 1er octet a l'adresse 0
  adresse := ada.streams.stream_io.count(convert4(IFH.offset_ifd1)+1);
  ada.streams.stream_io.set_index(fichier_tiff, adresse);
  
  -- 1/ Lecture des IFDs
  ----------------------
  s := ada.streams.stream_io.stream(fichier_tiff);
  nbifd := 0;
  
  loop
	   -- 1.1/ lecture du nb de tags pour un IFD
	   -----------------------------------------
	  string_2'read(s,NDE_string);
    if bigendian=false then   -- if Bigendian=True sous ObjectAda 7.1 !
	  inverse2(NDE_string);
	end if;
	   
		  -- 1.2/ Cr�ation d'un espace-memoire pour l'IFD que l'on va lire
	   ----------------------------------------------------------------
	  IFD_Courant := new IFD(NDE=>convert2(NDE_string));
		
		  -- 1.3/ On lit la suite des tags
	   --------------------------------
	  for i in 1..IFD_Courant.NDE loop
      Typ_Tag'read(s,ifd_courant.tags(i));
      if bigendian=false then   -- if Bigendian=True sous ObjectAda 7.1 !
        inverse2(IFD_Courant.tags(i).tagid);
        inverse2(IFD_Courant.tags(i).typ);
        inverse4(IFD_Courant.tags(i).nbvals);
      end if;
      inverse4(IFD_Courant.tags(i).valoff);  -- ligne a ne pas mettre sous ObjectAda 7.1 !
    end loop;
	   
	   -- 1.4/ Lecture de l'adresse de la prochaine IFD
		  ------------------------------------------------ 
	  t_offset_next_ifd'read(s, IFD_Courant.Offset_Next_Ifd);
    if bigendian=false then -- if Bigendian=True sous ObjectAda 7.1 !
      inverse4(IFD_Courant.Offset_Next_Ifd);
    end if;
    
	   -- 1.5/ On stocke le pointeur sur l'IFD courant dans un tableau
	   ---------------------------------------------------------------
	  nbifd := nbifd + 1;
	  Liste_IFDs(nbifd) := IFD_Courant;
		  
	   -- 1.6/ On a termine la lecture des ifds sinon on passe a l'ifd suivant 
	   -----------------------------------------------------------------------
	  exit when convert4(IFD_Courant.Offset_Next_Ifd) = 0; 
	  ada.streams.stream_io.set_index(fichier_tiff,ada.streams.stream_io.count
                                 (convert4(IFD_Courant.Offset_Next_Ifd) + 1));
  end loop;
    
exception when CONSTRAINT_ERROR =>
  Msg_Erreur:=To_Unbounded_String("Erreur de lecture du l'entete de l'image tiff:");
--  new_line;
--  put_line ("CONSTRAINT_ERROR from TIFF_IO.READ_IFDS");
  raise Constraint_error;
end Read_IFDs;   
   

------------------------------------------------------------------                                      
-- NOM         : Traite_IFDs
-- TRAITEMENT  : 1.3
-- DESCRIPTION : procedure de traitement des IFDS suivant les Tags
------------------------------------------------------------------
procedure Traite_IFDs( Liste_IFDs    		: in T_Liste_IFDs;
                       bigendian 			: in out boolean;
                       fichier_tiff 		: in ada.streams.stream_io.file_type;
                       nbifd 				: in natural;
                       lps,ncol,nlig,spp,longest,cmp			: out integer; --compression
                       stripoffset	, stripbytecounts		: out acces_tabstrip; 
                       --  multistrip : in boolean
                       predictor			: in out integer  --ajout lecture Tiff photoshop   
                      ) is  
 
ifd_courant	: acces_ifd;
tag			: typ_tag;
  
s 			: ada.streams.stream_io.stream_access;
sizt 		: array (short_integer range 1..5) of integer := (1,1,2,4,8);
			-- taille en octet des types de valeur
            -- respectivement byte, ascii, short, long, rational.
ch2 		: string_2;  -->
ch4 		: string_4;  --> variables utilisees pour les conversions sur le champ des valeurs
valoff_num 	: integer;	-->
bps_string 	: string_2;
l 			: integer := 1;
nbvals_num  : positive := 1;
n 			: integer := 0;
  
--parametres caracteristiques de l'image
bps  	: array (1..5) of integer := (others => 0);--- bits per sample(bits/composante)
photo	: integer := 0;------------------------------- photometricinterpretation
nbstrip	: integer := 0;------------------------------- nb de bandes
planar 	: integer := 0;----planarconfiguration (mode de stockage des composantes des pixels) 
fillorder	: integer := 0;---------------------ordre logique des bits dans un octet
                     
  NBSTRIP_ERROR		: exception;
  PLANAR_ERROR		: exception;  
  ACCES_ERROR 		: exception;
  FILLORDER_ERROR	: exception;
  
st2 : string_2;
                       
begin
  s := ada.streams.stream_io.stream(fichier_tiff);
 
  -- Pour chaque IFD
  for i in 1..nbifd loop
    ifd_courant := liste_ifds(i); -- renommage
    -- Pour chaque tag 
    for j in ifd_courant.tags'range loop-- pour chaque datastripectory entry
      tag := ifd_courant.tags(j); -- renommage
      case convert2(tag.tagid) is
        -- nb de colonnes
        when 256 =>
          if convert2(tag.typ) = 3 then 
            if bigendian then
              st2:=string_2(tag.valoff(1..2));
              inverse2(st2);              
              ncol:=integer(convert2(st2));
            else
              ncol:=integer(convert2(string_2(tag.valoff(1..2))));
            end if;
          else
            if bigendian then
              inverse4(tag.valoff);              
            end if;
            ncol:=integer(convert4(tag.valoff));
          end if;

        -- nb de lignes
        when 257 =>
          if convert2(tag.typ) = 3 then
            if bigendian then
              st2:=string_2(tag.valoff(1..2));
              inverse2(st2);              
              nlig:=integer(convert2(st2));
            else  
              nlig:=integer(convert2(string_2(tag.valoff(1..2))));
            end if;  
          else
            if bigendian then
              inverse4(tag.valoff);
            end if;
            nlig:=integer(convert4(tag.valoff));
          end if;
     
        -- bits per sample ( bits/composante)          
        when 258 =>
          if convert4(tag.nbvals) < 3 then
            if bigendian then
              st2:=string_2(tag.valoff(1..2));
              inverse2(st2);              
              bps(1):=integer(convert2(st2));
              st2:=string_2(tag.valoff(3..4));
              inverse2(st2);              
              bps(2):=integer(convert2(st2));
            else
              bps(1) := integer(convert2(string_2(tag.valoff(1..2))));
              bps(2) := integer(convert2(string_2(tag.valoff(3..4))));
            end if;  
          else
            if bigendian then
              inverse4(tag.valoff);
            end if;
            ada.streams.stream_io.set_index(fichier_tiff,ada.streams.stream_io.count(convert4(tag.valoff)+1));
            nbvals_num := positive(convert4(tag.nbvals));
            for k in 1..nbvals_num loop
              string_2'read(s,bps_string);
              if bigendian=false then inverse2(bps_string);
              end if;
              bps(k) := integer(convert2(bps_string));
            end loop;
          end if;
         
        -- compression 
        when 259 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            cmp:=integer(convert2(st2));
          else  
            cmp:=integer(convert2(string_2(tag.valoff(1..2))));
          end if;
          
        -- photometric interpretation
        when 262 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            photo:=integer(convert2(st2));
          else  
            photo := integer(convert2(string_2(tag.valoff(1..2)))); 
          end if;
          
        -- fillorder
        when 266 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            fillorder :=integer(convert2(st2));
          else  
            fillorder := integer(convert2(string_2(tag.valoff(1..2))));
          end if;
          if fillorder /= 1 then raise FILLORDER_ERROR;
          end if;	                  
                      
        -- adresse(s) des bandes            
        when 273 =>	nbstrip := integer(convert4(tag.nbvals));
          declare
            tablo_aux : tabstrip(1..nbstrip);
          begin                   
			      -- cas 1 : adresse(s) des bandes directement si donn�es tiennent sur 4 octets
		   		  if nbstrip*sizt(convert2(tag.typ)) < 5 then
			        -- cas 1.1 : plusieurs adresses de bandes
              if convert2(tag.typ) = 3 then
                for k in 1..nbstrip loop
                  if bigendian then
                    st2:=string_2(tag.valoff(l..l+1));
                    inverse2(st2);              
                    tablo_aux(k):=integer(convert2(st2));
                  else  
                    tablo_aux(k) := integer(convert2(string_2(tag.valoff(l..l+1))));
                  end if;
                  -- range ds le tab les adresses contenu ds valoff
                  l := l+2;
                end loop;
              -- cas 1.2 : une seule adresse de bande
              else
                if bigendian then
                  inverse4(tag.valoff);
                end if;
                valoff_num:=integer(convert4(tag.valoff));
                tablo_aux(1) := valoff_num;
              end if;
            -- cas 2 : adresse ou recuperer les adresses des bandes
            else
              if bigendian then
                inverse4(tag.valoff);
              end if;
              valoff_num := integer(convert4(tag.valoff));
              ada.streams.stream_io.set_index(fichier_tiff,
                                ada.streams.stream_io.count ((valoff_num)+1));
              -- cas 2.1 : valeurs de type short
              if convert2(tag.typ) = 3 then
                for k in 1..nbstrip loop
                  string_2'read(s,ch2);
                  if bigendian=false then inverse2(ch2);   --*--
                  end if;
                  tablo_aux(k) := integer(convert2(ch2));
                end loop;
              -- cas 2.2 : valeurs de type long
              else
                for k in 1..nbstrip loop
                  string_4'read(s,ch4);
                  if bigendian=false then inverse4(ch4); end if;  --*--
                  tablo_aux(k) := integer(convert4(ch4));
                end loop;
              end if;
            end if;
            -- ajustement de la taille du tableau
            stripoffset := new tabstrip(1..nbstrip);
            stripoffset.all := tablo_aux;
          end; -- declare
       
        -- composantes / pixel              
        when 277 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            spp:=integer(convert2(st2));
          else  
            spp := integer(convert2(string_2(tag.valoff(1..2)))); 
          end if;
          
        -- lignes / bandes
        when 278 =>
          if convert2(tag.typ) = 3 then
            if bigendian then
              st2:=string_2(tag.valoff(1..2));
              inverse2(st2);              
              lps:=integer(convert2(st2));
            else
              lps:=integer(convert2(string_2(tag.valoff(1..2))));
            end if;
          else
            if bigendian then
              inverse4(tag.valoff);
            end if;  
            lps := integer(convert4(tag.valoff));
          end if;
	         
        -- longueur des bandes en octets            
        when 279 => nbstrip := integer(convert4(tag.nbvals));
          declare
            tablo_aux : tabstrip(1..nbstrip);
          begin
            -- cas 1 : longueur des bandes directement si donnees tiennent sur 4 octets
            if nbstrip*sizt(convert2(tag.typ)) < 5 then
              -- cas 1.1 : plusieurs longueurs de bandes
              if convert2(tag.typ) = 3 then
                for k in 1..nbstrip loop
                  if bigendian then
                    st2:=string_2(tag.valoff(l..l+1));
                    inverse2(st2);              
                    tablo_aux(k):=integer(convert2(st2));
                  else
                    tablo_aux(k) := integer(convert2(string_2(tag.valoff(l..l+1))));
                  end if;
                  l := l+2;
                end loop;
                -- cas 1.2 : une seule longueur de bande
              else
                if bigendian then
                  inverse4(tag.valoff);
                end if;  
                valoff_num := integer(convert4(tag.valoff));
                tablo_aux(1) := valoff_num;
              end if;
            -- cas 2 : adresse ou recuperer les longueurs de bande
            else
              if bigendian then
                inverse4(tag.valoff);
              end if;  
              valoff_num := integer(convert4(tag.valoff));
              ada.streams.stream_io.set_index(fichier_tiff,
                                ada.streams.stream_io.count(valoff_num + 1));
              -- cas 2.1 : valeurs de type short
              if convert2(tag.typ) = 3 then
                for k in 1..nbstrip loop
                  string_2'read(s,ch2);
                  if bigendian=false then inverse2(ch2); end if;   --*--
                  tablo_aux(k) := integer(convert2(ch2));
                end loop;
              -- cas 2.2 : valeurs de type long
              else
                for k in 1..nbstrip loop
                  string_4'read(s,ch4);
                  if bigendian=false then inverse4(ch4); end if;  --*--
                  tablo_aux(k) := integer(convert4(ch4));
                end loop;
              end if;
            end if;
            stripbytecounts := new tabstrip(1..nbstrip);
            stripbytecounts.all := tablo_aux;
          end;-- declare
                         
        -- planar configuration
        when 284 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            planar:=integer(convert2(st2));
          else
            planar := integer(convert2(string_2(tag.valoff(1..2))));
          end if;
          if planar /= 1 then raise PLANAR_ERROR; end if;

        when 317 =>
          if bigendian then
            st2:=string_2(tag.valoff(1..2));
            inverse2(st2);              
            predictor:=integer(convert2(st2));
          else
            predictor:=integer(convert2(string_2(tag.valoff(1..2))));
          end if;          
        when others => null;-- tag non traite
      end case;
    end loop;-- pour chaque tag
  end loop;-- pour chaque ifd
  
  -- on recupere la longueur de la bande la plus longue 
  -- ceci nous permettra de dimensionner la structure accueillant les donnees de l'image
  longest := 0;
  if stripbytecounts = null then raise ACCES_ERROR; end if;
  for z in 1..nbstrip loop
    if stripbytecounts.all(z) > longest then
      longest := stripbytecounts.all(z);
    end if;
  end loop;
 
  exception when others =>
    Msg_Erreur:=To_Unbounded_String("ERREUR de lecture du l'entete de l'image tiff:");
    raise;
end Traite_IFDs;


 ----------------------------------------------------------------------                                      
 -- NOM         : Read_Param_Tiff
 -- TRAITEMENT  : 1.0
-- DESCRIPTION : procedure de lecture des parametres d'un fichier Tiff                                            
----------------------------------------------------------------------
procedure Read_Param_Tiff(fichier_tiff  : in ada.streams.stream_io.file_type;
                          lps, ncol, nlig, spp,longest,cmp : out integer;
                          stripoffset, stripbytecounts		: out acces_tabstrip ;
                          predictor 	: in out integer
                         ) is 

IFH 		: T_IFH;
Liste_IFDs	: T_liste_IFDs;
lastchar 	: integer := 0 ;
bigendian 	: boolean := true;
nbifd 		: natural := 0 ;

begin
-- lecture en-tete
read_IFH(fichier_tiff, IFH, bigendian);
-- lecture des ifds
read_IFDS(fichier_tiff, IFH, bigendian, Liste_IFDs, nbifd);
-- traitement des ifds
Traite_IFDs(Liste_IFDs, bigendian, fichier_tiff, nbifd, lps, ncol, nlig,
               spp, longest, cmp, stripoffset,stripbytecounts, predictor); 
-- liberation de la place memoire des IFDs    
for i in Liste_IFDs'range loop
    Free_IFD(Liste_IFDs(i));
end loop;     
       
exception when CONSTRAINT_ERROR =>
  Msg_Erreur:=To_Unbounded_String("ERREUR de lecture du l'entete de l'image tiff:");
--  put_line ("CONSTRAINT_ERROR from TIFF_IO.READ_PARAM_TIFF");
  raise Constraint_error;
end Read_Param_Tiff;


  ----------------------------------------------------------------------                                      
  -- NOM         : Charge_param_image
  -- TRAITEMENT  : 1.0
 	-- DESCRIPTION : procedure de lecture des param�tres d'un fichier Tiff                                            
 	----------------------------------------------------------------------
procedure Charge_param_image(lps, ncol : out integer;
                             nlig : out integer;
                             stripoffset: out acces_tabstrip) is
                                 
file_tiff: ada.streams.stream_io.file_type;                                                                  
spp, longest, cmp, predictor: integer := 0;
stripbytecounts: acces_tabstrip;
Compression_error : exception;
                                    
begin
  ada.streams.stream_io.open(file_tiff,ada.streams.stream_io.in_file,nomfichier_image(1..ncar_fi));
  Read_Param_Tiff(file_tiff, lps, ncol, nlig, spp, longest, cmp,stripoffset,stripbytecounts, predictor);
  if cmp/=1 then
    raise Compression_error;	
  end if;
  ada.streams.stream_io.close(file_tiff);                                           
  Free_tabstrip(stripbytecounts);
exception 
  when Compression_error => Msg_Erreur:=To_Unbounded_String("L'image de mutilation est compress�e"&eol&nomfichier_image(1..ncar_fi));
  raise;
  when others => Msg_Erreur:=To_Unbounded_String("Erreur de lecture de l'ent�te de l'image:"&eol&nomfichier_image(1..ncar_fi));
  raise;
end Charge_param_image;                                                  
                                       
     
-----------------------------------------------------------------------------------                                      
end TIFF_IO;



