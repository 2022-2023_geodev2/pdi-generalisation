with GEN_IO; use GEN_IO;
with inipan; use inipan;
with detpos; use detpos;
with win32.winuser;
With Unchecked_deallocation;

package NoRou is

-- debut modif 23.02.2006
-- NBRE_CAR_NOM : positive:=20;
NBRE_CAR_NOM : positive:=25;
-- debut modif 23.02.2006
SEUIL_PLAT : float:=0.1;
-- debut modif 24.03.2006
NBRE_PTS_D_ARC : integer:=20000;
-- NBRE_PTS_D_ARC : integer:=1000;
-- fin modif 24.03.2006
SEUIL_S : float; -- :=0.15;  --- en mm carte. valeur correspondant a un plafond de rectitude de la portion de route retenue. 0.06 dans le code MB 
NBRE_MAX_N_A : integer:=40;  -- anciennement : 10
-- MAXP : positive; -- :=10;
SEUIL_ORT : float:=0.1;
SEUIL_DENSITE : integer:=-1; -- :=20;  --- nombre d'intersections avant decoupe obligatoir. dans le code MB : 20
SEUIL_TAILLE : float; -- :=30.0;  --- en mm carte. anciennement 1500 unites plage = 9.5 mm carte
DIST_ROUTE	: float; -- :=0.5;  ---- valeur conseillee : 0.5 

SEUIL_TAILLE_COURBE : float:=0.0; -- :=1.0; -- :=150.0;  -- distance moyenne de r�p�tition
FILTRE_COURBE : float :=5.0;          -- valeur minimale du rapport (longueur de courbe / longueur de la cote) pour qu'il y ait placement
AMONT_A_DROITE : boolean:=true;       -- utilisation de courbes de niveau orient�es amont a droite
PENALITE_SENS_DE_LECTURE : float:=0.0; -- :=0.1;
PasMNTmm : float:=1.0;  -- pas du MNT 3*3 en mm pour etude locale du relief afin d'orienter les cotes de courbe
SEUIL_S_COURBE : float; --- en mm carte. valeur correspondant a un plafond de rectitude de la portion de courbe retenue 
COEF_S_COURBE : float:=0.0;
COEF_MUTIL_COURBE : float:=0.0;
COEF_DL_COURBE : float:=0.0; -- :=0.25;

COEF_S : float:=0.0; -- :=0.05;
COEF_DL : float:=0.0; -- :=0.25;
COEF_MUTIL : float:=0.0; -- :=0.20;
COEF_ANGLE : float:=0.0; -- :=0.25;
COEF_DAX : float:=0.0; -- :=0.25;

N_PTS_RTE : integer:=10000;--Nbre max de pts par route 1000 en principe
delta_xpix_r : natural; -- Emprise en X du graphe des routes 
delta_Ypix_r : natural; -- Emprise en Y du graphe des routes 
NBRE_ROUTES : integer;
Max_MT : integer:=2000;

-- Parametres utiles aux traitements morphologiques
-- Distance de la fermeture appliqu�e aux courbes support
CoefDilatErode : float:=0.0;  -- multiple du corps de police. 2.0 pour le 100.000
PrecisionFermeture : float:=1.0;
PrecisionDilatation : float:=1.5;
DistLissage : float:=0.08;      -- 0.08;

SEUIL_TAILLE_HYDRO : float:=0.0;  -- 150
DIST_HYDRO : float:=0.0;          -- 0.5
MaxInterMots : Integer:=0;
AngleMaxSuppl : integer:=0;
MaxProportionEnversPourcent : integer:=0;
COEF_HOMO : float:=0.0;
COEF_IDEAL_DIST : float:=0.0;
COEF_CORDE : float:=0.0;
COEF_CENTRE : float:=0.0;
COEF_ENVERS : float:=0.0;
COEF_MUTIL_HYDRO : float:=0.0;
COEF_DAX_HYDRO : float:=0.0;
-- SEUIL_S_HYDRO : float:=0.5;
-- Seuil_FDP : float:=0.2; -- Seuil de filtrage par Douglas et Peuker de l'hydro lineaire

type TypTabNom is array (positive range <>) of String(1..NBRE_CAR_NOM);
type TypTabL_Topo is array (positive range <>) of integer;
type typTabLigneSupport is array (positive range <>) of TypTabAngle(1..5000);

type ARC_NOM is record 
  NOM : TypTabNom(1..NbMaxMots):=(others=> (others => ' '));
  NCAR : TypTabNcar(1..NbMaxMots):=(others => 0);
  Nb_Mots : integer:=1;
  cODE : string30 := (1..30 => ' '); -- code typo associ� � l'arc
  MODE_P : typcateg := Desaxe; 
  MARQUE : boolean:=false;
end record;

type TARC_NOM is array (integer range <>) of ARC_NOM;
type Acces_TARC_NOM is access TARC_NOM;
Procedure GR_Free_TARC_NOM is new Unchecked_deallocation(TARC_NOM,acces_TARC_NOM);


type ROUTES is record
  NOM : TypTabNom(1..NbMaxMots):=(others=> (others => ' '));
  NCAR : TypTabNcar(1..NbMaxMots):=(others => 0);
  Nb_Mots : integer:=1;
  MODE_P : typcateg := Desaxe; 
  cODE : string30 := (1..30 => ' ');
  L_TOPO : TypTabL_Topo(1..NbMaxMots):=(others => 0);
  PT_DEB : integer:=0;
  PT_FIN : integer:=0;
  Kilo : boolean:=false;  -- s'agit-il d'un kilometrage
end record;

type TROUTES is array(integer range<>) of ROUTES;
type acces_TROUTES is access TROUTES;
Procedure GR_Free_TROUTES is new Unchecked_deallocation(TROUTES,acces_TROUTES);

type ARCS is record
  N_ARC : integer;
  N_TRONCON : integer;
end record;

type TARCS is array(integer range <>) of ARCS;
type acces_TARCS is access TARCS;
Procedure GR_Free_TARCS is new Unchecked_deallocation(TARCS,Acces_TARCS);

type TRONCON is record
  A_DEB : integer:=0;
  A_FIN : integer:=0;
end record;

type TTRONCON is array(integer range <>) of TRONCON;
type acces_TTRONCON is access TTRONCON;
Procedure GR_Free_TTRONCON is new Unchecked_deallocation(TTRONCON,Acces_TTRONCON);


Type MT is record
  T_DEB : integer:=0;
  T_FIN : integer:=0;
  -- I_ROUTE : integer:=0;
  -- POS_S : integer:=1;
  L_TOPO : TypTabL_Topo(1..NbMaxMots):=(others => 0);
  Nb_Mots : integer:=1;
  POLICE : integer:=0;
  CORPS : integer:=0;
  court : boolean:=false;
  Ecrase : boolean:=false;
  bord_image : boolean:=false;
  Hors_image : boolean:=false;
  Agencement : boolean:=false;
end record;

type TMT is array (integer range <>) of MT;
type acces_TMT is access TMT;
Procedure GR_Free_TMT is new Unchecked_deallocation(TMT,Acces_TMT);

type TGROUPEMENT is array (integer range <>) of integer;

type POSITION is array(positive range <>) of POSITION_TYPE;
type POSITIONS is array(positive range <>, positive range <>) of POSITION_TYPE;

type acces_POSITIONS is access POSITIONS;
Procedure GR_Free_pOSITIONS is new Unchecked_deallocation(POSITIONS,Acces_POSITIONS);


--Format pour la linearisation par les Moindres carrees (libres).
type PLT_OPT_REC is record
  coor_x	: integer;
  coor_y	: integer;
  c_pt   	: integer;
end record; 

type PLT_OPT is array (integer range <>) of plt_opt_rec;

type TM is array(short_integer range <>) of short_integer;

type acces_LPPAINTSTRUCT is access Win32.winuser.LPPAINTSTRUCT;


procedure RCH_ROUTE_D(gr : graphe_type;
			          nd : in out integer;
			          n_arc : integer;
                      att_gr : natural;
			          T_AN : in out TARC_NOM; 
			          T_ARCS : in out TARCS;
			          ttroncon : integer;
			          crst : in out integer);

procedure RCH_ROUTE_G(gr : graphe_type;
                      nd : in out integer;
                      n_arc : integer;
                      att_gr : natural;
                      T_AN : in out TARC_NOM;
                      T_ARCS : in out TARCS;
                      ttroncon : integer;
                      crst : in out integer);

procedure RECHERCHE_ROUTE(n_arc	: integer;
                          gr : graphe_type;               
                          T_AN : in out TARC_NOM; 
                          T_ARCS : in out TARCS;
                          ttroncon : integer;
                          crst : in out integer);

procedure R_R(graphe : graphe_type;
		      T_AN : in out TARC_NOM;
              Nb_Arcs : in integer;
		      T_RTES : out TROUTES;
		      T_ARCS : in out TARCS;
			  crs_tt : out integer;
			  Coheroute : out boolean);

function NORM_ANGLE(ang : float) return float;

FUNCTION TAILLE(graphe : graphe_Type; arc : integer) return float;

FUNCTION DENSITE_P(	graphe : graphe_Type;
			arc1	: integer;
			arc2	: integer) return integer;

FUNCTION SEGMENT1_LIN(graphe : graphe_type; arc : integer; resolution : in positive) return point_type;

procedure POINTS(T_MT : MT;
                 T_TRS : TTRONCON;
                 T_ARCS : TARCS;
                 graphe : graphe_type;
                 Tab : out point_liste_type;
                 crs : in out integer);

function C_ANGLE(p1, p2 : point_type) return float;

function TAILLE_STRING(ST : string) return integer; 

function C_REPERE(p1,p2 : point_type; an : float) return point_type;

function Translation(P1,P2 : point_type) return point_type;

function ROTATION(p : point_type; an : float) return point_type;

function MAX(tableau : point_liste_type; crs : integer:=0) return point_type;

function MIN(tableau : point_liste_type; crs : integer:=0) return point_type;

function PROJ(p,x0 : point_type; x,y : float) return point_type;

function DIST_PD(p,x0 : point_type; x,y : float) return float;

FUNCTION NOEUD_NEGLIGEABLE(graphe : graphe_type;
                           noeud : integer;
                           arc1 : integer;
                           arc2 : integer;
                           resolution : in positive) return boolean;

PROCEDURE DECOUPE_TRONCON(graphe : Graphe_type;
				          T_RTES : in out TROUTES;
                          T_ARCS : in out TARCS;
                          T_TRS : in out TTRONCON;
                          -- T_GROUP : TGROUPEMENT;
						  Resolution : in positive);

procedure MC(tab : point_liste_type; curs : integer; a,b : out float);

procedure MC2(tab : point_liste_type; curs1 : integer; curs2 : integer; a,b : out float);


procedure SEG_MC(arc_t : point_liste_type;
                 curseur : integer;
                 seg_t : in out point_liste_type;
                 curseur_f : in out integer;
                 seuil : float);

procedure MCL(tab : point_liste_type; curs : integer; a,b : out float);

procedure MCL2(tab : point_liste_type; curs1,Curs2 : integer; a,b : out float);

procedure SEG_MCL(arc_t : point_liste_type;
                  curseur : integer;
				  seg_t : in out plt_opt;
                  curseur_f : in out integer;
                  seuil : float);

function POINT_MOYEN(tb : point_liste_type; crs : integer) return point_type;

function POINT_MOYEN2(tb : point_liste_type; crs1,Crs2 : integer) return point_type;

end norou;
