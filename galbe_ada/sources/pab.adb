---------------------------
-- PLACEMENT DES BLASONS --
---------------------------

with text_io; use text_io;
with gen_io; use gen_io;
with inipan; use inipan;
with win32;
with win32.windef;
with win32.wingdi;
with win32.winuser;
with unchecked_conversion;
with system;
with gb; use gb;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

Package body pab is
  Package Entier_io is new Integer_io(integer); use Entier_io;
  
use type system.address;
use type win32.long;  
use type win32.int;
  
-- VARIABLES GLOBALES

maxser: positive;
lnomfic, lchaine : positive range 1..maxcar;
char,chars : character;
VALin : ALin;
lin : positive range 1..2;
nse : positive range 1..maxsertopo;
nbl,nbct,nbgd,indpla : natural range 0..maxblatopo :=0;
b,c : positive range 1..maxblatopo :=1; 
x,xtmp,xdep,xl,xcur,emprise,l : integer :=0;  
sens : integer range -1..1;
VAgd : array(1..maxblatopo) of natural range 0..maxblatopo;
Cent : Acent;
police : positive;
place : boolean;
drap : boolean:=true;

-------------------------------------------------------------------------------------------------------
---------------------- PROCEDURES ET FONCTIONS DU PACKAGE----------------------------------------------
-------------------------------------------------------------------------------------------------------
                                 
procedure DETXPOS (X : in out integer; 
		   Xdep : in integer; 
		   Nbplaces : in ANbplaces;
		   VAPlace : in out Aplace; 
		   VANom_type : in typtbnomi; 
		   Indnom :in positive; 
		   Sens : in integer;
		   Lin : in positive; 
		   Hesp : in natural;
       nbl : in positive) is
xtst : integer;
dxtst : natural;
indtst : positive range 1..maxblatopo;
i : natural range 0..maxblatopo; 
BEGIN
	i:=1;
	x:=xdep;
	while i<=nbplaces(lin)
	loop
	    if VAPlace(i,2,lin)=1 
	    then
		i:=i+1;
	    else
		indtst:=VAPlace(i,1,lin);
		xtst:=VANom_type(indnom).capict(nbl)(indtst).x;
		dxtst:=VANom_type(indnom).capict(nbl)(indtst).dx;
	       	if (x>=xtst) and (x<=xtst+dxtst) then 
	           if sens=1 then
		   	x:=xtst+dxtst+hesp-1;
		    else
		    	x:=xtst-hesp+1;
		    end if;
		    VAPlace(i,2,lin):=1;
		    i:=0;
	        end if;
	        i:=i+1;
	    end if;
	end loop;			
END DETXPOS;

-------------------------------------------------------------------------------------------------------
--la procedure qui suit est appelee en fin de PABMAIN; elle permet d'ameliorer l'esthetique du 
--positionnement. Ainsi, elle etale dans certaines conditions le positionnement des blasons d'une
--meme ligne (a savoir au-dessus ou au-dessous du nom) sur la totalite de l'emprise de cette ligne

procedure REPARTITION(VANom_type: in out typtbnomi;
                      Nbplaces: in ANbplaces;
                      hesp: in natural;
                      indnom: in positive;
                      VAPlace : Aplace;
                      nbl : in natural) is

bool,j,somme:natural:=0;
-- ex bug7 xblaslast,xlast,xclast,cpos,diff,flag1,flag2,tamp1:natural:=0;
xblaslast,xlast,xclast,cpos,diff,flag1,flag2,tamp1:integer:=0;
nse:positive;
temp,interm1,interm2,interm3,interm4,newesp:float;

BEGIN
for i in 1..2 loop
  somme:=0;
  cpos:=0;
  bool:=0;
  j:=1;
  flag1:=0;
  flag2:=0;
  xclast:=0;

  if nbplaces(i)/=0 then
    while vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(j,1,i)).ser).jhori='C' loop
      if j=nbplaces(i) then
        exit;
      end if;
      j:=j+1;
    end loop; 

    if j/=nbplaces(i) then
      if vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(j,1,i)).ser).jhori='G' then
        flag1:=1;
      elsif vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(j,1,i)).ser).jhori='D' then 
        flag2:=1;
      end if;
    end if;

    if flag1=1 then
      for indice in 1..nbplaces(i) loop
        if vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(indice,1,i)).ser).jhori='C' then 
          bool:=1;
          xclast:=VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).x + VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).dx;
        end if;
        somme:=somme+VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).dx+(hesp-1);
      end loop;

      xblaslast:=VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).x + VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).dx;
      xlast:=integer(VANom_type(indnom).calin(nbl)(i).empx);
      if (vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(nbplaces(i),1,i)).ser).jhori='G') and (xblaslast < xlast) then
        diff:=xlast-xblaslast-1;
        j:=nbplaces(i);
        if bool=1 then 
          while (VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x > xclast) loop
            VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x:=VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x + diff;
            j:=j-1;
          end loop;
        else
          interm1:=float((somme-hesp+1));
          interm2:=float(VANom_type(indnom).calin(nbl)(i).empx);
          temp:=float((interm1/interm2));

          if (temp>(float(etalparam)/100.0)) and (etalparam/=0) then
            tamp1:=(nbplaces(i)-1);
            interm3:=float(tamp1);
            interm4:=float(diff);
            newesp:=interm4/interm3;

            VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).x:=xlast - VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).dx - 1;
            for indice in 2..nbplaces(i)-1 loop
              VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).x
              :=VANom_type(indnom).capict(nbl)(VAPlace(indice-1,1,i)).x
               +VANom_type(indnom).capict(nbl)(VAPlace(indice-1,1,i)).dx
               +integer(newesp)+hesp-1;
            end loop;
          end if;
        end if;    
      end if;
    end if;

    if flag2=1 then
      for indice in 1..nbplaces(i) loop
        if vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(indice,1,i)).ser).jhori='C' then 
          bool:=1;
          xclast:=VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).x;
        end if;
        somme:=somme+VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).dx+(hesp-1);
      end loop;
      xblaslast:=VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).x;
      xlast:=0;
      if (vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(nbplaces(i),1,i)).ser).jhori='D') and (xblaslast > xlast) then
        diff:=xblaslast-xlast-1;
        j:=nbplaces(i);
        if bool=1 then 
          while (VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x + VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).dx < xclast) loop
            VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x:=VANom_type(indnom).capict(nbl)(VAPlace(j,1,i)).x - diff;
            j:=j-1;
          end loop;
        else
          interm1:=float((somme-hesp-1));
          interm2:=float(VANom_type(indnom).calin(nbl)(i).empx);
          temp:=float((interm1/interm2));
          
          if (temp > (float(etalparam)/100.0)) and (etalparam /= 0) then 
            tamp1:=(nbplaces(i)-1);
            interm3:=float(tamp1);
            interm4:=float(diff);
            newesp:=interm4/interm3;            
            VANom_type(indnom).capict(nbl)(VAPlace(nbplaces(i),1,i)).x:= 0;    
            for indice in 2..nbplaces(i)-1 loop
              VANom_type(indnom).capict(nbl)(VAPlace(indice,1,i)).x:=VANom_type(indnom).capict(nbl)(VAPlace(indice-1,1,i)).x-integer(newesp)-hesp+1; 
            end loop;
          end if;
        end if;
      end if;
    end if;
  end if;
end loop;--for--

END REPARTITION;  
---------------------------------------------------------------------------------------------------
procedure MOVETXT(VANom_type: in out typtbnomi; Nbplaces: in ANbplaces; indnom: in positive; vserie: in aserie; VAPlace : Aplace; nbl : in natural) is
bool1,bool2:boolean;
posblaadm,posblatxt:integer;
NoSerie : positive;
k : integer;

begin
  for j in 1..2 loop
    bool1:=false;
    bool2:=false;
    posblatxt:=0;
    posblaadm:=0;

    if nbplaces(j)/=0 then
      for i in 1..nbplaces(j) loop
        NoSerie:=vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(i,1,j)).ser).iser;
        k:=integer'image(Noserie)'length;
        if NoSerie <= NS then
          if vserie(NoSerie).nom(k+2..vserie(NoSerie).lnom)="administratif" then
            bool1:=true;
            posblaadm:=i;
         end if;
       end if;
       if vanom_type(indnom).capser(vanom_type(indnom).capict(nbl)(vaplace(i,1,j)).ser).iser > NS then
         bool2:=true;
         posblatxt:=i;
       end if;
    end loop; 

    if (bool1=true) and (bool2=true) then
      if j=1 then 
        vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).y:=vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).y + 
        integer(float((vanom_type(indnom).capict(nbl)(vaplace(posblaadm,1,j)).dy-vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).dy))/2.0);
      else
        vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).y:=vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).y - 
        integer(float((vanom_type(indnom).capict(nbl)(vaplace(posblaadm,1,j)).dy-vanom_type(indnom).capict(nbl)(vaplace(posblatxt,1,j)).dy))/2.0);
      end if;
    end if;
  end if;
end loop;
end MOVETXT;
      
---------------------------------------------------------------------------------------------------
--Les blasons ayant ete postionnes relativement autour du nom, cette fonction determine l'enveloppe
--des blasons soit au-dessus du nom (lin=1), soit au-dessous (lin=2)
   
function DET_BLASEMP( VANom_type:in typtbnomi; 
	              Indnom: positive;
                      lin : integer;
	              VAPlace: in Aplace; 
	              NBplaces : in ANBplaces;
                nbl : in natural) return Boite_type is   

box : Boite_type;
xmintemp,ymintemp:integer :=0;
xmaxtemp,ymaxtemp:integer :=0;

begin
  for i in 1..nbplaces(lin) loop
    if (vanom_type(indnom).nl = 1) or (lin=1) then
      xmintemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).x;
      ymintemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).y;
      xmaxtemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).x + vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).dx;
      ymaxtemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).y + vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).dy;        
    else
      xmintemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).x;
      ymintemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).y;
      xmaxtemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).x + vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).dx;
      ymaxtemp:=vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).y + vanom_type(indnom).capict(nbl)(vaplace(i,1,lin)).dy;
    end if;
    
    if i=1 then 
      box.minimum.coor_x:=xmintemp;
      box.minimum.coor_y:=ymintemp;
      box.maximum.coor_x:=xmaxtemp;
      box.maximum.coor_y:=ymaxtemp;
    else
      if xmintemp < box.minimum.coor_x then
        box.minimum.coor_x:=xmintemp;
      end if;
      if ymintemp < box.minimum.coor_y then
        box.minimum.coor_y:=ymintemp;
      end if;
      if xmaxtemp > box.maximum.coor_x then
        box.maximum.coor_x:=xmaxtemp;
      end if;
      if ymaxtemp > box.maximum.coor_y then
        box.maximum.coor_y:=ymaxtemp;
      end if;
    end if;

  end loop;
return box;

end DET_BLASEMP;


procedure empxyblastxt(tbnomi : in out typtbnomi; i: in integer; noblas : in integer; police : in positive; info_font: in typtfont; hdc: Win32.windef.hdc; nbl : in natural) is
use type win32.int;
use type win32.long;
                                   
taille: aliased win32.windef.size; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
taille_m: float;
ch: string(1..20);
n: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
--H_400: win32.long;
H_400: integer;
T: float;
AOC: Win32.PSTR; 
longueur : integer;
ddx,ddy : natural;
                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  function UC is new Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
  Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------
                              
begin

 -- creer le police logique
    Log_Font.lfFaceName:=info_font(police).police.lfFaceName;
    -- on charge systematiquement une police de corps 400
    Log_Font.lfHeight 		  :=win32.long(-400);
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
      
    Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police    
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
    if (fhandle = Null) then  put_line("erreur de chargement de police");
       get_line(ch,n); -- se produit quand erreur dans hdc
    end if;
    
    -- caracteristiques de la police
    res :=win32.wingdi.gettextmetrics(hDC,lptm'unchecked_access);
    
    -- Corps en points=lptm.tmheight-lptm.tmInternalleading
    -- Hauteur des majuscules=lptm.tmheight-lptm.tmdescent-lptm.tmInternalleading
    -- Hauteur des majuscules=Corps-lptm.tmdescent
    H_400:=integer(400-lptm.tmdescent);
    T:=float(info_font(police).hauteur)*float(25.4/(72.0*400.0))*float(ptparmm);
       
    -- calculer l'extension
    res:=win32.wingdi.getTextExtentPoint32(Hdc,
                              String_to_PCCH(tbnomi(i).capict(nbl)(noblas).str(1..tbnomi(i).capict(nbl)(noblas).lstr)),
                              win32.int(tbnomi(i).capict(nbl)(noblas).lstr),taille'unchecked_Access);

    tbnomi(i).capict(nbl)(noblas).dy:=integer(T*float(H_400));
    tbnomi(i).capict(nbl)(noblas).dx:=integer(T*float(taille.cx));
        
     --decharger la police
     res:=win32.wingdi.deleteobject(Font_Handle);
   
end empxyblastxt;


-----------------------------------------------------------------------------------------------------------

-- PROCEDURE PRINCIPALE
procedure PABMAIN(vanom_type: in out typtbnomi; InvEchelle : in integer; Resolution : in positive; vesp : in out integer; hdc : Win32.windef.hdc) is

indnom : positive;
id : integer; -- identifiant d'une l'ecriture
hesp,ephem : integer;
eps : integer;
fic : text_io.file_type;
info_font : typtfont(1..500);
nbfont : integer;
infocodesp : string30:=(1..30=>' ');
infocode : string30:=(1..30=>' ');
infocode_nbcar : integer;
p : positive;
idpresent : boolean :=true; -- presence de toutes les ecritures que les blasons referencent
idtrouve : boolean;
seriepresent : boolean :=true; -- presence de toutes les series invoquees
blasonpresent : boolean :=true; -- presence de tous les blasons invoques
formatvalid : boolean :=true; -- validite du format de toutes les lignes 
codetypopresent : boolean :=true; -- presence de tous les codes typo invoques
nbplaces : ANbplaces;
VAPlace : Aplace;
nbl : positive;
indpbla : positive:=1; -- indice absolu du premier blason d'une serie
imatex : character;
s : string100;
ch : character;
l : integer;
f : font_type;
abso : positive;
sb : string(1..32000);
lsb,dec : integer;
finsb : boolean;
no_lig : integer;

-- passage de relatif en absolu d'un numero de blason 
function RelaAbso (noSerie : in positive; rela : in positive) return positive is
abso : positive:=1;
begin
  for i in 1..noSerie-1 loop
    abso:=abso+(DefinSeries(i).nbla mod 100);
  end loop;
  abso:=abso+rela-1;
  return(abso);
end;

begin
eps:=0;

declare
  VSerie : ASerie(1..NS);
  VBlason : ABlason(1..NbBlasons);
  indser : positive;
  indbla : natural range 0..NbBlasons :=0;
begin
  hesp:=integer(float(hesp_terrain)*float(Resolution)*1000.0/float(InvEchelle));
  vesp:=integer(float(vesp_terrain)*float(Resolution)*1000.0/float(InvEchelle));

  for indser in 1..NS loop
    VSerie(indser).lnom:=DefinSeries(indser).lnom;
	VSerie(indser).nom(1..VSerie(indser).lnom):=DefinSeries(indser).nom(1..VSerie(indser).lnom);
    VSerie(indser).nbla:=DefinSeries(indser).nbla;
    VSerie(indser).decvert:=integer(float(DefinSeries(indser).decvert)*float(Resolution)*1000.0/float(InvEchelle));
	VSerie(indser).pbla:=indpbla;  
    indpbla:=indpbla+DefinSeries(indser).nbla;
  end loop; 

  for indbla in 1..NbBlasons loop
    VBlason(indbla).lnom:=DefinBlasons(indbla).lnom;
	VBlason(indbla).nom(1..VBlason(indbla).lnom):=DefinBlasons(indbla).nom(1..VBlason(indbla).lnom);
    VBlason(indbla).dx:=integer(float(DefinBlasons(indbla).dx)*float(Resolution)*1000.0/float(InvEchelle));
	VBlason(indbla).dy:=integer(float(DefinBlasons(indbla).dy)*float(Resolution)*1000.0/float(InvEchelle));
    VBlason(indbla).style:=indbla;
  end loop;


-----------------------------------------------------------------------------------------------------------
------------- CHARGEMENT DU FICHIER DES BLASONS ---------------------------------------------
-----------------------------------------------------------------------------------------------------------

  charge_fontes(info_font,nbfont);
  
  open(fic,in_file,ficblasons(1..ncar_ficblasons));

  begin
	
  no_lig:=0;	
  while not END_OF_FILE(fic) loop
    no_lig:=no_lig+1;	
  
    int_io.get(fic,id);

    idtrouve:=false;
    for i in 1..VANom_type'last-NbNoRou loop
      if VANom_type(i).id=id then
        indnom:=i;
        idtrouve:=true;
        exit;
      end if;    
    end loop;

    if idtrouve=false then
      idpresent:=false;      
      skip_line(fic);
      goto fin_boucle;
    end if;

    get(fic,char);
    get(fic,imatex);
    if imatex/='I' and imatex/='T' then
      formatvalid:=false;      
      skip_line(fic);
      goto fin_boucle;    
    end if;
    get(fic,char);

	nse := VANom_type(indnom).nser+1;

    l:=1;
    get(fic,ch);
    while ch/=tab loop
      s(l):=ch;
      l:=l+1;
      get(fic,ch);
    end loop;
    l:=l-1;

    if imatex='I' then
      for i in 1..NS+1 loop
        if i=NS+1 then
          seriepresent:=false;
          skip_line(fic);
          goto fin_boucle;
        end if;        
        if DefinSeries(i).lnom-integer'image(i)'length-1=l then
          if DefinSeries(i).nom(integer'image(i)'length+2..DefinSeries(i).lnom)=s(1..l) then
            VANom_type(indnom).CAPSer(nse).iser:=i;
            exit;
          end if;
        end if;
      end loop;  
    end if;

    if imatex='T' then
      for i in 1..nbfont+1 loop
        if i=nbfont+1 then
          codetypopresent:=false;
          skip_line(fic);
          goto fin_boucle;
        end if;
        f:=info_font(i);
        if s(1..l)=info_font(i).code(1..l) then
          VANom_type(indnom).CAPSer(nse).iser:=NS+i;          
          exit;
        end if;        
      end loop;
    end if;

	  VANom_type(indnom).nser:=nse;

		get(fic,VANom_type(indnom).CAPSer(nse).jvert);
		get(fic,char);
		get(fic,VANom_type(indnom).CAPSer(nse).jhori);
		if VANom_Type(indnom).CAPSer(nse).jvert='H' then
			get(fic,char);
			get(fic,VANom_type(indnom).CAPSer(nse).jalig);
		end if;

		if VANom_type(indnom).CAPSer(nse).iser>NS then
      -- lecture d'une ligne concernant un blason texte
			VAnom_type(indnom).npic:=VAnom_type(indnom).npic+1;
			nbl:=VAnom_type(indnom).npic;
			VANom_Type(indnom).capict(1)(nbl).ser:=nse;
			get(fic,char);
			get_line(fic,VANom_Type(indnom).capict(1)(nbl).str,VANom_Type(indnom).capict(1)(nbl).lstr);
      police:=VANom_type(indnom).CAPSer(nse).iser-NS;
      -- empxyblastxt(VANom_type,indnom,nbl,police,info_font,hdc,nbl);      
      empxyblastxt(VANom_type,indnom,nbl,police,info_font,hdc,1);      
    else
      -- lecture d'une ligne concernant un blason image
      get(fic,ch);
      get_line(fic,sb,lsb);
      dec:=0;
      finsb:=false;
      while finsb=false loop
        l:=1;
        while sb(l+dec)/=tab and l+dec<=lsb loop        
          s(l):=sb(l+dec);
          l:=l+1;
        end loop;
        dec:=dec+l;
        if dec=lsb+1 then
          finsb:=true;
        end if;
        for j in 1..DefinSeries(VANom_type(indnom).CAPSer(nse).iser).nbla+1 loop
          if j=DefinSeries(VANom_type(indnom).CAPSer(nse).iser).nbla+1 then
            blasonpresent:=false;
            skip_line(fic);
            goto fin_boucle;
          end if;
          abso:=RelaAbso(VANom_type(indnom).CAPSer(nse).iser,positive(j));
          if DefinBlasons(abso).nom(integer'image(j)'length+2..DefinBlasons(abso).lnom)=s(1..DefinBlasons(abso).lnom-integer'image(j)'length-1) then
            VANom_type(indnom).npic:=VANom_type(indnom).npic+1;
            nbl:=VAnom_type(indnom).npic;
            VANom_Type(indnom).capict(1)(nbl).ser:=nse;
			ephem:=VSerie(VANom_type(indnom).CAPSer(nse).iser).pbla+j-1;
 			VANom_Type(indnom).capict(1)(nbl).style:=VBlason(ephem).style;
		  	VANom_Type(indnom).capict(1)(nbl).dx:=VBlason(ephem).dx;
  			VANom_Type(indnom).capict(1)(nbl).dy:=VBlason(ephem).dy;
            exit;
          end if;
        end loop;                
      end loop;        
    end if;
    << fin_boucle >>
    null;
    end loop;        
  close(fic);

  exception
    when event : others => 
     -- if Msg_Erreur=Null_Unbounded_string then
     -- if 1=1 then
	    Msg_Erreur:=To_Unbounded_String("Erreur de lecture:"&EOL
                                       &"Fichier: "&ficblasons(1..ncar_ficblasons)&eol
									   &"Ligne n�"&Integer'Image(integer(no_lig)));
     --end if;
    raise;
  end;
  
  if idpresent=false then 
    gb.MsgBox("Certains blasons sont associ�s � des �critures d'identifiant inconnu."," Fichier des blasons",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
  if seriepresent=false and NS>0 then 
    gb.MsgBox("Certaines s�ries de blasons image sont inconnues."&eol&
	          "Les blasons image concern�s ne seront pas plac�s."," Fichier des blasons",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
  if codetypopresent=false then 
    gb.MsgBox("Le code typo de certains blasons texte est inconnu."&eol&
	          "Les blasons texte concern�s ne seront pas plac�s."," Fichier des blasons",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
  if blasonpresent=false and NS>0 then 
    gb.MsgBox("Certains blasons image sont inconnus."&eol&
	          "Ils ne seront pas plac�s."," Fichier des blasons",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
  if formatvalid=false then 
    gb.MsgBox("Certaines lignes ne sont pas au format requis."&eol&
	          "Les blasons concern�s ne seront pas plac�s."," Fichier des blasons",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
  
-- DEFINITION DES PARAMETRES DES NOMS 

for i in 1..vanom_type'last-NbNoRou loop
	if vanom_type(i).nl=1 then

		VANom_type(i).calin(1)(1).empx:=vanom_type(i).ln(1);
		VANom_type(i).calin(1)(1).empy:=vanom_type(i).corps;
		VANom_type(i).calin(1)(2).empx:=vanom_type(i).ln(1);
		VANom_type(i).calin(1)(2).empy:=vanom_type(i).corps;

		if vanom_type(i).ncar2/=0 then

		VANom_type(i).calin(2)(1).empx:=vanom_type(i).ln1;
		VANom_type(i).calin(2)(1).empy:=vanom_type(i).corps;
		VANom_type(i).calin(2)(2).empx:=vanom_type(i).ln2;
		VANom_type(i).calin(2)(2).empy:=vanom_type(i).corps;

		end if;
	else
		VANom_type(i).calin(2)(1).empx:=vanom_type(i).ln1;
		VANom_type(i).calin(2)(1).empy:=vanom_type(i).corps;
		VANom_type(i).calin(2)(2).empx:=vanom_type(i).ln2;
		VANom_type(i).calin(2)(2).empy:=vanom_type(i).corps;
	end if;
	
end loop;

for i in 1..vanom_type'last-NbNoRou
  loop
    if vanom_type(i).npic > 0 then 
      for j in 1..NS loop
        if (VANom_type(i).capser(j).iser > 0) and (VANom_type(i).capser(j).iser <= NS) then
          VANom_type(i).capser(j).decvert:=vserie(VANom_type(i).capser(j).iser).decvert;
        end if;
      end loop;
    end if;
  end loop;

  for i in 1..vanom_type'last-NbNoRou loop
	  if (vanom_type(i).nl=2) or (vanom_type(i).ncar2/=0) then
			for nb in 1..vanom_type(i).npic loop
			  VANom_Type(i).capict(2)(nb).ser:=VANom_Type(i).capict(1)(nb).ser;
			  VANom_Type(i).capict(2)(nb).style:=VANom_Type(i).capict(1)(nb).style;
			  VANom_Type(i).capict(2)(nb).dx:=VANom_Type(i).capict(1)(nb).dx;  
			  VANom_Type(i).capict(2)(nb).dy:=VANom_Type(i).capict(1)(nb).dy;
			  if VANom_Type(i).capict(1)(nb).lstr /=0 then
			  	VANom_Type(i).capict(2)(nb).str:=VANom_Type(i).capict(1)(nb).str;
			  	VANom_Type(i).capict(2)(nb).lstr:=VANom_Type(i).capict(1)(nb).lstr;
			  end if;
		  end loop;
	  end if;
  end loop;	



-----------------------------------------------------------------------------------------------------------
----------- A L G O R I T H M E S   D E   P L A C E M E N T -----------------------------------------------
-----------------------------------------------------------------------------------------------------------


for indnom in 1..vanom_type'last-NbNoRou loop
  nbl:=1;
  drap:=true;

  while (drap=true) loop --- Cette boucle "while" couplee aux 2 variables "ligne" et "drap"
		       ---assure la gestion du postionnement des blasons autour des noms pouvant etre ecrits
		       ---indifferemment sur 1 ou 2 lignes, en sauvegardant les 2 configurations eventuelles	 

    if (vanom_type(indnom).nl = 2) then
      nbl:=2;
    end if;

 -- INITIALISATIONS 
	
    nbgd:=0;
	  nbct:=0;
	  b:=1;
	  c:=1;	

    for i in 1..2 loop
		  nbplaces(i):=0;
		  for j in 1..maxblatopo loop
			  VAPLace(j,2,i):=0;
		  end loop;
	  end loop;

	        
    --ALGORITHME SEPARANT LES PICTOGRAMMES CENTRES DES AUTRES 
        
    for i in 1..VANom_type(indnom).npic loop
		  nse:=VANom_type(indnom).capict(nbl)(i).ser;
		  if VANom_type(indnom).CAPSer(nse).jhori='C' then
			  nbct:=nbct+1;
			  if VANom_type(indnom).CAPSer(nse).jvert='H' then
				  cent(nbct,2):=1;
			  else
				  cent(nbct,2):=2;
			  end if;
			  cent(nbct,1):=i;
		  else
			  nbgd:=nbgd+1;
			  VAgd(nbgd):=i;
		  end if;
	  end loop;

	
    -- PLACEMENT DES PICTOGRAMMES CENTRES

    if nbct>0 then
	    while (c<=nbct) loop
        <<next>>
        emprise:=0;
		    for i in 1..nbplaces(cent(c,2)) loop
			    emprise:=emprise+VANom_type(indnom).capict(nbl)(VAPlace(i,1,cent(c,2))).dx+hesp; 
        end loop;
		    VAPlace(nbplaces(cent(c,2))+1,1,cent(c,2)):=cent(c,1);
        xtmp:=integer(0.5*float((VANom_type(indnom).calin(nbl)(cent(c,2)).empx-(VANom_type(indnom).capict(nbl)(cent(c,1)).dx+emprise))));
	
		    if cent(c,2)=1 then

		      if (xtmp<(-eps)) and
            (VANom_type(indnom).CAPser(VANom_type(indnom).capict(nbl)(cent(c,1)).ser).jalig='F') and
            ((VANom_type(indnom).CAPser(VANom_type(indnom).capict(nbl)(cent(c,1)).ser).iser)/=2) then
	           cent(c,2):=2;
		         goto next;
		       else
		       	 VAnom_type(indnom).capict(nbl)(VAPlace(1,1,cent(c,2))).x:=xtmp;
  			     VANom_type(indnom).capict(nbl)(cent(c,1)).y:=VANom_type(indnom).calin(nbl)(1).empy+vesp+VANom_type(indnom).CAPser(VANom_type(indnom).capict(nbl)(cent(c,1)).ser).decvert;
	  	     end if;

		    else
  			  VAnom_type(indnom).capict(nbl)(VAPlace(1,1,cent(c,2))).x:=xtmp;
				  VANom_type(indnom).capict(nbl)(cent(c,1)).y:=-VANom_type(indnom).capict(nbl)(cent(c,1)).dy-vesp-
          VANom_type(indnom).CAPser(VANom_type(indnom).capict(nbl)(cent(c,1)).ser).decvert;
		    end if;

		    for i in 2..nbplaces(cent(c,2))+1 loop
		      VAnom_type(indnom).capict(nbl)(VAPlace(i,1,cent(c,2))).x:=VAnom_type(indnom).capict(nbl)(VAPlace(i-1,1,cent(c,2))).x
          +VAnom_type(indnom).capict(nbl)(VAPlace(i-1,1,cent(c,2))).dx+hesp;
		    end loop;

		    nbplaces(cent(c,2)):=nbplaces(cent(c,2))+1;
		    c:=c+1;
      end loop;
      
	  end if;


    -- PLACEMENT DES PICTOGRAMMES NON CENTRES
    
    if nbgd>0 then 
      while (b<=nbgd) loop
		    for i in 1..2 loop
			    for j in 1..maxblatopo loop
				    VAPlace(j,2,i):=0;
			    end loop;
        end loop;

		    place:=false;
		    nse:=VANom_type(indnom).capict(nbl)(VAgd(b)).ser;
		    if VANom_type(indnom).CAPSer(nse).jvert='H' then
			    lin:=1; 
		    else
			    lin:=2;
		    end if;
	      if VANom_type(indnom).CAPSer(nse).jhori='G' then
			    xdep:=-eps+1;
			    sens:=1;
		    else
			    xdep:=VANom_type(indnom).calin(nbl)(lin).empx+eps-1;
			    sens:=-1;
		    end if;
        while (place=FALSE) loop
			    DETXPOS(x,xdep,nbplaces,VAPlace,VANom_type,indnom,sens,lin,hesp,nbl);
			    l:=1;
		      while (l<=VANom_type(indnom).capict(nbl)(VAgd(b)).dx) loop
				    xl:=x+(l*sens);
				    DETXPOS(xcur,xl,nbplaces,VAPlace,VANom_type,indnom,sens,lin,hesp,nbl);
            if xcur/=xl then
					    x:=xcur;
					    l:=0;
				    end if;
			  	  l:=l+1;
			    end loop;
          if (lin=1) then
				    if VANom_type(indnom).CAPSer(nse).jalig='F' then	
			        if VANom_type(indnom).CAPSer(nse).jhori='G' then
	          	  if (xl>(VANom_type(indnom).calin(nbl)(lin).empx+eps-1)) and (VANom_type(indnom).CAPSer(nse).iser/=2) then
                  lin:=2;
							    xdep:=-eps+1;
                else
							    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x;
 						      place:=TRUE;
						    end if;
					    else
						    if (xl<(-eps+1)) and (VANom_type(indnom).CAPSer(nse).iser/=2) then
					        lin:=2;
							    xdep:=VANom_type(indnom).calin(nbl)(lin).empx+eps-1;
                else
							    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x-VANom_type(indnom).capict(nbl)(VAgd(b)).dx+1;
							    place:=TRUE;
				        end if;
				      end if;
				    else
				      if VANom_type(indnom).CAPSer(nse).jhori='G' then
						    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x;
						    place:=TRUE;
					    else
						    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x-VANom_type(indnom).capict(nbl)(VAgd(b)).dx+1;
						   place:=TRUE;                                           
				      end if;
				    end if;
				    if place=TRUE then
					    VANom_type(indnom).capict(nbl)(VAgd(b)).y:=VANom_type(indnom).calin(nbl)(lin).empy+vesp-1;
				    end if;     
          else
                
				    if VANom_type(indnom).CAPSer(nse).jhori='G' then
					    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x;
					    place:=TRUE;
				    else
					    VANom_type(indnom).capict(nbl)(VAgd(b)).x:=x-VANom_type(indnom).capict(nbl)(VAgd(b)).dx+1;
					    place:=TRUE; 	
            end if;
            VANom_type(indnom).capict(nbl)(VAgd(b)).y:=-VANom_type(indnom).capict(nbl)(VAgd(b)).dy-vesp+1;
          end if;
                    
			    if place=true then
            nbplaces(lin):=nbplaces(lin)+1;
				    VAPlace(nbplaces(lin),1,lin):=VAgd(b);
				    b:=b+1;
			    end if;
        end loop;
      end loop;
	  end if;

    REPARTITION(vanom_type,nbplaces,hesp,indnom,VAPlace,nbl);
    
	  if (dectxt/=0) and (vanom_type(indnom).npic > 0) then
      MOVETXT(vanom_type,nbplaces,indnom,vserie,VAPlace,nbl);
    end if;

    --determination de l'emprise totale des blasons sur les lignes inferieure et superieure du nom

	  if nbplaces(1)/=0 then
      vanom_type(indnom).empblastop(nbl):=det_blasemp(vanom_type,indnom,1,vaplace,nbplaces,nbl);
    end if;

	  if nbplaces(2)/=0 then
      vanom_type(indnom).empblasdown(nbl):=det_blasemp(vanom_type,indnom,2,vaplace,nbplaces,nbl); 
	  end if;

    nbl:=nbl+1;
    if nbl>2 then
      drap:=false;
    end if;
    if (vanom_type(indnom).ncar2=0) and (nbl=2) then
      drap:=false;
    end if;
  end loop; --while
end loop; --for--

end;

end PABMAIN;
END PAB;
