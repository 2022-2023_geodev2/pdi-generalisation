
with gen_io; use gen_io;

package manipboite is

type Bloc_type is record
   p1,p2,p3,p4 : point_type;
end record;

procedure QUATRE_COINS( coin1          :in point_type;
		        angle          :in float;
		        emprise,hauteur:in integer;
			boite          :out bloc_type);


procedure QUATRE_COINS( coin1          :in point_type;
                        coin2	       :in point_type;
                        angle          :in float;
		        hauteur        :in integer;
			boite          :out bloc_type);

--=============================================================================
-- Fonction determinant si 2 boites se chevauchent:
function CHEVAUCH(b1,b2: in boite_type) return boolean;

--=============================================================================
-- Fonction determinant si 2 boites obliques se chevauchent:
function CHEVAUCH_OBLIQUE(b1,b2: in bloc_type) return boolean;

--=============================================================================
-- Fonction determinant les distances entre une boite et un point:
function DBPT ( b: in boite_type; pt: in point_type ) return natural;

--=============================================================================
-- Fonction determinant la distance entre un point et un contour de points:
function dptcont	(p: in point_type;
                 ncont: in positive;
                 cont: in point_liste_type ) return natural;
                 
--=============================================================================
-- Fonction determinant la distance entre une boite et un contour de points:
function DBCONT	( b: in boite_type;
                  ncont: in positive;
                  cont: in point_liste_type ) return natural;

--=============================================================================
-- Fonction determinant la distance entre une boite et une autre boite:
function DBB ( b1: in boite_type; b2: in boite_type ) return natural;

--=============================================================================
-- Fonction determinant si une boite d�borde d'un cadre pench�:
function DEBORDE ( b: in boite_type; pne,pno,pso,pse: point_type) return boolean;

--=============================================================================
-- Fonction dilatant une boite:
function DILATEE (b: in boite_type; dila_x,dila_y: in natural) return boite_type;

--=============================================================================
-- Fonction dilatant une boite oblique:
function DILATEE_OBLIQUE (b: in bloc_type;Angle : in float;dila_x,dila_y: in natural) return Bloc_type;

--=========================================================================
-- Fonction d�tectant si deux ouverts ]a,b[ et ]c,d[ s'intersectent:
Function Intersec(a,b,c,d:in point_type) return boolean;

--==========================================================================
end manipboite;
