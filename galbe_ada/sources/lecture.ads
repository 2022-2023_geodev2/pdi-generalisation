With gen_io; use gen_io;
With inipan; use inipan;
with norou; use norou;

Package Lecture is

-- Gestion des symboles ponctuels decrits par un contour de point
Type Symbole_type is record
     Nom    : string(1..30);      -- Nom du symbole;
     Nb_pts : natural;            -- Nombre de points decrivant le symbole
     Tab_Coord  : Point_liste_type(1..300); -- Liste des points decrivant le contour
           -- du symbole, origine en (0,0) ou le diametre si une seule coordonnee,
           -- l'unite prevue est le metre terrain
end record;

Type Symbole_liste_type is array(natural range<>) of Symbole_type;

-- ====================================================================
-- Procedure visant a analyser les coordonnees d'un fichier Geoconcept
-- (chaque coordonnee etant separee par un ;) et a extraire :
--  * le min-max des coordonnees reelles
--  * le nombre d'enregistrements du fichier
--  * le nombre maximum de points par arc

Procedure AnalyseCoord	( 
        Nom_fic  : in string;		-- Non du fichier de type Geoconcept
        Position : in integer;		-- Position ou trouver les coordonnes
        Minimum  : out point_type_reel;	-- Minimum des coordonnees
        Maximum  : out point_type_reel;	-- Maximum des coordonnees
        Nb_enregistrement : out integer; -- Nombre d'enregistrements du fichier
        Nb_pts_max : out integer;	-- Nombre maximum de points par arc
        Nb_Pts_Courbes : out integer;
	    AnalyseRoute : boolean);
      
-- ====================================================================
-- Procedure visant a analyser les coordonnees du fichier des ecritures
-- (chaque coordonnee etant separee par un ;) et a extraire :
--  * le min-max des coordonnees reelles des pts d'accroche et des arcs
--  * le nombre d'enregistrements du fichier
--  * le nombre maximum de points par arc
       
Procedure AnalyseCoordNoms ( 
        Nom_fic  : in string;		-- Non du fichier de type Geoconcept
        Position : in integer;		-- Position ou trouver les coordonnes
        Minimum  : out point_type_reel;	-- Minimum des coordonnees
        Maximum  : out point_type_reel;	-- Maximum des coordonnees
        Nb_enregistrement : out integer;-- Nombre d'enregistrements du fichier
        Nb_pts_max : out integer;	-- Nombre maximum de points par arc
		NbLignes : out integer);
                                                          
-- ====================================================================
-- Fonction de conversion d'un Point_type en coordonnees reelles terrain
-- (metres) en un point_type en coordonnees entieres Plage (points), en fonction
--   - des coord. min sur le terrain
--   - de l'inverse de l'echelle (par ex 25000)
--   - de la resolution (nb de points par millimetres a l'echelle finale)

Function Convert_coord_terrain_graphe (
        Point     			: Point_type_reel; 	-- Coordonnees du pt, en metre
        InvEchelle      : positive;		-- Inverse echelle du graphe
        Resolution      : positive;		-- Nombre de pts par mm du graphe
        Minimum_terrain : Point_type_reel	-- Origine du graphe, en metre
                                      ) return Point_type;

-- ====================================================================
-- Fonction de conversion d'un Point_type en coordonnees entieres Plage (points)
-- en un point_type en coordonnees reelles terrain (metres), en fonction
--   - des coord. min sur le terrain
--   - de l'inverse de l'echelle (par ex 25000)
--   - de la resolution (nb de points par millimetres a l'echelle finale)

Function Convert_coord_graphe_terrain (
        Point    			 	: Point_type; 	-- Coordonnees du pt Plage
        InvEchelle      : positive;		-- Inverse echelle du graphe
        Resolution      : positive;		-- Nombre de pts par mm du graphe
        Minimum_terrain : Point_type_reel	-- Origine du graphe, en metre
                                      ) return Point_type_reel;



-- function Nbre_Portions_dans_image(Tab_Pts : point_liste_type, nb_pts : integer) return positive;

-- ==========================================================================
-- Procedure de lecture du fichier des routes pour creer le graphe des routes
procedure LireRoutes (
               NOM_Routes 	: in string ;		-- Nom du fichier des routes
               NOM_GRAPHE 	: in string ;		-- Nom du graphe des routes
               InvEchelle 	: in positive ;		-- Inverse echelle du graphe
               pts_par_mm 	: in positive;		-- Nombre de pts par mm du graphe
               Minimum_terrain : in point_type_reel;	-- Origine du graphe, en metre
               Maximum_terrain : in point_type_reel;	-- Maximum du graphe
               Nb_Arcs_a_lire : integer;           -- calcule au prealable
               Nb_Arcs_charges : in out integer;  -- calcule au prealable
               Nb_pts_routes 	: in integer;		-- Nb maximum de pts par route
               TAN : in out TARC_NOM;
			   NbNom : in integer;  -- Nb d'ecritures horizontales
			   Nb_Pts_Courbes : in integer);  -- Nb total des pts de courbes de niveau


-- ======================================================================
-- Procedure de lecture du fichier des ecritures pour creer le graphe associe
-- cas o� il existe un fichier de symboles d'appui (appui=true)

procedure LireEcritures (Nom_Ecritures 	: in string;
                         Nom_GRAPHE 	: in string ;
                         InvEchelle 	: in positive;
                         pts_par_mm 	: in positive;
                         Minimum_terrain : in point_type_reel;
                         Maximum_terrain : in point_type_reel;
                         Nb_ecritures 	: in integer;
                         Nb_pts_ecritures : in integer;
                         Tab_symboles 	: Symbole_liste_type;
                         Nb_symboles 	: integer;
                         tbnomi : in out typtbnomi);

-- ======================================================================
-- Procedure de lecture du fichier des ecritures pour creer le graphe associe
-- cas o� il n'y a pas de fichier de symboles d'appui (appui=false)

procedure LireEcritures (Nom_Ecritures 	: in string;
                         Nom_GRAPHE 	: in string;
                         InvEchelle 	: in positive;
                         pts_par_mm 	: in positive;
                         Minimum_terrain : in point_type_reel;
                         Maximum_terrain : in point_type_reel;
                         Nb_ecritures 	: in integer;
                         Nb_pts_ecritures : in integer;
                         tbnomi : in out typtbnomi);

-- ======================================================================
-- Procedure de lecture du fichier des symboles pour stocker les symboles
-- (nom + nombre de points du contour + coordonnees des points du contour)

procedure LireSymbolesPonct (NOM_SYMBOLES 	: in string;
                             InvEchelle 	: in positive;
                             pts_par_mm 	: in positive;
                             Tab_symboles 	: in out Symbole_liste_type;
                             Nb_symboles  	: in integer);
                             
-- ======================================================================
end Lecture ;
