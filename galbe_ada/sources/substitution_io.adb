--============================ CORPS DU PAQUETAGE SUBSTITUTION_IO =====================

with text_io; use text_io;
with ada.integer_text_io; use ada.integer_text_io;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body SUBSTITUTION_IO is 


------------------------------------------------------------------------------------------------------------
-- procedure de lecture du fichier contenant la table des interpretations
-- et la table de substitution
------------------------------------------------------------------------------------------------------------
procedure Charge_tabsub(tsubst: out acces_tabsub;
                        tinterp: out acces_tabinterp;
                        nb_interp: out integer) is

chaine: string(1..32000):= (others => ' ');
nb_sep, long_chaine, last, index, pos, nb_tab: integer:=0;
fichier_tds: text_io.file_type;
no_lig: text_io.count;
MonI : integer;

begin
  open(fichier_tds, text_io.in_file, nomfichier_tds(1..ncar_tds));
  -- on recupere le nb d'interpr�tations:
  get_line(fichier_tds, chaine, long_chaine);
  close(fichier_tds);
  for i in 1..long_chaine loop
    if chaine(i)=tab or chaine(i)=';' then nb_sep:=nb_sep+1; end if;
  end loop;
  nb_interp:=nb_sep;
  -- on initialise la table des interpr�tations
  tinterp:= new tabinterp(1..nb_interp);
  tinterp.all:= (others => (others => ' '));
  -- on initialise la table de substitution
  tsubst:= new tabsub(0..255, 1..nb_interp);
  -- initialisation a -1 et non 0 pour ne pas que les index 255,254... soient pris comme fond
  tsubst.all:= (others => (others => -1));
  -- on remplit les tables
  for interp in 1..nb_interp loop
    open(fichier_tds, text_io.in_file, nomfichier_tds(1..ncar_tds));
    chaine := (others => ' ');
    get_line(fichier_tds, chaine, long_chaine);
    
    nb_sep:=0;
    nb_tab:=0;
    for j in 1..long_chaine loop
      if chaine(j)=tab then nb_sep:=nb_sep+1; nb_tab:=nb_tab+1; end if;
      if chaine(j)=';' then nb_sep:=nb_sep+1; end if;
      if nb_sep=interp then
        for k in 1..30 loop
          if (j+k)>long_chaine then exit; end if;
          if chaine(j+k)=tab then exit; end if;
	      if chaine(j+k)=';' then exit; end if;
          tinterp(interp)(k):=chaine(j+k);
        end loop;
        exit;
      end if;
    end loop;
    
    loop
      if end_of_file(fichier_tds) then exit; end if;
      chaine := (others => ' ');
      begin
        no_lig:=line(fichier_tds);
        get_line(fichier_tds, chaine, long_chaine);
      exception when text_io.END_ERROR =>
        close(fichier_tds);
        open(fichier_tds, text_io.in_file, nomfichier_tds(1..ncar_tds));
        set_line(fichier_tds, no_lig);
        long_chaine:=0;
        while not End_of_file(fichier_tds) loop
          long_chaine:=long_chaine+1;
          get(fichier_tds, chaine(long_chaine));
        end loop;
      end;
      ada.integer_text_io.get(chaine, index, last);
      pos:=1;
      for j in 1..nb_tab loop  
        while chaine(pos) /= tab loop -- tabulation 
           pos:=pos+1;
        end loop;
        pos:=pos+1;
      end loop;
      ada.integer_text_io.get(chaine(pos..chaine'last), monI, last); ---
      tsubst.all(index, interp):=MonI;
    end loop;
    close(fichier_tds);
  end loop;
exception when  Event : others =>
  Msg_Erreur:=                                                       
    To_Unbounded_String("Erreur de lecture du fichier: "&nomfichier_tds(1..ncar_tds)&eol
                        &"Ligne n� "&Integer'Image(integer(no_lig)));
  close(fichier_tds);
  raise;  
end Charge_tabsub;                            
                                      
-------------------------------------------------------------------------------
-- proc�dure capable de lire les pixels d'une image tiff et de renvoyer 
-- les poids de  chacun en fonction d'une table dite de substitution
-------------------------------------------------------------------------------------
procedure remplir_tpoi (file_image	: in out byte_io.file_type;
		                xpixmin		: in integer;
		                ypixmin		: in integer;
		                xpixmax		: in integer;
		                ypixmax		: in integer;
                        codetypo : in string30;
                        lps, ncol   : in integer;
                        stripoffset : in acces_tabstrip;
                        tinterp      : in acces_tabinterp;
                        tsubst      : in acces_tabsub;
                        nb_interp   : in integer;
		                tabpoids	: in out typtipoi) is

numstrip: integer:=0;
interp: integer:=0;
n: integer:=0;
val: byte;
val_int: integer;
pos: byte_io.positive_count:=1;
--TPOI_RANGE_ERROR		: exception; 
--OUT_IMAGE_ERROR		: exception;                           
p : typipoi;
index_inconnu : exception;
Leln,Lecol : integer;
                                                        
begin 
	
   if nb_interp=1 then
     interp:=1;
   else
     for k in 2..nb_interp loop
       if tinterp(k)=codetypo then  
         interp:=k;
         exit;
       end if;    
     end loop;
     if interp=0 then interp:=1;
     end if;
   end if;   
                 
   for ln in ypixmax..ypixmin loop         
   	 numstrip := ((ln-1)/lps)+1;
     pos:=byte_io.positive_count(stripoffset.all(numstrip)+(((ln-1) mod lps)*ncol)+xpixmin);      
     byte_io.set_index(file_image, pos);
     for col in xpixmin..xpixmax loop
       byte_io.read (file_image, val);
       val_int:=integer(val);
       if tsubst.all(val_int,interp)=-1 then
	   	 LeLn:=ln;
		 LeCol:=Col;
         raise Index_Inconnu;
	   end if;
       tabpoids(col-xpixmin+1,ypixmin-ln+1):=convert_poi(tsubst.all(val_int,interp));
       p:=tabpoids(col-xpixmin+1,ypixmin-ln+1);
     end loop;
   end loop;

  exception when Index_Inconnu =>
  Msg_Erreur:=to_unbounded_string("L'indexe"&integer'image(Val_int)&" est absent de la table de substitution"
  &eol&"Il est pr�sent dans l'image au pixel de colonne"&integer'image(LeCol)&" et de ligne"&integer'image(LeLn))&eol
  &"(origine : coin sup�rieur gauche)";
  raise;

end remplir_tpoi;                                                       
                                                                                                                           
-------------------------------------------------------------------------------------
end SUBSTITUTION_IO;

