--************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES **********************
--************************************************************************

 with INIPAN; use INIPAN;
 with DETPOS; use DETPOS;
 with PLACT_SEQUENTIEL; use PLACT_SEQUENTIEL;
 with RECUIT; use RECUIT;
 with TOPONYMIO; use TOPONYMIO;
 with LECTURE; use LECTURE;
 with SUBSTITUTION_IO; use SUBSTITUTION_IO;
 with GEN_IO; use GEN_IO;
 with TEXT_IO; use TEXT_IO;
 with ADA.STRINGS.UNBOUNDED; use ADA.STRINGS.UNBOUNDED;
 with WIN32.WINNT; use WIN32.WINNT;
 with WIN32.WINUSER; use WIN32.WINUSER;
 with GB;
 with DIALOG3;
 with DIALOG1;
 with window1;
 with Win32.Winbase; use Win32.Winbase;
 with Interfaces.C; use Interfaces.C;
 with verifmut; use verifmut;
 with NoRou; use NoRou;
 with NoRou2; use NoRou2;
 with math_int_basic; use math_int_basic;
 with manipboite; use manipboite;
 With ada.numerics.elementary_functions; use  ada.numerics.elementary_functions;
 with geometrie; use geometrie;
 with sequential_io;
 
-------------------------------------------------------------------------- 
--------------------------------------------------------------------------

package PPAT_CALCUL is

    package int_io is new text_io.integer_io(integer);
    package Fix_Io is new Text_Io.Fixed_Io(Fix);
    Package integ_io is new sequential_io(integer); use integ_io;

	procedure PAT_CALCUL(hWnd: in handle; -- Handle (identificateur) de fen�tre Win
                       ficPat: in string; -- Nom du fichier *.pat
                       ficNouvPos: in string); -- Nom du fichier des nouvelles positions
                                               -- (utilis� dans le module de placement).
                                               -- Par d�faut, FicNouvPos est une chaine vide
					   
end PPAT_CALCUL;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

package BODY PPAT_CALCUL is
  
package flo_io is new text_io.float_io(float); use flo_io; 

----------------------------------------------------------------------------
  
procedure PAT_CALCUL (hWnd : in Handle;
                      ficPat : in string;
                      ficNouvPos : in string) is


maxnci: constant integer:=5000; -- Nb max de noms en interaction avec un nom donn�
ficn: text_io.file_type;  -- Fichier *.pat
nbnom: integer:=-1; -- nombre de lignes du fichier des ecritures horizontales
Position: integer;	-- Indice de la tabulation courante
Nb_Arcs,        			-- Nb d'arcs du fichier des arcs, puis = Nb_Arcs_charges
Nb_Arcs_charges,            -- Nb d'arcs charges dans le graphe Grr
Nb_ecritures : integer:=0;	-- Nb d'�critures � lire
Nb_symboles : integer:=0;	-- Nb de symboles � lire
Nb_pts_routes, 			-- Nb max de pts par arc dans Routes
Nb_Pts_Courbes,         -- Nb total des pts des courbes de niveau
Nb_pts_ecritures,		-- Nb max de pts par arc dans Ecritures (contour)
nb_pts_symboles: integer;	-- Nb max de pts par arc dans Symboles (contour) 
Min_routes, Max_routes,		-- Min et max terrain (en m) du fic des routes
Min_ecritures, Max_ecritures,	-- Min et max terrain (en m) du fic des �critures
Minimum_terrain : point_type_reel := (float'last,float'last); -- Min terrain (en m)
Maximum_terrain : point_type_reel := (float'First,float'first); -- Max terrain (en m)
Min_symboles, Max_symboles: point_type_reel; -- Sans valeur
InvEchelle: integer;	-- Inverse de l'echelle de travail
Resolution: positive;	-- Resolution de travail (a calculer)
-- Resolution_image: positive;	-- Resolution de l'image
csg_image:point_type; -- Coord graphe du coin sup gauche de l'image de mutilation
top: integer :=0; -- Indice de ligne dans la fenetre d'avancement 
ficviews : text_io.file_type;
nomficviews : string120:=(others => gb.NullChar);
Ficecris : text_io.File_type;
Nomficecris : string120:=(others => gb.nullChar);
ficparamviews : Text_io.File_type;
nomficparamviews : string120;
miecp : boolean:=false; -- presence d'ecritures placees
miecb : boolean:=false; -- presence d'ecritures bloquees
compesp : integer:=0; -- compteur d'ecritures horizontales sans position possible
compeb : integer:=0; -- compteur d'ecritures horizontales bloquees au placement
compmtsp : integer:=0; -- compteur de meta-troncons de routes sans position possible
compmtb : integer:=0; -- compteur de meta-troncons de routes bloques au placement
CompRouSansOcc : integer:=0; -- compteur des routes qui n'ont aucun meta-troncon plac�
jo : integer;
maxbla : integer:=0;
vesp : integer:=0;
fblatex,fblaima : Text_io.File_type;
nomfichier_blatex : string120:=(others => gb.NullChar);
nomfichier_blaima : string120:=(others => gb.NullChar);
boitencombre : Boite_type;
ficviewm : Text_io.File_type; -- fichier contenant les symboles ponctuels et surfaces correspondant aux ecritures sans position
nomficviewm : string120:=(others => gb.NullChar); -- nom du fichier precedant 
ficparamviewm : Text_io.File_type; -- fichier des parametres pour la saisie des positions des ecritures sans position
icparamviewm : string120:=(others => gb.NullChar);
nomficparamviewm : string120:=(others => gb.NullChar);
NomDefFicNouvPos : string120:=(others => gb.NullChar); -- nom par defaut du fichier de positions des ecritures sans positions = celui que cree le module visual

TAN : Acces_TARC_NOM;
T_ARCS : Acces_TARCS;
Trtes : Acces_TROUTES;
T_TRS : acces_TTRONCON;
T_ZI : Acces_Typtbe;
T_POSITIONS : Acces_positions;
T_MT : Acces_TMT;

NBRE_TRONCONS : integer;
I : integer;
Coheroute : boolean:=true;
-- LeCODE : string30;
LeMODE_P : typcateg;
A_Mil : integer;
NomTron : string(1..NBRE_CAR_NOM);
IndTPos, j : integer;
Btemp, Bcour : boite_type;
boite_qC, boite_qC2 : bloc_type;
coin1_terrain_MT : point_type_reel;

Ligne : string (1..20):=(1..20 => ' ');	-- Ligne du fichier des noms d'arcs
LLigne : integer;   -- Longueur de la ligne
last: positive;
str1,Str2,Str3,str4,Str5,Str6,Str7 : Unbounded_String := Null_Unbounded_String;

Delta_Terrain : float;
Resol_Mut_reel : float;
bidon : integer; -- variable bidon utilisee dans l'appel de AnalyseCoord sur le fichier des symboles d'appui
couleur, epaisseur, styligne : integer;

inforr : info_type;
Tab_arc	: point_liste_type(1..NBRE_PTS_D_ARC);
cpt : positive;
Sommital : boolean;

infoos_local : info_type;

FParamTiff,FCheminParamTiff : Text_io.file_type;
ppmm : integer;
ficvecteur : Text_io.File_type; -- fichier contenant les donn�es � incruster pour cr�er l'image de mutilation
CreMutil : boolean;
ji : integer;
Estomp_CoinInfG : point_type_reel;
Estomp_index : integer;
ImaEcr_CoinInfG : point_type_reel;
ImaEcr_index : integer;
Ligne30 : string30;
NbEcrPos : integer; -- Nbre d'ecritures ayant des positions possibles
-- GeomLast,
LL : integer;
-- TPIJ : Position_type;
-- TPIJGeomLast : integer;
--le_TPOSITIONS, le_TP : position_type;
--Le_TP_NGeom, Le_TP_NGeom1,Le_TP_NGeom2 : integer;
BoolBidon : boolean;
Le_Nb_ecritures,Le_NbNorou,le_nBRE_ROUTES : integer;
bloc : bloc_type;
nomfichier : string120;
ficnecritures : Text_io.file_type;
Nb_Ecritures_Plus_NbNoRou : integer;
--Fic_avant_det_noms,Fic_apres_det_noms,Fic_avant_det_ci,
--Fic_avant_det_pi,Fic_apres_det_pi,
--Fic_avant_sauvegarde, Fic_apres_sauvegarde, Fic_avant_declare_Tnpi_Tppi_Lpi : text_io.file_type;
Attendre : boolean:=true; -- devient false qd l'image de mutilation est prete
MutilCreee : boolean:=false; -- indique si une image de mutilation a �t� cr��e


use type Interfaces.C.Char_Array;
use type gb.Bool;
subtype C_String is Interfaces.C.Char_Array; 
  
function StringZ(S : string) return C_String is
SZ : C_String(Interfaces.C.Size_t(S'first)..Interfaces.C.Size_T(S'last+1));
begin
  for I in S'Range loop
    SZ(Interfaces.C.Size_T(I)):=Win32.Char'Val(Character'Pos(S(I)));
  end loop;
  SZ(SZ'LAST):=Win32.Char'val(Character'Pos(ASCII.NUL));
  return SZ;
end StringZ;

procedure Delete_file(File_Name : string) is
File_Name_Z: aliased C_String (Interfaces.C.Size_t(File_Name'first)..Interfaces.C.Size_t(File_Name'last+1)):= StringZ(File_Name);
R : Win32.INT := 0;
begin
  if Win32.Winbase.Deletefile(File_Name_Z(1)'Unchecked_Access)=0 then
    R:=R;
  end if;
end Delete_File;     

function To_C_String (Str : String) return C_String is      
Result : C_String (1 .. 1024);
begin
  for I in Str'Range loop         
    Result(Interfaces.C.Size_t(I)):=gb.Char'Val(Character'Pos(Str(I)));
  end loop;
  return Result (Interfaces.C.Size_t(Str'First)..Interfaces.C.Size_t(Str'Last));
end To_C_String;

--function  MsgBox1(Text : String := gb.NullString; Caption : String := gb.NullString; 
--                  Style : Win32.UInt := 0; Handle : Win32.WinDef.hWnd := gb.NullHandle) return Win32.Int is
--begin
--  return Win32.WinUser.MessageBox(Handle, Convert(Text & gb.NullString), Convert(Caption & gb.NullString), Style);
--end;
--
--procedure MsgBox2(Text : String := gb.NullString; Caption : String := gb.NullString; 
--                  Style : Win32.UInt := 0; Handle : Win32.WinDef.hWnd := gb.NullHandle) is
--IMB : Win32.Int;
--begin
--  iMB := Win32.WinUser.MessageBox(Handle, Convert(Text & gb.NullString), Convert(Caption & gb.NullString), Style);
--end;


begin

  Risque_Chevauchement:=false;
  TnpiMax:=0;
  Msg_Erreur:=To_Unbounded_String("Erreur d'ex�cution");

  -- initialisation des noms de fichiers
  nomfichier_objet  :=(1..120 => ' '); -- nom du fic des �critures
  nomfichier_image  :=(1..120 => ' '); -- nom de l'image de mutilation
  nomgraphe :=(1..120 => ' ');     -- nom du graphe des �critures
  nomgraphe_routes :=(1..120 => ' '); -- nom du graphe des routes
  nomfichier_param_calcul :=(1..120 => ' '); -- nom du fic des param de calcul
  nomfichier_param_recuit :=(1..120 => ' '); -- nom du fic des param de placement
  nomfichier_ambiguites :=(1..120 => ' '); -- nom du fic des ambiguit�s
  nomfichier_polices :=(1..120 => ' '); -- nom du fic des codes typo
  nomfichier_tds :=(1..120 => ' '); -- nom du fic de la table de substitution
  nomfichier_symboles :=(1..120 => ' '); -- nom du fichier des symboles d'appui
  nomfichier_Empilement  :=(1..120 => ' '); -- nom du fichier d'empilement 
  nomfichier_Palette  :=(1..120 => ' '); -- nom du fichier palette
  nomfichier_Estompage  :=(1..120 => ' '); -- nom du fichier palette

  top:=top+20;
  AFFICHAGE_DEROULEMENT(dialog3.Etape1, top); -- Initialisation
  -- Lecture du fichier .pat
  begin
    open(ficn,in_file,ficPat);
  exception when  Event : others =>  
    close(ficn);
    open(ficn,in_file,ficPat);
  end;
  
  -- Inverse de l'�chelle de la carte:
  set_line(ficn,6);  
  SKIPANDGET(ficn,InvEchelle);
  
  -- Fichier des �critures � placer:
  EXISTENCE_FIC(ficn, 9, nomfichier_objet, ncar_fo);
  -- si le nom du fichier des �critures � placer est blanc ou vide
  -- alors on prend le nom du fichier des donn�es lin�aires (ligne 24)  
  j:=0;
  for i in 1..ncar_fo loop
    if nomfichier_objet(I)/=' ' then exit; end if;
    J:=I;
  end loop;
  if J=Ncar_Fo then
  	NbNom:=0;
    EXISTENCE_FIC(ficn, 24, nomfichier_objet, ncar_fo);
  end if;
  close(ficn); 

  nbdir:=nbdir+1;
  tabdirvec(nbdir)(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  tabncrdir(nbdir,1):=ncar_fo-4;

  jo:=ncar_ff;
  loop
    if nomfichier_PAT(jo)='\' then
      exit;
    end if;
    jo:=jo-1;
  end loop;
  tabncrdir(nbdir,2):=jo;
  tabdirpat(nbdir)(1..jo):=nomfichier_PAT(1..jo);
  
  nomficparamviews(1..jo):=nomfichier_PAT(1..jo);
  nomficparamviews(jo+1..jo+10):="paramviews";
  
  nomficparamviewm(1..jo):=nomfichier_PAT(1..jo);
  nomficparamviewm(jo+1..jo+10):="paramviewm";
  
  open(ficn,in_file,ficPat);
  -- Fichier des codes typographiques (polices)
  EXISTENCE_FIC(ficn, 12, nomfichier_polices, ncar_pol);
  close(ficn);

  TEST_TABPOLICE(hwnd); -- Test de chargement de toutes les polices de ficn

  -- nom du graphe des ecritures = nom du fichier des �critures
  ncar_gr:=ncar_fo;
  nomgraphe(1..ncar_gr):=nomfichier_objet(1..ncar_fo);
  Position:=6;
  -- Lecture des coordonnees des �critures horizontales et verification de l'existence des codes typo invoqu�s
  if NbNom/=0 then  -- le fichier des �critures horizontales existe
    ANALYSECOORDNOMS(nomfichier_objet(1..ncar_fo),Position,Min_ecritures,Max_ecritures,Nb_ecritures,Nb_pts_ecritures,NbNom);
    -- nbnom:=Nb_ecritures;  -- Nb d'�critures � placer
    Minimum_terrain:=Min_ecritures; -- Initialisation de Min et maximum_terrain 
    Maximum_terrain:=Max_ecritures;
  end if;

  -- 1er test de declaration des tableaux de stockage des int�ractions entre positions 
  Msg_Erreur:=To_unbounded_string("Pas assez de m�moire pour stocker"&eol&
                                  "les int�ractions entre positions."&eol&eol&
								  "R�essayez en augmentant la m�moire"&eol&
							      "virtuelle ou en diminuant le nombre"&eol&
							      "de positions en interaction.");
  declare
    tnpi_test  :typtnpi(1..Nb_ecritures,1..maxp);
    tppi_test  :typtppi(1..Nb_ecritures,1..maxp);
    lpi_test   :typlpi (1..Nb_ecritures*maxp*maxnpi);
  begin
  	-- test ok : retour � la valeur par d�faut de Msg_Erreur
	Msg_Erreur:=to_unbounded_string("Erreur d'ex�cution");
  end;

  -- ancien emplacement
  -- TEST_TABPOLICE(hwnd); -- Test de chargement de toutes les polices de ficn

  open(ficn,in_file,ficPat);
      
  -- fichier des ambiguit�s entre codes typo:
  EXISTENCE_FIC(ficn, 15, nomfichier_ambiguites,ncar_amb);

  -- Fichier des parametres de calcul:
  EXISTENCE_FIC(ficn, 18, nomfichier_param_calcul,ncar_fpc);
  
  -- Fichier des parametres de placement:
  EXISTENCE_FIC(ficn, 21, nomfichier_param_recuit,ncar_fpr);
  
  -- Fichier des arcs � �viter de franchir:
  EXISTENCE_FICOPT(ficn, 24, nomgraphe_routes,ncar_grr, route);

  -- Fichier des symboles d'appui
  EXISTENCE_FICOPT(ficn, 36, nomfichier_symboles,ncar_fs, Appui);
	
  -- Fichier image de mutilation
  EXISTENCE_FIC2(ficn, 39, nomfichier_image, ncar_fi, mutil, cremutil);

  if mutil then
    begin
      -- Resolution de l'image
      set_line(ficn,42);
      SKIPANDGET(ficn,resolution_image);
      -- Emprise de l'image
      set_line(ficn,46);
      SKIPANDGET(ficn,tso.coor_x);  -- xmin (tso: terrain sud ouest)
      SKIPANDGET(ficn,tso.coor_y);  -- ymin
      SKIPANDGET(ficn,tne.coor_x);  -- xmax
      SKIPANDGET(ficn,tne.coor_y);  -- ymax
      tno.coor_x:=tso.coor_x;
      tno.coor_y:=tne.coor_y;
      tse.coor_x:=tne.coor_x;
      tse.coor_y:=tso.coor_y;
    exception  when others =>
      close(ficn);
      Msg_Erreur:=To_Unbounded_String("R�solution ou emprise de l'image incorrectes");
      raise;
    end;
    -- table de substitution
    EXISTENCE_FIC(ficn,49,nomfichier_tds,ncar_tds);
  end if;

  -- Fichier des blasons
  EXISTENCE_FICOPT(ficn,52,ficblasons,ncar_ficblasons,blasons);

  if cremutil then
    -- Fichier d'empilement
    EXISTENCE_FIC(ficn,55,nomfichier_empilement,ncar_fe);

    -- Fichier palette
    EXISTENCE_FIC(ficn,58,nomfichier_palette,ncar_fp);

	-- Estompage
    EXISTENCE_FICOPT(ficn,61,nomfichier_Estompage,ncar_Es,BoolBidon);
  end if;

  -- Fermeture du fichier .pat
  close(ficn);

  if (Option=Calcul or option=enchaine) and CreMutil=true then
    ji:=ncar_fi;
    loop
      if nomfichier_image(ji)='\' then
        exit;
      end if;
      ji:=ji-1;
    end loop;

    Create(FCheminParamTiff,Out_File,nomfichier_PAT(1..tabncrdir(nbdir,2))&"chemintif.txt","");
    put(FCheminParamTiff,nomfichier_image(1..ji-1));
    new_line(FCheminParamTiff);
    put(FCheminParamTiff,nomfichier_image(Ji+1..Ncar_fi));
    new_line(FCheminParamTiff);
    put(FCheminParamTiff,nomfichier_Palette(1..Ncar_fp));
    new_line(FCheminParamTiff);
    put(FCheminParamTiff,nomfichier_Estompage(1..Ncar_Es));
    new_line(FCheminParamTiff);
    nomfichier_ImageToponymes(1..ji):=nomfichier_image(1..ji);
    -- nomfichier_ImageToponymes(Ji+1..ji+14):="couverture.tif";
	-- Ncar_it:=Ji+14;
    nomfichier_ImageToponymes(Ji+1..ji+19):="image_ecritures.tif";
	Ncar_it:=Ji+19;
	put(FCheminParamTiff,nomfichier_ImageToponymes(1..Ncar_It));
    close(FCheminParamTiff);

    begin
      Create(FParamTiff,Out_File,nomfichier_image(1..ji)&"paramtif.txt","");
      int_io.put(FParamTiff,InvEchelle,0);
      new_line(FParamTiff);
      put(FParamTiff,tso.coor_x,0,2,0);
      new_line(FParamTiff);
      put(FParamTiff,tne.coor_X,0,2,0);
      new_line(FParamTiff);
      put(FParamTiff,tso.coor_y,0,2,0);
      new_line(FParamTiff);
      put(FParamTiff,tne.coor_y,0,2,0);
      new_line(FParamTiff);
      int_io.put(FParamTiff,Resol_Mut,0);
      close(FParamTiff);
      exception when others =>
        Msg_Erreur:=to_unbounded_string("L'image de mutilation ne peut pas �tre cr��e"&eol&"car le r�pertoire de cr�ation indiqu� n'existe pas :"&eol&eol&nomfichier_image(1..ji));
        raise;
    end;
  end if;
  
  -- Lecture des fichiers de donnnees pour determination de l'emprise de travail 
  -- et des tailles de tableau
  if route then
    -- Graphe des routes
    Position:=2;
    -- Lecture des coordonnees des routes
    ANALYSECOORD(nomgraphe_routes(1..ncar_grr),Position,Min_routes,Max_routes,Nb_Arcs,Nb_pts_routes,Nb_Pts_Courbes,true);

    if Nb_Arcs=0 then
      route:=false;
    end if;

   	if Min_Routes.coor_x<Minimum_terrain.coor_x then Minimum_terrain.coor_x:=Min_Routes.coor_x; end if;
    if Min_Routes.coor_y<Minimum_terrain.coor_y then Minimum_terrain.coor_y:=Min_Routes.coor_y; end if;
    if Max_Routes.coor_x>Maximum_terrain.coor_x then Maximum_terrain.coor_x:=Max_Routes.coor_x; end if;
    if Max_Routes.coor_y>Maximum_terrain.coor_y then Maximum_terrain.coor_y:=Max_Routes.coor_y; end if;
  end if;

  -- debut ajout 15.04.2005
  if option=enchaine or option=calcul then
    gb.Caption(dialog3.Etape1b,"Coord. lues : X=["&Integer'image(integer(Minimum_terrain.coor_X))&" <"&integer'image(integer(Maximum_terrain.coor_x))&"]"
                                          &"  Y=["&Integer'image(integer(Minimum_terrain.coor_Y))&" <"&integer'image(integer(Maximum_terrain.coor_Y))&"]");
    top:=top+20;
    AFFICHAGE_DEROULEMENT(dialog3.Etape1b, top); -- emprise des donnees vecteur
  end if;
  -- fin ajout 15.04.2005

  if appui then  
    -- fichier des symboles d'appui
    Position:=1;
    ANALYSECOORD(nomfichier_symboles(1..ncar_fs),Position,Min_symboles,Max_symboles,Nb_symboles,Nb_pts_symboles,bidon,false);
  end if;
  
  if mutil then  
    if tso.coor_x<Minimum_terrain.coor_x then Minimum_terrain.coor_x:=tso.coor_x; end if;
    if tso.coor_y<Minimum_terrain.coor_y then Minimum_terrain.coor_y:=tso.coor_y; end if;
    if tne.coor_x>Maximum_terrain.coor_x then Maximum_terrain.coor_x:=tne.coor_x; end if;
    if tne.coor_y>Maximum_terrain.coor_y then Maximum_terrain.coor_y:=tne.coor_y; end if;
  end if;
                         
  -- Determination de la resolution de travail
  Resolution:=Positive(160);
  if mutil then
    pt_image:=Positive(Resolution/Resolution_image);
    Resolution:=pt_image*Resolution_image;
  end if;
  ptparmm:=resolution;
  
  -- Mise d'une marge de 100 mm tout autour de la zone de travail
  Minimum_terrain.coor_x:=Minimum_terrain.coor_x-0.1*float(InvEchelle);
  Minimum_terrain.coor_y:=Minimum_terrain.coor_y-0.1*float(InvEchelle);
  Maximum_terrain.coor_x:=Maximum_terrain.coor_x+0.1*float(InvEchelle);
  Maximum_terrain.coor_y:=Maximum_terrain.coor_y+0.1*float(InvEchelle);

  -- Initialisation de l'emprise terrain des donnees vecteur :
  if option=enchaine or option=calcul then
    Vno:=(float'last,float'first);
    Vse:=(float'first,float'last);
  end if;

  if mutil then
  	-- Emprise de l'image en coord plage (pno: point nord ouest)
    pno:=CONVERT_COORD_TERRAIN_GRAPHE(tno,InvEchelle,resolution,Minimum_terrain);
    pne:=CONVERT_COORD_TERRAIN_GRAPHE(tne,InvEchelle,resolution,Minimum_terrain);
    pso:=CONVERT_COORD_TERRAIN_GRAPHE(tso,InvEchelle,resolution,Minimum_terrain); 
    pse:=CONVERT_COORD_TERRAIN_GRAPHE(tse,InvEchelle,resolution,Minimum_terrain);
  end if;
  
  -- Chargement des parametres de calcul
  CHARGE_PARAM;
  gb.Visible(dialog3.Panel3, gb.false);
  
  if route then
		
	if option=enchaine or option=calcul then
	  top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape2, top); -- cr�ation du graphe des donn�es lin�aires
    end if;

	-- augmentation de Nb_Arcs en prevision du clipage des arcs par l'image
    if mutil then
      -- debut modif 24.03.2006
	  -- Nb_Arcs_charges:=integer(float(Nb_Arcs+10)*1.6);
	  Nb_Arcs_charges:=integer(float(Nb_Arcs+10)*3.0);
	  -- debut modif 24.03.2006
    else
	  Nb_Arcs_charges:=Nb_arcs;
    end if;
    TAN := new TARC_NOM(1..Nb_Arcs_charges);
    nomgraphe_routes_plage:=(1..120 => ' ');
	nomgraphe_routes_plage(1..ncar_grr-4):=nomgraphe_routes(1..ncar_grr-4);

    gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
    gb.Move(dialog3.Panel2,0,0,0,20);
    gb.Move(dialog3.Panel3,0,0,0,20);
    gb.Visible(dialog3.Panel3, gb.true);
	gb.Visible(dialog3.Panel1, gb.true);
    gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
    gb.height(dialog3.form, gb.int(top+115));

	LIREROUTES(nomgraphe_routes(1..ncar_grr),
		       nomgraphe_routes_plage(1..ncar_grr-4),
               InvEchelle,Resolution,Minimum_terrain,Maximum_terrain,
               Nb_Arcs,Nb_Arcs_charges,Nb_pts_routes,TAN.all,NbNom,Nb_Pts_courbes);

	gb.Visible(dialog3.Panel1, gb.False);

    Nb_Arcs:=Nb_Arcs_charges;
    infor := GR_INFOS(grr);
    inforr:=infor;
    -- R�gionalisation du graphe
    GR_REGION(grr, 100*Resolution, 100*Resolution);
  end if;

  NbNoRou:=0;

  if (Option=Calcul or option=enchaine) and creMutil=true then
	goto Creatif;
  end if;
  << Reviens >>

  if (option=enchaine or option=calcul) and route and (StrNumRou(1)='O' or StrNumRou(1)='o' or StrCotesCourbes(1)='O' or StrCotesCourbes(1)='o' or StrEcriDispo(1)='O' or StrEcriDispo(1)='o') then
  	
    if option=enchaine or option=calcul then
      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape2b, top);  -- constitution et pr�d�coupage des objets lin�aires
    end if;

	T_ARCS := new TARCS(1..gr_infos(grr).nb_arcs);
    TRTES := new TROUTES(1..gr_infos(grr).nb_arcs);
    T_TRS := new TTRONCON(1..gr_infos(grr).nb_arcs);

    R_R(Grr,TAN.all,Nb_arcs,Trtes.all,T_ARCS.all,NBRE_ROUTES,Coheroute);

	-- 2e test de declaration des tableaux de stockage des int�ractions entre positions 
  Msg_Erreur:=To_unbounded_string("Pas assez de m�moire pour stocker"&eol&
                                  "les int�ractions entre positions."&eol&eol&
								  "R�essayez en augmentant la m�moire"&eol&
							      "virtuelle ou en diminuant le nombre"&eol&
							      "de positions en interaction.");
    declare
      tnpi_test  :typtnpi(1..Nb_ecritures+nBRE_ROUTES,1..maxp);
      tppi_test  :typtppi(1..Nb_ecritures+nBRE_ROUTES,1..maxp);
      lpi_test   :typlpi (1..(Nb_ecritures+nBRE_ROUTES)*maxp*maxnpi);
    begin
  	  -- test ok : retour � la valeur par d�faut de Msg_Erreur
	  Msg_Erreur:=to_unbounded_string("Erreur d'ex�cution");
    end;

    if NBRE_ROUTES>0 then

      DECOUPE_TRONCON(grr,Trtes.ALL,T_ARCS.all,T_TRS.ALL,resolution);

      top:=top+20;
      -- AFFICHAGE_DEROULEMENT(dialog3.Etape2B1, top); -- contr�le de coh�rence des modes de placement
       gb.Move(dialog3.Etape2B1, 20, gb.int(top), 185, 15);
       gb.Visible(dialog3.Etape2B1, gb.true);
       gb.Move(dialog3.CancelButton, 250, gb.int(top+35), 70, 20);
       gb.Visible(dialog3.CancelButton, gb.true);
       gb.height(dialog3.form, gb.int(top+90));

      -- ici, on verifie que les routes sont composees de troncons
      -- ayant meme code typo et meme mode de placement
      if option=enchaine or option=calcul then
	  	
--        I:=1;
--        while I<=gr_infos(grr).nb_arcs loop
--          if Trtes(I).PT_DEB=0 or coheroute=False then
--            exit; 	
--          end if;
--          if Trtes(I).PT_FIN=Trtes(I).PT_DEB then
--            goto finwhile;
--          end if;
--	      -- LeCODE:=TAN(abs(T_ARCS(Trtes(I).PT_DEB).N_ARC)).CODE;
--          LeMODE_P:=TAN(abs(T_ARCS(Trtes(I).PT_DEB).N_ARC)).MODE_P;
--	      for j in Trtes(I).PT_DEB+1..Trtes(I).PT_FIN loop
--            -- if TAN(abs(T_ARCS(J).N_ARC)).CODE/=LeCODE or
--			if TAN(abs(T_ARCS(J).N_ARC)).MODE_P/=LeMODE_P then
-- 	          Coheroute:=false;
--              exit;
--            end if;
--          end loop;
--          << finwhile >>
--          i:=i+1;
--        end loop;

		if coheroute=true then
          gb.Move(dialog3.Etape2B2, 212, gb.int(top), 300, 15);
          gb.Visible(dialog3.Etape2B2, gb.true);
        else
          gb.Move(dialog3.Etape2B3, 212, gb.int(top), 300, 15);
          gb.Visible(dialog3.Etape2B3, gb.true);       
        end if;

--        if Coheroute=false then
--          gb.MsgBox("Certains objets lin�aires sont compos�s d'arcs"&eol&
--		             "ayant des modes de placement diff�rents",
--					 " Fichier des donn�es lin�aires", Win32.WinUser.MB_ICONEXCLAMATION);
--        end if;
      end if;

      NBRE_TRONCONS:=TRTES(NBRE_ROUTES).pt_fin;

      T_zi:=New typtbe(1..NBRE_TRONCONS);
      T_POSITIONS:=new POSITIONS(1..NBRE_TRONCONS,1..maxP);
      T_MT:=new TMT(1..NBRE_TRONCONS);

      for i in T_POSITIONS'range loop  -- autrement dit 1..NBRE_TRONCONS 
        for j in 1..maxP loop
          T_POSITIONS(i,j).poss:=false;
        end loop;
      end loop;

      delta_xpix_r:=(pse.coor_x-pso.coor_x)/pt_image; -- +1 ??? 
      delta_ypix_r:=(pne.coor_y-pse.coor_y)/pt_image; -- +1 ??? Attention aux marges !

      if option=enchaine or option=calcul then
        top:=top+20;
        AFFICHAGE_DEROULEMENT(dialog3.Etape2c, top); -- calcul des positions possibles des �critures � disposition
      end if;

      gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
      gb.Move(dialog3.Panel2,0,0,0,20); 
      gb.Move(dialog3.Panel3,0,0,0,20); 
      gb.Visible(dialog3.Panel1, gb.true);
      gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
      gb.height(dialog3.form, gb.int(top+115));

      RECHERCHE_MT(Grr,Legr,TRTES.ALL,T_MT.ALL,T_TRS.ALL,T_ARCS.all,
	               T_POSITIONS.all,T_ZI.ALL,HWnd,InvEchelle,resolution);

      gb.Visible(dialog3.Panel1, gb.false);

      Le_NbNorou:=NbNorou;
      for i in T_MT.ALL'FIRST..T_MT.ALL'LAST loop
        if T_MT.ALL(i).T_Deb=0 then
          exit;
        end if;
        NbNoRou:=NbNoRou+1;
      end loop;
      Le_NbNorou:=NbNorou;

    end if;
  end if;

  if option=Placement then
  	nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  	nomfichier(ncar_fo-3..ncar_fo+11):="_NEcritures.tmp";
    open(ficnecritures,in_file,nomfichier(1..ncar_fo+11));
    int_io.get(ficnecritures,Nb_Ecritures_Plus_NbNoRou);
    close(ficnecritures);
	NbNoRou:=Nb_Ecritures_Plus_NbNoRou-Nb_Ecritures;
  end if;

  -- provoquons un plantage si Nb_ecritures+NbNoRou=0
  begin
  if Nb_ecritures+NbNoRou=0 then
    Ligne(1..1):="plante!";  	
  end if;
  exception  when others =>
    Msg_Erreur:=To_Unbounded_String("Il n'y a aucune �criture � placer");
    raise;
  end;

  Le_Nb_ecritures:=Nb_ecritures;
  Le_NbNorou:=NbNoRou;

  << creatif >>

  declare
    tbnomi : typtbnomi(1..Nb_ecritures+NbNoRou);
    tzi :typtbe (1..tbnomi'last):=(1..tbnomi'last=>((0,0),(0,0)));
    tnci :typtnci(1..tbnomi'last):=(others=>0);
    tici :typtici(1..tbnomi'last,1..maxnci):=((1..tbnomi'last)=>(1..maxnci=>0));
    tnp : typtnp(1..tbnomi'last):=(others=>0);
    tp : typtp (1..tbnomi'last,1..maxp);
  begin

	-- 3e test de declaration des tableaux de stockage des int�ractions entre positions 
    Msg_Erreur:=To_unbounded_string("Pas assez de m�moire pour stocker"&eol&
                                  "les int�ractions entre positions."&eol&eol&
								  "R�essayez en augmentant la m�moire"&eol&
							      "virtuelle ou en diminuant le nombre"&eol&
							      "de positions en interaction.");
    declare
      tnpi_test  :typtnpi(1..tbnomi'last,1..maxp);
      tppi_test  :typtppi(1..tbnomi'last,1..maxp);
      lpi_test   :typlpi (1..tbnomi'last*maxp*maxnpi);
    begin
  	  -- test ok : retour � la valeur par d�faut de Msg_Erreur
	  Msg_Erreur:=to_unbounded_string("Erreur d'ex�cution");
    end;

    if option=enchaine or option=calcul then

      -- Cr�ation du Graphe des Ecritures "gros" et de la legende associee "legos"
      nomgraphe_plage:=(1..120 => ' ');
      nomgraphe_plage(1..ncar_gr-4):=nomgraphe(1..ncar_gr-4);

      -- if nb_ecritures>0 then
      if nbNom>0 then
        if Mutilcreee=false then
		  top:=top+20;
          AFFICHAGE_DEROULEMENT(dialog3.Etape3, top); -- Cr�ation du graphe des �critures horizontales
        end if;

	    if appui then
          declare
            Tab_symboles : Symbole_liste_type(1..Nb_symboles);
          begin
            LireSymbolesPonct(nomfichier_symboles(1..ncar_fs),Invechelle, Resolution, Tab_symboles, Nb_symboles);
		    LireEcritures(nomgraphe(1..ncar_gr), nomgraphe_plage(1..ncar_gr-4),
                          InvEchelle, Resolution, Minimum_terrain, Maximum_terrain,
                          Nb_ecritures, Nb_pts_ecritures, Tab_symboles, Nb_Symboles,tbnomi);
          end;
        else
            LireEcritures(nomgraphe(1..ncar_gr), nomgraphe_plage(1..ncar_gr-4),
                          InvEchelle, Resolution, Minimum_terrain, Maximum_terrain,
                          Nb_ecritures, Nb_pts_ecritures, tbnomi);
        end if;

		if nb_ecritures>0 then
          infoos:= GR_INFOS(gros);
          -- Regionalisation du graphe
		  infoos_local:=infoos;
          GR_REGION(gros,100*Resolution,100*Resolution);
        end if;

        if MutilCreee=false then
		  top:=top+20;
          AFFICHAGE_DEROULEMENT(dialog3.Etape4, top); -- Chargement des informations sur les �critures horizontales
        end if;
        
      end if;

      if nbNorou>1 then
	    top:=top+20;
        AFFICHAGE_DEROULEMENT(dialog3.Etape4A, top); -- Stockage des positions possibles des �critures � disposition

        gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
        gb.Move(dialog3.Panel2,0,0,0,20);
	    gb.Visible(dialog3.Panel1, gb.true);
        gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
        gb.height(dialog3.form, gb.int(top+115));
      end if;

      for i in 1..NbNoRou loop

        if 300*(I-1)/NbNoRou /= 300*I/NbNoRou or I=NbNoRou then		
          gb.Move(dialog3.Panel2,0,0,gb.INT(300*I/NbNoRou),20);
        end if;

        declare
          nb_pts_max : integer :=gr_infos(grr).max_pts_arc;
          Ptsdarc : point_liste_type(1..nb_pts_max);
		  Nc : integer;	
        begin

          A_Mil:=(T_TRS(T_MT(I).t_DEB).A_DEB+T_TRS(T_MT(I).t_FIN).A_FIN)/2;
          Gr_Points_d_arc(grr,abs(T_ARCS(A_Mil).N_arc),ptsdarc,nc);
		  if decimal(float(nc)/2.0)=0.0 then -- Nc est pair
		  	Tbnomi(Tbnomi'LAST-NbNoRou+i).coord_objet:=
			(integer(float(ptsdarc(integer(float(nc)/2.0)).coor_x+ptsdarc(integer(float(nc)/2.0)+1).coor_X)/2.0)
			,integer(float(ptsdarc(integer(float(nc)/2.0)).coor_Y+ptsdarc(integer(float(nc)/2.0)+1).coor_Y)/2.0));
	      else -- Nc est impair
		    Tbnomi(Tbnomi'LAST-NbNoRou+i).coord_objet:=ptsdarc(max(integer(float(nc)/2.0),1));
	      end if;
        end;

        Tbnomi(Tbnomi'last-NbNoRou+i).id:=-i;

        Tbnomi(Tbnomi'last-NbNoRou+i).police:=T_MT(i).police;
        Tbnomi(Tbnomi'last-NbNoRou+i).corps:=T_MT(i).corps;
        Tbnomi(Tbnomi'last-NbNoRou+i).Nb_mots:=T_MT(i).Nb_Mots;
		for j in 1..T_MT(i).Nb_Mots loop
          Tbnomi(Tbnomi'last-NbNoRou+i).ln(J):=T_MT(i).L_topo(J);
          Tbnomi(Tbnomi'last-NbNoRou+i).ncar(J):=TAN(abs(T_ARCS(T_TRS(T_MT(I).T_DEB).A_DEB).N_ARC)).ncar(J);
          Tbnomi(Tbnomi'last-NbNoRou+i).chaine(J)(1..Tbnomi(Tbnomi'last-NbNoRou+i).ncar(J)):=TAN(abs(T_ARCS(T_TRS(T_MT(I).T_DEB).A_DEB).N_ARC)).nom(J)(1..Tbnomi(Tbnomi'last-NbNoRou+i).ncar(J));
        end loop;
        Tbnomi(Tbnomi'last-NbNoRou+i).ln1:=T_MT(i).L_topo(1);
        Tbnomi(Tbnomi'last-NbNoRou+i).Topo.code:=TAN(abs(T_ARCS(T_TRS(T_MT(I).T_DEB).A_DEB).N_ARC)).Code;
        Tbnomi(Tbnomi'last-NbNoRou+i).Topo.Mode_Placement:=TAN(abs(T_ARCS(T_TRS(T_MT(I).T_DEB).A_DEB).N_ARC)).Mode_P;

        Tbnomi(Tbnomi'last-NbNoRou+i).Topo.Objet_type:=Lineaire;
        if T_MT(i).Ecrase=true then
		  Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=Mutilation;
 	    end if;
        if T_MT(i).bord_image=true then
		  Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=bord_image;
        end if;
        if T_MT(i).hors_image=true then
		  Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=hors_image;
        end if;
        if T_MT(i).court=true then
		  Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=MetaCourt;
        end if;
        if T_MT(i).Agencement=true then
		  Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=Agencement;
        end if;

		-- priorite de hors image sur les autres causes de non placement
        if mutil then
		  if est_il_dans_rectangle(Tbnomi(Tbnomi'LAST-NbNoRou+i).coord_objet,Pno,Pne,pse,pso)=false then
            Tbnomi(Tbnomi'last-NbNoRou+i).Cause:=hors_image;
          end if;
        end if;

        for j in 1..maxP loop
          TP(Tbnomi'last-NbNoRou+i,j).poss:=false;
        end loop;

        j:=1;
        while T_POSITIONS(I,j).poss=true or j<=2 loop
			
		  if T_POSITIONS(I,j).poss=true then 
          Tp(Tbnomi'last-NbNoRou+i,J).poss:=true;
          Tp(Tbnomi'last-NbNoRou+i,J).nl:=1;
          Tp(Tbnomi'last-NbNoRou+i,J).poip:=T_POSITIONS(I,j).poip;
          -- coin1_terrain_MT:=Lecture.Convert_coord_graphe_terrain(T_POSITIONS(I,j).coin1,InvEchelle, resolution, Minimum_terrain);
          Tp(Tbnomi'last-NbNoRou+i,J).nb_mots:=Tbnomi(Tbnomi'last-NbNoRou+i).Nb_mots;
	      end if;
		  if j<=2 and (Tbnomi(Tbnomi'last-NbNoRou+i).TOPO.Mode_Placement=CDesaxe or Tbnomi(Tbnomi'last-NbNoRou+i).TOPO.Mode_Placement=CAxe) and T_POSITIONS(I,j).NGeom(1)/=0 then
            Tp(Tbnomi'last-NbNoRou+i,J).Geom := new point_liste_type(T_POSITIONS(I,j).Geom'range);
			Tp(Tbnomi'last-NbNoRou+i,J).Geom.all:=T_POSITIONS(I,j).Geom.all;
            Tp(Tbnomi'last-NbNoRou+i,J).NGeom(1):=T_POSITIONS(I,j).NGeom(1);
          end if;

		  if T_POSITIONS(I,j).poss=true then 
		  for K in 1..Tp(Tbnomi'last-NbNoRou+i,J).nb_mots loop
            Tp(Tbnomi'last-NbNoRou+i,J).offset(k):=T_POSITIONS(I,j).Offset(k);
			Tp(Tbnomi'last-NbNoRou+i,J).plusPi(k):=T_POSITIONS(I,j).plusPi(k);
            Tp(Tbnomi'last-NbNoRou+i,J).Point1(k):=T_POSITIONS(I,j).Point1(k);
            Tp(Tbnomi'last-NbNoRou+i,J).Point2(k):=T_POSITIONS(I,j).Point2(k);
            Tp(Tbnomi'last-NbNoRou+i,J).angle(k):=T_POSITIONS(I,j).angle(k);

			Tp(Tbnomi'last-NbNoRou+i,J).coin1(k):=T_POSITIONS(I,j).coin1(k);

          end loop;
	      end if;
          j:=j+1;
		  if j>MaxP then
		  	exit;
          end if;
        end loop;

		if T_POSITIONS(I,1).poss=true then
 	    	
		  if T_POSITIONS(I,2).poss=true then
		    Tnp(Tbnomi'last-NbNoRou+i):=J-1;
		  else
            Tnp(Tbnomi'last-NbNoRou+i):=1;
          end if;

          -- calcul de la ZI = boite d'encombrement du meta-troncon
          btemp:=((integer'last,integer'last),(integer'first,integer'first));
          for j in T_MT(I).t_DEB..T_MT(I).t_FIN loop
		    for k in T_TRS(J).A_DEB..T_TRS(J).A_FIN loop
              Bcour:=Gr_arc(grr,abs(T_ARCS(k).N_ARC)).Encombr;
		      Btemp:=((min(btemp.minimum.coor_x,bcour.minimum.coor_x),min(btemp.minimum.coor_y,bcour.minimum.coor_y))
                     ,(max(btemp.maximum.coor_x,bcour.maximum.coor_x),max(btemp.maximum.coor_y,bcour.maximum.coor_y)));
            end loop;
	      end loop;
          tzi(Tbnomi'last-NbNoRou+I):=Manipboite.dilatee(Btemp,2*integer(1.0+dist_route)*Tbnomi(Tbnomi'last-NbNoRou+i).corps,2*integer(1.0+dist_route)*Tbnomi(Tbnomi'last-NbNoRou+i).corps);

        end if;
      end loop;

	  if NbNorou>1 then
	    gb.Visible(dialog3.Panel1, gb.False);
      end if;

	  if NBRE_ROUTES>0 then

        for i in 1..NBRE_TRONCONS loop
	  	  for J in 1..2 loop
            if T_POSITIONS(i,j).geom/=null and T_POSITIONS(i,j).NGeom(1)/=0 then
              GR_Free_point_liste(T_POSITIONS(i,j).Geom);
            end if;
          end loop;
        end loop;
	    GR_Free_pOSITIONS(T_POSITIONS);
      end if;

      if route and (StrNumRou(1)='O' or StrNumRou(1)='o' or StrCotesCourbes(1)='O' or StrCotesCourbes(1)='o' or StrEcriDispo(1)='O' or StrEcriDispo(1)='o') then
        GR_Free_TROUTES(TRTES);
      end if;

 	if creMutil=true then

	  top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape4b, top); -- pr�paration des donn�es vecteur � incruster
      Estomp_Index:=-1;
      ImaEcr_Index:=-1;
      create(ficvecteur,Out_File,nomfichier_image(1..ji)&"vecteur1.txt","");
      export_creMutil(tbnomi,Ficvecteur,Ji,InvEchelle,Resolution,Minimum_terrain,Nb_symboles,Resol_mut,Estomp_CoinInfG,Estomp_Index,ImaEcr_CoinInfG,ImaEcr_Index);
      close(Ficvecteur);

	  open(FParamTiff,Append_File,nomfichier_image(1..ji)&"paramtif.txt","");
      int_io.put(FParamTiff,Estomp_Index,0);
      if Estomp_Index/=-1 then
        new_line(FParamTiff);
        put(FParamTiff,Estomp_CoinInfG.coor_x,0,2,0);
        new_line(FParamTiff);
        put(FParamTiff,Estomp_CoinInfG.coor_Y,0,2,0);
      end if;
	  new_line(FParamTiff);
      int_io.put(FParamTiff,ImaEcr_Index,0);
      if ImaEcr_Index/=-1 then
        new_line(FParamTiff);
        put(FParamTiff,ImaEcr_CoinInfG.coor_x,0,2,0);
        new_line(FParamTiff);
        put(FParamTiff,ImaEcr_CoinInfG.coor_Y,0,2,0);
      end if;
      close(FParamTiff);

      declare
        SI : aliased Win32.Winbase.STARTUPINFO;
        PI : aliased Win32.Winbase.PROCESS_INFORMATION;
        ProgramFile : string(1..ncar_nde+13):=nomdirexe(1..ncar_nde)&"\cremutil.exe";
        CommandLine : string:="";
        CommandLineZ : aliased C_STRING(1..Interfaces.C.Size_t(ProgramFile'length+1+CommandLine'length+1));
        ProgramLength : integer:=0;
        Directory_Name_Z : aliased C_STRING(Interfaces.C.Size_t(nomdirexe'first)..Interfaces.C.Size_t(nomdirexe'last+1)):=StringZ(nomdirexe);
      begin
        CommandLineZ(1..Interfaces.C.Size_t(ProgramFile'length)):=To_C_String(ProgramFile);
        for i in 1..ProgramFile'length loop
          exit when CommandLineZ(Interfaces.C.Size_t(i))=Win32.Char'Val(Character'Pos(ASCII.NUL));
          ProgramLength:=i;
        end loop;
        CommandLineZ(Interfaces.C.Size_T(ProgramLength+1)):=Win32.Char'Val(Character'Pos(' '));
        CommandLineZ(Interfaces.C.Size_T(ProgramLength+2)..Interfaces.C.Size_T(ProgramLength+CommandLine'length+1)):=To_C_String(CommandLine);
        CommandLineZ(Interfaces.C.Size_T(ProgramLength+CommandLine'length+2)):=Win32.Char'Val(Character'Pos(ASCII.NUL));
        win32.winbase.getstartupinfo(SI'Unchecked_Access);
        if win32.winbase.createprocess(
              null,                              -- application name
              CommandLineZ(1)'Unchecked_Access,  -- command line
              null,                              -- process attributes
              null,                              -- thread attributes 
              0,                                 -- new process inherit handles?
              HIGH_PRIORITY_CLASS,               -- creation flags
              null,                              -- optional environment
              null,                              -- new current directory
              SI'Unchecked_Access,               -- startup information
              PI'Unchecked_Access)               -- process information
              =0 then
           Msg_Erreur:=To_Unbounded_String("Impossible d'acc�der au fichier Cremutil.exe");
           -- provoquons un plantage :
		   Ligne(1..1):="plante!";  	
        else
           -- gb.MsgBox("Cliquez OK quand l'image"&eol&"de mutilation sera pr�te","Mise en attente");

	       top:=top+20;
           AFFICHAGE_DEROULEMENT(dialog3.Etape4C, top); -- cr�ation de l'image de mutilation

		   -- on attend jusqu'a ce que l'image de mutilation soit prete
           while attendre=true loop
             open(ficn,in_file,ficPat);
	         begin
               EXISTENCE_FIC2(ficn, 39, nomfichier_image, ncar_fi, mutil, Attendre);		     
             exception  when others => null;
             end;
             close(ficn);
           end loop;

	  	   -- PAT_CALCUL(hWnd,ficPat,ficNouvPos);
	    end if;
      exception  when others =>
        raise;
      end;
	  CreMutil:=false;
	  MutilCreee:=true;
	  goto reviens;
    end if;

      -- Chargement des polices, c�sure, et calcul des ZI des ecritures horizontales
      -- Remplissage de tbnomi;
	  DET_NOMS(tbnomi,--Nb_ecritures,
	  tzi,maxbla,vesp,InvEchelle,Resolution,Minimum_terrain,hWnd);

	  -- cet affichage cause un plantage dans certains cas !!
      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape5, top);  -- D�termination des interactions entre �critures
	  
      gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
      gb.Move(dialog3.Panel2,0,0,0,20);
      gb.Visible(dialog3.Panel1, gb.true);
      gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
      gb.height(dialog3.form, gb.int(top+115));

   	  -- Determination des interactions entre �critures
      DET_CI(tbnomi,tzi,tnci,tici);

      gb.Visible(dialog3.Panel1, gb.False);

      -- Determination des positions possibles des �critures horizontales
      DET_POSITIONS(tbnomi,tnci,tici,tzi,tnp,tp,mutil,Nb_symboles,InvEchelle,Resolution,Minimum_terrain,Maximum_terrain,top,Nb_Ecritures);

	  -- Destruction des graphes;
      -- if route then GR_DELETE(grr); end if;
      -- GR_DELETE(gros); 
    end if; -- option=enchaine or option=calcul

    if option=calcul then
		
      gb.Visible(dialog3.Panel1, gb.false);
      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape9, top); -- Pr�paration des fichiers de sortie

      -- cr�ation des fichiers binaires des tableaux des positions
	  -- et du fichier texte des �critures sans position
      SAUVEGARDE(tbnomi,tnp,tp,tnci,tici,InvEchelle, Resolution, Minimum_terrain, sansP);

      -- cr�ation du fichier "_viewerm" au format vecteur Viewer contenant la
	  -- g�om�trie des objets � renseigner interactivement dans le module de saisie
      nomficviewm(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
      nomficviewm(ncar_fo-3..ncar_fo+2):="_viewm";
      create(ficviewm,Out_File,nomficviewm(1..ncar_fo+2),"");
      put_line(ficviewm,"FORMAT VECTEUR VIEWER 1.0");
      put_line(ficviewm,"COORDONNEES TERRAIN");
      exportvecviewerm_horiz(tbnomi,ficviewm,InvEchelle,Resolution,Minimum_terrain,Nb_symboles,Tnp,true,true,true,true,true,true,true,true);
      if NbNoRou>0 then
        exportvecviewerm_NoRou(tbnomi,ficviewm,InvEchelle,Resolution,Minimum_terrain,Nb_symboles,Tnp,T_MT.ALL,T_TRS.ALL,T_ARCS.all,true,true,true,true,true,true,true,true);
      end if;
      close(ficviewm);

      -- cr�ation du fichier de param�tres "paramviewm" n�cessaire au module de saisie
	  create(ficparamviewm,Out_File,nomficparamviewm(1..jo+10),"");
      if mutil then
		put(ficparamviewm,nomfichier_image(1..ncar_fi));
      else
  	    if nomdirexe(1)='"' then
		  put(ficparamviewm,nomdirexe(2..ncar_nde)&"\fond.jpg");
        else
		  put(ficparamviewm,nomdirexe(1..ncar_nde)&"\fond.jpg");			
        end if;
      end if;
	  new_line(ficparamviewm);
      put(ficparamviewm,nomficviewm(1..ncar_fo+2));
      new_line(ficparamviewm);
      put(ficparamviewm,nomfichier_objet(1..ncar_fo-4)&"_SansPos.txt");
      new_line(ficparamviewm);
      NomDefFicNouvPos(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
      NomDefFicNouvPos(ncar_fo-3..ncar_fo+8):="_NouvPos.txt";
      put(ficparamviewm,NomDefFicNouvPos(1..ncar_fo+8));
      new_line(ficparamviewm);
      put(ficparamviewm,nomfichier_polices(1..ncar_Pol));
      new_line(ficparamviewm);
      if mutil then 
		put(ficparamviewm,tno.coor_x,0,2,0);
        new_line(ficparamviewm);
        put(ficparamviewm,tno.coor_y,0,2,0);
        new_line(ficparamviewm);
        put(ficparamviewm,float(InvEchelle)/(float(Resol_Mut)*1000.0),0,5,0);
      else
		put(ficparamviewm,Vno.coor_x,0,2,0);
        new_line(ficparamviewm);
        put(ficparamviewm,Vno.coor_y,0,2,0);
        new_line(ficparamviewm);
		Delta_Terrain:=max(Vse.coor_x-Vno.coor_x,Vno.coor_y-Vse.coor_y);
        Resol_Mut_reel:=float(InvEchelle)*8.0/Delta_Terrain;
        put(ficparamviewm,float(InvEchelle)/(float(Resol_Mut_reel)*1000.0),0,5,0);
	  end if;  
      new_line(ficparamviewm);
      int_io.put(ficparamviewm,maxp,0);
      new_line(ficparamviewm);
      Fix_io.put(ficparamviewm,EspaceInt,0,3,0);
      close(ficparamviewm);

      tabncrdir(Nbdir,3):=1;

      gb.move(dialog3.Label_Fin, 20, gb.int(top+40), 240, 15);
      gb.visible(dialog3.Label_Fin, gb.true);

      gb.visible(dialog3.CancelButton,gb.false);
	  gb.Move(dialog3.OKButton, 250, gb.int(top+35), 70, 20);
      gb.visible(dialog3.OKButton,gb.true);

	  return; -- Fin du module de calcul
    end if;  
    
    if option=placement then
      -- Lecture des fichiers binaires
      LECTURE_POSITIONS(tbnomi,tnp,tp,tnci,tici);
      -- Integration de nouvelles positions
	  if ficNouvPos(1)/=gb.nullchar then
        INSERE_NOUVELLES_POSITIONS(ficNouvPos,tbnomi,tnp,tp,InvEchelle,resolution, Minimum_terrain, hWnd);   
      end if;
      -- effacement des fichiers temporaires utiles a l'interface de saisie des positions
	  -- des ecritures sans position et des fichiers binaires temporaires
	end if;

    -- D�claration des tableaux utiles au calcul des interactions entre positions
    -- tnpi(o,p): nb de positions en interaction avec la position tp(o,p)
    -- tppi(o,p): indice dans lpi du 1er couple en interaction avec (o,p) 
    -- lpi(i): couple (o',p') nom-position en interaction
    declare
      tnpi  :typtnpi(1..tbnomi'last,1..maxp):=(others=>(others=>0));
      tppi  :typtppi(1..tbnomi'last,1..maxp):=(others=>(others=>0));
      lpi   :typlpi (1..tbnomi'last*maxp*maxnpi):=(others=>(0,0));
    begin
	
      gb.Visible(dialog3.Panel1, gb.false);
      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape7, top); -- Determination des positions en interaction
  
      gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
      gb.Move(dialog3.Panel2,0,0,0,20);
	  gb.Visible(dialog3.Panel1, gb.true);
      gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
      gb.height(dialog3.form, gb.int(top+115));

	  DET_PI(tbnomi,tnci,tici,tnp,tp,tnpi,tppi,lpi);
	  gb.Visible(dialog3.Panel1, gb.False);

      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape8, top);  -- D�termination du meilleur placement
      PLACEMENT_SEQUENTIEL(Tbnomi,Tnp,tp,Tnpi,tppi,Lpi,Tnci,Tici);

	  NbEcrPos:=0;
	  for i in 1..tbnomi'last loop
	    if TNP(i)/=0 then
		  NbEcrPos:=NbEcrPos+1;
        end if;
      end loop;

      -- if NbEcrPos>200 then
      if NbEcrPos>20 then
        CHARGE_PARAM_RECUIT;
        RECUI(tbnomi,tnpi,tppi,Lpi,tnp,tp);
      end if;

  	  -- closeFicLpiTxt;
  	  -- integ_io.Delete(FicLpi);

      gb.Visible(dialog3.Panel1, gb.false);

      -- debut orientation des cotes de courbes quand les courbes sont non orient�es
      if gb.checked(dialog1.CotesCourbesCheckBox)=gb.true and AMONT_A_DROITE=false then
        GR_REGION(grAlti,100*Resolution,100*Resolution);
        gb.Visible(dialog3.Panel1, gb.false);
        top:=top+20;
        AFFICHAGE_DEROULEMENT(dialog3.Etape8B, top); -- orientation des cotes de courbes

        gb.Move(dialog3.Panel1, 20, gb.int(top+20), 300, 20);
        gb.Move(dialog3.Panel2,0,0,0,20);
	    gb.Visible(dialog3.Panel1, gb.true);
        gb.Move(dialog3.CancelButton, 250, gb.int(top+60), 70, 20);
        gb.height(dialog3.form, gb.int(top+115));

        for no in 1..tbnomi'last loop
 	  	
          if 300*(no-1)/tbnomi'last /= 300*No/tbnomi'last or no=tbnomi'last then		
            gb.Move(dialog3.Panel2,0,0,gb.INT(300*No/tbnomi'last),20);
          end if;

          if tbnomi(no).p_choisie/=0 and Tbnomi(no).Topo.Mode_Placement=Courbe then
            declare
	          PasMNT : integer:=integer(PasMNTmm*float(160)); -- en unite plage, a mettre en fontion de la resolution et/ou de la densite de l'info alti
              DimMNT : integer:=0;
	  	      noeud : integer;
		      distance_carre : long_float;
			  G : point_type:=(0,0);  -- centre de gravit� du MNT
			  angleG : float:=0.0;
			  SommeAlti : float:=0.0;
			  AngleDiff : float:=0.0;
			  centre : point_type:=(TP(no,tbnomi(no).p_choisie).Coin1(1).coor_x
			                        +integer(0.5*float(tbnomi(no).ln(1))*abs(cos(TP(no,tbnomi(no).p_choisie).angle(1)))+0.5*float(tbnomi(no).corps)*abs(Sin(TP(no,tbnomi(no).p_choisie).angle(1))))
			  					   ,TP(no,tbnomi(no).p_choisie).Coin1(1).coor_Y
								    +integer(0.5*float(tbnomi(no).ln(1))*abs(sin(TP(no,tbnomi(no).p_choisie).angle(1)))+0.5*float(tbnomi(no).corps)*abs(cos(TP(no,tbnomi(no).p_choisie).angle(1)))));
			  NomCote : string(1..NBRE_CAR_NOM):=(others=>' ');
			  cote : angle_type;
			  perimetre, surface : float;
            begin
              for J in 1..tbnomi(no).ncar(1) loop
                if (tbnomi(no).Nb_Mots/=1) or (not((tbnomi(no).chaine(1)(J) in '0'..'9') or (tbnomi(no).chaine(1)(J)=',') or (tbnomi(no).chaine(1)(J)='.'))) then
                  exit;
                end if;
                if j=tbnomi(no).ncar(1) then
			 	
		          if NbCotes=0 then
                    exit;			  	
                  end if;

                  for k in 1..tbnomi(no).ncar(1) loop
			        if tbnomi(no).chaine(1)(k)/=',' then
 			  	      NomCote(k):=tbnomi(no).chaine(1)(k);
                    else
                      NomCote(k):='.';
                    end if;
                  end loop;
			      cote:=angle_type'value(NomCote(1..tbnomi(no).ncar(1)));
                  Gr_Creer_Noeud_Sur_Rien(grAlti,TP(no,tbnomi(no).p_choisie).Coin1(1),true,cote,0,Noeud);

                  sommital:=true;
                  while G=(0,0) and DimMNT<=0 loop  -- on abandonne si le MNT est toujours plat
                    DimMNT:=DimMNT+1;
			        declare
                      MNT : typMNT(-DimMNT..DimMNT,-DimMNT..DimMNT):=(others=>(others=>0.0));
                    begin
                      for X in -DimMNT..DimMNT loop
                        for Y in -DimMNT..DimMNT loop
			              if (abs(X)=DimMNT or abs(Y)=DimMNT) then
                            gr_noeud_proche_long(GrAlti,(Centre.coor_x+X*PasMNT,centre.coor_y+Y*pasMNT),Noeud,Distance_carre); -- (***)
				            MNT(X,Y):=Gr_noeud(GrAlti,noeud).orientation;
						    if (integer(MNT(X,Y)) mod 10)/=0 then
                              gb.MsgBox(integer'image(integer(MNT(X,Y))));
						    end if;
					        if MNT(X,Y)>cote then
					          sommital:=false;	
				            end if;
				            G.coor_x:=G.coor_x+integer(X*MNT(X,Y));
				            G.coor_Y:=G.coor_Y+integer(Y*MNT(X,Y));
                          end if;
                        end loop;
                      end loop;
                    end;
	              end loop;
                  if G.coor_x/=0 then
                    angleG:=Arctan(float(G.coor_Y)/float(G.coor_X));
	                if G.coor_x<0 then
                      if G.coor_Y>0 then
				        angleG:=angleG+pi;
                      else
				        angleG:=angleG-pi;
                      end if;
                    end if;
                  else
	                if G.coor_Y>0 then
                      angleG:=pi/2.0;
                    else
                      angleG:=-pi/2.0;
			        end if;
	                if G.coor_Y=0 then
				      tbnomi(no).cause:=MNT_Plat;
				      tbnomi(no).p_choisie:=0;
				      exit;
				      -- MNT plat, configuration a forte probabilite d'erreur sur le retournement
			        end if;
                  end if;

                  AngleDiff:=angleG-TP(no,tbnomi(no).p_choisie).angle(1);
                  if AngleDiff>Pi then AngleDiff:=AngleDiff-2.0*pi; end if;
                  if AngleDiff<-Pi then AngleDiff:=AngleDiff+2.0*pi; end if;

                  if abs(AngleDiff)<0.25*Pi or abs(AngleDiff)>0.75*Pi then  -- intervalle de forte probabilite d'erreur sur le retournement
			   	                                                         -- et a forte probabilite de position non satisfaisante
				    tbnomi(no).cause:=Direction_G;
				    tbnomi(no).p_choisie:=0;
				    exit;
			      end if;

                  if sommital=true then
	                if T_MT(No+NbNoRou-Tbnomi'last).T_DEB=T_MT(No+NbNoRou-Tbnomi'last).T_FIN and T_TRS(T_MT(No+NbNoRou-Tbnomi'last).T_DEB).A_DEB=T_TRS(T_MT(No+NbNoRou-Tbnomi'last).T_DEB).A_FIN then
				      gr_points_d_arc(grr,abs(T_ARCS(T_TRS(T_MT(No+NbNoRou-Tbnomi'last).T_DEB).A_DEB).N_ARC),Tab_arc,cpt);
                      if Tab_Arc(1)=Tab_Arc(Cpt) then
                        perimetre:=taille_ligne(TAB_arc,Cpt);
                        if perimetre<30.0*float(resolution) then
				          surface:=surface_arc(Tab_arc,Cpt);
					      if (perimetre*perimetre)/(4.0*pi*surface)>1.6 then   -- (1.0 pour un cercle)
                          -- meta-troncon forme d'un seul troncon lui-meme forme d'un seul arc ferme, court, applati et sommital
					      -- configuration a forte probabilite d'erreur sur le retournement
					        tbnomi(no).cause:=Sommet_Plat;
				            tbnomi(no).p_choisie:=0;
						    exit;
                          end if;
			            end if;
			          end if;
			        end if;
		          end if;

			      if angleDiff<0.0 then				  	
                    quatre_coins(TP(no,tbnomi(no).p_choisie).Coin1(1),TP(no,tbnomi(no).p_choisie).angle(1),tbnomi(no).ln(1),tbnomi(no).corps,bloc);
                    TP(no,tbnomi(no).p_choisie).Coin1(1):=bloc.p3;
				    if TP(no,tbnomi(no).p_choisie).angle(1)<0.0 then
				      TP(no,tbnomi(no).p_choisie).angle(1):=TP(no,tbnomi(no).p_choisie).angle(1)+Pi;
                    else
				      TP(no,tbnomi(no).p_choisie).angle(1):=TP(no,tbnomi(no).p_choisie).angle(1)-Pi;		  	
                    end if;
                  end if;

	            end if;
              end loop;    
	        end;
          end if;
        end loop;

        gb.Visible(dialog3.Panel1, gb.False);

      end if;
      -- fin orientation des cotes de courbes quand les courbes sont non orient�es

	  --- Pr�paration des fichiers de sortie
      top:=top+20;
      AFFICHAGE_DEROULEMENT(dialog3.Etape9, top);  -- pr�paration des fichiers de sortie
        nomficviews(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
        nomficviews(ncar_fo-3..ncar_fo+2):="_views";
        create(ficviews,Out_File,nomficviews,"");
        put_line(ficviews,"FORMAT VECTEUR VIEWER 1.0");
        put_line(ficviews,"COORDONNEES TERRAIN");

        nomficecris(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
        nomficecris(ncar_fo-3..ncar_fo+2):="_ecris";
        create(ficecris,Out_File,nomficecris,"");
        put_line(ficecris,"FORMAT VECTEUR VIEWER 1.0");
        put_line(ficecris,"COORDONNEES TERRAIN");

        nomfichier_blatex(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
        nomfichier_blatex(ncar_fo-3..ncar_fo+14):="_blasons_texte.txt";
        create(fblatex,out_file,nomfichier_blatex(1..ncar_fo+14));

        nomfichier_blaima(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
        nomfichier_blaima(ncar_fo-3..ncar_fo+14):="_blasons_image.txt";
        create(fblaima,out_file,nomfichier_blaima(1..ncar_fo+14));

        Sauve_ecritures_placees_recti(tbnomi,tnp,tp,InvEchelle,resolution, Minimum_terrain,ficviews,miecp,Ficecris);
        Sauve_ecritures_placees_Dispo(tbnomi,tnp,tp,InvEchelle,resolution, Minimum_terrain,ficviews,miecp,Ficecris);
        SAUVE_ECRITURES_BLOQUEES(tbnomi,tnp,TP,InvEchelle, resolution, Minimum_terrain,ficviews,compesp,compeb,compmtsp,compmtb,Ficecris);
        SAUVE_BOITES_PLACEES(tbnomi,tnp,tp,InvEchelle,resolution,Minimum_terrain,ficviews,fblatex,fblaima);
        SAUVE_BOITES_BLOQUEES(tbnomi,Tnp,InvEchelle,resolution,Minimum_terrain,ficviews,fblatex,fblaima);

		-- ecriture du contour de l'image pour la sortie viewer sans image
        if mutil then
          for code in 7..10 loop
	        put_line(ficecris,integer'image(code));
            put_line(ficecris,"LIGNE");
            couleur:=0; -- noir
            styligne:=0;
            Epaisseur:=3;
            put_line(ficecris,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
            put_line(ficecris,"5");
            put(ficecris,Tso.coor_x,0,2,0);
            put(ficecris,tab);
            put(ficecris,tso.coor_Y,0,2,0);
            new_line(ficecris);
            put(ficecris,tso.coor_x,0,2,0);
            put(ficecris,tab);
            put(ficecris,tne.coor_Y,0,2,0);
            new_line(ficecris);
            put(ficecris,tne.coor_x,0,2,0);
            put(ficecris,tab);
            put(ficecris,tne.coor_Y,0,2,0);
            new_line(ficecris);
            put(ficecris,tne.coor_x,0,2,0);
            put(ficecris,tab);
            put(ficecris,tso.coor_Y,0,2,0);
            new_line(ficecris);
            put(ficecris,tso.coor_x,0,2,0);
            put(ficecris,tab);
            put(ficecris,tso.coor_Y,0,2,0);
            new_line(ficecris);
          end loop;
        end if;

        close(ficviews);
		close(Ficecris);
        close(fblatex);
        close(fblaima);
 
		if blasons=false then 
          Delete_File(nomfichier_blatex(1..ncar_fo+14));
          Delete_File(nomfichier_blaima(1..ncar_fo+14));
    	end if;

      -- ecriture du fichier de parametres pour VisuSorie
      create(ficparamviews,Out_File,nomficparamviews,"");
      put(ficparamviews,nomficviews(1..ncar_fo+2));
      new_line(ficparamviews);
      if mutil then
	    put(ficparamviews,nomficecris(1..ncar_fo+2));
        new_line(ficparamviews);
      end if;
      put(ficparamviews,boolean'image(miecp));
      new_line(ficparamviews);
      put(ficparamviews,boolean'image((compesp+compeb+compmtsp+compmtb)/=0));
      close(ficparamviews);

      -- pas de degrisage de VisuSortie si le module de placement a �t� lanc� directement
      if option/=placement or Pat_calcul_SM_mort=true then
        Gb.Enabled(Window1.VisuSortie_SM,1);
      end if;
      gb.move(dialog3.Label_Fin, 20, gb.int(top+40), 240, 15);
      gb.visible(dialog3.Label_Fin, gb.true);

      if Nb_Ecritures>0 then
	    str1:=to_unbounded_string(" Toutes les �critures horizontales ont pu �tre plac�es")&eol;
      end if;
	  if NbNoRou>0 then
        str4:=eol&to_unbounded_string(" Toutes les �critures � disposition ont pu �tre plac�es");
      end if;

      if compesp+Compeb>0 then
	  	if compesp+Compeb=1 then 
          str1:=to_unbounded_string(" Une seule �criture horizontale n'a pas pu �tre plac�e, soit"&integer'image(integer((100.0*float(compesp+compeb))/float(Nb_Ecritures)))&"%");
          if compesp=1 then
            str2:=to_unbounded_string(eol&" Elle �tait sans position possible");
          else
		  	str2:=to_unbounded_string(eol&" Elle a �t� bloqu�e au cours du placement");
          end if;
		  Str3:=Null_unbounded_string&eol;
        else
	      str1:=to_unbounded_string(integer'image(Compesp+compeb)&" �critures horizontales n'ont pas pu �tre plac�es, soit"&integer'image(integer((100.0*float(compesp+compeb))/float(Nb_Ecritures)))&"%");
          case compesp is
            when 0 => str2:=to_unbounded_string(eol&" Elles ont toutes �t� �cart�es au cours du placement");
			          Str3:=eol&Null_unbounded_string;
            when 1 => str2:=to_unbounded_string(eol&" 1 �tait sans position possible");
                      if compeb=1 then
                        str3:=to_unbounded_string(eol&" 1 a �t� �cart�e au cours du placement")&eol;
                      else
                        str3:=to_unbounded_string(eol&integer'image(compeb)&" ont �t� �cart�es au cours du placement")&eol;
                      end if;
  	        when others => if compeb/=0 then
                        str2:=to_unbounded_string(eol&integer'image(compesp)&" �taient sans position possible");
                        if compeb=1 then
                          str3:=to_unbounded_string(eol&" 1 a �t� �cart�e au cours du placement")&eol;
                        else
                          str3:=to_unbounded_string(eol&integer'image(compeb)&" ont �t� �cart�es au cours du placement")&eol;
                        end if;
                      else
				  	    str2:=to_unbounded_string(eol&" Aucune d'entre elles n'avait de position possible");
						str3:=Null_unbounded_string&eol;
                      end if;
          end case;
        end if;
      end if;


      if compmtsp+Compmtb>0 then
	  	if compmtsp+Compmtb=1 then 
          str4:=eol&to_unbounded_string(" Une seule occurence d'�criture � disposition n'a pas pu �tre plac�e, soit"&integer'image(integer((100.0*float(compmtsp+compmtb))/float(NbNoRou)))&"%");
          if compmtsp=1 then
            str5:=to_unbounded_string(eol&" Elle �tait sans position possible");
          else
		  	str5:=to_unbounded_string(eol&" Elle a �t� �cart�e au cours du placement");
          end if;
		  Str6:=Null_unbounded_string;
    else
	      str4:=eol&to_unbounded_string(integer'image(Compmtsp+compmtb)&" occurences d'�critures � disposition n'ont pas pu �tre plac�es, soit"
		                           &integer'image(integer((100.0*float(compmtsp+compmtb))/float(NbNoRou)))&"%");
          case compmtsp is
            when 0 => str5:=to_unbounded_string(eol&" Elles ont toutes �t� �cart�es au cours du placement");
			          Str6:=Null_unbounded_string;
            when 1 => str5:=to_unbounded_string(eol&" 1 �tait sans position possible");
                      if compmtb=1 then
                        str6:=to_unbounded_string(eol&" 1 a �t� �cart�e au cours du placement");
                      else
                        str6:=to_unbounded_string(eol&integer'image(compmtb)&" ont �t� �cart�es au cours du placement");
                      end if;
  	        when others => if compmtb/=0 then
                        str5:=to_unbounded_string(eol&integer'image(compmtsp)&" �taient sans position possible");
                        if compmtb=1 then
                          str6:=to_unbounded_string(eol&" 1 a �t� �cart�e au cours du placement");
                        else
                          str6:=to_unbounded_string(eol&integer'image(compmtb)&" ont �t� �cart�es au cours du placement");
                        end if;
                      else
				  	    str5:=to_unbounded_string(eol&" Aucune d'entre elles n'avait de position possible");
						str6:=Null_unbounded_string;
                      end if;
          end case;
        end if;
      end if;

      if (compesp+Compeb>0) or (compmtsp+Compmtb>0) or (NBRE_ROUTES>0 and CompRouSansOcc>0) then
	    Gb.MsgBox(to_string(str1&str2&str3&Str4&str5&str6&str7)," D�compte des �critures non plac�es");
      else
	    Gb.MsgBox("Toutes les �critures ont pu �tre plac�es"," D�compte des �critures non plac�es");
      end if;

      if TnpiMax>Maxnpi then
        gb.MsgBox("Le nombre maximum d'interactions entre positions a �t� de"&integer'image(TnpiMax)&". Cette valeur est"&eol&
		          "sup�rieure au nombre maximum d'interactions prises en compte, qui est de"&integer'image(Maxnpi)&"."&eol&
		          "Il est donc possible que certaines �critures plac�es se chevauchent.", " Alerte", Win32.WinUser.MB_ICONEXCLAMATION);
      end if;

	  if coord=centre then
	  	Str1:=to_unbounded_string("soient celles des CENTRES des �critures");
      else
	  	Str1:=to_unbounded_string("soient celles des COINS INFERIEURS GAUCHE des �critures");
      end if;
	  gb.MsgBox("Vous avez choisi que les coordonn�es"&eol&"qui figurent dans le fichier des �critures plac�es"&eol&to_string(str1)," Rappel",MB_ICONEXCLAMATION);

      gb.visible(dialog3.CancelButton,gb.false);
	  gb.Move(dialog3.OKButton, 250, gb.int(top+35), 70, 20);
      gb.visible(dialog3.OKButton,gb.true);

      if GrosExiste then
        Gr_Delete(gros);
		GrosExiste:=false;
      end if;
      if GrrExiste then 
        Gr_Delete(grr);
		GrrExiste:=false;
      end if;
      if GrAltiExiste then 
        Gr_Delete(grAlti);
        GrAltiExiste:=false;
      end if;
    end;
  end;

  exception when  Event : others =>
  -- Affichage du message d'erreur
  gb.MsgBox(To_String(Msg_Erreur), "Erreur", gb.UINT(MB_OK + MB_ICONSTOP), hWnd);
  Dialog3.Close_Form;
end PAT_CALCUL;
 
----------------------------------------------------------------------------
  
end PPAT_CALCUL;
