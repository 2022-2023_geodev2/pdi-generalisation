--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--****************** M�thode de recuit simul� *****************************
--*************************************************************************

with inipan; use inipan;
with detpos; use detpos;
with text_io;use text_io;

Package recuit is
  
   package int_io is new text_io.integer_io(integer); use int_io; 

--=====================================================================
-- Parametres de recuit initialises dans le fichier des param de recuit
--===================================================================== 
init_niter : integer;
delta_niter: integer;
init_seuil : float;
fac_seuil  : integer;
mini_seuil : float;
OA_FACTOR  : float;

OAE : integer:=0; -- nb total de blocs over et ambigus pour les blocs actifs
UE : float:=0.0; -- somme des poids des blocs actifs dans la situation courante 
E : float:=0.0; -- cout total

Type TRANSITION_TYPE is record
	num_nom : integer;
	num_ancienne_position : integer;
	num_nouvelle_position : integer;
	delta_nbpos_inter : integer;	
	delta_poids : float;
end record;

--========================================================================
Procedure Charge_param_recuit;

-- =======================================================================
-- ------------ Procedure de recuit simule -------------------------------
-- =======================================================================
Procedure RECUI(tbnomi:in out typtbnomi;
		tnpi  :typtnpi;
		tppi  :typtppi;
		lpi   :typlpi;
		tnp   :typtnp;
		tp    :typtp );

--========================================================================

End recuit;

