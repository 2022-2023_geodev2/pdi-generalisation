with gen_io; use gen_io;
--- with X_LIB;
with text_io;use text_io;
with inipan; use inipan;
with win32.windef;

Package PAB is

package int_io is new text_io.integer_io(integer); use int_io; 
package flo_io is new text_io.float_io(float); use flo_io;
   
maxcar: constant := 50;

------------------------------------------------------------------------------
--------------------------- DECLARATION DES TYPES 
------------------------------------------------------------------------------
Type ANbplaces is array(1..2) of natural range 0..maxblatopo; 


procedure DETXPOS (x : in out integer; xdep : in integer; nbplaces : in ANbplaces;
		  VAPlace : in out Aplace; VANom_type : in typtbnomi;
		  indnom:in positive; sens : in integer; lin : in positive;
		  hesp : in natural; nbl : in positive);

procedure REPARTITION (VANom_type: in out typtbnomi;
                       nbplaces: in Anbplaces;
                       hesp: in natural;
                       indnom: in positive;
                       VAPlace : Aplace;
                       nbl : in natural);
                       
procedure MOVETXT (VANom_type: in out typtbnomi;
                       nbplaces: in Anbplaces;
                       indnom: in positive;
	                     vserie:in aserie;
                       VAPlace : Aplace;
                       nbl : in natural);

procedure empxyblastxt(tbnomi : in out typtbnomi; i : in integer; noblas : in integer; police : in positive; info_font: in typtfont; hdc: Win32.windef.hdc; nbl : in natural);
          				
procedure PABMAIN(VANom_type : in out typtbnomi; InvEchelle : in integer; Resolution : in positive;
  vesp : in out integer; hdc : Win32.windef.hdc);

end PAB;
