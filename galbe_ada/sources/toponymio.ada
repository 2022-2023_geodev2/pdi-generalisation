--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

with lecture;
With geometrie; use geometrie;
with Ada.Characters.Handling; use Ada.Characters.Handling;
with gb;
with math_int_basic; use math_int_basic;
with ada.numerics.elementary_functions; Use ada.numerics.elementary_functions;
with norou; use norou;
with Win32.WinUser;
with manipboite; use manipboite;
with ada.strings.unbounded; use ada.strings.unbounded;
with morpholigne; use morpholigne;
-- with polaire_fl; use polaire_fl;
with Lissage_filtrage; use Lissage_filtrage;


package body toponymio is
      
--===============================================================
-- Creation d'un fichier de sauvegarde des positions possibles --
-- En fin du module de calcul --
--===============================================================
procedure sauvegarde(tbnomi:in typtbnomi;
                      tnp   :in typtnp;
                      tp    :in typtp;
                      tnci  :in typtnci;
                      tici  :in typtici;
                      InvEchelle      : positive;
                      Resolution      : positive;
                      Minimum_terrain : Point_type_reel;
  					  Sp : in out boolean) is

 ficnom     :nom_io.file_type;
 ficposition:position_io.file_type;
 ficsansposition : text_io.file_type ;
 ficninter,ficnposition,ficinter : integ_io.file_type;
 nomfichier : string120;
 coord_objet :point_type_reel;
 ncar : integer;
 J : integer;
 ficnecritures : Text_io.File_type;
 I,L : integer;

begin	
    nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);

    ncar:=ncar_fo+4;
    nomfichier(ncar_fo-3..ncar):="_Nom.tmp";
    -- creation du fichier des noms:
    create(ficnom,out_file,nomfichier(1..ncar));
    for no in 1..tbnomi'last loop
      write(ficnom,tbnomi(no));
    end loop;
    close(ficnom);

    ncar:=ncar_fo+8;
    nomfichier(ncar_fo-3..ncar):="_SansPos.txt";
    -- creation du fichier texte des noms sans position
    create(ficsansposition,out_file,nomfichier(1..ncar));

    sp:=false;
    for no in 1..tbnomi'last loop
		
      -- debut gestion du caract�re '%' pour kilometrages
      l:=tbnomi(no).ncar(1);
      for K in 2..tbnomi(no).ncar(1)-1 loop
        if tbnomi(no).chaine(1)(K)='%' then
		  l:=k-1;
		  exit;
        end if;
      end loop;
	  -- fin gestion du caract�re '%' pour kilometrages

      if tnp(no)=0 and tbnomi(no).coord_objet.coor_x<=Pne.coor_x and tbnomi(no).coord_objet.coor_X>=Pso.coor_x
			       and tbnomi(no).coord_objet.coor_Y<=Pne.coor_y and tbnomi(no).coord_objet.coor_y>=Pso.coor_y
				   and tbnomi(no).topo.mode_placement/=CDesaxe and tbnomi(no).topo.mode_placement/=CAxe
				   -- tous les kilometrages sont concern�s, meme les courts
				   and (Tbnomi(no).cause/=MetaCourt or l/=tbnomi(no).ncar(1)) then
			sp:=true;
            put(ficsansposition,tbnomi(no).id,0);
            put(ficsansposition, tab);

	  	      -- ln:=ln+tbnomi(no).ln(i);
              -- put(ficsansposition,tbnomi(no).chaine(I)(1..tbnomi(no).ncar(i)));
            put(ficsansposition,tbnomi(no).chaine(1)(1..L));

            -- put(ficsansposition, tbnomi(no).chaine(1)(1..tbnomi(no).ncar(1)));
            put(ficsansposition, tab);
            coord_objet:=Lecture.Convert_coord_graphe_terrain(tbnomi(no).coord_objet,InvEchelle, resolution, Minimum_terrain);
            put(ficsansposition, coord_objet.coor_x,0,2,0);
            put(ficsansposition, ';');
            put(ficsansposition, coord_objet.coor_y,0,2,0);
            put(ficsansposition, tab);
            put(ficsansposition, integer((float(tbnomi(no).ln(1))/1000.0)*(float(invEchelle)/float(resolution))),0);
            put(ficsansposition, tab);
            put(ficsansposition, integer((float(tbnomi(no).corps)/1000.0)*(float(invEchelle)/float(resolution))),0);
            put(ficsansposition, tab);
            j:=tbnomi(no).topo.code'last;
            while tbnomi(no).topo.code(j)=' ' loop
              j:=j-1;
            end loop;
            put(ficsansposition,tbnomi(no).topo.code(1..j));
            put(ficsansposition, tab);
			if tbnomi(no).topo.objet_type=Ponctuel and tbnomi(no).topo.mode_placement=Interieur then
			  put(ficsansposition, "IMPRECIS");
			else
              put(ficsansposition, typcateg'image(tbnomi(no).topo.mode_placement));
		    end if;
            put(ficsansposition, tab);
			-- "orientabilit�"
            if tbnomi(no).topo.mode_placement=Desaxe or tbnomi(no).topo.mode_placement=CDesaxe
		       or tbnomi(no).topo.mode_placement=axe or tbnomi(no).topo.mode_placement=CAxe
			   or tbnomi(no).topo.mode_placement=Courbe then
              put(ficsansposition,"1");
            else
              put(ficsansposition,"0");
			end if;
            new_line(ficsansposition);
 		end if;    
    end loop;
    close(ficsansposition);

    ncar:=ncar_fo+10;
    nomfichier(ncar_fo-3..ncar):="_NPosition.tmp";
    -- creation du fichier des nombres de positions (tableau TNP)
    create(ficnposition,out_file,nomfichier(1..ncar));
    for no in 1..tbnomi'last loop 
        write(ficnposition,tnp(no));
    end loop;
    close(ficnposition);
    
    ncar:=ncar_fo+9;
    nomfichier(ncar_fo-3..ncar):="_Position.tmp";
    -- creation du fichier des positions (tableau TP)
	create(ficposition,out_file,nomfichier(1..ncar));
    for no in 1..tbnomi'last loop
      for ip in 1..max(tnp(no),2) loop
        write(ficposition,tp(no,ip));
      end loop;
      -- pour sauver la 2e g�om�trie qd il n'y a qu'une position possible: 
      if tnp(no)=1 then
        write(ficposition,tp(no,2));
      end if;
    end loop;
    close(ficposition);

    ncar:=ncar_fo+7;
    nomfichier(ncar_fo-3..ncar):="_NInter.tmp";
    -- creation du fichier des nombres d'interactions (tableau TNCI)
    create(ficninter,out_file,nomfichier(1..ncar));
    for no in 1..tbnomi'last loop 
      write(ficninter,tnci(no));
    end loop;
    close(ficninter); 
     
    ncar:=ncar_fo+6;
    nomfichier(ncar_fo-3..ncar):="_Inter.tmp";
    -- creation du fichier des interactions (tableau TICI)
    create(ficinter,out_file,nomfichier(1..ncar));
    for no in 1..tbnomi'last loop 
        for ic in 1..tnci(no) loop
          write(ficinter,tici(no,ic));
        end loop;   
    end loop;
    close(ficinter);

    ncar:=ncar_fo+11;
    nomfichier(ncar_fo-3..ncar):="_NEcritures.tmp";
    -- creation du fichier du nombre d'�critures
    create(ficnecritures,out_file,nomfichier(1..ncar));
    Int_io.put(ficnecritures,tbnomi'last,0);
    close(ficnecritures);  

End sauvegarde;

--============================================================
-- Lecture du fichier de sauvegarde des positions possibles --
--============================================================
procedure Lecture_positions (tbnomi	: in out typtbnomi;
                             tnp   	: in out typtnp;
                             tp    	: in out typtp;
                             tnci  	: in out typtnci;
                             tici  	: in out typtici) is
                               
 ficnom     :nom_io.file_type;
 ficposition:position_io.file_type;
 ficninter,ficnposition,ficinter:integ_io.file_type;
 npos,nci,no,ni,j:integer:=0;
 position:position_type;
 tpos:array(1..maxp*tbnomi'last) of position_type;
 tnp_temp:typtnp(1..tbnomi'last);
 nomfichier : string120;
 Le_NGeom : integer;
 
begin
   nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
    
   nomfichier(ncar_fo-3..ncar_fo+4):="_Nom.tmp";
   -- chargement des noms:
   open(ficnom,in_file,nomfichier(1..ncar_fo+4));
   for no in 1..tbnomi'last loop
     read(ficnom,tbnomi(no));
   end loop;
   close(ficnom);
   
   -- chargement des positions:
   nomfichier(ncar_fo-3..ncar_fo+10):="_NPosition.tmp";
   open(ficnposition,in_file,nomfichier(1..ncar_fo+10));
   nomfichier(ncar_fo-3..ncar_fo+9):="_Position.tmp";
   open(ficposition,in_file,nomfichier(1..ncar_fo+9));
   for no in 1..tbnomi'last loop
     read(ficnposition,npos);
     for ip in 1..max(npos,2) loop
       read(ficposition,position);
       tp(no,ip):=position;
     end loop;
     -- pour lire la 2e g�om�trie qd il n'y a qu'une position possible: 
     if Npos=1 then
       Read(ficposition,position);
       tp(no,2):=position;
     end if;
	 -- Le_NGeom:=tp(no,1).NGeom(1);
	 -- Le_NGeom:=tp(no,2).NGeom(1);
     tnp(no):=npos;
   end loop;
   close(ficnposition);
   close(ficposition);

   -- chargement des interactions:
   nomfichier(ncar_fo-3..ncar_fo+7):="_NInter.tmp";
   open(ficninter,in_file,nomfichier(1..ncar_fo+7));
   nomfichier(ncar_fo-3..ncar_fo+6):="_Inter.tmp";
   open(ficinter,in_file,nomfichier(1..ncar_fo+6));
   for no in 1..tbnomi'last loop
     read(ficninter,nci);
     if nci/=0 then
       for ic in 1..nci loop
         read(ficinter,tici(no,ic));
       end loop;
     end if;
     tnci(no):=nci;
   end loop;
   close(ficninter);  
   close(ficinter);

End Lecture_positions; 

--===============================================================
--Insertion des nouvelles positions --
--===============================================================
procedure Insere_Nouvelles_Positions (ficNouvPos: in string;
                                      tbnomi: in out typtbnomi;
	           	     		          tnp: in out typtnp;
  		   	     			          tp: in out typtp;
                                      InvEchelle: positive;
                                      Resolution: positive;
                                      Minimum_terrain: Point_type_reel;
                                      hWnd: in Win32.winnt.Handle) is 

fic : text_io.file_type ;
ncar : integer;
x,y : integer;
nom	:string(1..maxcar);
car	:character;
icar : integer;
poip: float;
nbpos : integer;
id : integer;
nnom : integer;
nc : integer;
Preel :Point_Type_Reel;
MaxLLigne : integer:=32000;	-- Longueur maximale des lignes du fichier texte
Ligne : string (1..MaxLLigne):=(1..MaxLLigne => ' ');	-- Ligne du fichier texte
LLigne : integer;		-- Longueur de la ligne
no_lig: text_io.count;
last: positive;
Ptr_indice : Liens_access_type;		-- Pointeur des indices des separateurs
idInconnu : boolean:=false;
boite_qC : bloc_type;
bloc : bloc_type;

begin
  begin
    open(fic,in_file, ficNouvPos);
  exception when text_io.NAME_ERROR => return ;
  end;
  
  loop
  	 << debut >>
     if end_of_file(fic) then exit; end if;
     begin
        no_lig:=line(fic);
        Ligne:=(1..MaxLLigne => ' ');
        get_line(fic,Ligne,LLigne);
     exception when text_io.END_ERROR =>
        close(fic);
        open(fic,in_file,ficNouvPos);
        set_line(fic, no_lig);
        LLigne:=0;
        while (not End_of_file(fic)) and ( LLigne<MaxLLigne ) loop
          LLigne:=LLigne+1;
          get(fic, Ligne(LLigne));
        end loop;
     end;
     
     Indice_Separateur(Ligne,LLigne,Tab,Ptr_Indice);
     
-- 1 - lecture de l'id
     int_io.get(Ligne,id, last);

-- 2 - retrouver le toponyme correspondant dans tbnomi
     for n in 1..tbnomi'last loop
	 	-- gb.Msgbox(integer'image(tbnomi(n).id));
        if tbnomi(n).id = id then 
          nnom:=n;
          exit;
        end if;
		if n=tbnomi'last then
          idInconnu:=true;
		  goto debut;
	    end if;
     end loop;

     if Ptr_indice(3)>Ptr_indice(2)+1 then -- 2 lignes possibles
       tbnomi(nnom).ncar1:=Ptr_indice(2)-Ptr_indice(1)-1;
       tbnomi(nnom).chaine1:=(1..100 => ' ');
       tbnomi(nnom).chaine1(1..tbnomi(nnom).ncar1):=Ligne(Ptr_indice(1)+1..Ptr_indice(2)-1);
       tbnomi(nnom).ncar2:=Ptr_indice(3)-Ptr_indice(2)-1;
       tbnomi(nnom).chaine2:=(1..100 => ' ');
       tbnomi(nnom).chaine2(1..tbnomi(nnom).ncar2):=Ligne(Ptr_indice(2)+1..Ptr_indice(3)-1);
       det_long(tbnomi, nnom, hWnd);
     end if;

     
--          quatre_coins(T_POSITIONS(I,j).coin1,T_POSITIONS(I,j).angle,T_MT(i).L_topo,T_MT(i).corps,boite_qC);
--          Tp(Tbnomi'last-NbNoRou+i,J).coin1:=(min(Min(boite_qC.P1.coor_x,boite_qC.P2.coor_x),min(boite_qC.P3.coor_x,boite_qC.P4.coor_x)),
--		                                      min(min(boite_qC.P1.coor_y,boite_qC.P2.coor_y),min(boite_qC.P3.coor_y,boite_qC.P4.coor_y)));


-- 3 - lecture du nb de positions
     int_io.get(Ligne(Ptr_indice(3)+1..LLigne),tnp(nnom), last);       
-- 4 - Saisissons les coordonnees et on les convertit en coord. carto:
     for pos in 1..tnp(nnom) loop
		-- flo_io.get(Ligne(Ptr_indice(3+pos)+1..LLigne),Preel.coor_x,last);
		flo_io.get(Ligne(Ptr_indice(2*(pos+1))+1..LLigne),Preel.coor_x,last);
        flo_io.get(Ligne(last+2..LLigne),Preel.coor_y, last);
        tp(nnom,pos).coin1(1):=lecture.Convert_coord_terrain_graphe(Preel,InvEchelle,Resolution,Minimum_terrain);
        tp(nnom,pos).nl:=1;
        if last<LLigne then
          if Ligne(last+1)=';' then -- 2 lignes
            flo_io.get(Ligne(last+2..LLigne),Preel.coor_x, last);
            flo_io.get(Ligne(last+2..LLigne),Preel.coor_y, last);
            tp(nnom,pos).coin2:=lecture.Convert_coord_terrain_graphe(Preel,InvEchelle,Resolution,Minimum_terrain);
            tp(nnom,pos).nl:=2;
          end if;
        end if;
		flo_io.get(Ligne(Ptr_indice(3+2*pos)+1..LLigne),tp(nnom,pos).Angle(1),last);
        poip:=10.0*(1.0-float(pos)/float(tnp(nnom)+1));
        tp(nnom,pos).poip:=poip;
        tp(nnom,pos).poss:=true;
     end loop; -- pos
     Gr_free_liens_array(Ptr_indice);
  end loop;
  close(fic);
  if idInconnu=true then
    gb.MSgBox("Certaines positions concernent des �critures d'identifiant inconnu"," Fichier des nouvelles positions",Win32.WinUser.MB_ICONEXCLAMATION);
  end if;
end Insere_Nouvelles_Positions;



--=========================================================
-- Fichier de sortie au format texte des ecritures placees
-- a support rectiligne + sortie viewer avec texte des blasons texte
--=========================================================
procedure sauve_ecritures_placees_Recti(tbnomi : in typtbnomi;
                                      tnp : in typtnp;  --nb de positions
                                      tp : in typtp;   -- tableau des positions
                                      InvEchelle : positive;
                                      Resolution : positive;
                                      Minimum_terrain : Point_type_reel;
                                      f2 : in out text_io.file_type;
                                      miecp : in out boolean;
									  f3 : in out text_io.file_type) is

f : text_io.file_type;
coin1_terrain, coin2_terrain : Point_type_reel:=(0.0,0.0);
nomfichier : string120;
centrage : point_type_reel;
centrage_oriente : point_type_reel;
code : integer:=7;
couleur : integer:=0; -- noir
symboleviewer : integer:=7; -- pas de symbole
gis : integer range 0..14:=0;
policename : string(1..32);
jo,j : integer;
info_font: typtfont(1..500);
nbfont : integer;
c : character;
M1g,M2g,M3g : Point_type;
M1t,M2t,M3t : Point_type_reel;
Delta_Terrain : float;
Resol_Mut_reel : float;
infocodesp : string30:=(1..30=>' ');
infocode : string30:=(1..30=>' ');
infocode_nbcar : integer;
police : positive; -- indice de police d'un blason texte
coin_blas_terrain,coin_opp_blas_terrain : point_type_reel; -- emprise terrain d'un blason
nl : integer; -- ecriture sur nl=1 ou nl=2 ligne(s) pour les ecritures horizontales
              -- dessus nl=1 ou dessous nl=2 pour les numeros de routes
cs,sn :float;
Delta_Ln_Terrain,Delta_corps_Terrain : float; -- decalages a introduire pour dessiner une boite orientee 
GeomT : Point_Type_reel;
NumGeom : integer;
ISurMilR : float:=Float(InvEchelle)/(1000.0*Float(Resolution));
Dist,AbscurvIni,AbscurvFin,AbscurvTotale,Reste : float;
No_Pred : integer;
Justification : float:=0.0;
N : integer;
Inters : Point_Access_type;
nInters : integer;
PT,Orth : point_type;
PTy : Point_Liste_Type(1..2);
Pas : float; -- pas d'echantillonnage
L : integer;

begin
  nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomfichier(ncar_fo-3..ncar_fo+19):="_placees_rectiligne.txt";
  create(f,out_file,nomfichier(1..ncar_fo+19));

  charge_fontes(info_font,nbfont);
  
  Delta_Terrain:=max(Vse.coor_x-Vno.coor_x,Vno.coor_y-Vse.coor_y);
  Resol_Mut_reel:=float(InvEchelle)*8.0/Delta_Terrain; -- 8 comme 8000*8000 : dimensions de l'image de fond

  for no in 1..tbnomi'last loop

    if tbnomi(no).p_choisie/=0 and tbnomi(no).topo.Mode_Placement/=CAxe and tbnomi(no).topo.Mode_Placement/=CDesaxe then

      -- debut gestion du caract�re '%' pour kilometrages
	  l:=tbnomi(no).ncar(1);
	  if tp(no,tbnomi(no).p_choisie).nl=1 then
	    for K in 2..tbnomi(no).ncar(1)-1 loop
          if tbnomi(no).chaine(1)(K)='%' then
		    l:=k-1;
		    exit;
          end if;
        end loop;
      end if;
      -- fin gestion du caract�re '%' pour kilometrages

	  if L=tbnomi(no).ncar(1) then
        put(f,tbnomi(no).id,0);
      else
        put(f,tbnomi(no).chaine(1)(L+2..tbnomi(no).ncar(1)));
      end if;
      put(f,sep);
      miecp:=true;
      put_line(f2,integer'image(code));
      put_line(f2,"POINT");
      put(f2,"1");
      put(f2,tab);
      put(f2,integer'image(symboleviewer));
      put(f2,tab);
      put(f2,integer'image(couleur));
      put(f2,tab);
      put(f2,integer'image(gis));
      put(f2,tab);
      M1g:=(0,0);
      M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
      if mutil then
	    put(F2,integer(float(tbnomi(no).corps*Resol_mut)*10.0/float(Resolution)));
      else
        put(F2,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
      end if;
      put(f2,tab);
      put(f2,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(1)*180.0/pi));
      new_line(f2);

      if mutil then
	    put_line(f3,integer'image(code));
        put_line(f3,"POINT");
        put(f3,"1");
        put(f3,tab);
        put(f3,integer'image(symboleviewer));
        put(f3,tab);
        put(f3,integer'image(couleur));
        put(f3,tab);
        put(f3,integer'image(gis));
        put(f3,tab);
        M1g:=(0,0);
        M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
        put(F3,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
        put(f3,tab);
        put(f3,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(1)*180.0/pi));
        new_line(f3);
      end if;

      for j in 0..31 loop
        c:=character(info_font(tbnomi(no).police).police.lfFaceName(j));
        policename(j+1):=c;
      end loop;
      jo:=33;
      for j in 1..32 loop
        if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
          jo:=j;
          exit;
        end if;
      end loop;
      put_line(f2,policename(1..jo-1));
      if mutil then
        put_line(f3,policename(1..jo-1));
      end if;
      
      if tp(no,tbnomi(no).p_choisie).nl=1 then
--  	    if sep=';' then
--          put(f,""""&tbnomi(no).chaine(1)(1..tbnomi(no).ncar(1))&"""");
--		else





          put(f,tbnomi(no).chaine(1)(1..L));
--          put(f,tbnomi(no).chaine(1)(1..tbnomi(no).ncar(1)));
--        end if;



--		  -- debut gestion du caract�re '%' pour kilometrages
--		  l:=tbnomi(no).ncar(1);
--		  for K in 2..tbnomi(no).ncar(1)-1 loop
--            if tbnomi(no).chaine(1)(K)='%' then
--		      l:=k-1;
--		      exit;
--            end if;
--          end loop;
--		  -- fin gestion du caract�re '%' pour kilometrages

	    put_line(f2,tbnomi(no).chaine(1)(1..L));
	    -- put_line(f2,tbnomi(no).chaine(1)(1..tbnomi(no).ncar(1)));
        if mutil then
          put_line(f3,tbnomi(no).chaine(1)(1..L));
          -- put_line(f3,tbnomi(no).chaine(1)(1..tbnomi(no).ncar(1)));
        end if;
      else
--  	    if sep=';' then
--          put(f,""""&tbnomi(no).chaine1(1..tbnomi(no).ncar1)&"""");
--		else
          put(f,tbnomi(no).chaine1(1..tbnomi(no).ncar1));
--        end if;
        put_line(f2,tbnomi(no).chaine1(1..tbnomi(no).ncar1));
        if mutil then
          put_line(f3,tbnomi(no).chaine1(1..tbnomi(no).ncar1));
        end if;
      end if;
      put_line(f2,"1");
      if mutil then
	    put_line(f3,"1");
      end if;
      put(f,sep);

      cs:=cos(tp(no,tbnomi(no).p_choisie).angle(1));
      sn:=sin(tp(no,tbnomi(no).p_choisie).angle(1));
      Delta_Ln_Terrain:=Float(tbnomi(no).Ln(1))*ISurMilR*sn;
      Delta_Corps_Terrain:=Float(tbnomi(no).Corps)*ISurMilR*sn;
      Centrage:=(0.0,0.0);
      if coord=centre then
        if tp(no,tbnomi(no).p_choisie).nl=1 or tbnomi(no).topo.objet_type=lineaire then
           centrage:=(float(tbnomi(no).ln(1)/2),float(tbnomi(no).corps/2));
        else
           centrage:=(float(tbnomi(no).ln1/2),float(tbnomi(no).corps/2));
        end if;
      end if;
      Centrage_oriente.coor_x:=(centrage.coor_x*cs-centrage.coor_y*Sn)*ISurMilR;
	  Centrage_oriente.coor_Y:=(centrage.coor_Y*cs+centrage.coor_X*Sn)*ISurMilR;
      coin1_terrain:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).coin1(1),InvEchelle,resolution,Minimum_terrain);

      put(f,coin1_terrain.coor_x+centrage_oriente.coor_x,0,2,0);
      put(f,";");
      put(f,coin1_terrain.coor_y+centrage_oriente.coor_y,0,2,0);		
      put(f,sep);

      if mutil then
		put(f2,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut)*1000.0)*Sn));
		put(f2,tab);
		put(f2,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut)*1000.0)*Cs));

		put(f3,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Sn));
		put(f3,tab);
		put(f3,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Cs));
      else
		put(f2,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Sn));		
		put(f2,tab);
		put(f2,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Cs));
      end if;

      new_line(f2);
      if mutil then
	    new_line(f3);
      end if;

      if tp(no,tbnomi(no).p_choisie).nl=2 and tbnomi(no).topo.objet_type/=lineaire then
--        if sep=';' then
--          put(f,""""&tbnomi(no).chaine2(1..tbnomi(no).ncar2)&"""");
--		else
          put(f, tbnomi(no).chaine2(1..tbnomi(no).ncar2));
--        end if;
        put(f,sep);
        put_line(f2,integer'image(code));
        put_line(f2,"POINT");
        put(f2,"1");
        put(f2,tab);
        put(f2,integer'image(symboleviewer));
        put(f2,tab);
        put(f2,integer'image(couleur));
        put(f2,tab);
        put(f2,integer'image(gis));
        put(f2,tab);

        if mutil then
          put(F2,integer(float(tbnomi(no).corps*Resol_mut)*10.0/float(Resolution)));
        else
          put(F2,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
        end if;
		put(f2,tab);
        put(f2,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(1)*180.0/pi));
        new_line(f2);

        put_line(f2,policename(1..jo-1));
        put_line(f2,tbnomi(no).chaine2(1..tbnomi(no).ncar2));
        put_line(f2,"1");
        centrage:=(0.0,0.0);

        if mutil then
		  put_line(f3,integer'image(code));
          put_line(f3,"POINT");
          put(f3,"1");
          put(f3,tab);
          put(f3,integer'image(symboleviewer));
          put(f3,tab);
          put(f3,integer'image(couleur));
          put(f3,tab);
          put(f3,integer'image(gis));
          put(f3,tab);
          put(F3,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
	  	  put(f3,tab);
          put(f3,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(1)*180.0/pi));
          new_line(f3);

          put_line(f3,policename(1..jo-1));
          put_line(f3,tbnomi(no).chaine2(1..tbnomi(no).ncar2));
          put_line(f3,"1");
        end if;

        if coord=centre then
          centrage:=(float(tbnomi(no).ln2/2)*ISurMilR,float(tbnomi(no).corps/2)*ISurMilR);
	    end if;
        coin2_terrain:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).coin2,InvEchelle, resolution, Minimum_terrain);
        put(f,coin2_terrain.coor_x+Centrage.coor_x,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_y+centrage.coor_y,0,2,0);
        put(f,sep);
        j:=tbnomi(no).topo.code'last;
        while tbnomi(no).topo.code(j)=' ' loop
          j:=j-1;
        end loop;
--  	    if sep=';' then
--          put(f,""""&tbnomi(no).topo.code(1..j)&"""");
--        else
          put(f,tbnomi(no).topo.code(1..j));
--        end if;
        put(f2,coin2_terrain.coor_x,0,2,0);
        put(f2,tab);
        if mutil then
          put(f2,integer(coin2_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR+9.0*float(InvEchelle)/(float(Resol_mut)*1000.0)));
        else
          put(f2,integer(coin2_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)));
        end if;
        new_line(f2);
		if mutil then
          put(f3,coin2_terrain.coor_x,0,2,0);
          put(f3,tab);
          put(f3,integer(coin2_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)));
          new_line(f3);
        end if;
      else
        put(f,sep);
        put(f,sep);
        if sep=';' then
          put(f,sep);
        end if;
        j:=tbnomi(no).topo.code'last;
        while tbnomi(no).topo.code(j)=' ' loop
          j:=j-1;
        end loop;
--  	    if sep=';' then
--          put(f,""""&tbnomi(no).topo.code(1..j)&"""");
--        else
          put(f,tbnomi(no).topo.code(1..j));
--        end if;
      end if;
      put(f,sep);

	  -- put(f,tp(no,tbnomi(no).p_choisie).angle(1)*180.0/pi,0,1,0);
	  put(f,tp(no,tbnomi(no).p_choisie).angle(1),0,3,0);  -- ici degres/radians --
      new_line(f);

      -- debut sortie viewer des textes des blasons texte
      nl:=tp(no,tbnomi(no).p_choisie).nl;
      for i in 1..tbnomi(no).npic loop
        if tbnomi(no).CAPSer(tbnomi(no).capict(nl)(i).ser).iser>NS then
          put_line(f2,integer'image(code));
          put_line(f2,"POINT");
          put(f2,"1");
          put(f2,tab);
          put(f2,integer'image(symboleviewer));
          put(f2,tab);
          put(f2,integer'image(couleur));
          put(f2,tab);
          put(f2,integer'image(gis));
          put(f2,tab);
          M1g:=(0,0);
          M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
          M2g:=(0,tbnomi(no).capict(nl)(i).dy);
          M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
          if mutil then
            put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0));
          else
            put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));          
          end if;
	      put(f2,tab);
          put(f2,0);
          new_line(f2);
          police:=tbnomi(no).CAPSer(tbnomi(no).capict(nl)(i).ser).iser-NS;
          for j in 0..31 loop
            c:=character(info_font(police).police.lfFaceName(j));
            policename(j+1):=c;
          end loop;          
          jo:=33;
          for j in 1..32 loop
            if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
              jo:=j;
              exit;
            end if;
          end loop;          
          put_line(f2,policename(1..jo-1));
          put_line(f2,tbnomi(no).capict(nl)(i).str(1..tbnomi(no).capict(nl)(i).lstr));
          put_line(f2,"1");
    
          if mutil then
	  	    put_line(f3,integer'image(code));
            put_line(f3,"POINT");
            put(f3,"1");
            put(f3,tab);
            put(f3,integer'image(symboleviewer));
            put(f3,tab);
            put(f3,integer'image(couleur));
            put(f3,tab);
            put(f3,integer'image(gis));
            put(f3,tab);
            M1g:=(0,0);
            M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
            M2g:=(0,tbnomi(no).capict(nl)(i).dy);
            M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
            put(f3,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));          
  		    put(f3,tab);
            put(f3,0);
            new_line(f3);
            police:=tbnomi(no).CAPSer(tbnomi(no).capict(nl)(i).ser).iser-NS;
            for j in 0..31 loop
              c:=character(info_font(police).police.lfFaceName(j));
              policename(j+1):=c;
            end loop;          
            jo:=33;
            for j in 1..32 loop
              if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
                jo:=j;
                exit;
              end if;
            end loop;          
            put_line(f3,policename(1..jo-1));
            put_line(f3,tbnomi(no).capict(nl)(i).str(1..tbnomi(no).capict(nl)(i).lstr));
            put_line(f3,"1");
          end if;

          if nl=1 then 
            coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
              +tp(no,tbnomi(no).p_choisie).coin1(1),InvEchelle,resolution,Minimum_terrain);
            coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
              +(tbnomi(no).capict(1)(i).dx,tbnomi(no).capict(1)(i).dy)
              +tp(no,tbnomi(no).p_choisie).coin1(1),InvEchelle,resolution,Minimum_terrain);
          else
            if tbnomi(no).capict(tp(no,tbnomi(no).p_choisie).nl)(i).y>0 then
              coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
                ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
                +tp(no,tbnomi(no).p_choisie).coin1(1),InvEchelle,resolution,Minimum_terrain);
              coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
                ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
                +(tbnomi(no).capict(2)(i).dx,tbnomi(no).capict(2)(i).dy)
                +tp(no,tbnomi(no).p_choisie).coin1(1),InvEchelle,resolution,Minimum_terrain);
            else
              coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
                ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
                +tp(no,tbnomi(no).p_choisie).coin2,InvEchelle,resolution,Minimum_terrain);
              coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
                ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
                +(tbnomi(no).capict(2)(i).dx,tbnomi(no).capict(2)(i).dy)
                +tp(no,tbnomi(no).p_choisie).coin2,InvEchelle,resolution,Minimum_terrain);
            end if;
          end if;

          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          if mutil then
            put(f2,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/float(Resol_Mut),0,2,0);
          else
            put(f2,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);          
          end if;
          new_line(f2);

          if mutil then
		    put(f3,coin_blas_terrain.coor_x,0,2,0);
            put(f3,tab);
            put(f3,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);          
            new_line(f3);
          end if;

        end if;
      end loop;
      -- fin sortie viewer des textes des blasons texte

      -- end loop;
    end if;
  end loop;
  close(f);
end Sauve_ecritures_placees_recti;

--=========================================================
-- Fichier de sortie au format texte des ecritures placees
-- a disposition sur courbe + sortie viewer
--=========================================================
procedure sauve_ecritures_placees_dispo (tbnomi : in typtbnomi;
                                      tnp : in typtnp;  --nb de positions
                                      tp : in typtp;   -- tableau des positions
                                      InvEchelle : positive;
                                      Resolution : positive;
                                      Minimum_terrain : Point_type_reel;
                                      f2 : in out text_io.file_type;
                                      miecp : in out boolean;
									  f3 : in out text_io.file_type) is

f : text_io.file_type;
coin1_terrain, coin2_terrain : Point_type_reel:=(0.0,0.0);
nomfichier : string120;
centrage : point_type_reel;
centrage_oriente : point_type_reel;
code : integer:=7;
couleur : integer:=0; -- noir
symboleviewer : integer:=7; -- pas de symbole
gis : integer range 0..14:=0;
policename : string(1..32);
jo,j : integer;
info_font: typtfont(1..500);
nbfont : integer;
c : character;
M1g,M2g,M3g : Point_type;
M1t,M2t,M3t : Point_type_reel;
Delta_Terrain : float;
Resol_Mut_reel : float;
infocodesp : string30:=(1..30=>' ');
infocode : string30:=(1..30=>' ');
infocode_nbcar : integer;
police : positive; -- indice de police d'un blason texte
coin_blas_terrain,coin_opp_blas_terrain : point_type_reel; -- emprise terrain d'un blason
nl : integer; -- ecriture sur nl=1 ou nl=2 ligne(s) pour les ecritures horizontales
              -- dessus nl=1 ou dessous nl=2 pour les numeros de routes
cs,sn :float;
Delta_Ln_Terrain,Delta_corps_Terrain : float; -- decalages a introduire pour dessiner une boite orientee 
GeomT : Point_Type_reel;
NumGeom : integer;
ISurMilR : float:=Float(InvEchelle)/(1000.0*Float(Resolution));
Dist,AbscurvIni,AbscurvFin,AbscurvTotale,Reste : float;
No_Pred : integer;
Justification : float:=0.0;
Offset : float;
N : integer;
Inters : Point_Access_type;
nInters : integer;
PT,Orth : point_type;
PTy : Point_Liste_Type(1..2);
OffsetplusCarre : float;
Pas : float; -- pas d'echantillonnage
ChaineSansDiese : String100:= (others => ' ');
TP_NGeom : integer;

begin
  nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomfichier(ncar_fo-3..ncar_fo+15):="_placees_courbe.txt";
  create(f,out_file,nomfichier(1..ncar_fo+15));

  charge_fontes(info_font,nbfont);
  
  Delta_Terrain:=max(Vse.coor_x-Vno.coor_x,Vno.coor_y-Vse.coor_y);
  Resol_Mut_reel:=float(InvEchelle)*8.0/Delta_Terrain; -- 8 comme 8000*8000 : dimensions de l'image de fond

  for no in 1..tbnomi'last loop

    if tbnomi(no).p_choisie/=0 and (tbnomi(no).topo.Mode_Placement=CAxe or tbnomi(no).topo.Mode_Placement=CDesaxe) then
      for k in 1..tbnomi(no).Nb_mots loop
        put(f,tbnomi(no).id,0);
	    if tbnomi(no).id<0 then
	      Put(f,".");
	      put(f,k,0);
        end if;
        put(f,sep);
        miecp:=true;
        put_line(f2,integer'image(code));
        put_line(f2,"POINT");
        put(f2,"1");
        put(f2,tab);
        put(f2,integer'image(symboleviewer));
        put(f2,tab);
        put(f2,integer'image(couleur));
        put(f2,tab);
        put(f2,integer'image(gis));
        put(f2,tab);
        M1g:=(0,0);
        M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
        if mutil then
	      put(F2,integer(float(tbnomi(no).corps*Resol_mut)*10.0/float(Resolution)));
        else
          put(F2,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
        end if;
        put(f2,tab);
        put(f2,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(K)*180.0/pi));
        new_line(f2);

        if mutil then
	      put_line(f3,integer'image(code));
          put_line(f3,"POINT");
          put(f3,"1");
          put(f3,tab);
          put(f3,integer'image(symboleviewer));
          put(f3,tab);
          put(f3,integer'image(couleur));
          put(f3,tab);
          put(f3,integer'image(gis));
          put(f3,tab);
          M1g:=(0,0);
          M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
          put(F3,integer(float(tbnomi(no).corps)*Resol_mut_reel*10.0/float(Resolution)));
          put(f3,tab);
          put(f3,integer(10.0*tp(no,tbnomi(no).p_choisie).angle(K)*180.0/pi));
          new_line(f3);
        end if;

        for j in 0..31 loop
          c:=character(info_font(tbnomi(no).police).police.lfFaceName(j));
          policename(j+1):=c;
        end loop;
        jo:=33;
        for j in 1..32 loop
          if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
            jo:=j;
            exit;
          end if;
        end loop;
        put_line(f2,policename(1..jo-1));
        if mutil then
          put_line(f3,policename(1..jo-1));
        end if;
      
        if tp(no,tbnomi(no).p_choisie).nl=1 then
          for NC in 1..tbnomi(no).ncar(K) loop
            if tbnomi(no).chaine(K)(NC)/='#' then
              ChaineSansDiese(NC):=tbnomi(no).chaine(K)(NC);
            else
              ChaineSansDiese(NC):=' ';
            end if;
          end loop;
  	      if sep=';' then
            put(f,""""&ChaineSansDiese(1..tbnomi(no).ncar(K))&"""");
		  else
            put(f,ChaineSansDiese(1..tbnomi(no).ncar(K)));
          end if;
	      put_line(f2,ChaineSansDiese(1..tbnomi(no).ncar(K)));
          if mutil then
            put_line(f3,ChaineSansDiese(1..tbnomi(no).ncar(K)));
          end if;
        else
          for NC in 1..tbnomi(no).ncar1 loop
            if tbnomi(no).chaine1(NC)/='#' 	then
              ChaineSansDiese(NC):=tbnomi(no).chaine1(NC);
            else
              ChaineSansDiese(NC):=' ';
            end if;
          end loop;
  	      if sep=';' then
            put(f,""""&ChaineSansDiese(1..tbnomi(no).ncar1)&"""");
		  else
            put(f,ChaineSansDiese(1..tbnomi(no).ncar1));
          end if;
          put_line(f2,ChaineSansDiese(1..tbnomi(no).ncar1));
          if mutil then
            put_line(f3,ChaineSansDiese(1..tbnomi(no).ncar1));
          end if;
        end if;
        put_line(f2,"1");
        if mutil then
	      put_line(f3,"1");
        end if;
        put(f,sep);

        cs:=cos(tp(no,tbnomi(no).p_choisie).angle(K));
        sn:=sin(tp(no,tbnomi(no).p_choisie).angle(K));
        Delta_Ln_Terrain:=Float(tbnomi(no).Ln(K))*ISurMilR*sn;
        Delta_Corps_Terrain:=Float(tbnomi(no).Corps)*ISurMilR*sn;
        Centrage:=(0.0,0.0);
        if coord=centre then
          if tp(no,tbnomi(no).p_choisie).nl=1 or tbnomi(no).topo.objet_type=lineaire then
            centrage:=(float(tbnomi(no).ln(K)/2),float(tbnomi(no).corps/2));
          else
            centrage:=(float(tbnomi(no).ln1/2),float(tbnomi(no).corps/2));
          end if;
        end if;
        Centrage_oriente.coor_x:=(centrage.coor_x*cs-centrage.coor_y*Sn)*ISurMilR;
	    Centrage_oriente.coor_Y:=(centrage.coor_Y*cs+centrage.coor_X*Sn)*ISurMilR;
        coin1_terrain:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).coin1(K),InvEchelle,resolution,Minimum_terrain);

        put(f,coin1_terrain.coor_x+centrage_oriente.coor_x,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_y+centrage_oriente.coor_y,0,2,0);		
        put(f,sep);

        if mutil then
		  put(f2,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut)*1000.0)*Sn));
		  put(f2,tab);
		  put(f2,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut)*1000.0)*Cs));

		  put(f3,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Sn));
		  put(f3,tab);
		  put(f3,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Cs));
        else
		  put(f2,integer(coin1_terrain.coor_x-float(tbnomi(no).corps)*ISurMilR*sn-9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Sn));		
		  put(f2,tab);
		  put(f2,integer(coin1_terrain.coor_Y+float(tbnomi(no).corps)*ISurMilR*Cs+9.0*float(InvEchelle)/(float(Resol_mut_reel)*1000.0)*Cs));
        end if;

        new_line(f2);
        if mutil then
	      new_line(f3);
        end if;

        put_line(f2,integer'image(code));
        put_line(f2,"LIGNE");
        put_line(f2,integer'image(1)&tab&integer'image(1)&tab&integer'image(couleur));
		-- debut de correction de fermeture (repetition des 2 premiers ou des 2 derniers vertex)
		if tp(no,tbnomi(no).p_choisie).offset(K)>0.0 then
          NumGeom:=1;
	    else
          NumGeom:=2;
        end if;
--        if LMax>=4 then
--	      if tp(no,1).Geom(LL+1)=tp(no,1).Geom(LL+3) and tp(no,1).Geom(LL+2)=tp(no,1).Geom(LL+4) then
--		    LMin:=LMin+2;
--	      end if;
--	      if tp(no,1).Geom(LL+Lmax-3)=tp(no,1).Geom(LL+LMax-1) and tp(no,1).Geom(LL+Lmax-2)=tp(no,1).Geom(LL+Lmax) then
--	        Lmax:=Lmax-2;
--	      end if;
--        end if;
		-- fin de correction de fermeture (repetition des 2 premiers ou des 2 derniers vertex)
		put_line(f2,integer'image(tp(no,NumGeom).NGeom(1)));

		for l in 1..tp(no,NumGeom).NGeom(1) loop
		  -- GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(MaxNGeom*(k-1)+L),InvEchelle, resolution, Minimum_terrain);
		  --* GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(LL+L),InvEchelle, resolution, Minimum_terrain);
		  GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,NumGeom).Geom(L),InvEchelle, resolution, Minimum_terrain);
          put(f2,GeomT.coor_X,0,2,0);
          put(f2,tab);
          put(f2,GeomT.coor_Y,0,2,0);
          new_line(f2);
        end loop;
		-- LL:=lL+tp(no,tbnomi(no).p_choisie).NGeom(K);
      -- end if;

--        put(f,sep);
--        put(f,sep);
--        put(f,sep);
--        if sep=tab then
--          put(f,sep);
--        end if;

        if mutil then
          -- new_line(f3);
          put_line(f3,integer'image(code));
          put_line(f3,"LIGNE");
          put_line(f3,integer'image(1)&tab&integer'image(1)&tab&integer'image(couleur));
		  if tp(no,tbnomi(no).p_choisie).offset(K)>0.0 then
            NumGeom:=1;
          else
            NumGeom:=2;
          end if;
		  put_line(f3,integer'image(tp(no,NumGeom).NGeom(1)));

		  for l in 1..tp(no,NumGeom).NGeom(1) loop
		    GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,NumGeom).Geom(L),InvEchelle, resolution, Minimum_terrain);
            put(f3,GeomT.coor_X,0,2,0);
            put(f3,tab);
            put(f3,GeomT.coor_Y,0,2,0);
            new_line(f3);
          end loop;
        end if;

        j:=tbnomi(no).topo.code'last;
        while tbnomi(no).topo.code(j)=' ' loop
          j:=j-1;
        end loop;
  	    if sep=';' then
          put(f,""""&tbnomi(no).topo.code(1..j)&"""");
        else
          put(f,tbnomi(no).topo.code(1..j));
        end if;
        put(f,sep);

   	  -- if tbnomi(no).nb_mots=1 then
  	    if sep=';' then
          put(F,"""");
        end if;

   	    -- debut du calcul de l'offset
        if tp(no,tbnomi(no).p_choisie).offset(K)>0.0 then
		  NumGeom:=1;
        else
		  NumGeom:=2;
		end if;

		offset:=0.0;

--        N:=integer(float(Tbnomi(no).Ln(k))/PAS_TOPO);
--        for L in 0..N loop
--		  offset:=offset+distance_a_polylignes_v2((integer(float((N-L)*tp(no,tbnomi(no).p_choisie).Point1(k).coor_X+L*tp(no,tbnomi(no).p_choisie).Point2(k).coor_X)/float(N)),
--		                                           integer(float((N-L)*tp(no,tbnomi(no).p_choisie).Point1(k).coor_Y+L*tp(no,tbnomi(no).p_choisie).Point2(k).coor_Y)/float(N)))
--				                                  ,tp(no,NumGeom).Geom.all,1,tp(no,NumGeom).NGeom(1),C_segment_type);
--        end loop;
--	    offset:=offset*2.0*(1.5-float(NumGeom))/float(N+1);

        N:=0;
	    Orth.coor_X:=5*(tp(no,tbnomi(no).p_choisie).Point1(k).coor_Y-tp(no,tbnomi(no).p_choisie).Point2(k).coor_Y);
        Orth.coor_y:=5*(tp(no,tbnomi(no).p_choisie).Point2(k).coor_X-tp(no,tbnomi(no).p_choisie).Point1(k).coor_X);

        for L in 0..10 loop
          PT:=(integer(float((10-L)*tp(no,tbnomi(no).p_choisie).Point1(k).coor_X+L*tp(no,tbnomi(no).p_choisie).Point2(k).coor_X)/10.0),
		       integer(float((10-L)*tp(no,tbnomi(no).p_choisie).Point1(k).coor_Y+L*tp(no,tbnomi(no).p_choisie).Point2(k).coor_Y)/10.0));
          PTy(1):=(PT.coor_X+Orth.coor_X,PT.coor_Y+Orth.coor_Y);
		  PTy(2):=(PT.coor_X-Orth.coor_X,PT.coor_Y-Orth.coor_Y);

		  TP_NGeom:=tp(no,NumGeom).NGeom(1);
	      Intersections_de_polylignes(tp(no,NumGeom).Geom.all,PTy,tp(no,NumGeom).NGeom(1),2,Inters,NInters,false);

		  if Ninters/=0 then
            N:=N+1;
			-- Dis[L]:=sqrt(distance_de_points(PT,Inters(1)));
            -- Offsetplus:=sqrt(distance_de_points(PT,Inters(1)));

		    OffsetplusCarre:=distance_de_points(PT,Inters(1));

			if NInters>1 then
              for num in 2..NInters loop
                if distance_de_points(PT,Inters(Num))<OffsetplusCarre then
                  OffsetplusCarre:=distance_de_points(PT,Inters(Num));
                end if;
              end loop;
            end if;
			Offset:=offset+sqrt(OffsetplusCarre);
--	      else
--	  	    Dis[L]:=0.0;
          end if;
          GR_Free_point_liste(Inters);          
        end loop;
		if n/=0 then
          -- offset = valeur mediane des valeurs non nulles de Dist[0..10]
	      offset:=offset*2.0*(1.5-float(NumGeom))/float(N);
        else
		  offset:=0.0;
        end if;

		if NumGeom=2 then
		  offset:=offset-float(Tbnomi(no).Corps);
        end if;
	    -- fin du calcul de l'offset

        -- debut du calcul de la justification
        declare
          ArcC : Point_access_reel;  -- Geometrie centree sur l'axe des lettres
		  ArcCR : Point_access_reel;
          ArcCRegulier : Point_access_reel;
          ArcCInt : Point_access_Type;
        begin
		  -- if tp(no,tbnomi(no).p_choisie).offset(K)>0.0 then
          -- if offset>0.0 then

		  if NumGeom=1 then
            declare
              TAB_TGReal : point_liste_reel(1..tp(no,1).NGeom(1));
              ParentC : Liens_access_type;
            begin
              for I in 1..tp(no,1).NGeom(1) loop
                TAB_TGReal(I):=(float(tp(no,1).Geom(I).coor_x),float(tp(no,1).Geom(I).coor_Y));
              end loop;
			  if tbnomi(no).topo.Mode_Placement=CDesaxe then
			    Dist:=offset+float(tbnomi(no).Corps)/2.0;
              else
		  	    Dist:=0.0;
		      end if;

		      Dilatation_Gauche(TAB_TGReal(1..tp(no,1).NGeom(1)),Dist,ArcC,ParentC,false,PrecisionDilatation);
			  GR_Free_liens_array(ParentC);
            end;
          else
            declare
              TAB_TDReal : point_liste_reel(1..tp(no,2).NGeom(1));
              ParentC : Liens_access_type;
            begin
              for I in 1..tp(no,2).NGeom(1) loop
                TAB_TDReal(I):=(float(tp(no,2).Geom(I).coor_x),float(tp(no,2).Geom(I).coor_Y));
              end loop;
			  if tbnomi(no).topo.Mode_Placement=CDesaxe then
			    Dist:=-offset-float(tbnomi(no).Corps)/2.0;
		      else
		  	    Dist:=0.0;
		      end if;
		      Dilatation_Droite(TAB_TDReal(1..tp(no,2).NGeom(1)),Dist,ArcC,ParentC,True,PrecisionDilatation);
			  GR_Free_liens_array(ParentC);
            end;
          end if;
          abscurvtotale:=Taille_ligne(ArcC.all,ArcC'last);
		  Pas:=0.3*float(tbnomi(no).corps);
          ArcCRegulier:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
		  Repartition_reguliere(ArcC.all,Pas,ArcCRegulier.all);
		  GR_Free_point_liste_Reel(ArcC);

		  -- ArcCR:=new Point_liste_reel(1..integer(abscurvtotale*2.0/float(Resolution)));
		  -- ArcCR.all:=Reconstruction(ArcC.all,240.0,ArcC'last);
          -- ArcCR.all:=Reconstruction(ArcC.all,0.001,integer(abscurvtotale*2.0/float(Resolution)));

          -- ArcCInt:=new Point_liste_type(1..integer(abscurvtotale*2.0/float(Resolution)));
          ArcCInt:=new Point_liste_type(1..ArcCRegulier'last);
	      for J in 1..ArcCInt'last loop
            -- ArcCInt.all(J):=(integer(ArcCR.all(J).coor_x),integer(ArcCR.all(J).coor_Y));
            ArcCInt.all(J):=(integer(ArcCRegulier.all(J).coor_x),integer(ArcCRegulier.all(J).coor_Y));
          end loop;
--          GR_Free_point_liste_Reel(ArcCR);

	      projection_polylignes_v2(ArcCInt.all,1,ArcCInt'last,tp(no,tbnomi(no).p_choisie).Point1(k),No_Pred,Reste);
		  Position_to_Abs_curv_v2(ArcCInt.all,1,ArcCInt'last,No_Pred,Reste,AbscurvIni);
          projection_polylignes_v2(ArcCInt.all,1,ArcCInt'last,tp(no,tbnomi(no).p_choisie).Point2(k),No_Pred,Reste);
          Position_to_Abs_curv_v2(ArcCInt.all,1,ArcCInt'last,No_Pred,Reste,AbscurvFin);
		  -- abscurvtotale:=Taille_ligne(ArcCInt.all,ArcC'last);

		  if AbsCurvTotale+AbscurvIni-AbscurvFin/=0.0 then
            Justification:=AbscurvIni/(AbsCurvTotale+AbscurvIni-AbscurvFin);
          else
            Justification:=0.5;
	      end if;
	      if Justification>1.0 then
            Justification:=1.0;
          end if;
          if Justification<0.0 then
            Justification:=0.0;
          end if;
          -- fin du calcul de la justification

		  if tp(no,tbnomi(no).p_choisie).offset(K)>0.0 then
            NumGeom:=1;
          else
            NumGeom:=2;
          end if;
--        if IMax>=4 then
--	      if tp(no,1).Geom(II+1)=tp(no,1).Geom(II+3) and tp(no,1).Geom(II+2)=tp(no,1).Geom(II+4) then
--		    IMin:=IMin+2;
--	      end if;
--	      if tp(no,1).Geom(II+IMax-3)=tp(no,1).Geom(II+IMax-1) and tp(no,1).Geom(II+IMax-2)=tp(no,1).Geom(II+IMax) then
--	        IMax:=IMax-2;
--	      end if;
--        end if;
          put(F,"1;");
          -- put(F,tp(no,NumGeom).NGeom(1),1);
          put(F,ArcCInt'last,1);
          put(F,";");
		  -- for I in 1..tp(no,NumGeom).NGeom(1) loop
		  for I in 1..ArcCInt'last loop
            -- GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(MaxNGeom*(k-1)+I),InvEchelle, resolution, Minimum_terrain);
            --* GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(II+I),InvEchelle, resolution, Minimum_terrain);
            -- GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,NumGeom).Geom(I),InvEchelle, resolution, Minimum_terrain);
            GeomT:=Lecture.Convert_coord_graphe_terrain(ArcCInt(I),InvEchelle, resolution, Minimum_terrain);
	  	    put(F,GeomT.coor_X,0,2,0);
            put(F,";");
	  	    put(F,GeomT.coor_Y,0,2,0);
            put(F,";");
          end loop;
  	      if sep=';' then
            put(F,"""");
          end if;
          put(f,sep);
	  	  -- put(f,tp(no,tbnomi(no).p_choisie).Justification(K),0,4,0);
	  	  put(f,Justification,0,4,0);
          put(f,sep);
	  	  -- put(f,tp(no,tbnomi(no).p_choisie).offset(K)*float(InvEchelle)/float(1000*Resolution),0,2,0);
	  	  put(f,-0.5*float(tbnomi(no).corps)*ISurMilR,0,2,0);
		  GR_Free_point_liste_Reel(ArcCRegulier);
		  GR_Free_point_liste(ArcCInt);
		end;
        new_line(f);
      end loop;
    end if;

    << finBoucleNo >>
	null;
  end loop;
  close(f);
end Sauve_ecritures_placees_dispo;


--==========================================================================
-- Fichier de sortie au format texte des ecritures bloquees + sortie viewer --
--==========================================================================
procedure sauve_ecritures_bloquees (tbnomi : in typtbnomi;
                                       tnp : in typtnp;  --nb de positions
                                       tp : in typtp;   -- tableau des positions
                                       InvEchelle : positive;
                                       Resolution : positive;
                                       Minimum_terrain : Point_type_reel;
                                       f2 : in out text_io.file_type;
                                       compesp : in out integer;
                                       compeb : in out integer;
                                       compmtsp : in out integer;
                                       compmtb : in out integer;
 									   f3 : in out text_io.file_type) is

  
fbloc :  text_io.file_type;
coord_objet: point_type_reel:=(0.0, 0.0);
nomfichier : string120;
centrage : point_type;
centrage_oriente : point_type;
code : integer:=8;
couleur : integer:=255; -- rouge
symboleviewer : integer:=7; -- pas de symbole
gis : integer range 0..14 :=0;
policename : string(1..32);
jo,j : integer;
info_font: typtfont(1..500);
nbfont : integer;
c : character;
M1g,M2g,M3g : Point_type;
M1t,M2t,M3t : Point_type_reel;
Delta_Terrain : float;
Resol_Mut_reel : float;
infocodesp : string30:=(1..30=>' ');
infocode : string30:=(1..30=>' ');
infocode_nbcar : integer;
police : positive; -- indice de police d'un blason texte
coin_blas_terrain,coin_opp_blas_terrain : point_type_reel; -- emprise terrain d'un blason
coin : point_type; -- coordonnee relative du coin inf gauche d'un blason dans le repere terrain
f : float;
tb : nom_type;
ln : integer;
GeomT : Point_Type_reel;
iI,IMin,IMax : integer;
Abscurvtotale,pas : float;
-- MinNumGeom,MaxNumGeom : integer;
I,k,l : integer;

begin
  nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomfichier(ncar_fo-3..ncar_fo+12):="_non_placees.txt";
  create(fbloc,out_file,nomfichier(1..ncar_fo+12));

  charge_fontes(info_font,nbfont);

  Delta_Terrain:=max(Vse.coor_x-Vno.coor_x,Vno.coor_y-Vse.coor_y);
  Resol_Mut_reel:=float(InvEchelle)*8.0/Delta_Terrain; -- 8 comme 8000*8000 : dimensions de l'image de fond

  for no in 1..tbnomi'last loop
    if tbnomi(no).p_choisie=0 then
		
      -- debut gestion du caract�re '%' pour kilometrages
	  l:=tbnomi(no).ncar(1);
	  for K in 2..tbnomi(no).ncar(1)-1 loop
        if tbnomi(no).chaine(1)(K)='%' then
		  l:=k-1;
		  exit;
        end if;
      end loop;
      -- fin gestion du caract�re '%' pour kilometrages

	  if L=tbnomi(no).ncar(1) then
        put(fbloc,tbnomi(no).id,0);
      else
        put(fbloc,tbnomi(no).chaine(1)(L+2..tbnomi(no).ncar(1)));
      end if;
      put(fbloc,sep);
	  if sep=';' then
	  	put(fbloc,"""");
      end if;
	  ln:=0;
      for i in 1..tbnomi(no).nb_mots loop
	  	ln:=ln+tbnomi(no).ln(i);
		if i=1 then
          put(fbloc,tbnomi(no).chaine(I)(1..L));
        else
          put(fbloc,tbnomi(no).chaine(I)(1..tbnomi(no).ncar(i)));	
        end if;
        if tbnomi(no).nb_mots>=2 and i/=tbnomi(no).nb_mots then
		  put(fbloc," ");
	    end if;
      end loop;
	  if sep=';' then
	  	put(fbloc,"""");
      end if;
      put(fbloc,sep);
      if coord=centre then
        coord_objet:=Lecture.Convert_coord_graphe_terrain(tbnomi(no).coord_objet,InvEchelle,resolution,Minimum_terrain);
      else                
        -- centrage:=(tbnomi(no).ln1/2,tbnomi(no).corps/2);
        centrage:=(ln/2,tbnomi(no).corps/2);
        coord_objet:=Lecture.Convert_coord_graphe_terrain(tbnomi(no).coord_objet-centrage,InvEchelle,resolution,Minimum_terrain);
      end if;                   
      put(fbloc,coord_objet.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coord_objet.coor_y,0,2,0);
      put(fbloc,sep);
      j:=tbnomi(no).topo.code'last;
      while tbnomi(no).topo.code(j)=' ' loop
        j:=j-1;
      end loop;
	  if sep=';' then
        put(fbloc,""""&tbnomi(no).topo.code(1..j)&"""");
	  else
        put(fbloc,tbnomi(no).topo.code(1..j));
      end if;


	  -- geometrie;
--      minNumGeom:=1;
--      maxNumGeom:=2;
--
--	  if tp(no,1).Geom=null then
--	  	minNumGeom:=2;
--      end if;
--	  if tp(no,2).Geom=null then
--	  	maxNumGeom:=1;
--      end if;

      for NumGeom in 1..2 loop
	  	declare
          GeomReguliere : Point_access_type;
        begin
	      if sep=';' then
            put(fbloc,sep&"""");
          else
            put(fbloc,sep);
	      end if;

--          if tp(no,NumGeom).Geom=null then
--            gb.msgBox("aaa");
--          end if;
--
--		  if tp(no,NumGeom).NGeom(1)=0 then
--            gb.msgBox("bbb");
--          end if;

          if tp(no,NumGeom).Geom=null then
            put(fbloc,"1;0;");
	        if sep=';' then
              put(fbloc,"""");
            end if;
			goto finBoucleNumGeom;
          end if;

          abscurvtotale:=Taille_ligne(tp(no,NumGeom).Geom.all,tp(no,NumGeom).NGeom(1));
		  Pas:=0.3*float(tbnomi(no).corps);
          GeomReguliere:=new point_liste_type(1..integer(abscurvtotale/Pas)+2);
		  Repartition_reguliere(tp(no,NumGeom).Geom(1..tp(no,NumGeom).NGeom(1)),Pas,GeomReguliere.all);

	      IMin:=1;
          IMax:=GeomReguliere'last;
          put(fbloc,"1;");
          put(fbloc,IMax-IMin+1,1);
          put(fbloc,";");
		  for I in Imin..Imax loop
            -- GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(MaxNGeom*(k-1)+I),InvEchelle, resolution, Minimum_terrain);
            --* GeomT:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).Geom(II+I),InvEchelle, resolution, Minimum_terrain);
            GeomT:=Lecture.Convert_coord_graphe_terrain(GeomReguliere(I),InvEchelle, resolution, Minimum_terrain);
	  	    put(fbloc,GeomT.coor_X,0,2,0);
            put(fbloc,";");
	  	    put(fbloc,GeomT.coor_Y,0,2,0);
            put(fbloc,";");
          end loop;
	      if sep=';' then
            put(fbloc,"""");
          end if;
		  GR_Free_point_liste(GeomReguliere);
        end;
		<< finBoucleNumGeom >>
		null;
      end loop;

	  if sep=';' then
        put(fbloc,sep&"""");
	  else
        put(fbloc,sep);
      end if;

      if tNP(no)=0 then
	    if tbnomi(no).topo.objet_type/=lineaire then
          compesp:=compesp+1;
		  case tbnomi(no).cause is
		    when hors_image => put(fbloc,"hors image");
            when ambigu => put(fbloc,"ambigu�t�");
		    when others => put(fbloc,"mutilation");
		  end case;
        else
          compmtsp:=compmtsp+1;
          tb:=tbnomi(no);
		  case tbnomi(no).cause is
            when mutilation => put(fbloc,"mutilation");
            when bord_image => put(fbloc,"bord image");
            when Hors_image => put(fbloc,"hors image");
            when MetaCourt => if tbnomi(no).topo.mode_placement=DROIT then
			                    put(fbloc,"horizontal");
                              else
                                put(fbloc,"court");
					          end if;
            when ambigu => put(fbloc,"ambigu�t�");
            when agencement => put(fbloc,"agencement");
            when others => put(fbloc,"sinueux");
          end case;
        end if;
      else
        if tbnomi(no).topo.objet_type/=lineaire then
          compeb:=compeb+1;
        else
          compmtb:=compmtb+1;
        end if;
		case tbnomi(no).cause is
		  when MNT_Plat => Put(fbloc,"orientation");
		  when Direction_G => Put(fbloc,"orientation");
		  when Sommet_Plat => Put(fbloc,"orientation");
		  when others => put(fbloc,"placement");
	    end case;
      end if;
	  if sep=';' then
        put(fbloc,"""");
	  end if;
  
      new_line(fbloc);

      put_line(f2,integer'image(code));
      put_line(f2,"POINT");
      put(f2,"1");
      put(f2,tab);
      put(f2,integer'image(symboleviewer));
      put(f2,tab);
      put(f2,integer'image(couleur));
      put(f2,tab);
      put(f2,integer'image(gis));
      put(f2,tab);
      M1g:=(0,0);      
      M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
      M2g:=(0,tbnomi(no).corps);
      M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
      if mutil then
        put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0));
      else
        put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));        
      end if;
	  put(f2,tab);
      put(f2,0);
      new_line(f2);
      for j in 0..31 loop
        c:=character(info_font(tbnomi(no).police).police.lfFaceName(j));
        policename(j+1):=c;
      end loop;
      jo:=33;
      for j in 1..32 loop
        if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
          jo:=j;
          exit;
        end if;
      end loop;
      put_line(f2,policename(1..jo-1));

      for i in 1..tbnomi(no).nb_mots loop
--		-- debut gestion du caract�re '%' pour kilometrages
--		l:=tbnomi(no).ncar(i);
--		for K in 2..tbnomi(no).ncar(i)-1 loop
--          if tbnomi(no).chaine(I)(K)='%' then
--		    l:=k-1;
--		    exit;
--          end if;
--        end loop;
--		-- fin gestion du caract�re '%' pour kilometrages
        if I=1 then
          put(f2,tbnomi(no).chaine(I)(1..L));
		else
          put(f2,tbnomi(no).chaine(I)(1..tbnomi(no).ncar(i)));
		end if;
        if tbnomi(no).nb_mots>=2 and i/=tbnomi(no).nb_mots then
		  put(f2," ");
	    end if;
      end loop;
	  New_line(F2);

      put_line(f2,"1");
      if coord=centre then
        -- M3g:=(-tbnomi(no).ln1/2,tbnomi(no).corps/2);      
        M3g:=(-ln/2,tbnomi(no).corps/2);      
      else
        -- M3g:=(-tbnomi(no).ln1/2,tbnomi(no).corps/2)+centrage;      
        M3g:=(-ln/2,tbnomi(no).corps/2)+centrage;      
      end if;
      M3t:=Lecture.Convert_coord_graphe_terrain(M3g,InvEchelle,resolution,Minimum_terrain);
      put(f2,coord_objet.coor_x+M3t.coor_x-M1t.coor_x,0,2,0);
      put(f2,tab);
      if mutil then
        put(f2,coord_objet.coor_y+M3t.coor_y-M1t.coor_y+9.0*float(InvEchelle)/1000.0/float(Resol_Mut),0,2,0);
      else
        put(f2,coord_objet.coor_y+M3t.coor_y-M1t.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);        
      end if;
      new_line(f2);

      if mutil then
        put_line(f3,integer'image(code));
        put_line(f3,"POINT");
        put(f3,"1");
        put(f3,tab);
        put(f3,integer'image(symboleviewer));
        put(f3,tab);
        put(f3,integer'image(couleur));
        put(f3,tab);
        put(f3,integer'image(gis));
        put(f3,tab);
        M1g:=(0,0);      
        M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
        M2g:=(0,tbnomi(no).corps);
        M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
        put(f3,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));        
	    put(f3,tab);
        put(f3,0);
        new_line(f3);
        for j in 0..31 loop
          c:=character(info_font(tbnomi(no).police).police.lfFaceName(j));
          policename(j+1):=c;
        end loop;
        jo:=33;
        for j in 1..32 loop
          if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
            jo:=j;
            exit;
          end if;
        end loop;
        put_line(f3,policename(1..jo-1));

        for i in 1..tbnomi(no).nb_mots loop
			
--		  -- debut gestion du caract�re '%' pour kilometrages
--		  l:=tbnomi(no).ncar(i);
--		  for K in 2..tbnomi(no).ncar(i)-1 loop
--            if tbnomi(no).chaine(I)(K)='%' then
--		      l:=k-1;
--		      exit;
--            end if;
--          end loop;
--		  -- fin gestion du caract�re '%' pour kilometrages

		  if i=1 then
            put(f3,tbnomi(no).chaine(I)(1..L));
          else  
            put(f3,tbnomi(no).chaine(I)(1..tbnomi(no).ncar(i)));	
          end if;
          if tbnomi(no).nb_mots>=2 and i/=tbnomi(no).nb_mots then
		    put(f3," ");
	      end if;
        end loop;
	    New_line(F3);
        -- put_line(f3,tbnomi(no).chaine1(1..tbnomi(no).ncar1));

        put_line(f3,"1");
        if coord=centre then
          -- M3g:=(-tbnomi(no).ln1/2,tbnomi(no).corps/2);      
          M3g:=(-ln/2,tbnomi(no).corps/2);      
        else
          -- M3g:=(-tbnomi(no).ln1/2,tbnomi(no).corps/2)+centrage;      
          M3g:=(-ln/2,tbnomi(no).corps/2)+centrage;      
        end if;
        M3t:=Lecture.Convert_coord_graphe_terrain(M3g,InvEchelle,resolution,Minimum_terrain);
        put(f3,coord_objet.coor_x+M3t.coor_x-M1t.coor_x,0,2,0);
        put(f3,tab);
        put(f3,coord_objet.coor_y+M3t.coor_y-M1t.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);        
        new_line(f3);
      end if;
	  
	  -- debut sortie viewer des textes des blasons texte
      for i in 1..tbnomi(no).npic loop
        if tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser>NS then
          put_line(f2,integer'image(code));
          put_line(f2,"POINT");
          put(f2,"1");
          put(f2,tab);
          put(f2,integer'image(symboleviewer));
          put(f2,tab);
          put(f2,integer'image(couleur));
          put(f2,tab);
          put(f2,integer'image(gis));
          put(f2,tab);
          M1g:=(0,0);
          M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
          M2g:=(0,tbnomi(no).capict(1)(i).dy);
          M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
          if mutil then
            put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*float(Resol_Mut)*10.0));
          else
            put(f2,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));          
          end if;
		  put(f2,tab);
          put(f2,0);
          new_line(f2);
          police:=tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser-NS;
          for j in 0..31 loop
            c:=character(info_font(police).police.lfFaceName(j));
            policename(j+1):=c;
          end loop;          
          jo:=33;
          for j in 1..32 loop
            if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
              jo:=j;
              exit;
            end if;
          end loop;          
          put_line(f2,policename(1..jo-1));
          put_line(f2,tbnomi(no).capict(1)(i).str(1..tbnomi(no).capict(1)(i).lstr));
          put_line(f2,"1");

          if mutil then
		    put_line(f3,integer'image(code));
            put_line(f3,"POINT");
            put(f3,"1");
            put(f3,tab);
            put(f3,integer'image(symboleviewer));
            put(f3,tab);
            put(f3,integer'image(couleur));
            put(f3,tab);
            put(f3,integer'image(gis));
            put(f3,tab);
            M1g:=(0,0);
            M1t:=Lecture.Convert_coord_graphe_terrain(M1g,InvEchelle,resolution,Minimum_terrain);
            M2g:=(0,tbnomi(no).capict(1)(i).dy);
            M2t:=Lecture.Convert_coord_graphe_terrain(M2g,InvEchelle,resolution,Minimum_terrain);
            put(f3,integer((M2t.coor_y-M1t.coor_y)/float(InvEchelle)*1000.0*Resol_Mut_reel*10.0));          
		    put(f3,tab);
            put(f2,0);
            new_line(f3);
            police:=tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser-NS;
            for j in 0..31 loop
              c:=character(info_font(police).police.lfFaceName(j));
              policename(j+1):=c;
            end loop;          
            jo:=33;
            for j in 1..32 loop
              if Is_Alphanumeric(policename(j))=false and policename(j)/=' ' and policename(j)/='-' then
                jo:=j;
                exit;
              end if;
            end loop;          
            put_line(f3,policename(1..jo-1));
            put_line(f3,tbnomi(no).capict(1)(i).str(1..tbnomi(no).capict(1)(i).lstr));
            put_line(f3,"1");
          end if;
		  
--		  coin:=(tbnomi(no).coord_objet.coor_x-tbnomi(no).ln1/2,
--                 tbnomi(no).coord_objet.coor_y-tbnomi(no).corps/2);    
		  coin:=(tbnomi(no).coord_objet.coor_x-ln/2,
                 tbnomi(no).coord_objet.coor_y-tbnomi(no).corps/2);    
          coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
            ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
            +coin,InvEchelle,resolution,Minimum_terrain);
          coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
            ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
            +(tbnomi(no).capict(1)(i).dx,tbnomi(no).capict(1)(i).dy)
            +coin,InvEchelle,resolution,Minimum_terrain);
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          if mutil then
            put(f2,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/float(Resol_Mut),0,2,0);
          else
            put(f2,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);          
          end if;
          new_line(f2);

          if mutil then
		    put(f3,coin_blas_terrain.coor_x,0,2,0);
            put(f3,tab);
            put(f3,coin_opp_blas_terrain.coor_y+9.0*float(InvEchelle)/1000.0/Resol_Mut_reel,0,2,0);          
            new_line(f3);
         end if;
		  
		end if;
      end loop;    
      -- fin sortie viewer des textes des blasons texte            

--      end loop;
    end if;  
  end loop;
  close(fbloc);
end Sauve_ecritures_bloquees;



--====================================================================
-- Fichier de sortie au format texte des boites des ecritures placees
-- et (separement) de leurs blasons + sortie viewer
--====================================================================
procedure sauve_boites_placees (tbnomi: in typtbnomi; 
                                   tnp: in typtnp;  --nb de positions
                                   tp: in typtp;   -- tableau des positions
                                   InvEchelle: positive;
                                   Resolution: positive;
                                   Minimum_terrain: Point_type_reel;
                                   f2 : in out text_io.file_type;
                                   fblatex : in out text_io.file_type;
                                   fblaima : in out text_io.file_type
                                   ) is
                                      
f : text_io.file_type;
coin_opp : point_type:=(0,0);
coin1_terrain,coin2_terrain,coin_opp_terrain : Point_type_reel:=(0.0,0.0);
nomfichier : string120;
code : integer:=9;
couleur : integer:=16744576; -- bleu profond
epaisseur : integer:=1;
styligne : integer range 0..4:=0; -- continue
n : integer:=5; -- nombre de sommets d'une boite
lin : integer:=1;
coin_blas_terrain,coin_opp_blas_terrain : point_type_reel;
compt,noser : integer;
info_font : typtfont(1..500);
nbfont : integer;
police : positive; -- indice de police d'un blason texte
j,L : integer;
cs,sn :float;
Delta_Ln_Terrain,Delta_corps_Terrain : float; -- decalages a introduire pour dessiner une boite orientee 
ChaineSansDiese : String100:= (others => ' ');
Ln_Terrain,corps_Terrain : float;
M : integer;

use type Interfaces.C.Char_Array;
subtype C_String is Interfaces.C.Char_Array;
use type Win32.Bool; 

function StringZ(S : string) return C_String is
SZ : C_String(Interfaces.C.Size_t(S'first)..Interfaces.C.Size_T(S'last+1));
begin
  for I in S'Range loop
    SZ(Interfaces.C.Size_T(I)):=Win32.Char'Val(Character'Pos(S(I)));
  end loop;
  SZ(SZ'LAST):=Win32.Char'val(Character'Pos(ASCII.NUL));
  return SZ;
end StringZ;

procedure Delete_file(File_Name : string) is
File_Name_Z: aliased C_String (Interfaces.C.Size_t(File_Name'first)..Interfaces.C.Size_t(File_Name'last+1)):= StringZ(File_Name);
begin
  if Win32.Winbase.Deletefile(File_Name_Z(1)'Unchecked_Access)=0 then
  -- gb.MsgBox("Erreur � l'effacement des fichiers de param�tres de visualisation");
    n:=n;
  end if;
end Delete_File;

-- passage d'absolu en relatif d'un numero de blason 
function AbsoRela (abso : in positive) return positive is
serie : positive:=1;
abso2 : positive:=(DefinSeries(1).nbla mod 100)+1;
begin
  while abso2<=abso loop
    serie:=serie+1;
    abso2:=abso2+(DefinSeries(serie).nbla mod 100);
  end loop;
  abso2:=abso2-DefinSeries(serie).nbla;
  return(abso-abso2+1);
end;  
  
begin
  nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomfichier(ncar_fo-3..ncar_fo+15):="_boites_placees.txt";
  create(f,out_file,nomfichier(1..ncar_fo+15));
  
  charge_fontes(info_font,nbfont);

  for no in 1..tbnomi'last loop
    if tbnomi(no).p_choisie/=0 then
 
	  	
      -- debut gestion du caract�re '%' pour kilometrages
	  M:=tbnomi(no).ncar(1);
      if tbnomi(no).nb_mots=1 and tp(no,tbnomi(no).p_choisie).nl=1 then
	    for K in 2..tbnomi(no).ncar(1)-1 loop
          if tbnomi(no).chaine(1)(K)='%' then
		    M:=k-1;
		    exit;
          end if;
        end loop;
      end if;
      -- fin gestion du caract�re '%' pour kilometrages
	  

      for k in 1..tbnomi(no).nb_mots loop
	  	
	  if M=tbnomi(no).ncar(1) then
        put(f,tbnomi(no).id,0);
      else
	  	put(f,tbnomi(no).chaine(1)(M+2..tbnomi(no).ncar(1)));
      end if;
      if tbnomi(no).id<0 then
        put(f,".");
        put(f,K,0);
      end if;

      for NC in 1..tbnomi(no).ncar(K) loop
        if tbnomi(no).chaine(K)(NC)/='#' then
          ChaineSansDiese(NC):=tbnomi(no).chaine(K)(NC);
        else
          ChaineSansDiese(NC):=' ';
        end if;
      end loop;

      put(f,sep);
	  if k=1 then
        if sep=';' then
          put(f,""""&ChaineSansDiese(1..M)&"""");
        else 
          put(f,ChaineSansDiese(1..M));
        end if;
      else
        if sep=';' then
          put(f,""""&ChaineSansDiese(1..tbnomi(no).ncar(k))&"""");
        else
          put(f,ChaineSansDiese(1..tbnomi(no).ncar(k)));
        end if;
      end if;
      put(f,sep);
	  if sep=';' then
        put(f,"""");
	  end if;
      if tp(no,tbnomi(no).p_choisie).nl=1 then
        put(f,"1;5;");
        coin1_terrain:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle, resolution, Minimum_terrain);
        cs:=cos(tp(no,tbnomi(no).p_choisie).angle(k));
		sn:=sin(tp(no,tbnomi(no).p_choisie).angle(k));
		Ln_Terrain:=Float(tbnomi(no).Ln(k))*Float(InvEchelle)/(1000.0*Float(Resolution));
		Corps_Terrain:=Float(tbnomi(no).Corps)*Float(InvEchelle)/(1000.0*Float(Resolution));

          put(f,coin1_terrain.coor_x,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_y,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_x-Corps_terrain*Sn,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_y+Corps_terrain*cs,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_x-Corps_terrain*Sn+Ln_terrain*Cs,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_Y+Corps_terrain*cs+Ln_terrain*Sn,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_x+Ln_terrain*Cs,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_y+Ln_terrain*Sn,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_x,0,2,0);
          put(f,";");
          put(f,coin1_terrain.coor_y,0,2,0);

          put_line(f2,integer'image(code));
          put_line(f2,"LIGNE");
          put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(f2,integer'image(n));
          put(f2,coin1_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin1_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin1_terrain.coor_x-Corps_terrain*Sn,0,2,0);
          put(f2,tab);
          put(f2,coin1_terrain.coor_y+Corps_terrain*cs,0,2,0);
          new_line(f2);      
          put(f2,coin1_terrain.coor_x-Corps_terrain*Sn+Ln_terrain*Cs,0,2,0);
          put(f2,tab);
          put(f2,coin1_terrain.coor_Y+Corps_terrain*cs+Ln_terrain*Sn,0,2,0);
          new_line(f2);      
          put(f2,coin1_terrain.coor_x+Ln_terrain*Cs,0,2,0);
          put(f2,tab);
          put(f2,coin1_terrain.coor_y+Ln_terrain*Sn,0,2,0);
          new_line(f2);      
          put(f2,coin1_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin1_terrain.coor_y,0,2,0);
          new_line(f2);

        for i in 1..tbnomi(no).npic loop
          coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
            ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
            +tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle,resolution,Minimum_terrain);
          coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
            ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
            +(tbnomi(no).capict(1)(i).dx,tbnomi(no).capict(1)(i).dy)
            +tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle,resolution,Minimum_terrain);

          -- ecriture dans le fichier viewer de l'emprise des blasons, texte sur 1 ligne
          put_line(f2,integer'image(code));
          put_line(f2,"LIGNE");
          put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(f2,integer'image(n));
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);
          
          -- ecriture dans les fichiers de sortie de blasons, texte sur 1 ligne
          if tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser>NS then
            put(fblatex,tbnomi(no).id,0);
            put(fblatex,sep);
            put(fblatex,tbnomi(no).capict(1)(i).str(1..tbnomi(no).capict(1)(i).lstr));
            put(fblatex,sep);            
            put(fblatex,coin_blas_terrain.coor_x,0,2,0);
            put(fblatex,";");
            put(fblatex,coin_blas_terrain.coor_y,0,2,0);
            put(fblatex,sep);
            police:=tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser-NS;
            j:=info_font(police).code'last;
            while info_font(police).code(j)=' ' loop
              j:=j-1;            
            end loop;
            put(fblatex,info_font(police).code(1..j));
			put(fblatex,sep);
			put(fblatex,"1");
            new_line(fblatex);
          else
            put(fblaima,tbnomi(no).id,0);
            put(fblaima,sep);
            noser:=0;
            compt:=1;
            while compt<=tbnomi(no).capict(1)(i).style loop
              noser:=noser+1;
              compt:=compt+DefinSeries(noser).nbla;
            end loop;            
            L:=integer'image(noser)'length;
            put(fblaima,DefinSeries(noser).nom(L+2..DefinSeries(noser).lnom));
            put(fblaima,sep);
            L:=integer'image(AbsoRela(tbnomi(no).capict(1)(i).style))'length;          
            put(fblaima,DefinBlasons(tbnomi(no).capict(1)(i).style).nom(L+2..DefinBlasons(tbnomi(no).capict(1)(i).style).lnom));
            put(fblaima,sep);
            put(fblaima,(coin_blas_terrain.coor_x+coin_opp_blas_terrain.coor_x)/2.0,0,2,0);
            put(fblaima,";");
            put(fblaima,(coin_blas_terrain.coor_y+coin_opp_blas_terrain.coor_y)/2.0,0,2,0);
			put(fblaima,sep);
			put(fblaima,"1");
            new_line(fblaima);
          end if;                
        end loop;

      else
        put(f,"2;5;");
        coin1_terrain:=Lecture.Convert_coord_graphe_terrain(tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle,resolution,Minimum_terrain);
        coin_opp:=(tp(no,tbnomi(no).p_choisie).coin1(k).coor_x+tbnomi(no).ln1,tp(no,tbnomi(no).p_choisie).coin1(k).coor_y+tbnomi(no).corps);
        coin_opp_terrain:=Lecture.Convert_coord_graphe_terrain(coin_opp,InvEchelle,resolution,Minimum_terrain);
        put(f,coin1_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin1_terrain.coor_y,0,2,0);

        put_line(f2,integer'image(code));
        put_line(f2,"LIGNE");
        put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(f2,integer'image(n));
        put(f2,coin1_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin1_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin1_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin1_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin1_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin1_terrain.coor_y,0,2,0);
        new_line(f2);      
          
        put(f, ";5;");
        coin2_terrain:=Lecture.Convert_coord_graphe_terrain(
                             tp(no,tbnomi(no).p_choisie).coin2,
                             InvEchelle, resolution, Minimum_terrain);
        coin_opp:=(tp(no,tbnomi(no).p_choisie).coin2.coor_x+tbnomi(no).ln2,
                   tp(no,tbnomi(no).p_choisie).coin2.coor_y+tbnomi(no).corps);
        coin_opp_terrain:=Lecture.Convert_coord_graphe_terrain(
                             coin_opp, InvEchelle, resolution, Minimum_terrain);
        put(f,coin2_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin_opp_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_y,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_x,0,2,0);
        put(f,";");
        put(f,coin2_terrain.coor_y,0,2,0);

        put_line(f2,integer'image(code));
        put_line(f2,"LIGNE");
        put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(f2,integer'image(n));
        put(f2,coin2_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin2_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin2_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin2_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin2_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin2_terrain.coor_y,0,2,0);
        new_line(f2);
        

        for i in 1..tbnomi(no).npic loop
          if tbnomi(no).capict(tp(no,tbnomi(no).p_choisie).nl)(i).y>0 then
            coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
              +tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle,resolution,Minimum_terrain);
            coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
              +(tbnomi(no).capict(2)(i).dx,tbnomi(no).capict(2)(i).dy)
              +tp(no,tbnomi(no).p_choisie).coin1(k),InvEchelle,resolution,Minimum_terrain);
          else
            coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
              +tp(no,tbnomi(no).p_choisie).coin2,InvEchelle,resolution,Minimum_terrain);
            coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
              ((tbnomi(no).capict(2)(i).x,tbnomi(no).capict(2)(i).y)
              +(tbnomi(no).capict(2)(i).dx,tbnomi(no).capict(2)(i).dy)
              +tp(no,tbnomi(no).p_choisie).coin2,InvEchelle,resolution,Minimum_terrain);
          end if;

          -- ecriture dans le fichier viewer de l'emprise des blasons, texte sur 2 lignes
          put_line(f2,integer'image(code));
          put_line(f2,"LIGNE");
          put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(f2,integer'image(n));
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
                
          -- ecriture dans les fichiers de sortie de blasons, texte sur 2 lignes
          if tbnomi(no).CAPSer(tbnomi(no).capict(2)(i).ser).iser>NS then
            put(fblatex,tbnomi(no).id,0);
            put(fblatex,sep);
            put(fblatex,tbnomi(no).capict(2)(i).str(1..tbnomi(no).capict(1)(i).lstr));
            put(fblatex,sep);
            put(fblatex,coin_blas_terrain.coor_x,0,2,0);
            put(fblatex,";");
            put(fblatex,coin_blas_terrain.coor_y,0,2,0);
            put(fblatex,sep);
            police:=tbnomi(no).CAPSer(tbnomi(no).capict(2)(i).ser).iser-NS;
            j:=info_font(police).code'last;
            while info_font(police).code(j)=' ' loop
              j:=j-1;            
            end loop;
            put(fblatex,info_font(police).code(1..j));
			put(fblatex,sep);
			put(fblatex,"1");
            new_line(fblatex);
          else
            put(fblaima,tbnomi(no).id,0);
            put(fblaima,sep);
            noser:=0;
            compt:=1;
            while compt<=tbnomi(no).capict(2)(i).style loop
              noser:=noser+1;
              compt:=compt+DefinSeries(noser).nbla;
            end loop;            
            L:=integer'image(noser)'length;
            put(fblaima,DefinSeries(noser).nom(L+2..DefinSeries(noser).lnom));
            put(fblaima,sep);
            L:=integer'image(AbsoRela(tbnomi(no).capict(2)(i).style))'length;          
            put(fblaima,DefinBlasons(tbnomi(no).capict(2)(i).style).nom(L+2..DefinBlasons(tbnomi(no).capict(2)(i).style).lnom));
            put(fblaima,sep);
            put(fblaima,(coin_blas_terrain.coor_x+coin_opp_blas_terrain.coor_x)/2.0,0,2,0);
            put(fblaima,";");
            put(fblaima,(coin_blas_terrain.coor_y+coin_opp_blas_terrain.coor_y)/2.0,0,2,0);
			put(fblaima,sep);
			put(fblaima,"1");
            new_line(fblaima);
          end if;
        end loop;
      end if;
	  if sep=';' then
        put(f,"""");
	  end if;
      new_line(f);
      end loop;
    end if;
  end loop;
  close(f);

  if boite=false then
    Delete_File(nomfichier(1..ncar_fo+14));
  end if;
    
end Sauve_boites_placees;


--===================================================================================
-- Fichier de sortie au format texte des boites des ecritures bloquees + sortie viewer--
--===================================================================================
procedure sauve_boites_bloquees (tbnomi : in typtbnomi;
                                    tnp : in typtnp;  --nb de positions
		   	   						InvEchelle : positive;
               						Resolution : positive;
               						Minimum_terrain : Point_type_reel;
                                    f2 : in out text_io.file_type;
                                    fblatex : in out text_io.file_type;
                                    fblaima : in out text_io.file_type) is
  
fbloc :  text_io.file_type;
coin : point_type:=(0,0);
coin_terrain : point_type_reel:=(0.0,0.0);
coin_opp : point_type:=(0,0);
coin_opp_terrain : point_type_reel:=(0.0,0.0);
nomfichier : string120;
code : integer:=10;
couleur : integer:=255; -- rouge
epaisseur : integer:=1;
styligne : integer range 0..4:=0; -- continue
hache : integer range 0..7; -- style de hachure des polygones
n : integer:=4; -- nombre de sommets d'une boite
coin_blas_terrain,coin_opp_blas_terrain : point_type_reel;
compt,noser : integer;
info_font : typtfont(1..500);
nbfont : integer;
policename : string(1..32);
police : positive; -- indice de police d'un blason texte
j,k : integer;
tb : nom_type;
ln : integer;
L : integer;

use type Interfaces.C.Char_Array;
subtype C_String is Interfaces.C.Char_Array;
use type Win32.Bool; 

function StringZ(S : string) return C_String is
SZ : C_String(Interfaces.C.Size_t(S'first)..Interfaces.C.Size_T(S'last+1));
begin
  for I in S'Range loop
    SZ(Interfaces.C.Size_T(I)):=Win32.Char'Val(Character'Pos(S(I)));
  end loop;
  SZ(SZ'LAST):=Win32.Char'val(Character'Pos(ASCII.NUL));
  return SZ;
end StringZ;

procedure Delete_file(File_Name : string) is
File_Name_Z: aliased C_String (Interfaces.C.Size_t(File_Name'first)..Interfaces.C.Size_t(File_Name'last+1)):= StringZ(File_Name);
begin
  if Win32.Winbase.Deletefile(File_Name_Z(1)'Unchecked_Access)=0 then
  -- gb.MsgBox("Erreur � l'effacement des fichiers de param�tres de visualisation");
  n:=n;
  end if;
end Delete_File;

-- passage d'absolu en relatif d'un numero de blason 
function AbsoRela (abso : in positive) return positive is
serie : positive:=1;
abso2 : positive:=(DefinSeries(1).nbla mod 100)+1;
begin
  while abso2<=abso loop
    serie:=serie+1;
    abso2:=abso2+(DefinSeries(serie).nbla mod 100);
  end loop;
  abso2:=abso2-DefinSeries(serie).nbla;
  return(abso-abso2+1);
end;  
  
begin
  nomfichier(1..ncar_fo-4):=nomfichier_objet(1..ncar_fo-4);
  nomfichier(ncar_fo-3..ncar_fo+19):="_boites_non_placees.txt";
  create(fbloc,out_file,nomfichier(1..ncar_fo+19));

  charge_fontes(info_font,nbfont);  

  for no in 1..tbnomi'last loop
    if tbnomi(no).p_choisie=0 then
		
	  -- debut gestion du caract�re '%' pour kilometrages
	  l:=tbnomi(no).ncar(1);
	  for K in 2..tbnomi(no).ncar(1)-1 loop
        if tbnomi(no).chaine(1)(K)='%' then
		  l:=k-1;
		  exit;
        end if;
      end loop;
      -- fin gestion du caract�re '%' pour kilometrages

	  if L=tbnomi(no).ncar(1) then
        put(fbloc,tbnomi(no).id,0);
      else
        put(fbloc,tbnomi(no).chaine(1)(L+2..tbnomi(no).ncar(1)));
      end if;
	  put(fbloc,sep);
      if sep=';' then
        put(fbloc,"""");
      end if;
      for i in 1..tbnomi(no).nb_mots loop
	  	if i=1 then
		  put(fbloc,tbnomi(no).chaine(I)(1..L));
        else
		  put(fbloc," ");
		  put(fbloc,tbnomi(no).chaine(I)(1..tbnomi(no).ncar(i)));
        end if;
      end loop;
      if sep=';' then
		put(fbloc,"""");
      end if;
      put(fbloc,sep);
      if sep=';' then
		put(fbloc,"""");
      end if;
	  ln:=0;
      for i in 1..tbnomi(no).nb_mots loop
        ln:=ln+tbnomi(no).ln(i);
      end loop;
--      coin:=(tbnomi(no).coord_objet.coor_x-tbnomi(no).ln1/2,tbnomi(no).coord_objet.coor_y-tbnomi(no).corps/2); 
      coin:=(tbnomi(no).coord_objet.coor_x-ln/2,tbnomi(no).coord_objet.coor_y-tbnomi(no).corps/2);
      coin_terrain:=Lecture.Convert_coord_graphe_terrain(coin, InvEchelle, resolution, Minimum_terrain);                   
--      coin_opp:=(tbnomi(no).coord_objet.coor_x+tbnomi(no).ln1/2,tbnomi(no).coord_objet.coor_y+tbnomi(no).corps/2); 
      coin_opp:=(tbnomi(no).coord_objet.coor_x+ln/2,tbnomi(no).coord_objet.coor_y+tbnomi(no).corps/2); 
      coin_opp_terrain:=Lecture.Convert_coord_graphe_terrain(coin_opp, InvEchelle, resolution, Minimum_terrain);
      put(fbloc,"1;5;");
      put(fbloc,coin_terrain.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_terrain.coor_y,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_terrain.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_opp_terrain.coor_y,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_opp_terrain.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_opp_terrain.coor_y,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_opp_terrain.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_terrain.coor_y,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_terrain.coor_x,0,2,0);
      put(fbloc,";");
      put(fbloc,coin_terrain.coor_y,0,2,0);
      if sep=';' then
		put(fbloc,"""");
      end if;
      new_line(fbloc);

      if tNP(no)=0 then
	    if tbnomi(no).topo.objet_type/=lineaire then
		  case tbnomi(no).cause is
		    when hors_image => -- hors image
				               hache:=3; -- hachures diagonal droite
		    when ambigu => -- ambigu�t�
				           hache:=0; -- hachures horizontales
            when others => -- mutilation
			               hache:=4; -- hachures verticales et horizontales
		  end case;
        else
          tb:=tbnomi(no);
		  case tbnomi(no).cause is
            when mutilation => -- mutilation
                               hache:=4; -- hachures verticales et horizontales
            when bord_image => -- bord image
                               hache:=2; -- hachures diagonal gauche
            when Hors_image => -- hors image
							   hache:=3; -- hachures diagonal droite 
            when MetaCourt => --court ou horizontal
                              hache:=1; -- hachures verticales
		    when ambigu => -- ambigu�t�
				           hache:=0; -- hachures horizontales
            when Agencement => -- agencement
                           hache:=0; -- hachures horizontales
            when others => -- sinueux
                           hache:=5; -- hachures diagonal gauche et diagonal droite
          end case;
        end if;
      else
		case tbnomi(no).cause is
		  when MNT_Plat => -- orientation
		  	               hache:=1; -- hachures verticales (+ hachurage diagonal droite)
		  when Direction_G => -- orientation
		  	                  hache:=1; -- hachures verticales (+ hachurage diagonal droite)
		  when Sommet_Plat => -- orientation
		  	                  hache:=1; -- hachures verticales (+ hachurage diagonal droite)
          when others => -- placement
                         hache:=6; -- pas de hachures
		end case;
      end if;

      << debut >>
      put_line(f2,integer'image(code));
      if hache=6 then 
	    put_line(f2,"LIGNE");
        put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
        put_line(f2,integer'image(n+1));
      else
        put_line(f2,"POLYGONE");
        put_line(f2,integer'image(epaisseur)&tab&integer'image(hache)&tab&integer'image(couleur));
        put_line(f2,integer'image(n));
      end if;
      put(f2,coin_terrain.coor_x,0,2,0);
      put(f2,tab);
      put(f2,coin_terrain.coor_y,0,2,0);
      new_line(f2);      
      put(f2,coin_terrain.coor_x,0,2,0);
      put(f2,tab);
      put(f2,coin_opp_terrain.coor_y,0,2,0);
      new_line(f2);      
      put(f2,coin_opp_terrain.coor_x,0,2,0);
      put(f2,tab);
      put(f2,coin_opp_terrain.coor_y,0,2,0);
      new_line(f2);      
      put(f2,coin_opp_terrain.coor_x,0,2,0);
      put(f2,tab);
      put(f2,coin_terrain.coor_y,0,2,0);
      new_line(f2);      
      if hache=6 then 
        put(f2,coin_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_terrain.coor_y,0,2,0);
        new_line(f2);
      end if;

      for i in 1..tbnomi(no).npic loop
        coin_blas_terrain:=Lecture.Convert_coord_graphe_terrain
          ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
          +coin,InvEchelle,resolution,Minimum_terrain);
        coin_opp_blas_terrain:=Lecture.Convert_coord_graphe_terrain
          ((tbnomi(no).capict(1)(i).x,tbnomi(no).capict(1)(i).y)
          +(tbnomi(no).capict(1)(i).dx,tbnomi(no).capict(1)(i).dy)
          +coin,InvEchelle,resolution,Minimum_terrain);

        -- ecriture des emprises de blasons dans le fichier viewer donnees de sortie
        put_line(f2,integer'image(code));
        if hache=6 then
		  put_line(f2,"LIGNE");
          put_line(f2,integer'image(epaisseur)&tab&integer'image(styligne)&tab&integer'image(couleur));
          put_line(f2,integer'image(n+1));
        else
		  put_line(f2,"POLYGONE");
          put_line(f2,integer'image(epaisseur)&tab&integer'image(hache)&tab&integer'image(couleur));
          put_line(f2,integer'image(n));
        end if;
        put(f2,coin_blas_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_blas_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_blas_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_opp_blas_terrain.coor_y,0,2,0);
        new_line(f2);      
        put(f2,coin_opp_blas_terrain.coor_x,0,2,0);
        put(f2,tab);
        put(f2,coin_blas_terrain.coor_y,0,2,0);
        new_line(f2);
		if hache=6 then
          put(f2,coin_blas_terrain.coor_x,0,2,0);
          put(f2,tab);
          put(f2,coin_blas_terrain.coor_y,0,2,0);
          new_line(f2);      
        end if;

        -- ecriture dans les fichiers de sortie de blasons
        if tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser>NS then
          put(fblatex,tbnomi(no).id,0);
          put(fblatex,sep);
          put(fblatex,tbnomi(no).capict(1)(i).str(1..tbnomi(no).capict(1)(i).lstr));
          put(fblatex,sep);
          put(fblatex,coin_blas_terrain.coor_x,0,2,0);
          put(fblatex,";");
          put(fblatex,coin_blas_terrain.coor_y,0,2,0);
          put(fblatex,sep);
          police:=tbnomi(no).CAPSer(tbnomi(no).capict(1)(i).ser).iser-NS;
          j:=info_font(police).code'last;
          while info_font(police).code(j)=' ' loop
            j:=j-1;            
          end loop;
          put(fblatex,info_font(police).code(1..j));
		  put(fblatex,sep);
          put(fblatex,"0");
          new_line(fblatex);
        else
          put(fblaima,tbnomi(no).id,0);
          put(fblaima,sep);
          noser:=0;
          compt:=1;
          while compt<=tbnomi(no).capict(1)(i).style loop
            noser:=noser+1;
            compt:=compt+DefinSeries(noser).nbla;
          end loop;
          k:=integer'image(noser)'length;
          put(fblaima,DefinSeries(noser).nom(k+2..DefinSeries(noser).lnom));
          put(fblaima,sep);
          k:=integer'image(AbsoRela(tbnomi(no).capict(1)(i).style))'length;          
          put(fblaima,DefinBlasons(tbnomi(no).capict(1)(i).style).nom(k+2..DefinBlasons(tbnomi(no).capict(1)(i).style).lnom));
          put(fblaima,sep);
          put(fblaima,(coin_blas_terrain.coor_x+coin_opp_blas_terrain.coor_x)/2.0,0,2,0);
          put(fblaima,";");
          put(fblaima,(coin_blas_terrain.coor_y+coin_opp_blas_terrain.coor_y)/2.0,0,2,0);
		  put(fblaima,sep);
		  put(fblaima,"0");
          new_line(fblaima);
        end if;                
      end loop;

      if (tbnomi(no).cause=MNT_Plat or tbnomi(no).cause=Direction_G or tbnomi(no).cause=Sommet_Plat) and hache=1 then
		hache:=3;
        goto debut;
      end if;
    end if;
  end loop;
  close(fbloc);
  if boite=false then
    Delete_File(nomfichier(1..ncar_fo+15));
  end if;
end Sauve_boites_bloquees;


--======================================================================
End toponymio;
