with GEOMETRIE; use GEOMETRIE;
With ada.numerics.elementary_functions; use ada.numerics.elementary_functions;
with manipboite; use manipboite;
with math_int_basic; use math_int_basic;
with Win32.winuser; use Win32.winuser;
with Win32.winnt; use Win32.winnt;
with Win32.windef; use Win32.windef;
with Win32.wingdi; use Win32.wingdi;
with system;
with text_io; use text_io;
with Ada.Characters.Handling; use Ada.Characters.Handling;
with Ada.Unchecked_Conversion;
with substitution_io; use substitution_io;
with tiff_io; use tiff_io;
with gb;
with detpos; use detpos;
with inipan; use inipan;
with lecture; use lecture;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with dialog3;
with dialog1;
with ada.numerics.discrete_random;
with Morpholigne; use Morpholigne;
with Lissage_filtrage; use Lissage_filtrage;
-- with polaire_fl; use polaire_fl;
with math_int_basic; use math_int_basic;

package body NoRou2 is
	

PROCEDURE NOEUDS_IMPORTANTS(graphe : Graphe_type;
                            T_ARC_A : TGROUPEMENT;
                            C_T : integer;
                            TAB_NOEUDS : out Point_liste_type;
                            CRS_NOEUDS : in out integer) is
Nd		: integer;
type_arc	: arc_type;
type_noeud	: noeud_type;
nini,nfin 	: integer;
Begin
    
-----------------------------------------------------------------------------
--Si il n'y a que deux points maximum (soit un seul arc)---------------------
if C_t<3 then 
  type_arc:=gr_arc(graphe,abs(T_ARC_A(1)));
  TAB_NOEUDS(1):=gr_noeud(graphe,type_arc.noeud_ini).coords;
  TAB_NOEUDS(2):=gr_noeud(graphe,type_arc.noeud_fin).coords;
  crs_noeuds:=2;
else  
  crs_noeuds:=0;
  for i in 1..C_t-1 loop
    if T_arc_a(i)/=T_Arc_a(i+1) then
      geometrie.noeud_commun(graphe,abs(T_ARC_A(i)),abs(T_ARC_A(i+1)),nini,nfin);
      if nini/=0 then
        nd:=nini;
      else
        nd:=nfin;
      end if;

-----------------------------------------------------------------------------
--Gestion du premier noeud---------------------------------------------------
      if crs_noeuds=0 then
        crs_noeuds:=crs_noeuds+1;
        type_arc:=gr_arc(graphe,abs(T_ARC_A(i)));
        if type_arc.noeud_fin=nd then
	     TAB_NOEUDS(crs_noeuds):=gr_noeud(graphe,type_arc.noeud_ini).coords;
        else
	     TAB_NOEUDS(crs_noeuds):=gr_noeud(graphe,type_arc.noeud_fin).coords;

        end if;
      end if;

-----------------------------------------------------------------------------
--Gestion des autres noeuds---------------------------------------------------
      type_noeud:=gr_noeud(graphe,nd);
      if type_noeud.nombre_arc>2 then
        crs_noeuds:=crs_noeuds+1;	
        TAB_NOEUDS(crs_noeuds):=type_noeud.coords;
      end if;
    end if;
  end loop;

-----------------------------------------------------------------------------
--Gestion du dernier noeud---------------------------------------------------
  if crs_noeuds/=0 then--cas ou plus de trois points mais du meme arc--------- 
    crs_noeuds:=crs_noeuds+1;
    type_arc:=gr_arc(graphe,abs(T_ARC_A(c_t)));
    if type_arc.noeud_fin=nd then
      TAB_NOEUDS(crs_noeuds):=gr_noeud(graphe,type_arc.noeud_ini).coords;
    else
      TAB_NOEUDS(crs_noeuds):=gr_noeud(graphe,type_Arc.noeud_fin).coords;
    end if;
  else
    type_arc:=gr_arc(graphe,abs(T_ARC_A(1)));
    TAB_NOEUDS(1):=gr_noeud(graphe,type_arc.noeud_ini).coords;
    TAB_NOEUDS(2):=gr_noeud(graphe,type_arc.noeud_fin).coords;
    crs_noeuds:=2;
  end if;
end if;

end NOEUDS_IMPORTANTS;

Procedure DEPLACE(x : in out integer; Y : in out integer; an : float; dx : float; dy : float) is
begin
  x:=integer(float(x)+dx*cos(an)-dy*sin(an));
  y:=integer(float(y)+dx*sin(an)+dy*cos(an));
end DEPLACE;

--==========================================================================
-- Procedure qui determine le poids mutile par une boite oblique ( pour le 
-- lineaire) et renvoie un flag dans le cas ou un pixel interdit est ecrase:
--==========================================================================
Procedure POIMUB_oblique(tpoi : typtipoi;
                         min : in point_type;
                         coin1 : in point_type;
                         angle : in float;    		         
                         emprise : in integer;
                         hauteur : in integer;
                         z : in integer;
                         ecrase  : out boolean;
						 bord_image : out boolean;
                         poim : out typpoi;
						 mode_P : in Typcateg;
						 resolution : in positive) is

poipix : typpoi:=0.0;
poim_temp : typpoi:=0.0;
npix : natural;
boite : manipboite.bloc_type;
x1,y1,x2,y2,x3,y3,x4,y4,l_ini,l_fin,c_ini,c_fin : integer:=0;
-- angle_calcul : float:=angle;
dilx,dily : float;
P,p1,p2,p3,p4 : point_type;
Cpt9 : integer:=0; -- compteur de pixels d'indice 9 recouverts, pour les cotes de courbe
-- max9_carre : integer; -- valeur max tol�r�e de Cpt9
MinD9 : float:=1.6; -- valeur min tol�r�e sur la distance en pixel entre un pixel d'indice 9 recouvert et les grands c�t�s du rectangle anglobant une cote de courbe
TanAngle : float:=tan(angle);
YPix : integer;

  begin
	
    ecrase:=false;
    bord_image:=false;

    Manipboite.Quatre_coins(coin1,angle,emprise,hauteur,boite);
    x1:=boite.p1.coor_x; y1:=boite.p1.coor_y;
    x2:=boite.p2.coor_x; y2:=boite.p2.coor_y;
    x3:=boite.p3.coor_x; y3:=boite.p3.coor_y;
    x4:=boite.p4.coor_x; y4:=boite.p4.coor_y;

    dilx:=float(hauteur)*DIST_ROUTE;
    dily:=float(hauteur)*DIST_ROUTE;
    if z=1 then  -- mode DESAXE
      DEPLACE(x1,y1,angle,-dilx,0.0);
      DEPLACE(x2,y2,angle,-dilx,dily);
      DEPLACE(x3,y3,angle,dilx,dily);
      DEPLACE(x4,y4,angle,dilx,0.0);
    elsif z=2 then
      DEPLACE(x1,y1,angle,-dilx,-dily);
      DEPLACE(x2,y2,angle,-dilx,0.0);
      DEPLACE(x3,y3,angle,dilx,0.0);
      DEPLACE(x4,y4,angle,dilx,-dily);

    elsif z=0 then  -- mode AXE ou COURBE ou CAXE ou CDESAXE
      --      dilx:=float(hauteur)*DIST_ROUTE/2.0;
      --      dily:=float(hauteur)*DIST_ROUTE;

	  --- dilatation specifiques aux cotes de courbes
      if mode_P=courbe then
        dilX:=0.5*float(Resolution); -- 0.5 mm carte
        dilY:=0.2*float(Resolution); -- 0.2 mm carte

        DEPLACE(x1,y1,angle,-dilx,-dily);
        DEPLACE(x2,y2,angle,-dilx,dily);
        DEPLACE(x3,y3,angle,dilx,dily);
        DEPLACE(x4,y4,angle,dilx,-dily);
      end if;
      if mode_P=AXE then
        dilx:=0.4*float(hauteur);
        dily:=0.05*float(hauteur);
        DEPLACE(x1,y1,angle,-dilx,-dily);
        DEPLACE(x2,y2,angle,-dilx,dily);
        DEPLACE(x3,y3,angle,dilx,dily);
        DEPLACE(x4,y4,angle,dilx,-dily);
	  end if;
	  if Mode_P=CDesaxe then  -- Dilatation pour les rivieres
        dilX:=0.3*float(Hauteur);
        dilY:=0.2*DIST_HYDRO*float(Hauteur);
		-- dilY:=0.5*float(Hauteur);
        DEPLACE(x1,y1,angle,-dilx,-Dily);
        DEPLACE(x2,y2,angle,-dilx,dily);
        DEPLACE(x3,y3,angle,dilx,dily);
        DEPLACE(x4,y4,angle,dilx,-dily);
      end if;
	  if Mode_P=CAxe then  -- Dilatation pour les fleuves
        dilX:=0.3*float(Hauteur);
        dilY:=0.1*float(Hauteur);
        DEPLACE(x1,y1,angle,-dilx,-dily);
        DEPLACE(x2,y2,angle,-dilx,dily);
        DEPLACE(x3,y3,angle,dilx,dily);
        DEPLACE(x4,y4,angle,dilx,-dily);	  	
      end if;
    end if;


-------------------------------------------------------------------------------

--Verifie que le rectangle ne deborde pas--------------------------------------
--max.coor-1 a cause de l'arrondi----------------------------------------------
	
    -- Passage en coordonnees image:
    x1:=(x1-min.coor_x)/pt_image; y1:=(y1-min.coor_y)/pt_image;
    x2:=(x2-min.coor_x)/pt_image; y2:=(y2-min.coor_y)/pt_image;
    x3:=(x3-min.coor_x)/pt_image; y3:=(y3-min.coor_y)/pt_image;
    x4:=(x4-min.coor_x)/pt_image; y4:=(y4-min.coor_y)/pt_image;

    P1:=(X1,y1);
    P2:=(X2,y2);
    P3:=(X3,y3);
    P4:=(X4,y4);

	-- Max9_carre:=4*distance_de_points(P2,P3);
	-- Max9_carre:=(40*sqrt(distance_de_points(P2,P3))*float(Resolution_Image)/float(Resolution))
	--           *(40*sqrt(distance_de_points(P2,P3))*float(Resolution_Image)/float(Resolution));

    if X1<tpoi'first(1) or X1>tpoi'last(1) or X2<tpoi'first(1) or X2>tpoi'last(1) or X3<tpoi'first(1) or X3>tpoi'last(1) or X4<tpoi'first(1) or X4>tpoi'last(1)
    or Y1<tpoi'first(2) or Y1>tpoi'last(2) or Y2<tpoi'first(2) or Y2>tpoi'last(2) or Y3<tpoi'first(2) or Y3>tpoi'last(2) or Y4<tpoi'first(2) or Y4>tpoi'last(2) then
      bord_image:=true;
	  poim:=0.0;
	  return;
	  -- goto finPoimub;
    end if;

    -- Calcul du nombre de pixels recouverts:
    npix:=0;
    If Angle = 0.0 Then
      for ypix in y1..y2 loop
	c_ini:=x1;
	c_fin:=x4; 
	for xpix in c_ini..c_fin loop
 	   npix:=npix+1;
	end loop;
      end loop;
    elsif Angle = PI/2.0 then
      for ypix in y1..y4 loop
	c_ini:=x3; 
	c_fin:=x4; 
	for xpix in c_ini..c_fin loop
 	   npix:=npix+1;
	end loop;
      end loop;
    elsif Angle = (-PI)/2.0 then
      for ypix in y4..y1 loop
	c_ini:=x4; 
	c_fin:=x3; 
	for xpix in c_ini..c_fin loop
 	   npix:=npix+1;
	end loop;
      end loop;
    elsif Angle > 0.0 then
      for ypix in y1..y3 loop
	if (y1<=ypix) and (ypix<=y2) then
	   c_ini:=Ent(float(x1)-float(ypix-y1)*tanAngle); 
	end if;
	if (y2<=ypix) and (ypix<=y3) then
	   c_ini:=Ent(float(x2)+float(ypix-y2)/tanAngle); 
	end if;
	if (y1<=ypix) and (ypix<=y4) then
	   c_fin:=Ent(float(x1)+float(ypix-y1)/tanAngle)+1; 
	end if;
	if (y4<=ypix) and (ypix<=y3) then
	   c_fin:=Ent(float(x4)-float(ypix-y4)*tanAngle)+1; 
	end if;
	for xpix in c_ini..c_fin loop
 	   npix:=npix+1;
	end loop;
      end loop;
    elsif Angle < 0.0 then
      for ypix in y4..y2 loop
	if (y4<=ypix) and (ypix<=y1) then
	   c_ini:=Ent(float(x4)+float(ypix-y4)/tanAngle); 
	end if;
	if (y1<=ypix) and (ypix<=y2) then
	   c_ini:=Ent(float(x1)-float(ypix-y1)*tanAngle); 
	end if;
	if (y4<=ypix) and (ypix<=y3) then
	   c_fin:=Ent(float(x4)-float(ypix-y4)*tanAngle)+1; 
	end if;
	if (y3<=ypix) and (ypix<=y2) then
	   c_fin:=Ent(float(x3)+float(ypix-y3)/tanAngle)+1; 
	end if;
	for xpix in c_ini..c_fin loop
 	   npix:=npix+1;
	end loop;
      end loop;
    end if;

    -- Calcul du poids de mutilation de l'image par la boite:
    If Angle = 0.0 Then
      for ypixA in y1..y2 loop
	    if (YPixA mod 2)=0 then
		  Ypix:=Y1+integer(float(YpixA-Y1)/2.0);
        else
		  Ypix:=Y2-integer(float(YpixA-Y1)/2.0);
        end if;
	    c_ini:=x1; 
	    c_fin:=x4; 
	    for xpix in c_ini..c_fin loop
          if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
	 	    ecrase:=true;
		    poim:=poim_temp;
		    return;
		    -- goto finPoimub;
          -- elsif tpoi(xpix,ypix)=9 and (mode_p=cOURBE or mode_p=cAXE)then
          elsif tpoi(xpix,ypix)=9 and mode_p=cOURBE then
		  	-- cpt9:=cpt9+1;
		    if (Ypix=Y1) or (Ypix=Y2) then -- or (Cpt9*cpt9>Max9_carre) then
              ecrase:=true;
		      poim:=poim_temp;
		      return;
			  -- goto finPoimub;
            end if;
          else  
		    poipix:=float(tpoi(xpix,ypix))/float(npix);
		    poim_temp:=poim_temp+poipix;
	      end if;
	    end loop;
      end loop;
    elsif Angle = PI/2.0 then
      for ypixA in y1..y4 loop
	    if (YPixA mod 2)=0 then
		  Ypix:=Y1+integer(float(YpixA-Y1)/2.0);
        else
		  Ypix:=Y4-integer(float(YpixA-Y1)/2.0);
        end if;
	    c_ini:=x3; 
	    c_fin:=x4; 
	    for xpix in c_ini..c_fin loop
          if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
	        ecrase:=true;
	 	    poim:=poim_temp;
		    return;
		    -- goto finPoimub;
          -- elsif tpoi(xpix,ypix)=9 and (mode_p=cOURBE or mode_p=cAXE) then
          elsif tpoi(xpix,ypix)=9 and mode_p=cOURBE then
		  	-- cpt9:=cpt9+1;
		    if (Xpix=C_ini) or (Xpix=C_fin) then -- or (Cpt9*cpt9>Max9_carre) then
              ecrase:=true;
		      poim:=poim_temp;
		      return;
			  -- goto finPoimub;
            end if;
	      else
		    poipix:=float(tpoi(xpix,ypix))/float(npix);
		    poim_temp:=poim_temp+poipix;
	      end if;
	    end loop;
      end loop;
    elsif Angle = (-PI)/2.0 then
      for ypixA in y4..y1 loop
	    if (YPixA mod 2)=0 then
		  Ypix:=Y4+integer(float(YpixA-Y4)/2.0);
        else
		  Ypix:=Y1-integer(float(YpixA-Y4)/2.0);
        end if;
	    c_ini:=x4; 
	    c_fin:=x3; 
	    for xpix in c_ini..c_fin loop
          if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
	 	    ecrase:=true;
		    poim:=poim_temp;
		    return;
		    -- goto finPoimub;
          -- elsif tpoi(xpix,ypix)=9 and (mode_p=cOURBE or mode_p=cAXE) then
          elsif tpoi(xpix,ypix)=9 and mode_p=cOURBE then
		  	-- cpt9:=cpt9+1;
		    if (Xpix=C_ini) or (Xpix=C_fin) then -- or (Cpt9*cpt9>Max9_carre) then
              ecrase:=true;
		      poim:=poim_temp;
		      return;
			  -- goto finPoimub;
            end if;
	      else
		    poipix:=float(tpoi(xpix,ypix))/float(npix);
		    poim_temp:=poim_temp+poipix;
	      end if;
	    end loop;
      end loop;
    elsif Angle > 0.0 then
      for ypixA in y1..y3 loop
		if (YPixA mod 2)=0 then
		  Ypix:=Y1+integer(float(YpixA-Y1)/2.0);
        else
		  Ypix:=Y3-integer(float(YpixA-Y1)/2.0);
        end if;
	    if (y1<=ypix) and (ypix<=y2) then
	      c_ini:=Ent(float(x1)-float(ypix-y1)*tanAngle); 
	    end if;
	    if (y2<=ypix) and (ypix<=y3) then
	      c_ini:=Ent(float(x2)+float(ypix-y2)/tanAngle); 
	    end if;
	    if (y1<=ypix) and (ypix<=y4) then
	      c_fin:=Ent(float(x1)+float(ypix-y1)/tanAngle)+1; 
	    end if;
	    if (y4<=ypix) and (ypix<=y3) then
	      c_fin:=Ent(float(x4)-float(ypix-y4)*tanAngle)+1; 
	    end if;

	    if C_ini<tpoi'first(1) or C_fin>tpoi'last(1) then
          bord_image:=true;
	      poim:=0.0;
	      return;
		  -- goto finPoimub;
        end if;

	    for xpix in c_ini..c_fin loop
          if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
	     	ecrase:=true;
		    poim:=poim_temp;
		    return;
		    -- goto finPoimub;
          -- elsif tpoi(xpix,ypix)=9 and (mode_p=cOURBE or mode_p=cAXE) then
          elsif (tpoi(xpix,ypix)=9 and mode_p=cOURBE) then
		  	-- cpt9:=cpt9+1;
		  	P:=(Xpix,Ypix);
            if math_int_basic.min(distance_a_ligne(P,p1,p4,C_segment_type),distance_a_ligne(p,p2,p3,C_segment_type))<MinD9 then -- or (Cpt9*cpt9>Max9_carre) then
			  ecrase:=true;
		      poim:=poim_temp;
		      return;
			  -- goto finPoimub;
            end if;
	      else
		    poipix:=float(tpoi(xpix,ypix))/float(npix);
		    poim_temp:=poim_temp+poipix;
	      end if;
	    end loop;
      end loop;
    elsif Angle < 0.0 then
      for ypixA in y4..y2 loop
		if (YPixA mod 2)=0 then
		  Ypix:=Y4+integer(float(YpixA-Y4)/2.0);
        else
		  Ypix:=Y2-integer(float(YpixA-Y4)/2.0);
        end if;
	    if (y4<=ypix) and (ypix<=y1) then
	      c_ini:=Ent(float(x4)+float(ypix-y4)/tanAngle); 
	    end if;
	    if (y1<=ypix) and (ypix<=y2) then
	      c_ini:=Ent(float(x1)-float(ypix-y1)*tanAngle); 
	    end if;
	    if (y4<=ypix) and (ypix<=y3) then
	      c_fin:=Ent(float(x4)-float(ypix-y4)*tanAngle)+1; 
	    end if;
	    if (y3<=ypix) and (ypix<=y2) then
	      c_fin:=Ent(float(x3)+float(ypix-y3)/tanAngle)+1; 
	    end if;

	    if C_ini<tpoi'first(1) or C_fin>tpoi'last(1) then
          bord_image:=true;
	      poim:=0.0;
	      return;
		  -- goto finPoimub;
        end if;

    	for xpix in c_ini..c_fin loop
	      if tpoi(xpix,ypix)=10 then -- Pixel interdit !!!
	     	ecrase:=true;
		    poim:=poim_temp;
		    return;
		    -- goto finPoimub;
          -- elsif tpoi(xpix,ypix)=9 and (mode_p=cOURBE or mode_p=cAXE) then
          elsif (tpoi(xpix,ypix)=9 and mode_p=cOURBE) then
		  	-- cpt9:=cpt9+1;
			P:=(Xpix,Ypix);
            if math_int_basic.min(distance_a_ligne(P,p1,p4,C_segment_type),distance_a_ligne(p,p2,p3,C_segment_type))<MinD9 then -- or (Cpt9*cpt9>Max9_carre) then
              ecrase:=true;
		      poim:=poim_temp;
		      return;
			  -- goto finPoimub;
            end if;
          else
	        poipix:=float(tpoi(xpix,ypix))/float(npix);
		    poim_temp:=poim_temp+poipix;
          end if;
	    end loop;
      end loop;
    end if;

    poim:=poim_temp;

--  << finPoimub >>
--  null;
end;


FUNCTION ZONE_INFLUENCE(Tableau : point_liste_type;
                        crs : positive;
                        Ecart : integer) return Boite_type is

ZIE : Boite_type;

Begin
  Zie.maximum:=norou.Max(tableau, crs);
  Zie.minimum:=norou.Min(tableau, crs);
  Zie.minimum.coor_x:=Zie.minimum.coor_x-ecart;
  Zie.minimum.coor_y:=Zie.minimum.coor_y-ecart;
  Zie.maximum.coor_x:=Zie.maximum.coor_x+ecart;
  Zie.maximum.coor_y:=Zie.maximum.coor_y+ecart;
  Return Zie;
end ZONE_INFLUENCE;

---------------------------------------------------------------------------
--RANGE_TOPO_DS_TAB********************************************************
--Range TOPO dans TAB dans l'ordre croissant des pds.----------------------
procedure RANGE_TOPO_DS_TAB(tab : in out POSITION; topo : position_type) is

i : integer:=1;
GeomStore : Point_access_type; 
NGeomStore : integer:=0;

begin                                       

  while (tab(i).poip<topo.poip) and (i<tab'last) loop
    i:=i+1;
  end loop;
  if tab(i).poip>=topo.poip then
    if i<tab'last then
      for j in reverse i..tab'last-1 loop	  	
	    GeomStore:=Tab(j+1).geom;
		NGeomStore:=Tab(j+1).NGeom(1);
        Tab(j+1):=Tab(j);
		Tab(j+1).geom:=geomStore;
        Tab(j+1).NGeom(1):=NGeomStore;
      end loop;
    end if;

    GeomStore:=tab(i).Geom;
	NGeomStore:=Tab(I).NGeom(1);
    tab(i):=topo;
	Tab(I).Geom:=GeomStore;
    Tab(I).NGeom(1):=NGeomStore;
  end if;
end RANGE_TOPO_DS_TAB;

PROCEDURE DETECTION_PORTION(Graphe : graphe_type;
                            Legende : legende_type;
                            T_ARC_A : TGROUPEMENT;
                            TAB : POINT_LISTE_TYPE;
                            C_TAB1 : integer;
                            C_TAB2 : integer;
                            P1 : POINT_TYPE;
                            P2 : POINT_TYPE;
                            Z : integer;
                            h_car : integer;
                            Hauteur : in out integer;
                            Pds_s : in out Float;
                            resolution : in positive;
							Le_Seuil_S : in float;
							DistRoute : in float) is

Hauteur_max,
Hauteur_min	: integer:=0;
Haut		: float:=0.0;
C_TAB		: integer:=C_TAB2-C_TAB1+1;
TAB_NV		: point_liste_type(1..C_TAB2-C_TAB1+2);
epaisseur	: float:=0.0;
epaisseur2	: float:=0.0;
P2b		: point_type;
x_deb		: integer;
x_fin		: integer;
C1		: POSITIVE;
C2		: POSITIVE;
TMP		: float;
angle_tmp	: float:=angle_horizontal(p1,p2);
monfloat : float;
Monint : integer;
poids : float:=0.0;


begin
---------------------------------------------------------------------------
--Passe les points dans le repere (p1,p2)----------------------------------
  for i in C_TAB1..C_TAB2 loop
    TAB_NV(i-C_TAB1+1):=Transforme_repere(TAB(i),P1,angle_tmp);
  end loop;
  P2b:=Transforme_repere(P2,P1,angle_tmp);
  C1:=1;

---------------------------------------------------------------------------
--recherche le segment qui correspond au debut du toponyme-----------------
  While (c1<C_TAB-1) and 	
	not((TAB_NV(C1).coor_x<=0) and (TAB_NV(c1+1).coor_x>=0)) and
	not((TAB_NV(C1).coor_x>=P2b.coor_x) 
		and (TAB_NV(c1+1).coor_X<=P2b.coor_X)) loop
    C1:=C1+1;
  end loop;

    if ((TAB_NV(C1).coor_x<=0) and (TAB_NV(c1+1).coor_x>=0)) then
      x_deb:=0;
    else
	epaisseur:=gr_legende_l(legende,
		gr_arc(graphe,T_arc_a(c_tab1-1+C1)).att_graph).largeur;
 
	if 	((TAB_NV(C1).coor_x>=P2b.coor_x) 
		and (TAB_NV(c1+1).coor_X<=P2b.coor_X)) then
	  x_deb:=p2b.coor_x;
        else
	  x_deb:=tab_nv(c1).coor_X;
        end if;
    end if;	


    C2:=c1+1;
    x_fin:=tab_nv(c2).coor_X;
---------------------------------------------------------------------------
--recherche le segment qui correspond a la fin du toponyme-----------------
    while (c2<C_TAB) and 
	not((TAB_NV(C2-1).coor_x>=0) and (TAB_NV(c2).coor_x<=0)) and
	not((TAB_NV(C2-1).coor_x<=P2b.coor_x) 
		and (TAB_NV(c2).coor_X>=P2b.coor_X)) loop
        c2:=c2+1;
    end loop;
    if ((TAB_NV(C2-1).coor_x>=0) and (TAB_NV(c2).coor_x<=0)) then
      x_fin:=0;
    else 
        epaisseur:=gr_legende_l(legende,
		gr_arc(graphe,T_arc_a(c_tab1-1+C2)).att_graph).largeur;
	if ((TAB_NV(C2-1).coor_x<=P2b.coor_x) 
		and (TAB_NV(c2).coor_X>=P2b.coor_X)) then
	  x_fin:=p2b.coor_X;
        else
	  x_fin:=tab_nv(c2).coor_X;
        end if;
    end if;	
---------------------------------------------------------------------------
--Au cas ou il n'a pas trouve de valeur pour epaisseur, one prend celle la-
	
	if epaisseur=0.0 then
        epaisseur:=gr_legende_l(legende,
    		gr_arc(graphe,T_arc_a(c_tab1-1+C2)).att_graph).largeur;
    end if;   
---------------------------------------------------------------------------
--On a definit c1 et c2 tel que TAB_NV(C1) et tab_NV(C2) encadrent---------
--le toponyme--------------------------------------------------------------
--Definit la hauteur par rapport a la linearisation------------------------
--La hauteur en x=0 correspond au premier point (0,Hauteur) de la portion--
    if TAB_NV(C1+1).coor_x-TAB_NV(C1).coor_x/=0 then
      EquationY(	float(TAB_NV(C1+1).coor_y-TAB_NV(C1).coor_y)
                   	/float(TAB_NV(C1+1).coor_x-TAB_NV(C1).coor_x),
		float(TAB_NV(C1).coor_y)-
		(float(TAB_NV(C1+1).coor_y-TAB_NV(C1).coor_y)
                   	/float(TAB_NV(C1+1).coor_x-TAB_NV(C1).coor_x))
			*float(TAB_NV(C1).coor_x),
		float(x_deb),
		Haut);
    else
      Haut:=0.0;
    end if;
    Hauteur:=integer(haut);

---------------------------------------------------------------------------
--Calcul du dernier point de la portion (p2b.coor_x, TMP)------------------
    if TAB_NV(C2).coor_x-TAB_NV(C2-1).coor_x/=0 then
      EquationY(	float(TAB_NV(C2).coor_y-TAB_NV(C2-1).coor_y)
                   	/float(TAB_NV(C2).coor_x-TAB_NV(C2-1).coor_x),
		float(TAB_NV(C2-1).coor_y)-
		(float(TAB_NV(C2).coor_y-TAB_NV(C2-1).coor_y)
                   	/float(TAB_NV(C2).coor_x-TAB_NV(C2-1).coor_x))
			*float(TAB_NV(C2-1).coor_x),
		float(x_fin),
		TMP);
    else
      TMp:=0.0;
    end if;
    TAB_NV(C1).coor_x:=x_deb;
    TAB_NV(C1).coor_y:=Hauteur;
    TAB_NV(C2).coor_x:=x_fin; 
    TAB_NV(C2).coor_y:=integer(TMP);
 

---------------------------------------------------------------------------
      for i in C1+1..C2-1 loop
        epaisseur2:=gr_legende_l(legende,
	    gr_arc(graphe,abs(T_arc_a(c_tab1-1+i))).att_graph).largeur;
        epaisseur:=math_int_basic.Max(epaisseur,epaisseur2);
      end loop;
      hauteur_max:=hauteur;
      hauteur_min:=hauteur;

      for i in C1+1..C2 loop
        Hauteur_max:=math_int_basic.Max(Hauteur_max,TAB_NV(i).coor_y);
        Hauteur_min:=math_int_basic.Min(Hauteur_min,TAB_NV(i).coor_y);
      end loop;

      poids:=float(hauteur_max-Hauteur_min)/(2.0*Le_Seuil_S*float(resolution));
      if poids>10.0 then
	  	pds_S:=10.0;
      else
   	    pds_S:=poids;
      end if;

--Definit la hauteur en fonction du placement du toponyme/linearisation----
---------------------------------------------------------------------------
--au dessus----------------------------------------------------------------
    if Z=1 then 
      -- Hauteur:=Hauteur_max+integer((float(Resolution)*epaisseur/2.0)+(float(h_car)*DIST_ROUTE));
      Hauteur:=Hauteur_max+integer((float(Resolution)*epaisseur/2.0)+(float(h_car)*DistRoute));

      Monint:=Resolution_image;
      Monint:=resolution;
      -- monfloat:=DIST_ROUTE;
      monfloat:=DistRoute;
---------------------------------------------------------------------------
--axe----------------------------------------------------------------------
    elsif z=0 then
      Hauteur:=(Hauteur_min+Hauteur_max)/2;
---------------------------------------------------------------------------
--au dessous----------------------------------------------------------------
    else
      -- Hauteur:=Hauteur_min-integer((float(Resolution)*epaisseur/2.0)+(float(h_car)*DIST_ROUTE));
      Hauteur:=Hauteur_min-integer((float(Resolution)*epaisseur/2.0)+(float(h_car)*DistRoute));
    end if;


end DETECTION_PORTION;


--******************************************************************************
--POIDS_MILIEU_LOCAL
--Calcule le poids du toponyme (defini par la barre P_g,P_d)
--de proximite d'un milieu entre deux intersections.
--******************************************************************************
function POIDS_MILIEU_LOCAL (tab_p : point_liste_type;
                             c_p : integer;
                             tab : point_liste_type;
                             crs : integer;
                             P_g : point_type;
                             p_d : point_type) return float is

-- Renvoie le point pf,projete sur la polyligne et renvoie j, le rang du point de la polyligne qui le precede
procedure PROJECTION_POLYLIGNES(TAB : point_liste_type;
                                I : integer;
                                P : point_type;
                                pf : out point_type;
                                j : in out integer) is
Tmp : float;

begin
  J:=RANG_POINT_PLUS_PROCHE(P,TAB,I);
  if J=1 then
   pf:=Projection_segment(p,tab(j), tab(j+1));
  else
    if j=i then
      pf:=Projection_segment(p,tab(j-1), tab(j));
      j:=j-1;
    else
      Tmp:=distance_a_ligne(p,tab(j),tab(j+1),C_SEGMENT_TYPE);
      if distance_a_ligne(p,tab(j-1),tab(j),C_SEGMENT_TYPE)<tmp  then
        pf:=Projection_segment(p,tab(j-1), tab(j));
        j:=j-1;
      else
        pf:=Projection_segment(p,tab(j), tab(j+1));               
      end if;
    end if;
  end if;
end PROJECTION_POLYLIGNES;

-- Renvoie l'abcisse curviligne de la projection du point p sur la polyligne Tab de i points
function TAILLE_PT_PROJ(Tab : point_liste_type;
                        I : integer;
                        p : point_type) return float is
j : integer:=1;
Pf : point_type;
TabF : point_liste_type(1..i);
begin
  PROJECTION_POLYLIGNES(Tab,i,p,pf,j);
  return (Taille_ligne(tab,j)+norme_v2(tab(j),pf));
end TAILLE_PT_PROJ;

--renvoie l'angle de rotation mettent les X>0 au dessus de l'arc
function NV_AXE_ANGLE(p1,p2 : point_type) return float is
-- PI	: float:=3.1416;
begin
  if p1.coor_x<p2.coor_x then
    return C_ANGLE(p1,p2);
  else 
    return (PI+C_ANGLE(p1,p2));
  end if;
end NV_AXE_ANGLE;

--change le repere/axe des x soit (p1,p2)
procedure NV_AXE_TAB(p1,p2 : point_type;
                     tab : point_liste_type;
                     curs : integer; 
                     tab_na : out point_liste_type) is
angle : float;
begin
  angle:=-NV_AXE_ANGLE(p1,p2);
  for i in 1..curs loop
    tab_na(i):=Transforme_repere(tab(i),p1,angle);
  end loop;
end NV_AXE_TAB;

tab_naxe : point_liste_type(1..crs);
point_milieu : point_type;
-- moy_dist : float:=1000.0;
moy_dist : float:=10000.0;
resul : float:=1.0;
dist_max : float;
P_max : point_type;
r_n1,r_n2 : integer:=0;
dist_min : float;
distance : float;

begin
  point_milieu.coor_x:=integer(float(p_g.coor_x+P_d.coor_x)/2.0);
  point_milieu.coor_y:=integer(float(p_g.coor_y+P_d.coor_y)/2.0);
  NV_AXE_TAB(point_milieu,p_d,tab,crs,tab_naxe);
  for i in 1..crs-1 loop
    --si les deux noeuds sont de part et d'autre de Oy

	-- debut modif 06.04.2005
    -- if tab_naxe(i).coor_x*tab_naxe(i+1).coor_x<=0 then
    if float(tab_naxe(i).coor_x)*float(tab_naxe(i+1).coor_x)<=0.0 then
   	-- fin modif 06.04.2005

      --Au cas ou il ya plusieurs noeuds qui encadrent le toponyme,
      --on va prendre celui qui est le segment qui est le plus pres de Ox.

      if moy_dist>float(abs(tab_naxe(i).coor_y)+abs(tab_naxe(i+1).coor_y))/2.0 then
        r_n1:=1;
        -- while (r_n1<=c_p) and (tab(i)/=tab_p(r_n1)) loop
        while (r_n1<c_p) and (tab(i)/=tab_p(r_n1)) loop
	      r_n1:=r_n1+1;
        end loop;
        r_n2:=1;
        -- while (r_n2<=c_p) and (tab(i+1)/=tab_p(r_n2)) loop
        while (r_n2<c_p) and (tab(i+1)/=tab_p(r_n2)) loop
	      r_n2:=r_n2+1;
        end loop;
        dist_min:=Taille_Ligne(tab_p,r_n1);
	    distance:=Taille_Ligne(tab_p,r_n2);
        if distance<dist_min then 
	      dist_max:=dist_min;
	      dist_min:=distance;
        else
          dist_max:=distance;
	    end if;
        distance:=(dist_max-dist_min)/2.0;

        if distance>0.0 then
          resul:=abs(TAILLE_PT_PROJ(Tab_p,c_p,point_milieu)-((dist_max+dist_min)/2.0))/distance;
	    else
	      resul:=1.0;
        end if;
        if resul>1.0 then
	      resul:=1.0;
        end if;
        moy_dist:=float(abs(tab_naxe(i).coor_y)+abs(tab_naxe(i+1).coor_y))/2.0;
      end if;
    end if;
  end loop;
  return resul;
end POIDS_MILIEU_LOCAL;


-- Calcul et classement des occurrences d'�critures en mode de placement DESAXE
PROCEDURE CALCULE_POSITIONS(GRAPHE : graphe_type;
                            Legende : Legende_Type;
                            TAB_T : POINT_LISTE_TYPE;
                            C_T : Positive;
                            T_ARC_A : TGROUPEMENT;
                            TAB_NOEUDS : POINT_LISTE_TYPE;
                            CRS_NOEUDS : positive;
                            MT_TMP : in out MT;
                            TAB_POS : in out POSITION;
                            MT_COURT : out boolean;
                            Zie : in out Boite_type;
                            codetypo : in string30;
                            resolution : in positive;
							cale : boolean;
							kilo : boolean) is

  bon_pos	: integer:=0;
  xpixmin	: integer;
  xpixmax	: integer;
  ypixmin	: integer; 
  ypixmax	: integer; 
  npx		: positive:=1;
  npy		: positive:=1;
  ecrase  	: boolean:=false;
  bord_image : boolean:=false;
  P0		: point_type:=(0,0);
  P1		: point_type;
  P2		: point_type;
  P3		: point_type;  
  P4		: point_type;
  Pi1		: point_type;
  Pi2		: point_type;
  TAB_LIN	: plt_opt(1..2*TAB_T'LAST);
  C_LIN		: positive;
  Ecart		: integer:=0;
  TOPO_MED	: position_type;
  Fanion	: integer;
  pds_S		: typpoi;
  k		: integer;
  Taille_seg	: float;
  l_m, l_m2		: float:=0.0;
  origine_Zie : point_type; -- coord plage du coin inf gauche de la portion d'image extraite  
  lps, ncol, Nlig : integer;
  stripoffset : acces_tabstrip;
  tinterp : acces_tabinterp;
  tsubst : acces_tabsub;
  nb_interp : integer;
  poids : float:=0.0;
  TAB_Cale : POINT_LISTE_TYPE(1..2);
  BoostCentrage : float;
  MonPoidD : float;

begin

  -- renforcement du centrage pour les kilometrages
  if kilo=true then
    BoostCentrage:=5.0;
  else
	BoostCentrage:=1.0;
  end if;

  MT_COURT:=true;
  MT_TMP.Ecrase:=false;
  MT_TMP.bord_image:=false;
  MT_TMP.hors_image:=false;

  --Initialisation de Tab_pos
  for gh in tab_pos'first..tab_pos'last loop
 	tab_pos(gh).poip:=10.0;
	tab_pos(gh).poss:=false;
  end loop;

  -- debut calcul de l'emprise de la portion d'image � charger
  if mutil then

    for cg in 1..C_T loop
      l_m2:=gr_legende_l(legende,gr_arc(graphe,abs(T_arc_a(cg))).att_graph).largeur;
      if l_m2>l_m then 
        l_m:=l_m2;
      end if;
    end loop;

    Ecart:=integer(sqrt(float(MT_TMP.corps**2+MT_TMP.L_TOPO(1)**2)))                              
	              +integer(3.0*float(MT_TMP.corps)*DIST_ROUTE
                  +l_m*float(Resolution_image))+1;

    Zie:=ZONE_INFLUENCE(TAB_T,C_T,Ecart);

    xpixmin:=math_int_basic.max(1,(zie.minimum.coor_x-pso.coor_x)/pt_image+1);
    xpixmax:=math_int_basic.min(delta_xpix_r,(zie.maximum.coor_x-pso.coor_x)/pt_image+1);
    ypixmin:=math_int_basic.min(delta_ypix_r,delta_ypix_r-((zie.minimum.coor_y-pso.coor_y)/pt_image)); 
    ypixmax:=math_int_basic.max(1,delta_ypix_r-((zie.maximum.coor_y-pso.coor_y)/pt_image));

    origine_Zie:=(math_int_basic.max(zie.minimum.coor_x,pso.coor_x),math_int_basic.max(zie.minimum.coor_y,pso.coor_y));

    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
      MT_TMP.hors_image:=true;
      return;
	end if;

    npx:=xpixmax-xpixmin+1;
    npy:=ypixmin-ypixmax+1;  --NB:coordonnees en y inversees!!!

  end if;
  -- fin calcul de l'emprise de la portion d'image � charger

  -- linearisation de la portion d'objet lin�aire nomm�e
  SEG_MCL(Tab_T,C_T,TAB_LIN,C_LIN,SEUIL_S*float(resolution));

  DECLARE
    tpoi : typtipoi(1..npx,1..npy);

  begin

    -- debut chargement de la portion d'image concernee dans tpoi
    if mutil then
      tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
      substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
      substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,codetypo, lps, ncol, stripoffset,tinterp, tsubst, nb_interp, tpoi);
      Free_tabstrip(stripoffset);
      byte_io.Close(file_image);
	  MT_TMP.Ecrase:=true;
      MT_TMP.bord_image:=true;
    end if;
    -- fin chargement de la portion d'image concernee dans tpoi

    k:=1;

    -- niveau d'emboitement 0 : boucle sur les segments de la lin�arisation
    while k<=c_lin-1 loop

      -- affectation de P1 et P2, extr�mit�s gauche et droite du segment courant
      if tab_lin(k).coor_x<tab_lin(k+1).coor_x then 
        p1.coor_x:=tab_lin(k).coor_x;
        p1.coor_y:=tab_lin(k).coor_y;              
        p2.coor_x:=tab_lin(k+1).coor_x;
        p2.coor_y:=tab_lin(k+1).coor_y;
      else
        p2.coor_x:=tab_lin(k).coor_x;
        p2.coor_y:=tab_lin(k).coor_y;
        p1.coor_x:=tab_lin(k+1).coor_x;
        p1.coor_y:=tab_lin(k+1).coor_y;
      end if;
      TOPO_MED.ANGLE(1):=Angle_horizontal(p1,p2);

      Taille_seg:=norme_v2(p1,p2);

      -- on passe les segments trop petits
      If float(MT_TMP.L_TOPO(1))<Taille_seg then
         
        -- niveau d'emboitement 1 : boucle sur la position de l'�criture relativement � la portion d'objet lin�aire nomm�e
	    -- Dd=1 : positions au-dessus de l'objet lineaire
        -- Dd=2 : positions en-dessous de l'objet lineaire
        for dd in 1..2 loop
          fanion:=0;

          -- niveau d'emboitement 2 : boucle sur la position de l'�criture le long du segment courant
          while PAS_TOPO*float(fanion)<Taille_seg-float(MT_TMP.L_TOPO(1)) loop

            p3.coor_x:=integer(PAS_TOPO)*fanion;
            p3.coor_y:=0;

            p4.coor_x:=p3.coor_x+MT_TMP.L_TOPO(1);
            p4.coor_y:=0;
            fanion:=fanion+1;

            -- calcul de la position courante de l'�criture en prenant en compte l'epaisseur du symbole
            -- (rectangle orient� defini dans le sens horaire par P3,Pi1,P4,Pi2 en partant du coin inferieur gauche)
            -- et de la variable pds_s qui mesure la rectitude de la portion de lin�aire support

			DETECTION_PORTION(Graphe, Legende,T_ARC_A, TAB_T,
		                      tab_lin(k).c_pt,tab_lin(k+1).c_pt,
		                      C_Repere(p3,p1,-topo_med.angle(1)),
				              C_Repere(p4,p1,-topo_med.angle(1)),
				              dd,MT_TMP.CORPS,p3.coor_y,pds_S,Resolution,SEUIL_S,DIST_ROUTE);
            if dd=2 then
	          p3.coor_y:=p3.coor_y-MT_TMP.CORPS;
	        end if;

            p4.coor_y:=p3.coor_y+integer(MT_TMP.CORPS);
            pi1.coor_x:=p3.coor_x;
	        pi1.coor_y:=p4.coor_y;
            pi2.coor_x:=p4.coor_x;
	        pi2.coor_y:=p3.coor_y;
            pi1:=C_Repere(pi1,p1,-topo_med.angle(1));
            pi2:=C_Repere(pi2,p1,-topo_med.angle(1));
            topo_med.coin1(1):=C_Repere(p3,p1,-topo_med.angle(1));
            p4:=C_Repere(p4,p1,-topo_med.angle(1));

	        Ecrase:=false;
			bord_image:=false;

            -- debut calcul de la mutilation de l'image par l'�criture
            if mutil then
				
			  POIMUB_OBLIQUE(tpoi,origine_zie,topo_med.coin1(1),topo_med.angle(1),MT_TMP.L_TOPO(1),MT_TMP.corps,dd,ecrase,bord_image,TOPO_MED.poim(1),Desaxe,resolution);

              if not(ecrase) and not(bord_image) then
                MT_TMP.Ecrase:=false;
              end if;
              if not(bord_image) then
                MT_TMP.bord_image:=false;
              end if;
              if bord_image and (est_il_dans_rectangle(Tab_T(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_T(c_t),Pno,Pne,Pse,Pso)=false) then
                MT_TMP.bord_image:=false;
                MT_TMP.hors_image:=true;
              end if;
            end if;
            -- fin calcul de la mutilation de l'image par l'�criture

		    if not(ecrase) and not(bord_image) then
	          topo_med.poss:=true;

			  -- debut calcul du poids de centrage
              if cale=False then  -- on favorise les positions centr�es
			    if kilo=true then -- kilometrage
                  TAB_Cale(1):=TAB_T(1);
                  TAB_Cale(2):=TAB_T(c_T);
			      if dd=1 then
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,topo_med.coin1(1),pi2);
                  else
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,pi1,p4);
                  end if;
			    else -- numero de route
				  if dd=1 then
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,topo_med.coin1(1),pi2);
                  else
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,pi1,p4);
			      end if;
                end if;
              else                -- on favorise les positions cal�es vers les extr�mit�s
                TAB_Cale(1):=TAB_T(1);
                TAB_Cale(2):=TAB_T(integer(float(c_T)/2.0));
			    if dd=1 then
                  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,topo_med.coin1(1),pi2);
                else
                  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,pi1,p4);
                end if;
              end if;
			  -- fin calcul du poids de centrage

              Poids:=COEF_S*(PDS_S*PDS_S)
                    -- debut modif 26.06.2006
			        -- +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))
			        +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))*BoostCentrage
                    -- fin modif 26.06.2006
				    +COEF_MUTIL*(TOPO_MED.POIM(1)*TOPO_MED.POIM(1))
				    +COEF_ANGLE*((TOPO_MED.angle(1))/(PI/2.0))**2
				    +COEF_DAX*float(dd-1);

              if poids>10.0 then
                topo_med.poip:=10.0;
              else
                topo_med.poip:=poids;
              end if;

              topo_med.nl:=dd;
		      bon_pos:=bon_pos+1;
	    	  RANGE_TOPO_DS_TAB(TAB_POS,TOPO_MED);
	  		end if;

		    if (dd=2) or (PAS_TOPO*float(bon_pos)>float(MT_TMP.L_TOPO(1))) then
			  MT_COURT:=false;
		    end if; 
	      end loop;	    
	    end loop;
	  end if;
      K:=K+2;
    end loop;
  END; --DECLARE--
  if MT_TMP.bord_image=true and MT_TMP.ecrase=true then -- on a rencontre que des segments trop petits
    MT_TMP.bord_image:=false;
	MT_TMP.ecrase:=false;
  end if;
end CALCULE_POSITIONS;

-- Calcul et classement des occurrences d'�critures en mode de placement DECALE
PROCEDURE CALCULE_POSITIONS_DECALE(GRAPHE : graphe_type;
                                   Legende : Legende_Type;
                                   TAB_T : POINT_LISTE_TYPE;
                                   C_T : Positive;
                                   T_ARC_A : TGROUPEMENT;
                                   TAB_NOEUDS : POINT_LISTE_TYPE;
                                   CRS_NOEUDS : positive;
                                   MT_TMP : in out MT;
                                   TAB_POS : in out POSITION;
                                   MT_COURT : out boolean;
                                   Zie : in out Boite_type;
                                   codetypo : in string30;
                                   resolution : in positive;
							       cale : boolean;
							       kilo : boolean) is

  bon_pos	: integer:=0;
  xpixmin	: integer;
  xpixmax	: integer;
  ypixmin	: integer; 
  ypixmax	: integer; 
  npx		: positive:=1;
  npy		: positive:=1;
  ecrase  	: boolean:=false;
  bord_image : boolean:=false;
  P0		: point_type:=(0,0);
  P1		: point_type;
  P2		: point_type;
  P3		: point_type;  
  P4		: point_type;
  Pi1		: point_type;
  Pi2		: point_type;
  TAB_LIN	: plt_opt(1..2*TAB_T'LAST);
  C_LIN		: positive;
  Ecart		: integer:=0;
  TOPO_MED	: position_type;
  Fanion	: integer;
  pds_S		: typpoi;
  k		: integer;
  Taille_seg	: float;
  l_m, l_m2		: float:=0.0;
  origine_Zie : point_type; -- coord plage du coin inf gauche de la portion d'image extraite  
  lps, ncol, Nlig : integer;
  stripoffset : acces_tabstrip;
  tinterp : acces_tabinterp;
  tsubst : acces_tabsub;
  nb_interp : integer;
  poids : float:=0.0;
  TAB_Cale : POINT_LISTE_TYPE(1..2);
  BoostCentrage : float;
  MonPoidD : float;
  TOPO_MED_ANGLE : float;
  New_topo_med_coin1 : point_type;

begin

  -- renforcement du centrage pour les kilometrages
  if kilo=true then
    BoostCentrage:=5.0;
  else
	BoostCentrage:=1.0;
  end if;

  MT_COURT:=true;
  MT_TMP.Ecrase:=false;
  MT_TMP.bord_image:=false;
  MT_TMP.hors_image:=false;

  --Initialisation de Tab_pos
  for gh in tab_pos'first..tab_pos'last loop
 	tab_pos(gh).poip:=10.0;
	tab_pos(gh).poss:=false;
  end loop;

  -- debut calcul de l'emprise de la portion d'image � charger
  if mutil then

    for cg in 1..C_T loop
      l_m2:=gr_legende_l(legende,gr_arc(graphe,abs(T_arc_a(cg))).att_graph).largeur;
      if l_m2>l_m then 
        l_m:=l_m2;
      end if;
    end loop;

    Ecart:=integer(sqrt(float(MT_TMP.corps**2+MT_TMP.L_TOPO(1)**2)))                              
	              +integer(3.0*float(MT_TMP.corps)*DIST_ROUTE
                  +l_m*float(Resolution_image))+1;

    Zie:=ZONE_INFLUENCE(TAB_T,C_T,Ecart);

    xpixmin:=math_int_basic.max(1,(zie.minimum.coor_x-pso.coor_x)/pt_image+1);
    xpixmax:=math_int_basic.min(delta_xpix_r,(zie.maximum.coor_x-pso.coor_x)/pt_image+1);
    ypixmin:=math_int_basic.min(delta_ypix_r,delta_ypix_r-((zie.minimum.coor_y-pso.coor_y)/pt_image)); 
    ypixmax:=math_int_basic.max(1,delta_ypix_r-((zie.maximum.coor_y-pso.coor_y)/pt_image));

    origine_Zie:=(math_int_basic.max(zie.minimum.coor_x,pso.coor_x),math_int_basic.max(zie.minimum.coor_y,pso.coor_y));

    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
      MT_TMP.hors_image:=true;
      return;
	end if;

    npx:=xpixmax-xpixmin+1;
    npy:=ypixmin-ypixmax+1;  --NB:coordonnees en y inversees!!!
  end if;
  -- fin calcul de l'emprise de la portion d'image � charger

  -- linearisation de la portion d'objet lin�aire nomm�e
  SEG_MCL(Tab_T,C_T,TAB_LIN,C_LIN,SEUIL_S*float(resolution));

  DECLARE
    tpoi : typtipoi(1..npx,1..npy);

  begin
  	
    -- debut chargement de la portion d'image concernee dans tpoi
    if mutil then
      tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
      substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
      substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,codetypo, lps, ncol, stripoffset,tinterp, tsubst, nb_interp, tpoi);
      Free_tabstrip(stripoffset);
      byte_io.Close(file_image);
	  MT_TMP.Ecrase:=true;
	  MT_TMP.bord_image:=true;
    end if;
    -- fin chargement de la portion d'image concernee dans tpoi

    k:=1;

    -- niveau d'emboitement 0 : boucle sur les segments de la lin�arisation
    while k<=c_lin-1 loop

      -- affectation de P1 et P2, extr�mit�s gauche et droite du segment courant
      if tab_lin(k).coor_x<tab_lin(k+1).coor_x then 
        p1.coor_x:=tab_lin(k).coor_x;
        p1.coor_y:=tab_lin(k).coor_y;              
        p2.coor_x:=tab_lin(k+1).coor_x;
        p2.coor_y:=tab_lin(k+1).coor_y;
      else
        p2.coor_x:=tab_lin(k).coor_x;
        p2.coor_y:=tab_lin(k).coor_y;
        p1.coor_x:=tab_lin(k+1).coor_x;
        p1.coor_y:=tab_lin(k+1).coor_y;
      end if;
      TOPO_MED.ANGLE(1):=Angle_horizontal(p1,p2);
      TOPO_MED_ANGLE:=TOPO_MED.ANGLE(1);

      Taille_seg:=norme_v2(p1,p2);

      -- on passe les segments trop petits
      If float(MT_TMP.corps)<Taille_seg then
 
        -- niveau d'emboitement 1 : boucle sur la position de l'�criture relativement � la portion d'objet lin�aire nomm�e
	    -- Dd=1 : positions au-dessus de l'objet lineaire
        -- Dd=2 : positions en-dessous de l'objet lineaire
        for dd in 1..2 loop
          fanion:=0;

          -- niveau d'emboitement 2 : boucle sur la position de l'�criture le long du segment courant
          while PAS_TOPO*float(fanion)<Taille_seg-float(MT_TMP.L_TOPO(1)) loop

            p3.coor_x:=integer(PAS_TOPO)*fanion;
            p3.coor_y:=0;

            p4.coor_x:=p3.coor_x+MT_TMP.L_TOPO(1);
            p4.coor_y:=0;
            fanion:=fanion+1;

            -- calcul de la position courante de l'�criture en prenant en compte l'epaisseur du symbole
            -- (rectangle orient� defini dans le sens horaire par P3,Pi1,P4,Pi2 en partant du coin inferieur gauche)
            -- et de la variable pds_s qui mesure la rectitude de la portion de lin�aire support

			DETECTION_PORTION(Graphe, Legende,T_ARC_A, TAB_T,
		                      tab_lin(k).c_pt,tab_lin(k+1).c_pt,
		                      C_Repere(p3,p1,-topo_med.angle(1)),
				              C_Repere(p4,p1,-topo_med.angle(1)),
				              dd,MT_TMP.CORPS,p3.coor_y,pds_S,Resolution,SEUIL_S,DIST_ROUTE);
            if dd=2 then
	          p3.coor_y:=p3.coor_y-MT_TMP.CORPS;
	        end if;

            p4.coor_y:=p3.coor_y+integer(MT_TMP.CORPS);
            pi1.coor_x:=p3.coor_x;
	        pi1.coor_y:=p4.coor_y;
            pi2.coor_x:=p4.coor_x;
	        pi2.coor_y:=p3.coor_y;
            pi1:=C_Repere(pi1,p1,-topo_med.angle(1));
            pi2:=C_Repere(pi2,p1,-topo_med.angle(1));
            topo_med.coin1(1):=C_Repere(p3,p1,-topo_med.angle(1));
            p4:=C_Repere(p4,p1,-topo_med.angle(1));

            if dd=1 and topo_med.angle(1)>0.0 then
              pi1.coor_x:=pi1.coor_x-MT_TMP.L_TOPO(1);
              pi2.coor_x:=pi2.coor_x-MT_TMP.L_TOPO(1);
              topo_med.coin1(1).coor_x:=topo_med.coin1(1).coor_x-MT_TMP.L_TOPO(1);
              p4.coor_x:=p4.coor_x-MT_TMP.L_TOPO(1);
            end if;

            if dd=2 and topo_med.angle(1)>0.0 then
              New_topo_med_coin1:=pi1-(0,MT_TMP.corps);
			  pi1:=pi1+New_topo_med_coin1-topo_med.coin1(1);
              pi2:=pi2+New_topo_med_coin1-topo_med.coin1(1);
              p4:=p4+New_topo_med_coin1-topo_med.coin1(1);
              topo_med.coin1(1):=New_topo_med_coin1;
            end if;

            if dd=2 and topo_med.angle(1)<0.0 then
              New_topo_med_coin1:=pi1-(MT_TMP.L_TOPO(1),MT_TMP.corps);
			  pi1:=pi1+New_topo_med_coin1-topo_med.coin1(1);
              pi2:=pi2+New_topo_med_coin1-topo_med.coin1(1);
              p4:=p4+New_topo_med_coin1-topo_med.coin1(1);
              topo_med.coin1(1):=New_topo_med_coin1;
            end if;

	        Ecrase:=false;
			bord_image:=false;

            -- debut calcul de la mutilation de l'image par l'�criture
            if mutil then
				
			  POIMUB_OBLIQUE(tpoi,origine_zie,topo_med.coin1(1),0.0,MT_TMP.L_TOPO(1),MT_TMP.corps,dd,ecrase,bord_image,TOPO_MED.poim(1),Desaxe,resolution);

              if not(ecrase) and not(bord_image) then
                MT_TMP.Ecrase:=false;
              end if;
              if not(bord_image) then
                MT_TMP.bord_image:=false;
              end if;
              if bord_image and (est_il_dans_rectangle(Tab_T(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_T(c_t),Pno,Pne,Pse,Pso)=false) then
                MT_TMP.bord_image:=false;
                MT_TMP.hors_image:=true;
              end if;
            end if;
            --fin calcul de la mutilation de l'image par l'�criture

		    if not(ecrase) and not(bord_image) then
	          topo_med.poss:=true;

			  -- debut calcul du poids de centrage
              if cale=False then  -- on favorise les positions centr�es
			    if kilo=true then -- il s'agit d'un kilom�trage
                  TAB_Cale(1):=TAB_T(1);
                  TAB_Cale(2):=TAB_T(c_T);
			      if dd=1 then
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,topo_med.coin1(1),pi2);
                  else
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,pi1,p4);
                  end if;
			    else -- numero de route
				  if dd=1 then
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,topo_med.coin1(1),pi2);
                  else
                    TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,pi1,p4);
			      end if;
                end if;
              else                -- on favorise les positions cal�es vers les extr�mit�s
                TAB_Cale(1):=TAB_T(1);
                TAB_Cale(2):=TAB_T(integer(float(c_T)/2.0));
			    if dd=1 then
                  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,topo_med.coin1(1),pi2);
                else
                  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,pi1,p4);
                end if;
              end if;
			  -- fin calcul du poids de centrage

              Poids:=COEF_S*(PDS_S*PDS_S)
                    -- debut modif 26.06.2006
			        -- +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))
			        +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))*BoostCentrage
                    -- fin modif 26.06.2006
				    +COEF_MUTIL*(TOPO_MED.POIM(1)*TOPO_MED.POIM(1))
				    +COEF_ANGLE*((TOPO_MED.angle(1))/(PI/2.0))**2
				    +COEF_DAX*float(dd-1);

              if poids>10.0 then
                topo_med.poip:=10.0;
              else
                topo_med.poip:=poids;
              end if;

              topo_med.nl:=dd;
		      bon_pos:=bon_pos+1;

              TOPO_MED.angle(1):=0.0;
			  RANGE_TOPO_DS_TAB(TAB_POS,TOPO_MED);
              TOPO_MED.angle(1):=TOPO_MED_angle;

	  		end if;

		    if (dd=2) or (PAS_TOPO*float(bon_pos)>float(MT_TMP.L_TOPO(1))) then
			  MT_COURT:=false;
		    end if; 
	      end loop;	    
	    end loop;
	  end if;
      K:=K+2;
    end loop;
  END; --DECLARE
  if MT_TMP.bord_image=true and MT_TMP.ecrase=true then -- on a rencontre que des segments trop petits
    MT_TMP.bord_image:=false;
	MT_TMP.ecrase:=false;
  end if;
end CALCULE_POSITIONS_DECALE;


-- Calcul et classement des occurrences d'�critures en mode de placement CAXE ou CDESAXE
PROCEDURE CALCULE_POSITIONS_HYDRO(GRAPHE : graphe_type;
                                  Legende : Legende_Type;
                                  TAB_TG : POINT_LISTE_TYPE;
                                  C_TG : integer;
                                  TAB_TD : POINT_LISTE_TYPE;
                                  C_TD : integer;
                                  MT_TMP : in out MT;
                                  TAB_POS : in out POSITION;
                                  MT_COURT : out boolean;
                                  Zie : in out Boite_type;
                                  codetypo : in string30;
                                  resolution : in positive;
							      cale : boolean;
								  DIST_H : in float;
								  TAB_TI : POINT_LISTE_TYPE;
                                  C_TI : integer;
								  ModePlacement : TypCateg;
								  L_M : in float) is

  xpixmin	: integer;
  xpixmax	: integer;
  ypixmin	: integer; 
  ypixmax	: integer; 
  npx		: positive:=1;
  npy		: positive:=1;
  ecrase  	: boolean:=false;
  bord_image : boolean:=false;
  P0		: point_type:=(0,0);
  -- P3		: point_type;  
  -- P4		: point_type;
  Pi1		: point_type;
  Pi2		: point_type;
  TAB_LING	: plt_opt(1..2*TAB_TG'LAST);
  C_LING	: positive:=2; -- 2 pour que les declarations passent meme si C_TG=0
  TAB_LIND	: plt_opt(1..2*TAB_TD'LAST);
  C_LIND	: positive:=2; -- 2 pour que les declarations passent meme si C_TD=0
  Ecart		: integer:=0;
  TOPO_MED	: position_type;
  EPS		: float:=0.0;  
  Fanion	: integer;
  k		: integer;
  -- l_m, l_m2		: float:=0.0;
  origine_Zie : point_type; -- coord plage du coin inf gauche de la portion d'image extraite  
  lps, ncol, Nlig : integer;
  stripoffset : acces_tabstrip;
  tinterp : acces_tabinterp;
  tsubst : acces_tabsub;
  nb_interp : integer;
  poids : float:=0.0;
  TAB_Cale : POINT_LISTE_TYPE(1..2);
  MaxP1P2G : float:=0.0;
  MaxP1P2D : float:=0.0;
  LEcr : float;
  LSupG : float:=0.0;
  LSupD : float:=0.0;
  MinInterMots : integer;
  -- MaxInterMots : integer;
  IncrInterMots, InterMots : integer;
  Chmt : boolean;
  NbMotsPasEcrase, NbMotsPasBordImage : integer;
  BonPosCetInterMot : integer;  -- nombre de pos valides pour l'InterMot courant du Dd courant
  BonPosInterMots : integer;  -- nombre d'InterMots du DD courant ayant des positions valides
  SeuilBonPosInterMots : integer;
  AbsCur1,AbsCur2,AbsCur,Norme : float;
  J1,J2 : TypTabNGeom(1..NbMaxMots);
  Jmin,Jmax : integer;
  J1Courant : TypTabNGeom(1..NbMaxMots):=(others => 0);
  J2Courant : TypTabNGeom(1..NbMaxMots):=(others => 0);
  TailleLigneSupport : float;
  NbAcroiDistPasEcrase, NbAcroiDistPasBordImage : integer;
  AcroiDist, MaxAcroiDist : integer;
  MaxMaxAcroiDist : integer;
  Longueur_endroit, Longueur_envers : float;
  Boite1, Boite2 : bloc_type;
  Mots_trop_proches : boolean;
  DistMinInterMots : float; -- en fraction de corps
  Point1, Point2 : point_type;
  TabDist : TypTabAngle(1..NbMaxMots);
  DistanceCarre : float;
  MinDist, MaxDist, MoyDist, IdealDist : float;
  proportionEnvers, MaxProportionEnvers : float;
  AleaForceMax : integer;
  IncrAcroiDist : float;
  AngleMax : float;
  TailleMinTroncon : integer;
  AnglePrecisSansPlusPi : TypTabAngle(1..NbMaxMots):=(others => 0.0);
  AuMoinsUnMotPasEcrase : boolean;
  N : integer;
  a,b : float;
  C,D : float;
  LE_COEF_MUTIL ,LE_COEF_DAX, LE_COEF_ENVERS, LE_COEF_HOMO, LE_COEF_IDEAL_DIST, LE_COEF_CORDE, LE_COEF_CENTRE : float;
  J1J2 : Point_liste_type(1..2);
  Inters,IntersI,IntersG,IntersD : Point_Access_type;
  nInters : Liens_array_type(1..NbMaxMots):=(others => 0);
  NintersI,NintersG,NintersD : integer; 
  -- NbCordesIntersec
  ZieG,ZieD : boite_type;
  No_Pred : natural;
  Reste : float;
  AbscurvIni,abscurvFin,AbscurvTotale : float;
  Justification : float;
  prolonge,ProlongeMin : float;
  Add : float;
  DdMin : integer:=1;
  DdMax : integer:=2;
  TMC : point_type;
  TMC2 : point_type;
  TPDI,TPDF : TypPoi;
  C1i,C1F : Point_type;
  LongueurIntersCorde : integer;
  proportionIntersCorde,MaxProportionIntersCorde : float;
  MaxNGeomMot : integer:=100;
  TPNG : integer;
  k1,k2 : integer;
  l : integer;
  MaxDS,MaxDS1,MaxDS2,MaxDS3,DSCarre,dS,DSmoy : float:=0.0;
  PT,PT0,PT1,Orth : point_type;
  PTy : Point_Liste_Type(1..2);
  FanI,E : integer;
  DistMinY : float;
  i : integer;
  MotQuiEcrase : integer:=1;
  AngleMaxSupplRad : float;
  Ag,Bg,Ad,Bd : float;
  MemeSensG, MemeSensD : boolean; -- true si les mots se lisent dans le sens de parcours des lignes support gauche et droite


  -- Definition des generateurs de nombres aleatoires
  subtype Alea5 is Integer range -5..5;
  package alea_InterMots5 is new ada.numerics.discrete_random(Alea5);

  subtype Alea10 is Integer range -10..10;
  package alea_InterMots10 is new ada.numerics.discrete_random(Alea10);

  subtype Alea15 is Integer range -15..15;
  package alea_InterMots15 is new ada.numerics.discrete_random(Alea15);

  subtype Alea20 is Integer range -20..20;
  package alea_InterMots20 is new ada.numerics.discrete_random(Alea20);

  subtype Alea25 is Integer range -25..25;
  package alea_InterMots25 is new ada.numerics.discrete_random(Alea25);

  subtype Alea30 is Integer range -30..30;
  package alea_InterMots30 is new ada.numerics.discrete_random(Alea30);

  subtype Alea35 is Integer range -35..35;
  package alea_InterMots35 is new ada.numerics.discrete_random(Alea35);

  subtype Alea40 is Integer range -40..40;
  package alea_InterMots40 is new ada.numerics.discrete_random(Alea40);

  subtype Alea45 is Integer range -45..45;
  package alea_InterMots45 is new ada.numerics.discrete_random(Alea45);

  subtype Alea50 is Integer range -50..50;
  package alea_InterMots50 is new ada.numerics.discrete_random(Alea50);

  subtype Alea60 is Integer range -60..60;
  package alea_InterMots60 is new ada.numerics.discrete_random(Alea60);

  subtype Alea100 is Integer range 1..100;
  package alea_InterMots100 is new ada.numerics.discrete_random(Alea100);

  gen_Alea5 : alea_InterMots5.generator;
  gen_Alea10 : alea_InterMots10.generator;
  gen_Alea15 : alea_InterMots15.generator;
  gen_Alea20 : alea_InterMots20.generator;
  gen_Alea25 : alea_InterMots25.generator;
  gen_Alea30 : alea_InterMots30.generator;
  gen_Alea35 : alea_InterMots35.generator;
  gen_Alea40 : alea_InterMots40.generator;
  gen_Alea45 : alea_InterMots45.generator;
  gen_Alea50 : alea_InterMots50.generator;
  gen_Alea60 : alea_InterMots60.generator;
  gen_Alea100 : alea_InterMots100.generator;

begin

  MT_COURT:=true;
  MT_TMP.Ecrase:=false;
  MT_TMP.bord_image:=false;
  MT_TMP.hors_image:=false;
  MT_TMP.Agencement:=false;

  -- initialisation de Tab_pos
  for gh in tab_pos'first..tab_pos'last loop
 	tab_pos(gh).poip:=10.0;
	tab_pos(gh).poss:=false;
  end loop;

  -- debut calcul de l'emprise de la portion d'image � charger
  if mutil then

    Ecart:=integer(sqrt(float(MT_TMP.corps**2+MT_TMP.L_TOPO(1)**2)))
	              +integer(3.0*float(MT_TMP.corps)*DIST_H
                  +l_m*float(Resolution_image)+EPS*PAS_TOPO)+1;

    if c_TG>0 then
      ZieG:=ZONE_INFLUENCE(TAB_TG,C_TG,Ecart);
    end if;
	if c_TD>0 then
      ZieD:=ZONE_INFLUENCE(TAB_TD,C_TD,Ecart);
    end if;
	if c_TG*c_TD/=0 then
      Zie.Minimum:=(math_int_basic.min(ZieG.minimum.coor_x,ZieD.minimum.coor_X),math_int_basic.min(ZieG.minimum.coor_Y,ZieD.minimum.coor_Y));
      Zie.Maximum:=(math_int_basic.max(ZieG.maximum.coor_x,ZieD.maximum.coor_X),math_int_basic.max(ZieG.maximum.coor_Y,ZieD.maximum.coor_Y));
    end if;
    if c_TG=0 then
      Zie:=ZieD;
    end if;
	if c_TD=0 then
      Zie:=ZieG;
    end if;

	-- debut ajout 18.04.2005
    if c_TG*c_TD=0 then
      Zie:=ZONE_INFLUENCE(TAB_TI,C_TI,Ecart);
    end if;
	-- fin ajout 18.04.2005

    xpixmin:=math_int_basic.max(1,(zie.minimum.coor_x-pso.coor_x)/pt_image+1);
    xpixmax:=math_int_basic.min(delta_xpix_r,(zie.maximum.coor_x-pso.coor_x)/pt_image+1);
    ypixmin:=math_int_basic.min(delta_ypix_r,delta_ypix_r-((zie.minimum.coor_y-pso.coor_y)/pt_image)); 
    ypixmax:=math_int_basic.max(1,delta_ypix_r-((zie.maximum.coor_y-pso.coor_y)/pt_image));

    origine_Zie:=(math_int_basic.max(zie.minimum.coor_x,pso.coor_x),math_int_basic.max(zie.minimum.coor_y,pso.coor_y));

    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
      MT_TMP.hors_image:=true;
      return;
	end if;

    npx:=xpixmax-xpixmin+1;
    npy:=ypixmin-ypixmax+1;  --NB:coordonnees en y inversees!!!

  end if;
  -- fin calcul de l'emprise de la portion d'image � charger


  -- linearisation de la geometrie de la ligne support superieure
  if C_TG>0 then
    SEG_MCL(Tab_TG,C_TG,TAB_LING,C_LING,1.8*float(resolution));
    --    SEG_MCL(Tab_TG,C_TG,TAB_LING,C_LING,SEUIL_S_HYDRO*float(resolution));
  end if;

  -- linearisation de la geometrie de la ligne support inferieure
  if c_tD>0 then
    SEG_MCL(Tab_TD,C_TD,TAB_LIND,C_LIND,1.8*float(resolution));
    --    SEG_MCL(Tab_TD,C_TD,TAB_LIND,C_LIND,SEUIL_S_HYDRO*float(resolution));
  end if;


--  -- stockage de la geometrie de la ligne support superieure linearisee dans la premiere position candidate
--  if c_tG>0 then
--    TAB_POS(1).NGeom(1):=c_tG;
--	if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
--      for j in 1..TAB_POS(1).NGeom(1) loop
--        TAB_POS(1).Geom(j):=TAB_TG(J);
--      end loop;
--    else
--      for j in 1..TAB_POS(1).NGeom(1) loop
--	    TAB_POS(1).Geom(j):=TAB_TG(C_TG-J+1);
--      end loop;
--    end if;
--  end if;
--
--  -- stockage de la geometrie de la ligne support inferieure lineralisee dans la deuxieme position candidate
--  if c_tD>0 then
--    TAB_POS(2).NGeom(1):=C_tD;
--    if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
--      for j in 1..TAB_POS(2).NGeom(1) loop
--		TAB_POS(2).Geom(j):=TAB_TD(J);
--      end loop;
--    else
--      for j in 1..TAB_POS(2).NGeom(1) loop
--		TAB_POS(2).Geom(j):=TAB_TD(C_TD-J+1);
--      end loop;
--    end if;
--  end if;


  -- regression lineaire sur les lignes support
  -- MC2(Tab_TG,1,c_TG,Ag,bg);
  -- MC2(Tab_TD,1,c_TD,Ad,bd);

  -- calcul par moindres carre de la pente generale des lignes support
  if c_tg>0 then
   MCL(Tab_TG,c_tG,Ag,bg);
  end if;
  if c_td>0 then
   MCL(Tab_TD,c_tD,Ad,bd);
  end if;

  -- stockage de la geometrie de la ligne support superieure linearisee dans la premiere position candidate
  if c_tG>0 then
    TAB_POS(1).NGeom(1):=c_tG;
	if 2.0*abs(Bg)<abs(ag) then -- segment de regression proche de la verticale (moins de 30�)
      if (ag*Bg<0.0 and tab_linG(1).coor_Y<tab_linG(c_linG).coor_Y) or (ag*Bg>0.0 and tab_linG(1).coor_Y>tab_linG(c_linG).coor_Y)then
      -- Ag*Bg<0.0 <=> la pente de la regression est positive
	    MemeSensG:=true;
        for j in 1..TAB_POS(1).NGeom(1) loop
          TAB_POS(1).Geom(j):=TAB_TG(J);
        end loop;
      else
        MemeSensG:=false;
        for j in 1..TAB_POS(1).NGeom(1) loop
	      TAB_POS(1).Geom(j):=TAB_TG(C_TG-J+1);
        end loop;
      end if;		
	else
	  if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
	    MemeSensG:=true;
        for j in 1..TAB_POS(1).NGeom(1) loop
          TAB_POS(1).Geom(j):=TAB_TG(J);
        end loop;
      else
        MemeSensG:=false;
        for j in 1..TAB_POS(1).NGeom(1) loop
	      TAB_POS(1).Geom(j):=TAB_TG(C_TG-J+1);
        end loop;
      end if;
    end if;
  end if;

  -- stockage de la geometrie de la ligne support inferieure lineralisee dans la deuxieme position candidate
  if c_tD>0 then
    TAB_POS(2).NGeom(1):=C_tD;
	if 2.0*abs(Bd)<abs(ad) then -- segment de regression proche de la verticale (moins de 30�)
      if (ad*Bd<0.0 and tab_linD(1).coor_Y<tab_linD(c_linD).coor_Y) or (ad*Bd>0.0 and tab_linD(1).coor_Y>tab_linD(c_linD).coor_Y)then
      -- Ad*Bd<0.0 <=> la pente de la regression est positive
        MemeSensD:=True;
        for j in 1..TAB_POS(2).NGeom(1) loop
          TAB_POS(2).Geom(j):=TAB_TD(J);
        end loop;
      else
        MemeSensD:=False;
        for j in 1..TAB_POS(2).NGeom(1) loop
	      TAB_POS(2).Geom(j):=TAB_TD(C_TD-J+1);
        end loop;
      end if;
	else
      if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
        MemeSensD:=True;
        for j in 1..TAB_POS(2).NGeom(1) loop
		  TAB_POS(2).Geom(j):=TAB_TD(J);
        end loop;
      else
        MemeSensD:=False;
        for j in 1..TAB_POS(2).NGeom(1) loop
		  TAB_POS(2).Geom(j):=TAB_TD(C_TD-J+1);
        end loop;
      end if;
    end if;
  end if;


  DECLARE
    tpoi : typtipoi(1..npx,1..npy);
    Tab_AngleG : typTabAngle(1..c_linG-1);
    Tab_AngleD : typTabAngle(1..c_linD-1);
    P1G : Point_liste_type(1..C_LinG-1);
    P2G : Point_liste_type(1..c_linG-1);
    P1D : Point_liste_type(1..C_LinD-1);
    P2D : Point_liste_type(1..c_linD-1);
    Taille_segG : typTabAngle(1..c_linG-1);
    Taille_segD : typTabAngle(1..c_linD-1);
    Fanion : typTabNGeom(1..MT_TMP.Nb_Mots);
    TabK : typTabNGeom(1..MT_TMP.Nb_Mots);
    TabAbsCurG : typTabLigneSupport(1..c_linG-1);
    TabAbsCurD : typTabLigneSupport(1..c_linD-1);

    begin

	  -- affectation de tableaux decrivant, dans le sens de lecture de l'ecriture, les segments successifs composant la ligne support sup�rieure lin�aris�e
      if c_TG>0 then
        k:=1;
	    MaxP1P2G:=0.0;
        -- if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
		if MemeSensG=True then
          while k<=c_linG-1 loop
            p1G(K).coor_x:=tab_linG(k).coor_x;
            p1G(K).coor_y:=tab_linG(k).coor_y;              
            p2G(K).coor_x:=tab_linG(k+1).coor_x;
            p2G(K).coor_y:=tab_linG(k+1).coor_y;
            Tab_AngleG(K):=Angle_horizontal(p1G(k),p2G(k));
            Taille_segG(k):=norme_v2(p1G(k),p2G(k));
            if Taille_segG(k)>MaxP1P2G then
		  	  MaxP1P2G:=Taille_segG(k);
            end if;
            k:=k+2;
          end loop;
        else
          while k<=c_linG-1 loop
            p1G(C_linG-K).coor_x:=tab_linG(k+1).coor_x;
            p1G(C_linG-k).coor_y:=tab_linG(k+1).coor_y;
            p2G(C_linG-k).coor_x:=tab_linG(k).coor_x;
            p2G(C_linG-k).coor_y:=tab_linG(k).coor_y;
            Tab_AngleG(C_linG-k):=Angle_horizontal(p1G(C_linG-k),p2G(C_linG-k));
            Taille_segG(C_linG-k):=norme_v2(p1G(C_linG-k),p2G(C_linG-k));
            if Taille_segG(C_linG-k)>MaxP1P2G then
		  	  MaxP1P2G:=Taille_segG(C_linG-k);
            end if;
            k:=k+2;
          end loop;
        end if;
      end if;

	  -- affectation de tableaux decrivant, dans le sens de lecture de l'ecriture, les segments successifs composant la ligne support inferieure lin�aris�e
      if c_td>0 then
        k:=1;
	    MaxP1P2D:=0.0;

	  	-- if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
		if MemeSensD=True then
          while k<=c_linD-1 loop
            p1D(K).coor_x:=tab_linD(k).coor_x;
            p1D(K).coor_y:=tab_linD(k).coor_y;              
            p2D(K).coor_x:=tab_linD(k+1).coor_x;
            p2D(K).coor_y:=tab_linD(k+1).coor_y;
            Tab_AngleD(K):=Angle_horizontal(p1D(k),p2D(k));
            Taille_segD(k):=norme_v2(p1D(k),p2D(k));
            if Taille_segD(k)>MaxP1P2D then
		  	  MaxP1P2D:=Taille_segD(k);
            end if;
            k:=k+2;
          end loop;
        else
          while k<=c_linD-1 loop
            p1D(C_linD-K).coor_x:=tab_linD(k+1).coor_x;
            p1D(C_linD-k).coor_y:=tab_linD(k+1).coor_y;
            p2D(C_linD-k).coor_x:=tab_linD(k).coor_x;
            p2D(C_linD-k).coor_y:=tab_linD(k).coor_y;
            Tab_AngleD(C_linD-k):=Angle_horizontal(p1D(C_linD-k),p2D(C_linD-k));
            Taille_segD(C_linD-k):=norme_v2(p1D(C_linD-k),p2D(C_linD-k));
            if Taille_segD(C_linD-k)>MaxP1P2D then
		  	  MaxP1P2D:=Taille_segD(C_linD-k);
            end if;
            k:=k+2;
          end loop;
        end if;
      end if;

	  -- Reglage de differents parametres du traitement :
	    -- parametres geometriques du calcul des positions candidates,
	    -- variables servant au classement des positions candidates
	    -- bornes des increments
	    -- variables correspondant a des criteres d'arret 
      DistMinInterMots:=1.0; -- en fraction de corps
      MinInterMots:=1; -- en fraction de corps
	  IncrInterMots:=1;
      SeuilBonPosInterMots:=1;
      IncrAcroiDist:=DIST_H/2.0;
	  -- inter-mot ideal =75% de l'inter-mot maximum
      IdealDist:=0.75*float(MaxInterMots*MT_TMP.Corps);
	  MaxProportionEnvers:=float(MaxProportionEnversPourcent)/100.0;
	  MaxProportionIntersCorde:=1.0;
      AleaForceMax:=2;
      MaxMaxAcroiDist:=1;  -- Maximum du maximum d'accroissement de la distance au bord de symbole
	  AngleMaxSupplRad:=float(AngleMaxSuppl)*Pi/180.0;  -- orientations possibles : [-Pi/2.0-AngleMaxSupplRad,+Pi/2.0+AngleMaxSupplRad]
      TailleMinTroncon:=0;  -- en mm carte
      LE_COEF_MUTIL:=COEF_MUTIL_HYDRO;
      LE_COEF_DAX:=COEF_DAX_HYDRO;
      LE_COEF_ENVERS:=COEF_ENVERS;
      LE_COEF_HOMO:=COEF_HOMO;
      LE_COEF_IDEAL_DIST:=COEF_IDEAL_DIST;
      LE_COEF_CORDE:=COEF_CORDE;
      LE_COEF_CENTRE:=COEF_CENTRE;
	  -- DIST_HYDRO est declar� dans norous. A regler a 0.33 pour le 100.000e, 0.5 pour le 25.000e
	  DistMinY:=0.2;  -- distance mini garrantie, en fraction de dist_HYDRO*corps, doit �tre au moins �gale au DilY utilis� dans Poimub_Oblique pour les noms hydro


      if ModePlacement=CAxe then
	  	MaxMaxAcroiDist:=0;
      end if;

	  if c_TG>0 then
	    LSupG:=Taille_ligne(Tab_TG,C_tG);
      end if;
	  if c_tD>0 then
	    LSupD:=Taille_ligne(Tab_TD,C_tD);
      end if;

      -- debranchement dans le cas d'un ligne support trop courte
      if math_int_basic.max(LSupG,LSupD)<float(TailleMinTroncon*Resolution) then
        MT_tMP.court:=true;
        MT_tMP.ecrase:=false;
        MT_tMP.hors_image:=false;
        MT_tMP.bord_image:=false;
        MT_tMP.Agencement:=false;
        return;
      end if;

      -- autre debranchement dans le cas d'un ligne support trop courte
	  LEcr:=0.0;
	  for i in 1..MT_TMP.Nb_Mots loop
        LEcr:=LEcr+float(MT_TMP.L_Topo(i));
      end loop;
      if LEcr+float((MT_TMP.Nb_Mots-1)*MinInterMots*MT_TMP.Corps)>math_int_basic.Max(LSupG,LSupD) then
        MT_tMP.court:=true;
        MT_tMP.ecrase:=false;
        MT_tMP.hors_image:=false;
        MT_tMP.bord_image:=false;
        MT_tMP.Agencement:=false;
        return;
      end if;

      MT_tMP.court:=false;

      -- debranchement dans le cas d'un ligne support trop sinueuse (presence d'un mot de l'ecriture + long que le plus long segment de la geometrie linearisee des lignes support)
	  for i in 1..MT_TMP.Nb_Mots loop
	    if float(MT_TMP.L_TOPO(i))>MaxP1P2G and float(MT_TMP.L_TOPO(i))>MaxP1P2D then
          MT_tMP.court:=false;
          MT_tMP.ecrase:=false;
          MT_tMP.hors_image:=false;
          MT_tMP.bord_image:=false;
          MT_tMP.Agencement:=false;
          return;
        end if;
      end loop;

      -- stockage des abscisses curvilignes des vertex de la ligne support superieure lin�arisee, calculees sur la ligne de support superieure non linearisee
      if c_tG>0 then
        k:=1;
	  	-- if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
		if MemeSensG=True then
          while k<=c_linG-1 loop
            TabAbsCurG(K)(1):=norme_v2(Tab_TG(tab_linG(K).c_pt),Tab_TG(tab_linG(K).c_pt+1));
			for j in 2..tab_linG(K+1).c_pt-tab_linG(K).c_pt loop
              TabAbsCurG(K)(J):=TabAbsCurG(K)(J-1)+norme_v2(Tab_TG(tab_linG(K).c_pt+J-1),Tab_TG(tab_linG(K).c_pt+J));
            end loop;
            k:=k+2;
          end loop;
        else
          while k<=c_linG-1 loop
            TabAbsCurG(C_linG-k)(1):=norme_v2(Tab_TG(tab_linG(K).c_pt),Tab_TG(tab_linG(K).c_pt+1));
            for j in 2..tab_linG(K+1).c_pt-tab_linG(K).c_pt loop
              TabAbsCurG(C_linG-k)(J):=TabAbsCurG(C_linG-k)(J-1)+norme_v2(Tab_TG(tab_linG(K).c_pt+J-1),Tab_TG(tab_linG(K).c_pt+J));
            end loop;
            k:=k+2;
          end loop;
        end if;
      end if;

      -- stockage des abscisses curvilignes des vertex de la ligne support inferieure lin�arisee, calculees sur la ligne de support inferieure non linearisee
	  if c_TD>0 then
        k:=1;
	    -- if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
		if MemeSensD=True then
          while k<=c_linD-1 loop
            TabAbsCurD(K)(1):=norme_v2(Tab_TD(tab_linD(K).c_pt),Tab_TD(tab_linD(K).c_pt+1));
		    for j in 2..tab_linD(K+1).c_pt-tab_linD(K).c_pt loop
              TabAbsCurD(K)(J):=TabAbsCurD(K)(J-1)+norme_v2(Tab_TD(tab_linD(K).c_pt+J-1),Tab_TD(tab_linD(K).c_pt+J));
            end loop;
            k:=k+2;
          end loop;
        else
          while k<=c_linD-1 loop
            TabAbsCurD(C_linD-k)(1):=norme_v2(Tab_TD(tab_linD(K).c_pt),Tab_TD(tab_linD(K).c_pt+1));
            for j in 2..tab_linD(K+1).c_pt-tab_linD(K).c_pt loop
              TabAbsCurD(C_linD-k)(J):=TabAbsCurD(C_linD-k)(J-1)+norme_v2(Tab_TD(tab_linD(K).c_pt+J-1),Tab_TD(tab_linD(K).c_pt+J));
            end loop;
            k:=k+2;
          end loop;
        end if;
      end if;

      -- debut chargement de la portion d'image concernee dans tpoi
      if mutil then
        tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
        substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
        byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
        substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,codetypo, lps, ncol, stripoffset,tinterp, tsubst, nb_interp, tpoi);
		Free_tabstrip(stripoffset);
        byte_io.Close(file_image);
	    MT_TMP.Ecrase:=true;
		MT_TMP.bord_image:=true;
		MT_TMP.hors_image:=true;
      end if;
      -- fin chargement de la portion d'image concernee dans tpoi

	  MT_TMP.Agencement:=true;
      TOPO_Med.Geom := new point_liste_type(1..MaxNGeomMot*MT_TMP.Nb_Mots);

      if C_TG=0 then
	    ddMin:=2;
      end if;
      if C_TD=0 then
	    ddMax:=1;
      end if;

      -- niveau d'emboitement 0 : boucle sur la position de l'�criture relativement � la portion d'objet lin�aire nomm�e
	  -- Dd=1 : positions au-dessus de l'objet lineaire
      -- Dd=2 : positions en-dessous de l'objet lineaire
      for dd in DdMin..DdMax loop
	  	
		-- on ne cherche des positions en-dessous que si aucune position au-dessus n'a ete trouvee
--		if (dd=2 and BonPosInterMots>0) then
--		  goto finBoucleDd;
--        end if;

        BonPosInterMots:=0;
        AuMoinsUnMotPasEcrase:=false;

	    InterMots:=MaxInterMots+IncrInterMots;

		-- niveau d'emboitement 1 : boucle sur la longueur des intermots
	    while InterMots>=MinInterMots+IncrInterMots loop  -- boucle en InterMots
		  InterMots:=InterMots-IncrInterMots;

		  BonPosCetInterMot:=0;  -- nombre de positions valides de l'ecriture pour l'intermot courant

		  -- niveau d'emboitement 2 : boucle sur l'accroissement maximum de la distance entre les mots de l'�criture et l'objet lin�aire nomm�
          -- for MaxAcroiDist in MaxMaxAcroiDist..MaxMaxAcroiDist loop
          for MaxAcroiDist in 0..MaxMaxAcroiDist loop

		    -- niveau d'emboitement 3 : boucle sur l'importance de la partie aleatoire de la longueur des inter-mots
            for AleaForce in 1..AleaForceMax loop

		      -- niveau d'emboitement 4 : boucle de repetition, avec a chaque fois des intermots differents car en partie aleatoires
              for iter in 1..MT_TMP.Nb_Mots*2**(math_int_basic.min(MT_TMP.Nb_Mots,3)-1) loop
		      -- (1->1 2->4 3->12 4->16 5->20 6->24 7->28)

		        -- calcul (en partie aleatoire) de la longueur des inter-mots,
		        -- calcul de la valeur initiale du tableau TabK, c-a-d des numeros de segment de la ligne de support linearisee sur lesquels se placent les differents mots dans leur position initiale,
		        -- et calcul de la valeur initiale du tableau Fanion = c-a-d des abscisses des positions des differents mots de l'ecriture sur leur segment dans leur position initiale
		        TabK(1):=1;
		        fanion(1):=0;
                for i in 2..MT_TMP.Nb_Mots loop
                  TabK(I):=TabK(I-1);
      		      case AleaForce is
                    when 0 => fanion(i):=fanion(I-1)+integer(float(MT_TMP.L_Topo(I-1)+InterMots*MT_TMP.Corps)/PAS_TOPO);
                    when 1 => fanion(i):=fanion(I-1)+integer((float(MT_TMP.L_Topo(I-1))+float(InterMots*MT_TMP.Corps)*(1.0+float(alea_InterMots20.random(gen_alea20))/100.0))/PAS_TOPO);
                    when 2 => fanion(i):=fanion(I-1)+integer((float(MT_TMP.L_Topo(I-1))+float(InterMots*MT_TMP.Corps)*(1.0+float(alea_InterMots50.random(gen_alea50))/100.0))/PAS_TOPO);
                    when others => null;
                  end case;
			      if dd=1 then
			        while fanion(i)>integer(Taille_segG(TabK(i))/PAS_TOPO) loop
                      fanion(i):=fanion(i)-integer(Taille_segG(TabK(i))/PAS_TOPO);
                      TabK(i):=TabK(i)+2;
			          if TabK(I)>=C_linG then
			            goto FinBoucleIter;
                      end if;
                    end loop;
	              else
			        while fanion(i)>integer(Taille_segD(TabK(i))/PAS_TOPO) loop
                      fanion(i):=fanion(i)-integer(Taille_segD(TabK(i))/PAS_TOPO);
                      TabK(i):=TabK(i)+2;
			          if TabK(I)>=C_linD then
			            goto FinBoucleIter;
                      end if;
                    end loop;
                  end if;
                end loop;

		        -- niveau d'emboitement 5 : boucle sur la position courante du premier mot
                loop

		          -- on verifie que le dernier mot ne va pas au-dela du dernier segment
			      chmt:=true;
                  while chmt=true loop
                    for i in 1..MT_TMP.Nb_Mots loop
			  	      if dd=1 then
                        while fanion(i)>integer(Taille_segG(TabK(i))/PAS_TOPO) loop
                          fanion(i):=fanion(i)-integer(Taille_segG(TabK(i))/PAS_TOPO);
                          TabK(i):=TabK(i)+2;
				          if TabK(I)>=C_linG then
			  	            chmt:=chmt;
                            goto FinBoucleIter;
		                  end if;
                        end loop;
		              else
                        while fanion(i)>integer(Taille_segD(TabK(i))/PAS_TOPO) loop
                          fanion(i):=fanion(i)-integer(Taille_segD(TabK(i))/PAS_TOPO);
                          TabK(i):=TabK(i)+2;
				          if TabK(I)>=C_linD then
                            goto FinBoucleIter;
		                  end if;
                        end loop;
		              end if;
                    end loop;

		            -- decalage de la position de chaque mot de 1 PAS_TOPO avec mise a jour des tableaux TabK et Fanion
			        Chmt:=false;
                    for i in 1..MT_TMP.Nb_Mots loop
			  	      if dd=1 then
                        if float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i))>Taille_segG(TabK(i)) 
				        or Tab_AngleG(TabK(I))>(0.5*Pi+AngleMaxSupplRad) or Tab_AngleG(TabK(I))<-(0.5*Pi+AngleMaxSupplRad) then
				          for J in 1..MT_TMP.Nb_Mots loop
				  	        if J/=i then
					          Fanion(J):=Fanion(J)+integer(Taille_segG(TabK(i))/PAS_TOPO)-Fanion(i);
                            end if;
                          end loop;
                          Fanion(I):=0;
                          TabK(i):=TabK(i)+2;
				          if TabK(I)>=C_linG then
                            goto FinBoucleIter;
		                  end if;
			              chmt:=true;
				          exit;
			            end if;
				      else
                        if float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i))>Taille_segD(TabK(i)) 
				        or Tab_AngleD(TabK(I))>(0.5*Pi+AngleMaxSupplRad) or Tab_AngleD(TabK(I))<-(0.5*Pi+AngleMaxSupplRad) then
				          for J in 1..MT_TMP.Nb_Mots loop
				  	        if J/=i then
					          Fanion(J):=Fanion(J)+integer(Taille_segD(TabK(i))/PAS_TOPO)-Fanion(i);
                            end if;
                          end loop;
                          Fanion(I):=0;
                          TabK(i):=TabK(i)+2;
				          if TabK(I)>=C_linD then
                            goto FinBoucleIter;
		                  end if;
			              chmt:=true;
				          exit;
			            end if;
                      end if;
                    end loop;
                  end loop;

                  MT_TMP.Agencement:=false;

                  NbMotsPasEcrase:=0;
                  NbMotsPasBordImage:=0;
			      LongueurIntersCorde:=0;

	      	      -- niveau d'emboitement 6 : boucle sur les mots de l'ecriture
                  for iA in 1..MT_TMP.Nb_Mots loop

			        -- s'il existe, on considere d'abord le mot ecrasant des pixels interdits lors de la precedente execution de cette boucle en IA
			        i:=(MotQuiEcrase+IA-1) mod MT_TMP.Nb_Mots;
		            if i=0 then
			          i:=MT_TMP.Nb_Mots;
                    end if;
				
		      	    if dd=1 then

                      -- Debut du calcul precis de l'orientation de la boite englobante du mot courant
                      -- if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
					  if MemeSensG=True then
			        	TailleLigneSupport:=TabAbsCurG(TabK(i))(tab_linG(TabK(i)+1).c_pt-tab_linG(TabK(i)).c_pt);
                        AbsCur1:=float(Fanion(i))*PAS_TOPO*TailleLigneSupport/Taille_segG(TabK(i));
                        AbsCur2:=(float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i)))*TailleLigneSupport/Taille_segG(TabK(i));
	      		  	    Jmin:=tab_linG(TabK(i)).c_pt;
		      		    Jmax:=tab_linG(TabK(i)+1).c_pt-1;
                      else
			  	        TailleLigneSupport:=TabAbsCurG(TabK(i))(tab_linG(C_linG-TabK(i)+1).c_pt-tab_linG(c_linG-TabK(i)).c_pt);
                        AbsCur1:=float(Fanion(i))*PAS_TOPO*TailleLigneSupport/Taille_segG(TabK(i));
                        AbsCur2:=(float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i)))*TailleLigneSupport/Taille_segG(TabK(i));
		  	            AbsCur:=AbsCur1;
			  	        abscur1:=TailleLigneSupport-Abscur2;
			  	        abscur2:=TailleLigneSupport-Abscur;
			  	        Jmin:=tab_linG(c_linG-TabK(i)).c_pt;
			  	        Jmax:=tab_linG(c_linG-TabK(i)+1).c_pt-1;
                      end if;
			          j1(i):=0;
			          J2(i):=0;
                      for J in Jmin..Jmax loop
				        if TabAbsCurG(TabK(I))(j-Jmin+1)>AbsCur1 and J1(i)=0 then
                          J1(i):=J;
                        end if;
				        if TabAbsCurG(TabK(i))(j-Jmin+1)>=AbsCur2 then
				          J2(i):=J+1;
		                  exit;
                        end if;
                      end loop;

			          -- debut du calcul des intersections de la ligne support avec sa corde (crit�re de r�gularit�)
		              if J1(i)/=J1Courant(i) or J2(i)/=J2Courant(i) then
                        J1J2(1):=Tab_TG(J1(i));
		                J1J2(2):=Tab_TG(J2(i));
	                    Intersections_de_polylignes(Tab_TG,J1J2,c_tG,2,Inters,NInters(i),false);
			            GR_Free_point_liste(Inters);
                      end if;
		              if NInters(I)>2 then
				        LongueurIntersCorde:=LongueurIntersCorde+MT_TMP.L_Topo(i);
                      end if;
			          -- fin du calcul des intersections de la ligne support avec sa corde (crit�re de r�gularit�)

                      if J1(i)/=J1Courant(i) or J2(i)/=J2Courant(i) then
			            -- if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
					  	if MemeSensG=True then
                          Topo_Med.Angle(i):=Angle_horizontal(Tab_TG(J1(i)),Tab_TG(J2(i)));
                        else
                          Topo_Med.Angle(i):=Angle_horizontal(Tab_TG(J2(i)),Tab_TG(J1(i)));
                        end if;
	        	        C:=Topo_Med.Angle(i);
				        -- C est une valeur approchee de l'orientation
				        -- Calcul precis par moindres carres :
                        -- MC2(Tab_TG,j1(i),j2(i),a,b);
						if i=3 then
						  C:=C;
                        end if;
                        MCL2(Tab_TG,j1(i),j2(i),a,b);
			            if b/=0.0 then
                          Topo_Med.Angle(i):=ArcTan(-a/B);
			            else
                          Topo_Med.Angle(i):=PI/2.0;
                        end if;
			            if abs(Topo_Med.Angle(i)-C)>Pi/2.0 then
                          if Topo_Med.Angle(i)<0.0 then
				            Topo_Med.Angle(i):=Topo_Med.Angle(i)+Pi;
			              else
				  	        Topo_Med.Angle(i):=Topo_Med.Angle(i)-Pi;
                          end if;
                        end if;
				        D:=Topo_Med.Angle(i);
			            if abs(c-d)<0.2 then
						  AnglePrecisSansPlusPi(i):=D; 
                        else -- dans les cas rares et non identifies ou C et D sont tres differents, c'est la valeur approchee qu'on retient
						  AnglePrecisSansPlusPi(i):=C;
                        end if;
				        J1Courant(i):=J1(i);
				        J2Courant(i):=J2(i);
                      end if;
                      -- Fin du calcul precis de l'orientation de la boite englobante du mot courant
			  
                      NbAcroiDistPasEcrase:=0;
                      NbAcroiDistPasBordImage:=0;

		              -- niveau d'emboitement 7 : boucle sur l'accroissement de la distance entre le mot courant de l'�criture et l'objet lin�aire nomm�
                      for AcroiDist in 0..MaxAcroiDist loop

                        Topo_Med.Angle(I):=AnglePrecisSansPlusPi(i);

						-- debut d�calage de la position courante du mot courant perpendiculairement � l'objet lin�aire
                        MaxDS1:=-float'last;
                        MaxDS2:=-float'last;
                        MaxDS3:=-float'last;
			            N:=integer(Taille_segG(TabK(i))/PAS_TOPO);
			            if n=0 then
			              N:=1;
                        end if;
			            PT0:=(integer((1.0-float(Fanion(I))/float(N))*float(P1G(TabK(i)).coor_X)+(float(Fanion(I))/float(N))*float(P2G(TabK(i)).coor_X)),
                              integer((1.0-float(Fanion(I))/float(N))*float(P1G(TabK(i)).coor_Y)+(float(Fanion(I))/float(N))*float(P2G(TabK(i)).coor_Y)));
			            PT1:=(PT0.coor_x+integer(float(MT_TMP.L_Topo(i))*cos(Topo_Med.Angle(i))),
			                  PT0.coor_Y+integer(float(MT_TMP.L_Topo(i))*sin(Topo_Med.Angle(i))));
                        Orth.coor_X:=5*(PT0.coor_Y-PT1.coor_Y);
                        Orth.coor_y:=5*(PT1.coor_X-PT0.coor_X);
			            for E in 0..10 loop
				          PT:=(integer(float((10-E)*PT0.coor_X+E*PT1.coor_X)/10.0),
				               integer(float((10-E)*PT0.coor_Y+E*PT1.coor_Y)/10.0));
                          PTy(1):=(PT.coor_X+Orth.coor_X,PT.coor_Y+Orth.coor_Y);
		  	              PTy(2):=(PT.coor_X-Orth.coor_X,PT.coor_Y-Orth.coor_Y);
	                      Intersections_de_polylignes(Tab_TI,PTy,c_tI,2,IntersI,NIntersI,false);
				          if NintersI/=0 then
				            DSCarre:=distance_de_points(PT,IntersI(1));
				            if NIntersI>1 then
                              for num in 2..NIntersI loop
                                if distance_de_points(PT,IntersI(Num))<DSCarre then
                                  DSCarre:=distance_de_points(PT,IntersI(Num));
                                end if;
                              end loop;
                            end if;

				            if ((Topo_Med.Angle(i)>-Pi/2.0 and Topo_Med.Angle(i)<Pi/2.0) and PT.Coor_y<IntersI(1).Coor_y)
				            or ((Topo_Med.Angle(i)<-Pi/2.0 or Topo_Med.Angle(i)>Pi/2.0) and PT.Coor_y>IntersI(1).Coor_y) then
					          DS:=sqrt(DSCarre)/float(MT_TMP.corps);
			                else
                              DS:=-sqrt(DSCarre)/float(MT_TMP.corps);
                            end if;
				            if DS>maxDS1 then
				  	          MaxDS3:=MaxDS2;
				              MaxDS2:=MaxDS1;
				              MaxDS1:=DS;
			                else
			  	              if DS>maxDS2 then
					            MaxDS3:=MaxDS2;
 				                MaxDS2:=DS;
				              else
					            if DS>maxDS3 then
                                  MaxDS3:=DS;
                                end if;
                              end if;
                            end if;
			                GR_Free_point_liste(IntersI);
                          end if;
                        end loop;

			            if MaxDS3=-float'last then
			  	          if MaxDS2=-Float'last then
				            if MaxDS1=-Float'last then
	                          MaxDS1:=0.0;
	                          MaxDS3:=0.0;
                            else
				              MaxDS3:=MaxDS1;
                            end if;
			              else
				            MaxDS3:=MaxDS2;
                          end if;
                        end if;

                        topo_med.coin1(I).coor_x:=PT0.coor_x
			            -integer(((math_int_basic.max(MaxDS1-MaxDS3+DistMinY*DIST_H,DIST_H)+MaxDS3)*float(MT_TMP.corps)+L_M*float(resolution)/2.0)*Sin(Topo_Med.Angle(i)));
                        topo_med.coin1(I).coor_Y:=PT0.coor_Y
			            +integer(((math_int_basic.max(MaxDS1-MaxDS3+DistMinY*DIST_H,DIST_H)+MaxDS3)*float(MT_TMP.corps)+L_M*float(resolution)/2.0)*Cos(Topo_Med.Angle(i)));
						-- fin d�calage de la position courante du mot courant perpendiculairement � l'objet lin�aire

                        if ModePlacement=CAXE then
                          topo_med.coin1(I).coor_x:=topo_med.coin1(I).coor_x+integer(0.5*float(MT_TMP.corps)*Sin(Topo_Med.Angle(i)));
                          topo_med.coin1(I).coor_Y:=topo_med.coin1(I).coor_Y-integer(0.5*float(MT_TMP.corps)*cos(Topo_Med.Angle(i)));
                        end if;

			            -- retournement du mot si son orientation depasse la verticale
			            if Topo_Med.Angle(i)>Pi/2.0 or Topo_Med.Angle(i)<-Pi/2.0 then
                          topo_med.PlusPi(I):=true;
		                else
                          topo_med.PlusPi(I):=false;
                        end if;

	                    Ecrase:=false;
                        bord_image:=false;

                        if mutil then

			              POIMUB_OBLIQUE(tpoi,origine_zie,topo_med.coin1(I),TOPO_MED.Angle(i),MT_TMP.L_TOPO(I),MT_TMP.corps,0,ecrase,bord_image,TOPO_MED.poim(I),ModePlacement,resolution);

                          if bord_image and MT_tMP.hors_image=true then
				            if est_il_dans_rectangle(Tab_TG(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_TG(c_tG),Pno,Pne,Pse,Pso)=false then
                              MT_tMP.hors_image:=True;
                              MT_tMP.court:=False;
                              MT_tMP.ecrase:=false;
                              MT_tMP.bord_image:=false;
                              MT_tMP.Agencement:=false;
					          GR_Free_point_liste(TOPO_MED.Geom);
				              return;
                            end if;
                          end if;

                          MT_tMP.hors_image:=false;

                          if ecrase and (MT_TMP.Bord_image=false or bord_image=true) and AcroiDist=MaxAcroiDist then
				            MotQuiEcrase:=I;
				            goto FinBoucleFanion;
			              end if;

                          if Bord_Image and (MT_TMP.Ecrase=false or Ecrase=true) and AcroiDist=MaxAcroiDist then
				            goto FinBoucleFanion;
			              end if;

                          if not(Ecrase) then
                            AuMoinsUnMotPasEcrase:=true;
                            NbAcroiDistPasEcrase:=NbAcroiDistPasEcrase+1;
                          end if;

                          if not(Ecrase) and NbAcroiDistPasEcrase=1 then
                            NbMotsPasEcrase:=NbMotsPasEcrase+1;
                          end if;

                          if not(Bord_Image) then
                            NbAcroiDistPasBordImage:=NbAcroiDistPasBordImage+1;					
                          end if;

                          if not(Bord_Image) and NbAcroiDistPasBordImage=1 then
                            NbMotsPasBordImage:=NbMotsPasBordImage+1;					
                          end if;

				          if NbMotsPasEcrase=MT_TMP.Nb_Mots then
				            MT_TMP.ecrase:=false;
                          end if;

				          if NbMotsPasBordImage=MT_TMP.Nb_Mots then
				            MT_TMP.Bord_image:=false;
                          end if;

                          if not(ecrase) and not(bord_image) then
                            exit;
                          end if;

                        end if;

                      end loop;  -- boucle en AcroiDist (niveau d'emboitement 7)
	   
	                else
			
                      -- Debut du calcul precis de l'orientation de la boite englobante du mot courant
                      -- if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
					  if MemeSensD=True then
			  	        TailleLigneSupport:=TabAbsCurD(TabK(i))(tab_linD(TabK(i)+1).c_pt-tab_linD(TabK(i)).c_pt);
                        AbsCur1:=float(Fanion(i))*PAS_TOPO*TailleLigneSupport/Taille_segD(TabK(i));
                        AbsCur2:=(float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i)))*TailleLigneSupport/Taille_segD(TabK(i));
			  	        Jmin:=tab_linD(TabK(i)).c_pt;
				        Jmax:=tab_linD(TabK(i)+1).c_pt-1;
                      else
			  	        TailleLigneSupport:=TabAbsCurD(TabK(i))(tab_linD(C_linD-TabK(i)+1).c_pt-tab_linD(c_linD-TabK(i)).c_pt);
                        AbsCur1:=float(Fanion(i))*PAS_TOPO*TailleLigneSupport/Taille_segD(TabK(i));
                        AbsCur2:=(float(Fanion(i))*PAS_TOPO+float(MT_TMP.L_Topo(i)))*TailleLigneSupport/Taille_segD(TabK(i));
		  	            AbsCur:=AbsCur1;
			  	        abscur1:=TailleLigneSupport-Abscur2;
			  	        abscur2:=TailleLigneSupport-Abscur;
			  	        Jmin:=tab_linD(c_linD-TabK(i)).c_pt;
				        Jmax:=tab_linD(c_linD-TabK(i)+1).c_pt-1;
                      end if;
			          j1(i):=0;
			          J2(i):=0;
                      for J in Jmin..Jmax loop
				        if TabAbsCurD(TabK(I))(j-Jmin+1)>AbsCur1 and J1(i)=0 then
                          J1(i):=J;
                        end if;
				        if TabAbsCurD(TabK(i))(j-Jmin+1)>=AbsCur2 then
				          J2(i):=J+1;
		                  exit;
                        end if;
                      end loop;

			          -- debut du calcul des intersections de la ligne support avec sa corde (crit�re de r�gularit�)
			          if J1(i)/=J1Courant(i) or J2(i)/=J2Courant(i) then
		                J1J2(1):=Tab_TD(J1(i));
		                J1J2(2):=Tab_TD(J2(i));
	                    Intersections_de_polylignes(Tab_TD,J1J2,c_tD,2,Inters,NInters(I),false);
		                GR_Free_point_liste(Inters);
                      end if;
                      if NInters(I)>2 then
			            LongueurIntersCorde:=LongueurIntersCorde+MT_TMP.L_Topo(i);
                      end if;
			          -- fin du calcul des intersections de la ligne support avec sa corde (crit�re de r�gularit�)

                      if J1(i)/=J1Courant(i) or J2(i)/=J2Courant(i) then					  	
			            -- if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
					  	if MemeSensD=True then
                          Topo_Med.Angle(i):=Angle_horizontal(Tab_TD(J1(i)),Tab_TD(J2(i)));
                        else
                          Topo_Med.Angle(i):=Angle_horizontal(Tab_TD(J2(i)),Tab_TD(J1(i)));
                        end if;
	        	        C:=Topo_Med.Angle(i);
				        -- C est une valeur approchee de l'orientation
				        -- Calcul precis par moindres carres :
                        -- MC2(Tab_TD,j1(i),j2(i),a,b);
                        MCL2(Tab_TD,j1(i),j2(i),a,b);
			            if b/=0.0 then
                          Topo_Med.Angle(i):=ArcTan(-a/B);
			            else
                          Topo_Med.Angle(i):=PI/2.0;
                        end if;
			            if abs(Topo_Med.Angle(i)-C)>Pi/2.0 then
                          if Topo_Med.Angle(i)<0.0 then
				            Topo_Med.Angle(i):=Topo_Med.Angle(i)+Pi;
			              else
				  	        Topo_Med.Angle(i):=Topo_Med.Angle(i)-Pi;
                          end if;
                        end if;
				        D:=Topo_Med.Angle(i);
			            if abs(c-d)<0.2 then
	   					  AnglePrecisSansPlusPi(i):=D;
                        else -- dans les cas rares et non identifies ou C et D sont tres differents, c'est la valeur approchee qu'on retient
						  AnglePrecisSansPlusPi(i):=C;
                        end if;
				        J1Courant(i):=J1(i);
				        J2Courant(i):=J2(i);
                      end if;
                      -- Fin du calcul precis de l'orientation de la boite englobante du mot courant
			  
                      NbAcroiDistPasEcrase:=0;
                      NbAcroiDistPasBordImage:=0;

		              -- niveau d'emboitement 7 : boucle sur l'accroissement de la distance entre le mot courant de l'�criture et l'objet lin�aire nomm�
                      for AcroiDist in 0..MaxAcroiDist loop

                        Topo_Med.Angle(I):=AnglePrecisSansPlusPi(i);

						-- debut d�calage de la position courante du mot courant perpendiculairement � l'objet lin�aire
                        MaxDS1:=-float'last;
                        MaxDS2:=-float'last;
                        MaxDS3:=-float'last;
			            N:=integer(Taille_segD(TabK(i))/PAS_TOPO);
			            if n=0 then
			              N:=1;
                        end if;

			            PT0:=(integer((1.0-float(Fanion(I))/float(N))*float(P1D(TabK(i)).coor_X)+(float(Fanion(I))/float(N))*float(P2D(TabK(i)).coor_X)),
                              integer((1.0-float(Fanion(I))/float(N))*float(P1D(TabK(i)).coor_Y)+(float(Fanion(I))/float(N))*float(P2D(TabK(i)).coor_Y)));
			            PT1:=(PT0.coor_x+integer(float(MT_TMP.L_Topo(i))*cos(Topo_Med.Angle(i))),
			                  PT0.coor_Y+integer(float(MT_TMP.L_Topo(i))*sin(Topo_Med.Angle(i))));
                        Orth.coor_X:=5*(PT0.coor_Y-PT1.coor_Y);
                        Orth.coor_y:=5*(PT1.coor_X-PT0.coor_X);
			            for E in 0..10 loop
				          PT:=(integer(float((10-E)*PT0.coor_X+E*PT1.coor_X)/10.0),
				               integer(float((10-E)*PT0.coor_Y+E*PT1.coor_Y)/10.0));
                          PTy(1):=(PT.coor_X+Orth.coor_X,PT.coor_Y+Orth.coor_Y);
		  	              PTy(2):=(PT.coor_X-Orth.coor_X,PT.coor_Y-Orth.coor_Y);
	                      Intersections_de_polylignes(Tab_TI,PTy,c_tI,2,IntersI,NIntersI,false);
				          if NintersI/=0 then
				            DSCarre:=distance_de_points(PT,IntersI(1));
				            if NIntersI>1 then
                              for num in 2..NIntersI loop
                                if distance_de_points(PT,IntersI(Num))<DSCarre then
                                  DSCarre:=distance_de_points(PT,IntersI(Num));
                                end if;
                              end loop;
                            end if;
				            if ((Topo_Med.Angle(i)>-Pi/2.0 and Topo_Med.Angle(i)<Pi/2.0) and PT.Coor_y>IntersI(1).Coor_y)
				            or ((Topo_Med.Angle(i)<-Pi/2.0 or Topo_Med.Angle(i)>Pi/2.0) and PT.Coor_y<IntersI(1).Coor_y) then
					          DS:=sqrt(DSCarre)/float(MT_TMP.corps);
                            else
                              DS:=-sqrt(DSCarre)/float(MT_TMP.corps);
                            end if;
				            if DS>maxDS1 then
				  	          MaxDS3:=MaxDS2;
				              MaxDS2:=MaxDS1;
				              MaxDS1:=DS;
			                else
			  	              if DS>maxDS2 then
					            MaxDS3:=MaxDS2;
 				                MaxDS2:=DS;
				              else
					            if DS>maxDS3 then
                                  MaxDS3:=DS;
                                end if;
                              end if;
                            end if;
			                GR_Free_point_liste(IntersI);
                          end if;
                        end loop;

			            if MaxDS3=-float'last then
			  	          if MaxDS2=-Float'last then
				            if MaxDS1=-Float'last then
	                          MaxDS1:=0.0;
	                          MaxDS3:=0.0;
                            else
				              MaxDS3:=MaxDS1;
                            end if;
			              else
				            MaxDS3:=MaxDS2;
                          end if;
                        end if;

                        topo_med.coin1(I).coor_x:=PT0.coor_x
			            +integer(((math_int_basic.max(MaxDS1-MaxDS3+DistMinY*DIST_H,DIST_H)+MaxDS3+1.0)*float(MT_TMP.corps)+L_M*float(resolution)/2.0)*Sin(Topo_Med.Angle(i)));
                        topo_med.coin1(I).coor_Y:=PT0.coor_Y
			            -integer(((math_int_basic.max(MaxDS1-MaxDS3+DistMinY*DIST_H,DIST_H)+MaxDS3+1.0)*float(MT_TMP.corps)+L_M*float(resolution)/2.0)*Cos(Topo_Med.Angle(i)));
						-- fin d�calage de la position courante du mot courant perpendiculairement � l'objet lin�aire

                        if ModePlacement=CAXE then
                          topo_med.coin1(I).coor_x:=topo_med.coin1(I).coor_x-integer(0.5*float(MT_TMP.corps)*Sin(Topo_Med.Angle(i)));
                          topo_med.coin1(I).coor_Y:=topo_med.coin1(I).coor_Y+integer(0.5*float(MT_TMP.corps)*cos(Topo_Med.Angle(i)));
                        end if;

			            -- retournement du mot si son orientation depasse la verticale
			            if Topo_Med.Angle(i)>Pi/2.0 or Topo_Med.Angle(i)<-Pi/2.0 then
                          topo_med.PlusPi(I):=true;
	                    else
                          topo_med.PlusPi(I):=false;
                        end if;

	                    Ecrase:=false;
                        bord_image:=false;

                        if mutil then
				  	
			              POIMUB_OBLIQUE(tpoi,origine_zie,topo_med.coin1(I),TOPO_MED.Angle(i),MT_TMP.L_TOPO(I),MT_TMP.corps,0,ecrase,bord_image,TOPO_MED.poim(I),ModePlacement,resolution);

                          if bord_image and MT_tMP.hors_image=true then
			                if est_il_dans_rectangle(Tab_TD(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_TD(c_tD),Pno,Pne,Pse,Pso)=false then
                              MT_tMP.hors_image:=True;
                              MT_tMP.court:=False;
                              MT_tMP.ecrase:=false;
                              MT_tMP.bord_image:=false;
                              MT_tMP.Agencement:=false;
					          GR_Free_point_liste(TOPO_MED.Geom);
				              return;
                            end if;
                          end if;

                          MT_tMP.hors_image:=false;

                          if ecrase and (MT_TMP.Bord_image=false or bord_image=true) and AcroiDist=MaxAcroiDist then
                            MotQuiEcrase:=I;
				            goto FinBoucleFanion;
			              end if;

                          if Bord_Image and (MT_TMP.Ecrase=false or Ecrase=true) and AcroiDist=MaxAcroiDist then
				            goto FinBoucleFanion;
			              end if;

                          if not(Ecrase) then
                            AuMoinsUnMotPasEcrase:=true;
                            NbAcroiDistPasEcrase:=NbAcroiDistPasEcrase+1;
                          end if;

                          if not(Ecrase) and NbAcroiDistPasEcrase=1 then
                            NbMotsPasEcrase:=NbMotsPasEcrase+1;
                          end if;

                          if not(Bord_Image) then
                            NbAcroiDistPasBordImage:=NbAcroiDistPasBordImage+1;					
                          end if;

                          if not(Bord_Image) and NbAcroiDistPasBordImage=1 then
                            NbMotsPasBordImage:=NbMotsPasBordImage+1;					
                          end if;

				          if NbMotsPasEcrase=MT_TMP.Nb_Mots then
				            MT_TMP.ecrase:=false;
                          end if;

				          if NbMotsPasBordImage=MT_TMP.Nb_Mots then
				            MT_TMP.Bord_image:=false;
                          end if;

                          if not(ecrase) and not(bord_image) then
                            exit;
                          end if;

                        end if;

                      end loop;  -- boucle en AcroiDist (niveau d'emboitement 7)
		            end if;

		          end loop;  -- boucle sur les mots (niveau d'emboitement 6)
           
                  if not mutil or (NbMotsPasEcrase=MT_TMP.Nb_Mots and NbMotsPasBordImage=MT_TMP.Nb_Mots) then

			        -- debut filtrage des ecritures qui se lisent trop a l'envers
                    Longueur_endroit:=0.0;
                    Longueur_envers:=0.0;
			        for i in 1..MT_TMP.Nb_Mots loop
                      if topo_med.PlusPi(I)=true then
                        Longueur_envers:=Longueur_envers+float(MT_TMP.L_Topo(i)); -- *(abs(Topo_Med.Angle(i))/(0.5*Pi+AngleMaxSupplRad));
				      else
                        Longueur_endroit:=Longueur_endroit+float(MT_TMP.L_Topo(i));
                      end if;
                    end loop;
			        proportionEnvers:=Longueur_envers/(Longueur_envers+Longueur_endroit);
			        if proportionEnvers>MaxProportionEnvers then
				      goto FinBoucleFanion;
                    end if;
			        -- fin de filtrage des ecritures qui se lisent trop a l'envers

                    -- debut filtrage des �critures dont la longueur des mots dont le support intersecte sa corde est trop grande
			        proportionIntersCorde:=float(LongueurIntersCorde)/float(Longueur_envers+Longueur_endroit);
			        if proportionIntersCorde>MaxProportionIntersCorde then
				      goto FinBoucleFanion;
                    end if;
                    -- fin filtrage des �critures dont la longueur des mots dont le support intersecte sa corde est trop grande

			        -- Mesure de l'homogeneite des distances realisees entre mots consecutifs
			        -- et filtrage des ecritures dont 2 mots consecutifs sont trop proches
                    if MT_TMP.Nb_Mots>=2 then
			          for i in 1..MT_TMP.Nb_Mots-1 loop
                        TabDist(i):=sqrt(float'last);
                      end loop;
			          MinDist:=float'last;
			          maxDist:=0.0;
			          MoyDist:=0.0;
                      for i in 1..MT_TMP.Nb_Mots-1 loop
                        if I=1 then
		                  QUATRE_COINS(topo_med.coin1(1),TOPO_MED.Angle(1),MT_TMP.L_Topo(1),MT_TMP.Corps,boite1);
	                    else
                          Boite1:=Boite2;
                        end if;
                        QUATRE_COINS(topo_med.coin1(I+1),TOPO_MED.Angle(i+1),MT_TMP.L_Topo(i+1),MT_TMP.Corps,boite2);
                        for j in 1..4 loop
                          case j is
                            when 1 => Point1:=Boite1.P1;
                            when 2 => Point1:=Boite1.P2;
                            when 3 => Point1:=Boite1.P3;
                            when 4 => Point1:=Boite1.P4;
                            when others => null;
                          end case;
                          for k in 1..4 loop
                            case k is
                              when 1 => Point2:=Boite2.P1;
                              when 2 => Point2:=Boite2.P2;
                              when 3 => Point2:=Boite2.P3;
                              when 4 => Point2:=Boite2.P4;
                              when others => null;
                            end case;
					        DistanceCarre:=Distance_de_points(Point1,Point2);
					        if DistanceCarre<TabDist(i)**2 then
					          TabDist(i):=sqrt(DistanceCarre);
                              if TabDist(i)<DistMinInterMots*float(MT_TMP.Corps) then
					            goto FinBoucleFanion;
                              end if;
                            end if;
                          end loop;
                        end loop;
				        if TabDist(i)<MinDist then
				          MinDist:=TabDist(i);
			            end if;
				        if TabDist(i)>MaxDist then
				          MaxDist:=TabDist(i);
			            end if;
				        MoyDist:=MoyDist+TabDist(i);
                      end loop;
                      MoyDist:=MoyDist/float(MT_TMP.Nb_Mots);
                    end if;
			        -- fin de la mesure de l'homogeneite des distances realisees entre mots consecutifs
			        -- et du filtrage des ecritures dont 2 mots consecutifs sont trop proches

			        MT_court:=false;  -- pas de prolongement du meta-troncon

                    -- debut de la mesure de l'�loignement des mots extr�mes par rapport aux extr�mit�s du meta-troncon
			        QUATRE_COINS(topo_med.coin1(1),TOPO_MED.Angle(1),MT_TMP.L_Topo(1),MT_TMP.Corps,boite1);
			        if MT_TMP.Nb_Mots>=2 then
			          QUATRE_COINS(topo_med.coin1(MT_TMP.Nb_Mots),TOPO_MED.Angle(MT_TMP.Nb_Mots),MT_TMP.L_Topo(MT_TMP.Nb_Mots),MT_TMP.Corps,boite2);
                    end if;
			        if dd=1 then
			  	      if TOPO_MED.PlusPi(1)=false then
				        TMC:=Boite1.p1;
				        TMC2:=Boite1.p3;
			          else
				        TMC:=Boite1.p3;
				        TMC2:=Boite1.p1;
                      end if;
                      TAB_Cale(1):=TAB_TG(1);
                      TAB_Cale(2):=TAB_TG(c_TG);
					  -- debut modif 06.04.2005
                      -- TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_TG,C_TG,TAB_Cale,2,TMC,TMC);
                      TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_TG,C_TG,TAB_Cale,2,TMC,TMC2);
					  -- fin modif 06.04.2005
                      if MT_TMP.Nb_Mots>=2 then
			  	        if TOPO_MED.PlusPi(MT_TMP.Nb_Mots)=false then
				          TMC:=Boite2.p4;
				          TMC2:=Boite2.p2;
			            else
				          TMC:=Boite2.p2;
				          TMC2:=Boite2.p4;
                        end if;
					    -- debut modif 06.04.2005
				        -- TOPO_MED.POID(MT_TMP.Nb_Mots):=POIDS_MILIEU_LOCAL(TAB_TG,C_TG,TAB_Cale,2,TMC,TMC);
				        TOPO_MED.POID(MT_TMP.Nb_Mots):=POIDS_MILIEU_LOCAL(TAB_TG,C_TG,TAB_Cale,2,TMC,TMC2);
					    -- fin modif 06.04.2005
                      end if;
                    else
			  	      if TOPO_MED.PlusPi(1)=false then
				        TMC:=Boite1.p2;
				        TMC2:=Boite1.p4;
			          else
				        TMC:=Boite1.p4;
				        TMC2:=Boite1.p2;
                      end if;
                      TAB_Cale(1):=TAB_TD(1);
                      TAB_Cale(2):=TAB_TD(c_TD);
					    -- debut modif 06.04.2005
                      -- TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_TD,C_TD,TAB_Cale,2,TMC,TMC);
                      TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_TD,C_TD,TAB_Cale,2,TMC,TMC2);
					    -- fin modif 06.04.2005
                      if MT_TMP.Nb_Mots>=2 then
			  	        if TOPO_MED.PlusPi(MT_TMP.Nb_Mots)=false then
				          TMC:=Boite2.p3;
				          TMC2:=Boite2.p1;
			            else
				          TMC:=Boite2.p1;
				          TMC2:=Boite2.p3;
                        end if;
					    -- debut modif 06.04.2005
                        -- TOPO_MED.POID(MT_TMP.Nb_Mots):=POIDS_MILIEU_LOCAL(TAB_TD,C_TD,TAB_Cale,2,TMC,TMC);
                        TOPO_MED.POID(MT_TMP.Nb_Mots):=POIDS_MILIEU_LOCAL(TAB_TD,C_TD,TAB_Cale,2,TMC,TMC2);
					    -- fin modif 06.04.2005
                      end if;
                    end if;
                    C1I:=topo_med.coin1(1);
                    C1F:=topo_med.coin1(MT_TMP.Nb_Mots);
                    TPDI:=TOPO_MED.POID(1);
                    TPDF:=TOPO_MED.POID(MT_TMP.Nb_Mots);
                    -- fin de la mesure de l'�loignement des mots extr�mes par rapport aux extr�mit�s du meta-troncon

                    Poids:=0.0;
			        for i in 1..MT_TMP.Nb_Mots loop
                      Poids:=Poids
				           -- poids de mutilation
				           +LE_COEF_MUTIL*(TOPO_MED.POIM(I)*TOPO_MED.POIM(I));
                    end loop;
			        Poids:=Poids/float(MT_TMP.Nb_Mots);
                    Poids:=Poids
                           -- crit�re de qualit� qui tend � �loigner les mots extr�mes de l'�critures des extr�mit�s du m�ta-troncon
			               +LE_COEF_CENTRE*(TOPO_MED.POID(1)*TOPO_MED.POID(1)+TOPO_MED.POID(MT_TMP.Nb_Mots)*TOPO_MED.POID(MT_TMP.Nb_Mots)) -- valeur dans [0,2.0*LE_COEF_CENTRE]
			               -- priorit� aux �critures en position sup
				           +LE_COEF_DAX*float(dd-1)                                          -- valeur 0 ou LE_COEF_DAX
					       -- critere de qualite qui tend a empecher les lettres de "danser"
					       +LE_COEF_CORDE*proportionIntersCorde/MaxProportionIntersCorde;    -- valeur dans [0,LE_COEF_CORDE]
			        if MaxProportionEnvers/=0.0 then
					       -- pr�f�rence sens de lecture a l'endroit
                      poids:=poids
				           +LE_COEF_ENVERS*proportionEnvers/MaxProportionEnvers;             -- valeur dans [0,LE_COEF_ENVERS]
			        end if;
			        if MT_TMP.Nb_Mots>=2 then
			          poids:=poids
				           -- homogeneite des distances realis�es entre mots cons�cutifs
				           +LE_COEF_HOMO*(1.0-minDist/MaxDist)                               -- valeur dans [0,LE_COEF_HOMO]
					       -- comparaison de l'inter-mots moyen et de l'inter-mots id�al = 75% de l'intermot max
                           +LE_COEF_IDEAL_DIST*abs(MoyDist-IdealDist)/math_int_basic.max(abs(IdealDist-float(MinInterMots*MT_TMP.Corps)),abs(float(MaxInterMots*MT_TMP.Corps)-IdealDist));
                                                                                       -- valeur dans [0,LE_COEF_IDEAL_DIST]
                    end if;
                    if DIST_H/=0.0 then
				      poids:=poids+3.0*float(MaxAcroiDist)*IncrAcroiDist/DIST_H;            -- valeur dans [0,3.0*MaxMaxAcroiDist*IncrAcroiDist/DIST_H]
                    end if;

                    if poids>10.0 then
                      topo_med.poip:=10.0;
                    else
                      topo_med.poip:=poids;
                    end if;

                    topo_med.nl:=dd;
			        BonPosCetInterMot:=BonPosCetInterMot+1;

                    -- debut enregistrement de la geometrie de la ligne support du mot
	                for i in 1..MT_TMP.Nb_Mots loop
                      if dd=1 then
                        TOPO_MED.NGeom(i):=J2(I)-J1(i)+1;
		  	            -- if tab_linG(1).coor_x<tab_linG(c_linG).coor_x then
					  	if MemeSensG=True then
                          for j in 1..TOPO_MED.NGeom(i) loop
			  	            TOPO_MED.Geom((I-1)*MaxNGeomMot+j):=TAB_TG(J1(I)+J-1);
                          end loop;
	                    else
                          for j in 1..TOPO_MED.NGeom(i) loop
			  	            TOPO_MED.Geom((I-1)*MaxNGeomMot+j):=TAB_TG(J2(I)-J+1);
                          end loop;
                        end if;
			          else
                        TOPO_MED.NGeom(i):=J2(I)-J1(i)+1;
		  	            -- if tab_linD(1).coor_x<tab_linD(c_linD).coor_x then
					  	if MemeSensD=True then
                          for j in 1..TOPO_MED.NGeom(i) loop
			  	            TOPO_MED.Geom((I-1)*MaxNGeomMot+j):=TAB_TD(J1(I)+J-1);
                          end loop;
		                else
                          for j in 1..TOPO_MED.NGeom(i) loop
			  	            TOPO_MED.Geom((I-1)*MaxNGeomMot+j):=TAB_TD(J2(I)-J+1);
                          end loop;
                        end if;
                      end if;
                    end loop;
                    -- fin enregistrement de la geometrie de la ligne support du mot

			        TOPO_MED.Nb_Mots:=MT_TMP.Nb_Mots;

			        for i in 1..MT_TMP.Nb_Mots loop
			          QUATRE_COINS(topo_med.coin1(I),TOPO_MED.Angle(I),MT_TMP.L_Topo(I),MT_TMP.Corps,boite1);
--			  	      if (dd=1 and TOPO_MED.PlusPi(I)=false) or (dd=2 and TOPO_MED.PlusPi(I)=true) then
--				        Point1:=boite1.P1;
--				        Point2:=boite1.P4;
--                      else
--				        Point1:=boite1.P2;
--				        Point2:=boite1.P3;
--                      end if;
--
--				      if TOPO_MED.PlusPi(I)=false then
--                        topo_med.Point1(I):=Point1;
--                        topo_med.Point2(I):=Point2;
--                      else
--                        topo_med.Point1(I):=Point2;
--                        topo_med.Point2(I):=Point1;
--                      end if;
                 
			  	      if dd=1 then
                        topo_med.Point1(I):=boite1.P1;
                        topo_med.Point2(I):=boite1.P4;
                      else
                        topo_med.Point1(I):=boite1.P2;
                        topo_med.Point2(I):=boite1.P3;
                      end if;

					  -- stockage de valeurs provisoires d'offset
                      if dd=1 then
				        TOPO_MED.offset(I):=1.0;
                      else
				        TOPO_MED.offset(I):=-1.0;
                      end if;
                    end loop;

	    	        RANGE_TOPO_DS_TAB(TAB_POS,TOPO_MED);

                  end if;

                 << FinBoucleFanion >>
                 for i in 1..MT_TMP.Nb_Mots loop
		           fanion(I):=Fanion(I)+1;	
                 end loop;

	            end loop;  -- boucle en fanion (niveau d'emboitement 5)
                << FinBoucleIter >>
		        null;
		        if BonPosCetInterMot>=1 then
                  BonPosInterMots:=BonPosInterMots+1;
			      goto FinBoucleInterMot;
                end if;
	          end loop;  -- boucle en Iter (niveau d'emboitement 4)
	          if BonPosInterMots>=SeuilBonPosInterMots then
                goto FinBoucleInterMot;
              end if;
	        end loop; -- boucle en AleaForce (niveau d'emboitement 3)
          end loop;  -- boucle en MaxAcroiDist (niveau d'emboitement 2)
          << FinBoucleInterMot >>
		null;
      end loop; -- boucle en InterMots (niveau d'emboitement 1)
	  << FinBoucleDd >>
	  null;
    end loop; -- boucle principale en Dd (niveau d'emboitement 0)
  END; --DECLARE--

  GR_Free_point_liste(TOPO_MED.Geom);

end CALCULE_POSITIONS_HYDRO;


-- Calcul et classement des occurrences d'�critures en mode de placement AXE ou COURBE
PROCEDURE CALCULE_POSITIONS_AXE(GRAPHE : graphe_type;
                                Legende : Legende_Type;
                                TAB_T : POINT_LISTE_TYPE;
                                C_T : Positive;
                                T_ARC_A	 : TGROUPEMENT;
                                TAB_NOEUDS : POINT_LISTE_TYPE;
                                CRS_NOEUDS : positive;
                                MT_TMP : in out MT;
                                TAB_POS : in out POSITION;
                                MT_COURT : out boolean;     
                                Zie : in out Boite_type;
								codetypo : in string30;
                                resolution : in positive;
								cale : boolean;
								Mode_P : in typcateg;
								MTD : boolean) is

bon_pos	: integer:=0;
xpixmin	: integer;
xpixmax	: integer;
ypixmin	: integer; 
ypixmax	: integer; 
npx		: positive:=1;
npy		: positive:=1;
ecrase : boolean:=false;
bord_image : boolean:=false;
P0		: point_type:=(0,0);
P1		: point_type;
P2		: point_type;
P3		: point_type;  
P4		: point_type;
Pi1		: point_type;
Pi2		: point_type;
TAB_LIN	: plt_opt(1..2*TAB_T'LAST);
C_LIN		: positive;
Ecart		: integer:=0;
TOPO_MED	: position_type;
EPS		: float:=0.0;  
Fanion	: integer;
pds_S		: typpoi;
k		: integer;
Taille_seg	: float;
l_m, l_m2 : float:=0.0;
dd : integer:=0;
origine_zie : point_type;

coin1T : Point_type_reel;
lps, ncol, Nlig : integer;
stripoffset : acces_tabstrip;
tinterp : acces_tabinterp;
tsubst : acces_tabsub;
nb_interp : integer;
poids : float:=0.0;

TAB_Cale : POINT_LISTE_TYPE(1..2);
bloc : bloc_type;

begin

  MT_COURT:=true;
  MT_TMP.Ecrase:=false;
  MT_TMP.bord_image:=false;
  MT_TMP.hors_image:=false;

  -- initialisation de Tab_pos
  for gh in tab_pos'first..tab_pos'last loop
 	tab_pos(gh).poip:=10.0;
	tab_pos(gh).poss:=false;       
  end loop;

  -- debut calcul de l'emprise de la portion d'image � charger
  if mutil then

  	for cg in 1..C_T loop
      l_m2:=gr_legende_l(legende,gr_arc(graphe,abs(T_arc_a(cg))).att_graph).largeur;
      if l_m2>l_m then 
        l_m:=l_m2;
      end if;
    end loop;

    Ecart:=integer(sqrt(float(MT_TMP.corps**2+MT_TMP.L_TOPO(1)**2)))
 	      +integer(3.0*float(MT_TMP.corps)*DIST_ROUTE
		  +l_m*float(Resolution_image)+EPS*PAS_TOPO)+1;


    Zie:=ZONE_INFLUENCE(TAB_T,C_T,Ecart);

    xpixmin:=math_int_basic.max(1,(zie.minimum.coor_x-pso.coor_x)/pt_image+1);
    xpixmax:=math_int_basic.min(delta_xpix_r,(zie.maximum.coor_x-pso.coor_x)/pt_image+1);
    ypixmin:=math_int_basic.min(delta_ypix_r,delta_ypix_r-((zie.minimum.coor_y-pso.coor_y)/pt_image)); 
    ypixmax:=math_int_basic.max(1,delta_ypix_r-((zie.maximum.coor_y-pso.coor_y)/pt_image)); 

    origine_Zie:=(math_int_basic.max(zie.minimum.coor_x,pso.coor_x),math_int_basic.max(zie.minimum.coor_y,pso.coor_y));

    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
      MT_TMP.hors_image:=true;
	  return;
	end if;

    npx:=xpixmax-xpixmin+1;
    npy:=ypixmin-ypixmax+1;--NB:coordonnees en y inversees!!! 
  end if;
  -- fin calcul de l'emprise de la portion d'image � charger

  -- linearisation de la portion d'objet lin�aire nomm�e
  if mode_P=Axe then
    SEG_MCL(Tab_T,C_T,TAB_LIN,C_LIN,SEUIL_S*float(resolution));
  else
    SEG_MCL(Tab_T,C_T,TAB_LIN,C_LIN,SEUIL_S_Courbe*float(resolution));
  end if;


  DECLARE
    tpoi : typtipoi(1..npx,1..npy);

  begin

    -- debut chargement de la portion d'image concernee dans tpoi
    if mutil then
      tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
      substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
      substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,codetypo, lps, ncol, stripoffset,tinterp, tsubst, nb_interp, tpoi);
	  Free_tabstrip(stripoffset);
      byte_io.Close(file_image);
      MT_TMP.Ecrase:=true;
      MT_TMP.bord_image:=true;
    end if;
    -- fin chargement de la portion d'image concernee dans tpoi

    k:=1;

    -- niveau d'emboitement 0 : boucle sur les segments de la lin�arisation
    while k<=c_lin-1 loop    

      -- affectation de P1 et P2, extr�mit�s gauche et droite du segment courant
      if tab_lin(k).coor_x<tab_lin(k+1).coor_x then 
        p1.coor_x:=tab_lin(k).coor_x;
        p1.coor_y:=tab_lin(k).coor_y;              
        p2.coor_x:=tab_lin(k+1).coor_x;
        p2.coor_y:=tab_lin(k+1).coor_y;
      else 
        p2.coor_x:=tab_lin(k).coor_x;
        p2.coor_y:=tab_lin(k).coor_y;
        p1.coor_x:=tab_lin(k+1).coor_x;
        p1.coor_y:=tab_lin(k+1).coor_y;
      end if;

      Taille_seg:=norme_v2(p1,p2);

	  -- on passe les segments trop petits
      If float(MT_TMP.L_TOPO(1))<Taille_seg+EPS*PAS_TOPO then

        MT_TMP.Ecrase:=true;
        MT_TMP.bord_image:=true;
	    dd:=0;
        fanion:=0;

        -- niveau d'emboitement 1 : boucle sur la position de l'�criture le long du segment courant
        while PAS_TOPO*float(fanion)<Taille_seg+EPS*PAS_TOPO-float(MT_TMP.L_TOPO(1)) loop

          TOPO_MED.ANGLE(1):=Angle_horizontal(p1,p2);

	      p3.coor_x:=integer(PAS_TOPO)*(fanion-integer(EPS/2.0));
          p3.coor_y:=0;
          p4.coor_x:=p3.coor_x+MT_TMP.L_TOPO(1);
          p4.coor_y:=0;
     	  fanion:=fanion+1;

          -- calcul de la position courante de l'�criture en prenant en compte l'epaisseur du symbole
          -- (rectangle orient� defini dans le sens horaire par P3,Pi1,P4,Pi2 en partant du coin inferieur gauche)
          -- et de la variable pds_s qui mesure la rectitude de la portion de lin�aire support

          if mode_P=Axe then
            DETECTION_PORTION(Graphe, Legende,T_ARC_A, TAB_T,
				            tab_lin(k).c_pt,tab_lin(k+1).c_pt,
				            C_Repere(p3,p1,-topo_med.angle(1)),
				            C_Repere(p4,p1,-topo_med.angle(1)),
				            0,MT_TMP.CORPS,p3.coor_y,pds_S,resolution,SEUIL_S,DIST_ROUTE);
          else -- mode_P=Courbe
            DETECTION_PORTION(Graphe, Legende,T_ARC_A, TAB_T,
				            tab_lin(k).c_pt,tab_lin(k+1).c_pt,
				            C_Repere(p3,p1,-topo_med.angle(1)),
				            C_Repere(p4,p1,-topo_med.angle(1)),
				            0,MT_TMP.CORPS,p3.coor_y,pds_S,resolution,SEUIL_S_Courbe,DIST_ROUTE);
          end if;

	      p3.coor_y:=p3.coor_y-integer(MT_TMP.CORPS/2);
          p4.coor_y:=p3.coor_y+integer(MT_TMP.CORPS);
          pi1.coor_x:=p3.coor_x;
	      pi1.coor_y:=p4.coor_y;
          pi2.coor_x:=p4.coor_x;
	      pi2.coor_y:=p3.coor_y;
          pi1:=C_Repere(pi1,p1,-topo_med.angle(1));
          pi2:=C_Repere(pi2,p1,-topo_med.angle(1));
          topo_med.coin1(1):=C_Repere(p3,p1,-topo_med.angle(1));
          p4:=C_Repere(p4,p1,-topo_med.angle(1));

	      Ecrase:=false;
          bord_image:=false;

          -- debut calcul de la mutilation de l'image par l'�criture
          if mutil then
            POIMUB_OBLIQUE(tpoi,Origine_zie,topo_med.coin1(1),topo_med.angle(1),MT_TMP.L_TOPO(1),MT_TMP.corps,dd,ecrase,bord_image,TOPO_MED.poim(1),mode_p,resolution);
            if not(ecrase) and not(bord_image) then
              MT_TMP.Ecrase:=false;
            end if;
            if not(bord_image) then
              MT_TMP.bord_image:=false;
            end if;
            if bord_image and (est_il_dans_rectangle(Tab_T(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_T(c_t),Pno,Pne,Pse,Pso)=false) then
              MT_TMP.bord_image:=false;
              MT_TMP.hors_image:=true;
            end if;
          end if;
          -- fin calcul de la mutilation de l'image par l'�criture

          if not(ecrase) and not(bord_image) then
	        topo_med.poss:=true;

			if mode_P=Axe then
			  -- debut calcul du poids de centrage
              if cale=false then         -- on favorise les positions centr�es
                TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,
                ((pi1.coor_x+topo_med.coin1(1).coor_x)/2,(pi1.coor_y+topo_med.coin1(1).coor_y)/2),
                ((p4.coor_x+pi2.coor_x)/2,(p4.coor_y+pi2.coor_y)/2));
              else                       -- on favorise les positions cal�es vers les extr�mit�s
                TAB_Cale(1):=TAB_T(1);
                TAB_Cale(2):=TAB_T(integer(float(c_T)/2.0));
                TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,
                ((pi1.coor_x+topo_med.coin1(1).coor_x)/2,(pi1.coor_y+topo_med.coin1(1).coor_y)/2),
                ((p4.coor_x+pi2.coor_x)/2,(p4.coor_y+pi2.coor_y)/2));
		      end if;
			  -- fin calcul du poids de centrage

			  poids:=COEF_S*(PDS_S*PDS_S)
				    +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))
				    +COEF_MUTIL*(TOPO_MED.POIM(1)*TOPO_MED.POIM(1))
				    +COEF_ANGLE*(TOPO_MED.angle(1))*(TOPO_MED.angle(1))/((PI/2.0)*(PI/2.0));

            else -- mode_P=Courbe
				
			  poids:=COEF_S_COURBE*(PDS_S*PDS_S)
				    +COEF_MUTIL_COURBE*(TOPO_MED.POIM(1)*TOPO_MED.POIM(1));

			  -- debut calcul du poids de centrage
              TAB_Cale(1):=TAB_T(1);
              TAB_Cale(2):=TAB_T(c_T);
			  -- debut modif 06.04.2005
		 	  -- TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,topo_med.coin1(1),topo_med.coin1(1));
		 	  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,((pi1.coor_x+topo_med.coin1(1).coor_x)/2,(pi1.coor_y+topo_med.coin1(1).coor_y)/2),((p4.coor_x+pi2.coor_x)/2,(p4.coor_y+pi2.coor_y)/2));
			  -- fin modif 06.04.2005
			  -- fin calcul du poids de centrage

              poids:=poids+COEF_DL_COURBE*(TOPO_MED.POID(1)*TOPO_MED.POID(1));
            end if;

            -- debut orientation des cotes de courbes lorsque les courbes sont orient�es amont � droite
            if mode_p=courbe and AMONT_A_DROITE=true then
			  if (MTD=false -- MTG ou MTI : les points de TAB_T et Tab_Lin sont list�s dans le meme ordre que le graphe donc : amont a droite
                  and tab_lin(k).coor_x<tab_lin(k+1).coor_x)
		      or (MTD=true -- MTD : les points de TAB_T et Tab_Lin sont list�s dans l'ordre inverse de celui du graphe donc : amont a gauche
                  and tab_lin(k).coor_x>tab_lin(k+1).coor_x) then

                quatre_coins(topo_med.coin1(1),TOPO_MED.angle(1),MT_TMP.L_TOPO(1),MT_TMP.corps,bloc);
                topo_med.coin1(1):=bloc.p3;
				if TOPO_MED.angle(1)<0.0 then
				  tOPO_MED.angle(1):=TOPO_MED.angle(1)+Pi;
                else
				  TOPO_MED.angle(1):=TOPO_MED.angle(1)-Pi;		  	
                end if;

				poids:=poids+PENALITE_SENS_DE_LECTURE;
              end if;
		    end if;
            -- fin orientation des cotes de courbes lorsque les courbes sont orient�es amont � droite

            if poids>10.0 then
              topo_med.poip:=10.0;
            else
              topo_med.poip:=poids;
            end if;

            topo_med.nl:=1;
		    bon_pos:=bon_pos+1;

	    	RANGE_TOPO_DS_TAB(TAB_POS,TOPO_MED);
            topo_med.plusPi(1):=false;
  	      end if;

		  if (PAS_TOPO*float(bon_pos)>float(MT_TMP.L_TOPO(1))) then
			MT_COURT:=false;
		  end if;
        end loop;	    
	  end if; 
      K:=K+2;
    end loop;
  END; --DECLARE
  if MT_TMP.bord_image=true and MT_TMP.ecrase=true then -- on a rencontre que des segments trop petits
    MT_TMP.bord_image:=false;
	MT_TMP.ecrase:=false;
  end if; 
end CALCULE_POSITIONS_AXE;	

-- Calcul et classement des occurrences d'�critures en mode de placement DROIT
PROCEDURE CALCULE_POSITIONS_DROIT(GRAPHE : graphe_type;
                                  Legende : Legende_Type;
                                  TAB_T : POINT_LISTE_TYPE;
                                  C_T : Positive;
                                  T_ARC_A	 : TGROUPEMENT;
                                  TAB_NOEUDS : POINT_LISTE_TYPE;
                                  CRS_NOEUDS : positive;
                                  MT_TMP : in out MT;
                                  TAB_POS : in out POSITION;
                                  MT_COURT : out boolean;     
                                  Zie : in out Boite_type;
								  codetypo : in string30;
                                  resolution : in positive;
                                  cale : boolean) is

bon_pos	: integer:=0;
xpixmin	: integer;
xpixmax	: integer;
ypixmin	: integer; 
ypixmax	: integer; 
npx     : positive:=1;
npy     : positive:=1;
ecrase  : boolean:=false;
bord_image  : boolean:=false;
P0		: point_type:=(0,0);
P1		: point_type;
P2		: point_type;
P3		: point_type;  
P4		: point_type;
Pi1		: point_type;
Pi2		: point_type;
TAB_LIN	: plt_opt(1..2*TAB_T'LAST);
C_LIN		: positive;
Ecart		: integer:=0;
TOPO_MED	: position_type;
EPS		: float:=0.0;  
Fanion	: integer;
pds_S		: typpoi;
k		: integer;
Taille_seg	: float;
dd : integer:=0;
origine_zie : point_type;
Angle : float;
Centre, Centrage_oriente, coin1 : point_type;
Centrage : point_type_reel;
cs, Sn : float;
CentreT : Point_type_reel;
Coin1T : Point_type_reel;
TopoMedCoin1T : Point_type_reel;

lps, ncol, Nlig : integer;
stripoffset : acces_tabstrip;
tinterp : acces_tabinterp;
tsubst : acces_tabsub;
nb_interp : integer;
poids : float:=0.0;

TAB_Cale : POINT_LISTE_TYPE(1..2);

begin

  MT_COURT:=true;
  MT_TMP.Ecrase:=false;
  MT_TMP.bord_image:=false;
  MT_TMP.hors_image:=false;

  -- initialisation de Tab_pos
  for gh in tab_pos'first..tab_pos'last loop
 	tab_pos(gh).poip:=10.0;
	tab_pos(gh).poss:=false;       
  end loop;

  -- debut calcul de l'emprise de la portion d'image � charger
  if mutil then

	Ecart:=integer(math_int_basic.Max(float(MT_TMP.corps),float(MT_TMP.L_TOPO(1))));

    Zie:=ZONE_INFLUENCE(TAB_T,C_T,Ecart);

    xpixmin:=math_int_basic.max(1,(zie.minimum.coor_x-pso.coor_x)/pt_image+1);
    xpixmax:=math_int_basic.min(delta_xpix_r,(zie.maximum.coor_x-pso.coor_x)/pt_image+1);
    ypixmin:=math_int_basic.min(delta_ypix_r,delta_ypix_r-((zie.minimum.coor_y-pso.coor_y)/pt_image)); 
    ypixmax:=math_int_basic.max(1,delta_ypix_r-((zie.maximum.coor_y-pso.coor_y)/pt_image)); 

    origine_Zie:=(math_int_basic.max(zie.minimum.coor_x,pso.coor_x),math_int_basic.max(zie.minimum.coor_y,pso.coor_y));

    if xpixmax-xpixmin<0 or ypixmin-ypixmax<0 then
      MT_TMP.hors_image:=true;
	  return;
	end if;

    npx:=xpixmax-xpixmin+1;
    npy:=ypixmin-ypixmax+1;  --NB:coordonnees en y inversees!!!
  end if;
  -- fin calcul de l'emprise de la portion d'image � charger

  -- linearisation de la portion d'objet lin�aire nomm�e
  SEG_MCL(Tab_T,C_T,TAB_LIN,C_LIN,SEUIL_S*float(resolution));

  DECLARE
    tpoi : typtipoi(1..npx,1..npy);

  begin

    -- debut chargement de la portion d'image concernee dans tpoi
    if mutil then
      tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
      substitution_io.Charge_tabsub(tsubst, tinterp, nb_interp);
      byte_io.open(file_image, byte_io.in_file, nomfichier_image(1..ncar_fi));
      substitution_io.Remplir_tpoi(file_image, xpixmin, ypixmin, xpixmax, ypixmax,codetypo, lps, ncol, stripoffset,tinterp, tsubst, nb_interp, tpoi);
	  Free_tabstrip(stripoffset);
      byte_io.Close(file_image);
	  MT_TMP.Ecrase:=true;
      MT_TMP.bord_image:=true;
    end if;
    -- fin chargement de la portion d'image concernee dans tpoi

    k:=1;

    -- niveau d'emboitement 0 : boucle sur les segments de la lin�arisation
    while k<=c_lin-1 loop    

      -- affectation de P1 et P2, extr�mit�s gauche et droite du segment courant
      if tab_lin(k).coor_x<tab_lin(k+1).coor_x then 
        p1.coor_x:=tab_lin(k).coor_x;
        p1.coor_y:=tab_lin(k).coor_y;              
        p2.coor_x:=tab_lin(k+1).coor_x;
        p2.coor_y:=tab_lin(k+1).coor_y;
      else
        p2.coor_x:=tab_lin(k).coor_x;
        p2.coor_y:=tab_lin(k).coor_y;
        p1.coor_x:=tab_lin(k+1).coor_x;
        p1.coor_y:=tab_lin(k+1).coor_y;
      end if;
      TOPO_MED.ANGLE(1):=Angle_horizontal(p1,p2);

      Taille_seg:=norme_v2(p1,p2);

	  -- on passe les segments trop petits
      If float(MT_TMP.corps)<Taille_seg+EPS*PAS_TOPO then   -- plutot abs(p1y-p2y)
	    dd:=0;
        fanion:=0;

        -- niveau d'emboitement 1 : boucle sur la position de l'�criture le long du segment courant
        while PAS_TOPO*float(fanion)<Taille_seg+EPS*PAS_TOPO-float(MT_TMP.L_TOPO(1)) loop
		  	
          p3.coor_x:=integer(PAS_TOPO)*(fanion-integer(EPS/2.0));
          p3.coor_y:=0;

          p4.coor_x:=p3.coor_x+MT_TMP.L_TOPO(1);
          p4.coor_y:=0;
	      fanion:=fanion+1;

          -- calcul de la position courante de l'�criture en prenant en compte l'epaisseur du symbole
          -- (rectangle orient� defini dans le sens horaire par P3,Pi1,P4,Pi2 en partant du coin inferieur gauche)
          -- et de la variable pds_s qui mesure la rectitude de la portion de lin�aire support

          DETECTION_PORTION(Graphe, Legende,T_ARC_A, TAB_T,
				            tab_lin(k).c_pt,tab_lin(k+1).c_pt,
				            C_Repere(p3,p1,-topo_med.angle(1)),
				            C_Repere(p4,p1,-topo_med.angle(1)),
				            0,MT_TMP.CORPS,p3.coor_y,pds_S,resolution,SEUIL_S,DIST_ROUTE);

	      p3.coor_y:=p3.coor_y-integer(MT_TMP.CORPS/2);
          p4.coor_y:=p3.coor_y+integer(MT_TMP.CORPS);
          pi1.coor_x:=p3.coor_x;
	      pi1.coor_y:=p4.coor_y;
          pi2.coor_x:=p4.coor_x;
	      pi2.coor_y:=p3.coor_y;
          pi1:=C_Repere(pi1,p1,-topo_med.angle(1));
          pi2:=C_Repere(pi2,p1,-topo_med.angle(1));
          topo_med.coin1(1):=C_Repere(p3,p1,-topo_med.angle(1));
          p4:=C_Repere(p4,p1,-topo_med.angle(1));

          cs:=cos(topo_med.angle(1));
          sn:=sin(topo_med.angle(1));
          centrage:=(float(MT_TMP.L_TOPO(1)/2),float(MT_TMP.corps/2));
		  Centrage_oriente.coor_x:=integer(centrage.coor_x*cs-centrage.coor_y*Sn); --*Float(InvEchelle)/(1000.0*Float(Resolution));
		  Centrage_oriente.coor_Y:=integer(centrage.coor_Y*cs+centrage.coor_X*Sn); --*Float(InvEchelle)/(1000.0*Float(Resolution));

          Centre:=topo_med.coin1(1)+Centrage_oriente;

          Coin1.coor_x:=centre.coor_x+integer(Cs*float(topo_med.coin1(1).coor_x-centre.coor_x)+Sn*float(topo_med.coin1(1).coor_Y-centre.coor_y));
          Coin1.coor_Y:=centre.coor_y+integer(-Sn*float(topo_med.coin1(1).coor_x-centre.coor_x)+Cs*float(topo_med.coin1(1).coor_Y-centre.coor_y));

	      topo_med.coin1(1):=coin1; 

	      Ecrase:=false;
          bord_image:=false;

          Angle:=TOPO_MED.Angle(1);
          TOPO_MED.Angle(1):=0.0;

          -- debut calcul de la mutilation de l'image par l'�criture
		  if mutil then
            POIMUB_OBLIQUE(tpoi,origine_zie,topo_med.coin1(1),topo_med.angle(1),MT_TMP.L_TOPO(1),MT_TMP.corps,dd,ecrase,bord_image,TOPO_MED.poim(1),Droit,resolution);

            if not(ecrase) and not(bord_image) then
              MT_TMP.Ecrase:=false;
            end if;
            if not(bord_image) then
              MT_TMP.bord_image:=false;
            end if;
            if bord_image then
              if (est_il_dans_rectangle(Tab_T(1),Pno,Pne,Pse,Pso)=false or est_il_dans_rectangle(Tab_T(c_t),Pno,Pne,Pse,Pso)=false) then
                MT_TMP.bord_image:=false;
                MT_TMP.hors_image:=true;
              end if;
            end if;
          end if;
          -- fin calcul de la mutilation de l'image par l'�criture

          TOPO_MED.Angle(1):=angle;

          if not(ecrase) and not(bord_image) then
	        topo_med.poss:=true;

			-- debut calcul du poids de centrage
            if cale=false then       -- on favorise les positions centr�es
	    	  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_noeuds,crs_noeuds,
              ((pi1.coor_x+topo_med.coin1(1).coor_x)/2,(pi1.coor_y+topo_med.coin1(1).coor_y)/2),
              ((p4.coor_x+pi2.coor_x)/2,(p4.coor_y+pi2.coor_y)/2));
            else                     -- on favorise les positions cal�es vers les extr�mit�s
              TAB_Cale(1):=TAB_T(1);
              TAB_Cale(2):=TAB_T(integer(float(c_T)/2.0));
	    	  TOPO_MED.POID(1):=POIDS_MILIEU_LOCAL(TAB_T,C_T,TAB_Cale,2,
              ((pi1.coor_x+topo_med.coin1(1).coor_x)/2,(pi1.coor_y+topo_med.coin1(1).coor_y)/2),
              ((p4.coor_x+pi2.coor_x)/2,(p4.coor_y+pi2.coor_y)/2));
            end if;
			-- fin calcul du poids de centrage

			poids:=COEF_S*(PDS_S*PDS_S)
                  +COEF_DL*(TOPO_MED.POID(1)*TOPO_MED.POID(1))
                  +COEF_MUTIL*(TOPO_MED.POIM(1)*TOPO_MED.POIM(1))
                  +COEF_ANGLE*(PI/2.0-abs(TOPO_MED.angle(1)))*(PI/2.0-abs(TOPO_MED.angle(1)))/((PI/2.0)*(PI/2.0));
            if poids>10.0 then
              topo_med.poip:=10.0;
            else
              topo_med.poip:=poids;
            end if;

            topo_med.nl:=1;
		    bon_pos:=bon_pos+1;

            Angle:=TOPO_MED.Angle(1);
            TOPO_MED.Angle(1):=0.0;
	    	RANGE_TOPO_DS_TAB(TAB_POS,TOPO_MED);
            TOPO_MED.Angle(1):=angle;

		  end if;
		  if (PAS_TOPO*float(bon_pos)>float(MT_TMP.L_TOPO(1))) then
		    MT_COURT:=false;
		  end if; 
	    end loop;	    
	  end if; 
      K:=K+2;
    end loop;
	if MT_TMP.bord_image=true and MT_TMP.ecrase=true then -- on a rencontre que des segments trop petits
      MT_TMP.bord_image:=false;
	  MT_TMP.ecrase:=false;
    end if; 
  END; --DECLARE--
end CALCULE_POSITIONS_DROIT;


PROCEDURE CALCULE_POSITIONS_QCQ(GRAPHE : graphe_type;
                                Legende : Legende_Type;
                                TAB_T : POINT_LISTE_TYPE;
                                C_T : integer;
                                T_ARC_A : TGROUPEMENT;
                                TAB_T2 : POINT_LISTE_TYPE;
                                C_T2 : integer;
                                T_ARC_A2 : TGROUPEMENT;
                                TAB_NOEUDS : POINT_LISTE_TYPE;
                                CRS_NOEUDS : positive;
                                MT_TMP : in out MT;
                                TAB_POS : in out POSITION;
                                MT_COURT : out boolean;     
                                Zie : in out Boite_type;
                                RTE : ROUTES;
                                Groupe : integer;
                                resolution : in positive;
								cale : boolean;
								MTD : boolean;
                                TAB_T3 : POINT_LISTE_TYPE;
                                C_T3 : integer;
								l_m : in float) is

Longueur_TAB_T : float;
Ymax : integer:=integer'first;
Ymin : integer:=integer'last;

Begin

  case Rte.mode_P is
    when axe => Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
                  if Longueur_TAB_T<float(MT_TMP.L_Topo(1)) then
                    MT_TMP.court:=true;
                  else
                    MT_TMP.court:=false;
                  end if;
                  CALCULE_POSITIONS_AXE(graphe,legende,TAB_T,C_T,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,RTE.Mode_P,MTD);
                  -- mt_tmp.t_plt:=false;
    when Courbe => Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
                   if Longueur_TAB_T<FILTRE_COURBE*float(MT_TMP.L_Topo(1)) then
                     MT_TMP.court:=true;
	   			     for i in 1..maxP loop
				  	   TAB_POS(I).poss:=false;
                     end loop;
				     return;
                   else  
                     MT_TMP.court:=false;
                   end if;
                   CALCULE_POSITIONS_AXE(graphe,legende,TAB_T,C_T,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,Cale,RTE.Mode_P,MTD);
    when droit => for i in 1..C_T loop
                    if TAB_T(i).coor_Y>Ymax then
                      Ymax:=TAB_T(i).coor_Y;
                    end if;
                    if TAB_T(i).coor_Y<Ymin then
                      Ymin:=TAB_T(i).coor_Y;
                    end if;
                  end loop;
	              if (Ymax-Ymin)<MT_TMP.corps then                    
				    MT_TMP.court:=true;
                  else
                    MT_TMP.court:=false;
                  end if;
	              CALCULE_POSITIONS_DROIT(graphe,legende,TAB_T,C_T,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale);

    when Desaxe => Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
                   if Longueur_TAB_T<float(MT_TMP.L_Topo(1)) then
                     MT_TMP.court:=true;
                   else
                     MT_TMP.court:=false;
                   end if;
	               CALCULE_POSITIONS(graphe,legende, TAB_T,C_T,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,Rte.kilo);

    when Decale => Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
                   if Longueur_TAB_T<float(MT_TMP.L_Topo(1)) then
                     MT_TMP.court:=true;
                   else
                     MT_TMP.court:=false;
                   end if;
	               CALCULE_POSITIONS_DECALE(graphe,legende, TAB_T,C_T,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,Rte.kilo);

    when CAxe => -- Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
--                   if Longueur_TAB_T<float(MT_TMP.L_Topo) then
--                     MT_TMP.court:=true;
--                   else
--                     MT_TMP.court:=false;
--                   end if;

--                   for i in 1..MaxP loop
--                     TAB_POS(i).Geom:=new point_liste_type(1..MaxNGeom*NbMaxMots);
--                   end loop;

	               -- CALCULE_POSITIONS_HYDRO(graphe,legende, TAB_T,C_T, T_ARC_A, TAB_T,C_T, T_ARC_A, MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,0.0,TAB_T3,C_T3,CAxe);
	               CALCULE_POSITIONS_HYDRO(graphe,legende,TAB_T,C_T,TAB_T,C_T,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,0.0,TAB_T3,C_T3,CAxe,l_m);
    when others => -- CDesaxe
--                   for i in 1..MaxP loop
--                     TAB_POS(i).Geom:=new point_liste_type(1..MaxNGeom*NbMaxMots);
--                   end loop;
	               CALCULE_POSITIONS_HYDRO(graphe,legende,TAB_T,C_T,TAB_T2,C_T2,MT_TMP,TAB_POS,MT_COURT,ZIe,RTE.CODE,resolution,cale,DIST_HYDRO,TAB_T3,C_T3,CDesaxe,l_m);
				   	
--				   gb.MsgBox("apres CALCULE_POSITIONS_HYDRO");
--	               gb.MsgBox(integer'image(TAB_POS(1).NGeom(1)));

  end case;
End CALCULE_POSITIONS_QCQ;

function longueur(nom : in string30;
                  ncar : in integer;
                  info_font: in typtfont;
                  Nofont : in integer;
                  hdc: Win32.windef.hdc) return integer is

use type win32.int;
use type win32.long;
                                   
taille: aliased win32.windef.size; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
taille_m: float;
ch: string(1..20);
n: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
H_400: integer;
T: float;
AOC: Win32.PSTR; 

                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC1(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC2(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC3(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
--function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
--  Ada.Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
--                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------
                              
begin

 -- creer le police logique
    Log_Font.lfFaceName:=info_font(Nofont).police.lfFaceName;
    -- on charge systematiquement une police de corps 400
    Log_Font.lfHeight 		  :=win32.long(-400);
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
      
    Font_Handle:=win32.wingdi.CreateFontIndirect(
          Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police    
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
--    if (fhandle = Null) then  put_line("erreur de chargement de police");
--       get_line(ch, n); -- se produit quand erreur dans hdc
--    end if;
    
    -- caracteristiques de la police
    res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
    
    -- Corps en points=lptm.tmheight-lptm.tmInternalleading
    -- Hauteur des majuscules=lptm.tmheight-lptm.tmdescent-lptm.tmInternalleading
    -- Hauteur des majuscules=Corps-lptm.tmdescent
    H_400:= Integer(400-lptm.tmdescent);
    T:=float(info_font(Nofont).hauteur)*float(25.4/(72.0*400.0))*float(ptparmm);
       
    -- calculer l'extension
    res:=win32.wingdi.getTextExtentPoint32(Hdc,String_to_PCCH(nom(1..ncar)),win32.int(ncar),taille'unchecked_Access);
							  
	res:=win32.wingdi.deleteobject(Font_Handle);

    return(integer(T*float(taille.cx)));
   
end longueur;

function Det_longueur(nom : string30;
                  Ncar : integer;
                  info_font: in typtfont;
                  Nofont : in integer;
                  hWnd : in Win32.winnt.Handle) return integer is

hDC : Win32.windef.hdc;
ps : Win32.winuser.LPPAINTSTRUCT;

begin
  ps  := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd, ps);
  Free_LPPAINTSTRUCT(ps);
  return(longueur(nom,Ncar,info_font,Nofont,hdc));
end Det_longueur;



function corps(info_font: in typtfont;
               Nofont : in integer;
               hdc: Win32.windef.hdc) return integer is

use type win32.int;
use type win32.long;
                                   
taille: aliased win32.windef.size; 
res: win32.bool;
Font_Handle, FHandle: aliased Win32.Windef.Hfont; --handle de police
Log_font: aliased Win32.wingdi.Logfont; -- police
taille_m: float;
ch: string(1..20);
n: integer;
lptm: aliased win32.wingdi.TEXTMETRIC; --caracteristiques de la police
--H_400: win32.long;
H_400: integer;
T: float;
AOC: Win32.PSTR; 

                         ------------------------------------------
-- conversion de string  en  Win32.PCHAR (pointeur sur un caractere constant)
function String_to_PCCH(C_Str : String) return Win32.PCCH is
  -- conversion de System.Address en Win32.PCHAR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.PCCH);
begin
  return UC1(C_Str(C_Str'First)'Address);
end String_to_PCCH; 
                              ------------------------------------------
-- conversion de string en  Win32.LPCSTR (pointeur sur un caractere)
function Str_to_LPCSTR (Str : string) return Win32.LPCSTR is
  -- conversion de System.Address en Win32.LPCSTR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.LPCSTR);
begin
  return UC2(Str(Str'First)'Address);
end Str_to_LPCSTR;
                              ------------------------------------------
-- conversion de CHAR_Array (tableau de caracteres) en  Win32.PCHAR
-- (pointeur sur un caractere constant)
function CHAR_Array_to_PCHAR(C_Str : Win32.CHAR_Array) return Win32.PCHAR is
  -- conversion de System.Address en Win32.LPCSTR
  -- function UC is new Ada.Unchecked_Conversion(System.Address,Win32.PCHAR);
begin
  return UC3(C_Str(C_Str'First)'Address);
end CHAR_Array_to_PCHAR;
                              ------------------------------------------
-- conversion de Win32.Wingdi.Ac_Logfont_T en  Win32.Wingdi.Ac_Textmetric_T
--function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new
--  Ada.Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,
--                       Target =>Win32.Wingdi.Ac_Textmetric_T);
                               ------------------------------------------
                              
begin

 -- creer le police logique
    Log_Font.lfFaceName:=info_font(nofont).police.lfFaceName;
    -- on charge systematiquement une police de corps 400
    Log_Font.lfHeight 		  :=win32.long(-400);
    Log_Font.lfWidth          := 0;
    Log_Font.lfEscapement     := 0;
    Log_Font.lfOrientation    := 0;
    Log_Font.lfWeight         := 0;
    Log_Font.lfItalic         := 0;
    Log_Font.lfUnderline      := 0;
    Log_Font.lfStrikeOut      := 0;
    Log_Font.lfCharSet        := 0;
    Log_Font.lfOutPrecision   := 0;
    Log_Font.lfClipPrecision  := 0;
    Log_Font.lfQuality        := 0;
    Log_Font.lfPitchAndFamily := 0;
      
    Font_Handle:=win32.wingdi.CreateFontIndirect(Lplf => Convert_Ac_Logfont_T_To_Ac_Textmetric_T (S =>log_font'unchecked_Access));
    -- charger la police    
    FHandle:= win32.wingdi.SelectObject (hdc, Font_Handle) ;
    
    res :=win32.wingdi.gettextmetrics(hDC, lptm'unchecked_access);
    
    H_400:= Integer(400-lptm.tmdescent);
    T:=float(info_font(Nofont).hauteur)*float(25.4/(72.0*400.0))*float(ptparmm);
       
    res:=win32.wingdi.deleteobject(Font_Handle);
    return(integer(T*float(H_400)));
   
end corps;


function Det_corps(info_font: in typtfont;
                   Nofont : in integer;
                   hWnd : in Win32.winnt.Handle) return integer is

hDC: Win32.windef.hdc;
ps: Win32.winuser.LPPAINTSTRUCT;

begin
  ps := new Win32.winuser.PAINTSTRUCT;
  hDC := Win32.winuser.BeginPaint (hWnd,ps);
  Free_LPPAINTSTRUCT(ps);
  return(CORPS(info_font,Nofont,hdc));
end Det_corps;


procedure Calcule_police(META : in out MT;
                         info_font : in out typtfont;
						 nbfont : in integer;
						 T_TRS : in out TTRONCON;
						 T_ARCS : in out TARCS;
						 T_RTES : in out TROUTES;
						 R : integer;
                         HWnd : in Win32.Windef.HWND) is

erreur_code: exception;
trouve : boolean:=false;
Code : string30;
Nom : string30 := (others => ' ');
Ncar : integer;

begin

  Code:=T_RTES(T_ARCS(T_TRS(math_int_basic.max(META.T_DEB,META.T_fin)).A_DEB).N_TRONCON).Code;
  for i in 1..nbfont loop
    if info_font(i).code = code then
      Meta.Police:=i;
	  if info_font(i).corps_plage=0 then
	    info_font(i).corps_plage:=det_corps(info_font,Meta.police,hWnd);
      end if;
      Meta.Corps:=info_font(i).corps_plage;
      trouve:=true;
      exit;
    end if;
  end loop;    

  if trouve=false then
    raise erreur_code;
  end if;

  META.Nb_mots:=T_RTES(T_ARCS(T_TRS(math_int_basic.max(META.T_DEB,META.T_fin)).A_DEB).N_TRONCON).Nb_Mots;
  for i in 1..META.Nb_mots loop
    ncar:=T_RTES(T_ARCS(T_TRS(math_int_basic.max(META.T_DEB,META.T_fin)).A_DEB).N_TRONCON).ncar(I);
    nom(1..ncar):=T_RTES(T_ARCS(T_TRS(math_int_basic.max(META.T_DEB,META.T_fin)).A_DEB).N_TRONCON).nom(I)(1..ncar);

    if r=1 then
      Meta.L_TOPO(I):=det_longueur(nom,ncar,info_font,Meta.police,hWnd)
	                 +integer(float(ncar-1)*info_font(Meta.police).Interlettres*float(info_font(Meta.police).corps_plage)/info_font(Meta.police).hauteur);
    end if;
    for rR in reverse 1..R-1 loop
	  for J in 1..T_RTES(RR).Nb_Mots loop
  	    if T_RTES(RR).ncar(J)=Ncar then
          if T_RTES(RR).Nom(J)(1..ncar)=Nom(1..Ncar) and T_RTES(RR).code=Code then
            Meta.L_TOPO(I):=T_RTES(RR).L_tOPO(J);
  		    goto FinBoucleI;
          end if;
        end if;
        if rR=1 then
	      Meta.L_TOPO(I):=det_longueur(nom,ncar,info_font,Meta.police,hWnd)
	                     +integer(float(ncar-1)*info_font(Meta.police).Interlettres*float(info_font(Meta.police).corps_plage)/info_font(Meta.police).hauteur);
   		  goto FinBoucleI;
        end if;
      end loop;
    end loop;
	<< finBoucleI >>
    T_RTES(R).L_Topo(I):=Meta.L_TOPO(i);
  end loop;

  exception when others => 
  Msg_Erreur:=To_Unbounded_String("Certains codes typo d'�critures � disposition"&eol&
                                  "sont absents du fichier des codes typo");
end Calcule_police;



PROCEDURE RECHERCHE_MT_G(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
						 Nbfont : integer;
                         RTE : ROUTES;
                         T_ARCS : in out TARCS;   
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         MT_TMP : in out MT;
                         TAB_POS : in out POSITION;
                         ZI_TMP : in out Boite_type;
                         HWnd : in Win32.Windef.HWND;
                         resolution : in positive;
						 cale : boolean) is

  MT_COURT	: boolean:=true;
  groupt	: integer;
  compt		: integer:=RTE.PT_DEB;
  Tab_arc	: point_liste_type(1..NBRE_PTS_D_ARC);
  c_arc		: positive;
  TAB_T		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TG		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TD		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TLG		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TLD		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  C_T : integer:=0;
  C_TG : integer:=0;
  C_TD : integer:=0;
  T_ARC_A : TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AG : TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AD : TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_NOEUDS : Point_liste_type(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+2));
  CRS_NOEUDS : positive;
  ND : integer;
  Suiv : integer:=0;--=0 si c'est le premier arc, 1 sinon
  AbsCurvTotale,Pas : float;
  l_m,l_mc : float:=0.0;
  XMin : integer:=integer'last;
  XMax : integer:=integer'first;
  YMin : integer:=integer'Last;
  YMax : integer:=integer'first;
  Longueur_TAB_T, Longueur_Ecriture : float:=0.0;
  a,b : float;
  Pso_local : point_type;
  Pne_local : point_type;

begin

MT_TMP.T_DEB:=compt;
CRS_NOEUDS:=1;

Longueur_Ecriture:=0.0;
for i in 1..MT_TMP.Nb_Mots loop
  Longueur_Ecriture:=Longueur_Ecriture+float(MT_TMP.L_Topo(i));
end loop;

-- agregation des troncons de la route en partant du premier, jusqu'a ce que MT_COURT=False ou que le dernier troncon soit atteint
while (MT_COURT) and (compt<=RTE.PT_FIN) loop

  Groupt:=TabLegendes(gr_arc(graphe,abs(T_ARCS(T_TRS(compt).A_deb).N_arc)).att_graph).NumGroupe;

  -- ajout dans Tab_T du troncon d'indice Compt
  for j in T_TRS(compt).a_Deb..T_TRS(compt).a_fin loop
    l_mc:=gr_legende_l(legende,gr_arc(graphe,abs(T_ARCS(j).N_ARC)).att_graph).largeur;
	if l_mc>L_m then
      l_m:=l_mc;
    end if;
    if c_t=0 then
      suiv:=0;
    else
      suiv:=1;
    end if;
    gr_points_d_arc(graphe,abs(T_ARCS(j).N_ARC),Tab_arc,c_arc);
    if T_ARCS(j).N_ARC<0 then
      for i in reverse 1..c_arc-Suiv loop
        C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	    if TAB_T(C_T).coor_X<XMin then XMin:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_X>XMax then XMax:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_Y<YMin then YMin:=TAB_T(C_T).coor_Y; end if;
	    if TAB_T(C_T).coor_Y>YMax then YMax:=TAB_T(C_T).coor_Y; end if;
      end loop;
    else
      for i in 1+Suiv..c_arc  loop
	    C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	    if TAB_T(C_T).coor_X<XMin then XMin:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_X>XMax then XMax:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_Y<YMin then YMin:=TAB_T(C_T).coor_Y; end if;
	    if TAB_T(C_T).coor_Y>YMax then YMax:=TAB_T(C_T).coor_Y; end if;
      end loop;
    end if;
  end loop;

  -- debranchement pour les meta-troncons hors image
  if mutil and (XMax<=Pso.Coor_X or XMin>=Pne.coor_X or YMax<=Pso.Coor_Y or YMin>=Pne.Coor_Y) then
  	goto label;
  end if;

  Pso_local:=Pso;
  Pne_local:=Pne;

  if Rte.mode_P=CDesaxe or Rte.mode_P=CAxe then  
  -- cas d'une occurrence d'�criture � disposition sur courbe
  -- calcul des lignes de support inf�rieure et sup�rieure

    declare
      TAB_TF : point_liste_reel(1..C_T);
      Arcg : Point_access_reel;
      Parentg : Liens_access_type;
      Arcd : Point_access_reel;
      Parentd : Liens_access_type;
      ArcgR : Point_access_reel;
      ArcdR : Point_access_reel;

    begin

      for i in 1..c_t loop
        TAB_TF(i):=(float(TAB_T(i).coor_x),float(TAB_T(i).coor_Y));
      end loop;

      -- debranchement pour les meta-troncons encore trop courts
--      Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
--      if Longueur_TAB_T<Longueur_Ecriture then
--        goto label;
--      end if;

      -- obtention des lignes support sup. et inf. par fermeture morphologique
      MCL(Tab_T,c_T,A,b); -- regression lineaire sur le meta_troncon
   	  if 2.0*abs(B)<abs(a) then -- segment de regression proche de la verticale (moins de 30�)
        if (a*B<0.0 and tab_T(1).coor_Y<tab_T(c_T).coor_Y) or (a*B>0.0 and tab_T(1).coor_Y>tab_T(c_T).coor_Y) then
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,Parentg,True,precisionFermeture);
          Fermeture_droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
        else			
          Fermeture_Droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,parentg,True,precisionFermeture);
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
        end if;
      else
        if tab_T(1).coor_x<tab_T(c_T).coor_x then
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,Parentg,True,precisionFermeture);
          Fermeture_droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
        else
          Fermeture_Droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,parentg,True,precisionFermeture);
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
        end if;
      end if;

--      -- obtention des lignes support sup. et inf. par fermeture morphologique 
--      if tab_T(1).coor_x<tab_T(c_T).coor_x then
--        Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,Parentg,True,precisionFermeture);
--        Fermeture_droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
--      else
--        Fermeture_Droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcg,parentg,True,precisionFermeture);
--        Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP.corps),CoefDilatErode*float(MT_TMP.corps),Arcd,Parentd,True,precisionFermeture);
--      end if;

      -- r�partition r�guli�re des vertex de la ligne support sup.
   	  Pas:=1.0*float(Resolution);
	  if Arcg/=null then
        absCurvTotale:=Taille_ligne(Arcg.all,Arcg'last);
        ArcgR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
        Repartition_reguliere(Arcg.all,Pas,ArcgR.all);
	  end if;

      -- filtre gaussien de la ligne support sup.
      if ArcgR/=null then
	    c_tG:=ArcgR'last;
        for i in 1..c_tG-1 loop
          TAB_TG(i):=(integer(ArcgR(i).coor_x),integer(ArcgR(i).coor_Y));
        end loop;
        TAB_TG(C_tG):=(integer(ArcgR(C_tG).coor_x),integer(ArcgR(C_TG).coor_Y));
        if c_TG/=2 then
		  FILTRE_GAUSSIEN_N(TAB_TG,c_tG,DistLissage*float(resolution),TAB_TLG);
        else
		  c_TG:=0;
		end if;
      end if;

      -- r�partition r�guli�re des vertex de la ligne support inf.
      if Arcd/=null then
	    absCurvTotale:=Taille_ligne(Arcd.all,Arcd'last);
        ArcdR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
        Repartition_reguliere(Arcd.all,Pas,ArcdR.all);
      end if;

      -- filtre gaussien de la ligne support inf.
      if ArcdR/=null then
	    c_tD:=ArcdR'last;
        for i in 1..c_tD-1 loop
          TAB_TD(i):=(integer(ArcdR(i).coor_x),integer(ArcdR(i).coor_Y));
        end loop;
        TAB_TD(C_tD):=(integer(ArcdR(C_tD).coor_x),integer(ArcdR(C_TD).coor_Y));

        if c_tD/=2 then
          FILTRE_GAUSSIEN_N(TAB_TD,c_tD,DistLissage*float(resolution),TAB_TLD);
        else
          C_TD:=0;
        end if;
      end if;

      -- calcul des positions candidates de l'occurrence d'�criture
      CALCULE_POSITIONS_QCQ(graphe,legende,TAB_TLG,C_TG,T_ARC_AG,TAB_TLD,C_TD,T_ARC_AD,TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZI_TMP,rte,groupt,resolution,cale,false,TAB_T,C_t,L_m);

--	  gb.MsgBox("apres calcul_positions_qcq");
--	  gb.MsgBox(integer'image(TAB_POS(1).NGeom(1)));

	  GR_Free_point_liste_reel(arcg);
	  GR_Free_point_liste_reel(arcd);
	  GR_Free_point_liste_reel(arcgR);
	  GR_Free_point_liste_reel(arcdR);
	  GR_Free_liens_array(Parentg);
	  GR_Free_liens_array(Parentd);

    end;
  else  -- cas d'une occurrence d'�criture � disposition � support rectiligne orient�
  	
    -- detection des noeuds importants du graphe que contient le meta-troncon, afin de favoriser
	-- le centrage de l'occurrence d'�criture entre 2 noeuds importants cons�cutifs
    NOEUDS_IMPORTANTS(graphe,T_ARC_A,C_T,TAB_NOEUDS,CRS_NOEUDS);

    -- calcul des positions candidates de l'occurrence d'�criture
    CALCULE_POSITIONS_QCQ(graphe,legende, TAB_T,C_T, T_ARC_A, TAB_T,C_T, T_ARC_A, TAB_NOEUDS,crs_NOEUDS,MT_TMP,TAB_POS,MT_COURT,ZI_TMP,rte,groupt,resolution,cale,false,TAB_T,C_T,0.0);
  end if;

  << label >>
  MT_TMP.T_FIN:=compt;
  Compt:=Compt+1;
  Suiv:=1;
end loop;

END RECHERCHE_MT_G;

PROCEDURE RECHERCHE_MT_D(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
                         nbfont : integer;
                         RTE : ROUTES;
                         T_ARCS : in out TARCS;   
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         crs_deb : integer;
                         MT_TMP2 : in out MT;
                         TAB_POS : in out POSITION;
                         ZI_TMP : in out boite_type;
                         HWnd : in Win32.Windef.HWND;
                         resolution : in positive) is

  MT_COURT	: boolean:=true;
  groupt	: integer;
  compt		: integer:=RTE.PT_FIN;
  Tab_arc	: point_liste_type(1..NBRE_PTS_D_ARC);
  c_arc		: positive;
  TAB_T		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TG		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TD		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TLG		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TLD		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  C_T : integer:=0;
  C_TG : integer:=0;
  C_TD : integer:=0;
  T_ARC_A	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AG	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AD	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_NOEUDS	: Point_liste_type(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+2));
  CRS_NOEUDS	: positive:=1;
  ND		: integer;
  Suiv		: integer:=0;--=0 si c'est le premier arc, 1 sinon.
  AbsCurvTotale,Pas : float;
  L_m,l_mc : float:=0.0;
  XMin : integer:=integer'last;
  XMax : integer:=integer'first;
  YMin : integer:=integer'Last;
  YMax : integer:=integer'first;
  Longueur_TAB_T, Longueur_Ecriture : float:=0.0;
  a,b : float;


begin

  MT_TMP2.T_FIN:=RTE.PT_FIN;
  CRS_NOEUDS:=1;

  Longueur_Ecriture:=0.0;
  for i in 1..MT_TMP2.Nb_Mots loop
    Longueur_Ecriture:=Longueur_Ecriture+float(MT_TMP2.L_Topo(i));
  end loop;

-- agregation des troncons de la route en partant du dernier, jusqu'a ce que MT_COURT=False ou que le ptremier troncon soit atteint
while MT_COURT and compt>crs_deb loop

  Groupt:=TabLegendes(gr_arc(graphe,abs(t_arcs(T_TRS(compt).a_Deb).n_arc)).att_graph).NumGroupe;
  
  -- ajout dans Tab_T du troncon d'indice Compt
  for j in reverse T_TRS(compt).a_Deb..T_TRS(compt).A_fin loop
    l_mc:=gr_legende_l(legende,gr_arc(graphe,abs(T_ARCS(j).N_ARC)).att_graph).largeur;
	if l_mc>L_m then
      l_m:=l_mc;
    end if;
    if c_t=0 then
      suiv:=0;
    else
      suiv:=1;
    end if;
    gr_points_d_arc(graphe,abs(T_ARCS(j).N_ARC),Tab_arc,c_arc);
    if T_ARCS(j).N_ARC>0 then
      for i in reverse 1..c_arc-Suiv  loop
	    C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	    if TAB_T(C_T).coor_X<XMin then XMin:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_X>XMax then XMax:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_Y<YMin then YMin:=TAB_T(C_T).coor_Y; end if;
	    if TAB_T(C_T).coor_Y>YMax then YMax:=TAB_T(C_T).coor_Y; end if;
      end loop;
    else
      for i in 1+Suiv..c_arc  loop
	    C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	    if TAB_T(C_T).coor_X<XMin then XMin:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_X>XMax then XMax:=TAB_T(C_T).coor_X; end if;
	    if TAB_T(C_T).coor_Y<YMin then YMin:=TAB_T(C_T).coor_Y; end if;
	    if TAB_T(C_T).coor_Y>YMax then YMax:=TAB_T(C_T).coor_Y; end if;
      end loop;
    end if;
  end loop;

  -- debranchement pour les meta-troncons hors image
  if mutil and (XMax<=Pso.Coor_X or XMin>=Pne.coor_X or YMax<=Pso.Coor_Y or YMin>=Pne.Coor_Y) then
  	goto label;
  end if;

  if Rte.mode_P=CDesaxe or Rte.mode_P=CAxe then
  -- cas d'une occurrence d'�criture � disposition sur courbe
  -- calcul des lignes de support inf�rieure et sup�rieure
  	
    declare
      TAB_TF : point_liste_reel(1..C_T);
      Arcg : Point_access_reel;
      Parentg : Liens_access_type;
      Arcd : Point_access_reel;
      Parentd : Liens_access_type;
      ArcgR : Point_access_reel;
      ArcdR : Point_access_reel;

    begin

      for i in 1..c_t loop
        TAB_TF(i):=(float(TAB_T(i).coor_x),float(TAB_T(i).coor_Y));
      end loop;

      -- debranchement pour les meta-troncons encore trop courts
--      Longueur_TAB_T:=taille_ligne(TAB_T,C_T);
--      if Longueur_TAB_T<Longueur_Ecriture then
--        goto label;
--      end if;

--      -- obtention des lignes support sup. et inf. par fermeture morphologique
--	  if tab_T(1).coor_x<tab_T(c_T).coor_x then
--        Fermeture_Gauche(TAB_TF(1..C_T),CoefDilat*float(MT_TMP2.corps),CoefErode*float(MT_TMP2.corps),Arcg,Parentg,True,precisionFermeture);
--        Fermeture_droite(TAB_TF(1..C_T),CoefDilat*float(MT_TMP2.corps),CoefErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
--      else
--        Fermeture_Droite(TAB_TF(1..C_T),CoefDilat*float(MT_TMP2.corps),CoefErode*float(MT_TMP2.corps),Arcg,Parentg,True,precisionFermeture);
--        Fermeture_Gauche(TAB_TF(1..C_T),CoefDilat*float(MT_TMP2.corps),CoefErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
--      end if;

      -- obtention des lignes support sup. et inf. par fermeture morphologique
      MCL(Tab_T,c_T,A,b); -- regression lineaire sur le meta_troncon
   	  if 2.0*abs(B)<abs(a) then -- segment de regression proche de la verticale (moins de 30�)
        if (a*B<0.0 and tab_T(1).coor_Y<tab_T(c_T).coor_Y) or (a*B>0.0 and tab_T(1).coor_Y>tab_T(c_T).coor_Y) then
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcg,Parentg,True,precisionFermeture);
          Fermeture_droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
        else			
          Fermeture_Droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcg,parentg,True,precisionFermeture);
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
        end if;
      else
	    if tab_T(1).coor_x<tab_T(c_T).coor_x then
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcg,Parentg,True,precisionFermeture);
          Fermeture_droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
        else
          Fermeture_Droite(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcg,Parentg,True,precisionFermeture);
          Fermeture_Gauche(TAB_TF(1..C_T),CoefDilatErode*float(MT_TMP2.corps),CoefDilatErode*float(MT_TMP2.corps),Arcd,Parentd,True,precisionFermeture);
        end if;
      end if;

      -- r�partition r�guli�re des vertex de la ligne support sup.
	  Pas:=1.0*float(Resolution);
	  if Arcg/=null then
        absCurvTotale:=Taille_ligne(Arcg.all,Arcg'last);
        ArcgR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
        Repartition_reguliere(Arcg.all,Pas,ArcgR.all);
      end if;

      -- filtre gaussien de la ligne support sup.
	  if ArcgR/=null then
	    c_tG:=ArcgR'last;
        for i in 1..c_tG-1 loop
          TAB_TG(i):=(integer(ArcgR(i).coor_x),integer(ArcgR(i).coor_Y));
        end loop;
        TAB_TG(C_tG):=(integer(ArcgR(C_tG).coor_x),integer(ArcgR(C_TG).coor_Y));
        if c_tG/=2 then
          FILTRE_GAUSSIEN_N(TAB_TG,c_tG,DistLissage*float(resolution),TAB_TLG);
	    else
		  c_TG:=0;
        end if;
      end if;

      -- r�partition r�guli�re des vertex de la ligne support inf.
      if arcd/=null then
	    absCurvTotale:=Taille_ligne(Arcd.all,Arcd'last);
        ArcdR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
        Repartition_reguliere(Arcd.all,Pas,ArcdR.all);
	  end if;

      -- filtre gaussien de la ligne support inf.
      if ArcdR/=null then
        c_tD:=ArcdR'last;
        for i in 1..c_tD-1 loop
          TAB_TD(i):=(integer(ArcdR(i).coor_x),integer(ArcdR(i).coor_Y));
        end loop;
        TAB_TD(C_tD):=(integer(ArcdR(C_tD).coor_x),integer(ArcdR(C_TD).coor_Y));
        if c_tD/=2 then
          FILTRE_GAUSSIEN_N(TAB_TD,c_tD,DistLissage*float(resolution),TAB_TLD);
        else
		  c_tD:=0;
		end if;
      end if;

      -- calcul des positions candidates de l'occurrence d'�criture
      CALCULE_POSITIONS_QCQ(graphe,legende, TAB_TLG,C_TG, T_ARC_AG, TAB_TLD,C_TD, T_ARC_AD, TAB_NOEUDS,crs_NOEUDS,MT_TMP2,TAB_POS,MT_COURT,ZI_TMP,rte,groupt,resolution,true,True,TAB_T,C_T,l_m);

	  GR_Free_point_liste_reel(arcg);
	  GR_Free_point_liste_reel(arcd);
	  GR_Free_point_liste_reel(arcgR);
	  GR_Free_point_liste_reel(arcdR);
	  GR_Free_liens_array(Parentg);
	  GR_Free_liens_array(Parentd);
    end;
  else  -- cas d'une occurrence d'�criture � disposition � support rectiligne orient�
  	
    -- detection des noeuds importants du graphe que contient le meta-troncon, afin de favoriser
	-- le centrage de l'occurrence d'�criture entre 2 noeuds importants cons�cutifs
    NOEUDS_IMPORTANTS(graphe,T_ARC_A,C_T,TAB_NOEUDS,CRS_NOEUDS);

    -- calcul des positions candidates de l'occurrence d'�criture
    CALCULE_POSITIONS_QCQ(graphe,legende, TAB_T,C_T, T_ARC_A, TAB_T,C_T, T_ARC_A, TAB_NOEUDS,crs_NOEUDS,MT_TMP2,TAB_POS,MT_COURT,ZI_TMP,rte,groupt,resolution,true,True,TAB_T,C_T,0.0);
  end if;

  << label >>
  MT_TMP2.T_DEB:=compt;
  COMPT:=COMPT-1; 

end loop;

END RECHERCHE_MT_D;


---------------------------------------------------------------------------
--Petite procedure de merde pour corriger le fait que lorsqu'on rajoute----
--un arc on a deux fois le meme point (fin de a1, debut de a2).------------
--Ca pose des problemes donc on les suppriment-----------------------------
--Utilise uniquement dans RECHERCHE_MT_I. POur les autres MT, les procedures
--ont ete modifies pour corriger ce probleme------------------------------- 
Procedure Corrige_tab_points(T1 : point_liste_type;
                             c1 : integer;
                             T2 : in out point_liste_type;
                             c2 : in out integer;
                             T3 : in out TGROUPEMENT) is

begin
  T2(1):=T1(1);
  c2:=1;
  for i in 2..c1 loop
    if t1(i)/=t2(c2) then
	c2:=c2+1;
	t2(c2):=t1(i);
        T3(c2):=T3(i);
    end if;
  end loop;
end Corrige_tab_points;


PROCEDURE RECHERCHE_MT_I(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
                         nbfont : integer;
                         RTE : ROUTES;
                         -- I_ROUTE : integer;
                         T_ARCS : in out TARCS;
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         TI_D : integer;
                         TI_F : integer;
                         T_MT : in out TMT;
                         c_TMT : in out integer;
                         TAB_POS : in out POSITIONS;
                         T_ZI : in out typtbe;
                         HWnd : in Win32.Windef.HWND;
						 resolution : in positive) is

  D		: integer:=0;
  T		: float:=0.0;
  G,groupt	: integer:=0;
  compt		: integer:=TI_D;
  MT_COURT	: boolean:=true;
  Tab_arc	: point_liste_type(1..NBRE_PTS_D_ARC);
  c_arc		: positive;
  TAB_T		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TG		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TD		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TGL		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_TDL		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  C_T		: integer:=0;
  C_TG : integer:=0;
  C_TD : integer:=0;
  N_TAB		: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  N_crs		: integer:=0;
  T_ARC_A	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AG	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  T_ARC_AD	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_T_TMP	: point_liste_type (1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  C_T_TMP	: integer:=0;
  T_ARC_A_TMP	: TGROUPEMENT(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+1)*NBRE_PTS_D_ARC);
  TAB_NOEUDS	: Point_liste_type(1..(T_TRS(RTE.PT_FIN).a_fin-T_TRS(RTE.PT_DEB).a_deb+2));
  CRS_NOEUDS	: positive:=1;
  CRS_NOEUDS_A	: positive:=1;
  ND		: integer;
  TAB_POS_C_Tmt : POSITION(1..maxP);
  c_tmt0 : integer:=C_tmt; -- valeur initiale de c_tmt
  GeomStore : Point_access_type;
  GeomLast,LL : integer;
  NG1,NG2 : integer;
  NGeomStore : integer;
  TP1,TP2 : position_type;
  AbsCurvTotale,Pas : float;
  L_M,L_Mc : float:=0.0;
  a,b : float;

begin

  for I in 1..maxP loop
    TAB_POS_c_tmt(I).poip:=10.0;
    TAB_POS_c_tmt(I).poss:=false;
  end loop;

  if (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then
    for i in 1..2 loop
      TAB_POS_c_tmt(i).Geom:=new point_liste_type(1..MaxNGeom);
    end loop;
  end if;

  -- ajout du premier troncon dans Tab_T
  c_tmt:=c_tmt+1;
  T_MT(c_TMT).T_DEB:=TI_D; 
  -- T_MT(c_TMT).I_ROUTE:=I_ROUTE;
  for j in T_TRS(TI_D).a_Deb..T_TRS(TI_D).a_fin loop
    gr_points_d_arc(graphe,abs(T_ARCS(j).N_ARC),Tab_arc,c_arc);
    if T_ARCS(j).N_ARC<0 then
      for i in reverse 1..c_arc  loop
	    C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
      end loop;
    else
      for i in 1..c_arc  loop
	    C_T:=C_T+1; 
        TAB_T(C_T):=tab_arc(i);
	    T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
      end loop;
    end if;
  end loop;

  Groupt:=TabLegendes(gr_arc(graphe,abs(T_ARCS(T_TRS(TI_D).A_deb).N_arc)).att_graph).NumGroupe;
  NOEUDS_IMPORTANTS(graphe,T_ARC_A,C_T,TAB_NOEUDS,CRS_NOEUDS);

  compt:=compt+1;

  -- parcourt des troncons de la route
  while (compt<=TI_F) loop 
    C_T_TMP:=0;

    -- ajout dans Tab_T du troncon d'indice Compt
    for j in T_TRS(compt).a_Deb..T_TRS(compt).a_fin loop
      gr_points_d_arc(graphe,abs(T_ARCS(j).N_ARC),Tab_arc,c_arc);
      if T_ARCS(j).N_ARC<0 then
        for i in reverse 1..c_arc  loop
	      C_T:=C_T+1;
          TAB_T(C_T):=tab_arc(i);
	      T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	      C_T_TMP:=C_T_TMP+1;
          TAB_T_TMP(C_T_TMP):=tab_arc(i);
	      T_ARC_A_TMP(C_T_TMP):=abs(T_ARCS(j).N_ARC);
        end loop;
      else
        for i in 1..c_arc  loop
	      C_T:=C_T+1; 
          TAB_T(C_T):=tab_arc(i);
	      T_ARC_A(C_T):=abs(T_ARCS(j).N_ARC);
	      C_T_TMP:=C_T_TMP+1; 
          TAB_T_TMP(C_T_TMP):=tab_arc(i);
	      T_ARC_A_TMP(C_T_TMP):=abs(T_ARCS(j).N_ARC);
        end loop;
      end if;
    end loop;

    -- detection des noeuds importants du graphe que contient le meta-troncon, afin de favoriser
	-- le centrage de l'occurrence d'�criture entre 2 noeuds importants cons�cutifs
    CRS_NOEUDS_A:=CRS_NOEUDS;
    NOEUDS_IMPORTANTS(graphe,T_ARC_A,C_T,TAB_NOEUDS,CRS_NOEUDS);
    T:=Taille_Ligne(TAB_T,C_T);
    D:=CRS_NOEUDS-1; -- -1 car on enleve l'une des extremites.
    G:=TabLegendes(gr_arc(graphe,abs(T_ARCS(T_TRS(compt).A_deb).N_arc)).att_graph).NumGroupe;

    -- conditions de creation d'un nouveau meta-troncon intermediaire
    if ((RTE.Mode_P=AXE or RTE.Mode_P=DESAXE or RTE.Mode_P=DROIT or RTE.Mode_P=DECALE) and T>SEUIL_TAILLE*float(Resolution))
	or (RTE.Mode_P=Courbe and T>SEUIL_TAILLE_COURBE*float(Resolution))
    or ((RTE.Mode_P=CAXE or RTE.Mode_P=CDESAXE) and T>SEUIL_TAILLE_hYDRO*float(Resolution))
	or (D>SEUIL_DENSITE) or (G/=Groupt) then

      T_MT(c_tmt).police:=T_MT(c_tmt0).police;
      T_MT(c_tmt).corps:=T_MT(c_tmt0).corps; 
      T_MT(c_tmt).nb_mots:=T_MT(c_tmt0).nb_mots;
	  for i in 1..T_MT(c_tmt).nb_mots loop
		T_MT(c_tmt).L_TOPO(i):=T_MT(c_tmt0).L_TOPO(i);
      end loop;

      T_MT(c_TMT).T_FIN:=compt-1;
      Corrige_tab_points(Tab_t,C_T-c_t_TMP,N_TAb,n_crs,T_ARC_A);

	  -- Initialisation du tableau des positions candidates de l'occurrence d'ecriture
      for i in TAB_POS_C_Tmt'range loop
	    if i<=2 then
          GeomStore:=TAB_POS_c_tmt(I).Geom;
          NGeomStore:=TAB_POS_c_tmt(I).NGeom(1);
	    end if;
	    TAB_POS_c_Tmt(I):=TAB_POS(c_tmt,i);
	    if i<=2 then
          TAB_POS_c_Tmt(I).Geom:=GeomStore;
          TAB_POS_c_Tmt(I).NGeom(1):=NGeomStore;
	    end if;
      end loop;

      if Rte.mode_P=CDesaxe or Rte.mode_P=CAxe then
      -- cas d'une occurrence d'�criture � disposition sur courbe
      -- calcul des lignes de support inf�rieure et sup�rieure
	  	
        declare
          TAB_TF : point_liste_reel(1..n_crs);
          Arcg      : Point_access_reel;
          Parentg   : Liens_access_type;
          Arcd      : Point_access_reel;
          Parentd   : Liens_access_type;
          ArcgR     : Point_access_reel;
          ArcdR     : Point_access_reel;

        begin

          for i in 1..n_crs loop
            TAB_TF(i):=(float(TAB_T(i).coor_x),float(TAB_T(i).coor_Y));
          end loop;

          -- obtention des lignes support sup. et inf. par fermeture morphologique 
          MCL(Tab_T,n_crs,A,b); -- regression lineaire sur le meta_troncon
   	      if 2.0*abs(B)<abs(a) then -- segment de regression proche de la verticale (moins de 30�)
            if (a*B<0.0 and N_TAB(1).coor_Y<N_TAB(n_crs).coor_Y) or (a*B>0.0 and N_TAB(1).coor_Y>N_TAB(n_crs).coor_Y) then
              Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
              Fermeture_droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
            else
              Fermeture_Droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,parentg,True,precisionFermeture);
              Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
            end if;
          else
    	    if N_TAB(1).coor_x<N_TAB(n_crs).coor_x then
              Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
              Fermeture_droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
            else
              Fermeture_Droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
              Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
            end if;
          end if;

          -- r�partition r�guli�re des vertex de la ligne support sup.
	      Pas:=1.0*float(Resolution);
    	  if Arcg/=null then
            absCurvTotale:=Taille_ligne(Arcg.all,Arcg'last);
            ArcgR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
            Repartition_reguliere(Arcg.all,Pas,ArcgR.all);
          end if;

          -- filtre gaussien de la ligne support sup.
    	  if ArcgR/=null then
            c_tG:=ArcgR'last;
            for i in 1..c_tG-1 loop
              TAB_TG(i):=(integer(ArcgR(i).coor_x),integer(ArcgR(i).coor_Y));
            end loop;
            TAB_TG(C_tG):=(integer(ArcgR(C_tG).coor_x),integer(ArcgR(C_TG).coor_Y));
            if c_TG/=2 then --or TAB_TG(1)/=TAB_TG(2) then
              FILTRE_GAUSSIEN_N(TAB_TG,C_TG,DistLissage*float(resolution),TAB_TGL);
            else
			  c_tG:=0;
            end if;
          end if;

          -- r�partition r�guli�re des vertex de la ligne support inf.
		  if arcd/=null then
            absCurvTotale:=Taille_ligne(Arcd.all,Arcd'last);
            ArcdR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
            Repartition_reguliere(Arcd.all,Pas,ArcdR.all);
	      end if;

          -- filtre gaussien de la ligne support inf.
		  if ArcdR/=null then
    	    c_tD:=ArcdR'last;
            for i in 1..c_tD-1 loop
              TAB_TD(i):=(integer(ArcdR(i).coor_x),integer(ArcdR(i).coor_Y));
            end loop;
            TAB_TD(C_tD):=(integer(ArcdR(C_tD).coor_x),integer(ArcdR(C_TD).coor_Y));
            if c_tD/=2 then --or TAB_TD(1)/=TAB_TD(2) then
              FILTRE_GAUSSIEN_N(TAB_TD,C_TD,DistLissage*float(resolution),TAB_TDL);
		    else
			  c_TD:=0;
            end if;
          end if;

          -- calcul des positions candidates de l'occurrence d'�criture
          CALCULE_POSITIONS_QCQ(graphe,legende,TAB_TGL,C_TG,T_ARC_AG,TAB_TDL,C_TD,T_ARC_AD,TAB_NOEUDS,crs_NOEUDS,T_MT(c_tmt),TAB_POS_c_tmt,MT_COURT,T_zi(c_tmt),
		  rte,groupt,resolution,false,false,n_tab,n_crs,l_m);

    	  GR_Free_point_liste_reel(arcg);
    	  GR_Free_point_liste_reel(arcd);
    	  GR_Free_liens_array(Parentg);
    	  GR_Free_liens_array(Parentd);
    	  GR_Free_point_liste_reel(arcgR);
    	  GR_Free_point_liste_reel(arcdR);

        end;

      else  -- cas d'une occurrence d'�criture � disposition � support rectiligne orient�

        -- calcul des positions candidates de l'occurrence d'�criture
        CALCULE_POSITIONS_QCQ(graphe,legende,n_tab,n_crs,T_ARC_A,n_tab,n_crs,T_ARC_A,TAB_NOEUDS,crs_NOEUDS_a,T_MT(c_tmt),TAB_POS_c_tmt,MT_COURT,T_zi(c_tmt),
		rte,Groupt,resolution,false,false,n_tab,n_crs,0.0);
      end if;

	  -- stockage des positions candidates et des lignes support dans TAB_POS
      for i in TAB_POS_C_Tmt'range loop
	  	if i<=2 and (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then
	      if TAB_POS_c_tmt(i).NGeom(1)/=0 then
            GeomLast:=0;
	        GeomLast:=GeomLast+TAB_POS_c_tmt(i).NGeom(1);
            TAB_POS(c_tmt,i).Geom:=new point_liste_type(1..GeomLast);
		    LL:=0;
		  	for L in 1..TAB_POS_c_tmt(i).NGeom(1) loop
	          TAB_POS(c_Tmt,I).Geom(LL+L):=TAB_POS_c_tmt(i).geom(L);
            end loop;
			LL:=LL+TAB_POS_c_tmt(i).NGeom(1);
            GeomStore:=TAB_POS(c_tmt,I).Geom;
          end if;
	    end if;
	    TAB_POS(c_Tmt,I):=TAB_POS_c_tmt(i);
	  	if i<=2 and (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then
          if TAB_POS_c_tmt(i).NGeom(1)/=0 then
            TAB_POS(c_Tmt,I).Geom:=GeomStore;
          end if;
        end if;
      end loop;

      -- on oublie les meta-troncons intermediaires qui sont trop courts pour etre nomm�s
      if MT_COURT then
        c_TMT:=C_TMT-1;
      end if;

      -- Initialisation d'un nouveau meta-troncon
      Groupt:=g;
      c_TMT:=c_TMT+1;
      T_MT(C_TMT).T_DEB:=compt;
      -- T_MT(c_TMT).I_ROUTE:=I_ROUTE;

      -- Rangement du troncon dans le tableau
      TAB_T:=TAB_T_TMP;
      T_ARC_A:=T_ARC_A_TMP;
      C_T:=C_T_TMP;
      NOEUDS_IMPORTANTS(graphe,T_ARC_A,C_T,TAB_NOEUDS,CRS_NOEUDS);

   end if;

   compt:=compt+1;

 end loop;

 -- TRAITEMENT DU DERNIER META-TRONCON

 groupt:=TabLegendes(gr_arc(graphe,abs(t_arcs(T_TRS(compt).a_Deb).n_Arc)).att_graph).NumGroupe;

 T_MT(c_tmt).police:=T_MT(c_tmt0).police;   -- pour le desemboitement de calcule_police
 T_MT(c_tmt).corps:=T_MT(c_tmt0).corps;     -- pour le desemboitement de calcule_police 
 T_MT(c_tmt).nb_mots:=T_MT(c_tmt0).nb_mots;     -- pour le desemboitement de calcule_police 
 for i in 1..T_MT(c_tmt).nb_mots loop
   T_MT(c_tmt).L_TOPO(i):=T_MT(c_tmt0).L_TOPO(i);    -- pour le desemboitement de calcule_police
 end loop;

 T_MT(c_TMT).T_FIN:=compt-1;

 Corrige_tab_points(Tab_t,C_T,N_TAb,n_crs,T_ARC_A);

 -- Initialisation du tableau des positions candidates de l'occurrence d'ecriture
 for i in TAB_POS_C_Tmt'range loop
   if i<=2 then
     GeomStore:=TAB_POS_c_tmt(I).Geom;
     NGeomStore:=TAB_POS_c_tmt(I).NGeom(1);
   end if;
   TAB_POS_c_Tmt(I):=TAB_POS(c_tmt,i);
   if i<=2 then
     TAB_POS_c_Tmt(I).Geom:=GeomStore;
     TAB_POS_c_Tmt(I).NGeom(1):=NGeomStore;
   end if;
 end loop;

 if Rte.mode_P=CDesaxe or Rte.mode_P=CAxe then
 -- cas d'une occurrence d'�criture � disposition sur courbe
 -- calcul des lignes de support inf�rieure et sup�rieure
 	
   declare
     TAB_TF : point_liste_reel(1..n_crs);
     Arcg      : Point_access_reel;
     Parentg   : Liens_access_type;
     Arcd      : Point_access_reel;
     Parentd   : Liens_access_type;
     ArcgR      : Point_access_reel;
     ArcdR     : Point_access_reel;

   begin

     for i in 1..n_crs loop
       TAB_TF(i):=(float(TAB_T(i).coor_x),float(TAB_T(i).coor_Y));
     end loop;

     -- obtention des lignes support sup. et inf. par fermeture morphologique
	 MCL(Tab_T,n_crs,A,b); -- regression lineaire sur le meta_troncon
   	 if 2.0*abs(B)<abs(a) then -- segment de regression proche de la verticale (moins de 30�)
       if (a*B<0.0 and N_TAB(1).coor_Y<N_TAB(n_crs).coor_Y) or (a*B>0.0 and N_TAB(1).coor_Y>N_TAB(n_crs).coor_Y) then
         Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
         Fermeture_droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
       else	
         Fermeture_Droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,parentg,True,precisionFermeture);
         Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
       end if;
     else
       if N_TAB(1).coor_x<N_TAB(n_crs).coor_x then
         Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
         Fermeture_droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
       else
         Fermeture_Droite(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcg,Parentg,True,precisionFermeture);
         Fermeture_Gauche(TAB_TF(1..n_crs),CoefDilatErode*float(T_MT(c_tmt).corps),CoefDilatErode*float(T_MT(c_tmt).corps),Arcd,Parentd,True,precisionFermeture);
       end if;
	 end if;

     -- r�partition r�guli�re des vertex de la ligne support sup.
	 Pas:=1.0*float(Resolution);
     if Arcg/=null then
	   absCurvTotale:=Taille_ligne(Arcg.all,Arcg'last);
       ArcgR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
       Repartition_reguliere(Arcg.all,Pas,ArcgR.all);
     end if;

     -- filtre gaussien de la ligne support sup.
     if ArcgR/=null then
 	   c_tG:=ArcgR'last;
       for i in 1..c_tG-1 loop
         TAB_TG(i):=(integer(ArcgR(i).coor_x),integer(ArcgR(i).coor_Y));
       end loop;
       TAB_TG(C_tG):=(integer(ArcgR(C_tG).coor_x),integer(ArcgR(C_TG).coor_Y));
       if c_tG/=2 then --or TAB_TG(1)/=TAB_TG(2) then
         FILTRE_GAUSSIEN_N(TAB_TG,C_TG,DistLissage*float(resolution),TAB_TGL);
       else
   	     c_TG:=0;
       end if;
     end if;

     -- r�partition r�guli�re des vertex de la ligne support inf.
     if arcd/=null then
	   absCurvTotale:=Taille_ligne(Arcd.all,Arcd'last);
       ArcdR:=new point_liste_reel(1..integer(abscurvtotale/Pas)+2);
       Repartition_reguliere(Arcd.all,Pas,ArcdR.all);
     end if;

     -- filtre gaussien de la ligne support inf.
     if arcdR/=null then
       c_tD:=ArcdR'last;
       for i in 1..c_tD-1 loop
         TAB_TD(i):=(integer(ArcdR(i).coor_x),integer(ArcdR(i).coor_Y));
       end loop;
       TAB_TD(C_tD):=(integer(ArcdR(C_tD).coor_x),integer(ArcdR(C_TD).coor_Y));
       if c_tD/=2 then --or TAB_TD(1)/=TAB_TD(2) then
         FILTRE_GAUSSIEN_N(TAB_TD,C_TD,DistLissage*float(resolution),TAB_TDL);
       else
   	     c_TD:=0;
       end if;
	 end if;

    -- calcul des positions candidates de l'occurrence d'�criture
     CALCULE_POSITIONS_QCQ(graphe,legende,TAB_TGL,C_TG,T_ARC_AG,TAB_TDL,C_TD,T_ARC_AD,TAB_NOEUDS,crs_NOEUDS,t_mt(c_tmt),TAB_POS_c_tmt,MT_COURT,T_zi(c_tmt),
	 rte,groupt,resolution,false,false,n_tab,n_crs,l_m);
     
	 GR_Free_point_liste_reel(arcg);
     GR_Free_point_liste_reel(arcd);
     GR_Free_liens_array(Parentg);
     GR_Free_liens_array(Parentd);
	 GR_Free_point_liste_reel(arcgR);
     GR_Free_point_liste_reel(arcdR);
   end;
 else  -- cas d'une occurrence d'�criture � disposition � support rectiligne orient�
 	
    -- calcul des positions candidates de l'occurrence d'�criture
   CALCULE_POSITIONS_QCQ(graphe,legende,n_TAB,n_crs,T_ARC_A,n_TAB,n_crs,T_ARC_A,TAB_NOEUDS,crs_NOEUDS,t_mt(c_tmt),TAB_POS_c_tmt,MT_COURT,t_zi(c_tmt),
   rte,groupt,resolution,false,false,n_tab,n_crs,0.0);
 end if;

 -- stockage des positions candidates et des lignes support dans TAB_POS
 for i in TAB_POS_C_Tmt'range loop
   if i<=2 and (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then --*
     if TAB_POS_c_tmt(i).NGeom(1)/=0 then
   	   GeomLast:=0;
       GeomLast:=GeomLast+TAB_POS_c_tmt(i).NGeom(1);
       TAB_POS(c_tmt,i).Geom:=new point_liste_type(1..GeomLast);
	   LL:=0;
       for L in 1..TAB_POS_c_tmt(i).NGeom(1) loop
	   	 TAB_POS(c_Tmt,I).Geom(LL+L):=TAB_POS_c_tmt(i).geom(L);
       end loop;
	   LL:=LL+TAB_POS_c_tmt(i).NGeom(1);
       GeomStore:=TAB_POS(c_tmt,I).Geom;
     end if;
   end if;
   TAB_POS(c_Tmt,I):=TAB_POS_c_tmt(i);
   if i<=2 and (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then
     if TAB_POS_c_tmt(i).NGeom(1)/=0 then
       TAB_POS(c_Tmt,I).Geom:=GeomStore;
     end if;
   end if;
 end loop;

 -- on oublie les meta-troncons intermediaires
 -- qui sont trop courts pour etre nomm�s
 if MT_COURT then
   c_TMT:=C_TMT-1;
 end if;

 if (RTE.Mode_P=CAxe or RTE.Mode_P=CDesaxe) then
   for i in 1..2 loop
     GR_Free_point_liste(TAB_POS_c_tmt(i).Geom);
   end loop;
 end if;

END RECHERCHE_MT_I;


PROCEDURE RECHERCHE_MT(graphe : graphe_type;
                       Legende : Legende_type;
                       T_RTES : in out TROUTES;
                       T_MT : in out TMT;
                       T_TRS : in out TTRONCON;
                       T_ARCS : in out TARCS;   
                       TAB_POSITION : in out POSITIONS;
                       T_ZI : in out typtbe;
                       HWnd : in Win32.Windef.HWND;
					   invEchelle : in integer;
					   resolution : in positive) is

use type gb.bool;

-- partie entiere
function E(x : in float) return integer is
begin
  if float(integer(x))<=x then
    return(integer(x));
  else
    return(integer(x)-1);
  end if;
end E;

  MT_TMP		: MT;      
  MT_TMP2		: MT;
  ZI_TMP		: boite_type;
  c_mt			: integer:=0;
  c_mt_deb		: integer:=0;
  TAB_POS		: POSITION(1..maxP);
  info_font     : typtfont(1..500);
  nbfont : integer;
  X_tab			: point_liste_type(1..N_PTs_RTE);
  X_crs			: integer:=1;
  cpt_a			: integer:=0;
  incoherenceX,incoherenceY : float:=0.0;
  EMPRISE_ERROR : exception;
  lps,ncol,nlig : integer;
  stripoffset: tiff_io.acces_tabstrip;
  Ncol_calc, Nlig_calc : integer;
  r : integer;
  GeomStore : Point_Access_Type;
  GeomLast, LL : integer;
  nG1,NG2 : integer;
  hauteurBarre : integer;
  Deja_passe : boolean:=false;

begin

  -- test de compatibilit� entre les hauteur et largeur physiques de l'image et l'emprise terrain, l'�chelle et la r�solution fournies
  if mutil then 
    tiff_io.Charge_param_image(lps, ncol, nlig, stripoffset);
	Free_tabstrip(stripoffset);
    incoherenceX:=Abs(Float(Ncol)-float(Resolution)*1000.0*Float(Xmax_Image-Xmin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
    incoherenceY:=Abs(Float(Nlig)-float(Resolution)*1000.0*float(Ymax_Image-Ymin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
    If incoherenceX>=1.0 or incoherenceY>=1.0 then
      raise EMPRISE_ERROR;
    end if;
  end if;

  charge_fontes(info_font,nbfont);

  -- boucle sur les objets lin�aires
  R:=T_RTES'FIRST-1;
  while R<=NBRE_ROUTES-1 loop

    R:=R+1;

	if 300*(R-1)/NBRE_ROUTES /= 300*R/NBRE_ROUTES or R=NBRE_ROUTES then		
      gb.Move(dialog3.Panel2,0,0,gb.INT(300*R/NBRE_ROUTES),20);
    end if;
	
	if NBRE_ROUTES>600 then
      if 300*20*(R-1)/NBRE_ROUTES /= 300*20*R/NBRE_ROUTES then
		hauteurBarre:=integer(float(integer(float(R)*20.0*300.0/float(NBRE_ROUTES)) mod 20));
		if hauteurBarre=0 then
		  hauteurBarre:=20;
        end if;
        gb.Move(dialog3.Panel3,gb.INT(300*R/NBRE_ROUTES),0,1,gb.INT(HauteurBarre));
      end if;
    end if;

    for J in 1..T_RTES(r).ncar(1) loop
		
      if not((T_RTES(r).Nom(1)(J) in '0'..'9') or (T_RTES(r).Nom(1)(J)=',') or (T_RTES(r).Nom(1)(J)='.')) and t_rTES(R).Mode_P=Courbe then
        exit;
      end if;
      if j=T_RTES(r).ncar(1) then
	  	
        if (t_rTES(R).Mode_P=Desaxe or t_rTES(R).Mode_P=axe or t_rTES(R).Mode_P=Droit or t_rTES(R).Mode_P=Decale) and gb.Checked(dialog1.NumRouCheckBox)=gb.false then
		  exit;
        end if;

        if t_rTES(R).Mode_P=Courbe and (NbCotes=0 or gb.Checked(dialog1.CotesCourbesCheckBox)=gb.false)then
		  exit;
        end if;

        if (t_rTES(R).Mode_P=CDesaxe or t_rTES(R).Mode_P=CAxe) and gb.Checked(dialog1.EcriDispoCheckBox)=gb.false then
		  exit;
        end if;

   	    if T_RTES(r).PT_DEB=0 then
          exit;
        end if;

        --On saute les cas de noms vides
        if (T_RTES(r).NOM(1)(1)=' ') and (Taille_String(T_RTES(r).NOM(1))=1) then
          c_mt:=c_mt+1;
          for k in 1..maxP loop
            TAB_POSITION(c_mt,k).poip:=10.0;
            TAB_POSITION(c_mt,k).poss:=false;
          end loop;

          T_MT(c_mt).t_deb:=T_RTES(R).PT_DEB;
          T_MT(c_mt).t_FIN:=T_RTES(R).PT_FIN;
          norou.Points(t_MT(c_mt),t_Trs,t_arcs,graphe,X_tab,X_crs);
          T_ZI(C_MT):=ZONE_INFLUENCE(X_tab,X_crs,0);
          -- T_MT(c_mt).i_route:=r;
          T_rtes(r).pt_deb:=c_mt;
          T_rtes(r).pt_fin:=c_mt;
          T_MT(c_mt).L_topo(1):=1;
          T_MT(c_mt).police:=1;
          T_MT(c_mt).corps:=1;

        else
	
          MT_TMP.T_DEB:=T_RTES(R).PT_DEB;

          c_mt:=c_mt+1;

	      Calcule_police(mt_tmp,info_font,Nbfont,T_TRS,T_ARCS,T_RTES,R,HWnd);

		  -- debut initialisations pour le meta-troncon extreme gauche
	      for I in 1..maxP loop
            TAB_POS(I).poip:=10.0;
            TAB_POS(I).poss:=false;
          end loop;

	      if (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
            for i in 1..2 loop
              TAB_POS(i).Geom:=new point_liste_type(1..MaxNGeom);
	          TAB_POS(i).NGeom(1):=0;
            end loop;
	      end if;
		  -- fin initialisations pour le meta-troncon extreme gauche

	      -- decoupage et traitement du meta-troncon extreme gauche.
		  -- dernier parametre (booleen cale) = False,
		  -- ce qui favorise les positions centrees par rapport aux extremites du meta-troncaon
          RECHERCHE_MT_G(graphe,Legende,info_font,nbfont,T_RTES(R),T_ARCS,T_TRS,T_RTES,
			             MT_TMP,TAB_POS,ZI_TMP,HWnd,resolution,false);

--	      gb.MsgBox("apres RECHERCHE_MT_G");
--	      gb.MsgBox(integer'image(TAB_POS(1).NGeom(1)));

          -- MT_TMP.I_ROUTE:=r;
          T_MT(c_mt):=MT_TMP;
          T_ZI(C_MT):=ZI_TMP;

          -- debut stockage des r�sultats concernant le meta-troncon extreme gauche
	      for i in 1..maxP loop
            if i<=2 and (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
	          if TAB_POS(i).NGeom(1)/=0 then
	  	        GeomLast:=0;
		        GeomLast:=GeomLast+TAB_POS(i).NGeom(1);
                TAB_POSITION(C_Mt,i).Geom:=new point_liste_type(1..GeomLast);
		        LL:=0;
		        for L in 1..TAB_POS(i).NGeom(1) loop
                  TAB_POSITION(c_mt,I).Geom(LL+L):=TAB_POS(i).geom(L);
                end loop;
		        LL:=LL+TAB_POS(i).NGeom(1);
                GeomStore:=TAB_POSITION(c_mt,I).Geom;
              end if;
            end if;
	        TAB_POSITION(c_mt,I):=TAB_POS(i);
	        if i<=2 and (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
	          if TAB_POS(i).NGeom(1)/=0 then
                TAB_POSITION(c_mt,I).Geom:=GeomStore;
              end if;
            end if;
          end loop;
          -- fin stockage des r�sultats concernant le meta-troncon extreme gauche

          c_mt_deb:=c_mt;

		  -- si il reste des troncons a droite du meta-troncon extreme gauche, le traitement continue,
          -- sauf cas particulier des kilometrages qui sont � occurrences de placement uniques
          if MT_TMP.T_FIN<T_RTES(r).PT_FIN and t_rTES(R).Kilo=false then

		    -- debut initialisations pour le meta-troncon extreme gauche
	        for k in 1..maxP loop
              TAB_POSITION(c_mt,k).poip:=10.0;
              TAB_POSITION(c_mt,k).poss:=false;
            end loop;

	        for I in 1..maxP loop
              TAB_POS(I).poip:=10.0;
              TAB_POS(I).poss:=false;
            end loop;

	        for i in 1..2 loop
	          TAB_POS(i).NGeom(1):=0;
            end loop;
		    -- debut initialisations pour le meta-troncon extreme gauche

	        -- nouveau decoupage et traitement du meta-troncon extreme gauche, ce qui �crase les r�sultats du precedent traitement.
		    -- dernier parametre (booleen cale) = True,
			-- ce qui favorise les positions proches des extremites du meta-troncaon
            RECHERCHE_MT_G(graphe,Legende,info_font,nbfont,T_RTES(R),T_ARCS,T_TRS,T_RTES,
    	                   MT_TMP,TAB_POS,ZI_TMP,HWnd,resolution,True);

            T_MT(c_mt):=MT_TMP;
            T_ZI(C_MT):=ZI_TMP;

            -- debut stockage des r�sultats concernant le meta-troncon extreme gauche
	        for i in 1..maxP loop
	          if i<=2 then
	            if TAB_POS(i).NGeom(1)/=0 then
		          LL:=0;
		          for L in 1..TAB_POS(i).NGeom(1) loop
                    TAB_POSITION(c_mt,I).Geom(LL+L):=TAB_POS(i).geom(L);
                  end loop;
		          LL:=LL+TAB_POS(i).NGeom(1);
                  GeomStore:=TAB_POSITION(c_mt,I).Geom;
                end if;
	          end if;
        
	          TAB_POSITION(c_mt,I):=TAB_POS(i);
	          if i<=2 then
	            if TAB_POS(i).NGeom(1)/=0 then
                  TAB_POSITION(c_mt,I).Geom:=GeomStore;
                end if;
              end if;
            end loop;
            -- fin stockage des r�sultats concernant le meta-troncon extreme gauche

			-- debut initialisations pour le meta-troncon extreme droit
            MT_TMP2.police:=MT_TMP.police;
            MT_TMP2.corps:=MT_TMP.corps;
            MT_TMP2.Nb_Mots:=MT_TMP.Nb_Mots;
            for i in 1..MT_TMP2.Nb_Mots loop
	          MT_TMP2.l_TOPO(i):=MT_TMP.L_TOPO(i);
            end loop;

	        for I in 1..maxP loop
              TAB_POS(I).poip:=10.0;
              TAB_POS(I).poss:=false;
            end loop;

	        for i in 1..2 loop
	          TAB_POS(i).NGeom(1):=0;
            end loop;
			-- fin initialisations pour le meta-troncon extreme droit

            -- decoupage et traitement du meta-troncon extreme droit
            RECHERCHE_MT_D(graphe,Legende,info_font,nbfont,T_RTES(R),T_ARCS,T_TRS,T_RTES,
			               MT_TMP.T_FIN,MT_TMP2,TAB_POS,ZI_TMP,HWnd,resolution);

            -- MT_TMP2.I_ROUTE:=r;
            c_mt:=c_mt+1;
            T_MT(c_mt):=MT_TMP2;
            T_ZI(c_mt):=ZI_TMP;

            -- debut stockage des r�sultats concernant le meta-troncon extreme droit
            for i in 1..maxP loop
              if i<=2 and (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
	            if TAB_POS(i).NGeom(1)/=0 then
                  GeomLast:=0;
		          GeomLast:=GeomLast+TAB_POS(i).NGeom(1);
                  TAB_POSITION(C_Mt,i).Geom:=new point_liste_type(1..GeomLast);
		          lL:=0;
	  	          for L in 1..TAB_POS(i).NGeom(1) loop
		      	    TAB_POSITION(c_mt,I).Geom(LL+L):=TAB_POS(i).geom(L);
                  end loop;
                  LL:=LL+TAB_POS(i).NGeom(1);
                  GeomStore:=TAB_POSITION(c_mt,I).Geom;
                end if; 
 	          end if;
	          TAB_POSITION(c_mt,I):=TAB_POS(i);
	          if i<=2 and (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
		        if TAB_POS(i).NGeom(1)/=0 then
                  TAB_POSITION(c_mt,I).Geom:=GeomStore;
                end if;
	          end if;
            end loop;
            -- fin stockage des r�sultats concernant le meta-troncon extreme droit

          end if;

	      if (t_rTES(R).Mode_P=CAxe or t_rTES(R).Mode_P=CDesaxe) then
            for i in 1..2 loop
              GR_Free_point_liste(TAB_POS(i).Geom);
            end loop;
          end if;

          -- si il reste des troncons entre les deux m�ta-troncon extremes,
		  -- decoupage et traitement des m�ta-troncons intermediaires
          if MT_TMP2.T_deb-MT_TMP.T_fin>1 then
	  	    T_MT(c_mt).police:=MT_TMP.police;
            T_MT(c_mt).corps:=MT_TMP.corps;
            T_MT(c_mt).Nb_Mots:=MT_TMP.Nb_Mots;
		    for i in 1..T_MT(c_mt).Nb_Mots loop
              T_MT(c_mt).L_TOPO(i):=MT_tMP.L_TOPO(i);
            end loop;

            RECHERCHE_MT_I(Graphe,Legende,info_font,nbfont,T_RTES(r),T_ARCS,T_TRS,T_RTES,
		                   MT_TMP.T_FIN+1,MT_TMP2.T_DEB-1,T_MT,c_mt,TAB_POSITION,T_ZI,HWnd,resolution);
          end if;

          --Modification des pointeurs de T_RTES
          T_RTES(r).PT_DEB:=c_mt_deb;
          T_RTES(r).PT_FIN:=c_mt; 
        end if;

      end if;
    end loop;

  end loop;--boucle sur les routes

  exception when EMPRISE_ERROR =>
	Ncol_calc:=E(float(Resolution)*1000.0*Float(Xmax_Image-Xmin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
	Nlig_calc:=E(float(Resolution)*1000.0*Float(Ymax_Image-Ymin_Image)/(Float(InvEchelle)*Float(Pt_Image)));
    Msg_Erreur:=To_Unbounded_String("L'emprise terrain de l'image, sa r�solution et l'�chelle des donn�es"
                     	            &eol&"sont incompatibles avec les dimensions physiques de l'image"
                     	        &eol&eol&"dimensions de l'image :"&tab&integer'image(ncol)&"  � "&integer'image(nlig)
                     	            &eol&"dimensions calcul�es :"&tab&integer'image(ncol_calc)&"  � "&integer'image(nlig_calc));
    raise;

    when Event : others =>
    if Msg_Erreur=To_Unbounded_String("Erreur d'ex�cution") then
       Msg_Erreur:=To_Unbounded_String("Erreur de d�termination des positions"&eol
                                       &"de la"&integer'image(R)&"�me �criture � disposition :");
       for i in 1..MT_TMP.Nb_Mots loop
	   	 Msg_Erreur:=Msg_Erreur&" "&To_Unbounded_String(T_RTES(r).nom(I)(1..T_RTES(r).ncar(I)));
       end loop;
    end if;
    raise;

end RECHERCHE_MT;

end norou2;
