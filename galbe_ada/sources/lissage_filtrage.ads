with Gen_io;use Gen_io;

Package Lissage_filtrage is

procedure Repartition_reguliere(TABE : in point_liste_reel;
                                Pas : in float;
                                TABS : in out point_liste_reel);

procedure Repartition_reguliere(TABE : in point_liste_type;
                                Pas : in float;
								TABS : in out point_liste_type);


--***************************************************************************--
--** Specification du paquetage pour l'algorithme de FILTRE_GAUSSIEN       **--
--***************************************************************************--

---------------------------------------------------------------------------
-- Entree :     ligne 		: Liste des points de l'arc en entier    --
--              np		: Nombre de points de l'arc              --
--              sig		: Nombre de points de gs / 4             --
--                                                                       --
-- Sortie :     ligne_lissee 	: polyligne sortie en entier  		 --
---------------------------------------------------------------------------
 Procedure FILTRE_GAUSSIEN_N( ligne 	   : in point_liste_type;
                  -- debut modif 18.04.2005
			      -- Np   	   : in natural;
			      np   	   : in out natural;
                  -- fin modif 18.04.2005
			      sig 	   : in float;
                   	      ligne_lissee : out point_liste_type);


---------------------------------------------------------------------------
-- Entree :     ligne_aux 	: Liste des points de l'arc en reel      --
--              n_aux		: Nombre de points de l'arc              --
--              sig		: Nombre de points de gs / 4             --
--                                                                       --
-- Sortie :     ligne_lisse 	: polyligne sortie en reel    		 --
---------------------------------------------------------------------------
-- Procedure FILTRE_GAUSSIEN_R( ligne_aux	   : in point_liste_reel;
--			      n_aux   	   : in natural;
--			      sig 	   : in float;
--                   	      ligne_lisse  : out point_liste_reel);
--
--
--*************************************************************************-- 
--**   Filtre gaussien reel "general", i.e. non dedie au lissage de	 **--
--** lignes sous Plage : on peut choisir de decomposer la ligne		 **--
--** (decomposer = true) ou non, auquel cas la ligne doit deja etre	 **--
--** decomposee a pas constant. Dans les 2 cas on passe a la procedure	 **--
--** le pas de decomposition (valeur par defaut 1.0) : pour decomposition**--
--** si decomposer=true, a titre d'information pour le calcul des poids	 **--
--** sinon								 **-- 
--*************************************************************************-- 

Procedure FILTRE_GAUSSIEN_R ( ligne_aux   : in point_liste_reel;
			      n_aux   	  : in natural;
			      sig   	  : in float;
                   	      ligne_lisse : out point_liste_reel;
			      decomposer  : boolean := true;
			      pas	  : float := 1.0);

--*************************************************************************-- 
--**   Specif du paquetage pour l'algorithme du Filtre Gaussien "matheux"   --
--*************************************************************************-- 
-- But = lisser une fonction mathematique, i.e. les ordonnees des points
--       d'une ligne.
-- En entree : -la ligne (coordonnees reelles), deja segmentee a pas constant
-- 		en abscisse
-- 	       -N = le nb de points a prendre en compte pour le lissage de 
--		    part et d'autre de chaque point
--	            (on en deduit sigma tq le dernier pt pris en compte ait
--		     un poids corresp. a 4 sigma, i.e. considere comme
--		     negligeable)

Procedure FILTRE_GAUSSIEN_MATH ( ligne_aux   : in point_liste_reel;
			      n_aux   	  : in natural;
			      N   	  : in natural;
                   	      ligne_lisse : out point_liste_reel);


--************************************************************************--
--**  Specification du paquetage pour l'algorithme de DOUGLAS & PEUCKER **--
--************************************************************************--

  -- procedure permettant d'appliquer a un arc l'algorithme de 
  -- DOUGLAS & PEUCKER :
  --  Les parametres =
  --    tab_points_init : liste des points constituamt l'arc initial
  --    nb_pts_init : dimension de la liste precedente
  --    tab_points_final : liste des points constituant l'arc final
  --    nb_pts_final : dimension de la liste precedente
  --    seuil : critere de selection/suppression des points
  --    resolution : resolution (nombre de pixels par mm) du graphe

   procedure Douglas_Peucker( tab_points_init : point_access_type;
                              nb_pts_init : natural;
                              tab_points_final : point_access_type;
                              nb_pts_final : out natural;
                              seuil: float;
                              resolution : positive);

   -- tableau permettant de conserver les differents points flottants
   type pile_type is array(positive range <>) of positive;

   -- enregistrement permettant de recuperer, a partir d'une fonction,
   -- un point et sa distance a un autre point ou a une droite
   type couple_dist_pt is record
        distance : float;
        point : positive;
   end record;

--************************************************************************--
--**  Specification du paquetage pour l'algorithme de DOUGLAS & PEUCKER **--
--**     2 versions ameliorees pour diminuer l'effet anguleux           **--
--************************************************************************--
   procedure Douglas_Peucker_Plus( tab_points_init : point_access_type;
                                   nb_pts_init : natural;
                                   tab_points_final : point_access_type;
                                   nb_pts_final : out natural;
                                   seuil: float;
                                   resolution : positive);

   procedure Douglas_Peucker_2Plus( tab_points_init : point_access_type;
                                    nb_pts_init : natural;
                                    tab_points_final : point_access_type;
                                    nb_pts_final : out natural;
                                    seuil: float;
                                    rapall : float;
                                    ectm : float;
                                    resolution : positive);
                  

--************************************************************************--
--**     Specification du paquetage pour l'algorithme de VAN HORN       **--
--************************************************************************--

  -- procedure permettant d'appliquer a un arc l'algorithme de 
  -- Van Horn :
  --  Les parametres =
  --    tab_points_init : liste des points constituant l'arc initial
  --    nb_pts_init : dimension de la liste precedente
  --    (Xrep,Yrep) : origine du repere servant de base pour le treillis
  --    pas : longueur, donnee en pixel, du cote des carres 
  --          constituant le treillis (strictement positive)
  --    tab_points_final : liste des points constituant l'arc final
  --    nb_pts_final : dimension de la liste precedente

   procedure Van_Horn( tab_points_init : point_access_type;
                       nb_pts_init : natural;
                       Xrep,Yrep : integer;
                       pas : integer; 
                       tab_points_final : point_access_type;
                       nb_pts_final : out natural);




--***************************************************************************--
--** Specification du paquetage pour les trois versions de l'algorithme de **--
--**                          WALL & DANIELSSON                            **--
--***************************************************************************--


  -- procedure permettant d'appliquer a un arc le vserion de base de 
  -- l'algorithme de WALL & DANIELSSON :
  --  Les parametres =
  --    tab_points_init : liste des points constituamt l'arc initial
  --    nb_pts_init : dimension de la liste precedente
  --    tab_points_final : liste des points constituant l'arc final
  --    nb_pts_final : dimension de la liste precedente
  --    seuil : critere de selection/suppression des points

   procedure Wall_Danielsson( tab_points_init : point_access_type;
                              nb_pts_init : natural;
                              tab_points_final : point_access_type;
                              nb_pts_final : out natural;
                              seuil: float; resolution : positive);


  -- procedure permettant d'appliquer a un arc la version amelioree de l'algo-
  -- rithme de WALL & DANIELSSON : cette amelioration consiste a detecter les
  -- points caracteristiques de l'arc parmi ceux qui ne satisfont pas le 
  -- critere de surface. Un point est considere comme caracteristique lorsqu'il
  -- est le plus eloigne de l'origine.          
  --****                    I M P O R T A N T                            ****--
  --    Dans cette version amelioree, les points caracteristiques ne sont 
  --  pas pris comme origine de la portion d'arc restante                 
  --                                                                      
  --  Les parametres =
  --    tab_points_init : liste des points constituamt l'arc initial
  --    nb_pts_init : dimension de la liste precedente
  --    tab_points_final : liste des points constituant l'arc final
  --    nb_pts_final : dimension de la liste precedente
  --    seuil : critere de selection/suppression des points

   procedure Wall_Danielsson_amelioree( tab_points_init : point_access_type;
                                        nb_pts_init : natural;
                                        tab_points_final : point_access_type;
                                        nb_pts_final : out natural;
                                        seuil: float; resolution : positive);



  -- procedure permettant d'appliquer a un arc la version amelioree de l'algo-
  -- rithme de WALL & DANIELSSON : cette amelioration consiste a detecter les 
  -- points caracteristiques de l'arc parmi ceux qui ne satisfont pas le cri-
  -- tere de surface. Un point est considere comme caracteristique lorsqu'il
  -- est le plus eloigne de l'origine.        
  --****                    I M P O R T A N T                            ****--
  --   Dans cette version amelioree, tout point caracteristique devient l'ori-
  --  gine de la portion d'arc restante                            
  -- 
  --  Les parametres =
  --    tab_points_init : liste des points constituamt l'arc initial
  --    nb_pts_init : dimension de la liste precedente
  --    tab_points_final : liste des points constituant l'arc final
  --    nb_pts_final : dimension de la liste precedente
  --    seuil : critere de selection/suppression des points

   procedure Wall_Danielsson_amelioree_CHO(tab_points_init : point_access_type;
                                           nb_pts_init : natural;
                                           tab_points_final : point_access_type;
                                           nb_pts_final : out natural;
                                           seuil: float; resolution : positive);





--***************************************************************************--
--** Specification du paquetage pour l'algorithme de BROPHY                **--
--***************************************************************************--

------------------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc                   --
--              navant : Nombre de points de l'arc                    --
--              seuil : Facteur de lissage compris entre 0 et 1       --
--                                                                    --
-- Sortie :                                                           --
--              tabres : Liste des points apres lissage               --
--              napres : Nombre de points apres lissage (idem navant) --
------------------------------------------------------------------------
procedure Brophyx(      tableau : in Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;
                        napres : out natural;
                        seuil : float );



--***************************************************************************--
--** Specification du paquetage pour l'algorithme de moyenne ponderee     **--
--***************************************************************************--

----------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc initial   --
--              navant : Nombre de points de l'arc            --
--              k : Type de voisinage (3, 5, 7...)            --
--                                                            --
-- Sortie :     tabres : Liste des points de l'arc resultat   --
--              napres : Nombre de points de l'arc            --
----------------------------------------------------------------
procedure paveragex(
                        tableau : Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;                     
                        napres : out natural;
                        k : natural ) ;



--***************************************************************************--
--** Specification du paquetage pour l'algorithme de moyenne ponderee     **--
--**                                 avec glissement vers le point moyen  **--
--***************************************************************************--

----------------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc                 --
--              navant : Nombre de points de l'arc                  --
--              k : Valeur du voisinage (3, 5, 7...)                --
--              slide : Facteur de glissement compris entre 0 et 1  --
--                                                                  --
-- Sortie :     tabres : Liste des points de l'arc resultat         --
--              napres : nombre de points de l'arc (idem navant)    --
----------------------------------------------------------------------
procedure saveragex(
                        tableau : Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;
                        napres : out natural;
                        k : natural;
                        slide : float );


--***************************************************************************--
--** Specification du paquetage pour l'algorithme de moyenne ponderee par  **--
--**                                                           distance    **--
--***************************************************************************--
                                                
-------------------------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc
--              navant : Nombre de points de l'arc
--              poids_points : Poids associe au point a deplacer
--              poids_dist : Poids associe a la somme pondere par les distances
--              k : Valeur du voisinage (3, 5, 7...)
--
-- Sortie :     tabres : Liste des points apres lissage
--              napres : Nombre de points de l'arc resultat (idem navant)
-------------------------------------------------------------------------------
procedure daveragex(
                        tableau : Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;
                        napres : out natural;
                        poids_point : float;
                        poids_dist : float;
                        k : natural ) ;
                                              

--***************************************************************************--
--** Specification du paquetage pour l'algorithme de THAPA                 **--
--***************************************************************************--


-------------------------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc initial                  --
--              navant : Nombre de points de l'arc                           --
--              seuil : Valeur permettant de controler le degre de detection --
--                      des points critiques ( < 1 )                         --
--                                                                           --
-- Sortie :     tabres : Liste des points critiques                          --
--              napres : Nombre de points critiques                          --
-------------------------------------------------------------------------------
procedure thapa1x(
                        tableau : Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;
                        napres : out natural;
                        seuil : float ) ;



--***************************************************************************--
--** Specification du paquetage pour l'algorithme de WHIRLPOOL             **--
--***************************************************************************--


---------------------------------------------------------------------------
-- Entree :     tableau : Liste des points de l'arc                      --
--              navant : Nombre de points de l'arc                       --
--              seuil : Indique la proximite maximale entre deux points  --
--              precision : Nombre de pixel/mm                           --
--                                                                       --
-- Sortie :     tabres : Liste des points apres simplification           --
--              napres : Nombre de points du nouvel arc                  --
---------------------------------------------------------------------------

 procedure whirlpoolx(
                        tableau : Point_Access_type;
                        navant : natural;
                        tabres : Point_Access_type;
                        napres : out natural;
                        seuil : float;
                        precision : positive ) ;



--***************************************************************************--
--** Specification du paquetage pour l'algorithme de LANG                  **--
--***************************************************************************--

--*************************************************************************--
--* Procedure Lang:                                                       *--
--*  Elle permet d'appliquer a un arc l'algorithme de LANG                *--
--*    tabi : liste des points constituant l'arc initial                  *--
--*    nbi : dimension de la liste precedente                             *--
--*    tabf : liste des points constituant l'arc final                    *--
--*    nbf : dimension de la liste precedente                             *--
--*    n : nombre de points d'analyse                                     *--
--*    t : tolerance                                                      *--
--*************************************************************************--

Procedure Lang  (       tabi    	:       Point_Access_type;
                        nbi     	:       natural;
                        tabf    	:       Point_Access_type;
                        nbf     	:       out natural;
                        n       	:       natural;
                        t      		:       float;
			resolution      :	positive   );


end;

