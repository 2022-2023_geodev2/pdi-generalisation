with GEN_IO; use GEN_IO;
with inipan; use inipan;
with norou; use norou;
with Win32.windef; use Win32.windef;
with Win32.winnt; use Win32.winnt;
with detpos; use detpos;
with unchecked_deallocation;
with unchecked_conversion;
with win32.winuser; use win32.winuser;
with system;
with Win32.wingdi; use Win32.wingdi;
with text_io; use text_io;

package NoRou2 is
	
package int_io is new text_io.integer_io(integer); use int_io; 
package flo_io is new text_io.float_io(float); use flo_io; 

PROCEDURE NOEUDS_IMPORTANTS(graphe : Graphe_type;
                            T_ARC_A : TGROUPEMENT;
                            C_T : integer;
                            TAB_NOEUDS : out Point_liste_type;
                            CRS_NOEUDS : in out integer);

Procedure DEPLACE(x : in out integer; Y : in out integer; an : float; dx : float; dy : float);

Procedure POIMUB_oblique(tpoi : typtipoi;
                         min : in point_type;
                         coin1 : in point_type;
                         angle : in float;    		         
                         emprise : in integer;  
                         hauteur : in integer;
                         z : in integer;
                         ecrase  : out boolean;
						 bord_image : out boolean;
                         poim : out typpoi;
						 mode_P : typcateg;
                         resolution : in positive);

FUNCTION ZONE_INFLUENCE(Tableau : point_liste_type;
                        crs : positive;
                        Ecart : integer) return Boite_type;

procedure RANGE_TOPO_DS_TAB(tab : in out POSITION; topo : position_type);

PROCEDURE DETECTION_PORTION(Graphe : graphe_type;
                            Legende : legende_type;
                            T_ARC_A : TGROUPEMENT;
                            TAB : POINT_LISTE_TYPE;
                            C_TAB1 : integer;
                            C_TAB2 : integer;
                            P1 : POINT_TYPE;
                            P2 : POINT_TYPE;
                            Z : integer;
                            h_car : integer;
                            Hauteur : in out integer;
                            Pds_s : in out Float;
                            resolution : in positive;
							Le_Seuil_S : in float;
							DistRoute : in float);

function POIDS_MILIEU_LOCAL (tab_p : point_liste_type;
                             c_p : integer;
                             tab : point_liste_type;
                             crs : integer;
                             P_g : point_type;
                             p_d : point_type) return float;

PROCEDURE CALCULE_POSITIONS(GRAPHE : graphe_type;
                            Legende : Legende_Type;
                            TAB_T : POINT_LISTE_TYPE;
                            C_T : Positive;
                            T_ARC_A : TGROUPEMENT;
                            TAB_NOEUDS : POINT_LISTE_TYPE;
                            CRS_NOEUDS : positive;
                            MT_TMP : in out MT;
                            TAB_POS : in out POSITION;
                            MT_COURT : out boolean;
                            Zie : in out Boite_type;
                            codetypo : in string30;
                            resolution : in positive;
							cale : boolean;
							kilo : boolean);

PROCEDURE CALCULE_POSITIONS_DECALE(GRAPHE : graphe_type;
                            Legende : Legende_Type;
                            TAB_T : POINT_LISTE_TYPE;
                            C_T : Positive;
                            T_ARC_A : TGROUPEMENT;
                            TAB_NOEUDS : POINT_LISTE_TYPE;
                            CRS_NOEUDS : positive;
                            MT_TMP : in out MT;
                            TAB_POS : in out POSITION;
                            MT_COURT : out boolean;
                            Zie : in out Boite_type;
                            codetypo : in string30;
                            resolution : in positive;
							cale : boolean;
							kilo : boolean);

PROCEDURE CALCULE_POSITIONS_HYDRO(GRAPHE : graphe_type;
                                  Legende : Legende_Type;
                                  TAB_TG : POINT_LISTE_TYPE;
                                  C_TG : integer;
                                  -- T_ARC_AG : TGROUPEMENT;
                                  TAB_TD : POINT_LISTE_TYPE;
                                  C_TD : integer;
                                  -- T_ARC_AD : TGROUPEMENT;
                                  MT_TMP : in out MT;
                                  TAB_POS : in out POSITION;
                                  MT_COURT : out boolean;
                                  Zie : in out Boite_type;
							      codetypo : in string30;
                                  resolution : in positive;
							      cale : Boolean;
								  DIST_H : in float;
								  TAB_TI : POINT_LISTE_TYPE;
                                  C_TI : integer;
								  ModePlacement : TypCateg;
								  l_m : in float);

PROCEDURE CALCULE_POSITIONS_AXE(GRAPHE : graphe_type;
                                Legende : Legende_Type;
                                TAB_T : POINT_LISTE_TYPE;
                                C_T : Positive;
                                T_ARC_A	 : TGROUPEMENT;
                                TAB_NOEUDS : POINT_LISTE_TYPE;
                                CRS_NOEUDS : positive;
                                MT_TMP : in out MT;
                                TAB_POS : in out POSITION;
                                MT_COURT : out boolean;     
                                Zie : in out Boite_type;
                                codetypo : in string30;
                                resolution : in positive;
								cale : Boolean;
								Mode_P : in typcateg;
								MTD : boolean);

PROCEDURE CALCULE_POSITIONS_DROIT(GRAPHE : graphe_type;
                                Legende : Legende_Type;
                                TAB_T : POINT_LISTE_TYPE;
                                C_T : Positive;
                                T_ARC_A	 : TGROUPEMENT;
                                TAB_NOEUDS : POINT_LISTE_TYPE;
                                CRS_NOEUDS : positive;
                                MT_TMP : in out MT;
                                TAB_POS : in out POSITION;
                                MT_COURT : out boolean;     
                                Zie : in out Boite_type;
                                codetypo : in string30;
                                resolution : in positive;
								Cale : Boolean);

PROCEDURE CALCULE_POSITIONS_QCQ(GRAPHE : graphe_type;
                                Legende : Legende_Type;
                                TAB_T : POINT_LISTE_TYPE;
                                C_T : integer;
                                T_ARC_A : TGROUPEMENT;
                                TAB_T2 : POINT_LISTE_TYPE;
                                C_T2 : integer;
                                T_ARC_A2 : TGROUPEMENT;
                                TAB_NOEUDS : POINT_LISTE_TYPE;
                                CRS_NOEUDS : positive;
                                MT_TMP : in out MT;
                                TAB_POS : in out POSITION;
                                MT_COURT : out boolean;     
                                Zie : in out Boite_type;
                                RTE : ROUTES;
                                Groupe : integer;
                                resolution : in positive;
								cale : boolean;
								MTD : boolean;
                                TAB_T3 : POINT_LISTE_TYPE;
                                C_T3 : integer;
								l_m : in float);

procedure Free_LPPAINTSTRUCT is new unchecked_deallocation(PAINTSTRUCT, PPAINTSTRUCT);
function UC1 is new Unchecked_Conversion(System.Address,Win32.PCCH);
function UC2 is new Unchecked_Conversion(System.Address,Win32.LPCSTR);
function UC3 is new Unchecked_Conversion(System.Address,Win32.PCHAR);
function Convert_Ac_Logfont_T_To_Ac_Textmetric_T is new Unchecked_Conversion(Source =>Win32.Wingdi.Ac_Logfont_T,Target =>Win32.Wingdi.Ac_Textmetric_T);

function longueur(nom : in string30;
                  ncar : in integer;
                  info_font: in typtfont;
                  Nofont : in integer;
                  hdc: Win32.windef.hdc) return integer;

function Det_longueur(nom : string30;
                      Ncar : integer;
                      info_font: in typtfont;
                      Nofont : in integer;
                      hWnd : in Win32.winnt.Handle) return integer;

function corps(info_font: in typtfont;
                Nofont : in integer;
				hdc: Win32.windef.hdc) return integer;

function Det_corps(info_font: in typtfont;
                   Nofont : in integer;
                   hWnd : in Win32.winnt.Handle) return integer;

procedure CALCULE_POLICE(META : in out MT;
                         info_font : in out typtfont;
                         nbfont :in integer;
                         T_TRS : in out TTRONCON;
                         T_ARCS : in out TARCS;
                         T_RTES : in out TROUTES;
						 R : integer;
                         HWnd : in Win32.Windef.HWND);

PROCEDURE RECHERCHE_MT_G(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
                         nbfont : integer;
                         RTE : ROUTES;
                         T_ARCS : in out TARCS;   
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         -- T_GROUP : TGROUPEMENT;
                         MT_TMP : in out MT;
                         TAB_POS : in out POSITION;
                         ZI_TMP : in out Boite_type;
                         HWnd : in Win32.Windef.HWND;
                         resolution : in positive;
						 cale : boolean);

PROCEDURE RECHERCHE_MT_D(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
                         nbfont : integer;
                         RTE : ROUTES;
                         T_ARCS : in out TARCS;   
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         crs_deb : integer;
                         MT_TMP2 : in out MT;
                         TAB_POS : in out POSITION;
                         ZI_TMP : in out boite_type;
                         HWnd : in Win32.Windef.HWND;
                         resolution : in positive);

Procedure Corrige_tab_points(T1 : point_liste_type;
                             c1 : integer;
                             T2 : in out point_liste_type;
                             c2 : in out integer;
                             T3 : in out TGROUPEMENT);

PROCEDURE RECHERCHE_MT_I(Graphe : graphe_type;
                         Legende : Legende_type;
                         info_font : in out typtfont;
                         nbfont : integer;
                         RTE : ROUTES;
                         -- I_ROUTE : integer;
                         T_ARCS : in out TARCS;
                         -- T_GROUP : TGROUPEMENT;
                         T_TRS : in out TTRONCON;
                         T_RTES : in out TROUTES;
                         TI_D : integer;
                         TI_F : integer;
                         T_MT : in out TMT;
                         c_TMT : in out integer;
                         TAB_POS : in out POSITIONS;
                         T_ZI : in out typtbe;
                         HWnd : in Win32.Windef.HWND;
						 resolution : in positive);


PROCEDURE RECHERCHE_MT(graphe : graphe_type;
                       Legende : Legende_type;
                       T_RTES : in out TROUTES;
                       T_MT : in out TMT;
                       T_TRS : in out TTRONCON;
                       T_ARCS : in out TARCS;
                       -- T_GROUP : TGROUPEMENT;
                       TAB_POSITION : in out POSITIONS;
                       T_ZI : in out typtbe;
                       HWnd : in Win32.Windef.HWND;
					   invEchelle : in integer;
					   resolution : in positive);

end NoRou2;
