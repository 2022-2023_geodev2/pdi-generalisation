--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--*************************************************************************

------------------------------------------------------------------------------
-- Ce package concerne la definition des structures et des fonctions ---------
-- utilisees pour determiner les positions et les interactions entre ---------
-- positions:  - Determination d'une liste de points de depart de placements -
-- 	       - Determination de la geometrie des positions -----------------
--	       - Filtrage des positions  -------------------------------------
-- 	       - Determination du poids propre des positions -----------------
-- 	       - Tri des maxp meilleures positions pour chaque objet ---------
--  	       - Determination des interactions entre positions --------------
------------------------------------------------------------------------------


with gen_io; use gen_io;
with inipan; use inipan;
with tiff_io; use tiff_io;
with substitution_io; use substitution_io;
with text_io; use text_io;

 
package detpos is
  
  package int_io is new text_io.integer_io(integer); use int_io;
   
--========================================================================
--===================== Positions des noms ===============================
--========================================================================

type typTabAngle is array (positive range <>) of Float;
type typTabCoin1 is array (positive range <>) of point_type;
type typTabPoi is array (positive range <>) of typPoi;
type typTabNGeom is array (positive range <>) of integer;
type typTabPlusPi is array (positive range <>) of boolean;


type POSITION_TYPE is record
  coin1 : typTabCoin1(1..NbMaxMots) := (others => (0,0)); -- Coord. du coin bas gauche du nom.
  coin2 : point_type:=(0,0)  ; -- Coord. du coin bas gauche de la 2e ligne.
  nl    : positive range 1..2; -- Nombre de lignes du nom. 
  Nb_Mots : integer:=1; -- Nombre de mots de l'ecriture a disposition
  angle : typTabAngle(1..NbMaxMots) := (others => 0.0); -- Orientation de la position.
  poim  : typTabPoi(1..NbMaxMots) := (others => 0.0); -- Poids de mutilation de la position.
  poid  : typTabPoi(1..NbMaxMots) := (others => 0.0); -- Poids de distance de la position.
  poip  : typPoi:=0.0        ; -- Poids propre de la position.
  dnc   : natural:=0         ; -- Distance du nom � l'objet du graphe.
  dir   : typdir:=int        ; -- Direction de la position.
  poss  : boolean:=true      ; -- Indicateur de possibilite de la position.
  plusPi : typTabPlusPi(1..NbMaxMots):=(others => false); -- Position de cote de courbe dans le sens inverse de lecture : pi a ajouter
  Geom : Point_access_type; -- G�om�tries support d'une �criture � disposition � support courbe.
  NGeom : TypTabNGeom(1..NbMaxMots):=(others => 0); -- Nombre de vertex des g�om�tries support d'une �criture � disposition sur courbe.
  Offset : typTabAngle(1..NbMaxMots):=(others => 0.0); -- D�calage entre les mots s�parables des �critures � support courbe et leur g�om�trie support de sortie. Champ utile � l'import des r�sultats de placement sous Lamps2
  Point1 : typTabCoin1(1..NbMaxMots) := (others => (0,0));
  Point2 : typTabCoin1(1..NbMaxMots) := (others => (0,0));
  Justification : typTabAngle(1..NbMaxMots):=(others => 0.0); -- Justification au sens de DataDraw3 des mots s�parables des �critures � support courbe par rapport a leur g�ometrie support de sortie (0.0 = d�but du mot au d�but de la g�ometrie; 1.0 = fin du mot � la fin de la g�ometrie).
end record;



pragma Pack(POSITION_TYPE);

type typTNP is array(positive range<>) of integer;
type typTP  is array(positive range<>,positive range <>) of position_type;

type typnpp is new natural range 0..6;
type typTPP is array(typnpp) of position_type;

--=====================================================================
--============== Interactions entre positions =========================
--=====================================================================
-- couple ecriture/position
-- ex i/j -> ecriture Tbnomi(i) / position Tp(i,j)
type obj_pos is record
        obj: integer;
        pos: integer;
end record;

-- Nb de positions en interaction avec chaque position de chaque objet:
type typTNPI is array(positive range <>,positive range <>) of integer;
-- Pointeur sur la liste LPI
type typTPPI is array(positive range <>,positive range <>) of integer;
-- Liste des couples ecriture/position:
type typLPI is array(positive range <>) of OBJ_POS;



----==========================================================================
-----------------------------------------------------------------------------
---- Ces procedures determinent une liste de points a partir desquels -------
---- seront determinees les positions (a partir d'un maillage, d'un cercle -- 
---- ou d'une face d'un graphe ----------------------------------------------
-----------------------------------------------------------------------------

procedure POINTS_DE_CERCLE (no    :in integer;
                            tbnomi:in typtbnomi;
                            n     :in out integer;
                            contpt:in out point_liste_type;
                            iest,inord,iouest,isud:out natural;
                            viewer : in boolean);


--====================================================================
-- determination des positions possibles:
-----------------------------------------
procedure DET_POSITIONS (tbnomi: in out typtbnomi;
                         tnci : in typtnci;
                         tici : in typtici; -- tableau des �critures en interaction
                         tzi : in typtbe;
                         tnp : in out typtnp;
                         tp : in out typtp; -- tableau des positions possibles
                         mutil : in boolean;
                         Nb_symboles : in integer;
                         InvEchelle : in integer;
                         Resolution : in integer;
                         Minimum_terrain : in point_type_reel;
                         Maximum_terrain : in point_type_reel;
                         top : in out integer;
						 NbNom : in integer);

--====================================================================
-- Determination des interactions entre positions:
------------------------------------------------
procedure DET_PI(tbnomi:in out typtbnomi;
                 tnci  :in typtnci;
                 tici  :in typtici; -- tableau des �critures en interaction
                 tnp   :in typtnp;
                 tp    :in typtp; -- tableau des positions possibles
                 tnpi  :in out typtnpi;
                 tppi  :in out typtppi;
                 lpi   :in out typlpi ); -- liste des couples ecriture/position

--====================================================================
                  
                  
-- types utiles au controle de la coherence vecteur-image
type typverdict is (passe,echoue,pasfait1,pasfait2,pasfait3,pasfait4);
-- pasfait1 : aucun valeur d'index n'est interpretee comme interdite par tous les codes typo
type typinter is (inter, pinter, binter, fond);
-- inter=interdit  pinter=pas interdit  binter=bande interdite
type typtabstat is array(integer range <>, typinter range <>) of integer;                                                                                              
type acces_typtabstat is access typtabstat;
                                             
end detpos;
