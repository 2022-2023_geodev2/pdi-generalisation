--*************************************************************************
--************* PROGRAMME DE PLACEMENT DE TOPONYMES ***********************
--****************** M�thode de recuit simul� *****************************
--*************************************************************************

with gen_io; use gen_io;
with ada.numerics.discrete_random ;
with math_int_basic; use math_int_basic; 
with ada.numerics.elementary_functions; use ada.numerics.elementary_functions;
with norou; use norou;
with gb; use gb;

Package body Recuit is
  
--=======================================================================
-- Fonction qui retourne le nombre de positions actives en interaction --
-- avec une position donnee d'un nom donne ------------------------------
--=======================================================================
Function NB_POS_ACTIVE_INTER(tbnomi  :typtbnomi;
                             tnpi    :typtnpi;
                             tppi    :typtppi;
                             lpi     :typlpi;
                             nom     :integer;
                             position:integer) return integer is
 n:integer:=0;
begin
  if tnpi(nom,position)/=0 then
    for pi in 1..tnpi(nom,position) loop
      if tbnomi(lpi(tppi(nom,position)+pi-1).obj).p_choisie
            =lpi(tppi(nom,position)+pi-1).pos then n:=n+1;
      end if;
    end loop;
  end if;
  return n;
end NB_POS_ACTIVE_INTER;

--=============================================================
-- Fonction qui retourne une nouvelle transition pour un nom --
--=============================================================
Function TRANSITION(tbnomi:typtbnomi;
                    tnpi  :typtnpi;
                    tppi  :typtppi;
                    lpi   :typlpi;
                    tp    :typtp;
                    nom   :integer;
                    nouvelle_position:integer) return transition_type is
 t: transition_type;           
 ninter_nouv:integer:=NB_POS_ACTIVE_INTER(tbnomi,tnpi,tppi,lpi,nom,nouvelle_position);
begin
  t.num_nom:= nom;
  t.num_ancienne_position:= tbnomi(nom).p_choisie;
  t.num_nouvelle_position:= nouvelle_position;
  ----- si la ville n'a pas de position active -----
  if tbnomi(nom).p_choisie=0 then
    t.delta_poids:=tp(nom,nouvelle_position).poip;
    t.delta_nbpos_inter:=ninter_nouv;
  else
    t.delta_poids:=tp(nom,nouvelle_position).poip-tp(nom,tbnomi(nom).p_choisie).poip;
    t.delta_nbpos_inter:=ninter_nouv-NB_POS_ACTIVE_INTER(tbnomi,tnpi,tppi,lpi,nom,tbnomi(nom).p_choisie);
  end if;

  return t;
end TRANSITION;

--==================================================================
-- Fonctions de calcul de difference de poids pour une transition --
--==================================================================
Function TRANS_EVAL(delta_poids:float;delta_nbpos_inter:integer) return float is
  begin
     return delta_poids+OA_FACTOR*float(delta_nbpos_inter);
  end TRANS_EVAL;

--=========================================================================
Function TRANS_COST(transition: transition_type) return float is
   begin
     return TRANS_EVAL(transition.delta_poids,transition.delta_nbpos_inter);
   end TRANS_COST;

--========================================================================
-- Procedure qui opere une transition et calcule les nouvelles energies --
--========================================================================
Procedure TRANS_OPER(tbnomi:in out typtbnomi; transition:transition_type) is
begin
  tbnomi(transition.num_nom).p_choisie:=transition.num_nouvelle_position;
  UE := UE +transition.delta_poids;
  OAE:= OAE+transition.delta_nbpos_inter;
  E  := E  +TRANS_COST(transition);
end TRANS_OPER;

--=============================================================
-- Procedure qui initialise les energies dans l'etat initial --
--=============================================================
Procedure TRANS_INIT(tbnomi:typtbnomi;
                     tnpi  :typtnpi;
                     tppi  :typtppi;
                     lpi   :typlpi;
                     tp    :typtp ) is

ninter: integer:=0;

begin
	
  OAE:=0;
  UE:=0.0;
  E:=0.0;
  for nom in 1..tbnomi'last loop
  	ninter:=0;
    if tbnomi(nom).p_choisie/=0 then
      if tnpi(nom,tbnomi(nom).p_choisie)/=0 then
        for pi in 1..tnpi(nom,tbnomi(nom).p_choisie) loop
          if tbnomi(lpi(tppi(nom,tbnomi(nom).p_choisie)+pi-1).obj).p_choisie
             =lpi(tppi(nom,tbnomi(nom).p_choisie)+pi-1).pos then
            ninter:=ninter+1;
          end if;
        end loop;
      end if;
      OAE:=OAE+ninter;
      UE :=UE +Tp(nom,(tbnomi(nom).p_choisie)).poip;
    end if;
  end loop;
  OAE:=integer(OAE/2);
  E  :=UE+OA_FACTOR*float(OAE);
end TRANS_INIT;

-- ===================================================================
-- -- Fonction qui retourne aleatoirement une transition -------------
-- ===================================================================
Function FIND_TRANSITION(tbnomi:typtbnomi;
                         tnpi  :typtnpi;
                         tppi  :typtppi;
                         lpi   :typlpi;
                         tnp   :typtnp;
                         tp    :typtp ) return transition_type is
--------------------------------------------------------------------------------------------  
    subtype un_nombre is  positive range 1..tbnomi'last-1;
    package alea_topo is new ada.numerics.discrete_random(un_nombre);
    subtype une_position is positive range 1..maxp;
    package alea_posit is new ada.numerics.discrete_random(une_position);
  -- fonction renvoyant une valeur    comprise entre 0 et taillemax
  function alea (taillemax : in integer) return une_position is
      gen_posit :alea_posit.generator;
      res : une_position;
  begin
      alea_posit.reset(gen_posit);
      loop
      res:=alea_posit.random(gen_posit);
      if   res<=taillemax then exit; end if;
      end loop;
      return res;
  end alea ;
  ------------------------------------------------------------------------------------------------                                      
  ok:boolean:=false;
  nom,n_alea,position,p_alea:integer;
  gen_topo :alea_topo.generator;
   
begin
  alea_topo.reset(gen_topo);
  while ok=false loop
	-- Choix aleatoire d'un nom --
    n_alea:=alea_topo.random (gen_topo)+1;
    if Tnp(n_alea)>1 then        
	    -- Choix aleatoire d'une position pour ce nom --
      p_aleA:=alea(Tnp(n_alea));
      if p_alea/=Tbnomi(n_alea).p_choisie then
        nom:=n_alea;
        position:=p_alea;
        ok:=true;
      end if;
    end if;
  end loop;
  return TRANSITION(tbnomi,tnpi,tppi,lpi,tp,nom,position);
end FIND_TRANSITION;

--==============================================================================
-- Procedure de descente de gradient appelee en fin de recuit --
-- OPTIMISATION: sauvegarde initiale de toutes les transitions possibles 
-- dans un tableau et calcul des nouvelles transitions uniquement autour 
-- du nom subissant une transition lors de la descente de gradient --
--===================================================================
Procedure DESCEND(tbnomi:in out typtbnomi;
                  tnpi  :typtnpi;
                  tppi  :typtppi;
                  lpi   :typlpi;
                  tnp   :typtnp;
                  tp    :typtp ) is
                    
 nom,position,taille:integer;
 seuil,best_cost,cost:float;
 encore:integer:=1;
 best_t:transition_type;
 TTV:liens_array_type(1..tbnomi'last);
 -- TTV:tableau de pointeurs sur le tableau TT --
 -- Memorise la ligne de la 1ere transition pour chaque nom --
 cpt : integer:=1;
 -- Fic_transition : text_io.file_type;
 -- Fic_UE_E_OAE_apres_recuit : text_io.file_type;
 -- Fic_UE_E_OAE_descend : text_io.file_type;
 
 begin	
   --======================================================
   -- initialisation de toutes les transitions possibles --
   --------------------------------------------------------
   ttv(1):=1;
   for no in 2..tbnomi'last loop
     if Tnp(no-1)>1 then
       ttv(no):=ttv(no-1)+Tnp(no-1);
     else
       ttv(no):=ttv(no-1);
     end if;
   end loop;

   -- TAILLE: lgr du tab TT (somme des pos poss de chaque nom)
   -----------------------------------------------------------
   if Tnp(tbnomi'last)>1 then 
     taille:=ttv(tbnomi'last)+Tnp(tbnomi'last)-1;
   else
     taille:=ttv(tbnomi'last)-1;
   end if;
 
   DECLARE
     type ttransition_type is array (natural range <>) of transition_type;
     tab_transition:ttransition_type(1..taille);
     nom_corresp:integer;
   BEGIN
	 -- create(Fic_transition,out_file,nomfichier_objet(1..ncar_fo-4)&"_transition.txt","");	
	 -- put(Fic_transition,tbnomi'last);
     -- put_line(Fic_transition,"");
     for no in 1..tbnomi'last loop
	 	
	   -- put(Fic_transition,no);
       -- put_line(Fic_transition,"");
       if Tnp(no)>1 then
         for position in 1..Tnp(no) loop
           if position/=Tbnomi(no).p_choisie then
             tab_transition(ttv(no)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,no,position);
           else
             tab_transition(ttv(no)+position-1):=(no,0,0,0,0.0);
           end if;
         end loop;
       end if;
     end loop;

     -- put_line(Fic_transition,"FIN");
	 -- close(Fic_transition);

     -- create(Fic_UE_E_OAE_apres_recuit,out_file,nomfichier_objet(1..ncar_fo-4)&"_ue-e-oae_apres_recuit.txt","");

	   -- trans_init(tbnomi,tnpi,tppi,lpi,tp);

     -- flo_io.Put(Fic_UE_E_OAE_apres_recuit,E);
     -- flo_io.Put(Fic_UE_E_OAE_apres_recuit,UE);
     -- int_io.Put(Fic_UE_E_OAE_apres_recuit,OAE);
     -- close(Fic_UE_E_OAE_apres_recuit);



     -- ==========================================================
     -- Descente de gradient tant qu'un moindre cout est trouve --
     -- ==========================================================

     -- create(Fic_UE_E_OAE_descend,out_file,nomfichier_objet(1..ncar_fo-4)&"_ue-e-oae_descend.txt","");

     while encore=1 loop
       -- gb.msgbox(integer'image(cpt));
	   cpt:=cpt+1;
       encore:=0;
       best_cost:=0.0;
       for i in 1..taille loop
         if tab_transition(i)/=(tab_transition(i).num_nom,0,0,0,0.0) then
           cost:=TRANS_COST(tab_transition(i));
           if cost<best_cost then
             best_cost:=cost;
             best_t:=tab_transition(i);
             encore:=1;
           end if;
         end if;
       end loop;

	   -- critere d'arret brutal car il existe des cas o�
	   -- le critere d'arret ci-dessus n'est jamais v�rifi�
       if cpt=15000 then
	   	 encore:=0;
       end if;

     if encore=1 then

       TRANS_OPER(tbnomi,best_t);
	   -- trans_init(tbnomi,tnpi,tppi,lpi,tp);

       --================================================
       -- Nouveau calcul des transitions pour la ville --
       --------------------------------------------------
       for position in 1..tnp(best_t.num_nom) loop
         if Tbnomi(best_t.num_nom).p_choisie/=position then
           tab_transition(ttv(best_t.num_nom)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,best_t.num_nom,position);
         else
           tab_transition(ttv(best_t.num_nom)+position-1):=(best_t.num_nom,0,0,0,0.0);
         end if;
       end loop;
       --=========================================================== 
       -- Nouveau calcul des transitions pour les villes alentour --
       -------------------------------------------------------------
       if tnpi(best_t.num_nom,best_t.num_nouvelle_position)/=0 then
         for pi in 1..tnpi(best_t.num_nom,best_t.num_nouvelle_position) loop
           nom_corresp:=lpi(tppi(best_t.num_nom,best_t.num_nouvelle_position)+pi-1).obj;
           if lpi(tppi(best_t.num_nom,best_t.num_nouvelle_position)+pi-1).pos
               =tbnomi(nom_corresp).p_choisie then
             for position in 1..tnp(nom_corresp) loop
               if position/=tbnomi(nom_corresp).p_choisie then
                 tab_transition(ttv(nom_corresp)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,nom_corresp,position);
               end if;
             end loop;
           else
             for position in 1..tnp(nom_corresp) loop
               if position=lpi(tppi(best_t.num_nom,best_t.num_nouvelle_position)+pi-1).pos then
                 tab_transition(ttv(nom_corresp)+position-1)
                   :=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,nom_corresp,position);
                 exit;
               end if;
             end loop;
           end if;
         end loop;
       end if;   

       if tnpi(best_t.num_nom,best_t.num_ancienne_position)/=0 then
         for pi in 1..tnpi(best_t.num_nom,best_t.num_ancienne_position) loop
           nom_corresp:=lpi(tppi(best_t.num_nom,best_t.num_ancienne_position)+pi-1).obj;
           if lpi(tppi(best_t.num_nom,best_t.num_ancienne_position)+pi-1).pos
               =tbnomi(nom_corresp).p_choisie then
             for position in 1..tnp(nom_corresp) loop
               if position/=tbnomi(nom_corresp).p_choisie then
                 tab_transition(ttv(nom_corresp)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,nom_corresp,position);
               end if;
             end loop;
           else
             for position in 1..tnp(nom_corresp) loop
               if tnpi(best_t.num_nom,best_t.num_nouvelle_position)/=0 then
                 if position =lpi(tppi(best_t.num_nom,best_t.num_nouvelle_position)+pi-1).pos then
                   tab_transition(ttv(nom_corresp)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,nom_corresp,position);
                   exit;
                 end if;
               end if;
             end loop;
           end if;
         end loop;
       end if;


--     for no in 1..tbnomi'last loop
--       if Tnp(no)>1 then
--         for position in 1..Tnp(no) loop
--           if position/=Tbnomi(no).p_choisie then
--             tab_transition(ttv(no)+position-1):=TRANSITION(tbnomi,tnpi,tppi,lpi,tp,no,position);
--           else
--             tab_transition(ttv(no)+position-1):=(no,0,0,0,0.0);
--           end if;
--         end loop;
--       end if;
--     end loop;


     end if;

	 -- int_io.put(Fic_UE_E_OAE_descend,cpt);
     -- flo_io.Put(Fic_UE_E_OAE_descend,E);
     -- flo_io.Put(Fic_UE_E_OAE_descend,UE);
     -- int_io.Put(Fic_UE_E_OAE_descend,OAE);
	 -- flo_io.put(Fic_UE_E_OAE_descend,best_cost);
	 -- put_line(Fic_UE_E_OAE_descend,"");

     end loop; -- Boucle While
     -- put_line(Fic_UE_E_OAE_descend,"FIN");
     -- close(Fic_UE_E_OAE_descend);
   END;
End DESCEND;                                                 


--====================================================================
-- Procedure de chargement des parametres de recuit:
---------------------------------------------------- 
procedure Charge_param_recuit is
  ficpar: text_io.file_type;
  begin
    text_io.open(ficpar,text_io.in_file,nomfichier_param_recuit(1..ncar_fpr));
    skipandget(ficpar,init_niter);
    skipandget(ficpar,delta_niter);
    skipandget(ficpar,init_seuil);
    skipandget(ficpar,fac_seuil);
    skipandget(ficpar,mini_seuil);
    skipandget(ficpar,oa_factor);
    text_io.close(ficpar);
  end ;

-- =======================================================================
-- ------------ Procedure de recuit simule -------------------------------
-- =======================================================================
Procedure RECUI(tbnomi:in out typtbnomi;
                tnpi  :typtnpi;
                tppi  :typtppi;
                lpi   :typlpi;
                tnp   :typtnp;
                tp    :typtp ) is
                  
seuil:float:=init_seuil;
niter:integer:=init_niter;
transition:transition_type;
nt :integer:=0;
ntp:integer:=0;
ct :float;
coupl_inter:obj_pos;
reponse:character;
nrep,ni:integer:=0;
classe1,classe2 : integer;

--fic_debug : text_io.file_type ;
-- Fic_mini_seuil : text_io.file_type;
-- Fic_UE_E_OAE_ini : text_io.file_type;


begin
  TRANS_INIT(tbnomi,tnpi,tppi,lpi,tp);

--  create(Fic_UE_E_OAE_ini,out_file,nomfichier_objet(1..ncar_fo-4)&"_ue-e-oae_ini.txt","");
--  flo_io.Put(Fic_UE_E_OAE_ini,UE);
--  flo_io.Put(Fic_UE_E_OAE_ini,E);
--  int_io.Put(Fic_UE_E_OAE_ini,OAE);
--  close(Fic_UE_E_OAE_ini);

  for no in 1..tbnomi'last loop
    if tnp(no)/=0 and tbnomi(no).p_choisie=0 then
      ni:=ni+1;
    end if;
  end loop;
  
    --============================================================
    -- Calcul de transition aleatoire tant que le seuil est sup.--
    ------------------ a mini_seuil=0.05 -------------------------

  -- create(Fic_mini_seuil,out_file,nomfichier_objet(1..ncar_fo-4)&"_mini_seuil.txt","");
  while seuil>mini_seuil loop
    ntp:=0;
    for i in 1..niter loop
      transition:=FIND_TRANSITION(tbnomi,tnpi,tppi,lpi,tnp,tp);
      ct:=TRANS_COST(transition);
      if ct<seuil then
        TRANS_OPER(tbnomi,transition);
		-- trans_init(tbnomi,tnpi,tppi,lpi,tp);
        ntp:=ntp+1;
      end if;
    end loop;
    ntp:=ntp+1;
    nt :=nt+ntp;
    seuil:=seuil*float(fac_seuil)/100.0;
    niter:=niter*delta_niter/100;

    -- int_io.put(Fic_mini_seuil,ntp);
    -- flo_io.Put(Fic_mini_seuil,seuil);
    -- Put_line(fic_mini_seuil,"");
  end loop;
  -- put_line(Fic_Mini_seuil,"FIN");
  -- close(Fic_mini_seuil);

  -- Descente de gradient ---------
  DESCEND(tbnomi,tnpi,tppi,lpi,tnp,tp);

    -- Elimination des positions se recouvrant apres Recuit --
    for i in 1..tbnomi'last loop		      
     if tbnomi(i).p_choisie/=0 then
      if NB_POS_ACTIVE_INTER(tbnomi,tnpi,tppi,lpi,i,tbnomi(i).p_choisie)/=0 then
	      for pi in 1..tnpi(i,tbnomi(i).p_choisie) loop
	        coupl_inter:=lpi(tppi(i,tbnomi(i).p_choisie)+pi-1);
    	    if tbnomi(coupl_inter.obj).p_choisie=coupl_inter.pos then 
	   -- Prise en compte du plus grand des 2 noms se recouvrant:
           if coupl_inter.obj>i then -- soit un recouvrt. pas encore traite:
--             put_line(fic_debug, tbnomi(i).chaine(1..tbnomi(i).ncar)&" et "&
--	             tbnomi(coupl_inter.obj).chaine(1..tbnomi(coupl_inter.obj).ncar)&
--	             " se recouvrent. ");
               
               -- debut gestion des priorit�s de placement
               case tbnomi(i).Topo.mode_placement is
                 when CAxe => Classe1:=1;
                 when CDesaxe => classe1:=1;
                 when interieur => Classe1:=2;
                 when mordant => Classe1:=2;
                 when exterieur => Classe1:=2;
                 when decentre => Classe1:=2;
                 when centre => Classe1:=2;
				 when Decale => Classe1:=2;
                 when desaxe => Classe1:=3;
                 when axe => Classe1:=3;
                 when Droit => Classe1:=3;
				 when Courbe => Classe1:=4;
               end case;
               case tbnomi(coupl_inter.obj).Topo.mode_placement is
                 when CAxe => Classe2:=1;
                 when CDesaxe => Classe2:=1;
                 when interieur => Classe2:=2;
                 when mordant => Classe2:=2;
                 when exterieur => Classe2:=2;
                 when decentre => Classe2:=2;
                 when centre => Classe2:=2;
				 when Decale => Classe2:=2;
                 when desaxe => Classe2:=3;
                 when axe => Classe2:=3;
                 when Droit => Classe2:=3;
				 when Courbe => Classe2:=4;
               end case;

               if (classe1<classe2)
			   or (classe1=classe2 and classe1=1 and tbnomi(i).Nb_Mots*tbnomi(i).Corps>tbnomi(coupl_inter.obj).Nb_Mots*tbnomi(coupl_inter.obj).Corps)
			   or (classe1=classe2 and classe1=2 and tbnomi(i).NPic>0 and tbnomi(coupl_inter.obj).NPic=0)
			   or (classe1=classe2 and classe1=2 and tbnomi(i).NPic=0 and tbnomi(coupl_inter.obj).NPic=0 and tbnomi(i).Corps*tbnomi(i).Ln(1)>tbnomi(coupl_inter.obj).corps*tbnomi(coupl_inter.obj).ln(1)) then
			   -- or (classe1=classe2 and classe1=2 and (tbnomi(i).NPic=0 or tbnomi(coupl_inter.obj).NPic>0) and tbnomi(i).Corps*tbnomi(i).Ln(1)>tbnomi(coupl_inter.obj).corps*tbnomi(coupl_inter.obj).ln(1)) then
		       	 tbnomi(coupl_inter.obj).p_choisie:=0;
		       else
		         -- tbnomi(coupl_inter.obj).p_choisie:=0;
		         tbnomi(I).p_choisie:=0;
				 goto next_i;
               end if;
               -- fin gestion des priorit�s de placement

--	           if -- (tbnomi(i).Id>0 and tbnomi(coupl_inter.obj).id<0) or
--			      -- priorite de l'hydro sur tout le reste
--			      (tbnomi(i).Topo.mode_placement=CAXE or tbnomi(i).Topo.mode_placement=CDESAXE) and (tbnomi(coupl_inter.obj).Topo.mode_placement/=CAXE and tbnomi(coupl_inter.obj).Topo.mode_placement/=CDESAXE)
--			      -- priorite de l'horizontal sur numeros de route et cotes de courbe
--				  (tbnomi(i).Id>0 and tbnomi(coupl_inter.obj).id<0 and tbnomi(coupl_inter.obj).Topo.mode_placement/=CAXE and tbnomi(coupl_inter.obj).Topo.mode_placement/=CDESAXE)
--			      -- priorite de numeros de route sur cotes de courbe
--   			   or (tbnomi(i).NPic>0 and tbnomi(coupl_inter.obj).NPic=0)
--			   or ((tbnomi(i).Id/abs(tbnomi(i).Id))*tbnomi(coupl_inter.obj).id>0 and tbnomi(i).ln>tbnomi(coupl_inter.obj).ln) then
----	           if tbnomi(i).ln>tbnomi(coupl_inter.obj).ln then
--		         tbnomi(coupl_inter.obj).p_choisie:=0;         ------ ici pour priorite aux horizontales
----               put_line(fic_debug, 
----                    tbnomi(coupl_inter.obj).chaine(1..tbnomi(coupl_inter.obj).ncar)&
----		                " est ecarte");
--               else
--		         tbnomi(i).p_choisie:=0;
----               put_line(fic_debug, tbnomi(i).chaine(1..tbnomi(i).ncar)&
----                        " est ecarte");
--		           goto next_i;
--	           end if;
 	         end if;		
	       end if;
   	   end loop;--pi
      end if;
     end if;
     <<next_i>> null;
    end loop;--i
    
end RECUI;

--===================================================================
End Recuit;

