# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce module à pour objectif de mettre à disposition les principales         ##
##  fonctions des algorithmes GALBE développés en ADA, ainsi que quelques     ##
##  fonctions de lissage et filtrage. Il est le seul destiné à etre importé   ##
##  pour utiliser les algorithmes en python                                   ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-05-10                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################
import sys
path = "/".join(__file__.split("/")[:-1])
sys.path.insert(1, path)

from bindage import (
    douglas_peucker,
    filtre_gaussien,
    faille_max,
    accordeon,
    schematisation,
    decoupage_conflits,
    decoupage_conflits_pts,
    indicateur_conflits,
    nb_intersections,
    filtrage,
    buffer,
    )

from faille_min import (
    faille_min,
)
from dev_galbe import (
    galbe,
    )

import WKT
import graphe as GR

sys.path.pop(sys.path.index(path))
del path, sys


PARAMS_GALBE_SIGNIFICATION = (
    "Separabilite du premier decoupage",
    "Sigma du lissage des zones sans conflit",
    "Exageration de faille max",
    "Exageration de l'accordeon",
    
    "Separabilite du deuxieme decoupage",
    "Exageration de faille min",
    "Amortissement des deplacements",
    "Critere d'ecart maxi de l'accordeon",
    
    "Nombre maximal de schematisations",
    "Seuil de Douglas et Peucker pour le filtrage apres fusion",
    "Sigma pour Gauss dans le lissage final apres fusion")

PARAMS_GALBE_DEFAULT = (
    2.5,  0  ,   0  ,  0  ,
    0  ,  0  ,  50  ,  0.5,
    5  ,  0.5,   2.5)
