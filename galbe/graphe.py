# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier à pour objectif fournir une structure de graphe permettant la  ##
##  manipulation de noeuds et d'arcs, comme la structure développéen en ADA   ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-04-05                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
CONVENTION UTILISEE:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""

import geometry as _geometry
import matplotlib.pyplot as _plt
import WKT as _WKT
try:
    import pandas as _pd
except ImportError:
    # ce module n'est utilisé que pour lire un fichier au format csv.
    # Il ne sera pas utilié dans l'intégration ArcGis
    class _pd:
        def read_csv(self, *args):
            raise ImportError("Erreur lors de l'import du module pandas")
import propagation as _propagation

class _Arc:
    def __init__(self, lPts, noeud1, noeud2, largeur, attributs = {}):
        self.lPts = lPts
        self.noeuds = (noeud1, noeud2)
        self.largeur = largeur
        self.attributs = attributs

class _Noeud:
    def __init__(self, pt):
        self.pt = pt
        self.arcs = set()

class Graphe:
    def __init__(self):
        self._noeuds = []
        self._arcs = []

    def creer_noeud(self, pt):
        if None in self._noeuds:
            index = self._noeuds.index(None)
            self._noeuds[index] = _Noeud(pt)
            return index
        else:
            self._noeuds.append(_Noeud(pt))
            return len(self._noeuds)-1

    def creer_noeud_sur_arc(self, num_arc, pt):
        arc = self.get_arc(num_arc)
        lPts = arc.lPts
        num_noeud_1, num_noeud_2 = arc.noeuds

        pos = _geometry.get_pos_to_insert_pt(lPts, pt, False)
        lPts1 = lPts[:pos] if lPts[pos-1] == pt else lPts[:pos] + [pt]
        lPts2 = lPts[pos:] if lPts[ pos ] == pt else [pt] + lPts[pos:]
        num_noeud_inter = self.creer_noeud(pt)

        arc.lPts = lPts1
        arc.noeuds = (num_noeud_1, num_noeud_inter)

        num_arc2 = self.creer_arc(lPts2, num_noeud_inter, num_noeud_2, arc.largeur, arc.attributs)
        self.get_noeud(num_noeud_2).arcs.remove(num_arc)
        self.get_noeud(num_noeud_2).arcs.add(num_arc2)
        self.get_noeud(num_noeud_inter).arcs.add(num_arc)
        
        return num_arc, num_arc2, num_noeud_inter

    def retourne_arc(self, num_arc):
        arc = self.get_arc(num_arc)
        arc.lPts = arc.lPts[::-1]
        arc.noeuds = arc.noeuds[::-1]

    def fusion_noeud(self, num_noeud):
        noeud = self.get_noeud(num_noeud)
        if len(noeud.arcs) != 2:
            raise ValueError("Pour fusionner un noeud, celui-ci doit être lié à exactement 2 arcs")

        num_arc1, num_arc2 = noeud.arcs
        
        arc_1 = self.get_arc(num_arc1)
        arc_2 = self.get_arc(num_arc2)

        if arc_1.lPts[0] == noeud.pt:
            self.retourne_arc(num_arc1)
        if arc_2.lPts[-1] == noeud.pt:
            self.retourne_arc(num_arc2)

        arc_1.lPts += arc_2.lPts[1:]
        arc_1.noeuds = (arc_1.noeuds[0], arc_2.noeuds[1])

        noeud2 = self.get_noeud(arc_2.noeuds[1])
        noeud2.arcs.remove(num_arc2)
        noeud2.arcs.add(num_arc1)

        self._arcs[num_arc2] = None
        self._noeuds[num_noeud] = None
        return num_arc1

    def delete_arc(self, num_arc):
        num_noeud1, num_noeud2 = self.get_arc(num_arc).noeuds

        self.get_noeud(num_noeud1).arcs.remove(num_arc)
        self.get_noeud(num_noeud2).arcs.remove(num_arc)

        self._arcs[num_arc] = None

    def delete_noeud(self, num_noeud, delete_arcs_associes = False):
        noeud = self.get_noeud(num_noeud)

        if delete_arcs_associes:
            for num_arc in noeud.arcs:
                self.delete_arc(num_arc)

        if len(noeud.arcs) != 0:
            raise ValueError("Il y a des arcs sur ce noeud. Suppression impossible")

        self._noeuds[num_noeud] = None
        
    def creer_arc(self, lPts, noeud1 = None, noeud2 = None, largeur = 10, attributs = {}):
        if noeud1 is None:
            n, d = self.get_noeud_proche(lPts[0])
            noeud1 = n if d == 0 else self.creer_noeud(lPts[0])
        if noeud2 is None:
            n, d = self.get_noeud_proche(lPts[-1])
            noeud2 = n if d == 0 else self.creer_noeud(lPts[-1])
        if not 0 <= noeud1 < len(self._noeuds): raise ValueError
        if not 0 <= noeud2 < len(self._noeuds): raise ValueError
        if None in self._arcs:
            num_arc = self._arcs.index(None)
            self._arcs[num_arc] = _Arc(lPts, noeud1, noeud2, largeur, attributs)
        else:
            self._arcs.append(_Arc(lPts, noeud1, noeud2, largeur, attributs))
            num_arc = len(self._arcs) -1
        self._noeuds[noeud1].arcs.add(num_arc)
        self._noeuds[noeud2].arcs.add(num_arc)
        return num_arc

    def get_noeud(self, num_noeud):
        return self._noeuds[num_noeud]

    def get_num_arcs(self):
        return list(range(len(self._arcs)))

    def get_num_noeuds(self):
        return list(range(len(self._noeuds)))

    def get_arc(self, num_arc):
        return self._arcs[num_arc]

    def get_noeud_proche(self, pt):
        if len(self._noeuds) == 0:
            return (None, float("inf"))
        return min([(_geometry.distance_pts(pt, self._noeuds[i].pt), i)
                    for i in range(len(self._noeuds))
                    ])[::-1]

    def plot(self, arcs = None, style_arcs = {}, style_noeuds = {}, show_axis = True):
        style_noeuds_ = dict(marker = ".", linestyle = "")
        style_noeuds_.update(style_noeuds)
        style_arcs_ = dict(linewidth = 0.5)
        style_arcs_.update(style_arcs)
        
        _plt.plot(*zip(*[noeud.pt for noeud in self._noeuds if noeud is not None]), **style_noeuds_)
        for arc in (self._arcs if arcs is None else [self._arcs[i_arc] for i_arc in arcs]):
            if arc is not None:
                _plt.plot(*zip(*arc.lPts), **style_arcs_)
        _plt.axis("equal")
        if not show_axis:
            _plt.gca().axes.get_xaxis().set_visible(False)
            _plt.gca().axes.get_yaxis().set_visible(False)

    def show(self, arcs = None, style_arcs = {}, style_noeuds = {}, show_axis = True):
        self.plot(arcs, style_arcs, style_noeuds)
        _plt.show()

    def load_from_csv(self, path):
        data = _pd.read_csv(path)
        if not "geom" in data or not "dx" in data or not "dy" in data:
            raise ValueError(f"Field 'geom' not found in csv file '{path}'")
        for iLn, multilinez, dx, dy in zip(data.index, data.geom, data.dx, data.dy):
            if multilinez[-2:] != '))':
                print(iLn+1)
            else:
                for line in _WKT.multiLineStringZ2lineString(multilinez):
                    lPts = _geometry.translateLine(_WKT.lineString2lPts(line), dx, dy)
                    self.creer_arc(_geometry.round_line(lPts))

    def test_integrite(self, analyse = False):
        """
        on dit qu'un graphe est integre ssi:
        · les pointeurs  arcs -> extremités  et  noeud -> arcs sont valides
        · les coordonnées des extremités des arcs correspondent aux coordonnées des noeuds
        """
        # detail des vérifications
        #   Vérification des pointeurs  arcs -> extremités  et  noeud -> arcs
        #       pour chaque arc, les noeuds des extremités contiennent le numéro de l'arc
        #       pour chaque noeuds, les arcs liés ont bien ce noeud pour l'une des extremités
        #   Vérification de la cohérence des coordonnées
        #       les coordonnées des extremités des arcs correspondent aux coordonnées des noeuds
        #   Vérification de la séparation des noeuds

        for num_arc, arc in zip(range(len(self._arcs)), self._arcs):
            if arc is None:
                continue
            for num_noeud in arc.noeuds:
                try:
                    if not num_arc in self.get_noeud(num_noeud).arcs:
                        return False if not analyse else f"arc {num_arc} non enregistré dans noeud {num_noeud}"
                except IndexError:
                    return False if not analyse else f"arc {num_arc} contient le noeud {num_noeud} inconnu"

        for num_noeud, noeud in zip(range(len(self._noeuds)), self._noeuds):
            if noeud is None:
                continue
            for num_arc in noeud.arcs:
                try:
                    if not num_noeud in self.get_arc(num_arc).noeuds:
                        return False if not analyse else f"noeud {num_noeud} non enregistré dans l'arc {num_arc}"
                except IndexError:
                    return False if not analyse else f"noeud {num_noeud} contient l'arc {num_arc} inconnu"

        for num_arc, arc in zip(range(len(self._arcs)), self._arcs):
            if arc is None:
                continue
            num_noeud1, num_noeud2 = arc.noeuds
            if self.get_noeud(num_noeud1).pt != arc.lPts[0]:
                return False if not analyse else f"arc {num_arc}: coordonnées du noeud initial {num_noeud1} différentes du premier point"
            if self.get_noeud(num_noeud2).pt != arc.lPts[-1]:
                return False if not analyse else f"arc {num_arc}: coordonnées du noeud final {num_noeud2} différentes du dernier point"

        for num_noeud1 in range(len(self._noeuds)):
            noeud1 = self.get_noeud(num_noeud1)
            if noeud1 == None:
                continue
            for num_noeud2 in range(num_noeud+1, len(self._noeuds)):
                if noeud1 == self.get_noeud(num_noeud2):
                    return False if not analyse else f"noeuds {num_noeud1} et {num_noeud2} identiques"

        return True if not analyse else "aucune incohérence detecté"

    def round_coords(self, ndigits = 3):
        for noeud in self._noeuds:
            noeud.pt = (round(noeud.pt[0], ndigits), round(noeud.pt[1], ndigits))
        for arc in self._arcs:
            arc.lPts = _geometry.round_line(arc.lPts, ndigits)

    def propagation_gaussienne(self, arcs_deplaces = [], amortissement = 500):
        """
        propage le déplacement des extremites des arcs pour les faire coincider avec les noeuds correspondants

        TODO : gerer quand plusieurs arcs deplacent les memes noeuds: moyenne puis réinterpolation
        """
        # détail de l'algo:
        #   récupération des noeuds correspondants aux arcs
        #   calcul de leur nouvelle coordonnées en prennant les extremites (pour les noeuds communs, moyenne)
        #   mise à jour dans le graphe
        #   déplacement des arcs déplacés pour coller aux noeuds
        #   déplacement des autres arcs

        lPtsIni = []
        lPtsFin = []
        arcs_deplaces = set(arcs_deplaces)
        noeuds_fixes = {}
        for num_arc in arcs_deplaces:
            arc = self.get_arc(num_arc)
            num_noeud1, num_noeud2 = arc.noeuds
            if not num_noeud1 in noeuds_fixes:  noeuds_fixes[num_noeud1] = []
            if not num_noeud2 in noeuds_fixes:  noeuds_fixes[num_noeud2] = []
            noeuds_fixes[num_noeud1].append(arc.lPts[ 0])
            noeuds_fixes[num_noeud2].append(arc.lPts[-1])

        moy = lambda l: sum(l)/len(l) # moyenne d'une liste
        for num_noeud_fixe in noeuds_fixes:
            noeuds_fixes[num_noeud_fixe] = tuple(map(moy, zip(*noeuds_fixes[num_noeud_fixe]))), self.get_noeud(num_noeud_fixe).pt
            self.get_noeud(num_noeud_fixe).pt = noeuds_fixes[num_noeud_fixe][0]

        # propager dans les arcs deplaces (dans le cas ou les noeuds ont bougé)
        for num_arc in arcs_deplaces:
            arc = self.get_arc(num_arc)
            lPtsIni = [arc.lPts[0], arc.lPts[-1]]
            lPtsFin = [noeuds_fixes[arc.noeuds[0]][0], noeuds_fixes[arc.noeuds[1]][0]]
            if lPtsIni != lPtsFin:  # c-a-d: un noeud de cet arc a été déplacé différement par plusieurs arcs
                arc.lPts = list(map(tuple, _propagation.interpolation_gaussienne(arc.lPts, lPtsIni, lPtsFin, amortissement)))

        # propager dans les autres arcs et les autres noeuds
        lPtsIni = [ptIni for ptFin, ptIni in noeuds_fixes.values()]
        lPtsFin = [ptFin for ptFin, ptIni in noeuds_fixes.values()]

        nb_noeuds_non_fixe = len(self._noeuds) - len(noeuds_fixes)
        lPtsAInterpoler = [[noeud.pt if noeud is not None else (0,0) for noeud in self._noeuds], *(arc.lPts if arc is not None else [(0,0)] for arc in self._arcs)]
        lPtsNoeuds, *lPtsArcs = _propagation.interpolation_gaussienne(lPtsAInterpoler, lPtsIni, lPtsFin, amortissement)

        for num_noeud, noeud in zip(range(len(self._noeuds)), self._noeuds):
            if not num_noeud in noeuds_fixes and noeud is not None:
                noeud.pt = tuple(lPtsNoeuds[num_noeud])

        for num_arc, arc in zip(range(len(self._arcs)), self._arcs):
            if not num_arc in arcs_deplaces and arc is not None:
                arc.lPts = list(map(tuple, lPtsArcs[num_arc]))
