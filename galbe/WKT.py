# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier à pour objectif de mettre à disposition des fonctions de       ##
##  manipulation de chaines au format WKT. Pour une utilisation en            ##
##  production, préferer un module publique, qui sera plus complet et plus à  ##
##  jour.                                                                     ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-04-13                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

################################################################################
##  Gestion des erreurs                                                       ##
################################################################################

class LineStringError(Exception):
    pass

################################################################################
##  Conversions                                                               ##
################################################################################

def lineString2lPts(linestring):
    if linestring.split("(")[0].capitalize().strip() != "Linestring":
        raise LineStringError("Invalid LineString")
    return list(map(lambda e:tuple(map(float,e.split())),linestring.split("(")[1].split(")")[0].split(",")))

def lPts2lineString(lPts):
    return "LineString (" + ", ".join(map(lambda e:" ".join(map(str, e)), lPts)) + ")"

def format_linestring(lineString):
    "LineString (1.342E+02 3.432E+00, ...)  -->  LineString (134.2 3.432, ...)"
    if type(lineString) in (tuple, list):
        return list(map(format_linestring, linestring))
    #return lPts2lineString(lineString2lPts(lineString))    # un peu moins optimisé
    if lineString.split("(")[0].capitalize().strip() != "Linestring":
        raise LineStringError("Invalid LineString")
    return "LineString (" + ", ".join(map(lambda e:" ".join(map(lambda v:str(float(v)),e.split())),lineString.split("(")[1].split(")")[0].split(","))) + ")"

def multiLineStringZ2lineStringZ(multiLineStringZ):
    if multiLineStringZ.split("(")[0].capitalize().strip() != "Multilinestringz":
        raise LineStringError("Invalid MultiLineStringZ")
    multiLineStringZ = multiLineStringZ.replace("),(", "), (").replace("  ", " ")   # corrections mineures
    return ["LineStringZ (" + line + ")" for line in multiLineStringZ.split("((")[1].split("))")[0].split("), (")]

def multiLineString2lineString(multiLineString):
    if multiLineString.split("(")[0].capitalize().strip() != "Multilinestring":
        raise LineStringError("Invalid MultiLineString")
    multiLineString = multiLineString.replace("),(", "), (").replace("  ", " ")   # corrections mineures
    return ["LineString (" + line + ")" for line in multiLineString.split("((")[1].split("))")[0].split("), (")]

def lineStringZ2multiLineStringZ(l_lineStringZ):
    if type(l_lineStringZ) == str:
        l_lineStringZ = [l_lineStringZ]
    elif type(l_lineStringZ) not in (list, tuple):
        raise ValueError("Invalid argument for `lineStringZ2multiLineStringZ`")
    for line in l_lineStringZ:
        if line.split("(")[0].capitalize().strip() != "Linestringz":
            raise LineStringError("Invalid LineStringZ")
    return "MultiLineStringZ (" + ", ".join(["("+line.split("(")[1] for line in l_lineStringZ]) + ")"

def lineString2multiLineString(l_lineString):
    if type(l_lineString) == str:
        l_lineString = [l_lineString]
    elif type(l_lineString) not in (list, tuple):
        raise ValueError("Invalid argument for `lineString2multiLineString`")
    for line in l_lineString:
        if line.split("(")[0].capitalize().strip() != "Linestring":
            raise LineStringError("Invalid LineString")
    return "MultiLineString (" + ", ".join(["("+line.split("(")[1] for line in l_lineString]) + ")"

def lineStringZ2lineString(lineStringZ):
    if type(lineStringZ) in (tuple, list):
        return list(map(lineStringZ2lineString, lineStringZ))
    if lineStringZ.split("(")[0].capitalize().strip() != "Linestringz":
        raise LineStringError("Invalid LinesStringZ")
    return "LineString (" + ", ".join(map(lambda pt: " ".join(pt.split()[:2]), lineStringZ.split("(")[1].split(")")[0].split(","))) + ")"

def lineString2lineStringZ(lineString, z = 0):
    if type(lineString) in (tuple, list):
        return list(map(lambda ls, z=z:lineString2lineStringZ(ls, z), lineString))
    if lineString.split("(")[0].capitalize().strip() != "Linestring":
        raise LineStringError("Invalid LinesString")
    return "LineStringZ (" + ",".join([pt + " " + str(z) for pt in lineString.split("(")[1].split(")")[0].split(",")]) + ")"

###########################
##  Fonctions composées  ##
###########################

def lineString2multiLineStringZ(lineString, z = 0):
    return lineStringZ2multiLineStringZ(lineString2lineStringZ(lineString, z))

def lineStringZ2multiLineString(lineStringZ):
    return lineString2multiLineString(lineStringZ2lineString(lineStringZ))

def multiLineStringZ2multiLineString(multiLineStringZ):
    return lineString2multiLineString(lineStringZ2lineString(multiLineStringZ2lineStringZ(multiLineStringZ)))

def multiLineString2multiLineStringZ(multiLineString, z = 0):
    return lineStringZ2multiLineStringZ(lineString2lineStringZ(multiLineString2lineString(multiLineString), z))

def multiLineStringZ2lineString(multiLineStringZ):
    return lineStringZ2lineString(multiLineStringZ2lineStringZ(multiLineStringZ))

def multiLineString2lineStringZ(multiLineString, z = 0):
    return lineString2lineStringZ(multiLineString2lineString(multiLineString), z)

#######################
##  Tests unitaires  ##
#######################

if __name__ == "__main__":
    assert lineString2lPts("LineString (1 1, 2 2)") == [(1,1), (2, 2)]
    assert lPts2lineString([(1,1), (2, 2)]) == "LineString (1 1, 2 2)"
    assert format_linestring("LineString (1.342E+02 3.432E+00)") == "LineString (134.2 3.432)"

    lineString = ["LineString (1 1, 2 2)", "LineString (3 4, 2 5)"]
    multiLineString = "MultiLineString ((1 1, 2 2), (3 4, 2 5))"
    lineStringZ = ["LineStringZ (1 1 0, 2 2 0)", "LineStringZ (3 4 0, 2 5 0)"]
    multiLineStringZ = "MultiLineStringZ ((1 1 0, 2 2 0), (3 4 0, 2 5 0))"
    
    assert multiLineStringZ2lineStringZ("MultiLineStringZ ((1 1 1, 2 2 2), (3 3 3, 4 4 4))") == ["LineStringZ (1 1 1, 2 2 2)", "LineStringZ (3 3 3, 4 4 4)"]
    assert lineStringZ2multiLineStringZ(["LineStringZ (1 1 1, 2 2 2)", "LineStringZ (3 3 3, 4 4 4)"]) == "MultiLineStringZ ((1 1 1, 2 2 2), (3 3 3, 4 4 4))"
    assert lineStringZ2lineString("LineStringZ (1 1 1, 2 2 2)") == "LineString (1 1, 2 2)"
    assert lineString2lineStringZ("LineString (1 1, 2 2)", -1) == "LineStringZ (1 1 -1, 2 2 -1)"
    assert lineString2multiLineString("LineString (1 1, 2 2)") == "MultiLineString ((1 1, 2 2))"
    assert multiLineString2lineString("MultiLineString ((1 1, 2 2))") == ["LineString (1 1, 2 2)"]

    assert lineString2lineStringZ     (lineString, 0) == lineStringZ
    assert lineString2multiLineString (lineString)    == multiLineString
    assert lineString2multiLineStringZ(lineString, 0) == multiLineStringZ

    assert lineStringZ2lineString      (lineStringZ) == lineString
    assert lineStringZ2multiLineString (lineStringZ) == multiLineString
    assert lineStringZ2multiLineStringZ(lineStringZ) == multiLineStringZ

    assert multiLineString2lineStringZ     (multiLineString, 0) == lineStringZ
    assert multiLineString2lineString      (multiLineString)    == lineString
    assert multiLineString2multiLineStringZ(multiLineString, 0) == multiLineStringZ

    assert multiLineStringZ2lineString     (multiLineStringZ) == lineString
    assert multiLineStringZ2multiLineString(multiLineStringZ) == multiLineString
    assert multiLineStringZ2lineStringZ    (multiLineStringZ) == lineStringZ
