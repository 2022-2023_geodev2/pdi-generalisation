# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier à pour objectif de mettre à disposition des fonctions de       ##
##  calcul sur des géométries                                                 ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-04-19                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
CONVENTION UTILISEE:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""

import numpy as _np  # calculs optimisés d'intersection


def distance_pts(p1, p2):
    return ((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)**.5

def distance_droite_rel(p1, p2, p):
    return ((p2[1]-p1[1])*(p2[0]-p[0])-(p2[0]-p1[0])*(p2[1]-p[1]))/distance_pts(p1, p2)

def distance_droite(p1, p2, p):
    return abs(distance_droite_rel(p1, p2, p))

def length_line(lPts):
    l = 0
    for p1, p2 in zip(lPts, lPts[1:]):
        l += distance_pts(p1, p2)
    return l


def proj_in_seg(p1, p2, p):
    d_p2_p = distance_pts(p2, p)
    d_p1_p2 = distance_pts(p1, p2)
    return d_p2_p == 0 or (d_p1_p2 != 0 and 0 <= ((p2[0]-p1[0])*(p2[0]-p[0])+(p2[1]-p1[1])*(p2[1]-p[1]))/d_p1_p2 <= 1)

def distance_seg(p1, p2, p):
    return distance_droite(p1, p2, p) if proj_in_seg(p1, p2, p) else min(distance_pts(p1, p), distance_pts(p2, p))

#inserer en position qui augmente le moins la distance totale de la ligne
def get_pos_to_insert_pt(line, pt, allow_extrem = True):
    if allow_extrem and line != []:
        i_min = 0
        d_min = distance_pts(line[0], pt) # distance ajouté si insertion en début de liste
        d = distance_pts(line[-1], pt) # distance ajouté si insertion en fin de liste
        if d < d_min:
            d_min = d
            i_min = len(line)
    else:
        i_min = None
        d_min = float("inf")
    for i in range(1, len(line)):
        d = distance_pts(line[i-1], pt) + distance_pts(pt, line[i]) - distance_pts(line[i-1], line[i])
        if d < d_min:
            d_min = d
            i_min = i
    return i_min

def translateLine(lPts, dx, dy):
    return [(x+dx, y+dy) for (x,y) in lPts]

def round_line(line, ndigits = 3):
    return [(round(x, ndigits), round(y, ndigits)) for (x,y) in line]

def densification(lPts, dMax):
    lPts = lPts[::] # copie pour ne pas affecter la liste d'origine
    i = 0
    while i < len(lPts)-1:
        d = distance_pts(lPts[i], lPts[i+1])
        n = int(d / dMax) # nombre de points à ajouter
        x1, y1 = lPts[i]
        x2, y2 = lPts[i+1]
        for k in range(n):
            i += 1
            lPts.insert(i, (x1 + (x2-x1) * (k+1)/(n+1),
                            y1 + (y2-y1) * (k+1)/(n+1)))
        i += 1
    return lPts

def intersection_ligne_droite(lPts, droite):
    """
    détermine un point d'intersection entre la droite d'équation ax+by+c=0 et la liste de points

    détermination dichotomique: pour une initialisation correcte, il est important que les extremités de la ligne soient des deux cotés de la droite. Dans le cas contraire, la fonction renverra la valeur None

    lPts: [(x1, y1), ..., (xn, yn)]
    droite: (a, b, c)

    return: (x,y) OR None
    """
    a, b, c = droite
    iMin = 0
    iMax = len(lPts)-1
    eMin = a * lPts[0][0]  + b * lPts[0][1]  + c
    eMax = a * lPts[-1][0] + b * lPts[-1][1] + c
    if iMax < 1: return None
    if eMin * eMax > 0:
        return None
    while iMax - iMin > 1:
        iMoy = (iMin + iMax) // 2
        eMoy = a * lPts[iMoy][0] + b * lPts[iMoy][1] + c
        if eMin * eMoy <= 0:
            iMax, eMax = iMoy, eMoy
        else:
            iMin, eMin = iMoy, eMoy

    x1, y1 = lPts[iMin]
    x2, y2 = lPts[iMax]

    A = _np.array([[a, b,   0  ],
                  [1, 0, x1-x2],
                  [0, 1, y1-y2]])
    B = _np.array([[-c],
                  [x1],
                  [y1]])

    [[x], [y], [t]] = _np.linalg.lstsq(A, B, rcond = None)[0]
    return x, y

def intersection_droites(p1, p2, p3, p4):
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    x4, y4 = p4
    A = _np.array([[x2-x1, x3-x4],[y2-y1, y3-y4]])
    B = _np.array([[x3-x1], [y3-y1]])
    try:
        [[t1], [t2]] = _np.linalg.inv(A) @ B
    except:
        return ((x2+x3)/2, (y2+y3)/2)
    return (x1 + t1 * (x2 - x1), y1 + t1 * (y2 - y1))

def intersection_segments(p1, p2, p3, p4):
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    x4, y4 = p4
    A = _np.array([[x2-x1, x3-x4],[y2-y1, y3-y4]])
    B = _np.array([[x3-x1], [y3-y1]])
    try:
        [[t1], [t2]] = _np.linalg.inv(A) @ B
    except:
        return None
    if 0 <= t1 <= 1 and 0 <= t2 <= 1:
        return (x1 + t1 * (x2 - x1), y1 + t1 * (y2 - y1))
    return None
