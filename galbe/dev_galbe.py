# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier à pour objectif de mettre à disposition la version développée  ##
##  en python de l'algorithme galbe                                           ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-05-05                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
CONVENTION UTILISEE:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""

import bindage as _bindage
import propagation as _propagation
import graphe as _GR
import WKT as _WKT
import geometry as _geometry
from faille_min import faille_min as _faille_min 

def galbe(graphe, num_arc, parametres, resolution, ignore_error = False):
    arc = graphe.get_arc(num_arc)
    largeur = arc.largeur

    if len(arc.lPts) <= 3 or largeur <= 0:
        return

    try:
        conflit = _bindage.indicateur_conflits(
            _WKT.lPts2lineString(arc.lPts),
            resolution,
            largeur / 2,# * parametres[2],
            1.7)
    except:
        if not ignore_error: raise
        else: conflit = (0,0)
    
    if conflit == (0, 0):   # pas de conflit sur l'arc: juste lissage
        line = _WKT.lPts2lineString(arc.lPts)
        try:
            if parametres[1]:
                line = _bindage.filtre_gaussien(
                    line,
                    resolution,
                    parametres[1])
            if parametres[9]:
                line = _bindage.douglas_peucker(
                    line,
                    resolution,
                    parametres[9])
        except:
            if not ignore_error: raise

        arc.lPts = _WKT.lineString2lPts(line)
        
        graphe.propagation_gaussienne(
            arcs_deplaces = [num_arc],
            amortissement = parametres[6])
        return

    # création d'un graphe intermédiaire
    gr_interm = _GR.Graphe()
    num_arc_interm = gr_interm.creer_arc(arc.lPts)

    try:
        pts_decoupe = _bindage.decoupage_conflits_pts(
            _WKT.lPts2lineString(arc.lPts),
            resolution,
            largeur / 2,# * parametres[2],
            parametres[0],
            1.7)
    except:
        if not ignore_error: raise
        else: return

    for pt in pts_decoupe:
        num_arc1, num_arc_interm, num_noeud = gr_interm.creer_noeud_sur_arc(num_arc_interm, pt)

    #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    l_num_arcs_liss = []
    l_num_arcs_fMax = []
    l_num_arcs_fMin = []
    l_num_arcs_acco = []
    
    for num_arc_i in gr_interm.get_num_arcs():
        arc = gr_interm.get_arc(num_arc_i)

        # filtrage des points doubles
        arc.lPts = _bindage.filtrage(arc.lPts)

        # analyse du conflit
        try:
            conflit = _bindage.indicateur_conflits(
                _WKT.lPts2lineString(arc.lPts),
                resolution,
                largeur / 2,# * parametres[2],
                1.7)
        except:
            if not ignore_error: raise
            else: conflit = (0,0)

        if conflit == (0, 0):
            l_num_arcs_liss.append(num_arc_i)
        elif conflit[0] != 0 and conflit[1] != 0:
            l_num_arcs_acco.append(num_arc_i)
        else:
            l_num_arcs_fMax.append(num_arc_i)

    ##  ---------------  Traitement des arcs par accordéon  ----------------  ##
    for num_arc_accord in l_num_arcs_acco:
        arc = gr_interm.get_arc(num_arc_accord)
        if arc.lPts[0] == arc.lPts[-1]:
            continue

        for i_schemat in range(int(parametres[8])):
            try:
                lPts_accord = _WKT.lineString2lPts(_bindage.accordeon(
                    _WKT.lPts2lineString(arc.lPts),
                    int(largeur * parametres[3]),
                    resolution))
            except:
                if not ignore_error: raise
                else: lPts_accord = arc.lPts

            lPts_accord_filtre = _bindage.filtrage(lPts_accord)
            line_accord_filtre = _WKT.lPts2lineString(lPts_accord_filtre)

            try:
                if (_bindage.nb_intersections(line_accord_filtre, resolution)
                    or max(_geometry.distance_pts(arc.lPts[0], lPts_accord_filtre[0]),
                           _geometry.distance_pts(arc.lPts[-1], lPts_accord_filtre[-1])) > parametres[7] * largeur):

                    try:
                        line_sch = _bindage.schematisation(
                            _WKT.lPts2lineString(arc.lPts),
                            resolution)
                    except:
                        if not ignore_error: raise
                    else:
                        arc.lPts = _WKT.lineString2lPts(line_sch)
                else:
                    arc.lPts = _WKT.lineString2lPts(line_accord_filtre)
                    break
            except:
                if not ignore_error: raise

        try:
            # _propagation
            arc.lPts = _bindage.filtrage(arc.lPts)
            gr_interm.propagation_gaussienne(
                arcs_deplaces = [num_arc_accord],
                amortissement = parametres[6])

            # découpage pour seconde passe
            pts_decoupe = _bindage.decoupage_conflits_pts(
                _WKT.lPts2lineString(arc.lPts),
                resolution,
                largeur / 2 * 0.8,# * parametres[2],
                parametres[4],
                1.7)
        except:
            if not ignore_error: raise
            else: pts_decoupe = []

        num_arc_decoupe = num_arc_accord
        for pt in pts_decoupe:
            _, num_arc_decoupe, _ = gr_interm.creer_noeud_sur_arc(num_arc_decoupe, pt)
        #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

        for num_arc_i in gr_interm.get_num_arcs():
            arc_decoupe = gr_interm.get_arc(num_arc_i)

            # filtrage des points doubles
            try:
                arc_decoupe.lPts = _bindage.filtrage(arc_decoupe.lPts)
            except:
                if not ignore_error: raise

            # analyse du conflit
            try:
                conflit = _bindage.indicateur_conflits(
                    _WKT.lPts2lineString(arc_decoupe.lPts),
                    resolution,
                    largeur / 2 * 0.8,# * parametres[2],
                    1.7)
            except:
                if not ignore_error: raise
                else: conflit = (0,0)

            if conflit == (0, 0):
                l_num_arcs_liss.append(num_arc_i)
            elif conflit[0] != 0 and conflit[1] != 0:
                pass
            else:
                l_num_arcs_fMin.append(num_arc_i)       # conflit simple

        #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    ##  ---------------  Traitement des arcs par faille max  ---------------  ##
    if parametres[2]:
        for num_arc_fmax in l_num_arcs_fMax:
            arc = gr_interm.get_arc(num_arc_fmax)

            try:
                arc.lPts = _bindage.filtrage(arc.lPts)
                arc.lPts = _WKT.lineString2lPts(_bindage.faille_max(
                    _WKT.lPts2lineString(arc.lPts),
                    largeur / 2 * parametres[2],
                    resolution))
                arc.lPts = _bindage.filtrage(arc.lPts)
            except:
                if not ignore_error: raise

            gr_interm.propagation_gaussienne(
                arcs_deplaces = [num_arc_fmax],
                amortissement = parametres[6])

            #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    ##  ---------------  Traitement des arcs par faille min  ---------------  ##
    if parametres[5]:
        for num_arc_fmin in l_num_arcs_fMin:
            arc = gr_interm.get_arc(num_arc_fmin)

            try:
                arc.lPts = _bindage.filtrage(arc.lPts)
                arc.lPts = _faille_min(
                    arc.lPts,
                    largeur / 2 * parametres[5],
                    resolution)
                arc.lPts = _bindage.filtrage(arc.lPts)
            except:
                if not ignore_error: raise


            gr_interm.propagation_gaussienne(
                arcs_deplaces = [num_arc_fmin],
                amortissement = parametres[6])

            #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    ##  ----------------  Traitement des arcs par lissage  -----------------  ##
    if parametres[1]:
        for num_arc_liss in l_num_arcs_liss:
            arc = gr_interm.get_arc(num_arc_liss)

            try:
                arc.lPts = _bindage.filtrage(arc.lPts)
                arc.lPts = _WKT.lineString2lPts(_bindage.filtre_gaussien(
                    _WKT.lPts2lineString(arc.lPts),
                    resolution,
                    parametres[1]))
                arc.lPts = _bindage.filtrage(arc.lPts)
            except:
                if not ignore_error: raise

            gr_interm.propagation_gaussienne(
                arcs_deplaces = [num_arc_liss],
                amortissement = parametres[6])

    #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    for num_noeud in gr_interm.get_num_noeuds():
        if len(gr_interm.get_noeud(num_noeud).arcs) == 2:
            num_arc_i = gr_interm.fusion_noeud(num_noeud)

    #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    arc = gr_interm.get_arc(num_arc_i)
    line = _WKT.lPts2lineString(arc.lPts)
    try:
        line = _bindage.filtre_gaussien(line, resolution, parametres[10])
        line = _bindage.douglas_peucker(line, resolution, parametres[9])
    except:
        if not ignore_error: raise

    arc.lPts = _WKT.lineString2lPts(line)
    gr_interm.propagation_gaussienne(
        arcs_deplaces = [num_arc_i],
        amortissement = parametres[6])

    #assert gr_interm.test_integrite(), gr_interm.test_integrite(True)

    arc_gr = graphe.get_arc(num_arc)
    arc_gr.lPts = arc.lPts
    graphe.propagation_gaussienne(
        arcs_deplaces = [num_arc],
        amortissement = parametres[6])

    #assert graphe.test_integrite(), graphe.test_integrite(True)

