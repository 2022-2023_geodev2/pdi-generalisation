# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier fourni une alternative aux fonctoins de propagation de         ##
##  déplacement dans un graphe utilisées en ADA.                              ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-04-05                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
CONVENTION UTILISEE:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""

import numpy as _np

def _distances_2_a_2(lPts1, lPts2):
    n = len(lPts1)
    m = len(lPts2)
    
    x1 = _np.zeros((m, n))
    x1[:, :] = lPts1[:, 0]
    x2 = _np.zeros((n, m))
    x2[:, :] = lPts2[:, 0]
    dx = x2.T - x1

    y1 = _np.zeros((m, n))
    y1[:, :] = lPts1[:, 1]
    y2 = _np.zeros((n, m))
    y2[:, :] = lPts2[:, 1]
    dy = y2.T - y1

    dists = (dx**2 + dy**2) **.5
    return dists


def interpolation_gaussienne(lPts, lPtsIni, lPtsFin, amortissement = 500):
    lPtsIni = _np.array(lPtsIni)
    lPtsFin = _np.array(lPtsFin)
    n = len(lPtsIni)

    # gaussienne (fonction utilisée pour associer un poids à chaque distance)
    def f(dist, sigma):
        return _np.exp(-(dist/sigma)**2)

    # détermination des paramètres pour l'interpolation
    
    dists = _distances_2_a_2(lPtsIni, lPtsIni)
    A = f(dists, amortissement)
    A_ = _np.linalg.inv(A)

    lVect = lPtsIni - lPtsFin
    
    X = A_ @ lVect[:, 0:1]
    Y = A_ @ lVect[:, 1:2]

    # application de l'interpolation aux points
    try:
        int(lPts[0][0])
    except TypeError:  # type non scalaire (liste, ...)
        multiList = True
    else:
        lPts = [lPts]
        multiList = False

    lPtsOut = []
    for lPt in lPts:
        lPt = _np.array(lPt)
        m = len(lPt)

        dists = _distances_2_a_2(lPtsIni, lPt)
        A = f(dists, amortissement)

        lPtsOut.append(_np.zeros((m, 2)))
        lPtsOut[-1][:, 0:1] = lPt[:, 0:1] - A @ X
        lPtsOut[-1][:, 1:2] = lPt[:, 1:2] - A @ Y

    if multiList:
        return lPtsOut
    else:
        return lPtsOut[0]
