# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier à pour objectif de mettre à disposition les principales        ##
##  fonctions des algorithmes GALBE développés en ADA, ainsi que quelques     ##
##  fonctions de lissage et filtrage.                                         ##
##                                                                            ##
##  Ce module s'appuie sur une librairie en C générée avec le projet galbe_c  ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-03-29                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
On utilisera la convention python suivante:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""

################################################################################
##  INITIALISATION de la librairie C                                          ##
################################################################################

import ctypes as _ctypes
import sys as _sys, os as _os, glob as _glob
import WKT as _WKT
import geometry as _geometry

# recherche du path menant à la librairie
for _path in _sys.path:
    if _glob.glob(f"{_path}/lib/libgalbe.*") != []:
        break

if _sys.platform == "linux":
    _galbe = _ctypes.CDLL(_path + "/lib/libgalbe.so")
elif _sys.platform == "darwin":
    _galbe = _ctypes.CDLL(_path + "/lib/libgalbe.dylib")
elif _sys.platform == "win32":
    _galbe = _ctypes.CDLL(_path + "/lib/libgalbe.dll")
else:
    print(f"os inconnu: {_sys.platform}")
    raise OSError;

_galbe.galbeinit.restype = None
_galbe.galbeinit()

################################################################################
##  Gestion des erreurs                                                       ##
################################################################################

class AdaException(Exception):
    pass

def _get_ada_exception(text):
    # analyse le texte et revoie l'exception correspondante
    if text[:7] != "raised ":
        raise ValueError(f"Invalid error: {text}")
    name_error, *msg = text[7:].split(" : ")
    msg = " : ".join(msg)
    name_error = name_error.lower()
    if name_error == "wkt_io.wkt_linestring_error":
        return _WKT.LineStringError(msg)
    elif name_error == "galbe_c.value_error":
        return ValueError(msg)
    else:
        return AdaException(text)


################################################################################
##  Encapsulation des fonctions de la librairie                               ##
################################################################################

_galbe.douglas_peucker.restype = _ctypes.c_char_p
def douglas_peucker(lineString, resolution, seuil):
    result = _galbe.douglas_peucker(
        lineString.encode(),
        resolution,
        str(seuil).encode()
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)


_galbe.filtre_gaussien.restype = _ctypes.c_char_p
def filtre_gaussien(lineString, resolution, sig):
    result = _galbe.filtre_gaussien(
        lineString.encode(),
        resolution,
        str(sig).encode()
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)

"""
_galbe.faille_min.restype = _ctypes.c_char_p
def faille_min(lineString, largeur, courbure, resolution):
    result = _galbe.faille_min(
        lineString.encode(),
        str(largeur).encode(),
        str(courbure).encode(),
        resolution
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)
"""

_galbe.faille_max.restype = _ctypes.c_char_p
def faille_max(lineString, largeur, resolution):
    result = _galbe.faille_max(
        lineString.encode(),
        str(largeur).encode(),
        resolution
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)

_galbe.accordeon.restype = _ctypes.c_char_p
def accordeon(lineString, largeur, resolution, sigma = None):
    result = _galbe.accordeon(
        lineString.encode(),
        str(largeur).encode(),
        resolution,
        b"1" if sigma is None else b"0",
        b"20.0" if sigma is None else str(sigma).encode()
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)

_galbe.schematisation.restype = _ctypes.c_char_p
def schematisation(lineString, resolution, sigma = None, virages = None):
    result = _galbe.schematisation(
        lineString.encode(),
        resolution,
        b"1" if sigma is None else b"0",
        b"20.0" if sigma is None else str(sigma).encode(),
        b"1" if virages is None else b"0",
        0 if virages is None else virages[0],
        0 if virages is None else virages[1]
        ).decode()
    try:
        return _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    raise _get_ada_exception(result)


_galbe.decoupage_conflits.restype = _ctypes.c_char_p
def decoupage_conflits_pts(lineString, resolution, largeur, separabilite = 1.7, tolerence = 1.7):
    """
        découpe une ligne en portions de conflit uniforme
        input : LineString
        output: lPts: points de découpage
    """
    result = _galbe.decoupage_conflits(
        lineString.encode(),
        resolution,
        str(largeur).encode(),
        str(separabilite).encode(),
        str(tolerence).encode(),
        ).decode()
    try:
        decoupage = _WKT.format_linestring(result)
    except _WKT.LineStringError:
        pass
    else:
        l_pts_dec = _WKT.lineString2lPts(decoupage)[1:-1]
        return l_pts_dec
    raise _get_ada_exception(result)

def decoupage_conflits(lineString, resolution, largeur, separabilite = 1.7, tolerence = 1.7):
    """
        découpe une ligne en portions de conflit uniforme
        input : LineString
        output: MultiLineString
    """
    l_pts_dec = decoupage_conflits_pts(lineString, resolution, largeur, separabilite, tolerence)
    if l_pts_dec == []:
        return _WKT.lineString2multiLineString(lineString)
    l_pts = _WKT.lineString2lPts(lineString)
    lines = []
    for pt_dec in l_pts_dec:
        pos = _geometry.get_pos_to_insert_pt(l_pts, pt_dec, False)
        lines.append(_WKT.lPts2lineString(l_pts[0:pos] + [pt_dec]))
        l_pts = [pt_dec] + l_pts[pos:]
    lines.append(_WKT.lPts2lineString(l_pts))
    return _WKT.lineString2multiLineString(lines)


_galbe.indicateur_conflits.restype = _ctypes.c_char_p
def indicateur_conflits(lineString, resolution, largeur, tolerence = 1.7):
    """
        Indique l'empatement sur la ligne. Un empatement nul signifie l'absence de conflit de ce coté.
        input : LineString
        output: (empatement_gauche, empatement_droite)
    """
    result = _galbe.indicateur_conflits(
        lineString.encode(),
        resolution,
        str(largeur).encode(),
        str(tolerence).encode(),
        ).decode()
    try:
        gauche, droite = result.split()
        return (float(gauche), float(droite))
    except ValueError:
        pass
    raise _get_ada_exception(result)


_galbe.nb_intersections.restype = _ctypes.c_char_p
def nb_intersections(lineString, resolution):
    """
        Deonne le nombre d'intersection de la ligne avec elle meme
        input : LineString
        output: int
    """
    result = _galbe.nb_intersections(
        lineString.encode(),
        resolution).decode()
    try:
        return int(result)
    except ValueError:
        pass
    raise _get_ada_exception(result)


def filtrage(lineString):
    """
        Supprime les doublons (ie: lorsque deux points successifs de la ligne
        sont égaux)
        input : LineString (ou lPts)
        output: LineString (ou lPts)
    """
    # cette fonction se fait facilement en python, passer par un bindage sera
    # plus long (en temps de développement et d'execution)
    # ps: equivalent à Douglas_Peucker avec un seuil de 0
    l_pts = _WKT.lineString2lPts(lineString) if type(lineString) != list else lineString
    l_pts = [l_pts[i]
             for i in range(len(l_pts))
             if i == len(l_pts)-1 or l_pts[i] != l_pts[i+1]]
    return _WKT.lPts2lineString(l_pts) if type(lineString) != list else l_pts


def buffer(lineString, distance, resolution):
    """
        Réalise un buffer autours de la ligne donnée

        input : LineString (ou lPts)
        output: LineString (ou lPts)
    """
    lPts = lineString if type(lineString) == list else _WKT.lineString2lPts(lineString)
    buf = _WKT.lineString2lPts(faille_max(
        _WKT.lPts2lineString([lPts[1]] + lPts + lPts[::-1] + [lPts[1]]),
        distance,
        resolution))
    buf = buf[:len(buf)-buf[::-1].index(buf[0])]
    return buf if type(lineString) == list else _WKT.lPts2lineString(buf)


################################################################################
##  CLOTURE de la librairie C                                                 ##
################################################################################

_galbe.galbefinal.restype = None
_galbe.galbefinal()
