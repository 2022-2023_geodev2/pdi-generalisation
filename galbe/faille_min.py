# -*- coding: utf-8 -*-
################################################################################
##                                                                            ##
##  Ce fichier contient la réécriture en python de la fonction faille_min     ##
##  Note: cette version de faille_min est un peu simplifié par rapport à la   ##
##  fonction originelle. Le résultat pourra etre légèrement altéré dans       ##
##  certains cas (cf rapport)                                                 ##
##                                                                            ##
##  Auteur: Giudicelli Vincent                                                ##
##  Email:  vincent.giudicelli@ensg.eu                                        ##
##  Date de création: 2023-05-10                                              ##
##  Dernière modification: 2023-05-11 par V. Giudicelli                       ##
##                                                                            ##
################################################################################

"""
CONVENTION UTILISEE:
les noms de fonctions et variables commencant par le caractère `_` sont privés
et ne doivent être utilisés que dans l'environnement local dans lequel il est
défini.
"""


"""
Description de l'algoritme faille_min:

On dispose d'une route présentant un empattement d'un seul coté, sur toute sa
longeur. L'algorithme va elargir le virage au minimum, de sorte qu'il n'y ait
plus d'empattement.

Le calcul se passe en deux étapes:
1) calcul de la ligne 'médiane' du virage: la ligne en tous points equidistante
   des deux cotés à la route. Ce calcul se fait par triangulation.
2) calcul d'un buffer 'ouvert' autours de cette ligne (on profite de la fonction
   faille_max)
"""
import WKT as _WKT
import geometry as _geometry
import math as _math
import bindage as _bindage

def _triangulation(lPts):
    triangles = []
    i = 0
    j = len(lPts)-1
    while i+1 != j:
        if _geometry.distance_pts(lPts[i], lPts[j-1]) > _geometry.distance_pts(lPts[i+1], lPts[j]):
            triangles.append((lPts[i], lPts[j], lPts[i+1]))
            i += 1
        else:
            triangles.append((lPts[i], lPts[j], lPts[j-1]))
            j -= 1
    return triangles

def _creer_arc(p1, p2, centre, n = 10, r = None):
    a1 = _math.atan2(p1[1]-centre[1], p1[0]-centre[0])
    a2 = _math.atan2(p2[1]-centre[1], p2[0]-centre[0])
    if a1 == a2:
        return [p1, p2]
    if a1 > a2:
        a2 += 2*math.pi
    if r is None:
        r = _geometry.distance_pts(centre, p1)
    nb_pts = max(1,int(2*n*(a2-a1)/math.pi))
    arc = [(centre[0] + r*_math.cos(a1 + k/nb_pts*(a2-a1)), centre[1] + r*_math.sin(a1 + k/nb_pts*(a2-a1)))
            for k in range(nb_pts+1)]
    return arc

def _filtre_line(lPts, filtre_double_pts = True, filtre_pts_aligne = True, ndigits = None):
    lPts = lPts[:]
    if ndigits is not None:
        lPts = [(round(x, ndigits), round(y, ndigits)) for (x,y) in lPts]
    if filtre_double_pts:
        i = 0
        while i < len(lPts)-1:
            if lPts[i] == lPts[i+1]:
                lPts.pop(i)
            else:
                i += 1
    if filtre_pts_aligne:
        i = 0
        while i < len(lPts)-2:
            (x1, y1), (x2, y2), (x3, y3) = lPts[i:i+3]
            v1 = (x2-x1, y2-y1)
            v2 = (x3-x2, y3-y2)
            if (x2-x1)*(y3-y2) == (y2-y1)*(x3-x2) and (x2-x1)*(x3-x2) + (y2-y1)*(y3-y2) >= 0:
                lPts.pop(i+1)
            else:
                i += 1
    return lPts

def faille_min(lineString, distance, resolution = 100):
    ##  Calcul de la médiane  ##################################################
    # densification de la ligne pour une médiane plus densément définie
    lPts = _WKT.lineString2lPts(lineString) if type(lineString) == str else lineString
        
    lPts_dens = _geometry.densification(lPts, distance/10)
    tri = _triangulation(lPts_dens)
    mediane = [((x1+x2)/2, (y1+y2)/2) for ((x1, y1), (x2, y2), _) in tri]
    mediane = _filtre_line(mediane, filtre_pts_aligne = False)
    if len(mediane) <= 1:   # ligne trop courte: on n'a pas pu déterminer de direction
        return lPts
    extrem = tri[-1][-1]
    while len(mediane) > 2 and _geometry.distance_pts(mediane[-1], extrem) < distance:
        mediane.pop()

    mediane_filtre = _filtre_line(mediane)
    if len(mediane_filtre) == 1:
        mediane_filtre = mediane[:2]    # on a besoin d'au moins deux points pour avoir une orientation

    ##  Calcul du buffer  ######################################################
    buffer = _WKT.lineString2lPts(_bindage.faille_max(
        _WKT.lPts2lineString(mediane_filtre + mediane_filtre[::-1]),
        distance,
        resolution))

    # remetre le buffer dans le meme sens que la ligne de départ
    if (_geometry.distance_pts(lPts[0], buffer[0]) + _geometry.distance_pts(lPts[-1], buffer[-1])
        > _geometry.distance_pts(lPts[0], buffer[-1]) + _geometry.distance_pts(lPts[-1], buffer[0])):
        buffer = buffer[::-1]
    return buffer if type(lineString) == list else _WKT.lPts2lineString(buffer)
