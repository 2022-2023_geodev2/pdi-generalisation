# Code pour faire tourner l'outil arcGIS de Douglas-Peucker
# Changer la BDD dans le main
# Pour faire marcher cette outil il faut suivre le guide d'installation des outils PDI
# notamment pour installer les librairies ADA et python

import sys, os
sys.path.insert(1, os.path.abspath("../dev_galbe"))
import galbe
WKT = galbe.WKT
import arcpy


def generalisation():
    #Paramètre entrés par l'utilisateur
    Couche_source = arcpy.GetParameterAsText(0)
    Seuil_DP = arcpy.GetParameterAsText(1)

    # Couche de sortie
    couche_sortie = (Couche_source.split("\\")[-1])[:-4] + "_sortieDP_PDI" + ".shp"

    workspace = r".\Socle_carto.gdb"
    arcpy.AddMessage("\nworkspace: " + workspace + "\n")
    arcpy.AddMessage("\nDébut du traitement\n")
    arcpy.AddMessage("\nCouche_source: " + Couche_source + "\n")
    arcpy.AddMessage("\nSeuil_DP: " + Seuil_DP + "\n")

    # On remplit le tableau ligne de la géométrie en entrée.
    ligne = []
    for row in arcpy.da.SearchCursor(Couche_source, ["SHAPE@WKT"]):
        route = "LineString (" + ", ".join(
            (map(lambda e: " ".join(e.split()[:2]), row[0].split("((")[1].split("))")[0].split(",")))) + ")"
        ligne.append(route)

    sr = arcpy.SpatialReference(2154)
    liste_ligne_f = []


    # On crée une nouvelle ligne à partir de la variable "ligne" en on lui applique DP avec galbe
    # Pour la compatibilité des formats on passe par des WKT via la fonction lineString2multiLineString
    for i in range(0, len(ligne)):
        ligne_f = galbe.douglas_peucker(ligne[i], 1, 100)
        ligne_f = WKT.lineString2multiLineString(ligne_f)
        ligne_f = arcpy.FromWKT(ligne_f, sr)
        liste_ligne_f.append(ligne_f)

    # enregistrement vers le répertoire de travail de la couche nouvellement crée
    chemin_arrive = "\\".join(Couche_source.split("\\")[:-1]) + "\\" + couche_sortie
    arcpy.AddMessage("\n Enregistrement vers: \n")
    arcpy.AddMessage(chemin_arrive)
    arcpy.CopyFeatures_management(liste_ligne_f, chemin_arrive)



if __name__ == '__main__':
    # Global Environment settings
    # Changer le gdb en fonction du projet.
    # le faire également ligne 21 dans la fonction generalisation
    with arcpy.EnvManager(
            scratchWorkspace=r".\Socle_carto.gdb",
            workspace=r".\Socle_carto.gdb",
            overwriteOutput=True
    ):
        generalisation()

        # Update derived parameter values using arcpy.SetParameter() or arcpy.SetParameterAsText()


