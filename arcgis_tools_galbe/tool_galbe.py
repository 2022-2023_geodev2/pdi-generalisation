#______________________Projet_developpement_____retour_vers_le_futur_cartographique
#autrice:Chapeau_Marion


# Code pour faire tourner l'outil arcGIS de galbe
# Pour faire marcher cette outil il faut suivre le guide d'installation des outils PDI


import arcpy
import sys
import string
import galbe
import os
import fonction_formatage_donnee_couche as symbo

def generalisation():
    
    #Paramètre entrées par l'utilisateur
    Couche_source = arcpy.GetParameterAsText(0)
    nom_projet = arcpy.GetParameterAsText(1)
    nom_carte = arcpy.GetParameterAsText(2)
    Separabilite_premier_decoupage = arcpy.GetParameterAsText(3)
    Sigma_lissage = arcpy.GetParameterAsText(4)
    Exageration_faille_max = arcpy.GetParameterAsText(5)
    Exageration_accordeon = arcpy.GetParameterAsText(6)
    Separabilite_deuxieme_decoupage = arcpy.GetParameterAsText(7)
    Exageration_faille_min = arcpy.GetParameterAsText(8)
    Amortissement_deplacement = arcpy.GetParameterAsText(9)
    Critere_maxi_accordeon = arcpy.GetParameterAsText(10)
    Nombre_maxi_schematisation = arcpy.GetParameterAsText(11)
    Seuil_DP = arcpy.GetParameterAsText(12)
    Sigma_Gauss = arcpy.GetParameterAsText(13)
    echelle = arcpy.GetParameterAsText(14)
    path = arcpy.GetParameterAsText(15)

    liste_paramettre= [Separabilite_premier_decoupage, Sigma_lissage,Exageration_faille_max, Exageration_accordeon,
                       Separabilite_deuxieme_decoupage, Exageration_faille_min, Amortissement_deplacement, Critere_maxi_accordeon,
                       Nombre_maxi_schematisation, Seuil_DP, Sigma_Gauss]
    liste_paramettre = list(map(float,liste_paramettre))
    
    # Couche de sortie
    couche_sortie = (Couche_source.split("\\")[-1])[:-4] + "_sortie_galbe" + ".shp"

    nom_couche= Couche_source.split("\\")[-1]

    
    arcpy.AddMessage("\nDébut du traitement\n")
    arcpy.AddMessage(Couche_source)

    #on recupere la symbologie de la couche et les linestring des routes 
    s = symbo.symbologie(str(nom_projet),str(nom_carte),nom_couche[:-4],float(echelle))

    info_couche = symbo.info_couche(path,Couche_source)
    table_symbo = symbo.info_couche_avec_symbo(info_couche, s)

    arcpy.AddMessage("\export_couche_fait\n")

    #creation des graphes
    graphe = galbe.GR.Graphe()
    for i in range (0,len(table_symbo)):
        #convertion des lignestrings en liste de points
        liste_point = galbe.WKT.lineString2lPts(table_symbo[i][3])
        graphe.creer_arc(liste_point, largeur = int(table_symbo[i][2]))
    arcpy.AddMessage("graphe fait\n")
        
    for num_arc in graphe.get_num_arcs():
        try :
            galbe.galbe(graphe,num_arc,liste_paramettre,100)
        except:
            arcpy.AddMessage(f"galbe : {num_arc}/{len(graphe.get_num_arcs())} erreur")
        else:
            arcpy.AddMessage(f"galbe : {num_arc}/{len(graphe.get_num_arcs())}")
            graphe.round_coords(2)
    arcpy.AddMessage("galbe_1")
    ligne_f = []
    sr = arcpy.SpatialReference(2154)
    ligne_ =[galbe.WKT.lPts2lineString(graphe.get_arc(num_arc).lPts) for num_arc in graphe.get_num_arcs()]
    arcpy.AddMessage("galbe_2")
    ligne_ = list(map(galbe.WKT.lineString2multiLineString,ligne_))
    arcpy.AddMessage("galbe_3")
   
    
    for i in range(len(ligne_)):
        ligne_f.append(arcpy.FromWKT(ligne_[i], sr))
          
    arcpy.AddMessage("galbe execute\n")

    # enregistrement vers le répertoire de travail de la couche nouvellement crée
    chemin_arrive = "\\".join(Couche_source.split("\\")[:-1]) + "\\" + couche_sortie
    arcpy.AddMessage("\n Enregistrement vers: \n")
    arcpy.AddMessage(chemin_arrive)
    arcpy.CopyFeatures_management(ligne_f, chemin_arrive)



if __name__ == '__main__':
    # Global Environment settings
    # Changer le gdb en fonction du projet.
    # le faire également ligne 21 dans la fonction generalisation
    with arcpy.EnvManager(
            scratchWorkspace=r".\Socle_carto.gdb",
            workspace=r".\Socle_carto.gdb",
            overwriteOutput=True
    ):
        generalisation()





