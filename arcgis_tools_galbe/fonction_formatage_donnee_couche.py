#_________________FONCTIONs_IMPORT_DONNEES_COUCHE_________________
#Realise_par_Chapeau_Marion
#Projet_developpement_ensg_2023


import arcpy
import galbe
import WKT
import arcpy, os, sys


def symbologie(nom_projet,nom_carte,nom_couche,echelle):
    
    #nom_projet: chemin du projet arcgis type:str
    #nom_carte: chemin de la carte arcgis type:str
    #nom_couche: chemin de la couche dont on veut recuperer la symbologie type:str
    #symbo: liste de couple nom de la symbologie et largeur correspondante en m type:list
    #Attention: Largeur calculer pour une échelle 25k
    
    relpath = os.path.dirname(sys.argv[0])
    p = arcpy.mp.ArcGISProject(os.path.join(relpath,nom_projet))
    m = p.listMaps(nom_carte)[0]
    lyr = m.listLayers(nom_couche)[0]
    sym1 = lyr.symbology
    symbo=[]
    for grp in sym1.renderer.groups:
        for itm in grp.items:
            label = itm.label
            #replacement des accents et des espaces 
            label = label.replace('é','e')
            label = label.upper()
            label = label.replace(' ','_')
            #changement de certains noms  de symbologie pour des abreviations car des abreviations sont faites dans les couches
            label = label.replace('ROUTE_NON_REVÊTUE_CARROSSABLE','NON_REVETUE_CARRO')
            label = label.replace('NON_CLASSEE_AVEC_ACCÈS_RESTREINT','NON_CLASSEE_RESTREINT')
            #ajout des deux attributs nom et largeur(conversion de point en m)
            symbo.append([label,itm.symbol.size* 0.000352778*echelle])
    return symbo

    
def info_couche (path,couche_initiale):
    
    #path: chemin de l'invironnement de travail type: str
    #couche_initiale: chemin de la couche contenants les données a generalise type: str
    #info_couche: liste de triplets identifiant(type:int),
    #symbo qui correspond au nom de la symbo utlise sur la ligne (type: str),
    #ligne(type :linesString) type: list
    
    mypath = path
    arcpy.env.workspace = mypath
    ligne = []
    identifiant = []
    symbo = []
    info_couche = []
    #conversion de shape a WKT avec Multilinesstring convertie en linestring
    for row in arcpy.da.SearchCursor(couche_initiale, ["SHAPE@WKT"]):
        route ="LineString (" + ", ".join((map(lambda e: " ".join(e.split()[:2]), row[0].split("((")[1].split("))")[0].split(",")))) + ")"
        ligne.append(route)
    #recuperation des attributs id et nom des symbo de la couche
    with arcpy.da.SearchCursor(couche_initiale, ['OID@', "SYMBO"]) as cursor :
        for row in cursor:
            identifiant.append(row[0])
            symbo.append(row[1])

    for i in range (len(ligne)):
        info_couche.append([identifiant[i], symbo[i], ligne[i]])
    return info_couche

def info_couche_avec_symbo(info_couche, symbo):
    
    #info_couche:liste de triplet [id,nom symbologie, ligne] type:list
    #symbo:liste de couple nom symbologie et largeur symbologie d'un couche
    #table_couche_finale: liste de 4-uplets [id,nom symbologie,largeur_symbologie,ligne] type:liste

    table_couche_finale = []
    for i in range (0,len(info_couche)):
        for j in range (0,len(symbo)):
            if info_couche[i][1]==symbo[j][0]:
                table_couche_finale.append([info_couche[i][0],info_couche[i][1],symbo[j][1],info_couche[i][2]])
    return table_couche_finale
    


        
        

